#!/usr/bin/env python3

"""
Author:             Jonathan Stites
Date Created:       2015-01-20
Company:            Dovetail Genomics

Takes as input one or many component_x.txt files from HiRise 
component_chunk_filter.py and outputs a plot of the CDFs of
the connected components.

"""

import sys
import argh
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np


@argh.arg("-l", "--labels", nargs="+")
@argh.arg("-c", "--components", nargs="+", required=True)
def main(components: "component_x.txt"=None,
         labels: "Labels of components"=None,
         output: "Output file"=None,
         show: "Show figure in X server"=False,
         num: "Number of connected components" =10):
    if len(labels) != len(components):
        print("Labels not equal to components")
        print(components)
        print(labels)
        sys.exit(4)
    sizes = []
    for c in components:
        l = collect_component_lengths(c)
        sizes.append(l)

    plot_cc(sizes, output, show, num, labels)
        

def plot_cc(sizes, output, show, num, labels):
    ax = plt.subplot(111)
    for size, label in zip(sizes, labels):      
        cdf = get_cdf(size)
        x = range(num)
        y = cdf[:num]

        plt.scatter(x, y)
        plt.plot(x, y, label=label)
    plt.yticks(np.arange(0, 1, 0.1))
    plt.grid(True)
    plt.xlabel("Number of connected components")
    plt.ylabel("Fraction of contigs")
    plt.legend()

    if show:
        plt.show()



def get_cdf(size):
    cdf = []
    total = sum(size)
    c = 0
    for s in sorted(size, reverse=True):
        c += s/total
        cdf.append(c)
    return cdf

def collect_component_lengths(component_file):
    l = []
    with open(component_file) as c:
        for line in c:
            if line.startswith("c:"):
                size = int(line.split()[3])
                l.append(size)
    return l

if __name__ == "__main__":
    argh.dispatch_command(main)
