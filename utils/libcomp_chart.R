#!/usr/bin/env Rscript

suppressPackageStartupMessages(library(argparse))
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(reshape2))

parser <- ArgumentParser()
parser$add_argument("--input", type="character", required=TRUE, nargs="+")
parser$add_argument("--names", type="character")
parser$add_argument("--plot", type="character", required=TRUE)
parser$add_argument("--table", type="character")
parser$add_argument("--header", type="character")
args <- parser$parse_args()


foo <- lapply(args$input, read.table, sep="\t", header=TRUE)

names <- strsplit(args$names, ",")

cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

foo2 <- lapply(foo, "[[", "Percent")

foo2$Library <- lapply(foo, "[[", "Type")[1]



d <- setNames(data.frame(foo2), unlist(c(names, "Library")))
comp_names <- d$Library
df <- melt(d, id.var=length(d))


my_plot <- ggplot(data=df, aes(x=Library, y=value, fill=variable)) + 
geom_bar(stat="identity", position="dodge") + scale_fill_manual(values=cbbPalette) + theme_bw() +
theme(text=element_text(size=20)) + labs(title=args$header, x="", y="Percent") + scale_x_discrete(limits=comp_names)
ggsave(args$plot, width=20, height=10)

for (i in seq(1, length(d) -1)) {
    d[,i] <- paste(d[,i], "%")
}

write.table(d[,c(length(d), 1:length(d)-1)], file=args$table, quote=FALSE, sep="\t", row.names=FALSE)
