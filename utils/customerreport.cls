\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{customerreport}[09/09/2015 Customer report class]
\LoadClass{article}
\RequirePackage{titlesec}
%\RequirePackage{graphicx}
\RequirePackage{grffile}
\graphicspath{ {images/} }
\RequirePackage{csvsimple}
\RequirePackage[letterpaper, margin=.5in]{geometry}
\RequirePackage{eso-pic}
\RequirePackage{background}
\RequirePackage{tikz}
\RequirePackage[Q=yes]{examplep}
\RequirePackage{hyperref}
\RequirePackage{caption}
\RequirePackage{threeparttable}
\RequirePackage[norule]{footmisc}
\usepackage[nopostdot,nonumberlist,sort=def]{glossaries}
\setlength{\footnotemargin}{0.6\textwidth}
\setlength{\skip\footins}{0pt}

\renewcommand{\footnoterule}{}
%uncomment below for footnote rule on right side of page
%\renewcommand{\footnoterule}{\vspace*{-3pt}\hspace*{0.5\textwidth}\rule{3in}{0.4pt}\vspace*{2.6pt}}

\newcommand{\makeDTtitle}[4]{
  \NoBgThispage
  \begin{titlepage}
    \centering
    \includegraphics[width=\textwidth]{#1}\par
    \vspace{6.5cm}
    {\Huge Dovetail #2 Genome Assembly\\
     \vspace{1cm}
     #3\\
     #4\\
     \today\\\par}
    \vfill
  \end{titlepage}
}


\newcommand{\makeDTtitleNoInstitution}[3]{
  \NoBgThispage
  \begin{titlepage}
    \centering
    \includegraphics[width=\textwidth]{#1}\par
    \vspace{6.5cm}
    {\Huge Dovetail #2 Genome Assembly\\
     \vspace{1cm}
     #3\\
     \today\\\par}
    \vfill
  \end{titlepage}
}

\newcommand{\DTglossary}{
  \makeglossaries
  \newglossaryentry{sequence-coverage}{
    name={Sequence Coverage},
    description={For a given position in the genome, the sequence coverage
                 is the number of times this basepair is directly observed in the
                 sequencing data. Typically given as an average over the
                 whole genome, or estimated by the total length of reads divided
                 by the genome size.}
  }

  \newglossaryentry{physical-coverage}{
    name={Physical Coverage},
    description={For a given position in the genome, the physical coverage
                 is the number of read pairs that span this position. Typically 
                 given as an average over the whole genome, or estimated by 
                 the area under the insert distribution divided
                 by the genome size.}
  }

  \newglossaryentry{contig}{
    name=Contig,
    description={A contiguous genomic sequence without any gaps in an assembly.}
  }

  \newglossaryentry{scaffold}{
    name=Scaffold,
    description={A genomic sequence consisting of contigs that have been ordered
                 and oriented relative to each other. Contigs within scaffolds 
                 are separated by gaps (indicated by stretches of Ns).}
   }

  \newglossaryentry{N50}{
    name=N50,
    description={The scaffold length such that the sum of the lengths of all
                 scaffolds of this size or larger is equal to 50\% of the total
                 assembly length.}
  }

  \newglossaryentry{N90}{
    name=N90,
    description={The scaffold length such that the sum of the lengths of all
                 scaffolds of this size or larger is equal to 90\% of the total
                 assembly length.}
  }
 
}


\newcommand{\project}[1]{
  \centering{\Huge{#1\\}}
}

\newcommand{\dovetail}[1]{
  \centering{\Large{#1\\}}
}

\newcommand{\logo}[1]{
  \begin{figure}[b]
    \includegraphics[width=3cm, height=1.5cm]{#1}
  \end{figure}
}
    
\newcommand{\stats}[1]{
  \begin{center}
    \csvautotabular[separator=pipe]{#1}
  \end{center}
}

%\newcommand{\miscstats_old}[1]{
%  \csvreader[centered tabular=|l|c|,table head=\hline\multicolumn{2}{|c|}{Miscellaneous Assembly Stats}\\\hline,table foot=\hline]{#1}{}{\csvcoli & \csvcolii}
%}

\newcommand{\comparestats}[1]{
  \begin{center}
    \begin{threeparttable}
    \begin{tabular}{|l|c|c|}
      \hline
      \multicolumn{3}{|c|}{ Comparative Assembly Statistics}\\
      \hline
      \csvreader[late after line=\\\hline,head=false,separator=pipe]{#1}{}{\csvcoli & \csvcolii & \csvcoliii}
    \end{tabular}
    \begin{tablenotes}
      {\footnotesize \item[*] Note: Every join made by HiRise creates a gap.}
    \end{tablenotes}
    \end{threeparttable}
  \end{center}
}

\newcommand{\comparestatssmall}[1]{
  \begin{center}
    \scalebox{0.90}{
    \begin{threeparttable}
    \begin{tabular}[h]{|l|c|c|}
      \hline
      \multicolumn{3}{|c|}{ Comparative Assembly Statistics}\\
      \hline
      \csvreader[late after line=\\\hline,head=false,separator=pipe]{#1}{}{\csvcoli & \csvcolii & \csvcoliii}
    \end{tabular}
    \begin{tablenotes}
      {\footnotesize \item[*] Note: Every join made by HiRise creates a gap.}
    \end{tablenotes}
    \end{threeparttable}
    }
  \end{center}
}


\newcommand{\miscstatssmall}[1]{
%  \begin{center}
  \scalebox{0.90}{
    \begin{tabular}[h]{|p{10cm}|c|}
      \hline
      \multicolumn{2}{|c|}{Other Statistics}\\
      \hline
      \csvreader[late after line=\\\hline,head=false,separator=pipe]{#1}{}{\csvcoli & \csvcolii}
    \end{tabular}
  }
%  \end{center}
}

\newcommand{\miscstats}[1]{
  \begin{center}
    \begin{tabular}{|p{9cm}|c|}
      \hline
      \multicolumn{2}{|c|}{Other Statistics}\\
      \hline
      \csvreader[late after line=\\\hline,head=false,separator=pipe]{#1}{}{\csvcoli & \csvcolii}
    \end{tabular}
  \end{center}
}
        
\newcommand{\contiguityploteps}[1]{
  \begin{center}
  \begin{figure}[h]
    \input{#1}
    \caption*{A comparison of the contiguity of the input assembly and the final 
              HiRise scaffolds. Each curve shows the fraction of the total length of the assembly 
              present in scaffolds of a given length or smaller. The fraction of 
              the assembly is indicated on the Y-axis and the scaffold length 
              in basepairs is given on the X-axis. The two dashed lines mark the 
              N50 and N90 lengths of each assembly. This plot excludes scaffolds 
              less than 1 kb.}
  \end{figure}
  \end{center}
}

\newcommand{\contiguityplotpdf}[1]{
  \begin{figure}[h]
    \includegraphics[width=1\textwidth]{#1}
    \caption*{A comparison of the contiguity of the input assembly and the final 
              HiRise scaffolds. Each curve shows the fraction of the total length of the assembly 
              present in scaffolds of a given length or smaller. The fraction of 
              the assembly is indicated on the Y-axis and the scaffold length 
              in basepairs is given on the X-axis. The two dashed lines mark the 
              N50 and N90 lengths of each assembly. This plot excludes scaffolds 
              less than 1 kb.}
  \end{figure}
}

\newcommand{\insertdistribution}[1]{
  \begin{figure}[h]
    \includegraphics[width=1\textwidth]{#1}
    \caption*{This figure shows the distribution of insert sizes in the Chicago 
              library. The distance between the forward and reverse reads is
              given on the X-axis in basepairs, and the probability of observing a read 
              pair with a given insert size is shown on the Y-axis.}
  \end{figure}
}

\newcommand{\coveragehisteps}[1]{
  \begin{center}
  \begin{figure}[h]
    \input{#1}
    \caption*{Histogram of Chicago coverage over 5000 randomly sampled sites.
              Coverage values are calculated as the number of Chicago read pairs
              with inserts between 1 and 100kb spanning the sampled site.}
  \end{figure}
  \end{center}
}

\newcommand{\coveragehist}[1]{
  \begin{figure}[h]
    \includegraphics[width=1\textwidth]{#1}
    \caption*{Histogram of Chicago coverage over 5000 randomly sampled sites.
              Coverage values are calculated as the number of Chicago read pairs
              with inserts between 5 and 15kb spanning the sampled site.}
  \end{figure}
}

\newcommand{\syntenyplot}[1]{
  \begin{figure}[h]
    \noindent\makebox[\textwidth]{
       \includegraphics[width=1.5\textwidth]{#1}}
        \caption*{The figure shows a comparison of marker synteny between the final HiRise assembly and the reference genome. The vertical lines separate each scaffold in the reference, while the horizontal lines separate HiRise scaffolds. HiRise scaffolds are ordered by the median position of their conserved markers in the reference genome and reference scaffolds are ordered by size. The $(x,y)$ coordinates of each dot represent the positions of a single annotated protein-coding gene in the reference and its best chained BLASTN hit in the HiRise assembly respectively. Reference scaffolds are labeled on the top of the plot.}
  \end{figure}
  
}

\newcommand{\backgroundlogo}[1]{
  \begin{tikzpicture}[remember picture,overlay,yshift=2cm, xshift=2.5cm]
    \node at (0,0) {\includegraphics[width=4cm,height=2cm]{#1}};
  \end{tikzpicture}
}


\newcommand{\placelogo}[1]{
  \SetBgContents{\backgroundlogo{#1}}
  \SetBgPosition{current page.south west}% Select location
  \SetBgOpacity{1.0}% Select opacity
  \SetBgAngle{0.0}% Select roation of logo
  \SetBgScale{1.0}% Select scale factor of logo
}

\newcommand{\coverage}[1]{
  \centerline{Estimated Chicago physical coverage (1-50 Kb pairs): #1 X}
}

\newcommand{\inputmisjoins}[1]{
  \centerline{Number of broken input misjoins: #1}
}

%\newcommand{\synteny}[3]{
%  \begin{figure}[hp]
%    #2 HiRise vs. #3 Reference
%    \centering
%    \includegraphics[width=1\textwidth]{#1}
%  \end{figure}
%}

\newcommand{\synteny}[3]{
  \begin{minipage}{\linewidth}
    #2 HiRise vs. #3 Reference
    \centering
    \includegraphics[width=1\textwidth]{#1}
  \end{minipage}
}


\newcommand{\header}[1]{
  \centering{\Large{#1}}
}

%\newcommand{\sharelink}[1]{
%  \centering
%  Google Drive: Final scaffolds, sequence and contig layout\\
%  #1\\
%}

%\newcommand{\sftp}[3]{
%  \centering
%  SFTP: Dovetail library data aligned to original assembly in BAM format\\
%  \vspace{10 px}
%  #1\\
%  login: #2\\
%  password: #3\\
%}

%\newcommand{\data}[4]{
%  \centering
%  \sharelink{#1}
%  \vspace{40 px}
%  \sftp{#2}{#3}{#4}
%}

\newcommand{\sftp}[3]{
  \centering
  SFTP: Dovetail library data aligned to original assembly in BAM format\\
  \vspace{10 px}
  #1\\
  login: #2\\
  password: #3\\
}

\newcommand{\data}[3]{
  \centering
  \vspace{40 px}
  \sftp{#1}{#2}{#3}
}
