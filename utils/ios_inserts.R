#!/usr/bin/env Rscript

suppressPackageStartupMessages(library(argparse))
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(reshape2))

parser <- ArgumentParser()
parser$add_argument("--input", type="character", required=TRUE)
parser$add_argument("--header", type="character")
parser$add_argument("--xlim", type="integer", default=0)
parser$add_argument("--output", type="character", required=TRUE)
args <- parser$parse_args()

d <- read.table(args$input, header=TRUE)
d <- setNames(data.frame(d$size, d$innies, d$outties, d$same), c("Distance", "innies", "outties", "same"))
d <- melt(d, id.var=1)

xlim = args$xlim

if (xlim == 0) {
    xlim = max(d$Size)
}

my_plot <- ggplot(data=d, aes(x=Distance, y=value, color=variable)) + geom_point(size=4) +  scale_y_log10() +
    theme_bw() + theme(text=element_text(size=20)) +
    labs(title=args$header, x="Insert (bp)", y="Count") +
    scale_x_continuous(breaks=seq(0, xlim, xlim/5)) +
    coord_cartesian(xlim=c(0, xlim)) + scale_color_manual(values=c("red", "blue", "black")) 

ggsave(args$output, width=15, height=10)
      
             
