#!/usr/bin/env python3

from setuptools import setup
import glob

setup(name='hirise',
      version='0.9.4',
      description="Dovetail assembly pipeline for Chicago data.",
      author='Nicholas Putnam',
      author_email='nik@dovetail-genomics.com',
      license='None',
      packages=['hirise'],
      install_requires=[
          'pysam>=0.8.4',
          'numpy',
          'scipy',
          'future',
          'networkx==1.9.1',
          'snakemake<4.0.0',
          'argh',
          'matplotlib',
          'h5py'
      ],
      zip_safe=False,
      scripts =  [f for f in glob.glob('hirise/*.py') if f != "hirise/__init__.py"] +
                 [f for f in glob.glob('hirise/*.sh')])

