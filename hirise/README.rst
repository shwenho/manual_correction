
HiRise Assembly Pipeline
------------------------

To install:

``python ./setup.py develop``

To run the assembler, set up a config.json file, then start the snakemake pipeline:

``python3 ~/downloads/snakemake/bin/snakemake -p -j 16 --snakefile ~/dovetail/snake/snakefile.hirise assembly_report.org``

