#!/usr/bin/env python3


import argh


def main(audits, scaffold_lengths, output="/dev/stdout", min_stretch=1000):
    lengths = read_scaffold_lengths(scaffold_lengths)
    dump_lengths = []
    running_total = 0
    seen = set()
    with open(output, 'w') as output_handle:
        for scaffold, o, stretch in parse_audits(audits, min_stretch):
            if scaffold in seen:
                continue
            seen.add(scaffold)
            print("debug, got", scaffold, o, stretch, sep="\t")
            if o > 0:
                end1, end2 = 5, 3
            else:
                end1, end2 = 3, 5
            this_length = lengths[scaffold]
            
            print("P", 1, scaffold, 1, end1, running_total, this_length, file=output_handle)
            running_total += this_length
            print("P", 1, scaffold, 1, end2, running_total, this_length, file=output_handle)
            running_total += 100
            dump_lengths.append((scaffold, this_length,))

        for scaffold, length in dump_lengths:
            print("L", scaffold, length, file=output_handle)

# will output scaffold multiple times if seen multiple times
#AAAGATAGAAACATCAATGATGAGAGAGAATCATTGATCAGCTGCCTCCTG     GL430014        1212706 +       Scaffold2       3197    +
def parse_audits(audits, min_stretch):
    previous_scaffold_name = None
    reference_start = None
    previous_reference_start = None
    same_o = 0
    scaffold_start = None
    previous_scaffold_start = None
    with open(audits) as handle:
        for line in handle:
            if "None" in line:
                continue
            s = line.split()
            this_scaff_name = s[1]
            this_scaff_start = int(s[2])
            this_scaff_o = s[3]

            this_ref_name = s[4]
            this_ref_start = int(s[5])
            this_ref_o = s[6]

            if not previous_scaffold_name:
                previous_scaffold_name = this_scaff_name
                same_o = 0
                reference_start = this_ref_start
                scaffold_start = this_scaff_start
                previous_reference_start = this_ref_start
                previous_scaffold_start = this_scaff_start

            print("debug1 ", previous_scaffold_name, scaffold_start, previous_scaffold_start, same_o, reference_start, previous_reference_start, this_ref_start, previous_reference_start - reference_start)
            if previous_scaffold_name != this_scaff_name:
                print("debug, maybe yielding", previous_scaffold_name, same_o, previous_reference_start - reference_start)
                if previous_reference_start - reference_start > min_stretch:
                    if scaffold_start > previous_scaffold_start:
                        same_o = same_o * - 1
                    print("debug, yielding", previous_scaffold_name, same_o, previous_reference_start - reference_start)
                    yield previous_scaffold_name, same_o, previous_reference_start - reference_start
                else:
                    pass
                    #print("did not pass", scaff_name, same_o, ref_start - scaffold_start)
                previous_scaffold_name = this_scaff_name
                same_o = 0
                reference_start = this_ref_start
                scaffold_start = this_scaff_start
            previous_reference_start = this_ref_start
            previous_scaffold_start = this_scaff_start
            print("debug2 ", previous_scaffold_name, same_o, reference_start, previous_reference_start, this_ref_start, previous_reference_start - reference_start)
            if this_scaff_o == this_ref_o:
                same_o +=1
            else:
                same_o -= 1

        if previous_scaffold_name and reference_start - this_ref_start > min_stretch:
            if scaffold_start > previous_scaffold_start:
                same_o = same_o * -1
            yield previous_scaffold_name, same_o, reference_start - this_ref_start
            
def read_scaffold_lengths(scaffold_lengths):
    lengths = {}
    with open(scaffold_lengths) as handle:
        for line in handle:
            length, scaffold = line.split()
            lengths[scaffold] = int(length)
    return lengths


if __name__ == "__main__":
    argh.dispatch_command(main)
