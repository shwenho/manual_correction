#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import sys

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-t','--threshold',default=0.0 ,  type=float)
    parser.add_argument('-r','--ref')
    parser.add_argument('-a','--ass') 
    parser.add_argument('-s','--skip',default=0,type=int) 
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    args = parser.parse_args()
    #ces.debug=args.debug
    if args.debug:
        args.progress=True
    if args.progress: log( str(args) )

    lastx=0
    lasts=-1
    lp1=False
    lp2=False
    f=open(args.ref)
    skipped=args.skip
    while True:
        l=f.readline()
        if not l: break
        if skipped<args.skip: 
            skipped +=1
            continue
        else:
            skipped=0
        c=l.strip().split()
        if len(c)<7: continue
        if c[2]=="None": continue
        
        p1=(c[1],int(c[2]),c[3])
        p2=(c[4],int(c[5]),c[6])
        if lp2 and p2[0]==lp2[0]:
            if p1[0]==lp1[0]:
                synt=1
                sm=1
                if lp2[2]==p2[2] and not lp1[2]==p1[2]: sm=0
                if not lp2[2]==p2[2] and lp1[2]==p1[2]: sm=0
                print(abs(p1[1]-lp1[1]),abs(p2[1]-lp2[1]),sm,synt)
            else:
                synt=0
                sm=0
                print(-1,abs(p2[1]-lp2[1]),sm,synt)
                
        lp1=p1
        lp2=p2
#            print "#",l.strip()
         #   continue

        
    f.close()

