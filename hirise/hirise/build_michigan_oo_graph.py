#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import networkx as nx
from networkx.algorithms import bipartite
import simulateMichigan as sm
from genomeUtils import build_chromosome_dict

def r2contig(r,l):
    return "%s.%d" % (r[4],int(old_div(float(r[0]),l)))


if __name__=="__main__":

    import sys
    import argparse
    import re
    parser = argparse.ArgumentParser(description="""
A script for simulating scaffolding information in a Dovetail Microarray "Michigan" data set.

This script simulates an idealized version of the data, then constructs a graph of relationships between contigs of a genome.  The contigs are of a uniform size, separated by zero-length gaps.  On output, print in matrix form the number of 
microarray spots indicating linkage of pairs of contigs.  

""")
    parser.add_argument('-c','--chromosomes',required=True,help="A file which lists the chromosomes in the genome and the length of each.")
    parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging output.")
#parser.add_argument('-n','--nbreaks',default=100,type=int)
    parser.add_argument('-C',"--multiplicity",default=100,type=int,help="What's the mean number of aggregates per spot -- this gets used to calculate the total number of aggregates.")
    parser.add_argument('-m',"--microarraySpots",default=90000,type=int,help="How many spots are on the array?")
    parser.add_argument('-L','--length',default=150000,type=int,help="What's the mean length of the DNA in chromatin aggregates?")
    parser.add_argument('-u','--lengthUnc',default=15000,type=int,help="What's the standard deviation of the length of the DNA in chromatin aggregates?")
    parser.add_argument('-N','--nreads',default=500000000,type=int,help="How many reads (assuming forward reads only) get sampled?")
    parser.add_argument('-f','--noiseFraction',default=0.25,type=float,help="What fraction of the reads are noise, in the sense that they don't come from a 'set' from get ligated independently to a random spot.")
    parser.add_argument('-r','--readLength',default=100 ,type=int,help="ignored for now.")
    parser.add_argument('-H','--head',default=False ,type=int,help="Don't simulate a whole genome's worth of data, just sample reads from the first HEAD bases of chr1.")

    parser.add_argument('-l','--contigSize',default=5000,type=int,help="Chop the chromosomes into 'contigs' of this length")
    parser.add_argument('-M','--minSetAnchor',default=2,type=int,help="The threshhold number of reads to 'anchor' a contig to a set (spot)")
    parser.add_argument('-S','--minSpots',default=2,type=int,help="The threshhold number of shared anchored spots to infer scaffolding links between contigs.")
    parser.add_argument('--hilight',default=False,type=int,help="Hilight the edges between contigs that are HILIGHT steps away along the chromosome.")
    parser.add_argument('--colorCode',default=False,type=str,help="Display matrix in terminal color codes.")

#    print sys.argv

    i=0
    zeros={'k':'000','K':'000','M':'000000',"G":'000000000'}
    for i in range(1,len(sys.argv)):
        arg = sys.argv[i]
#        print arg,"."
        m=re.match("\d+([MGKk])",arg)
        if m:
            arg = re.sub(m.group(1),zeros[m.group(1)],arg)
#            print arg,m.group(0)
            sys.argv[i]=arg
    main_args = parser.parse_args(sys.argv[1:])

#    print main_args

    if True or main_args.debug:
        print(main_args)

    chromLen=build_chromosome_dict(main_args.chromosomes)
    
    G=nx.Graph()

    data = sm.Michigan_data_generator(main_args,chromLen)
    print(data.params_blurb())
    spots = {}
    contigs = {}
    contigs_in_order=[]
    for r in data.generate_reads():
        contig = r2contig(r,main_args.contigSize) 
        if main_args.debug:
            print(r)
        if G.has_edge(contig,r[3]):
            G[contig][r[3]]['weight']+=1
        else:
            G.add_edge(contig,r[3],weight=1)    
        spots[r[3]]=1+spots.get(r[3],0)
        contigs[contig]=1+contigs.get(contig,0)
        if not contigs_in_order or not contigs_in_order[-1]==contig:
            contigs_in_order.append(contig)
        #write_output_line(r,"x",main_args.readLength)

#    for s in spots.keys():
#        print s,spots[s]


#    print G.edge
    to_remove=[]
    for (i,j) in G.edges():
        w = G[i][j]['weight'] 
        if main_args.debug:
            print("#weight",i,j,w)
        if w < main_args.minSetAnchor:
            to_remove.append((i,j))
        G[i][j]['weight'] =1 #  This is so that in the next step we can count sets, not reads


#    print len(G.edges())
    #print to_remove
    G.remove_edges_from(to_remove)
#    print len(G.edges())
        
#        if G[e]['weight] 

    H = bipartite.weighted_projected_graph(G,list(contigs.keys()))
#    print G.n_edges(),G.n_nodes(),len(spots.keys())

    to_remove=[]
    for (i,j) in H.edges():
        w = H[i][j]['weight'] 
        if main_args.debug:
            print("#weight",i,j,w)
        if w < main_args.minSpots:
            to_remove.append((i,j))
    H.remove_edges_from(to_remove)
    

#    for n in H.nodes():
#        print n,H[n]

    for i in range(len(contigs_in_order)):
        c1=contigs_in_order[i]
        print("% 10s" % (c1), end=' ')
        for j in range(len(contigs_in_order)):
            c2=contigs_in_order[j]
        
            w="    "
            try:
                if main_args.hilight and abs(i-j)==main_args.hilight:
                    w = "\033[1;31m% 4d\033[0m" % (H[c1][c2]['weight'])
                else:
                    w = "% 4d" % (H[c1][c2]['weight'])
                
            except:
                pass
            print(w, end=' ')
        print("")
    
    print("bold red test\n")
