#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3

#x: 15 4 Scaffold182986_1 (49540, -1) True
#x: 15 3 Scaffold41819_1 (37560, 1) False
#x: 15 2 Scaffold31942_1 (11215, 1) False
#x: 15 1 Scaffold136140_1 (8586, 1) False
#x: 16 6 Scaffold173446_1 (28440, 1) False
import sys

ll={}
f=open(sys.argv[1])
while True:
    l=f.readline()
    if not l: break
    if l[0]=="#": continue
    c=l.strip().split()
    if not len(c) > 1: 
#        print l
        break
    ll[c[0]]=int(c[1])
f.close()

sn=1
edgelines=[]
cc=[]
last_end=False
last_scaffold=-1
while True:
    l=sys.stdin.readline()
    if not l: break
    if l[0]=="#": continue
    if l[:3]=="cc:": print(l.strip())
    if l[:2]=="x:": 
        c = l.strip().split()
        scaffold,i,contig,x,s,strand = c[1],c[2],c[3],c[4],c[5],eval(c[6])
        x,s=eval("".join([x,s]))
        #        edgelines.append( "\t".join(map(str,["#edge:",contig+".3",contig+".5",{'length':ll.get(contig), 'contig':True}])))
        print("\t".join(map(str,["#edge:",contig+".3",contig+".5",{'length':ll.get(contig), 'contig':True}])))
        if scaffold==last_scaffold:
            #print "cc:",sn,len(cc),cc

            if not strand:   # strand is true if flipped.  since these lines are backtrace, it's 3'->5'
                print("\t".join(map(str,["#edge:",last_end,contig+".3",{'length':x-lastx, 'contig':False}])))
                #                edgelines.append( "\t".join(map(str,["#edge:",last_end,contig+".3",{'length':1000.0, 'contig':False}])))
            else:
                print("\t".join(map(str,["#edge:",last_end,contig+".5",{'length':x-lastx, 'contig':False}])))
                #edgelines.append( "\t".join(map(str,["#edge:",last_end,contig+".5",{'length':1000.0, 'contig':False}])))
        #else:
            #print "cc:",
            #for e in edgelines:
            #    print e
            #edgelines=[]

        if not strand: 
            last_end = contig+".5"
        else:
            last_end = contig+".3"
            
        last_scaffold=scaffold
        lastx=x
 
