#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
import sys
import re

id=False
left=""
right=""
ipil=-1
ipir=-1
while True:
    l=sys.stdin.readline()
    if not l: break
    c=l.strip().split('=')
    #print c
    lm=re.match("PRIMER_LEFT_(\d+)_SEQUENCE",c[0])
    lr=re.match("PRIMER_RIGHT_(\d+)_SEQUENCE",c[0])

    if c[0]=="SEQUENCE_ID":
        id = c[1]
    elif lm:
        left=c[1]
        ipil=int(lm.group(1))
    elif lr:
        right=c[1]
        ipir=int(lr.group(1))

    if id and ipil>=0 and ipil==ipir:
        print(id+"_"+str(ipil),left,right,10,1000)
        ipil=-1
