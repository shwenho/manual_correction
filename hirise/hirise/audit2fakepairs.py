#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import sys

strand_map = {
     ('+','+') : '+',
     ('+','-') : '-',
     ('-','+') : '-',
     ('-','-') : '+',
}

flip = {
     '+':'-',
     '-':'+'
}

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-t','--threshold',default=0.0 ,  type=float)
    parser.add_argument('-r','--ref')
#    parser.add_argument('-a','--ass') 
#    parser.add_argument('-L',default=10000.0,type=float) 
#    parser.add_argument('-T','--tolerance',default=0.95,type=float) 
#    parser.add_argument('-t','--thin',default=100,type=int) 
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    args = parser.parse_args()
    #ces.debug=args.debug
    if args.debug:
        args.progress=True
    if args.progress: log( str(args) )

#    targets = [5000,10000,50000,100000]

    thin=1
    p_buffer=[]
#    buffer_len=args.A
    lastx=0
    lasts=-1
    lp1=False
    lp2=False
    f=open(args.ref)
#    skipped=args.skip
    started=False
    lastp1=False
    lastp2=False
    while True:
        l=f.readline()
        if not l: break

        c=l.strip().split()
        if len(c)<7: continue
        if c[2]=="None": c[2]="-1"

        p1=(c[1],int(c[2]),c[3]) #position 1
        p2=(c[4],int(c[5]),c[6]) #position 2

        if lastp1 and lastp2[0]==p2[0] and not lastp1[0]==p1[0] and not lastp1[0]=="None" and not p1[0]=="None":
            s1 =      strand_map[lastp1[2],lastp2[2]]
            s2 = flip[strand_map[    p1[2],    p2[2]]]
#            print(lastp1[0],lastp1[1],s1,    p1[0],p1[1],s2, p2[1]-lastp2[1] ,lastp1,lastp2,p1,p2,sep="\t")
            print(lastp1[0],lastp1[1],s1,    p1[0],p1[1],s2, p2[1]-lastp2[1]                      ,sep="\t")

        lastp1=p1
        lastp2=p2

        
    f.close()

