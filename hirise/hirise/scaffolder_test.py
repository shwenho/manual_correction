#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
#!/usr/bin/env python3
import sys
import networkx as nx
import greedy_chicagoan as gs
import math

default_gapsize=100

def update_end_distance(end_distance,n,g):
    x=0
    q=[n]
    seen={}
    last=False
    while len(q)>0:
        m=q.pop(0)
        if last:
            try:
                x+=g[last][m]['length']
            except Exception as e:
                print("wtf?",last,m,x,n)
                print(math.log(-1.0))
        last=m
        if (not m in end_distance) or end_distance[m]>x: end_distance[m]=x
        
        seen[m]=True
        if len(g.neighbors(m))>2:
            print("topology error:",m,list(g.neighbors(m)))
            print(math.log(-1.0))
        for l in g.neighbors(m):
            if not l in seen:
                q.append(l)

    return x

#    print end_distance

#def llr(e1,e2):
#    return 1.0

L=200000.0

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
#    parser.add_argument('-L','--links')
    parser.add_argument('-s','--scaffolds')
#    parser.add_argument('-S','--alreadyDone')
    parser.add_argument('-b','--besthits')
    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')
    parser.add_argument('-F','--filter')
    parser.add_argument('-N','--name')
    parser.add_argument('-m','--minscore' ,default=5.0,type=float)
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
#    parser.add_argument('-K','--slices'   ,default=1,type=int)
#    parser.add_argument('-k','--slice'    ,default=0,type=int)
 
    args = parser.parse_args()
    if args.seed != -1 :
        random.seed(args.seed)
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    name_prefix=""
    if args.name:
        name_prefix=args.name
    else:
        import idGen
        name_prefix="Scf" + idGen.id()

    ll={}
    if args.lengths:
        f = open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            ll[c[0]]=int(c[1])
        f.close()

    besthit={}
    if args.besthits:
#        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
    #            print c[1],besthit[c[1]]
            f.close()
    if args.progress: print("#Done reading besthits")

    g=nx.Graph()
    if args.scaffolds:
        f=open(args.scaffolds)
        while True:
            l=f.readline()
            if not l: break
            c=l.strip().split()
            if c[0]=="#edge:":
                g.add_edge(c[1],c[2],eval(" ".join(c[3:])))
#                print "#add edge",c[1],c[2],eval(" ".join(c[3:]))
    sys.stdout.flush()
    sc=1
    scaffold={}
    for c in nx.connected_components(g):
        for cc in c:
            scaffold[cc]=sc
            scaffold[cc[:-2]]=sc
        sc+=1


#    scaffold_pairs_tested={}
#Scaffold50016_1 Scaffold40593_1 ['Scaffold77744_1.5', 'Scaffold246520_1.5'] ['Scaffold111955_1.3', 'Scaffold216064_1.3'] 1141 455 1 15 1

    joins_g=nx.Graph()

    scores={}
    while True:
        l=sys.stdin.readline()
        if not l: break
#        print l
#        print "\""+l[:10]+"\""
        if not l[:10]=="link score": continue
        c=l.strip().split()
#        print c[2],c[3],c[4],c[5],c[6]
        x=max(list(map(float,c[4:])))
        if x > args.minscore:
            scores[c[2],c[3]]=x
            joins_g.add_edge( c[2],c[3],weight=x )
#        scores[c[2],c[3]]=max(map(float,c[4:]))
#        print c[2],c[3],scores[c[2],c[3]]


    cnx=1
    ccd={}
    for c in nx.connected_components(g):
        for cc in c:
            ccd[cc]=cnx
        cnx+=1

    def same_component(s1,s2):
        cs1=ccd[s1]
        while cs1 in ccd: cs1=ccd[cs1]
        cs2=ccd[s2]
        while cs2 in ccd: cs2=ccd[cs2]
        if cs1==cs2: return True
        return False

    joins =list( scores.keys())
    joins.sort(key = lambda x: scores[x], reverse=True)
    linked={}
    for s1,s2 in joins:
#        print s1,s2,scores[s1,s2]
        bh1=besthit.get(s1[:-2],[-1,-1,-1,-1,-1])
        bh2=besthit.get(s2[:-2],[-1,-1,-1,-1,-1])
        es1=sorted([ e[2]['weight'] for e in joins_g.edges([s1],data=True) ],reverse=True)
        es2=sorted([ e[2]['weight'] for e in joins_g.edges([s2],data=True) ],reverse=True)
        di=1e10
        tag="skipped"
        if bh1[1]==bh2[1]:
            di=min(list(map(abs, [ int(bh1[3])-int(bh2[3]), int(bh1[3])-int(bh2[4]), int(bh1[4])-int(bh2[3]), int(bh1[4])-int(bh2[4]) ])) )
        if bh1[1]==-1 or bh2[1]==-1:
            d=-100
        if not s1 in linked and not s2 in linked and not same_component(s1,s2):
            tag="added"
            g.add_edge(s1,s2, {'length': default_gapsize, 'contig': False} )
            linked[s1]=True
            linked[s2]=True

            #track the merger of these components
            cs1=ccd[s1]
            while cs1 in ccd: cs1=ccd[cs1]
            cs2=ccd[s2]
            while cs2 in ccd: cs2=ccd[cs2]

            ccd[cs1]=cs2

        
        print("\t".join(map(str,[tag,s1,s2,scores[s1,s2],ll[s1[:-2]],ll[s2[:-2]],di,len(es1),len(es2),es1[:5],es2[:5]]+besthit.get(s1[:-2],[])+besthit.get(s2[:-2],[]) ))) 


    end_distance={}
    sn=1
    for sg in nx.connected_component_subgraphs(g):
        ends=[]
        bh_stats={}
        for n in sg.nodes():
            if sg.degree(n)==1:
                ends.append(n)

        if len(ends)==0: 
            print("why no ends?", sn) 
            sn+=1
            continue
        maxx=update_end_distance(end_distance,ends[0],sg)

# ['34329.0', '3', '+', '71834554', '71853152', '1', '1', '18598']

        t=0
        gap_len=0
        for s1,s2 in sg.edges():
            t+=sg[s1][s2]['length']
            if not sg[s1][s2]['contig']: gap_len += sg[s1][s2]['length']
            print("#",sn,s1,s2,sg[s1][s2]['length'],t)
        print(t,n,"slen",gap_len,t-gap_len)

        node_tlen=0
        nodes_list = list(sg.nodes())
        nodes_list.sort(key=lambda x: end_distance[x])
        for n in nodes_list:
            base_name,end_id=n[:-2],n[-1:]
            if end_id=="5": node_tlen+= ll[base_name]
            bh = besthit.get(base_name,False)
            x=-1
            chr="-"
            if bh:
                chr=bh[1]
                if bh[2]=="+":
                    if n[-1:]=="5":
                        x=int(bh[3])
                    else:
                        x=int(bh[4])
                if bh[2]=="-":
                    if n[-1:]=="5":
                        x=int(bh[4])
                    else:
                        x=int(bh[3])
                    
            print("p:",sn,n,end_distance[n],chr,x,t,ll[base_name],bh)
        print(node_tlen,"node_tlen")

        sn+=1

    exit(0)

    exit(0)

