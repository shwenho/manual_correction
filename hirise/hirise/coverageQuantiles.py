#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import pysam
import sys
import hirise.mapper
from collections import deque
#import heapq
from itertools import chain
import bisect
import re
import hirise.chicago_edge_scores as ces
import math

def log(s):
    sys.stderr.write(  "%s\n" %(s) )


def find_coverage_quantiles(model,xmin,xmax,nquantiles):
    ces.set_exp_insert_size_dist_fit_params(model)
    N=model['N']
    G=model['G']
#    print(N,G,model,xmin,xmax,nquantiles)
    r=[]
#    print (N/G)
    d= ((N/G))*( ces.model.H0_good(xmax) - ces.model.H0_good(xmin)  )
    #print("total depth in pairs between {} and {}: {}".format(xmin,xmax,d))
    
    def find_quantile_cutoff(model,xmin,dt,xmax=xmax):
        xmin0=xmin
        N=model['N']
        G=model['G']
        x=old_div((xmax-xmin),2)
        d=(old_div(N,G))*( ces.model.H0_good(x) - ces.model.H0_good(xmin0)  )
        if dt == 0:
            return x
#        print "bt:",xmin,xmax,x,dt,d,abs(d-dt)/dt

        while (abs(d-dt)/dt) > 0.01 and (xmax-xmin)>1:
#            print "bt:",xmin,xmax,x,dt,d,abs(d-dt)/dt
            if d>dt:
                xmax=x
                x=xmin+old_div((x-xmin),2)
            else:
                xmin=x
                x=xmax-old_div((xmax-x),2)
            d=(old_div(N,G))*( ces.model.H0_good(x) - ces.model.H0_good(xmin0)  )
        return x

    lastx=xmin
    dd = (d/nquantiles)
#    print(d)
    for i in range(nquantiles):
        d_cum = dd*(i+1)
        x=find_quantile_cutoff(model,xmin,d_cum,xmax)
        r.append([dd,lastx,x,(old_div(N,G))*( ces.model.H0_good(x) - ces.model.H0_good(xmin))])
#        print(dd,"X coverage in the range",lastx,"to",x, (old_div(N,G))*( ces.model.H0_good(x) - ces.model.H0_good(xmin)  ))
        lastx=x
    return r

def main():
    import sys

    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-N','--nquantiles',default=4,type=int)
    parser.add_argument('--xmin',default=1000.0,type=float)
    parser.add_argument('--xmax',default=200000.0,type=float)
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('-M','--set_insert_size_dist_fit_params')

    args = parser.parse_args()
    nodes={}

    fmodel=open( args.set_insert_size_dist_fit_params )
    contents = fmodel.read()
    try:
        fit_params=eval(contents)
    except:
        "couldn't deal with option", args.param
    fmodel.close

 #   print fit_params
    model=fit_params

    xmin=args.xmin
    xmax=args.xmax
    nquantiles = args.nquantiles

    rr=find_coverage_quantiles(model,xmin,xmax,nquantiles)
    for r in rr:
#        print(dd,"X coverage in the range",lastx,"to",x, (old_div(N,G))*( ces.model.H0_good(x) - ces.model.H0_good(xmin)  ))

        print("{} X coverage in the range {} to {} {}".format(r[0],r[1],r[2],r[3]))

if __name__=="__main__":
    main()
