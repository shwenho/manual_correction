#!/usr/bin/env python3

import argh
import sys

def main(input_file=None, output=None):
    if not input_file: in_file = sys.stdin
    else: in_file =  open(input_file,'r')
    if not output: output = sys.stdout
    else: output = open(output,'w')
    prev_line = next(in_file)[:-1]
    prev_scaf = prev_line.split()[1]
    split = False
    print("#See manifest.txt for detailed description of fields.",file=output)
    print('\t'.join(["#Hirise_scaf_id","input_scaf_id","input_start","input_end","strand","hirise_start","hirise_end"]),file=output)
    for line in in_file:
        data = line.split()
        if data[1] == prev_scaf:
            print(prev_line,file=output)
            split = True
        elif split:
            print(prev_line,file=output)
            split = False
        prev_line = line[:-1]
        prev_scaf = data[1]
    if split: print(prev_line,file=output)
    in_file.close()
    output.close()



if __name__ == "__main__":
    argh.dispatch_command(main)
