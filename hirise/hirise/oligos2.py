#!/usr/bin/env python3
from __future__ import print_function
from builtins import range

p5array  =              "TCCCTACACGACGCTCTTCCGATCT"#-->
#bc template  XXXXXXXXXXX.........................BBBBBBBBBBGATC 
#             12345678901234567890123456789012345678901234567890


#from string import str.maketrans

tr = str.maketrans("ACTGactg","TGACtgac")

def rc(s):
    r=s[::-1]
    rc = r.translate(tr)
    return(rc)

k=10
bases=["A","C","T","G"]
def int2seq(x):
    s=""
    for i in range(k):
        b =  x & 3
        x = x>>2
        s = s+bases[b]
    return s

import random

def longest_homo(s):
    maxl=0
    l=1
    for i in range(1,len(s)):
        if s[i-1]==s[i] and not s[i]=="X": 
            l+=1
            if l>maxl: maxl=l
        else:
            l=1
    return maxl

p5rc=rc(p5array)

seen={}

while True:
    x =random.getrandbits(k*2)
    if x in seen: continue
    seen[x]=1
    s=int2seq(x)
    ol= "AATT"+s+p5rc+("X"*11)
    worst=0
    worsti=-1
    for i in range(k+4+1-1):
        m=0
        for j in range (8):
            if ol[i:][j]==p5rc[j]: m+=1
        if m>worst: 
            worst=m
            worsti=i
#        print i,m
#        print ol[i:]
#        print p5rc

    
    print(worst,worsti,longest_homo(ol),ol)
#    print ol[worsti:]
#    print p5rc
#    print " "*(k+4)+rc(p5array)
