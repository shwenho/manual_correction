#!/usr/bin/env python3
import hirise.mapper
import sys
import pysam

sam=pysam.Samfile(sys.argv[1])
h=sam.header
seqs=h['SQ']

#    seqs=h['SQ']

#    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

slen=[ s['LN'] for s in seqs ]
snam=[ s['SN'] for s in seqs ]


m=mapper.map_graph( snam, slen , minlength=1000)

m.write_map_to_file(sys.argv[2])
    

