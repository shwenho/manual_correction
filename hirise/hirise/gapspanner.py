#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import pysam
import sys
import hirise.mapper
from collections import deque
#import heapq
from itertools import chain
import bisect
import re
import gaussian_edge_scores as ces
import math


#from string import str.maketrans


tr = str.maketrans("ACTGactg","TGACtgac")

def rc(s):
    r=s[::-1]
    rc = t.translate(tr)
    return(rc)



def log(s):
    sys.stderr.write(  "%s\n" %(s) )

if __name__=="__main__":
    import sys

#    print " ".join(sys.argv)

    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-c','--chunk',required=True)
    parser.add_argument('-b','--bamfiles',required=True)
#    parser.add_argument('-i','--ignoreDuplicates',default=True,action="store_false")
    parser.add_argument('-q','--mapq',default=10,type=int)
    parser.add_argument('-t','--trim',default=100,type=int)
    parser.add_argument('-L','--minlen',default=1000,type=int)

    parser.add_argument('-f','--nfact',default=1.0,type=float)
    parser.add_argument('-A','--end_threshold',default=25.0,type=float)
    parser.add_argument('-B','--support_cutoff',default=10.0,type=float)
    parser.add_argument('-R','--range_cutoff',default=200000.0,type=float)
    parser.add_argument('-n','--pn',type=float)

    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('-M','--set_insert_size_dist_fit_params')

    args = parser.parse_args()
    nodes={}

    range_cutoff=args.range_cutoff
    t1=args.end_threshold
    t2=args.support_cutoff

#    recombinings2.max_n_breaks=args.max_n_breaks

    fmodel=open( args.set_insert_size_dist_fit_params )
    contents = fmodel.read()
    try:
        fit_params=eval(contents)
        fit_params['N']*=args.nfact
    except:
        "couldn't deal with option", args.param
    fmodel.close

    if args.pn: fit_params['pn']=args.pn
    ces.set_exp_insert_size_dist_fit_params(fit_params)

    N=fit_params['N']

    bamfiles = args.bamfiles.split(",")

    oname={}
    bams=[]
    for bamfile in bamfiles:
        bams.append( pysam.Samfile(bamfile,"rb") )
        oname[bams[-1]]=bamfile
    h=bams[0].header
    seqs=h['SQ']

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]

    llen={}
    for i in range(len(snam)):
        llen[snam[i]]=slen[i]

    last_scaffold=-1
    templates={}
    is_reverse={}
    gn=0
    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        if not c[0]=="p:": continue
        
        scaffold,end = c[1],c[2]
        contig = end[:-2]
        length=int(c[7])
        blast_coord = int(c[5])
        
        blast_chr = c[4]
        ocontig,base=contig.split("_")

        bh=eval(" ".join(c[8:]))
        if bh:
            unc =abs(length- abs(int(bh[3])-int(bh[4])))

            if bh[2]=="+" and end[-1]=="5": blast_coord -= int(bh[5])-1
            if bh[2]=="+" and end[-1]=="3": blast_coord += length-int(bh[6])-1
            if bh[2]=="-" and end[-1]=="5": blast_coord += int(bh[5])-1
            if bh[2]=="-" and end[-1]=="3": blast_coord -= length-int(bh[6])-1

        else:
            unc=0
    #"""o1 and o2 indicate the orientations of the two contigs being used  0 means 5' to 3'; 1 means flipped"""
    #if (o1,o2) == (0,0):     #         -------1----->    ----2------>
    #if (o1,o2) == (0,1):     #         -------1----->   <---2-------
    #if (o1,o2) == (1,0):     #        <-------1-----     ----2------>
    #if (o1,o2) == (1,1):     #        <-------1----->   <----2------

        ctrim=175.0

        if last_scaffold==-1 or (scaffold==last_scaffold and not contig==last_contig):
            region = "{}:{}-{}".format(ocontig,int(base),int(base)+length)

            is_reverse={}
            templates={}
            sequences={}
            qualities={}
            import struct
            for b in bams:
                for aln in b.fetch(region=region):
                    if aln.is_duplicate: continue
                    if aln.mapq<10.0: continue

                    templates[aln.qname]=aln.pos if not aln.is_reverse else aln.aend
#                    print dir(aln)
                    sequences[aln.qname] = aln.seq[:]
                    qualities[aln.qname] = aln.qual[:]
#                    print "seq:",aln.seq
#                    print "aend:",aln.aend #-aln.pos,aln.tlen,slen[aln.tid],slen[aln.tid]-aln.pos,aln.pos
                    is_reverse[aln.qname]=aln.is_reverse
#                    print aln.qname
            print("#",region,len(list(templates.keys())))
            if scaffold==last_scaffold:
                o1=0 if last_end[-1:]=="3" else 1
                o2=0 if end[-1:]     =="5" else 1
                n_spanning_shotgun_templates=0
                links=[]
                orientations={}
                n_templates=0
                read_data=[]
                for t in list(templates.keys()):
                    if t in last_templates:
#                        print t
#                        print "{}:{}".format(sequences.get((t,True)) ,qualities.get((t,True)) )
#                        print "{}:{}".format(sequences.get((t,False)),qualities.get((t,False)))

#                        print "{}:{}".format(last_sequences.get((t,True)) ,last_qualities.get((t,True)) )
#                        print "{}:{}".format(last_sequences.get((t,False)),last_qualities.get((t,False)))
#                        print ""
                        n_templates+=1
                        s1=1 if last_reverse[t] else 0
                        s2=0 if is_reverse[t] else 1
                        orientations[s1,s2]=orientations.get((s1,s2),0)+1
                        if (s1,s2)==(o1,o2):
                            links.append( [float(last_templates[t])-ctrim,float(templates[t])-ctrim] )
                            seq1=last_sequences[t] if o1==0 else rc(last_sequences[t])
                            qal1=last_qualities[t] if o1==0 else last_qualities[t][::-1]
                            read_data.append((seq1,qal1))
#                            read_data.append("{}:{}".format(seq1,qal1))
                            seq2=     sequences[t] if o2==0 else rc(sequences[t])
                            qal2=     qualities[t] if o2==0 else    qualities[t][::-1]
#                            read_data.append("{}:{}".format(seq2,qal2))
                            read_data.append((seq2,qal2))
                print("rd:")
                print(read_data)
                print("")
                l1,l2=last_length-2*ctrim,length-2*ctrim
                ds =  [ ces.fragment_size(l1,l2,o1,o2,links[i],0) for i in range(len(links))]
                md=old_div(float(sum(ds)),len(ds)) if len(ds)>0 else 0
                n=len(links)
                bestgap=-300
                if len(links)>0:
                    bestll=-1e10
                    lastll=bestll
                    for gap in range(0,1200,10):

                        ll = ces.model.lnL(l1,l2,o1,o2,gap,links)
                        ddg_ll = ces.model.ddg_lnL(l1,l2,o1,o2,gap,links)
                        Npp= ces.model.N*ces.model.p_prime(l1,l2,gap)
                        d2dg2_ll = ces.model.d2dg2_lnL(l1,l2,o1,o2,gap,links)
                        if ll>bestll: 
                            bestll=ll
                            bestgap=gap
#                        if (bestll-ll)>1.0 and gap>0: break

                              #         3  4  5     6     7        8       9 10 11   12
                        print("xy:",gn,gap,ll,ddg_ll,d2dg2_ll,Npp) #,p0s,ll-p0s,ddg_llr,d2dg2_llr,z,w,logpl,ww
                        lastll=ll

                    print("xy:")
                    print("xy:")
                ml_gap = ces.ml_gap(l1,l2,o1,o2,-1,-1,links,-1,10) if len(links)>0 else (-1,0)
                true_gap = blast_coord-last_blast_coord if last_blast_chr==blast_chr else -1
                nlinks=len(links)
                print("\t".join(map(str,["gap:",gn,ml_gap[0],bestgap,true_gap,unc+last_unc,nlinks,md,last_contig,contig , last_end,end,(o1,o2),last_length,length,ocontig,base,ocontig,n_spanning_shotgun_templates,md,links,orientations,ds])))
                gn+=1
 
        last_scaffold=scaffold
        last_end=end
        last_contig=contig
        last_length=length
        last_ocontig=ocontig
        last_base=base
        last_templates=templates
        last_reverse=is_reverse
        last_blast_coord=blast_coord
        last_blast_chr=blast_chr
        last_unc=unc
        last_sequences = sequences
        last_qualities = qualities
