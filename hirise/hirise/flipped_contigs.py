#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

i=0
counts={}
saved={}
ll={}
while True:
     l=sys.stdin.readline()
     if not l: break
     c = l.strip().split()
     if not c[0]=="p:": continue
     i+=1
     i=i%2
     mb=eval(" ".join(c[8:]))
     if not mb:
          st=0
     else:
          st=mb[2]
     #print c[:8],mb[2]
     sc,end,x,ch=c[1],c[2],int(c[3]),c[4]
     e=end[-1]
     print(i,sc,end,x,st,ch)
     if not (sc,ch) in counts:
          counts[sc,ch]={}
     if i==0:
          saved[end[:-2]]=(sc,ch,e,st)
          counts[sc,ch][e,st] = counts[sc,ch].get((e,st),0) + 1
     ll[end[:-2]]=int(c[7])

for f in list(saved.keys()):
     sc,ch,e,st = saved[f]
     x=list(counts[sc,ch].keys())
     x.sort(key=lambda z: counts[sc,ch][z],reverse=True)
     y=x[:2]
     n=sum([ counts[sc,ch][i] for i in x ])
     print("\t".join(map(str,["x",n,(e,st) in y,ll[f], f, saved[f],counts[sc,ch][e,st], x,counts[sc,ch], (e,st) in y])))
