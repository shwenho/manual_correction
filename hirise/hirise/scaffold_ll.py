#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import range
#!/usr/bin/env python3
import sys
import networkx as nx
#import greedy_chicagoan2 as gs
#import 
import glob
from random import shuffle
import hirise.chicago_edge_scores as ces
import math


GenomeSize = 3.0e9
pn = 0.5
N=1

def update_end_distance(end_distance,n,g):
    x=0
    q=[n]
    seen={}
    last=False
    while len(q)>0:
        m=q.pop(0)
        if last:
            x+=g[last][m]['length']
        last=m
        if (not m in end_distance) or end_distance[m]>x: end_distance[m]=x
        
        seen[m]=True
        for l in g.neighbors(m):
            if not l in seen:
                q.append(l)

#    print end_distance

#def llr(e1,e2):
#    return 1.0

L=200000.0

def test_interc(c1,c2,join_options,graph,ends,linked,slen,max_interc_len):
    os1=oscaffold.get(c1,-1)
    #os1_mine = (args.fromS <= os1) and (os1 < args.endS)
    #if not os1_mine: return([-100])
    #print "test interc:",c1,c2,oscaffold.get(c1,0),oscaffold.get(c2,0) ,ends[scaffold[c1]],ends[scaffold[c2]]#,c2_end in linked, c1_end in linked #,linked.get(c1),linked.get(c2),len(linked.keys())
    print("test interc:", c1,c2,scaffold[c1],scaffold[c2],ends[scaffold[c1]],ends[scaffold[c2]])
    if slen[scaffold[c1]] < max_interc_len: 
        for free_end in ends[scaffold[c1]]:
            for c2_end in [c2+".5",c2+".3"]:
#                print "c2_end in linked:",c2_end,c2_end in linked
                if c2_end in linked:
                    print(1,free_end,c2_end)
                    try:
                        gs.test_interc_option( c2_end, linked[c2_end], free_end, join_options, graph )
                    except Exception as e:
                        print("c2_end=",c2_end,"--",linked[c2_end],free_end)
                        print(e)
                        raise Exception('too connected 1')
    if slen[scaffold[c2]] < max_interc_len:
        for free_end in ends[scaffold[c2]]:
            for c1_end in [c1+".5",c1+".3"]:
#                print "c1_end in linked:",c1_end,c1_end in linked
                if c1_end in linked:
                    print(2,free_end,c1_end)
                    try:
                        gs.test_interc_option( c1_end, linked[c1_end], free_end, join_options, graph )
                    except Exception as e:
                        print("c1_end=",c1_end,"--",linked[c1_end],free_end)
                        print(e)
                        raise Exception('too connected 2')

    if len(join_options)>0:
        join_options.sort(reverse=True)
        print("join options:",join_options)
        return(join_options[0])
    return([-100.0])


def get_score(c1_stranded,c2_stranded,gaplen=-1,cache={}):
    if gaplen<0.0: print(math.log(-1))
#    if cache.has_key((c1_stranded,c2_stranded,gaplen)):
#        return cache[c1_stranded,c2_stranded,gaplen]
#    if cache.has_key((c2_stranded,c1_stranded,gaplen)):
#        return cache[c2_stranded,c1_stranded,gaplen]

#    if (o1,o2) == (0,0):     #         -------1----->    ----2------>
#    if (o1,o2) == (0,1):     #         -------1----->   <---2-------
#    if (o1,o2) == (1,0):     #        <-------1-----     ----2------>
#    if (o1,o2) == (1,1):     #        <-------1-----    <----2------

    o1,o2=0,0
    if c1_stranded[-2:]==".5": o1=1
    if c2_stranded[-2:]==".3": o2=1
    c1=c1_stranded[:-2]
    c2=c2_stranded[:-2]

    if not (c1,c2) in links: return 0.0

    l1=ll[c1]
    l2=ll[c2]
    p0 = ces.p_not_a_hit(l1,l2,GenomeSize,gaplen,pn)
    if p0<0.0:
        print("wtf?  p0<0",p0,l1,l2,GenomeSize,gaplen,pn)
#    print c1_stranded,c2_stranded,links.get((c1,c2))
    thisscore=ces.llr_v0(l1,l2,o1,o2,GenomeSize,pn,links.get((c1,c2),[]),N,gaplen,p0 )
    #cache[c1_stranded,c2_stranded,gaplen]=thisscore
    return thisscore


if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-L','--links')
    parser.add_argument('-a','--chunk1',type=int,default=0)
#    parser.add_argument('-b','--chunk2',type=int)
    parser.add_argument('-1','--first')
    parser.add_argument('-2','--second')
    parser.add_argument('-s','--scaffolds')
    parser.add_argument('-S','--alreadyDone')
    #parser.add_argument('-b','--besthits')
    parser.add_argument('-l','--max_interc_len',default=20000,type=int)
    parser.add_argument('-w',default=False,type=int)
    parser.add_argument('-E','--edgefile')
    parser.add_argument('--test_intercs',default=False,action="store_true")
    parser.add_argument('-F','--filter')
    parser.add_argument('-K','--endS',default=100000,type=int)
    parser.add_argument('-k','--fromS' ,default=0,type=int)
    parser.add_argument('-N','--nchunks' ,default=32,type=int)
    parser.add_argument('-t','--threshold' ,default=0.10,type=float)
    parser.add_argument('-Z','--chunkfile')
    parser.add_argument('-M','--set_insert_size_dist_fit_params',default="3.85301461797326,1.42596694138494,1.38674994280385e-05,10940.8191219759,49855.7525034142,0.3,420110993")
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()

    if args.seed != -1 : 
      random.seed(args.seed)
    if args.debug:
        args.progress=True

    max_interc_len=args.max_interc_len

    if args.progress: print("#", str(args)) 

    print("#"+str(args))
    fit_params={}
    try:
        fit_params = eval(args.set_insert_size_dist_fit_params )
    except Exception as e:
        f=open( args.set_insert_size_dist_fit_params )
        contents = f.read()
        try:
            fit_params=eval(contents)
        except:
            "couldn't deal with option", args.param
        f.close
        GenomeSize = fit_params['G']
        pn  = fit_params['pn']
        N  = fit_params['N']
        ces.set_exp_insert_size_dist_fit_params(fit_params)

    if args.scaffolds:
        my_commandline_scaffolds = args.scaffolds.split(",")
    else: 
        my_commandline_scaffolds = []
    print("#",my_commandline_scaffolds)
    ll={}

    oscaffold={}
    #oslen={}
    slen={}
    g=nx.Graph()
    linked={}

    scaffold_hashes={}
    my_contigs={}
    my_scaffolds={}
    contigs={}
    scaffold={}
    import hashlib
    import struct

    while True:
        l=sys.stdin.readline()
        if not l: break
        c=l.strip().split()
        if c[0]=="#edge:":
            ht=eval(" ".join(c[3:]))
            g.add_edge(c[1],c[2],ht)
            if not ht['contig']:
                linked[c[1]]=c[2]
                linked[c[2]]=c[1]
            scaffold[c[1]]=scaffold[c[1][:-2]]
            scaffold[c[2]]=scaffold[c[2][:-2]]
            slen[scaffold[c[1]]]=slen.get(scaffold[c[1]],0)+ht['length']
            if ht['contig']: ll[c[1][:-2]]=ht['length']
        if c[0]=="scaffold:":
            contig,cscaffold=c[1],c[2]
            contigs[cscaffold] =contigs.get(cscaffold,[])+[contig]
            scaffold[contig]=cscaffold
            if cscaffold in my_commandline_scaffolds:
                my_scaffolds[cscaffold]=1
            if not cscaffold in scaffold_hashes:
                h=struct.unpack("<L", hashlib.md5(cscaffold.encode("utf-8")).digest()[:4])[0]
                scaffold_hashes[cscaffold]=(h%args.nchunks)
#                print "scaffold_hash",c[1],cscaffold ,  scaffold_hashes[cscaffold] , scaffold_hashes[cscaffold] in (args.chunk1, ),(args.chunk1, )
                #print "scaffold_hash",cscaffold,scaffold_hashes[cscaffold],args.chunk1,args.chunk2
                if scaffold_hashes[cscaffold] in (args.chunk1,):
                    my_scaffolds[cscaffold]=1
            if scaffold_hashes[cscaffold] in (args.chunk1, ) or cscaffold in my_commandline_scaffolds:
                my_contigs[c[1]]=1
               # print "my contig",c[1],cscaffold
            

    linkfiles = list(glob.glob(args.links))
    shuffle(linkfiles)
    #print linkfiles

    links={}
    links_interc={}
    nlinks=0
    scaffold_pair_links={}
    pairs_to_test={}
    inter_chunk_pairs={}
    inter_chunk_scaffolds={}
    for lfilename in linkfiles:
        print("#",lfilename)
        f = open(lfilename,"rt")
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            c=l.strip().split()
            c1,c2=c[0],c[1]
            if not (c1 in my_contigs and c2 in my_contigs) : continue
            if not c1 in scaffold: continue
            if not c2 in scaffold: continue
            s1,s2=scaffold[c1],scaffold[c2]
            if not s1==s2: continue
            links[c1,c2]=eval(" ".join(c[5:]))
            nlinks+=1
            if nlinks%1000==1: print("#nlinks=",nlinks,c1,c2,scaffold[c1],scaffold[c2])
            if not c1 in ll: ll[c1]=int(c[2])
            if not c2 in ll: ll[c2]=int(c[3])
            sp=tuple(sorted([scaffold[c1],scaffold[c2]]))
        f.close()
    end_distance={}
    ends={}

    for n in g.nodes():
        if g.degree(n)==1:# and n in oscaffold and oscaffold[n]<args.endS:
#            update_end_distance(end_distance,n,g)
            ends[scaffold[n]] = ends.get(scaffold[n],[])+[n]
#x            print n,scaffold[n],ends[scaffold[n]]

#    print "#done setting edge distances"
    sys.stdout.flush()

    max_interc_len = 20000
    gaps_list=(1000,)
    scaffold_pairs_tested={}
    link_scores={}
    #gs.ll=ll
    #gs.links=links
#    pairs_to_test=inter_chunk_pairs.keys()
#    if args.first:
#        pairs_to_test = [(args.first,args.second)]
#    for c1,c2 in links.keys():
#    for c1,c2 in [("Scaffold68143_1","Scaffold42944_1"),("Scaffold232867_1","Scaffold519668_1"),("Scaffold82730_1","Scaffold59156_1")]:
    for s in list(my_scaffolds.keys()):
        if not s in contigs: continue

        if contigs[s][0]+".5" in ends[s]:
            update_end_distance(end_distance,contigs[s][0]+".5",g) 
        elif contigs[s][0]+".3" in ends[s]:
            update_end_distance(end_distance,contigs[s][0]+".3",g) 
        else:
            print(s,contigs[s][0],ends[s])
            print(math.log(-2))
        contigs[s].sort(key=lambda x: end_distance[x+".5"] )

        nc = len(contigs.get(s,[]))
        for i in range(nc):
            c1 = contigs[s][i]
#            print "#y",c1,end_distance[c1+".5"],end_distance[c1+".3"],end_distance[c1+".5"]<end_distance[c1+".3"]

        scores={}

        for i in range(nc):
            c1 = contigs[s][i]
            for j in range(i+1,nc):
                c2 = contigs[s][j]

                gap = min( [abs(end_distance[c1+z]-end_distance[c2+w])  for z in (".5", ".3") for w in (".5",".3")  ] )

                for e1 in (".5",".3"):
                    for e2 in (".5",".3"):
                        scores[c1+e1,c2+e2]=get_score( c1+e1, c2+e2, gap )
                        scores[c2+e2,c1+e1]=scores[c1+e1,c2+e2]
#                        print "#x:",c1,c2,e1,e2,scores[c1+e1,c2+e2],gap
#def get_score(c1_stranded,c2_stranded,gaplen=default_gapsize,cache={}):

        ts=0.0
        for i in range(nc):
            ci = contigs[s][i]
#            print i,ci,end_distance[ ci+".5"], end_distance[ ci+".3" ]
            if end_distance[ ci+".5"] < end_distance[ ci+".3" ]:
                strandi='+'
                left_end  = ci+".5"
                right_end = ci+".3"
            else:
                strandi='-'
                left_end  = ci+".3"
                right_end = ci+".5"

            sP = 0.0
            sM = 0.0

            sPa= 0.0
            sMa= 0.0
            for j in range(i):
                if args.w and abs(i-j)>args.w: continue
                cj = contigs[s][j]
                if end_distance[ cj+".3"] < end_distance[ cj+".5" ]:
                    sP += scores[ cj+".5", left_end  ]
                    sM += scores[ cj+".5", right_end ]
                else: 
                    sP += scores[ cj+".3", left_end  ]
                    sM += scores[ cj+".3", right_end ]

            for j in range(i+1,nc):
                if args.w and abs(i-j)>args.w: continue
                cj = contigs[s][j]
                if end_distance[ cj+".3"] > end_distance[ cj+".5" ]:
                    pass
                    sPa += scores[ cj+".5", right_end ]
                    sMa += scores[ cj+".5", left_end  ]
                else:
                    pass
                    sPa += scores[ cj+".3", right_end ]
                    sMa += scores[ cj+".3", left_end  ]
            ts+=sP+sPa
#            print "\t".join(map(str,[s,len(contigs[s]),ll[ci],sP,sM,sP-sM,sPa-sMa, end_distance[ ci+".5"] < end_distance[ ci+".3" ], end_distance[ ci+".5"], end_distance[ ci+".3" ],ci]))
        print(s, ts)





