HiRise Quickstart Guide
=======================

Make sure the dovetail/hirise directory is in your PATH.

In the directory that will contain the assembly, create a config.json file.  (There is a template in dovetail/hirise/Sample_HiRise_config.json.)

Run the assembler:  snakemake --snakefile dovetail/hirise/Snakefile -j 32 -p hirise_iter_3.fasta

