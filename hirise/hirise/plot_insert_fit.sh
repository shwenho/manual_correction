#!/bin/bash

inserts=$1
smooth=$2
corrected=$3
datamodel=$4
outfile=$5
fn=`eval cat datamodel.out | python -c 'import sys;[sys.stdout.write(eval(x)["fn"]) for x in sys.stdin.readlines()]'`

echo "set terminal pdfcairo color
set term pdfcairo font \"Arial,8\"
set output '$outfile'
set log xy
plot '$inserts' u 1:2 w l t 'Raw pair separation frequency', '$smooth' u 1:2 w l lt 4 t 'Smoothed frequency', '$corrected' u 1:2 w l lt 3 t 'Smoothed, contiguity-corrected frequency', $fn lw 3 lt 0 t 'Model fit'" | gnuplot

#plot '$1' u 1:2:(\$1<\$2 ? \$5 : 1/0 )  w p palette pt 5 ps 0.05, '' u 1:2:( \$1<\$2 ? 1/0 : \$3 ) w p palette2 pt 5 ps 0.05 , x+40000 lt 0, x-40000 lt 0" | gnuplot
#plot '< cat $1 | grep xy | sort -k3n' u 1:2:5 w p palette pt 5 ps 0.01, x+40000 lt 0, x-40000 lt 0" | gnuplot

