#!/usr/bin/env python3


import argh
import sys
import numpy
import matplotlib as mpl
mpl.rcParams["backend"] = "Agg"
import matplotlib.pyplot as plt
import os
import hashlib
from matplotlib.ticker import FuncFormatter

plot_offset = 0

@argh.arg('-v','--vs',action='append',type=int)
def main(coords="/dev/stdin", show=False, save=None, title=None, ms=10,offset=0,vs=None):
    global plot_offset
    plot_offset=offset
    x1, x2, contigs, contig_names, curve, clipped_curve, breaks = get_coordinates(coords)
    chicago_plot(x1, x2, contigs, contig_names, curve, clipped_curve, breaks,show, save, title=title, ms=ms,offset=offset,vs=vs)
    
    
def get_coordinates(coords):
    x1 = []
    x2 = []
    contigs = []
    contig_names = []
    curve = []
    clipped_curve = []
    breaks = []
    with open(coords) as coords_file:
        for line in coords_file:
            if line.startswith("x:"):

                s = line.split()
                left = int(s[1])
                right = int(s[2])
                x1.append(left)
                x2.append(right)
            elif line.startswith("c:"):
                s = line.split()
                contig_left = int(s[4])
                contig_right = int(s[5])
                name = s[1]
                contigs.append([contig_left, contig_right])
                contig_names.append(name)
            elif line.startswith("score:"):
                s = line.strip().split()
                x = int(s[1])
                score = float(s[2])
                curve.append((x,score))
            elif line.startswith("clipped_score:"):
                s = line.strip().split()
                x = int(s[1])
                score = float(s[2])
                clipped_curve.append((x,score))
            elif line.startswith("break:"):
                s = line.strip().split()
                scaf = s[1]
                pos = int(s[2])
                breaks.append((scaf,pos))
    return x1, x2, contigs, contig_names, curve, clipped_curve,breaks

def color_map(s):

    #    array = os.urandom(1 << 20)
    md5 = hashlib.md5()
    md5.update(s.encode())
    digest = md5.hexdigest()
    seed = int(digest, 16) % 4294967295


    numpy.random.seed(seed)
    #    numpy.random.seed(seed)
    return numpy.random.rand(3,1)



def chicago_plot(x1, x2, contigs, contig_names, curve, clipped_curve, breaks, show=False, save=None, log=sys.stderr, title=None, ms=10,offset=None,vs=None):

    rotated_x, rotated_y = rotate_coords(x1, x2)
    print("drawing figure", file=log)
    fig = plt.figure(figsize=(15,10))
    ax = fig.add_axes([0.1,0.1,0.65,0.8])
    
    ax.plot(rotated_x, rotated_y, "o", label="read pairs",ms=2)
    print("using title ", title)
    if title:
        plt.title(title, fontsize=fs)
    locs,labels = plt.yticks()
    ystep = locs[1] - locs[0]
    contig_offset = -ystep/5
    for contig, contig_name in zip(contigs, contig_names):
        print(contig, contig_name)
        ax.plot(contig, [contig_offset, contig_offset], linewidth=10, label=contig_name,color=color_map(contig_name))
    xmin,xmax = plt.xlim()
    ymin,ymax = plt.ylim()

    llr_y = ax.twinx()
    if curve:
        cx = [0] + [p[0] for p in curve] + [contigs[-1][1]]
        cy = [0] + [p[1] for p in curve] + [curve[-1][1]]
        llr_y.step( cx,cy , label="support" )
    if clipped_curve:
        cx = [0] + [p[0] for p in clipped_curve] + [contigs[-1][1]]
        cy = [0] + [p[1] for p in clipped_curve] + [clipped_curve[-1][1]]
        llr_y.step( cx,cy , label="clipped support" ,c="red")

    if vs:
        for v in vs:
            if offset: v -= offset
            left_arm_x = [v,xmin]
            left_arm_y = [0,2*(v-xmin)]
            right_arm_x = [v,xmax]
            right_arm_y = [0,2*(xmax-v)]
            ax.plot(left_arm_x,left_arm_y,c='black',ls='dashed',lw=1)
            ax.plot(right_arm_x,right_arm_y,c='black',ls='dashed',lw=1)
    if breaks:
        for scaf,pos in breaks:
            if offset: pos -= offset
            x = [pos,pos]
            y = [0,ymax]
            ax.plot(x,y,c='k',ls='dashed',lw=1)

    llr_y.set_ylabel("LLR support")
    h1, l1 = llr_y.get_legend_handles_labels()
    h2, l2 = ax.get_legend_handles_labels()
    lgd = ax.legend(h1+h2,l1+l2,loc='center left', bbox_transform=fig.transFigure,bbox_to_anchor=(0.8, 0.5),fontsize=6,borderpad=1)
    ax.set_xlabel("positions")
    ax.set_ylabel("pair distance")
    bp_fmt = FuncFormatter(bpFormatter)
    ax.xaxis.set_major_formatter(bp_fmt)
    ax.set_ylim((contig_offset,ymax))
    if show:
        print("showing figure", file=log)
        plt.show(bbox_extra_artists=(lgd,), bbox_inches='tight')
    if save:
        print("saving figure", file=log)
        plt.savefig(save, dpi=100,bbox_extra_artists=(lgd,),bbox_inches='tight') 


def rotate_coords(x1, x2):
    # midpoints of x1 and x2
    rotated_x = [(left + right) / 2 for left, right in zip(x1, x2)]
    rotated_y = [right - left for left, right in zip(x1, x2)]
    return rotated_x, rotated_y
    

def bpFormatter(x,pos):
    global plot_offset
    x += plot_offset
    if x > 1e9:
        return "%0.2f Gbp" %(x/1e9)
    if x > 1e6:
        return "%0.2f Mbp" %(x/1e6)
    if x > 1e3:
        return "%0.2f kbp" %(x/1e3)
    if x < 0:
        return "%d" % (x)
    else:
        return "%d bp" % (x)

if __name__ == "__main__":
    argh.dispatch_command(main)
