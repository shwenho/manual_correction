#!/usr/bin/env python3
from __future__ import print_function
from builtins import range
#!/usr/bin/env python3


def least_full(fills):
    m=fills[0]
    j=0
    for i in range(len(fills)):
        if fills[i]<m:
            m=fills[i]
            j=i
    return j

def seqs(f,n_parts,my_part):

    fills=[0]*n_parts

    name=False
    sbuff=""
    lbuff=[]
    lenacc=0
    while True:
        l=f.readline()
        if not l: break
        if l[0]==">":
            if name:
                j = least_full(fills)
                fills[ j ] += lenacc
                if j==my_part:
                    yield {'name':name, 'seq':"".join(lbuff) }
            lbuff=[]
            name=l[1:].strip()
            lenacc=0
        else:
            lbuff.append(l.strip())
            lenacc+=len(l)-1

if __name__=="__main__":
    import sys
    fn = sys.argv[1]
    n_parts = int(sys.argv[2])
    my_part = int(sys.argv[3])
    
    f = open(fn)
    x=0
    for s in seqs(f,n_parts,my_part):
        x+=len(s['seq'])
        print(s['name'], len(s['seq']),x)
