#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import pysam

if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
     parser.add_argument('-b','--bamfile',default=False,help="Bam file.")
     parser.add_argument('-B','--bedfile',default=False,help="Bed file.")
     parser.add_argument('-q','--mapq',default=10,type=int,help="Mapq.")
     parser.add_argument('-1','--t1',required=True,type=float)
     parser.add_argument('-2','--t2',required=True,type=float)
     parser.add_argument('-o','--outhist')

     args = parser.parse_args()
     if args.progress: print("#",args)

     mycontigs=[]
     for l in open(args.bedfile):
          c=l.strip().split()
          mycontigs.append(c[0])

     bamfile=pysam.Samfile(args.bamfile)
     
#     print(help(bamfile))
     mycontigs.sort( key=lambda x: bamfile.gettid(x) )

#     print("#")
     out=0
     in1=1
     in2=2

     state=out

     h={}

     last_scaffold="-"
     last_x="-"
     start_x=0

     t1,t2 = args.t1,args.t2

     for c in mycontigs:
          for p in bamfile.pileup(reference=c):

               scaffold,x,y = c,p.pos,p.n
               y = len( [ s for s in p.pileups if s.alignment.mapq > args.mapq  ] )

               if args.outhist: h[y]=h.get(y,0)+1

               if scaffold==last_scaffold:
                    if state == in2 and y < t1:
                         print(scaffold,start_x,x)
                         state=out
                         continue
               else: 
                    if state==in2:
                         print(last_scaffold,start_x,last_x, "#toend")
                    state=out
                    last_scaffold=scaffold
                    last_x=x
                    continue

               if state==out and y >= t1: 
                    state=in1
                    start_x=x

               if y >= t2: 
                    state=in2


               if y< t1: state=out
               last_scaffold=scaffold
               last_x=x



if args.outhist:
     fh=open(args.outhist,"wt")
     depths=list(h.keys())
     depths.sort()
     for d in depths:
          fh.write("{}\t{}\n".format(d,h[d]))
     fh.close()




