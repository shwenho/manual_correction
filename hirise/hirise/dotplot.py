#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from past.utils import old_div
import sys
import argparse

parser = argparse.ArgumentParser(description="""
This script is designed to make dot plots.  It outputs either gnuplot or asymptote code.
It reads rows from STDIN in with the following columns:
1.  Marker ID (could be a kmer or gene)
2.  Contig in genome/assembly 1
3.  Position in contig1
4.  Strand in contig 1  ("+" or "-")
5.  Contig in genome/assembly 2
6.  Position in contig2
7.  Strand in contig 2 ("+" or "-")

""")

#parser.add_argument('-i','--input')
parser.add_argument('-F','--flip',default=False,action="store_true")
parser.add_argument('-P','--pdf',default=False)
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-J','--joinlist',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-g','--gp',default=False,action="store_true",help="Write a gnuplot command file")
parser.add_argument('-y','--ylabels',default=False,action="store_true",help="Add labels for hirise scaffolds, can be very messy.")
#parser.add_argument('-L','--length',default=False)
#parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-L','--lengths',action="append",default=[], help ="File names containing the lengths of the contigs.  Can be specified multiple times (i.e. one per genome)")
parser.add_argument('-o','--outfile',default="-")
parser.add_argument('--chr',default=False)
parser.add_argument('--xlabelprefix',default="")
parser.add_argument('-H','--head',default=False,type=int)
parser.add_argument('-m','--minlen',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

if args.outfile=="-":
     ofh = sys.stdout
else:
     ofh = open(args.outfile,"w")

ll={}
for lf in args.lengths:
     print(lf)
     f=open(lf)
     while True:
          l=f.readline()
          if not l: break
          c=l.strip().split()
          ll[c[1]]=float(c[0])



offset1=0
offset2=0
bumper1=0.050e6
bumper2=bumper1
last_c1=False
last_c2=False

offsets={}

hrules=[]
vrules=[]

max1=0
max2=0
sf=1.0e6
penmap={("+","+"):"pen1",
        ("+","-"):"pen2",
        ("-","-"):"pen3",
        ("-","+"):"pen4"        
}

if not args.gp:
     ofh.write("pen rulepen=linewidth(0.1*linewidth());\n")
     ofh.write("pen pen1=red;\n")
     ofh.write("pen pen2=blue;\n")
     ofh.write("pen pen3=orange;\n")
     ofh.write("pen pen4=purple;\n")
     ofh.write("var t=rotate(0);\n")
else:
     pass


dots=[]

nc=0
pairs = []
c1s={}
c2s={}
while True:
     l=sys.stdin.readline()
     if not l: break
     kmer,c1,x1,s1,c2,x2,s2=l.strip().split()
     #if args.chr and not c1 ==args.chr: continue
     if args.minlen and (ll[c1]<args.minlen or ll[c2]<args.minlen): continue
     dots.append( ( c1,float(x1),c2,float(x2) ) )
     if args.debug: print("a",dots[-1])
     c1s[c1]=1
     c2s[c2]=1

c1list = list(c1s.keys())
c1list.sort(key=lambda x: ll[x],reverse=True)
c2list = list(c2s.keys())
c2list.sort(key=lambda x: ll[x],reverse=True)

offsets1={}
offsets2={}
rs=0.0
rs2=0.0
xlabels=[]
ylabels=[]
for c1 in c1list:
     if args.chr and not args.chr==c1: continue
     offsets1[c1]=rs2
     rs+=ll[c1]
     xlabels.append((c1,rs2+old_div(ll[c1],2)))
     rs2+=ll[c1]
     vrules.append(rs2)
     rs2+=bumper1
     vrules.append(rs2)

c1c2hits={}
c1c2x={}
c1c2y={}
for c1,x1,c2,x2 in dots:
     c1c2hits[c2]= c1c2hits.get(c2,{}) #.get(c1,0)+1
     c1c2x[c2]   = c1c2x.get(c2,{})    #.get(c1,0)+1
     c1c2y[c2]   = c1c2y.get(c2,{})    #.get(c1,0)+1
     c1c2hits[c2][c1] = c1c2hits[c2].get(c1,0)+1
     c1c2x[c2][c1] = c1c2x[c2].get(c1,0)+x1
     c1c2y[c2][c1] = c1c2y[c2].get(c1,0)+x2
     if args.debug: print("x",c1,c2,x1,x2)

c2bestc1={}
c1lt = list(c1list)
for c2 in c2list:
     hits=c1c2hits.get(c2,{})
     c1lt.sort( key=lambda x: hits.get(x,0) ,reverse=True)
     c2bestc1[c2]=c1lt[0]
c2bestx={}
c2besty={}
for c2 in c2list:
     nx = c1c2hits[c2].get(c2bestc1[c2],0)

     sx = c1c2x[c2].get(c2bestc1[c2],0)
     sy = c1c2y[c2].get(c2bestc1[c2],0)
     if nx>0:
          c2bestx[c2]=sx/nx
          c2besty[c2]=sy/nx
     else:
          nx=0
     if args.debug: print("w",c2,sx,nx,c2bestx[c2])

a={}
b={}
flipped={}
for c1,x1,c2,x2 in dots:
     if c1==c2bestc1[c2]:
          if args.debug:  print("z",c1,c2,c2bestx[c2],x1-c2bestx[c2],x2-c2besty[c2],x1,x2)#, b.get(c2),a.get(c2)/b.get(c2))
          if not 0.0==(x1-c2bestx[c2]):
               a[c2] = a.get(c2,0.0) + (x2-c2besty[c2])/(x1-c2bestx[c2])
               b[c2] = b.get(c2,0.0) + 1

for c2 in c2list:
     if args.flip:
          if (a.get(c2,0)/b.get(c2,1)) > 0.0 : flipped[c2]=True 
     else:
          if (a.get(c2,0)/b.get(c2,1)) < 0.0 : flipped[c2]=True
     if args.debug:  print("b  ",c2,c2bestc1[c2],c2bestx[c2],a.get(c2),b.get(c2),(a.get(c2,0)/b.get(c2,1)))#, b.get(c2),a.get(c2)/b.get(c2))

c2list.sort(key=lambda x: ( offsets1.get(c2bestc1[x],0),c2bestx[x] ) )

if args.joinlist:
     lastc2=-1
     lastc1=-1
     for c2 in c2list:
          c1=c2bestc1[c2]
          if (not lastc2==-1) and c1==lastc1:
               print(lastc2,c2,flipped.get(lastc2,False),flipped.get(c2,False),lastc1,c1,sep="\t")
          lastc2=c2
          lastc1=c1

rs=0.0
rs2=0.0
hrules.append(rs2)
for c2 in c2list:
     if args.chr and not args.chr == c2bestc1.get(c2): continue
     offsets2[c2]=rs2
     rs+=ll[c2]
     ylabels.append((c2,rs2+ll[c2]/2))
     rs2+=ll[c2]
     hrules.append(rs2)
     rs2+=bumper2
     hrules.append(rs2)

#for c2 in c2list:
#     print("\t".join(map(str,(c2,c2bestc1[c2],c2bestx[c2],offsets2[c2]))))


for c1,x1,c2,x2 in dots:
     if args.head and nc>args.head: break
     if args.chr and not args.chr == c1: continue
     if args.chr and not args.chr ==  c2bestc1.get(c2): continue
     a=float(x1)+offsets1[c1]
     if c2 in flipped:
          b=ll[c2]-float(x2)+offsets2[c2]
     else:
          b=float(x2)+offsets2[c2]


     if a>max1: max1=a
     if b>max2: max2=b


     if args.gp:
          pairs.append((a/sf,b/sf,c1,c2))
#          ofh.write(("{}\t{}\n".format( a/sf , b/sf )))          
     else:
          ofh.write(("dot(t*({},{}),{});\n".format( old_div(a,sf) , old_div(b,sf), penmap[s1,s2] )))

     last_c1 = c1
     last_c2 = c2

if not args.gp:
     for vr in vrules:
          ofh.write("draw(t*(({},{})--({},{})),rulepen);\n".format(old_div(vr,sf),0.0,old_div(vr,sf),old_div(max2,sf)))
     for hr in hrules:
          ofh.write("draw(t*(({},{})--({},{})),rulepen);\n".format(0.0,old_div(hr,sf),old_div(max1,sf),old_div(hr,sf)))
else:

#     print(vrules)
#     print(hrules)
#set style line 1 lt 0 lw 3
#set style arrow 1 nohead ls 1

#     ofh.write("set style line 3 lt -1 lw 0.5\n")

     if args.pdf: 
          ofh.write("set term 'pdf'\n") 
          ofh.write("set output '{}'\n".format(args.pdf)) 

     if args.chr:
          ofh.write("set xlabel 'Position in {} {} (Mb)'\n".format(args.xlabelprefix,args.chr)) # the vertical lines
     else:
          ofh.write("set xlabel 'Reference genome position (Mb)'\n") # the vertical lines

     ofh.write("set ylabel 'HiRise Scaffold position (Mb)'\n") # the vertical lines

     ofh.write("set style arrow 2 nohead lw 0.1 lc rgb 'black'\n") # the vertical lines
     ofh.write("set style arrow 3 nohead lw 0.05 lc rgb 'black'\n") # the horizontal lines

#     ofh.write("unset xtics\n") 
     ofh.write("set size ratio -1\n")
     ofh.write("set xrange [0:%d]\n" % (max1/sf)) 
     ofh.write("set yrange [0:%d]\n" % (max2/sf)) 

     nn=0
     spacing = max2/(sf*30)
     oh = max2/(sf*100)
     if not args.chr:
          for xl in xlabels:
               ofh.write("set label \"{}{}\" at {},{} font ',8' front\n".format(args.xlabelprefix,xl[0],old_div(xl[1],sf),old_div(max2,sf)+oh+nn*spacing))
               #nn+=1
               #nn=nn%2

     nn=0
     if args.ylabels:
          for yl in ylabels:
               ofh.write("set label \"{}\" at {},{} font ',8' front\n".format(yl[0],max1/sf,yl[1]/sf))
               nn+=1
               nn=nn%2

     if not args.chr:
          for vr in vrules:
               ofh.write("set arrow from {},{} to {},{} as 2\n".format(old_div(vr,sf),0.0,old_div(vr,sf),old_div(max2,sf)))
     for hr in hrules:
          ofh.write("set arrow from {},{} to {},{} as 3\n".format(0.0,hr/sf,max1/sf,hr/sf))

     if args.pdf:
          ofh.write("plot '-' using 1:2 ps 0.05 pt 1 t ''\n")
     else:
          ofh.write("plot '-' using 1:2 ps 0.05 pt 5 t ''\n")
     for a,b,c1,c2 in pairs:
          if args.chr and not c1==args.chr: continue
          ofh.write("{}\t{}\t{}\t{}\tdata\n".format(a,b,c1,c2))
     ofh.write("e\n\n")

     
     ofh.close()
     if args.pdf:
          
          import subprocess
          import shlex
          cmd = "gnuplot {}".format(args.outfile)
          #print(cmd)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE ).communicate()
          #print( output.decode('utf-8') )
