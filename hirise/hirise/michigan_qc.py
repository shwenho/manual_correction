#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import range
from builtins import object
from past.utils import old_div
import networkx as nx
import simulateMichigan as sm
from genomeUtils import build_chromosome_dict
import numpy as np
import math

def r2contig(r,l):
    return "%s.%d" % (r[4],int(old_div(float(r[0]),l)))

# pAB is probability of a transition from A->B.  0 for in a set, 1 for out
def p00(i,s,p,d):  # stay in a set
        return (s[i-1][0]+p['stay_in' ] +d*p['dont_tag']  + p['tag'])
def p20(i,s,p,d):  # stay in a set
        return (s[i-1][2]+p['stay_in' ] +d*p['dont_tag']  + p['tag'])
def p02(i,s,p,d):  # New set, but no noise reads in between
        return (s[i-1][0]+p['leave' ] +p['enter'] +d*p['not_noise']  + p['tag'])
def p12(i,s,p,d):  # enter a set
        return ( s[i-1][1]+p['enter']   +d*p['not_noise']  + p['tag'])
def p11(i,s,p,d):
        return (s[i-1][1]+p['stay_out'] +d*p['not_noise'] + p['noise'])
def p01(i,s,p,d):
        return (s[i-1][0]+p['leave']    +d*p['not_noise'] + p['noise'])

#params_default = {'p_start':0.06067673484993309, 'p_end':0.4403116030132488, 'p_set':7.5854609697452894e-05, 'p_noise':1.4325387089986626e-07}
#params_default = {'p_noise': 1.4391479941065045e-07, 'p_start': 0.060509700203329714, 'p_end': 0.4381862978063483,  'p_set': 7.5957390412411213e-05}
#x =             {'p_noise': 1.4502095908810545e-07, 'p_start': 0.06129253254167618,  'p_end': 0.44513188907276463, 'p_set': 7.7206030374403492e-05}

params_default = {'p_start':old_div(1.0,100) ,'p_end':old_div(1.0,15.0), 'p_set': old_div(1.0,13000.0), 'p_noise':old_div(1.0,21938698.8244) }

class MichiganHMM(object):

    def set_scores(self,params):
        scores={}
        scores['stay_in'] =math.log( 1.0-params['p_end'] )
        scores['enter']   =math.log( params['p_start']   )
        scores['leave']   =math.log( params['p_end']   )
        scores['stay_out']=math.log( 1.0-params['p_start']   )
        scores['noise'] =math.log( params['p_noise'] )
        scores['not_noise'] =math.log( 1.0 - params['p_noise'] )
        scores['tag'] =math.log( params['p_set'] )
        scores['dont_tag'] =math.log( 1.0 - params['p_set'] )
        return(scores)

    def __init__(self,maxn=500000,params=params_default):
        self.s = np.zeros((maxn,3))
        self.a = np.zeros((maxn,3))
        self.d = np.zeros(maxn)
        self.i = 1
        self.maxn = maxn
        self.params = params
        self.scores= self.set_scores(params)

    def add_d(self,d):
        i=self.i
        s=self.s
        a=self.a
        p=self.scores
        if i==self.maxn:
            print("overflowed.",i,"self.maxn")
            exit(0)

        self.d[i]=d

#        d=dd[0]
#        l=[ (p00(i,s,p,d),'0'  ),(p0a0(i,s,p,d),'a'),(p10(i,s,p,d),'1') ]
#        l.sort()
#        best = l[-1][1]


# 0  Continue in a set
        if p00(i,s,p,d) > p20(i,s,p,d):
            s[i][0] = p00(i,s,p,d)
            a[i][0] = 0
        else:
            s[i][0] = p20(i,s,p,d)
            a[i][0] = 2
# 2  Start a new set
        if p02(i,s,p,d) > p12(i,s,p,d):
            s[i][2] = p02(i,s,p,d)
            a[i][2] = 0
        else:
            s[i][2] = p12(i,s,p,d)
            a[i][2] = 1
# 1  Noise read  
        if p01(i,s,p,d) > p11(i,s,p,d):
            s[i][1] = p01(i,s,p,d)
            a[i][1] = 0
        else:
            s[i][1] = p11(i,s,p,d)
            a[i][1] = 1

#        print s[i][0],s[i][1]
        self.i+=1

            

    def train(self,dd):
        self.dd=dd
        params = dict(self.params)

        for rep in range(10):
            self.i=1
            for d in dd:
                self.add_d(d[0])
            self.backtrace()
        new_params = dict(self.params)

    def report_params(self):
        print(self.params)

    def backtrace(self):
        x=0
        i=self.i-1
        s=self.s
        a=self.a
        dd=self.dd
        d=self.d
        if s[i][0]>s[i][1]:
            x=0
        else:
            x=1

        ll = max(s[i][0],s[i][1],s[i][2])        
        print("log likelihood:",ll)

        n_set_starts=0
        n_set_ends=0
        n_reads=i
        sum_inside_steps=0.0
        sum_outside_steps=0.0
        n_inside_steps=0
        n_outside_steps=0
        p=self.scores

        while i>0:
            next_x=a[i][x]
            if x==0:
                print("# |.",i,dd[i-1],s[i],x)
                n_inside_steps+=1
                sum_inside_steps+=d[i]                    
                if next_x==0:
                    pass
                elif next_x==2:
                    pass

#                    print "# |.",i,self.d[i],s[i],x
#                    n_set_starts+=1
#                    n_set_ends+=1
#                    sum_outside_steps+=self.d[i]
#                    n_outside_steps+=1

            elif x==2:
                if next_x == 0:
                    print("# >.",i,dd[i-1],s[i],x)
                    n_set_starts+=1
                    n_set_ends+=1
#                    sum_outside_steps+=d[i]
#                    n_outside_steps+=1
                else:
                    print("# L.",i,dd[i-1],s[i],x)
                    n_set_starts+=1
#                    sum_outside_steps+=self.d[i]
#                    n_outside_steps+=1
                    
                    
            else:
                if next_x==1:
                    print("# .|",i,dd[i-1],s[i],x)
#                    n_set_ends+=1
                    n_outside_steps+=1
                    sum_outside_steps+=d[i]
                    
                else:
                    print("# .J",i,dd[i-1],s[i],x)
                    n_set_ends+=1                   
                    n_outside_steps+=1
                    sum_outside_steps+=d[i]                    

            i-=1
            x=next_x
            
        print("n_inside_steps", n_inside_steps)
        print("n_set_ends", n_set_ends)
        print("n_set_starts", n_set_starts)
        print("n_outside_steps", n_outside_steps)
        self.params["p_start"]= old_div(float(n_set_starts+1), (n_outside_steps+1))
        self.params["p_end"  ]= old_div(float(n_set_ends+0.5), (n_inside_steps+2))
        self.params["p_set"  ]= old_div(float(n_inside_steps+0.5), (sum_inside_steps+1))
        self.params["p_noise"]= old_div(float(n_outside_steps+1), (sum_outside_steps+1))

        print(n_set_starts,n_reads,n_set_ends,n_inside_steps,sum_inside_steps,sum_outside_steps,n_outside_steps)

        new_params = {'p_start': self.params["p_start"],'p_end': self.params["p_end"], 'p_set': self.params["p_set"],'p_noise': self.params["p_noise"] }
        print('Noise spacing:', old_div(1.0,new_params['p_noise']), 'Set read spacing:', old_div(1.0,new_params['p_set']), 'Reads per set:', old_div(float(n_inside_steps + n_set_starts),n_set_starts), \
                          'Spacing between sets:',(old_div(1.0,new_params['p_noise']))*(old_div(1.0,self.params["p_start"])), \
                          "Fraction of reads in sets:", old_div(float( n_inside_steps + n_set_starts ),n_reads),\
                          'Reads between sets:', old_div(1.0,new_params['p_start']) , "ll:",ll)
        print("params:", new_params)
        self.scores= self.set_scores(self.params)
        return new_params

if __name__=="__main__":

    import sys
    import argparse
    import re
    parser = argparse.ArgumentParser(description="""
A script for learning the parameters of the Michigan library likelihood model from data.
""")
    parser.add_argument('-s','--simulate',default=False, action='store_true',help="Use simulated rather than real data.")

    parser.add_argument('-c','--chromosomes',required=True,help="A file which lists the chromosomes in the genome and the length of each.")
    parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging output.")
#parser.add_argument('-n','--nbreaks',default=100,type=int)
    parser.add_argument('-C',"--multiplicity",default=100,type=int,help="What's the mean number of aggregates per spot -- this gets used to calculate the total number of aggregates.")
    parser.add_argument('-m',"--microarraySpots",default=90000,type=int,help="How many spots are on the array?")
    parser.add_argument('-L','--length',default=150000,type=int,help="What's the mean length of the DNA in chromatin aggregates?")
    parser.add_argument('-u','--lengthUnc',default=15000,type=int,help="What's the standard deviation of the length of the DNA in chromatin aggregates?")
    parser.add_argument('-N','--nreads',default=500000000,type=int,help="How many reads (assuming forward reads only) get sampled?")
    parser.add_argument('-f','--noiseFraction',default=0.25,type=float,help="What fraction of the reads are noise, in the sense that they don't come from a 'set' from get ligated independently to a random spot.")
    parser.add_argument('-r','--readLength',default=100 ,type=int,help="ignored for now.")
    parser.add_argument('-H','--head',default=False ,type=int,help="Don't simulate a whole genome's worth of data, just sample reads from the first HEAD bases of chr1.")

    parser.add_argument('-l','--contigSize',default=5000,type=int,help="Chop the chromosomes into 'contigs' of this length")
    parser.add_argument('-M','--minSetAnchor',default=2,type=int,help="The threshhold number of reads to 'anchor' a contig to a set (spot)")
    parser.add_argument('-S','--minSpots',default=2,type=int,help="The threshhold number of shared anchored spots to infer scaffolding links between contigs.")
    parser.add_argument('--hilight',default=False,type=int,help="Hilight the edges between contigs that are HILIGHT steps away along the chromosome.")
    parser.add_argument('--colorCode',default=False,type=str,help="Display matrix in terminal color codes.")

    i=0
    zeros={'k':'000','K':'000','M':'000000',"G":'000000000'}
    for i in range(1,len(sys.argv)):
        arg = sys.argv[i]
#        print arg,"."
        m=re.match("\d+([MGKk])",arg)
        if m:
            arg = re.sub(m.group(1),zeros[m.group(1)],arg)
#            print arg,m.group(0)
            sys.argv[i]=arg
    main_args = parser.parse_args(sys.argv[1:])

#    print main_args

    if True or main_args.debug:
        print(main_args)

    chromLen=build_chromosome_dict(main_args.chromosomes)
    
    G=nx.Graph()
    
    if main_args.simulate:
        data = sm.Michigan_data_generator(main_args,chromLen)
    else:
        print("Reading from a real bam file not yet implemented.")
        exit(0)


    reads=[]
    print(data.params_blurb())
    spots = {}
    contigs = {}
    contigs_in_order=[]
    for r in data.generate_reads():
        contig = r2contig(r,main_args.contigSize) 
        if main_args.debug:
            print(r)
        reads.append(r)

    reads.sort(key=lambda x: (x[3],x[4],x[0]))
    hmm = MichiganHMM()
    dd=[]
    lastb=r[2]
    lastx=r[0]
    for i in range(1,len(reads)):
        if reads[i][3]==reads[i-1][3]:
            if reads[i][4]==reads[i-1][4]:
            #print "debug xxx:",reads[i], reads[i][0]-reads[i-1][0]
                dd.append([reads[i][0]-reads[i-1][0],reads[i]])
            else:
                dd.append([reads[i][0],reads[i]])
                
    hmm.train(dd)
