#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import hirise.mapper
import sys
import pysam

sam=pysam.Samfile(sys.argv[1])
h=sam.header
seqs=h['SQ']

#    seqs=h['SQ']
#    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

slen=[ s['LN'] for s in seqs ]
snam=[ s['SN'] for s in seqs ]

#print snam

m=mapper.map_graph( snam, slen )

while True:
    l = sys.stdin.readline()
    if not l: break
    if l[0]=="#": continue
    c = l.strip().split()
    print(c[0],c[1])
    print(c[0],c[2])

    a,b=m.add_break_in_original(int(c[0]),int(c[1])+1)
    m.flip(a)
    if int(c[2])>int(c[1])+500:
        a,b=m.add_break_in_original(int(c[0]),int(c[2])+1)
        m.flip(a)

m.write_map_to_file(sys.argv[2])
    

