#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from past.utils import old_div
import sys
#import networkx as nx
import hirise.chicago_edge_scores as ces


if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-m','--minlength',default=1000,type=int)
    parser.add_argument('-q','--quantile',default=0.99,type=float)

    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')

    args = parser.parse_args()

    ll={}
    if args.lengths:
        f = open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            l = int(c[1])
            ll[c[0]]=int(c[1])

        f.close()


    n_links={}
    if args.edgefile:
        f = open(args.edgefile)
    else:
        f=sys.stdin
    while True:
        l = f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        u,v,w = c[0],c[1],float(c[2])

        n_links[u] = n_links.get(u,0.0)+1
        n_links[v] = n_links.get(v,0.0)+1
        
    if args.edgefile:
        f.close()

    degrees = []
    for c,d in list(n_links.items()):
        degrees.append( old_div(d,ll[c]) )

    degrees.sort()

    z=int( args.quantile * len(degrees) )
    print(degrees[z])
