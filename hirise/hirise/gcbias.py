#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import random
#import hirise.chicago_edge_scores as ces
import pysam
#from random import random
from bisect import bisect
from hirise.bamtags import BamTags
import numpy.random

cum_weights=[]
total_weight=0.0

debug=False

def weighted_choice(l,weighting):
    global cum_weights
    global total_weight
    if len(cum_weights)==0:
        total = 0
        for i in l:
            total += weighting[i]
            cum_weights.append(total)
        total_weight=total
    x = random.random() * total_weight
    i = bisect(cum_weights, x)
    if debug: print(i,l[i])
    return l[i]

def get_nlinks_dict(c1,bams,name2index,args,ll={}):

    count={}
#    index2=name2index[c2]
    for sam in bams:
#        print "{}:{}-{}".format(c1,0,int(ll[c1])-1)
        for aln in sam.fetch(reference=c1):
            if (not aln.is_duplicate) and (aln.mapq >= args.mapq) and (BamTags.mate_mapq(aln) >= args.mapq):
                count[aln.rnext]=count.get(aln.rnext,0)+1
    return count

def windowed_cg_content(seq,w):
    L=len(seq)
    fp=0
    tp=0
    cc={}
    r={}
    while fp<L+old_div(w,2):
        if fp<L:
            new_base=seq[fp]
            cc[new_base] = cc.get(new_base,0)+1
        fp+=1
        while fp-tp>w:
            trailing_base = seq[tp]
            cc[trailing_base] = cc.get(trailing_base,0)-1
            tp+=1
        if fp>old_div(w,2):
            nn=cc.get('G',0)+cc.get('C',0)+cc.get('g',0)+cc.get('c',0)+cc.get('A',0)+cc.get('T',0)+cc.get('a',0)+cc.get('t',0)
            gc=cc.get('G',0)+cc.get('C',0)+cc.get('g',0)+cc.get('c',0)
            #print fp-w/2,cc,nn,gc,float(gc)/nn
            r[fp-old_div(w,2)-1]=old_div(float(gc),nn)
    return r

def coverage_stats(c1,bams,seq,minL,maxL,mapq,gcwindow,interval,verbose=True,histogram=False):
#    histo={}
    sort_buffer=[(0,0),(len(seq),0)]
    L=len(seq)
    for sam in bams:
        for aln in sam.fetch(reference=c1):
            if (aln.rnext==aln.tid) and (not aln.is_duplicate) and (aln.mapq >= mapq) and (BamTags.mate_mapq(aln) >= mapq):
                delta = aln.pnext - aln.pos
                if minL <= delta and delta <= maxL:
                    sort_buffer.append( (aln.pos  , 1) )
                    sort_buffer.append( (aln.pnext,-1) )
    sort_buffer.sort(  )
    x=0
    i=0
    lastx=0
    coverage=0
    lsb=len(sort_buffer)
    gc_dict={}
    if seq:
        gc_dict=windowed_cg_content(seq,gcwindow)    
    report_cursor=0 #args.interval
    while i<lsb:
        x,d=sort_buffer[i]
#        while i<lsb-1 and sort_buffer[i+1][0]==x:
#            d+=sort_buffer[i+1][1]
#            i+=1
        coverage+=d
#        print "s",c1,x,coverage,gc_dict.get(x)
        while x>report_cursor + interval:
            report_cursor+= interval
            y=report_cursor
            if verbose: print(c1,y,last_coverage,gc_dict.get(y))
            if histogram: histogram[last_coverage] = histogram.get(last_coverage,0)+1
        lastx=x
        last_coverage=coverage
        i+=1
#    if histogram:
#        return histo

def coverage_histogram(nsamples,bams,minL,maxL,mapq,interval):

    h=bams[0].header
    seqs=h['SQ']

    ll={}
    totalL=0
    contigs = []
    weights=[]
    for f in seqs:
        l = s['LN']
        if l > maxL:
            ll[ s['SN'] ] = l
            totalL+=l
            contigs.append(s['SN'])
            weights.append(l)
    weights = [ float(i)/total for i in weights]
    sampled_contigs = scipy.random.choice( contigs, replace=False, size=200, p=weights )
    histogram = {}
    while sum(histogram.values()) < nsamples:
        next_contig = sampled_contigs.pop()
        coverage_stats(next_contig,bams,False,minL,maxL,mapq,1,interval,histogram=histogram,verbose=False)

def get_nlinks(c1,c2,ll,bams,name2index,args):
    l1=ll[c1]
    l2=ll[c2]
    if l2<l1:
        x = c1
        c1= c2
        c2= x

    count=0
    index2=name2index[c2]
    for sam in bams:
        for aln in sam.fetch(region=c1):
            if (aln.rnext==index2) and (not aln.is_duplicate) and (aln.mapq >= args.mapq) and (BamTags.mate_mapq(aln) >= args.mapq):
                count+=1

    return count

def get_nlinks_spanning(c1,xx,ll,bams,name2index,args,gap):
    l1=ll[c1]

    count=0
#    index2=name2index[c2]
    for sam in bams:
        for aln in sam.fetch(region=c1):
            if (aln.rnext==aln.tid) and (not aln.is_duplicate) and (aln.mapq >= args.mapq) and (BamTags.mate_mapq(aln) >= args.mapq) and (aln.pos<xx-old_div(gap,2)) and (aln.pnext>xx+old_div(gap,2)):
                count+=1
    
    return count

def main():

    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-l','--links')
    parser.add_argument('-P','--param')
    parser.add_argument('-o','--outfile')
    parser.add_argument('-f','--fasta')
    parser.add_argument('-b','--bamfiles',required=True)
    parser.add_argument('-N','--ncontigs',default=25,type=int)
    parser.add_argument('-m','--minlen',default=10000,type=int)
    parser.add_argument('-w','--gcwindow',default=5000,type=int)
    parser.add_argument('-i','--interval',default=500,type=int)

    parser.add_argument('-A','--minL',default=5000,type=int)
    parser.add_argument('-B','--maxL',default=8000,type=int)

    parser.add_argument('-q','--mapq',default=10,type=int)
    parser.add_argument('-G','--genomesize',default=3e9,type=float)
#    parser.add_argument('-n','--ncontigs',type=int,required=True)
    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('--nogc',default=False ,  action='store_true')
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()
    #ces.debug=args.debug
    if args.seed != -1 :
        random.seed(args.seed)


    if args.debug:
        args.progress=True


    oname={}
    bams=[]
    for bamfile in args.bamfiles.split(','):
        bams.append( pysam.Samfile(bamfile,"rb") )
        oname[bams[-1]]=bamfile
    h=bams[0].header
    seqs=h['SQ']

    ffile=False
    if args.fasta:
        ffile=pysam.Fastafile(args.fasta)

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]
    contigs = []
    ncontigs = 0

    ll={}
    name2index={}
    G=0.0
    for i in range(len(snam)):
        if args.debug: print("n:",len(snam),i,snam[i],slen[i])
        cn=snam[i]
        ll[cn]=float(slen[i])
        if ll[cn]>=args.minlen:
            G+=ll[snam[i]]
            contigs.append(cn)
            ncontigs+=1
        name2index[snam[i]]=i
    if args.debug: print("done")
#    G=args.genomesize

    def area(l1,l2):
        return (old_div(l1,1000.0))*(old_div(l2,1000.0))

    rate=0.0
    totaln=0.0
    n_pairs=0
    gap=300.0

    seen={}
    seq=""
    while n_pairs < args.ncontigs:
        n_pairs+=1
        
        c1 = weighted_choice(contigs,ll)
        while c1 in seen:
            c1 = weighted_choice(contigs,ll)
        seen[c1]=1
        
        if args.fasta and not args.nogc: seq = ffile.fetch(reference=c1)
        print("#",c1,ll[c1],len(seq))
        coverage_stats(c1,bams,seq,args.minL,args.maxL,args.mapq,args.gcwindow,args.interval)


if __name__=="__main__":
    main()
