#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import sys
import argparse
import numpy as np
import numpy.linalg as la
import networkx as nx
import hirise.assembly_io as aio
import hirise.chicago_edge_scores as ces
from hirise.greedy_chicagoan2 import traverse_and_layout
from hirise.greedy_chicagoan2 import strand_check
import numpy as np
from numpy.linalg import solve
import struct
import hashlib

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-E','--edgefile',default="-")
parser.add_argument('-b','--besthits',default=False)
parser.add_argument('-N','--nchunks',default=128,type=int)
parser.add_argument('-f','--nfact',default=1.0,type=float)
parser.add_argument('-n','--mychunk',default=1,type=int)
parser.add_argument('-l','--nearestNeighborsOnly',default=False,action="store_true")
parser.add_argument('-M','--set_insert_size_dist_fit_params',default=False )
parser.add_argument('-L','--links')

args = parser.parse_args()
if args.progress: print("#",args)

ces.debug=args.debug

fmodel=open( args.set_insert_size_dist_fit_params )
contents = fmodel.read()
try:
     fit_params=eval(contents)
     fit_params['N']*=args.nfact
except:
     "couldn't deal with option", args.param
fmodel.close
ces.set_exp_insert_size_dist_fit_params(fit_params)

ah=aio.AssemblyReader(args.edgefile)
g=nx.Graph()
ll={}
c2scaffold={}
ah.edges2graph(g,ll,c2scaffold)
#print "c2scaffold",c2scaffold
besthit={}
if args.besthits:
     besthit=aio.BestHitsReader(args.besthits).besthit
 
def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False



def qdist(x,y):
    if (not x) or (not y): return (-1)
    if x[0]==y[0]:
        x,y,w,z = list(map(int,[x[2],x[3],y[2],y[3]]))
        ol = pairs_overlap((x,y),(w,z))
        if ol:
            return(-1*min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
        else:
            return(min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
    else:
        return(1.0e12)



def true_gap(c1,c2,bh):
     last_bhi = bh.get(c1,False)
     this_bhi = bh.get(c2,False)
     if this_bhi and last_bhi :
          aa = tuple(last_bhi[1:5])
          bb = tuple(this_bhi[1:5])
          qd = qdist(aa,bb)
          return qd
     return 1e12

mycontigs={}
scaffold_hashes={}

for c in list(c2scaffold.keys()):
     scaffold=c2scaffold[c]
     if not scaffold in scaffold_hashes:
          h=struct.unpack("<L", hashlib.md5(scaffold.encode("utf-8")).digest()[:4])[0]
          scaffold_hashes[scaffold]=h%args.nchunks
     if scaffold_hashes[scaffold]==args.mychunk-1:
          mycontigs[c]=1

#ces.ll=ll
links={}
if args.links:
     links=aio.LinksReader(glob=args.links,scaffoldmap=c2scaffold,intra=1,mycontigs=mycontigs).readlinks()

print("#loaded data")
sys.stdout.flush()

ends=[]
for n in g.nodes():
     if g.degree(n)==1:
          if n[:-2] in mycontigs:
               ends.append(n)

print("#ends:",ends)
sys.stdout.flush()

#g.nx.Graph()
seen={}
scaffold=1
for end in ends:
     if end in seen: continue

     coords={}
     facing={}
     nlinks={}
     contig_list=traverse_and_layout(end,coords,facing,0,1,g)
     for e in list(coords.keys()): seen[e]=1

     contig_list.sort(key=lambda x: coords[x])
     print("#",contig_list[:10])
     scaffold=c2scaffold[contig_list[0][:-2]]
     n=len(contig_list)
     nn=old_div(n,2)

     dij=np.zeros((nn+1,nn+1))
     kij=np.zeros((nn+1,nn+1))

     kij[nn,0]=kij[0,nn]=1.0
     l0=old_div(float(ll[contig_list[0][:-2]]),2)
     print("l0:",l0)
     dij[nn,0]=-l0
     dij[0,nn]= l0
     
     for i in range(n):
          ci=contig_list[i]
          xi=coords[ci]
          o1=0
          if ci[-1:]=="5": o1=1
          l1 = float(ll[ci[:-2]])
          for j in range(i+1,n):
               cj=contig_list[j]
               xj=coords[cj]
               #d_ij = xj-xi #coords[cj]-coords[ci]
               if abs(xj-xi)>200000: break
               l2 = float(ll[cj[:-2]])
               o2=0
               if ci[-1:]=="3": o2=1
               if strand_check(ci,cj,coords,facing):
                    links_ij =links.get((ci[:-2],cj[:-2]),[])
                    nlinks_ij=len(links_ij)
                    nlinks[ci[:-2],cj[:-2]]=nlinks_ij
                    if nlinks_ij>0 and ((not args.nearestNeighborsOnly) or (j-i==1)):
                         #print "#",links_ij
                         gap,score=ces.ml_gap(l1,l2,o1,o2,0,0,links_ij,0,50)
                         d_ij=gap+old_div(l1,2)+old_div(l2,2)
                         k_ij=-ces.model.d2dg2_llr( l1,l2,o1,o2,links_ij,gap)

                         dij[old_div(i,2),old_div(j,2)]=-d_ij
                         dij[old_div(j,2),old_div(i,2)]= d_ij
                         kij[old_div(i,2),old_div(j,2)]=kij[old_div(j,2),old_div(i,2)]=k_ij

                         print(i,j,old_div(i,2),old_div(j,2),l1,l2,scaffold,ci,cj,ci[:-2],cj[:-2],l1,l2,xi,xj,nlinks_ij,o1,o2,gap,score,k_ij,d_ij)
                    elif j-i==1:
                         gap,score=100,-1
                         d_ij=gap+old_div(l1,2)+old_div(l2,2)
                         k_ij=1.0e-6

                         dij[old_div(i,2),old_div(j,2)]=-d_ij
                         dij[old_div(j,2),old_div(i,2)]= d_ij
                         kij[old_div(i,2),old_div(j,2)]=kij[old_div(j,2),old_div(i,2)]=k_ij

                         print(i,j,old_div(i,2),old_div(j,2),l1,l2,scaffold,ci,cj,ci[:-2],cj[:-2],l1,l2,xi,xj,nlinks_ij,o1,o2,gap,score,k_ij,d_ij)

     m=np.zeros((nn+1,nn+1))
     for i in range(nn+1):
          for j in range(nn+1):
               if not i==j:
                    m[i,j]=kij[i,j]
               else:
                    m[i,j]=-sum([ kij[i,l] for l in range(nn+1) if not l==i ])

     y=np.zeros(nn+1)
     for i in range(nn+1):
          y[i] = -sum([ kij[i,j]*dij[i,j] for j in range(nn+1) if not j==i ])

     m[nn,nn]+=1.0

     x=solve(m,y)
     print("dummy:",x[nn])

     for i in range(0,n,2):
          ci=contig_list[i]
          cj=contig_list[i+1]
          contig=cj[:-2]
          print("scaffold:",contig,scaffold)
          print("\t".join(map(str,["#edge:",ci,cj,{'length':int(ll[contig]),'contig':True}])))

#     lastb=0.0
     i=0
     ci=contig_list[i*2]
     #xi=coords[ci]
     contig=ci[:-2]
     last_contig=contig
     l1 = float(ll[contig])
     last_length=l1
     a,b=x[i]-old_div(l1,2),x[i]+old_div(l1,2)
     lastb=b
     print("\t".join(map(str,["#adjusted_layout:",scaffold,i,l1,a,b])))
     for i in range(1,nn):
          ci=contig_list[i*2-1]
          cj=contig_list[i*2]

#          g[ci][cj]['length']=int(gap)

          contig=cj[:-2]
          l1 = float(ll[contig])
          a,b=x[i]-old_div(l1,2),x[i]+old_div(l1,2)
          gap=a-lastb

          g[ci][cj]['length']=max(2,gap)

          print("\t".join(map(str,["#adjusted_layout:",scaffold,i,l1,last_length,nlinks.get((last_contig,contig),0),a,b,gap,true_gap(contig,last_contig,besthit),kij[i-1,i],dij[i,i-1],contig,cj]+besthit.get(contig,[]))))
#          print "\t".join(map(str,["#edge:",ci,cj,{'length':max(2,gap),'contig':False}]))

          lastb=b
          last_contig=contig
          last_length=l1

     
aio.AssemblyWriter(g,ll,besthit,c2scaffold)
