#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import sys
import argparse
import numpy as np
import numpy.linalg as la
import networkx as nx
import assembly_io as aio
import hirise.chicago_edge_scores as ces
from greedy_chicagoan2 import traverse_and_layout
from greedy_chicagoan2 import strand_check
import numpy as np
from numpy.linalg import solve
import struct
import hashlib
import pysam
import math
import random
from hirise.bamtags import BamTags

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
#parser.add_argument('-E','--edgefile',default="-")
#parser.add_argument('-b','--besthits',default=False)
#parser.add_argument('-N','--nchunks',default=128,type=int)
#parser.add_argument('-f','--nfact',default=1.0,type=float)
#parser.add_argument('-n','--mychunk',default=1,type=int)
parser.add_argument('-b','--bamfiles',required=True)
parser.add_argument('-R','--region')
#parser.add_argument('-l','--nearestNeighborsOnly',default=False,action="store_true")
parser.add_argument('-M','--set_insert_size_dist_fit_params',default=False )
parser.add_argument('-L','--ilength',type=float,default=25000)
parser.add_argument('-w','--unmappable_window',type=float,default=200)
parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
args = parser.parse_args()
if args.seed != -1 :
     random.seed(args.seed)


if args.progress: print("#",args)

ces.debug=args.debug

fmodel=open( args.set_insert_size_dist_fit_params )
contents = fmodel.read()
try:
     fit_params=eval(contents)
#     fit_params['N']*=args.nfact
except:
     "couldn't deal with option", args.params
fmodel.close
ces.set_exp_insert_size_dist_fit_params(fit_params)

bamfiles = args.bamfiles.split(",")
    
oname={}
bams=[]
for bamfile in bamfiles:
     bams.append( pysam.Samfile(bamfile,"rb") )
     oname[bams[-1]]=bamfile
h=bams[0].header
seqs=h['SQ']
    
slen=[ s['LN'] for s in seqs ]
snam=[ s['SN'] for s in seqs ]
    
llen={}
for i in range(len(snam)):
     llen[snam[i]]=slen[i]

def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False



def qdist(x,y):
    if (not x) or (not y): return (-1)
    if x[0]==y[0]:
        x,y,w,z = list(map(int,[x[2],x[3],y[2],y[3]]))
        ol = pairs_overlap((x,y),(w,z))
        if ol:
            return(-1*min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
        else:
            return(min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
    else:
        return(1.0e12)


region=args.region
tname,crange=region.split(":")
x1,x2=list(map(int,crange.split("-")))
print(tname,x1,x2)
pairs=[]
for b in bams:
     print("#bam",b)
     for aln in b.fetch(region=region):
          if aln.is_duplicate: continue
          if aln.mapq<10.0: continue
          if BamTags.mate_mapq(aln)<10.0: continue
          if not aln.rnext==aln.tid : continue
          if not (x1<=aln.pnext and aln.pnext<=x2): continue
          if not aln.pos<aln.pnext: continue
          pairs.append((aln.pos,aln.pnext))
          print("sep:",aln.pnext-aln.pos,aln.pos,aln.pnext)

if len(pairs)==0: 
#     print "r:",-999
     exit(0)

print("npairs:",len(pairs))
print("mean sep:",old_div(float(sum([x[1]-x[0] for x in pairs])),len(pairs)))

def score0(x):
     s=0.0
     for x1,x2 in x:
          s+=math.log(ces.model.f(abs(x2-x1)))
     return s

def length_on_inverted_hap(x1,x2,a,b):
     d0=abs(x2-x1)

     x1m = x1
     if a<x1 and x1<b:
          x1m = b-(x1-a)
#          print "#hit",a,x1,b

     x2m = x2
     if a<x2 and x2<b:
          x2m = b-(x2-a)
#          print "#hit",a,x2,b

     d1=abs(x2m-x1m)
     if not d1==d0: print("#hit",d1,d0,d1-d0)
     return d1

def aff(x,a,b):
     x1,x2=x
     r = False
     if a<x1 and x1<b:
          r=True


     s=False
     if a<x2 and x2<b:
          s=True

     if r or s and not (r and s): return True
     return False

def naff(x,a,b):
     nd=0
     for xx in x:
          if aff(xx,a,b): nd+=1
     return nd

def simulate_inversion(x,a,b):
     d=[]
     for xx in x:
          x1,x2=xx
          x1m=x1
          x2m=x2
          if a<=x1 and x1<b:
               x1m=b-(x1-a)
          if a<=x2 and x2<b:
               x2m=b-(x2-a)

          if random.random()<0.5:
               d.append( (min(x1m,x2m) ,max(x2m,x1m) ) )
          else:
               d.append(xx)
     return d

def filter_breakpoint_windows(x,a,b,w):
     d=[]
     for xx in x:
          x1,x2=xx
          if min((abs(x1-a),abs(x2-a),abs(x1-b),abs(x2-b)))<old_div(w,2) : continue
          d.append(xx)
     return d


def score1(x,a,b):
     s=0.0
     for xx in x:
          x1,x2=xx
          if aff(xx,a,b):
               f1 = ces.model.f(abs(x2-x1))
               f2 = ces.model.f( length_on_inverted_hap(x1,x2,a,b) )
               s+=math.log(f1+f2-f1*f2)
          else:
               s+=math.log(ces.model.f(abs(x2-x1)))               
     return s

print(x1,x2)
center = old_div((x1+x2),2)

mean_delta = math.log(2) #sum([ (math.log( 2.0*ces.model.f(abs(x2-x1)) -ces.model.f(abs(x2-x1))*ces.model.f(abs(x2-x1)) ) - math.log( ces.model.f(abs(x2-x1)) ) ) for x1,x2 in pairs ])/len(pairs)
print("mean delta:",mean_delta)

a,b=int(center-old_div(args.ilength,2)),int(center+old_div(args.ilength,2))
pairsI0 = simulate_inversion(pairs,a,b)
pairsI  = filter_breakpoint_windows(pairsI0,a,b,args.unmappable_window)

pairs= filter_breakpoint_windows(pairs,a,b,args.unmappable_window)

s1=score0(pairs)
s20=score1(pairs,center,center+1)
s20I=score1(pairsI,center,center+1)
for l in [args.ilength]:
     a=center-(old_div(l,2))
     b=a+l
#     s1=score0(pairs)
     s2=score1(pairs,a,b)
     s2I=score1(pairsI,a,b)
     naffected=naff(pairs,a,b)
     d=s2-s20
     score = s2I-(s20I+naffected*mean_delta)
     score0= s2 -(s20 +naffected*mean_delta)
     print("r:",score,score0,l,a,b,s1,s2,d,s2I-s20I,s2I-s2,s2,s2I,s2-(s20+naffected*mean_delta),s2I-(s20I+naffected*mean_delta),naffected)



