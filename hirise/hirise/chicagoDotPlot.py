#!/usr/bin/env python3
import argparse
import sys
import os
import pysam
import matplotlib
import math
import itertools
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter


def bpFormatter(x,pos):
    if x > 1e9:
        return "%0.2f Gbp" %(x/1e9)
    if x > 1e6:
        return "%0.2f Mbp" %(x/1e6)
    if x > 1e3:
        return "%0.2f kbp" %(x/1e3)
    if x < 0:
        return "%d" % (x)
    else:
        return "%d bp" % (x)

def prepareBamFiles(bams):
    bam_objects = []
    for bam in bams:
        checkBamIndex(bam)
        fh = pysam.AlignmentFile(bam,mode='rb')
        bam_objects.append(fh)
    return bam_objects
        

def checkBamIndex(bam):
    index_path = bam + ".bai"
    if not os.path.isfile(index_path):
        print("No index file found for BAM file: %s\nCreating index." % (bam),file=sys.stderr)
        subprocess.check_call("samtools index %s" % (bam),shell=True)
    return

def lengthsFromHeaders(bams):
    prev_lengths = None
    prev_bam = None
    for bam in bams:
        lengths = {}
        header = bam.header
        for entry in header['SQ']:
            scaf = entry['SN']
            length = entry['LN']
            lengths[scaf]=length
        if prev_lengths:
            if lengths != prev_lengths:
                print("Error: BAM headers don't match: %s %s" % (prev_bam,bam.filename))
                sys.exit(1)
        prev_lengths = lengths
        prev_bam = bam.filename
    return lengths

def parseRegions(regions):
    ret = []
    for region in regions:
        scaf,coords = region.split(":")
        start,end = map(int,coords.split("-"))
        ret.append((scaf,start,end))
    return ret

def parseVs(points,region_map):
    ret = []
    for point in points:
        if ":" in region:
            scaf,point = region.split(":")
        else:
            point = int(region)
        ret.append((scaf,start,end))
    return ret

def prepareAxis(regions,lengths,buff):
    pos = 0
    region_map = {}
    for scaf,start,end in regions:
        length = end-start
        region_map[scaf] = pos
        pos += length + buff
    return region_map


def rotate(points):
    trans = math.sqrt(2)
    trans2 = trans/2
    rot = lambda x,y: (trans*x+(y-x)*trans2,(y-x)*trans2)
    return [(x,y) for (x,y) in itertools.starmap(rot,points)]


def get_points(regions,region_map,bams,mapq,minsep,maxsep):
    points = []
    mapqs  = []
    n = 0
    for bam in bams:
        for i,(scaf,start,stop) in enumerate(regions):
            for aln in bam.fetch(reference=scaf,start=start,end=stop):
                if aln.is_read2:
                    continue
                if aln.is_unmapped:
                    continue
                if aln.mate_is_unmapped:
                    continue
                if aln.mapq < mapq:
                    continue
                if aln.opt("MQ") < mapq:
                    continue
                if aln.is_duplicate:
                    continue
                mate_in_targets = False
                for j,(nscaf,nstart,nstop) in enumerate(regions):
                    if aln.next_reference_name != nscaf:
                        continue
                    if aln.next_reference_start < nstart:
                        continue
                    if aln.next_reference_start > nstop:
                        continue
                    mate_in_targets=True
                    break
                if not mate_in_targets: continue
                r1 = region_map[scaf] + aln.reference_start 
                r2 = region_map[nscaf] + aln.next_reference_start 
                x = (r1 + r2)/2
                y = abs(r1 - r2)
                if scaf == nscaf:
                    if y < minsep or y > maxsep: continue 
                aln_mapq = min(aln.mapq,aln.opt("MQ"))
                points.append((x,y))
                mapqs.append(aln_mapq)
    return points,mapqs

def make_plot(points,mapqs,region_map,show,save,regions,vs,dotsize=2,line_width=2):
    print("drawing figure")
    plt.figure(figsize=(20,10))
    x,y = zip(*points)
    plt.scatter(x,y,marker="o",s=dotsize,c=mapqs,label="read pairs",linewidths=0)
    plt.colorbar(orientation='vertical',shrink=0.5,use_gridspec=True)
    plt.plot([regions[0][1],regions[0][2]],[0,0],c='blue',lw=2)
    plt.ylabel("Read pair separation")
    if len(regions)==1:
        plt.xlabel("%s position" % (regions[0][0]))
    else:
        plt.xlabel("Position in concatenated coordinate system")
    xmin,xmax = plt.xlim()
    ymin,ymax = plt.ylim()
    if vs:
        for v in vs:
            left_arm_x = [v,xmin]
            left_arm_y = [0,2*(v-xmin)]
            right_arm_x = [v,xmax]
            right_arm_y = [0,2*(xmax-v)]
            plt.plot(left_arm_x,left_arm_y,c='black',ls='dashed',lw=line_width)
            plt.plot(right_arm_x,right_arm_y,c='black',ls='dashed',lw=line_width)
    plt.xlim((regions[0][1],regions[0][2]))
    plt.ylim((ymin/5,ymax))
    bp_fmt = FuncFormatter(bpFormatter)
    ax = plt.gca()
    ax.xaxis.set_major_formatter(bp_fmt)
    ax.yaxis.set_major_formatter(bp_fmt)
    labels = ax.get_xticklabels()
    plt.setp(labels,rotation=30)
    plt.tight_layout()
    if show:
        print("showing figure")
        plt.show()
    if save:
        print("saving figure")
        plt.savefig(save, dpi=100)


def main(args):
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('bams',nargs='+',help="BAM files containing data to plot.")
    parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
    parser.add_argument('-q','--mapq',default=20,  type=float,help="Minimum map quality threshold.")
    parser.add_argument('-m','--minsep',default=500,  type=int,help="Hide short pairs for speed.")
    parser.add_argument('-X','--maxsep',default=200000,  type=float,help="Max pair sep.")
    parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
    #parser.add_argument('-R','--regions',nargs='+',help="Regions to plot in samtools style")
    parser.add_argument('-R','--region',required=True,action="append",help="Region to plot in samtools style")
    parser.add_argument('-b','--buffer',action="store",type=int,default=100000,
        help="Size of buffer between regions in the plot, defaults to 100kb.")
    parser.add_argument('-s','--support',type=argparse.FileType('w'),help="Specify a file containing"
        "support scores in .IGV format to display support curve.")
    parser.add_argument('--show',default=False,action="store_true",help="Display plot.")
    parser.add_argument('--save',default=None,action="store",help="Save plot to specified file.")
    parser.add_argument('--annotate-points',dest="vs",nargs='+',type=int,help="Points to draw a V at, indicating all "
        "read pairs spanning the given positon. Specified in scaffold coordinates.")
    parser.add_argument('--dot-size',default=2,type=int,help="Dot size in points, defaults to 2.")
    parser.add_argument('--line-width',default=1,type=int,help="Line width in points, defaults to 1.")

    args = parser.parse_args()
    bam_objects = prepareBamFiles(args.bams)
    lengths = lengthsFromHeaders(bam_objects)
    regions = parseRegions(args.region)
    region_map = prepareAxis(regions,lengths,args.buffer)
    points,mapqs = get_points(regions,region_map,bam_objects,args.mapq,args.minsep,args.maxsep)
    make_plot(points,mapqs,region_map,args.show,args.save,regions,args.vs,args.dot_size,args.line_width)

if __name__ == "__main__":
    main(sys.argv)
