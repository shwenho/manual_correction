#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
import sys


f1=open(sys.argv[1])
f2=open(sys.argv[2])

#def get_next_line(f):
#    l1 = 

s2,x2=-1,-1

debug=False

f1end=False
f2end=False

while True:

    caughtup=False
    while not caughtup:
        l1 = f1.readline()
        if not l1: 
            f1end=True
            break
        c1 = l1.strip().split()
        s1,x1,y1 = list(map(int,c1[1:]))
        if (s1,x2)>(s2,x2): 
            caughtup=True
        if debug: print("<<",s1,x1,y1)
        if s1==s2 and ((x1<=x2 and x2<=y1)or(x2<=x1 and x1<=y2)): print("break:",s1,max(x1,x2),min(y1,y2)) #,"#overlap"

    if f1end and f2end: break


    caughtup = False
    while not caughtup:
        l2 = f2.readline()
        if not l2: 
            f2end=True
            break
        if not l2: break
        c2 = l2.strip().split()
        s2,x2,y2 = list(map(int,c2[1:]))
        if debug: print(">>" ,s2,x2,y2)
        if (s2,x2)>(s1,x1): 
            caughtup=True
            if debug: print("##")
        if s1==s2 and ((x1<=x2 and x2<=y1)or(x2<=x1 and x1<=y2)): print("break:",s1,max(x1,x2),min(y1,y2)) #,"#overlap"

    
    if f1end and f2end: break

    
    
