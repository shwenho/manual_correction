#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function

import sys
import argparse
import os
import time

import glob

units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
def human_readable_size(nbytes):
    if nbytes == 0: return '0 b'
    i = 0
    while nbytes >= 1024 and i < len(units)-1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, units[i])

def main():

     parser = argparse.ArgumentParser(description="Clean up a hirise run directory to save disk space.")
     parser.add_argument('-d','--debug',default=False,action="store_true")
     parser.add_argument('-n','--dryrun',default=False,action="store_true",help="Don't actually delete anything:  dry run only.")
     parser.add_argument('-v','--verbose',default=False,action="store_true",help='Show the description of each.')
     parser.add_argument('-D','--cwd',metavar='dir',help="Give the path to a hirise run to leaf-blow.")
     parser.add_argument('-o','--outfile',metavar='file',help="Write logging output to file.")
     parser.add_argument('-L','--level',metavar='N',type=int,default=1,help="Severity level.")

     args = parser.parse_args()

     if args.cwd: 
          os.chdir(args.cwd)

     if args.debug:
          args.verbose=True

     deleted=0

     for f in glob.glob("*.meraudierin.*.inter"):
          if args.verbose: print("file to delete:",f)
          deleted+=os.path.getsize(f)
          if not args.dryrun:
               os.remove(f)

     for f in glob.glob("refined.*.out.inter"):
          if args.verbose: print("file to delete:",f)
          deleted+=os.path.getsize(f)
          if not args.dryrun:
               os.remove(f)

     for f in glob.glob("hirise_iter_?.gapclose.??.err"):
          if args.verbose: print("file to delete:",f)
          deleted+=os.path.getsize(f)
          if not args.dryrun:
               os.remove(f)

     for f in glob.glob("hirise_iter_?.gapclose.??"):
          if args.verbose: print("file to delete:",f)
          deleted+=os.path.getsize(f)
          if not args.dryrun:
               os.remove(f)

     for f in glob.glob("chicago_weak_segs_iter*_part*.out"):
          if args.verbose: print("file to delete:",f)
          deleted+=os.path.getsize(f)
          if not args.dryrun:
               os.remove(f)

     for f in glob.glob("chicago_support_iter*_part*.out"):
          if args.verbose: print("file to delete:",f)
          deleted+=os.path.getsize(f)
          if not args.dryrun:
               os.remove(f)

     for f in glob.glob("*.audit"):
          if os.path.exists(f+"_merge") or os.path.exists(f+"_merge.gz"):
               if args.verbose: print("file to delete:",f, " because {}_merge".format(f))
               deleted+=os.path.getsize(f)
               if not args.dryrun:
                    os.remove(f)

     print("total size:",human_readable_size(deleted))

if __name__=="__main__":
     main()

