#!/usr/bin/env python3
#!/usr/bin/env python3

## Double threshold

from __future__ import print_function
import sys
import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-1','--t1',required=True,type=int)
parser.add_argument('-2','--t2',required=True,type=int)
parser.add_argument('-d','--debug',action="store_true")
parser.add_argument('-o','--outhist')

args = parser.parse_args()

out=0
in1=1
in2=2

state=out

h={}

last_scaffold="-"
last_x="-"
start_x=0
while True:
    l= sys.stdin.readline()
    if not l: break
    c = l.strip().split()
    scaffold,x,y = c[0],int(c[1]),int(c[2])
    if args.debug: print("#",scaffold,x,y,state,start_x)

    if args.outhist: h[y]=h.get(y,0)+1

    if scaffold==last_scaffold:
        if state == in2 and y<args.t1:
            print(scaffold,start_x,x)
            state=out
            continue
    else: 
        if state==in2:
            print(last_scaffold,start_x,last_x, "#toend")
        state=out
        last_scaffold=scaffold
        last_x=x
        continue

    if state==out and y >= args.t1: 
        state=in1
        start_x=x
        
    if y>=args.t2: 
        state=in2
    

    if y<args.t1: state=out
    last_scaffold=scaffold
    last_x=x

if args.outhist:
    fh=open(args.outhist,"wt")
    depths=list(h.keys())
    depths.sort()
    for d in depths:
        fh.write("{}\t{}\n".format(d,h[d]))
    fh.close()

