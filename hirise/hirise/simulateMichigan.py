#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from builtins import object
from past.utils import old_div
import random
import heapq as hq
import math
import numpy as np 


def build_chromosome_dict(filename):
    """read a two-column text file mapping chromosomes to lengths and return a dictionary."""
    d={}
    f=open(filename)
    while True:
        l=f.readline()
        if not l:
            break
        c=l.strip().split()
        d[c[0]]=int(c[1])

    f.close()
    return d

def weighted_random_chromosome(l={},g=1,L=[]):
    """Chose a chromosome at random, with probability proportional to their lengths."""
    if not L:
        if args.debug:
            print("init chooser")
        s=0.0
        for chr in sorted(l.keys()):
            x=old_div(float(l[chr]),g)
            s+=x
            L.append([chr,s])
        if args.debug:
            print(L)

    r=random.random()
    for l in L:
        if r<l[1]:
            return l[0]


#def random_genome_location():
#    """return a 2-tuple with chromosome and position for base in the genome selected uniformly at random."""
#    chromosome=weighted_random_chromosome(chromLen,genome_length)
#    x=int(random.random()*chromLen[chromosome])
#    return((chromosome,x))

    
bsize=6000
class Buffered_geometric_int_factory(object):
    def __init__(self,p):
        if p<0: raise Exception 
        self.p = p
        self.buffer=np.random.geometric(p=self.p, size=bsize)
        self.i=0

    def draw(self):
        if self.i==bsize:
            self.buffer=np.random.geometric(p=self.p, size=bsize)
            self.i=0
        i=self.i
        self.i+=1
        return self.buffer[i]

class Buffered_uniform_int_factory(object):
    def __init__(self,m):
        self.m = m
        self.buffer=np.random.randint(1,self.m+1, size=bsize)
        self.i=0

    def draw(self):
        if self.i==bsize:
            self.buffer=np.random.randint(1,self.m+1, size=bsize)
            #self.buffer=np.random.geometric(p=self.p, size=bsize)
            self.i=0
        i=self.i
        self.i+=1
        return self.buffer[i]


def draw_step(p,buffer={}):
    x=math.floor(old_div(-math.log(random.random()),(-math.log(1.0-p)))); 
    return x

def merge_heap_list_pop(l1,l2):
    if len(l1)>0 and (len(l2)==0 or l1[0][0]<l2[0][0]):
        return hq.heappop(l1)
    elif len(l2)>0:
        return l2.pop(0)
    else:
        return(())

class Michigan_data_generator(object):

    def expected_set_span(self):
        args=self.args
        return args.length - 2.0*(args.length/(self.expected_reads_per_set+1)) 
    def expected_set_span_unc(self):
        args=self.args
        return math.sqrt(args.lengthUnc**2.0 + 2.0*((args.length/(self.expected_reads_per_set+1))**2.0)) 

    def __init__(self,args,chromLen):
        self.args = args
        self.chromLen = chromLen
        self.spot_dice = Buffered_uniform_int_factory(args.microarraySpots)
        self.genome_length = sum(chromLen.values())
        self.chromosomes=sorted(chromLen.keys())
        self.expected_n_sets =  float(args.microarraySpots) * args.multiplicity
        self.p_set_start_per_base = self.expected_n_sets / self.genome_length
        self.set_step =Buffered_geometric_int_factory(p=self.p_set_start_per_base)

        self.expected_reads_per_set = (1.0 - args.noiseFraction) * args.nreads / self.expected_n_sets
        self.p_noise_read_per_base = args.noiseFraction*args.nreads / self.genome_length
        self.nr_step=Buffered_geometric_int_factory(p=self.p_noise_read_per_base)
        self.n_spots = args.microarraySpots
        if args.debug:
            print("##", old_div(1.0,self.p_set_start_per_base),self.expected_reads_per_set,old_div(self.args.length,self.expected_reads_per_set),old_div(1.0,self.p_noise_read_per_base))

    def params_blurb(self):
        print("""#############
# Simulating michigan data with:
#                       Total N reads: %3g
#             Fraction of noise reads: %3g
#                  N microarray spots: %3g  
#       Chromatin aggregates per spot: %3.2g
# Expected total number of aggregates: %3.2g
#    Expected number or reads per set: %3.3f
#       Expected spacing between sets: %3.3f
#         Mass of sampled DNA in sets: %3.3f ng
#                   DNA fragemnt size: %3.3f +- %3.3f Kbp
#              Expected set footprint: %3.3f +- %3.3f Kbp
#     Same-barcode noise read spacing: %3.3f
        ##########""" % (self.args.nreads, self.args.noiseFraction, self.args.microarraySpots, self.args.multiplicity, self.expected_n_sets, self.expected_reads_per_set, self.args.microarraySpots/self.p_set_start_per_base - self.args.length, (self.args.length*self.expected_n_sets)/0.978e12 , self.args.length/1000, self.args.lengthUnc/1000 , self.expected_set_span()/1000, self.expected_set_span_unc()/1000 , (1.0/self.p_noise_read_per_base)*self.n_spots ))

    def generate_reads(self):
        hp=[]

        rid=0
        setid=0
        nrx=0

        noise_buffer=[]

        for ch in self.chromosomes:
            if self.args.debug:
                print(ch)
            x=0
            nrx=0

            while nrx < x+self.args.length:
                nrx+=self.nr_step.draw()  #draw_step(p_noise_read_per_base)
                rid+=1
                spotid = self.spot_dice.draw() #np.random.randint(90000) #random.choice(range(args.microarraySpots))
                if nrx<self.chromLen[ch]:
                    noise_buffer.append((nrx,rid,0,spotid))
                if self.args.debug:
                    print("###",nrx,len(noise_buffer))
            while True:
                incr = self.set_step.draw()  #draw_step(p_set_start_per_base)
                frag_length = int(random.normalvariate(self.args.length,self.args.lengthUnc))
                while frag_length < 0.0:
                    frag_length = int(random.normalvariate(self.args.length,self.args.lengthUnc))

                while nrx < self.chromLen[ch] and nrx < x+incr+frag_length:
                    nrx+=self.nr_step.draw() #draw_step(p_noise_read_per_base)
                    rid+=1
                    #spotid = random.choice(range(args.microarraySpots))
                    spotid = self.spot_dice.draw() #np.random.randint(90000) #random.choice(range(args.microarraySpots))

                    #hq.heappush(hp,(nrx,rid,0,spotid))
                    if nrx<self.chromLen[ch]:
                        noise_buffer.append((nrx,rid,0,spotid))
                        if self.args.debug:
                            print("###",nrx,len(noise_buffer))



                x+=incr
                if x>= self.chromLen[ch]:
                    break

                if x+frag_length >= self.chromLen[ch]:
                    continue

                if self.args.head and self.args.head < x:
                    break

                while (len(noise_buffer)>0 or len(hp)>0) and ( (len(hp)>0 and hp[0][0]<x) or ( len(noise_buffer)>0 and noise_buffer[0][0]<x)):
                    r = merge_heap_list_pop(hp,noise_buffer)
                    yield(r+(ch,))
#                    write_output_line(r,ch,self.args.readLength)
                    #print "XXXX",r

                setid+=1
        #        spotid = random.choice(range(args.microarraySpots))
                spotid = self.spot_dice.draw() #np.random.randint(90000) #random.choice(range(args.microarraySpots))

                n_reads = np.random.poisson(self.expected_reads_per_set) #int(random.normalvariate(expected_reads_per_set,math.sqrt(expected_reads_per_set)))
                if n_reads==0: continue
#                while n_reads==0:
#                    n_reads = np.random.poisson(self.expected_reads_per_set) #int(random.normalvariate(expected_reads_per_set,math.sqrt(expected_reads_per_set)))
                    
                p_read_per_base = n_reads/frag_length
                if p_read_per_base>1.0: p_read_per_base=0.9999
                read_step =Buffered_geometric_int_factory(p=p_read_per_base)
                if self.args.debug:
                    print("#",setid,str((ch,x)),frag_length,n_reads,len(hp),len(noise_buffer),rid,"%0.4f" %(old_div(float(x),self.chromLen[ch])))
                y=x
                for r in range(n_reads):
                    y+= read_step.draw()  #draw_step(p_read_per_base)
        #            if y-x>frag_length:
        #                break
                    rid+=1
        #            print "# read%d\tset%d\tspot%d\t%s\t%d\t%d" % (rid,setid,spotid,ch,y,y+100)
                    hq.heappush(hp,(y,rid,setid,spotid))
        #        print hp

            if self.args.head:
                break

            while (len(noise_buffer)>0 or len(hp)>0):
                r = merge_heap_list_pop(hp,noise_buffer)
                yield(r + (ch,))
#                write_output_line(r,ch,self.args.readLength)


def write_output_line(r,ch,readLength):
    print("r%d\t%d\tm%d\t%s\t%d\t%d" % (r[1],r[2],r[3],r[4],r[0],r[0]+readLength))

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c','--chromosomes',required=True)
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-C',"--multiplicity",default=100,type=int,help="Number of aggregates per barcode.")
    parser.add_argument('-m',"--microarraySpots",default=90000,type=int,help="Number of barcodes")
    parser.add_argument('-L','--length',default=150000,type=int,help="Length of DNA fragments")
    parser.add_argument('-u','--lengthUnc',default=15000,type=int,help="Standard deviation of length distribution.")
    parser.add_argument('-N','--nreads',default=500000000,type=int,help="Number of reads sampled.")
    parser.add_argument('-f','--noiseFraction',default=0.25,type=float,help="Fraction of reads that are 'noise'")
    parser.add_argument('-r','--readLength',default=100 ,type=int,help="Read length.")
    parser.add_argument('-H','--head',default=False ,type=int,help="Exit after HEAD bases of simualted reference.")

    main_args = parser.parse_args()

    if main_args.debug:
        print(main_args)

    chromLen=build_chromosome_dict(main_args.chromosomes)
    
    data = Michigan_data_generator(main_args,chromLen)

    data.params_blurb()
    for r in data.generate_reads():
        #print r
        write_output_line(r,"x",main_args.readLength)
