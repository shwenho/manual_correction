#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import map
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx
import hirise.mapper
import pickle as pickle
import re

def log(s):
    print(s)

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--debug',action='store_true',default=False)
    parser.add_argument('--map')


    nodes={}

    args = parser.parse_args()

    if args.debug: print("load",args.map)

    mapp = pickle.load(open(args.map,"rb"))
    if args.debug: print("done loading")

    edges = []
    degrees = {}

    for r in mapp.roots:
        if not r == r.root: print("wtf?",mapp.print_name(r),mapp.print_name(r.root))
        print("\t".join(map(str,[mapp.print_name(r),1+max(r.leaf.mapped_coord,r.root.mapped_coord)])))



