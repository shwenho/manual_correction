#!/usr/bin/env python3
#!/usr/bin/env python3
#import pysam
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
from builtins import object
from past.utils import old_div
import sys
import math
import hirise.mapper

from math import factorial as fac
from fractions import Fraction as mpq
one = 1
mpz = int


#Final set of parameters            Asymptotic Standard Error
#=======================            ==========================
#
#a               = 11.2064          +/- 0.2885       (2.574%)
#b               = 0.844533         +/- 0.003699     (0.438%)
#c               = 0.00290609       +/- 4.107e-05    (1.413%)
#d               = 19448.3          +/- 176.7        (0.9085%)


#correlation matrix of the fit parameters:

#               a      b      c      d      
#a               1.000 
#b               0.999  1.000 
#c               0.892  0.911  1.000 
#d              -0.761 -0.783 -0.933  1.000 
#gnuplot> plot [1000:][1e-7:] "/mnt/nas/projects/human/analysis/hg19/chicago_1/inserts/chicago_1.smalt.ios.no_pseudo.dat" u (($1+500)):10, a*(x**-b)*exp(-x/10000)+c*exp(-x/d)+4e-7


#Insert size distribution fit parameters:
isd_a               = old_div(11.2064, 1000.0)     #    +/- 0.2885       (2.574%)
isd_b               = 0.844533             #    +/- 0.003699     (0.438%)
isd_c               = old_div(0.00290609,1000.0)   #    +/- 4.107e-05    (1.413%)
isd_d               = 19448.3              #    +/- 176.7        (0.9085%)
isd_f = 10000.0

isd_a,isd_b,isd_c,isd_d,isd_f = 0.0000648979,0.979283,1.93325e-10,19448.3,10000.0

alpha=[]
beta=[]
G=3000000000
pn=0.5
N=1000000

def set_exp_insert_size_dist_fit_params(params):
    
    global alpha
    global beta
    global pn
    global N
    global G
    G=params["G"]
    pn=params["pn"]
    N=params["N"]
    alpha=params["alpha"]
    beta=params["beta"]
#    print alpha,beta


def set_insert_size_dist_fit_params(a,b,c,d,f,pn=0.3,N=400000000.0,G=3.0e9):
    isd_a=a
    isd_b=b
    isd_c=c
    isd_d=d
    isd_f=f


#def _harmonic5(a, b):
#    if b-a == 1:
#        return one, mpz(a)
#    m = (a+b)//2
#    p, q = _harmonic5(a,m)
#    r, s = _harmonic5(m,b)
#    return p*s+q*r, q*s

#def harmonic5(n):
#    return mpq(*_harmonic5(1,n+1))


mean_fragment_size = 15000.0
inverse_fragment_size = old_div(1.0,mean_fragment_size)
one_minus_inverse_fragment_size = 1.0 - old_div(1.0,mean_fragment_size)

minp=1.0e-9

def insert_size_dist_exp(d):
    #if args.debug: print "p(d):",inverse_fragment_size,one_minus_inverse_fragment_size,d,one_minus_inverse_fragment_size**d
    return (inverse_fragment_size)*(one_minus_inverse_fragment_size**d)

pl_norm= 0.5772156649 + math.log(200000) #float(harmonic5(150000))

def insert_size_dist_pl(d):
    if d<1: return minp
#    if d>200000.0: d = 200000.0
    return old_div((old_div(1.0,float(d))),pl_norm)

def insert_size_dist_fit(x):
    if x<0.0:        sys.stderr.write("wtf? negative insert size tested? {} \n".format(x))
    #a               = 11.2064 / 1000.0     #    +/- 0.2885       (2.574%)
    #b               = 0.844533             #    +/- 0.003699     (0.438%)
    #c               = 0.00290609 /1000.0   #    +/- 4.107e-05    (1.413%)
    #d               = 19448.3              #    +/- 176.7        (0.9085%)
    #f = 10000.0
    x=float(x)
    return ((isd_a*(x**-isd_b)*math.exp(old_div(-x,isd_f))+isd_c*math.exp(old_div(-x,isd_d))))

def insert_size_dist_expsum(x):
    return sum( [ alpha[i]*beta[i]*math.exp(-beta[i]*x) for i in range(len(alpha)) ] )

#insert_size_dist = insert_size_dist_fit
insert_size_dist = insert_size_dist_expsum

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

debug=False
endsonly=False
args=False
links = {}
lastt = 0
slen=[]# s['LN'] for s in seqs ]
snam=[]# s['SN'] for s in seqs ]
G=0.0

def omega(l1,l2,g,d):
    la = min(l1,l2)
    lb = max(l1,l2)
    if d<g:
        if debug: print("#omega=0",l1,l2,g,d)
        return 0
    if d<la+g:
        return d-g
    if d<lb+g:
        return la
    if d < la+lb+g:
        return la+lb+g-d
    if debug: print("#omega=0 * ",l1,l2,g,d)
    return 0


#00:  l1-x + y
#01:  l1-x + l2-y
#10:  x+y
#11:  x+l2-y
def fragment_size(l1,l2,o1,o2,coords,g=0):
    x,y = coords[0],coords[1]
    if (o1,o2) == (0,0):
        return (y+l1-x+g)
    if (o1,o2) == (0,1):
        return (l2-y+l1-x+g)
    if (o1,o2) == (1,0):
        return (x+y+g)
    if (o1,o2) == (1,1):
        return (l2-y+x+g)

def logp_lengths(l1,l2,o1,o2,G,pn,links,N,g=0):
    n = (len(links))
#    llg = -n * math.log(G)
    llg=0.0
    for i in range(n):
        d = fragment_size(l1,l2,o1,o2,links[i],g)
        #p_d = max(minp,insert_size_dist(d))
        p_d = insert_size_dist(d)
        if debug: print("insert, p_ins:",d,p_d)
        llg += math.log( old_div(pn,G) + (1.0-pn)*p_d )
#        o = omega(l1,l2,g,d)
#        if p_d > 0.0 and o > 0.0:
#            llg += math.log( pn/G + (1.0-pn)*p_d )
#            llg += math.log( omega(l1,l2,g,d) )
    return llg

max_frag = 150000
p_not_hit_min = -1
p_not_cache={}

def s_exact(l1a,l2a,G,g,pn):
    l1,l2=l1a,l2a
#    l1 = min(l1a,max_frag)
#    l2 = min(l2a,max_frag)
    S=0.0
    for d in range(int(g),int(l1+l2+g)):
        om = float(omega(l1,l2,g,d))
        S+=insert_size_dist(d)*(om)
    return S

def p_not_a_hit_exact(l1a,l2a,G,g,pn):
    l1,l2 = l1a,l2a

    r = pn*(1.0 - (old_div(l1,G)) * (old_div(l2,G)))
    r2=0.0
    l1 = min(l1a,max_frag)
    l2 = min(l2a,max_frag)
    if (l1,l2,g) in p_not_cache:
        r2 = p_not_cache[l1,l2,g]
    else:
        r2=1.0
        for d in range(int(g),int(l1+l2+g)):
            om = float(omega(l1,l2,g,d))
    #        if l1==2246 or om<0.0 or (l1+l2+g-d)<100 : print "pnah",l1,l2,d,r,omega(l1,l2,g,d)
            r2 -= 2.0*insert_size_dist(d)*(old_div(om,G))
        p_not_cache[l1,l2,g] = r2
    r3=r+(1.0-pn)*r2
#    r=1.0-r
    if debug: print("p_not_a_hit:",r,r2,r3)
#    if r < 0.96:        exit(0)

    return r3

from scipy.special import gammaincc
#def H(x,a,b,c,d,f):
#    print "#H",a,f**(2.0-b),gammainc(2.0-b,x/f),-a*f**(2.0-b)*gammainc(2.0-b,x/f),-d*d*c*gammainc(2.0,x/d),x,a,b,c,d,f
#    return -a*(f**(2.0-b))*gammainc(2.0-b,x/f)-d*d*c*gammainc(2.0,x/d)
#def J(x,a,b,c,d,f):
#    print "#G",-a*f**(1.0-b)*gammainc(1.0-b,x/f),- c*d*math.exp(-x/d),x,a,b,c,d,f
#    return -a*(f**(1.0-b))*gammainc(1.0-b,x/f)-   c*d*math.exp(-x/d)

def H(x1,x2,a,b,c,d,f):
#    print "#H", -a*(f**(2.0-b)), gammaincc(2.0-b,x2/f)-gammaincc(2.0-b,x1/f) #a,f**(2.0-b),gammainc(2.0-b,x/f),-a*f**(2.0-b)*gammainc(2.0-b,x/f),-d*d*c*gammainc(2.0,x/d),x,a,b,c,d,f
    return -a*(f**(2.0-b))*(gammaincc(2.0-b,old_div(x2,f))-gammaincc(2.0-b,old_div(x1,f)))-d*d*c*(gammaincc(2.0,old_div(x2,d))-gammaincc(2.0,old_div(x1,d)))
def J(x1,x2,a,b,c,d,f):
#    print "#G", -a*(f**(1.0-b)), (gammaincc(1.0-b,x2/f)-gammaincc(1.0-b,x1/f)) #-a*f**(1.0-b)*gammainc(1.0-b,x/f),- c*d*math.exp(-x/d),x,a,b,c,d,f
    return -a*(f**(1.0-b))*(gammaincc(1.0-b,old_div(x2,f))-gammaincc(1.0-b,old_div(x1,f)))-   c*d*(math.exp(old_div(-x2,d))-math.exp(old_div(-x1,d)))

def H_expsum(x1,x2,alpha,beta):
    return sum([ -(old_div(alpha[i],beta[i]))*(math.exp(-beta[i]*x2)*(beta[i]*x2 + 1.0) - math.exp(-beta[i]*x1)*(beta[i]*x1+1.0))  for i in range(len(alpha)) ])
def J_expsum(x1,x2,alpha,beta):
    return sum( [ -alpha[i]*(math.exp(-beta[i]*x2)-math.exp(-beta[i]*x1))  for i in range(len(alpha))] )

#    return sum( [ alpha[i]*beta[i]*math.exp(-beta[i]*x) for i in range(len(alpha)) ] )

def ddg_p_expsum(x,alpha,beta):
    return ( sum([ -alpha[i]*beta[i]*beta[i]*math.exp(-beta[i]*x) for i in range(len(alpha)) ]) )

def d2dg2_p_expsum(x,alpha,beta):
    return ( sum([ alpha[i]*(beta[i]**3.0)*math.exp(-beta[i]*x) for i in range(len(alpha)) ]) )


def s_approx_expsum(l1a,l2a,G,g,pn):
    S=0.0
    l1 = min(l1a,l2a)
    l2 = max(l1a,l2a)

    #print l1,l2,alpha,beta
    S1a  = H_expsum( g,g+l1, alpha,beta )  #     H(g+l1,a,b,c,d,f)-H(g   ,a,b,c,d,f)
    S1b  = -g*J_expsum(g,g+l1, alpha,beta)  #( J(g+l1,a,b,c,d,f)-J(g   ,a,b,c,d,f))

    S2 = l1*J_expsum(g+l1,g+l2,alpha,beta)  #( J(g+l2,a,b,c,d,f)-J(g+l1,a,b,c,d,f))

    S3a  =      -1.0*H_expsum(g+l2,g+l2+l1, alpha,beta)  #(H(g+l1+l2,a,b,c,d,f)-H(g+l2,a,b,c,d,f))
    S3b  = (l1+l2+g)*J_expsum(g+l2,g+l2+l1, alpha,beta) #(J(g+l1+l2,a,b,c,d,f)-J(g+l2,a,b,c,d,f))
    #print S1a,S1b,S2,S3a,S3b
    S= S1a+S1b+S2+S3a+S3b

    return S # (S,S1a,S1b,S2,S3a,S3b) #r+(1.0-pn)*(1.0 - (2.0/G)*S)
    

def s_approx(l1a,l2a,G,g,pn):
    S=0.0
    l1 = min(l1a,l2a)
    l2 = max(l1a,l2a)

#    a               = 11.2064 / 1000.0     #    +/- 0.2885       (2.574%)
#    b               = 0.844533             #    +/- 0.003699     (0.438%)
#    c               = 0.00290609 /1000.0   #    +/- 4.107e-05    (1.413%)
#    d               = 19448.3              #    +/- 176.7        (0.9085%)
#    f = 10000.0
        
#    r = pn*(1.0 - (l1/G) * (l2/G))
    
    S1a  = H( g,g+l1,   isd_a,isd_b,isd_c,isd_d,isd_f )  #     H(g+l1,a,b,c,d,f)-H(g   ,a,b,c,d,f)
    S1b  = -g*J(g,g+l1, isd_a,isd_b,isd_c,isd_d,isd_f )  #( J(g+l1,a,b,c,d,f)-J(g   ,a,b,c,d,f))

    S2 = l1*J(g+l1,g+l2,isd_a,isd_b,isd_c,isd_d,isd_f)  #( J(g+l2,a,b,c,d,f)-J(g+l1,a,b,c,d,f))

    S3a  =      -1.0*H(g+l2,g+l2+l1,isd_a,isd_b,isd_c,isd_d,isd_f)  #(H(g+l1+l2,a,b,c,d,f)-H(g+l2,a,b,c,d,f))
    S3b  = (l1+l2+g)*J(g+l2,g+l2+l1,isd_a,isd_b,isd_c,isd_d,isd_f) #(J(g+l1+l2,a,b,c,d,f)-J(g+l2,a,b,c,d,f))

    S=S1a+S1b+S2+S3a+S3b
    return S # (S,S1a,S1b,S2,S3a,S3b) #r+(1.0-pn)*(1.0 - (2.0/G)*S)

def p_not_a_hit_approx(l1a,l2a,G,g,pn):
    l1 = min(l1a,l2a)
    l2 = max(l1a,l2a)

    r = pn*(1.0 - (old_div(l1,G)) * (old_div(l2,G)))

    S = s_approx_expsum(l1a,l2a,G,g,pn)
#    S = s_exact(l1a,l2a,G,g,pn)

    return r+(1.0-pn)*(1.0 - (old_div(2.0,G))*S)

p_not_a_hit = p_not_a_hit_approx

#def ddg_p(x,a,b,c,d,f):
#    return ( (-a*b*(x**(-b-1.0)) - (a/f)*(x**(-b)))*math.exp(-x/f) - (c/d)*math.exp(-x/d))

#def d2dg2_p(x,a,b,c,d,f):
#    return ( (a*b*(b+1.0)*(x**(-b-2.0))+ (2*a*b/f)*(x**(-b-1.0)) + (a/(f*f))*(x**(-b)))*math.exp(-x/f) + (c/(d*d))*math.exp(-x/d))


#def H_expsum(x1,x2,alpha,beta):
#def J_expsum(x1,x2,alpha,beta):

def ddg_llr(l1,l2,o1,o2,G,pn,links,N,g,p0):
    n = (len(links))
    x=-2.0*(N-n)*(1.0-pn)/(G*p0)*( J_expsum(l2+g,l2+l1+g,alpha,beta)-J_expsum(g,l1+g,alpha,beta) ) 
    y=0.0
    for i in range(n):
        xx = fragment_size(l1,l2,o1,o2,links[i],g)
        p_d = insert_size_dist(xx)
        y += (1.0-pn)*ddg_p_expsum(xx,alpha,beta)/((old_div(pn,G))+(1.0-pn)*p_d)
    return(x+y)

#def J(x1,x2,a,b,c,d,f):
def d2dg2_ll(l1,l2,o1,o2,G,pn,links,N,g,p0):
    n = (len(links))
#    sys.stderr.write("gapsize: {}\n".format(g))
    x= 2.0*(N-n)*(1.0-pn)/(G*p0*p0)*( ( J_expsum(l2+g,l2+l1+g,alpha,beta)-J_expsum(g,l1+g,alpha,beta) )*ddg_p_expsum(g,alpha,beta) - p0*(insert_size_dist(g+l1+l2) - insert_size_dist(g+l2) - insert_size_dist(g+l1) + insert_size_dist(g)) )

    y=0.0
    z=0.0
    for i in range(n):
        xx = fragment_size(l1,l2,o1,o2,links[i],g)
        #p_d = max(minp,insert_size_dist(d))
        p_d = insert_size_dist(xx)
        pp_d =  ddg_p_expsum(  xx,alpha,beta)
        ppp_d = d2dg2_p_expsum(xx,alpha,beta)
#        print "xx",pp_d,ppp_d,((pn/G)+(1.0-pn)*p_d)
        y += -(((1.0-pn)*ddg_p_expsum(xx,alpha,beta)/((old_div(pn,G))+(1.0-pn)*p_d))**2.0)
##        y += -(((1.0-pn)*ddg_p(xx,a,b,c,d,f))**2.0)/(((pn/G)+(1.0-pn)*p_d)**2.0)
        z +=  old_div((((1.0-pn)*d2dg2_p_expsum(xx,alpha,beta))),((old_div(pn,G))+(1.0-pn)*p_d))
#    print "#2partial",g,x,y,z
#    print "zz",x,y,x,x+y+z
    return(x+y+z)

def ml_gap(l1,l2,o1,o2,G,pn,links,N,g0):
    gap=g0
    #    p0=
    last_gap=g0
    for i in range(100):
        p0 = p_not_a_hit(l1,l2,G,gap,pn)
        x1=  ddg_llr(l1,l2,o1,o2,G,pn,links,N,gap,p0) #1st derivative of the likelihood wrt. gap size
        x2= d2dg2_ll(l1,l2,o1,o2,G,pn,links,N,gap,p0) #2nd derivative of the likelihood wrt. gap size
        score=llr_v0(l1,l2,o1,o2,G,pn,links,N,gap,p0)
        print("\t".join(map(str,[ "#it",i,o1,o2,gap,score,gap-old_div(x1,2),x1,x2,old_div(x1,x2)])))
        gap = gap - old_div(x1,x2)
        if gap<0.0: 
            gap=10.0
#            break
        if gap>200000.0: 
            gap=200000.0
#            break
        if abs(old_div(x1,x2))<0.1: break
        if abs(gap-last_gap)<1.0: break
        last_gap=gap
        
    score=llr_v0(l1,l2,o1,o2,G,pn,links,N,gap,p0)
    return gap,score
    

#def dll_dg(l1,l2):
#    pass

#00:  l1-x + y
#01:  l1-x + l2-y
#10:  x+y
#11:  x+l2-y
def link_end_filter(links,o1,o2,l1,l2):
    if (o1,o2)==(0,0):
        links2 = [ (l[0]-max(0,(l1-args.endwindow)),l[1]                    ) for l in links if ((l1-l[0])<args.endwindow) and (l[1]      <args.endwindow) ] 
        return links2
    if (o1,o2)==(0,1):
        links2 = [ (l[0]-max(0,(l1-args.endwindow)),l[1]-max(0,(l2-args.endwindow))) for l in links if ((l1-l[0])<args.endwindow) and ((l2-l[1]) <args.endwindow) ] 
        return links2
    if (o1,o2)==(1,0):
        links2 = [ l for l in links if (l[0]     <args.endwindow) and (l[1]      <args.endwindow) ] 
        return links2
    if (o1,o2)==(1,1):
        links2 = [ (l[0],l[1]-max(0,(l2-args.endwindow))) for l in links if (l[0]     <args.endwindow) and ((l2-l[1]) <args.endwindow) ] 
        return links2

def llr_v0(l1,l2,o1,o2,G,pn,links,N,g,p0):
    n = len(links)
    if l1*l2 == 0:
        return 0.0

    #   Optimization:  this term is negligible
    #    p0=0.999999
    #print math.log(p0)
    if endsonly:
        if debug: print("########################## end only")
        links2 = link_end_filter(links,o1,o2,l1,l2)
        n=len(links2)
        l1 = min(l1,args.endwindow)
        l2 = min(l2,args.endwindow)
        p0 = p_not_a_hit(l1,l2,G,g,pn)

        q = 1.0 - old_div((2.0 * l1* l2 * pn),(G*G))
        
        llpsl=logp_lengths(l1,l2,o1,o2,G,pn,links,N,g)
            
#        print "#",g,p0,q,pn,llpsl, (N-n)*(math.log(p0)-math.log(q)) +n*(math.log(G)-math.log(pn)) + llpsl

        try: 
            x=( (N-n)*(math.log(p0)-math.log(q)) +n*(math.log(G)-math.log(pn)) + llpsl )

        except Exception as e:
            print(e)
            print(N-n,p0,q,n,G,pn,llpsl)
            print ( (N-n)*(math.log(p0)-math.log(q)) +n*(math.log(G)-math.log(pn)) + llpsl )  
            exit(0)
        return x
#        return (n*( (math.log(G*G /  (l1*l2*pn)) - math.log(p0))) + math.log( math.factorial(n)) + N *( math.log(p0) + pn * (l1/G) * (l2/G)) + logp_lengths(l1,l2,o1,o2,G,pn,links2,N))

    else:

        q = 1.0 - old_div((2.0 * l1* l2 * pn),(G*G))
        
        llpsl=logp_lengths(l1,l2,o1,o2,G,pn,links,N,g)
#        print "#",g,p0,q,pn,llpsl, (N-n)*(math.log(p0)-math.log(q)) +n*(math.log(G)-math.log(pn)) + llpsl

        try:
            x=( (N-n)*(math.log(p0)-math.log(q)) +n*(math.log(G)-math.log(pn)) + llpsl )
        except Exception as e:
            print(e)
#math domain error  100000000.0 -1108.0605932 0.999999999894 0 3000000000.0 0.3 0.0    
            print(N-n,p0,q,n,G,pn,llpsl)
            exit(0)
        return x
#        return (n*( (math.log(G*G /  (l1*l2*pn)) - math.log(p0))) + math.log( math.factorial(n)) + N *( math.log(p0) + pn * (l1/G) * (l2/G)) + logp_lengths(l1,l2,o1,o2,G,pn,links,N))

N=76710553 #1000000
pn=0.2
G=3.0e9
def process(lastt,links):
#    print "#",snam[lastt]
    for x in list(links.keys()):
        if not lastt == x:
            if len(links[x])>0:
                l1,l2 =0,0
                if mapp:
                    l1 = mapp.length[lastt] 
                    l2 = mapp.length[x] 
                else:
                    l1 = slen[lastt]
                    l2 = slen[x]

                p0 = p_not_a_hit(l1,l2,G,0,pn)
#                endlinks = [ l for l in links[x] if ((l[0]<150000 or l[0] >= l1-150000) and (l[1]<150000 or l[1] >= l2-150000))  ]
#                endlinks = [ l for l in links[x] if ((l[0]<args.endwindow or l[0] >= l1-args.endwindow) and (l[1]<args.endwindow or l[1] >= l2-args.endwindow))  ]
#                links2 = link_end_filter(links[x],o1,o2,l1,l2)

                if not mapp:
                    print("\t".join(map(str,[snam[lastt]           ,snam[x]           ,l1,l2,len(links[x]),math.log(p0)])), end=' ') #llr_v0( slen[lastt],slen[x],0,0,G,pn,links[x],N,0 ),links[x],len(links[x])
                else:
                    print("\t".join(map(str,[mapp.print_name(lastt),mapp.print_name(x),l1,l2,len(links[x]),math.log(p0)])), end=' ') #llr_v0( slen[lastt],slen[x],0,0,G,pn,links[x],N,0 ),links[x],len(links[x])

#                print "\t",llr_v0( l1,l2,0,0,G,pn,[],N,0 ,p0),
                scores = [llr_v0( l1,l2,0,0,G,pn,[],N,0 ,p0)]
                for (o1,o2) in ((0,0),(0,1),(1,0),(1,1)):
                    scores.append( llr_v0( l1,l2,o1,o2,G,pn,links[x],N,0,p0 ) )
#                    print "\t",llr_v0( l1,l2,o1,o2,G,pn,links[x],N,0,p0 ),
#                print snam[lastt],snam[x],x,llr_v0( slen[lastt],slen[x],0,0,G,pn,links[x],N,0 ),links[x],len(links[x])
                nll = [len(link_end_filter(links[x],o1,o2,l1,l2)) for o1,o2 in ((0,0),(0,1),(1,0),(1,1))]
                
                print("\t"+"\t".join(map(str,[max(scores)]+scores)),sum(nll),max(nll),"\t".join(map(str,nll)))  #,endlinks #[len(link_end_filter(links[x],o1,o2,l1,l2)) for o1,o2 in ((0,0),(0,1),(1,0),(1,1))],endlinks


class readpair(object):
    def __init__(self,a):
        self.tid = a.tid
        self.pos = a.pos
        self.rnext = a.rnext
        self.pnext = a.pnext
        self.is_duplicate = a.is_duplicate
        self.mapq = a.mapq
        self.tags = a.tags
    def map(self,mapp):
        x = mapp.map_coord(self.tid,self.pos)
        self.tid = x[0]
        self.pos = x[1]
        x = mapp.map_coord(self.rnext,self.pnext)
        self.rnext = x[0]
        self.pnext = x[1]
    def __repr__(self):
        return "\t".join(map(str,[self.tid,self.pos,self.rnext,self.pnext,self.mapq,self.is_duplicate,self.tags]))
    

if __name__=="__main__":
    import pysam
    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-s','--savegraph',default=False)
    parser.add_argument('-S','--restoregraph',default=False)
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-m','--minlength',default=1000,type=int)
    parser.add_argument('-M','--min_insert',default=1000,type=int)
    parser.add_argument('-e','--endsonly',default=False,action="store_true")
    parser.add_argument('-E','--endwindow',default=150000,type=int)
    parser.add_argument('-D','--dotfile',default=False)
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('-c','--cutoff',default=(old_div(200.0,1.0e13)),type=float)
    parser.add_argument('-q','--minquality',default=10.0,type=float)
    parser.add_argument('-L','--contiglist',default=False)
    parser.add_argument('--map_file',default=False)

    nodes={}

    args = parser.parse_args()
    
    endsonly=args.endsonly
    debug=args.debug

    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )


    cl = []
    if args.contiglist:
        f = open(args.contiglist)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            cl.append(l.strip())
    print("#",cl)
  
    sam=pysam.Samfile(args.bamfile)
    snam={}
    mapp = False
    if args.map_file:
        import pickle
        mapp = pickle.load(open(args.map_file))
#        h = sam.header
#        seqs=h['SQ']
#        slen=[ s['LN'] for s in seqs ]
#        snam = {}
#        for i in range(len(seqs)):
#            snam[i] = seqs[i]['SN']
#            print snam[i]
#        snam=dict([ s['SN'] for s in seqs ])
#        mapp=mapper.map_graph( snam, slen )

    if args.progress: log( "opened %s" % args.bamfile )

    h=sam.header
    seqs=h['SQ']

    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]
    G = float(sum(slen))

    if args.progress: log(  "built length and name map arrays" )   

    for i in range(len(seqs)):
        if slen[i]>args.minlength:
            nodes[i]=1

    n=0

    if mapp:
        if not args.contiglist:
            def next_alignment():
                for r in mapp.roots:
                    print("#r:", r, type(r))
                for r in mapp.roots:
                    originals = mapp.originals(r)
                    for o in originals:
                        #print "XXXXXXXX",r,o,snam[o]
                        for a in sam.fetch(o):
                            a = readpair(a)
                            a.map(mapp)
                            if a.tid == (r.s,r.x):
                                yield a


        else:
            roots = []
            for c in cl:
                r = mapp.find_node_by_name(c)
                roots.append(r)
            print("##",roots)
            def next_alignment():
                for r in roots:
                    originals = mapp.originals(r)
                    for o in originals:
                        #print "ZZZZZZZZZZZZ",r,o,snam[o]
                        for a in sam.fetch(snam[o]):
                            a = readpair(a)
                            a.map(mapp)

                            if a.tid == (r.s,r.x):

                                yield a
    else:
        if not args.contiglist:
            def next_alignment():
                for a in sam.fetch(until_eof=True):
                    yield a
        else:
            def next_alignment():
                for c in cl:
                    print("##",c)
                    if c == '' : continue
                    for a in sam.fetch(c):
                        yield a


    for a in next_alignment():
        n+=1

        if a.tid != lastt:
            process(lastt,links)
            links={}

        if a.mapq>=args.minquality:
            if a.tid>=0 and a.rnext>=0 and (not a.tid == a.rnext) and (not a.is_duplicate) : 
                d= dict(a.tags)                
                if int(d['xm'])>=args.minquality:
                    links[a.rnext] = links.get( a.rnext, []) + [(a.pos,a.pnext)] # .append( (a.pos,a.pnext) )
        lastt = a.tid
    process(lastt,links)
