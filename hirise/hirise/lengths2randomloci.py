#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
#parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-N','--number',default=100,type=int)
parser.add_argument('-w','--window',default=50000,type=int)
parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
args = parser.parse_args()

if args.seed != -1 :
     random.seed(args.seed)

if args.progress: print("#",args)

w=args.window

ll={}
totalL=0
while True:
     l=sys.stdin.readline()
     if not l: break
     id,leng = l.strip().split()
     leng=int(leng)
     ll[id]=leng
     totalL+=leng

seqs=list(ll.keys())

import random
for i in range(args.number):
     r=random.random()*totalL
     j=0
     rs=ll[seqs[0]]
     #print r,rs
     while not r<rs:
          j+=1
          rs+=ll[seqs[j]]
          #print r,rs,j,seqs[j]
     s=seqs[j]
     c=ll[seqs[j]]-(rs-r)
     print("{}:{}-{}".format(s,int(c-old_div(w,2)),int(c+old_div(w,2))))

#
#     for i in range(1+leng/args.length):
#          print id,i*args.length

