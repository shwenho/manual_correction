#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
from past.utils import old_div
import pysam
import networkx as nx
import sys

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-s','--savegraph',default=False)
    parser.add_argument('-S','--restoregraph',default=False)
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-m','--minlength',default=1000,type=int)
    parser.add_argument('-e','--endsonly',default=False,action="store_true")
    parser.add_argument('-E','--endwindow',default=50000,type=int)
    parser.add_argument('-D','--dotfile',default=False)
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('-c','--cutoff',default=(old_div(200.0,1.0e13)),type=float)

    g = nx.Graph()
    g2 = nx.Graph()

    nodes={}

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )
  
    sam=pysam.Samfile(args.bamfile)

    if args.progress: log( "opened %s" % args.bamfile )


    h=sam.header
    seqs=h['SQ']

    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]

    if args.progress: log(  "built length and name map arrays" )   


    for i in range(len(seqs)):
        if slen[i]>args.minlength:
            nodes[i]=1

    n=0
    ne=0
    hist={}
    nr=0

    if args.restoregraph:
        if args.progress: log(  "about to re-read saved graph %s" % args.restoregraph )   

        g=nx.read_weighted_edgelist(args.restoregraph,nodetype=int)

        if args.progress: log(  "done reading" )



    else:
        if args.progress: log("about to iterate over bamfile alignments")


        for aln in sam.fetch(until_eof=True):
            nr+=1
            if nr%200000 == 0:
                sys.stderr.write("%d\n"%nr)

            i,j = aln.tid, aln.rnext
    #            print aln.tlen,aln.pnext,seqs[aln.rnext]['SN'],seqs[aln.tid]['SN'],aln.qname,aln.rname,aln.inferred_length,aln.seq
    #            print aln.tid,aln.rnext
            if aln.tid != aln.rnext:
                if args.debug: print(aln.pos,aln.pnext,i,j,slen[i],slen[j],snam[i],snam[j],(slen[i]>args.minlength) and (slen[j]>args.minlength))


                if (slen[i]>args.minlength) and (slen[j]>args.minlength):
                        if (not args.endsonly):
                            if g.has_edge(i,j):
                                g[i][j]['weight']+=1
                            else:
                                g.add_edge(i,j,weight=1)    
                            ne+=1
                        else:
                            ii=i
                            jj=j
                            if slen[i] > 2*args.endwindow:
                                g.add_edge(i,-i,weight = 10000)
                                if aln.pos < args.endwindow:
                                    ii = -i
                                if aln.pos > slen[i] - args.endwindow:
                                    ii = i
                                else:
                                    ii = False

                            if slen[j] > 2*args.endwindow:
                                g.add_edge(j,-j,weight=10000)
                                if aln.pos < args.endwindow:
                                    jj = -j
                                if aln.pos > slen[j] - args.endwindow:
                                    jj = j
                                else:
                                    jj = False
                            if ii and jj:
                                if g.has_edge(ii,jj):
                                    g[ii][jj]['weight']+=1
                                else:
                                    g.add_edge(ii,jj,weight=1)    
                                ne+=1

    #                        or (( aln.pos < args.endwindow   or slen[i]-aln.pos < args.endwindow ) and ( aln.pnext < args.endwindow or slen[j]-aln.pnext < args.endwindow ) ):


            if args.insertHist and aln.rnext == aln.tid:
               hist[aln.tlen] = hist.get(aln.tlen,0)+1
            n+=1
            if args.head and n>args.head:
                break

    if args.savegraph:
        if args.debug:
            sys.stderr.write( "nodes: %d\n" %(  len(g.nodes())) )
            sys.stderr.write( "edges: %d\n" %(  len(g.edges())) )
            sys.stderr.write( "edge inserts: %d\n" %(  ne) )

        if args.progress: log( "write edge list to %s\n" % (args.savegraph))
        nx.write_weighted_edgelist(g,args.savegraph)
        
    if args.debug:
        print("nodes:", len(g.nodes()))
        print("edges:", len(g.edges()))
        print("edge_inserts:", ne)

    for i,j in g.edges():
    #s = float( g[i][j]['weight'])/(seqs[i]['LN']*seqs[j]['LN'])
        s = old_div(float( g[i][j]['weight']),(min((slen[i],2*args.endwindow))*min((slen[j],2*args.endwindow))))
        if args.debug:
            print("sc:",i,j,g[i][j]['weight'],s)
        #if s>args.cutoff:
        if  g[i][j]['weight'] > 2:
            g2.add_edge(i,j,weight=old_div(g[i][j]['weight'],2))


    degree_dict = {}
    total_degree=0.0
    nnodes=0
    for n in list(nodes.keys()):
        nnodes+=1
        if g2.has_node(n):
            d = g2.degree(n)
            total_degree += d
            degree_dict[n]=d
    mean_degree = old_div(total_degree, nnodes)

    vs = 0.0
    for n,d in list(degree_dict.items()):
        vs += ( mean_degree - d )**2.0
    sdev_degree = (old_div(vs, nnodes))**0.5

    degree_limit = mean_degree+sdev_degree
    log("degree stats: %f pm %f" % (mean_degree,sdev_degree))
    log("set max degree to %f" % (degree_limit))
    
    log("graph has %d nodes." % ((g2.number_of_nodes())))
    g2.remove_nodes_from( [ n for n,d in list(degree_dict.items()) if d > degree_limit] )
    log("graph has %d nodes." % ((g2.number_of_nodes())))
    
    def node_neighborhood_stats(n,g,rang=5):
        nnn = nx.single_source_shortest_path_length(g,n,rang)
        return ( [ len( [ i for i,d in list(nnn.items()) if d <= x ] )-1 for x in range(1,rang) ] )

    def node_neighborhoods(n,g,rang=5):
        
        nnn = nx.single_source_shortest_path_length(g,n,rang)

        result=[]
        for r in range(rang):
            result.append([])

        for i,d in list(nnn.items()):
            for j in range(d-1,rang):
                result[j].append(i)

        return result
#        return ( [ len( [ i for i,d in nnn.items() if d <= x ] )-1 for x in range(1,rang) ] )


    for n in list(nodes.keys()):
        if g2.has_node(n):
            if g2.degree(n)>0:
                nnn = node_neighborhoods(n,g2,5)
                #c=0.0
                #try:
                c = nx.clustering(g2,n)
                #except Exception as e:
                #    print e
#                print "stats:\t%d\t%d\t" % (n, g2.degree(n)) + "\t".join(map(str,node_neighborhood_stats(n,g2)))
#                for nn in nnn:
#                    print nx.average_clustering(g2,nn),nn,nx.transitivity(g2.subgraph(nn)),g2.edges(nn)
                print("stats:\t%d\t%f\t" % (n,c) + "\t".join(map(str,[ len(x) for x in nnn ])) +"\t" + "\t".join(map(str,[  nx.transitivity(g2.subgraph(nnn[i])) for i in range(5)  ])))

    if args.dotfile:
        f=open(args.dotfile,"wt")
        f.write("graph G {\n")

        for c in nx.connected_components(g2):
            if len(c)>2:
                for e in g2.edges(c):
                    if g2.degree(e[0]) < 5 and g2.degree(e[1])<5:
                        f.write("\t\"%s\" -- \"%s\" ;\n" % (e[0],e[1]))
        f.write("}\n")
        f.close()

#    lengths = list(hist.keys())
#    lengths.sort()
#    for l in lengths:
#        print "%d\t%d" %(l,hist[l])
