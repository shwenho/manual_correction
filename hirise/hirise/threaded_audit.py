#!/usr/bin/env python3

import subprocess
import argh
import hirise
import os

@argh.arg("args", nargs="+")
def main(args):
    hirise_dir = os.path.dirname(hirise.__file__) 
    audit = os.path.join(hirise_dir, "threaded_audit")
    cmd = [audit] + args
    subprocess.call(cmd)

if __name__ == "__main__":
    argh.dispatch_command(main)
