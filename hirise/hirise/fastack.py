#!/usr/bin/env python3
import sys
import argparse
import pysam

tr = str.maketrans("ACTGactg","TGACtgac")
def rc(s):
    r=str(s[::-1])
    rc =  r.translate(tr)
    return(rc)

if __name__=="__main__":
     """Read .table.txt file from std in and make sure the implied pairs of sequences on each line do match up. """
     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-F','--fasta',default=False,help="Fasta file.")
     parser.add_argument('-S','--scaffolds',default=False,help="Scaffold fasta file.")

     args = parser.parse_args()
     #     print(args)

     scaffolds = pysam.Fastafile(args.scaffolds)
     contigs =   pysam.Fastafile(args.fasta)

     for line in sys.stdin:
          if line[0]=="#": continue
          c = line.strip().split()
          sc = c[0]
          co = c[1]
          a,b = int(c[2]),int(c[3])
          x,y = int(c[5]),int(c[6])
          
          s_seq = scaffolds.fetch(reference=sc,start=x,end=y) #  str(scaffolds[sc][x:y] )
          c_seq = contigs.fetch(  reference=co,start=a,end=b) # str(contigs[co][a:b] )

          if (c[4]=="-"):
               c_seq = rc(c_seq)
          
          if not str(s_seq.lower())==str(c_seq.lower()): 
               print(line.strip(), b-a-(y-x),sep="\t" )
               print( "scaffold\t", " "*1, s_seq )
               print( "contig  \t", " ", c_seq )
#               raise Exception 


