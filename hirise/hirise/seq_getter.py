#!/usr/bin/env python3
from __future__ import print_function
from builtins import object
import sys
#from string import str.maketrans


tr = str.maketrans("ACTGactg","TGACtgac")

def rc(s):
    r=s[::-1]
    rc = t.translate(tr)
    return(rc)

class seqs(object):
    def __init__(self,filename):
        self.f = open(filename)
        self.offsets={}
        name=""
        while True:
            x=self.f.tell()
            l=self.f.readline()
            if not l:
                break
            if l[0]==">":
                c=l[1:].strip().split()
                name = c[0]
                self.offsets[name]=x
        
    def get(self,name):
        if name not in self.offsets: 
            print("unknown name")
            exit(0)
        self.f.seek( self.offsets[name] )
        hline = self.f.readline()
        if not hline[0]==">": 
            print("expected header line")
            exit(0)
        s = ""
        l=self.f.readline()
        while l and not l[0]==">" :
            s = s+ l.strip()
            l=self.f.readline()
        return s

    def get_range(self,name,a,b):
        s=self.get(name)
        if b<a:
            return rc(s[b-1:a])
        return s[a-1:b]
