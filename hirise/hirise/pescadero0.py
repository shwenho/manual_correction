#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
#!/usr/bin/env python3
import glob
import sys
import bisect
import re
#import networkx as nx
#import numpy as np
#import hirise.chicago_edge_scores as ces
import math
import random
import subprocess
import shlex

def tab(x):
    return "\t".join(map(str,x))

#from string import str.maketrans


#tr = str.maketrans("ACTGactg","TGACtgac")


tm_cache={}

def tm(s):
    if s in tm_cache:
        return tm_cache[s]
    cmd = "oligotm {}".format(s)
    #print cmd
    output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
    #print output
    tm_cache[s]=float(output)
    return tm_cache[s]

#from string import str.maketrans
tr = str.maketrans("ACTGactg","TGACtgac")

def reverse(s):
    return s[::-1]

def rc(s):
    r=s[::-1]
    rc = r.translate(tr)
    return(rc)


def longest_homo(s):
    return max(list(map(len,re.findall('A+|C+|G+|T+',s))))

 
if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-l','--loci')
#    parser.add_argument('-L','--lengths')
#    parser.add_argument('-p','--peaks')
#    parser.add_argument('-P','--pairs')
    parser.add_argument('-d','--debug',action="store_true")
#    parser.add_argument('-M','--set_insert_size_dist_fit_params')
    args = parser.parse_args()
 
#
#               ===---------===---------===----------= 
#               01234567890123456789012345678901234567
    template = "NNNNGGCCNNNNNNNNNAATTNNNNNNNNAAGCTTNNT"

#    forward_5prime_overhang= {1: "AGA", 2: "GTG", 3: "TAC"} 

#               ===---------===---------===----------= 
#               01234567890123456789012345678901234567
    template = "AGANGGCCNNNNGTGNNGCGCNNNTACNNNCATGNNNT"



#    forward_5prime_overhang= {1: "AGA", 2: "GTG", 3: "TAC"} 

#               ----------===---------===----------= 
#               012345678901234567890123456789012345
    template = "NNNNNGGCCNGTGNNGCGCNNNTACNNNCATGNNNT"

    restriction_sites=["CATG","GCGC","GGCC","GATC"]

    def n2r(s):
        r=""
        for i in range(len(s)):
            if s[i]=="N":
                r+=random.choice(["A","C","T","G","G","C","G","C","G","C","G","C","G","C","G","C","G","C","G","C"])
            else:
                r+=s[i]
        return r

    r1f = lambda x:    x[-14      :        ]
    r2f = lambda x:    x[-14-12   :-14     ]
    r3f = lambda x:    x[ :10 ]

    bc1 = lambda x:    x[-11      :-1 ]
    bc2 = lambda x:    x[-11-12   :-11-12+9 ]
    bc3 = lambda x:    x[         :10 ]


    r1r = lambda x: rc(x[-11      :-1    ])
    r2r = lambda x: rc(x[-11-12   :-11   ])
    r3r = lambda x: rc(x[         : 13])


    print(template)
    s = n2r(template)
    print(s)
    
    for rs in restriction_sites:
        m=re.findall("({})".format(rs),s)
        print(rs,m)

    print(s, bc1(s),bc2(s),bc3(s),  tm(bc1(s)),tm(bc2(s)),tm(bc3(s)))

    oligos=[r1f(s),r2f(s),r3f(s),r1r(s),r2r(s),r3r(s)]
    print(oligos)

    print("layout:")
    print("forward: 5' - {}    {}    {} ->".format(r3f(s), r2f(s), r1f(s)))
    print("reverse: 3'<- {}    {}    {} - ".format(r3r(s)[::-1], r2r(s)[::-1], r1r(s)[::-1]))

    print("round 1 forward:",r1f(s))
    print("round 2 forward:",r2f(s))
    print("round 3 forward:",r3f(s))
    print("round 1 reverse:",r1r(s))
    print("round 2 reverse:",r2r(s))
    print("round 3 reverse:",r3r(s))

    def test_pair(s1,s2):
        matches=[]
        for i in range(1,len(s1)):
            prefix1 = s1[:i]
            prefix2 = s2[:i]
            if rc(prefix1)==prefix2:
                print(s1,s2,i,prefix1,prefix2,"prefix",tm(prefix1))
                
                matches.append(tm(prefix1))

        for i in range(len(s1)-1):
            suffix1 = s1[i:]
            suffix2 = s2[i:]
            if rc(suffix1)==suffix2:
                print(s1,s2,i,suffix1,suffix2,"suffix",tm(suffix1))
                matches.append(tm(suffix1))

#        if not matches:
#            return False
        matches.sort(reverse=True)
        return matches

    stable={}
    for i in range(len(oligos)):
        for j in range(i,len(oligos)):
            print(i,j,oligos[i],oligos[j])
            x=test_pair(oligos[i],oligos[j])
            if x and x[0]>15.0: 
                stable[i,j]=x[0]
    print(stable)
    
    for k in list(stable.keys()):
        if k not in [(1,4),(2,5)]:
            print("######## BAD")
    print(longest_homo(s))
