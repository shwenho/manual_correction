#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import str
from past.utils import old_div
import sys
import networkx as nx
import hirise.chicago_edge_scores as ces

def is_shaved_tail(G,shave_round,shaved_degree,shave_limit):
    leaves=[]
    r={}
    for n in G.nodes():
        if shave_round.get(n,0)>shave_limit and shaved_degree.get(n,0)==1:
            leaves.append(n)
    for l in leaves:
        q=[l]
        while len(q)>0:
            n=q.pop()
            r[n]=True
            for nn in G.neighbors(n):
                if shave_round.get(nn,0)>shave_limit and shaved_degree.get(nn,0)<=2 and (not nn in q) and (not nn in r ):
                    q.append(nn)
    return r


def distance_to_nearest_branch(G,shave_round,shave_limit,trim_degree):
    branchpoints=[]
    r={}

    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if shave_round.get(n,0)>shave_limit and trim_degree.get(n,0)>2:
            branchpoints.append(n)
#    print leaves

    round=1
#    print "#",leaves
    boundary=list(branchpoints)
    next=[]
    done=[]
    #r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next) :
                    next.append(nn)
#                    r[nn]=round
        for b in next: 
            r[b] = round
#            if len( [nn for nn in G.neighbors(b) if not r.has_key(nn)] )==1: r[b]=round
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if n in r: boundary.append(n)
#        next=[]

#    for n in G.nodes():
#        if not r.has_key(n): r[n]=round
    return r


def distance_to_nearest_leaf(G,shave_round,shave_limit,trim_degree):
    leaves=[]
    r={}
    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if shave_round.get(n,0)<=shave_limit :
            r[n]=0
    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if trim_degree.get(n,0)==1 and shave_round.get(n,0)>shave_limit:
            leaves.append(n)
#    print leaves

    round=1
#    print "#",leaves
    boundary=list(leaves)
    next=[]
    done=[]
#    r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next):
                    next.append(nn)
#                    r[nn]=round
        for b in next: 
            r[b] = round
#            if len( [nn for nn in G.neighbors(b) if not r.has_key(nn)] )==1: r[b]=round
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if n in r: boundary.append(n)
#        next=[]
#    for n in G.nodes():
#        if not r.has_key(n): r[n]=round
    return r
    

def shave_round(G):
    leaves=[]
    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if G.degree(n)==1:
            leaves.append(n)
#    print leaves

    round=1
#    print "#",leaves
    boundary=list(leaves)
    next=[]
    done=[]
    r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next):
                    next.append(nn)
#                    r[nn]=round
        nr={}
        for b in next: 
            if len( [nn for nn in G.neighbors(b) if nn not in r] )==1: nr[b]=round
        r.update(nr)
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if n in r: boundary.append(n)
#        next=[]
    for n in G.nodes():
        if n not in r: r[n]=round
    return r

def log(x):
    sys.stderr.write(x+"\n")


LOCAL_BRIDGE=1
CUT_ME=2

edge_tags={}

def add_tag(s1,s2,tag):
    if s1<s2:
        ot = edge_tags.get((s1,s2),set())
        ot.add(tag)
        edge_tags[s1,s2] = ot
    else:
        ot = edge_tags.get((s2,s1),set())
        ot.add(tag)
        edge_tags[s2,s1] = ot


def get_tags(s1,s2):
    if s1<s2:
        ot = edge_tags.get((s1,s2),set())
        return tuple(ot)
    else:
        ot = edge_tags.get((s2,s1),set())
        return tuple(ot)


edge_color_setting="hair"
def edge_tag_to_style(tags,setting=edge_color_setting):
    if setting == "hair":
        style=""
        if "hair" in tags:
            style= "color=red"
        elif "longHair" in tags:
            style= "color=orange"
        elif "H" in tags:
            style= "color=blue"
        elif "Y" in tags:
            style= "color=goldenrod"
        elif "nearY" in tags:
            style= "color=goldenrod4"
        elif "bigH" in tags:
            style= "color=green"
        if "promisc" in tags:
            style += " style=dashed"
        return style
            
def printdot(g,gg0,c,n,ll,bh,annot,trim_level={},post_trim_degree={},tag="bad",yDist={},leafDist={},edgeTags=edge_tags):
#def printdot(g,c,n,ll,bh,annot,tag="bad"):
#    print ll

    import colorsys

    chromosomes={}
    chr_mins={}
    chr_maxs={}
    if bh:
        sb=[]
        for cc in c:
            bhi = bh.get(cc,[0,0,0,0,0,0])
            sb.append( ( bhi[1],old_div((float(bhi[3])+float(bhi[4])),2.0),bhi[2],cc ) )
            chromosomes[bhi[1]]=1
            if chr_mins.get(bhi[1],5.0e9)>min( float(bhi[3]), float(bhi[4]) ):  chr_mins[bhi[1]]=min( float(bhi[3]), float(bhi[4]) )
            if chr_maxs.get(bhi[1],-1.0) <max( float(bhi[3]), float(bhi[4]) ):  chr_maxs[bhi[1]]=max( float(bhi[3]), float(bhi[4]) )
        sb.sort()

# {'19': 46194830.0, '18': 59221558.0, '8': 96645227.0, '4': 18548230.0, 'X': 102465955.0}
# {}

    print("#",chr_mins)
    print("#",chr_maxs)

    nchrs=len(list(chromosomes.keys()))
    i=0
    chr_hue={}
    for ch in list(chromosomes.keys()):
        chr_hue[ch] = old_div(float(i),nchrs)
        i+=1

    gg = nx.subgraph(g,c)
    f=open("%s-%d.txt" % (tag,n), "wt")
    nn=1
    lab0={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]
#        lab0[d]=lab0.get(d,nn)
        lab0[d]=lab0.get( d, float(ll.get(d,0)))
        #print x,d,p,lab0[d]
        nn+=1
    lab={}
    node_fill={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]

        bhi = bh.get(x,False)
        if bhi:
            lab[x]="{:.1f} {}{}\\n{:.2f}-{:.2f}\\n{}".format( old_div(lab0.get(d,0.0),1000), bhi[1],bhi[2],old_div(float(bhi[3]),1.0e6),old_div(float(bhi[4]),1.0e6), x)
#            lab[x]="{:.1f} {}{}\\n{:.2f}-{:.2f}\\n{} {} {} {}".format( lab0.get(d,0.0)/1000, bhi[1],bhi[2],float(bhi[3])/1.0e6,float(bhi[4])/1.0e6,leafDist.get(x,""),yDist.get(x,""), trim_level[x], post_trim_degree.get(x,"") )
            if ( chr_maxs.get(bhi[1], (1.0+float(bhi[3])+float(bhi[4])) ) ) ==0.0: #/2.0)-chr_mins.get(bhi[1],0.0))==0.0: 
                print("wtf?",x,bhi,bhi[1],( chr_maxs.get(bhi[1], (1.0+float(bhi[3])+float(bhi[4])) ) ))
            rgb=(0,0,0)
            try:
                rgb=colorsys.hls_to_rgb( chr_hue[bhi[1]], 0.5, old_div((old_div((float(bhi[3])+float(bhi[4])),2.0) - chr_mins.get(bhi[1],0)),(chr_maxs.get(bhi[1],old_div((1.0+float(bhi[3])+float(bhi[4])),2.0))-chr_mins.get(bhi[1],0.0)))  )
            except Exception as e:
                print(e)
            node_fill[x]= '#%02x%02x%02x' % (255.0*rgb[0], 255.0*rgb[1], 255.0*rgb[2] ) #"#{}{}{}".format()
        else:
            lab[x]="{:.1f}\\n{}".format(old_div(lab0.get(d,0.0),1000),str(x))
            node_fill[x]="white"
#        lab[x]="{} {}".format( trim_level.get(x,"?"), post_trim_degree.get(x,"?") )

    f.write( "graph G {\n")
#    f.write( "node [margin=0 fontcolor=blue fontsize=32 width=0.5 shape=circle style=filled]")
    f.write( "node [margin=0 fontsize=6 shape=box];\n")
    f.write( "edge [ fontsize=6 ];\n")

    for x in list(lab.keys()):
        f.write( "{0} [label=\"{1}\" fillcolor=\"{2}\" style=\"filled\" color=\"{2}\"] ; \n".format(x,lab[x],node_fill[x]) )

    if bh:
        last=False
        lastx=0.0
        lastc=0
        for c in sb:
            if last and c[0]==last and (c[1]-lastx)<1000000:
                last_bhi = bh.get(lastc,False)
                this_bhi = bh.get(c[-1],False)
                blast_label=str(last_bhi) + str(this_bhi)
                if this_bhi and last_bhi :
                    aa = tuple(last_bhi[1:5])
                    bb = tuple(this_bhi[1:5])
                    qd = qdist(aa,bb)
                    blast_label = "{}".format(qd)
                    
                if gg0.has_edge(lastc,c[-1]) and not t.has_edge(lastc,c[-1]):
                    f.write("\t \"{}\" -- \"{}\" [weight=2 style=dotted label=\"{} {}\" fontcolor=red] ;\n".format(lastc,c[-1],blast_label,int(abs(gg0[lastc][c[-1]]['weight']))))
                else:
                    f.write("\t \"{}\" -- \"{}\" [weight=2 style=dotted label=\"{}\" fontcolor=blue] ;\n".format(lastc,c[-1],blast_label))
            last=c[0]
            lastx=c[1]
            lastc=c[-1]

    for e in gg.edges():
#        f.write( "\t\"%s\" -- \"%s\";\n" % (lab[e[0]],lab[e[1]]) ) #,gg[e[0]][e[1]]['weight'])
#        color="black"
#        if annot.get(e,0)&LOCAL_BRIDGE : color="red"
#        if annot.get(e,0)&CUT_ME       : color="yellow"
        f.write( "\t \"%s\" -- \"%s\" [label=\"%d\" weight=1 %s];\n" % ( e[0],e[1],int(abs(gg[e[0]][e[1]]['weight'])),edge_tag_to_style( get_tags(e[0],e[1]) ) ))

    f.write( "}\n")


def independent_path(G,a,b,k,t):
    q=[a]
    l={}
    l[a]=0
    r=[]
    while len(q)>0:
#        print a,b,G[a][b],q,r
        n=q.pop(0)
        r.append(n)
        for nn in G.neighbors(n):
            if (n==a and nn==b) or (n==b and nn==a): continue
            if G[n][nn]['weight']>-t: continue
            if nn==b: return True
#            print q,[l[i] for i in q]
            l[nn] = min(l.get(nn,10000), l[n]+1)
            if (not nn in q+r) and (l[nn]<=k):
                q.append(nn)
    return False
    
def annotate_edges(t,G,node_list):
    an={}
#    nn= len(list(t.edges()))
#    i=0.0
    for a,b in t.edges():

        if not independent_path(G,a,b,4,2): 
            an[a,b]="local_bridge"
            an[b,a]="local_bridge"
            
    return an


def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False

def qdist(x,y):
    if (not x) or (not y): return (-1)
    if x[0]==y[0]:
        x,y,w,z = list(map(int,[x[2],x[3],y[2],y[3]]))
        ol = pairs_overlap((x,y),(w,z))
        if ol:
            return(-1*min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
        else:
            return(min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
    else:
        return(1.0e12)

def update_scaffold_coords(coord_sys,n,g,sn):
    x=0
    q=[n]
    seen={}
    cstrand={}
    last=False
    while len(q)>0:
        m=q.pop(0)
        contig=m[:-2]
        if last:
            try:
                x+=g[last][m]['length']
            except Exception as e:
                print("wtf?",last,m,x,n)
                print(math.log(-1.0))
        last=m

        #if (not m in end_distance) or end_distance[m]>x: end_distance[m]=x
        strand=1
        if not contig in seen:
            if m[-1:]=="5":
                strand=1
                cstrand[contig]=1
            elif m[-1:]=="3":
                strand=-1
                cstrand[contig]=-1
            else:
                print("#wtf?")
                raise Exception('not plus or minus?')
        coord_sys[m]=x
        
        if contig in seen:
#            coord_sys[contig]={ "strand":strand, "start":min( coord_sys[contig+".5"], coord_sys[contig+".3"] )  , "stop":max( coord_sys[contig+".5"], coord_sys[contig+".3"] ) }
            coord_sys[contig]=[coord_sys[contig+".5"],cstrand[contig],sn]
#            print "layout:",m,contig,seen.get(contig),coord_sys[contig]

        seen[m]=True
        seen[contig]=True
        if len(g.neighbors(m))>2:
            print("topology error:",m,list(g.neighbors(m)))
            print(math.log(-1.0))
        for l in g.neighbors(m):
            if not l in seen:
                q.append(l)

#    return x

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-t','--threshold',default=0.0 ,  type=float)
    parser.add_argument('-z','--mergeT',default=3.0 ,  type=float)

    parser.add_argument('-H','--head',default=False,type=int)
    parser.add_argument('-D','--savetreedots',default=False,action='store_true')
    parser.add_argument('-d','--debug',default=False,action='store_true')
#    parser.add_argument('-I','--nointerc',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-M','--maxdegree',default=False,type=int)
    parser.add_argument('-m','--minlength',default=500,type=int)

    parser.add_argument('-S','--silent',default=False,action='store_true')
    parser.add_argument('-K','--cutPromisc',default=False,action='store_true')
    parser.add_argument('-J','--logH',default=False,action='store_true')
    parser.add_argument('-T','--logTags',default=False,action='store_true')
    parser.add_argument('-C','--cheat',default=False,action='store_true')
    parser.add_argument('-B','--blacklist')

    parser.add_argument('-b','--besthits')
    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')
    parser.add_argument('-L','--maxLength',type=float,default=150000.0)
    parser.add_argument('-P','--promisc',type=float,default=0.023)
    parser.add_argument('-o','--dotLabel',default="bad")

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )
    print("#"+str(args))

    G=nx.Graph()
    SG=nx.Graph()
    OG=nx.Graph()

    # 1 Scaffold29756_1.5 Scaffold91187_1.3 1000.0 200936.0
    f=open(args.edgefile)
    while True:
        l=f.readline()
        if not l: break
        if l[0]=="#":
            c=l.strip().split()
            if c[2][:-2]==c[3][:-2]:
                OG.add_edge(c[2],c[3],length=float(c[4]),contig=True)
            else:
                OG.add_edge(c[2],c[3],length=float(c[4]),contig=False)
    f.close()
    coord_sys={}
    sn=1
    for sg in nx.connected_component_subgraphs(OG):
        #coord_sys={}
        ends=[]
        bh_stats={}
        for n in sg.nodes():
            if sg.degree(n)==1:
                ends.append(n)

        if len(ends)==0: 
            print("why no ends?", sn) 
            continue
        update_scaffold_coords(coord_sys,ends[0],OG,sn)
        sn+=1
        #for en in coord_sys.keys():
        #    print en,coord_sys[en]
#        maxx=update_end_distance(end_distance,ends[0],sg)

    ll={}
    if args.lengths:
        f = open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            l = int(c[1])
            ll[c[0]]=int(c[1])
            if l>= args.minlength:
                G.add_node(c[0])
                SG.add_node(c[0])
        f.close()
    if args.progress: print("#Done reading lengths")

    besthit={}
    if args.besthits:
#        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
    #            print c[1],besthit[c[1]]
            f.close()
    if args.progress: print("#Done reading besthits")

    links={}
    if True or args.links:
        f=sys.stdin #open(args.links)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            c = l.strip().split()
            if len(c)<5: continue
            
            pp = eval(" ".join(c[5:]))
            links[c[0],c[1]]= pp
            links[c[1],c[0]]= [ (p[1],p[0]) for p in pp ] 
            G.add_edge(c[0],c[1],{"weight": len(pp)})
  
        f.close()

    tag_tallies={}
    bad_tag_tallies={}
    strx={"+":0, "-":1}
    strings = []
    ccn=0
    component={}
    ncontigs={}
#    for c in nx.connected_components(SG):
    for c in nx.connected_components(OG):
        ccn+=1
#        print "#c:",ccn,len(c)
        for cc in c: 
            component[cc]=ccn
            component[cc[:-2]]=ccn
            
        ncontigs[ccn]=len(c)

    all_hits=[]

    edges_to_add=[]
    for c in nx.connected_components(OG):        
#    for c in nx.connected_components(SG):        
        if len(c)<=8:
            ecc={}
            ecn={}
            lcn={}
            pxs={}
            ll0=0
            nc2c={}
            contigs={}
            anchors=[]
            for cc1 in c:
                c1=cc1[:-2]
                if c1 in contigs: continue
                contigs[c1]=True
                ll0+=ll[c1]
                for c2 in G.neighbors(c1):
                    if component.get(c1)==component.get(c2) or not c2 in component: continue
                    ecc[component[c2]] = ecc.get(component[c2],0.0) - int(G[c1][c2]['weight']) # tally the total number
                    ecn[component[c2]] = ecn.get(component[c2],0  ) + 1
                    lcn[component[c2]] = lcn.get(component[c2],0  ) + ll[c2]
                    nc2c[component[c2]] = nc2c.get(component[c2],[])+[c2]
                    for x,y in links[c1,c2]:
                        z=coord_sys[c2]
#                        print "\t".join(map(str,[c1,c2,component[c1],component[c2],x,y,z,z[0]+z[1]*y,c2]))
                        anchors.append(( component[c2],z[0]+z[1]*y,c2 ))
            anchors.sort()
            last=(-1,0,"")
            nr=1
            hits=[]
            hit_cutoff=2
            for a in anchors:
                t=""
                if a[0]==last[0] and (a[1]-last[1])<400000:
                    t="#"
                    nr+=1
                else:
                    if nr>=hit_cutoff:
                        t="XXXXX "+str((c1,last[2],nr))
                        hits.append([ component[c1],last[0],nr,c1,last[2] ])
                    nr=1
                print("\t".join(map(str,a))+"\t"+t)
                last=a
            if nr>=hit_cutoff:
                print("XXXXX"+str((c1,last[2],nr)))
                hits.append([ component[c1],last[0],nr,c1,last[2] ])
            for hit in hits:
                print("\t".join(map(str,["hit:"]+hit)))
                all_hits.append(hit)
            ocn=list(ecc.keys())
            #ocn = [ x for x in ocn if ecc[x] > 1.0 ]
            ocn.sort(key=lambda x:ecc[x],reverse=True)
            ocn = ocn[:10]
            if len(ocn)<=10:
                ocn += [-1]*(10-len(ocn))
            print("\t".join(map(str,["s:",component[c[0]],len(c),ll0]+  [ ecc.get(x,0) for x in ocn ]+ [ ecn.get(x,0) for x in ocn ] + ocn + [ncontigs.get(x,0) for x in ocn ]+[lcn.get(x,0) for x in ocn ] + [1.0e6*float(ecc.get(x,0))/(1.0+ll0*lcn.get(x,0)) for x in ocn] )))
            for x in ocn:
                if ecc.get(x,0)>=args.mergeT:
                   edges_to_add.append([c[0],nc2c[x][0]])
            #ecc = total number of links
            #ecn = number of contigs with those links
            #ocn: which are the scaffolds
            #ncontigs: number of contigs total in the scaffold
            #
        
    for c1,c2 in edges_to_add:
        OG.add_edge(c1,c2)

    ccn=0
    for c in nx.connected_components(OG):
        ccn+=1
        print("2:",ccn,len(c))
