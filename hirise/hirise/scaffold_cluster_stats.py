#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-L','--maxlength',default=20000.0,type=float)
    parser.add_argument('-o','--endlog')
    parser.add_argument('-b','--besthits')
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    besthit={}
    if args.besthits:
        f = open(args.besthits)
        while True:
            l = f.readline()
            if not l: break

            if not l[:5]=="best:": continue
            c=l.strip().split()
            besthit[c[1]]=c[2:]
#            print c[1],besthit[c[1]]
        f.close()

    ends={}
    if args.endlog:
        f = open(args.endlog)
        while True:
            l = f.readline()
            if not l: break
            c=l.strip().split()
            ends[c[0]] = [c[1],c[2]]
        f.close()

    while True:
        l= sys.stdin.readline()
        if not l: break
        c = l.strip().split()
        print(c)
        for cc in c:
            e1,e2=ends.get(cc,("-1.3","-1.5"))
            print(cc, [e1,e2],besthit.get(e1[:-2]),besthit.get(e2[:-2]))


