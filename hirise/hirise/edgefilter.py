#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
#parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-b','--blacklist',default=False)

args = parser.parse_args()
if args.progress: print("#",args)

bl={}
f=open(args.blacklist)
while True:
     l=f.readline()
     if not l: break
     c=l.strip().split()
     bl[c[0]]=1

while True:
     l=sys.stdin.readline()
     if not l: break
     if l[0]=="#": 
          print() 
     else:
          c=l.strip().split()
          if c[0] in bl: continue
          if c[1] in bl: continue
          print(l.strip())

