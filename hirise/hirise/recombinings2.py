#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div



import sys
import networkx as nx

n_ends=10
ends = list(range(n_ends))

max_n_breaks=3

originals=[]
target_breaks=0

breakers =  [ nn for nn in range(1,n_ends) if not (nn%6 == 5 or nn%6==0) and not nn==n_ends-1 ]
end={}


def n_free(l):
    n=0
    for r in l:
        if len(r)==1:
            n+=1
    return n
            
def n_breaks(l,n_ends,end,originals):
    g=nx.Graph()
    for i in range(0,n_ends,2):
        g.add_edge(i,i+1)
        #    print "edges:", g.edges()
    ends_broken={}
    for r in l:
        if len(r)==1:
#            if r[0] in breakers:
            if not end[r[0]] ==r[0]:
                ends_broken[end[r[0]]]=True
        else:
            g.add_edge(*r)
            if not r in originals:
                for rr in r:
#                    if rr in breakers:
                    if not end[rr]==rr:
                        ends_broken[end[rr]]=True
                        #print "edges:",g.edges(),l
    if len(nx.cycle_basis(g))>0:
        #print "found a cycle in",l,nx.cycle_basis(g)
        return 10000
    return(len(list(ends_broken.keys())))
    #    return 0

moves=[]
printed={}
color={}

def recombinings(e,blocked,forbidden,n_ends,end,originals,configs,forced=[]):
    
    #print "r:",e, moves, len(e),n_breaks(moves,n_ends,end,originals),max_n_breaks
    if set(e).intersection(blocked):
        for x in set(e).intersection(blocked):
            moves.append( (x,) )
        recombinings(tuple(sorted(list( set(e) - blocked ))),blocked,forbidden,n_ends,end,originals,configs,forced)
        for i in blocked:
            if i < n_ends: moves.pop()
        #print "### end of blocked"
        return()

    if forced:
        these_ends=[]
        for r in forced: 
            moves.append(r)
            for rr in r:
                these_ends.append(rr)
        newe=tuple(sorted(list( set(e) - set(these_ends) )))
        #print "after forced",newe
        if newe==tuple():
            if True or n_breaks(moves,n_ends,end,originals)==target_breaks:
                #print "\t".join(map(str,["moves:", n_breaks(moves,n_ends,end,originals),n_free(moves),tuple(sorted( moves))]))
                configs.append( (n_breaks(moves,n_ends,end,originals),n_free(moves),tuple(sorted( moves)))) 
        else:
            recombinings(newe,blocked,forbidden,n_ends,end,originals,configs)
        for r in forced: moves.pop()
        return()

    if tuple(sorted(moves)) in color: return
    color[tuple(sorted(moves))]=1
        
    if len(e)==1:
        moves.append( (e[0],) )
        if n_breaks(moves,n_ends,end,originals)<=max_n_breaks and not printed.get(tuple(sorted(moves))):
            #yield moves
            printed[tuple(sorted(moves))]=1
            if True or n_breaks(moves,n_ends,end,originals)==target_breaks:
                #print "\t".join(map(str,["moves:", n_breaks(moves,n_ends,end,originals),n_free(moves),tuple(sorted( moves))]))
                configs.append( (n_breaks(moves,n_ends,end,originals),n_free(moves),tuple(sorted( moves)))) 
#                print "moves:", n_breaks(moves),n_free(moves), tuple(sorted( moves))
            pass
        moves.pop()

    if len(e)==2:
        e1,e2=e
        for r in (((e1,),(e2,)),((e1,e2),)):
            for rr in r:
                if not rr in forbidden:
                    moves.append( rr )
                else:
                    moves.append( (rr[0],) )
                    moves.append( (rr[1],) )
                    
            if n_breaks(moves,n_ends,end,originals)<=max_n_breaks and not printed.get(tuple(sorted(moves))):
                    #yield moves
                    printed[tuple(sorted(moves))]=1
                    if True or n_breaks(moves,n_ends,end,originals)==target_breaks:
                        #print "\t".join(map(str,["moves:", n_breaks(moves,n_ends,end,originals),n_free(moves),tuple(sorted( moves))]))
                        configs.append( (n_breaks(moves,n_ends,end,originals),n_free(moves),tuple(sorted( moves)))) 
                        #print "moves:", n_breaks(moves),n_free(moves), tuple(sorted( moves))
                    pass
            for rr in r:
                if not rr in forbidden:
                    moves.pop()
                else:
                    moves.pop()
                    moves.pop()
                    
    if len(e)>2:
         for e1 in e:
            moves.append( (e1,))
            #print e1, n_breaks(moves)
            if n_breaks(moves,n_ends,end,originals)<=max_n_breaks:
                newe=tuple(sorted(list(set(e)-set([e1]))))
                #print "ne:",newe,moves
                recombinings( newe,blocked,forbidden, n_ends,end,originals,configs )
            moves.pop()       
            for e2 in e:
                if e2>e1:
                    if not (e1,e2) in forbidden:
                        moves.append( (e1,e2) )
                        if n_breaks(moves,n_ends,end,originals)<=max_n_breaks:
                            newe =  tuple(sorted(list(set(e)-set([e1])-set([e2]))))
                            recombinings(newe,blocked,forbidden,n_ends,end,originals,configs) 
                        moves.pop()

if False:
    for nn in range(1,n_ends,2):
        if not (nn%6 == 5 or nn%6==0) and not nn==n_ends-1 :
            end[nn]=nn
            end[nn+1]=nn
            originals.append((nn,nn+1))
    for i in range(n_ends):
        for j in range(i+1,n_ends):
            if old_div(j,6) - old_div(i,6) > 1:
                forbidden.append((i,j))


if __name__=="__main__":

    n=int(sys.argv[1])
    n_ends=n
    max_n_breaks=int(sys.argv[2])

    forbidden=[ (i,i+1) for i in range(0,n,2)]
    blocked=set([0,5,6,11,12,17])



    configs = ["none","left","right","both","cut","doublecut"]
    ncuts={"none":0,"left":0,"right":0,"both":0,"cut":1,"doublecut":2}
    config_blocked={"none":(0,5),"left":(5,),"right":(0,),"both":tuple(),"cut":(0,5),"doublecut":(0,5)}
    required={"none":((1,2),(3,4)),"left":((1,2),(3,4)),"right":((1,2),(3,4)),"both":((1,2),(3,4)),"cut":((3,4),),"doublecut":tuple()}
    import itertools
    t=tuple(range(int(sys.argv[1])))
    config_i=0

    for mconfig in itertools.product( tuple(configs), tuple(configs), tuple(configs) ):
        num_breaks = sum([ncuts[c] for c in mconfig])
        num_breaks_v = tuple([ncuts[c] for c in mconfig])
        if num_breaks<=max_n_breaks:
            target_breaks=num_breaks
            config_i+=1
            bbl = []
            req=[]
            noffset=0
            for cc in mconfig:
                for b in config_blocked[cc]:
                    bbl.append(b+noffset)
                for r in required[cc]:
                    req.append((r[0]+noffset,r[1]+noffset))
                noffset+=6
            print("\t".join(map(str,["config",config_i,num_breaks,num_breaks_v,mconfig,bbl,req])))
            blocked=set(bbl)
            printed={}
            configs=[]
            recombinings(t,blocked,forbidden,n_ends,end,originals,configs,req)

    #print t,recombinings,recombinings(t,[(1,2),(3,4),(7,8)])
    #recombinings(t)
    #for r in recombinings(tuple(range(int(sys.argv[1])))):
    #    print r
