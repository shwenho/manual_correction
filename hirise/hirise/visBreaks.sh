#! /bin/bash

mkdir -p break_vis

tail -n +3 customer_package/hirise_iter_broken_3.input_breaks.txt | awk '$3 !=0{printf "%s:%d-%d\n",$2,$3-100000<0?0:$3-100000,$3+100000; tot+=1; if(tot==20) exit;}' > break_vis/input_break_regions.txt
tail -n +3 customer_package/hirise_iter_broken_3.input_breaks.txt | awk '{prev=curr;prev_start=curr_start;prev_end=curr_end;curr=$1;curr_start=$6;curr_end=$7;} $3 !=0{split(prev,prev_id,"_");split($1,curr_id,"_");printf "%s:%d-%d,%s:%d-%d\n",prev_id[2],prev_start-100000<0?0:prev_start-100000,prev_end+100000,curr_id[2],$6-100000<0?0:$6-100000,$6+100000; tot+=1; if(tot==20) exit;}' > break_vis/final_break_regions.txt

parallel  --gnu "examine_layout.py -R {} -i assembly0.hra | vis.py | mpl_oovis.py --save break_vis/input_break{#}_region{}.png" :::: break_vis/input_break_regions.txt
parallel --gnu "examine_layout.py -R {} -i hirise_iter_broken_3.hra | vis.py -A hirise_iter_broken_3.hra | mpl_oovis.py --save break_vis/final_break{#}_region_{}.png" :::: break_vis/final_break_regions.txt
