#!/usr/bin/env python3
from builtins import map
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import random
import sys
#566 97 33
#566 98 22
#566 99 67
#nput@bigdove1:~$ cat count_matrix.txt  | sort -k2n | tail 
#99 162 0



#mat = np.zeros((163,567))
mat = np.zeros((567,163))
#mat = np.zeros((100,100))

while True:
    l = sys.stdin.readline()
    if not l:break
    x,y,n = list(map(int,l.strip().split()))
    mat[x,y]=n



#plt.pcolor(mat,norm=LogNorm(vmin=0.001))
#plt.colorbar()
#plt.show()
if False:
        plt.pcolor(mat)
        plt.colorbar()
        plt.show()
else:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax=ax.imshow(mat,interpolation="nearest",norm=LogNorm(vmin=1))
#        cax=ax.imshow(mat,interpolation="nearest")
        fig.colorbar(cax,shrink=0.5)
        fig.savefig("heatmap.png")
#        if args.debug: print "saved to out.png"
#        plt.figure().savefig("out.png")
