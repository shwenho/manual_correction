#!/usr/bin/env python3

import argh

@argh.arg("-s", "--scaffold_ids", nargs="+", required=True)
def main(hra, output="/dev/stdout", scaffold_ids=None):
    scaff_starts = False
    contigs = []
    with open(output, 'w') as output_handle:
        with open(hra) as hra_handle:
            for line in hra_handle:
                if line.startswith("P"):
                    scaff_starts = True
                if not scaff_starts:
                    print(line, file=output_handle, end="")
                if line.startswith("P"):
                    scaff = line.split()[1]
                    if scaff in scaffold_ids:
                        print(line, file=output_handle, end="")
                        contigs.append(line.split()[2])
                if line.startswith("L"):
                    contig = line.split()[1]
                    if contig in contigs:
                        print(line, file=output_handle, end="")
                    

if __name__ == "__main__":
    argh.dispatch_command(main)
