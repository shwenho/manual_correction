#!/usr/bin/env python3
#!/usr/bin/env python3


from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
#from string import str.maketrans
import hashlib

tr = str.maketrans("ACTGactg","TGACtgac")

def rc(s):
    r=s[::-1]
    rc = r.translate(tr)
    return(rc)

nb=5
dn=float(1<<(4*(nb)))

def process(name,seq,args):
    hws=old_div(args.wordsize,2)
    #print name,len(seq)
    for i in range(len(seq)-args.wordsize+1):
        mer=seq[i:i+args.wordsize].upper()
        print(mer,name,i,i+hws)
#        merrc=rc(mer)
#        st="+"
#        if mer>merrc: 
#            mer=merrc
#            st="-"
#        m=hashlib.md5(mer.encode("utf-8"))
#        d=m.hexdigest()
#        #if (hex(d[0]) & 7)==0:
#        f = float(int(d[0:nb],16))/dn
#        if f < args.fraction :
#            print name, i, mer,st,m.hexdigest(),f

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-w','--wordsize',default=101  ,  type=int)
#    parser.add_argument('-f','--fraction',default=0.001 ,  type=float)
    parser.add_argument('-i','--input'   ,default="-"   )
    parser.add_argument('-N','--nparts',default=False,type=int)
    parser.add_argument('-n','--part',default=False,type=int)

    args = parser.parse_args()

    fh=sys.stdin
    if args.input == "-":
        fh=sys.stdin

    else:
        fh=open(args.input)



    if args.nparts:
        from balanced_fasta_reader import seqs
        for s in seqs(fh,args.nparts,args.part):
            print(s['name'],len(s['seq']))
            process(s['name'],s['seq'],args)
    else:
        
        seq=""
        name=""
        while True:
            l = fh.readline()
            if not l: break
            if l[0]=="#": continue
            if l[0]==">":
                c=l[1:].strip().split()
                if name: process(name,seq,args)
                seq=""
                name=c[0]
            else:
                seq += l.strip() 


        process(name,seq,args)
