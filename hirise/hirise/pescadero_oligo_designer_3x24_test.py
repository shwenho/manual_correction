#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
from past.utils import old_div
import sys
import random
import argparse
import subprocess, shlex
import re
import numpy as np

#global k
k=9

nparts=1
asize = old_div(2**(k*2),nparts)
#print asize


nn1= k*3
n1m = np.zeros(nn1,dtype=np.dtype('u4'))
#print n1m
n1m[0]=1
n1m[1]=2
n1m[2]=3

for i in range(1,k):


#    print i*k,i*k+1,i*k+2
    n1m[3*i  ]=n1m[3*(i-1)  ]<<2
    n1m[3*i+1]=n1m[3*(i-1)+1]<<2
    n1m[3*i+2]=n1m[3*(i-1)+2]<<2

#for i in range(k*3):
#    print bin(n1[i])

def update_nbuffs(k):
    global nn1
    global asize
    global n1m
    
    asize = old_div(2**(k*2),nparts)
    #print asize


    nn1= k*3
    n1m = np.zeros(nn1,dtype=np.dtype('u4'))
    #print n1m
    n1m[0]=1
    n1m[1]=2
    n1m[2]=3

    for i in range(1,k):


    #    print i*k,i*k+1,i*k+2
        n1m[3*i  ]=n1m[3*(i-1)  ]<<2
        n1m[3*i+1]=n1m[3*(i-1)+1]<<2
        n1m[3*i+2]=n1m[3*(i-1)+2]<<2


def neighbors1(i,k):
    a=[]
    for j in range(k*3):
        a.append(i^n1m[j])
    return a


def neighbors2(i,k):
    a=[]
    for j in range(k):
        for jj in range(j+1,k):
            for z in range(3):
                for zz in range(3):
                    a.append(i^n1m[j*3+z]^n1m[jj*3+zz])
    return a

def neighbors3(i,k):
    a=[]
    for j in range(k):
        for jj in range(j+1,k):
            for jjj in range (jj+1,k):
                for z in range(3):
                    for zz in range(3):
                        for zzz in range(3):
                            a.append(i^n1m[j*3+z]^n1m[jj*3+zz]^n1m[jjj*3+zzz])
#    print "16 choose 3 times 27?",len(a)
    return a


bases=["A","C","T","G"]
def int2seq(x):
    s=""
    for i in range(k):
        b =  x & 3
        x = x>>2
        s = s+bases[b]
    return s
    
def seq2int(s):
    x=0
    for i in range(len(s)-1,-1,-1):
        x = x<<2
        x = x | bases.index(s[i])
    return x
        
def gen_oligo(n,g):
    s=""
    for i in range(n):
        if random.random()<g:
            s+=random.choice(["G","C"])
        else:
            s+=random.choice(["A","T"])
    return s

tm_cache={}

def tm(s):
    if s in tm_cache:
        return tm_cache[s]
    cmd = "oligotm {}".format(s)
    output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
    tm_cache[s]=float(output)
    return tm_cache[s]

#from string import str.maketrans
tr = str.maketrans("ACTGactg","TGACtgac")

def reverse(s):
    return s[::-1]

def rc(s):
    r=s[::-1]
    rc = r.translate(tr)
    return(rc)


def longest_homo(s):
    return max(list(map(len,re.findall('A+|C+|G+|T+',s))))

#  For TA ligation style:
#
#           Round 3         Round 2        Round 1        Genomic
#         <.........---   <.........---  <.........     <A......      Reverse strand
#    5' ---BBBBBBBBB>   ---BBBBBBBBB>  ---BBBBBBBBBT>     GGGGGG      Forward strand
#
#        ^               ^              ^          ^ 
#        |               |              |          |
#     R3 5'            R2 5'           R1 5'      R1 3'
#      overhang        overhang     overhang     overhang
#
#  For sticky end ligation style:
#
#           Round 3         Round 2        Round 1        Genomic
#         <.........---   <.........---  <.........----   G<......      Reverse strand
#    5' ---BBBBBBBBB>   ---BBBBBBBBB>  ---BBBBBBBBB>   GATCGGGGGG      Forward strand
#
#        ^               ^              ^            ^ 
#        |               |              |            |
#     R3 5'            R2 5'           R1 5'       R1 reverse 5'
#      overhang        overhang     overhang        overhang
#



if __name__=="__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('-H',default=3   ,type=int, help="longest homopolymer stretch allowed")
    parser.add_argument('-N',default=1   ,type=int, help="Number of distinct oligos to pick")

    parser.add_argument('--ta',default=False ,action='store_true',help="Design for TA-ligation style")
    parser.add_argument('--stickyend',default="ATC" ,help="5prime overhang on the starting DNA")
    parser.add_argument('--finaloverhang',default="   " ,help="5prime overhang after the third round")
    parser.add_argument('-n',default=10   ,type=int,help="Length of oligos")
    parser.add_argument('-g',default=0.5,type=float,help="Fraction of GC bases on average in generated oligos")
    parser.add_argument('-M',default=0.0,type=float,help="Minimum Tm for oligos")
    parser.add_argument('-E',default=False,action='store_true',help="No edit-distance-one pairs allowed")
    parser.add_argument('-X',default=100.0,type=float,help="Maximum Tm for oligos")

    args = parser.parse_args()
    
    #global k
    k=args.n
    update_nbuffs(k)

    n_found=0
    oligos={}
    edit1={}

    rounds = [1,2,3]
    forward_5prime_overhang= {1: "TGA", 2: "GTG", 3: "   "} 
    reverse_5prime_overhang= {}
    forward_3prime_overhang= {}
    reverse_3prime_overhang= {}
    
    forward_5prime_overhang[3]=args.finaloverhang

    if args.ta:
        reverse_5prime_overhang[1]=""
        forward_3prime_overhang[1]="T"
    else:
        reverse_5prime_overhang[1]=rc(args.stickyend)
        forward_3prime_overhang[1]=""
        
    for i in range(1,len(rounds)):
        reverse_5prime_overhang[rounds[i]]=rc(forward_5prime_overhang[rounds[i-1]])

    
    while n_found < float(args.N)*1.5:
        s=gen_oligo(args.n,args.g)
        while tm(s)<args.M or tm(s)>args.X or s in oligos or longest_homo(s)>args.H or (args.E and s in edit1):
            s=gen_oligo(args.n,args.g)
        if args.E:
            for n in map(int2seq, neighbors1(seq2int(s),args.n)):
                edit1[n]=1

        #print "#",s,tm(s),n_found, longest_homo(s), seq2int(s) 
        oligos[s]=tm(s)
        n_found += 1

    n_per_round = int( old_div(float(args.N), len(rounds)))

    oligo_table={}

    def bad_oligo(s):
        f=s
        r=rc(s)
        l=len(s)

        longest_perfect_match=0
        for i in range(l):
            if f[-i:] == r[:i]:
                lmatch=i
                if lmatch > longest_perfect_match:
                    longest_perfect_match=i

            if f[:i] == r[-1:]:
                lmatch=i
                if lmatch > longest_perfect_match:
                    longest_perfect_match=i
        if longest_perfect_match>4:
            print("#",s,"s has a self-hybridization of len",longest_perfect_match)
            return True
        return False

    def bad_fr_pair(f,r):

        l=len(f)
        longest_perfect_match=0
        for i in range(l):
            if f[-i:] == r[:i]:
                lmatch=i
                if lmatch > longest_perfect_match:
                    longest_perfect_match=i

            if f[:i] == r[-1:]:
                lmatch=i
                if lmatch > longest_perfect_match:
                    longest_perfect_match=i
        if longest_perfect_match>l-3:
            print("#",f,r,"have a hybridization of len",longest_perfect_match)
            return True
        return False


    barcodes = list(oligos.keys())
    for r in rounds:
        print("#Round",r)
        for i in range(n_per_round):
            bc = barcodes.pop()
            oligo_table[r,i,"f"] = forward_5prime_overhang.get(r,"   ") +    bc  + forward_3prime_overhang.get(r,"   ")
            oligo_table[r,i,"r"] =  reverse_5prime_overhang.get(r,"   ") + rc(bc) + reverse_3prime_overhang.get(r,"   ")

            while bad_oligo( oligo_table[r,i,"f"] ) or bad_oligo( oligo_table[r,i,"r"] ) or bad_fr_pair(oligo_table[r,i,"f"],oligo_table[r,i,"r"]):
                bc = barcodes.pop()
                oligo_table[r,i,"f"] = forward_5prime_overhang.get(r,"   ") +    bc  + forward_3prime_overhang.get(r,"   ")
                oligo_table[r,i,"r"] =  reverse_5prime_overhang.get(r,"   ") + rc(bc) + reverse_3prime_overhang.get(r,"   ")

            print("#round {:<3d} barcode {:<4d} {:>15s}: 3'<   {:<20.20s}-5'  tm: {}".format(r,i,"reverse oligo",reverse( reverse_5prime_overhang.get(r,"   ") + rc(bc) + reverse_3prime_overhang.get(r,"   ") ), oligos[bc]))
            print("#                       {:>15s}: 5'-   {:<20.20s}>3'".format(    "forward oligo",         forward_5prime_overhang.get(r,"   ") +    bc  + forward_3prime_overhang.get(r,"   ") ))
            print()


    def oligo_to_well(r,i,altA=False):
        rows=["A","B","C","D","E","F","G","H"]
        if altA:
            row = rows[ 6 + (old_div(i,12))  ]
        else:
            row = rows[ old_div(((r-1)*24+i),12) ]

        column = i%12
        return row+str(column+1)

    for k in list(oligo_table.keys()):
        print("#oligo:",k,oligo_table[k])

    n_per_round = 24
    for r in rounds:
        for i in range(n_per_round):
            for d in ["f"]:
                print("{}\tR{}b{:02d}{}\t{}\t{}\t{}\t{}".format(oligo_to_well(r,i),r,i,d,oligo_table[r,i,d],r,i,d))
            
#    for r in [1]:
#        for i in range(n_per_round):
#            for d in ["f"]:
#                print "{}\tR{}Ab{:02d}{}\t{}\t{}\t{}\t{}".format(oligo_to_well(r,i,True),r,i,d,oligo_table[r,i,d][:-1],r,i,d)

    for r in rounds:
        for i in range(n_per_round):
            for d in ["r"]:
                print("{}\tR{}b{:02d}{}\t{}\t{}\t{}\t{}".format(oligo_to_well(r,i),r,i,d,oligo_table[r,i,d],r,i,d))

#    for r in [1]:
#        for i in range(n_per_round):
#            for d in ["r"]:
#                print "{}\tR{}Ab{:02d}{}\t{}\t{}\t{}\t{}".format(oligo_to_well(r,i,True),r,i,d,"GATC"+oligo_table[r,i,d],r,i,d)
        

    for n in range(5):
        print("#Example",n)
        barcodes= {}
        for r in rounds:
            barcodes[r] = random.choice( list(range(96)) )
            
        print("# 3' <-"+" "*len(forward_5prime_overhang[rounds[-1]])+reverse("*".join(  [ oligo_table[r,barcodes[r],"r"].strip() for r in rounds ]  )) + "   - 5'")
        print("# 5'  -   "+"*".join(  [ oligo_table[r,barcodes[r],"f"].strip() for r in reversed(rounds) ]  )                                             + "   ->3'")

