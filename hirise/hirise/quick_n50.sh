#! /bin/bash
source ~/virt_env/virt2/bin/activate

for f in `ls -ct hirise*.out | grep -v fasta` ; do echo -n "$f "; grep slen $f | n50stats.py -l -N $1 | awk '{print $1/1e6,$2}'; done
