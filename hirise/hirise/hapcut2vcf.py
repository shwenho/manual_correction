#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys
import argparse
import pkg_resources
import gzip as gz

import vcf
from vcf.parser import _Filter

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v','--vcf',required=True)
parser.add_argument('-b','--blocks',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-d','--debug',default=False,action="store_true")

args = parser.parse_args()

if args.debug:
    print(args)

input = []
if args.vcf[-2:]=="gz":
    input = gz.open(args.vcf,"r")
else:
    input = open(args.vcf,"rb")

print(dir(input))

of=open(args.outfile,"w")
inp = vcf.Reader(input)
oup = vcf.Writer(of,inp)



for record in inp:
    print(record)
    oup.write_record(record)


of.close()
