#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
from hirise.hirise_assembly import HiriseAssembly
import struct
import hashlib

if __name__=="__main__":
     import sys
     import argparse

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-B','--bin',default=False,action="store_true",help="Bin the reference contigs")
     parser.add_argument('-C','--nchunks' ,default=1,type=int,help="Number of chunks.")
     parser.add_argument('-w','--window' ,   default=1000,type=int,help="Window size.")
     parser.add_argument('-W','--binsize' ,   default=100000,type=int,help="Reference bin size.")
     parser.add_argument('-m','--min_links' ,default=2,type=int,help="Min links to another contig to count.")
     parser.add_argument('-M','--maxothers',default=3,type=float,help="Max other contigs.")
#     parser.add_argument('-S','--shotgunT',default=False,type=float,help="Max shotgun reads in the window.")
     parser.add_argument('-c','--chunk',default=0,  type=int,help="This chunk.")
     parser.add_argument('-K','--contig',default=False, help="This contig only.")
     parser.add_argument('-H','--head',default=False,  type=int,help="Quit after looking at this many contigs.")
     parser.add_argument('-i','--infile',default=False,help="Filename for serialised assembly input file.")
     parser.add_argument('-o','--outfile',default=False,help="Filename for writing a list of segments on the raw contigs to mask for being promiscuous in linking.")
     # -m 2 -w 1000 -M $( cat {input.threshold} ) 
     args = parser.parse_args()

     if args.infile:
          asf = HiriseAssembly()
          asf.load_assembly(args.infile)

     if args.outfile:
          of=open(args.outfile,"wt")
     else:
          of=sys.stdout

     ndone=0
     if args.contig:
          it = iter([args.contig])
     else:
          def my_contigs_iter():
               for oc in asf.ocontigs_iter():
                    chunk = struct.unpack("<L", hashlib.md5(oc.encode("utf-8")).digest()[:4])[0]%args.nchunks
                    if chunk == args.chunk:
                         yield oc
          it = my_contigs_iter()

     asf.binsize=args.binsize
     for ocontig in it:

          links={}
          asf.chicago_promiscuity_mask(ocontig,mapq=10,outfile=of,w=args.window,debug=args.debug,bins=args.bin,maxothers=args.maxothers,minlinks=args.min_links)

          ndone+=1
          if args.head and ndone > args.head: break
     of.close()
