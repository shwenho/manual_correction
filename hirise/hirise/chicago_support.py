#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
from hirise.mapperlite import MapperLite
import struct
import hashlib
import pysam
import hirise.chicago_edge_scores as ces
#import hirise.BamTags
from hirise.bamtags import BamTags
from hirise.chicago_edge_links2 import read_mask_ranges, mask_test
debug=False

def chicago_pairs(sca,mapper,bamlist,minq=20,mask={}):
     for seg in mapper.scaffold_contigs[sca]:
#          yield( sc,seg,bamlist)
          
          ref="_".join(seg.split("_")[:-1])

          for b in bamlist:
               for aln in b.fetch(until_eof=True,reference=ref):
                    if not aln.is_read1: continue
                    if aln.is_duplicate: continue
                    if aln.mapq < minq : continue
                    if BamTags.mate_mapq(aln) < minq : continue
                    if mask_test(ref,aln.pos,mask) or mask_test(b.getrname(aln.rnext),aln.pnext,mask) : continue
                    
                    contig = b.getrname(aln.tid) # snam[aln.tid]
                    
                    ncontig= b.getrname(aln.rnext) if aln.rnext>=0 else -1

                    scaffold ,z1a,z2a,z3a,c1 = mapper.mapCoord( contig, aln.pos,     aln.pos+1 ) 
                    nscaffold,z2p,x2p,z3p,c2 = mapper.mapCoord(ncontig, aln.pnext,aln.pnext+1 ) 
                    if debug: print(("#x",contig,ncontig,aln.pos,aln.pnext,scaffold,nscaffold,sca,z1a,z2p,ref,mapper.ocontig_contigs.get(ref,[])))
                    if scaffold==nscaffold and sca==scaffold:
                         #yield( sc,seg,contig,ncontig,scaffold,z1a,z2a,z3a,nscaffold,z2p,x2p,z3p )
                         yield( sca,z1a,z2p,c1,c2,seg,contig,ncontig,scaffold,z1a,z2a,z3a,nscaffold,z2p,x2p,z3p )

def pairs2support(scaffold,pairs,model,mapper,buff=500,debug=False,t1=20.0,t2=5.0,gpf=False,minx=1000,maxx=1e7,joins=[],logfile=False):
     slen=mapper.scaffold_length[scaffold]
     #print(slen)
     edges=[]
     nb=[]
     #xpp=[]
     #print(pairs)
     for p in pairs:
          if debug: print(p)
          if p[1]<p[2]:
               a,b,c1,c2,w,z=p[1],p[2],p[3],p[4],p[6],p[7]
          else:
               a,b,c1,c2,w,z=p[2],p[1],p[4],p[3],p[7],p[6]
               
#          a,b=min(p[1:3]),max(p[1:3])


          if (b-a)>minx and (b-a)<maxx:
               if gpf: gpf.write("{}\t{}\t{}\t{}\n".format(0.5*(a+b),0.5*(b-a),w,z))
               ll=model.lnF(b-a)

               edges.append( tuple([a+buff         , ll,c1,"a"]) )
               edges.append( tuple([b-buff         ,-ll,c2,"b"]) )

               nb.append( tuple([a+buff         , 1]) )
               nb.append( tuple([b-buff         ,-1]) )

     if gpf: gpf.write("\n\n\n")
     edges.sort()
     nb.sort()
     rs=0
     n=0
     tripped=False
     last=0
     state=0
     stretch_start=0
     low_point=0.0
     ji=0
     gap_scores={}
     gap_coverages={}

     break_buffer = []
     last_trip=0
     for i in range(len(edges)):
          rs+=edges[i][1]
          n+=nb[i][1]
          x=edges[i][0]

          try:
               score=model.cutScore(slen,x,n,rs,rangeCutoff=maxx,minSep=minx)
          except Exception as e:
               print(edges)
               print(i,edges[i],nb[i],rs,n,x,scaffold,slen,len(edges))
               raise e

          while ji<len(joins) and x>joins[ji][0]: 
               gap_scores[ji]    =score
               gap_coverages[ji] =n
               ji+=1

          if score>t1:
               tripped=True
               last_trip=x
          if tripped and score<t2 and state==0:
               stretch_start=x
               state=1
               low_point =score
               minx = x
          if state==1 and score>t2:
               
               if logfile: 
                    break_buffer.append( (scaffold,stretch_start,x,minx,low_point,slen)  )
#               print(scaffold,stretch_start,x,slen,low_point,"break")
               state=0
          if state==1:
               if score<low_point:
                    low_point=score
                    minx=x
          if debug: print("dd:",scaffold,edges[i][0],score,rs,state,x,stretch_start,score,n,edges[i][2])
          if gpf: gpf.write("{}\t{}\t{}\t{}\n".format(edges[i][0],score,rs,n))

          last=edges[i][0]

     for scaffold,stretch_start,x,minx,low_point,slen in break_buffer:
          if x < last_trip:
               logfile.write("{} {} {} {} {} {}\n".format(scaffold,stretch_start,x,minx,low_point,slen))

     for ji in range(len(joins)):
          print("\t".join(map(str,["gapscore:",ji]+list(joins[ji])+[gap_scores.get(ji,0),gap_coverages.get(ji,0)])))


if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
     parser.add_argument('-l','--layout',default=False,help="File containing scaffolding layout.")
     parser.add_argument('-s','--segments',default=False,help="File containing scaffolding segments.")
#     parser.add_argument('-L','--length',default=False,help="File containing lenghts.")
     parser.add_argument('-m','--mask',default=False,help="File containing segments to mask.")
     parser.add_argument('-g','--plotfiles',default=False,help="Plot file name prefix.")

     parser.add_argument('-f','--logfile',default=False,help="Output file for storing score segments.")
     
     parser.add_argument('-q','--mapq',type=int,default=55)

     parser.add_argument('-t','--t1',type=float,default=100.0,help="Don't break in the trailing regions at the end of scaffolds where support never exceeds this number.")
     parser.add_argument('-T','--t2',type=float,default=75.0,help="Report all segments where support dips below this threshold, if they are not in the trailing ends.")

     parser.add_argument('-c','--my_chunk',type=int,default=1)
     parser.add_argument('-C','--nchunks',type=int,default=32)
     parser.add_argument('--minx',type=int,default=1000, help=" ")
     parser.add_argument('--maxx',type=int,default=200000,help=" ")
     parser.add_argument('-S','--scaffold',default=False)
     #     parser.add_argument('-b','--bamfiles',required=True)
     parser.add_argument('-b','--bamfile',action="append")
     parser.add_argument('-M','--model')

     args = parser.parse_args()
     if args.progress: print("#",args)

     fmodel=open( args.model )
     contents = fmodel.read()
     try:
          fit_params=eval(contents)
     except:
          print("couldn't set model parameters", args.model)
     fmodel.close
     ces.set_exp_insert_size_dist_fit_params(fit_params)

     if args.mask:
          mask_ranges = read_mask_ranges( open(args.mask) )
     else:
          mask_ranges={}
     
     #
     # Read in a hirise layout (from "^p:" lines)
     #
     mapper = MapperLite()
     mapper.load_layout(open(args.layout))

     if args.scaffold:
          my_scaffolds={args.scaffold:1}
     else:
          my_scaffolds={}
          scaffold_hashes={}
          for s in list(mapper.scaffolds.keys()):
               scaffold_hashes[s]=struct.unpack("<L", hashlib.md5(s.encode("utf-8")).digest()[:4])[0]%args.nchunks
               if scaffold_hashes[s]==args.my_chunk:
                    my_scaffolds[s]=1
                    if args.debug: print("my scaffold:",s)
          
     bamlist = [ pysam.Samfile(bamfile,"rb") for bamfile in args.bamfile ] 

     segment_logfile=False
     if args.logfile: segment_logfile = open(args.logfile,"wt")

     for sc in sorted(my_scaffolds.keys()):
          ncontigs=len(mapper.scaffold_contigs[sc]) 
          slen=mapper.scaffold_length[sc]
          if not ncontigs>1: continue
          print("sc:",sc,ncontigs,slen)
          #          print(sc,mapper.scaffold_contigs[sc])
          fp=False
          contigs = sorted(mapper.scaffold_contigs[sc],key=lambda x: mapper.contigx[x])
          ii=0
          gap_locations=[]
          for i in range(len(contigs)-1):
               con = contigs[i]
               x= max(mapper.contigx[con]+mapper.contig_strand[con]*mapper.contig_length[con],mapper.contigx[con])
               con2 = contigs[i+1]
               y= min(mapper.contigx[con2]+mapper.contig_strand[con2]*mapper.contig_length[con2],mapper.contigx[con2])
               gap_locations.append((int((x+y)/2),con,con2))

          if args.plotfiles: 
               fn="{}{}".format(args.plotfiles,sc)
               fp=open(fn,"wt")
               print("#plot \"{}\" i 2 u 1:2 w steps, \"\" i 1 u 1:($2/20) lt 3 pt 5 ps 0.7, \"\" i 0 u 1:(($2-5)*100) w steps lt 3, -500 lt 3".format(fn))
               ii=0
               fp.write("0\t0\n")
               for con in contigs:
                    ii+=1
                    ii=ii%2
                    if args.debug: print(con,mapper.contigx[con],mapper.contig_strand[con],mapper.contig_length[con],slen)
                    if mapper.contig_strand[con]==1:
                         fp.write("{}\t{}\n".format( mapper.contigx[con],2*ii-1 ))
                         fp.write("{}\t{}\n".format( mapper.contigx[con]+mapper.contig_length[con],0 ))
                    else:
                         fp.write("{}\t{}\n".format( mapper.contigx[con]-mapper.contig_length[con],2*ii-1 ))
                         fp.write("{}\t{}\n".format( mapper.contigx[con],0 ))
               fp.write("\n\n\n")

          pairs2support(sc,chicago_pairs(sc,mapper,bamlist,minq=args.mapq,mask=mask_ranges),ces.model,mapper,debug=args.debug,gpf=fp,joins=gap_locations,minx=args.minx,maxx=args.maxx,logfile=segment_logfile,t1=args.t1,t2=args.t2)
          if fp: fp.close()
