#!/bin/bash

echo "$#"

usage ()
{
  echo 'Usage : copy_upto.bash source_dir FILE dest_dir'
  echo '     Copies all files as old or older than FILE in the directory tree starting at source_dir.'
  exit
}

if [ "$#" -ne 3 ]
then
  usage
fi


ad=$1
cp=$2
de=$3
td=`mktemp -d -p .`
echo $td
find $ad -not -newer $cp > $td/files.txt
find $ad -name "broken.audit*"   >> $td/files.txt
find $ad -name "broken.misjoin*" >> $td/files.txt
find $ad -name "hs.fragOnly.*"   >> $td/files.txt

head $td/files.txt
wc -l $td/files.txt
rsync -Rah --files-from=$td/files.txt . $td
ls $td
mv $td/$ad $de
rm -rf $td
