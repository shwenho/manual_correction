#!/usr/bin/env python3
from __future__ import print_function
import sys
import glob


maternal_count={}
paternal_count={}
both={}

#21_maternal 9411205 AAGAAATTGTAGTTTTCTTCTGGCTTAGAGGTAGATCATCTTGGTCCAATCAGACTGAAATGCCTTGAGGCTAGATTTCAGTCTTTGTGGCAGCTGGTGAA + 00a1e9bb7a07a16139b9ae426c058ad1 0.00247001647949
#21_maternal 9411254 ATTGGGACCCCTAAAAAGCTAATCCCTAGCTGAAAAGGCAAACTAGAAATTCACCAGCTGCCACAAAGACTGAAATCTAGCCTCAAGGCATTTCAGTCTGA - 0042f44264bbba08c3a0609cf17e90d8 0.00102138519287


for fn in glob.glob("./*maternal*"):
    print(fn)
    f=open(fn)
    while True:
        l=f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        maternal_count[c[2]] = maternal_count.get(c[2],0)+1
        both[c[2]] = both.get(c[2],0)+1
    f.close()

for fn in glob.glob("./*paternal*"):
    print(fn)
    f=open(fn)
    while True:
        l=f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        paternal_count[c[2]] = paternal_count.get(c[2],0)+1
        both[c[2]] = both.get(c[2],0)+1
    f.close()

for k in list(both.keys()):
    print(k,both.get(k,0),maternal_count.get(k,0),paternal_count.get(k,0))






