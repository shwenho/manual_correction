#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#config  1       0       (0, 0, 0)       ('none', 'none', 'none')        [0, 5, 6, 11, 12, 17]   [(1, 2), (3, 4), (7, 8), (9, 10), (13, 14), (15, 16)]
#moves:  0       6       ((0,), (1, 2), (3, 4), (5,), (6,), (7, 8), (9, 10), (11,), (12,), (13, 14), (15, 16), (17,))
#config  2       0       (0, 0, 0)       ('none', 'none', 'left')        [0, 5, 6, 11, 17]       [(1, 2), (3, 4), (7, 8), (9, 10), (13, 14), (15, 16)]
#moves:  0       6       ((0,), (1, 2), (3, 4), (5,), (6,), (7, 8), (9, 10), (11,), (12,), (13, 14), (15, 16), (17,))
#config  3       0       (0, 0, 0)       ('none', 'none', 'right')       [0, 5, 6, 11, 12]       [(1, 2), (3, 4), (7, 8), (9, 10), (13, 14), (15, 16)]
#moves:  0       6       ((0,), (1, 2), (3, 4), (5,), (6,), (7, 8), (9, 10), (11,), (12,), (13, 14), (15, 16), (17,))
#config  4       0       (0, 0, 0)       ('none', 'none', 'both')        [0, 5, 6, 11]   [(1, 2), (3, 4), (7, 8), (9, 10), (13, 14), (15, 16)]

import sys

configs_by_n={}
rearrangements_by_config={}

config=""
while True:
    l=sys.stdin.readline()
    if not l: break
    if l[0]=="#": continue

    c=l.strip().split("\t")
    if c[0]=="config":
        configs_by_n[eval(c[3])] = configs_by_n.get(eval(c[3]),[]) + [eval(c[4])]
        config = eval(c[4])
    elif c[0]=="moves:":
        rearrangements_by_config[config] = rearrangements_by_config.get(config,[])+[eval(c[3])]


n1=int(sys.argv[1])
n2=int(sys.argv[2])
n3=int(sys.argv[3])

for b in list(configs_by_n.keys()):
    print("\t".join(map(str,[b,len(configs_by_n[b]),sum([ len(rearrangements_by_config[i]) for i in configs_by_n[b] ]),configs_by_n[b], [ len(rearrangements_by_config[i]) for i in configs_by_n[b] ]])))

for b in list(configs_by_n.keys()):
#    print "#> ",n1,n2,n3,b[0],b[1],b[2], b[0]<=n1 , b[1]<=n2, b[2]<=n3
    if b[0]<=n1 and b[1]<=n2 and b[2]<=n3:
        print("#> "+"\t".join(map(str,[b,len(configs_by_n[b]),sum([ len(rearrangements_by_config[i]) for i in configs_by_n[b] ]),configs_by_n[b], [ len(rearrangements_by_config[i]) for i in configs_by_n[b] ]])))
