#example command line:
#bash make_dotplot.bash hirise_iter_3.fasta hirise_iter_3.fasta.length /mnt/nas2/bigdove1/scratch2/projects/prairie_chicken/Meleagris_gallopavo.UMD2.cds.all.fa turkey.lengths turkey_transcript_locations.txt iter_3_dots.gp

# transcript coords file should look something like this:
#
#Ensembl Transcript ID   Chromosome Name Gene Start (bp) Gene End (bp)   Strand
#ENSMGAT00000001225      GL424145.1      1       7963    1
#ENSMGAT00000001399      GL424225.1      1       4898    -1
#ENSMGAT00000001653      GL424691.1      1       3469    -1
#ENSMGAT00000003115      GL424564.1      1       3230    1
#ENSMGAT00000003313      GL424568.1      1       945     1

# ref lengths something like this:

#204065131 1
#31125776 10
#24221871 11
#20663262 12
#20109194 13

#/home/chuck/scratch/tumbler/hirise_03/best_blast_placements.txt
#best:   ENSMGAT00000019968      595.0   ScT83nN_99      -       53769614        53770049        573     1008    None
#best:   ENSMGAT00000008371      1568.0  ScT83nN_59      +       269235  270452  264     1463    None
#best:   ENSMGAT00000018925      466.0   ScT83nN_169     -       29350289        29350563        254     528     None
#best:   ENSMGAT00000001363      959.0   ScT83nN_876     +       204032  205081  742     1809    None
#best:   ENSMGAT00000018542      712.0   ScT83nN_782     -       19070957        19071475        1       519     None
#best:   ENSMGAT00000001332      636.0   ScT83nN_396     -       101431  102127  8       704     None
#best:   ENSMGAT00000014424      622.0   ScT83nN_253     +       27125880        27126592        17      714     None
#best:   ENSMGAT00000007936      555.0   ScT83nN_789     +       3352184 3352543 1682    2041    None


#xxx
td=`mktemp -d -p .`
hirise_fasta=$1                        #hirise_iter_3.fasta
assembly_lengths=$2
ref_cds=$3
ref_lens=$4
transcript_coords=$5                   #turkey_transcript_locations.txt
outfile=$6

for infile in $hirise_fasta $assembly_lengths $ref_cds $ref_lens $transcript_coords ; do
    if [ ! -e $infile ] ; 
    then 
	echo "missing: $infile"
	exit 0 ; 
    fi
done

echo "formatdb -p F -i $hirise_fasta"
formatdb -p F -i $hirise_fasta
for f in ${ref_cds/.fa/.??.fa}; do blastall -pblastn -d $hirise_fasta -i $f -e 1e-120 -o $td/`basename ${f/.fa/.blastn}` -m 8 -F 'm D' -U & done
jobs
echo "waiting"
wait
mbChain.py --cds -b <( cat $td/*.blastn )| grep best > $td/best_blast_placements.txt
colJoin2.py $transcript_coords 0 $td/best_blast_placements.txt 1 > $td/hirise_dot.txt
cat $td/hirise_dot.txt| grep -v GL4 | awk 'BEGIN {OFS="\t"} {print $2,$12,($13+$14)/2,"+",$4,($6+$7)/2,$5}' | sort -k2,2V -k3,3n  | dotplot.py -g -L $ref_lens  -L $assembly_lengths -o $outfile
#
