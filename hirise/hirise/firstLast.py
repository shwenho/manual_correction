#!/usr/bin/env python3
from __future__ import print_function
import sys

c=['']*7
last_line=False

while True:
    l=sys.stdin.readline()
    if not l: break
    c=l.strip().split()
    if not last_line:
        print("\t".join(c))
        
    if last_line and not c[4]==last_line[4]:
        print("\t".join(last_line))
        print("\t".join(c))
    last_line = c
