#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from past.utils import old_div
import pysam
import sys
import math
import networkx as nx
import hirise.mapper
import pickle
import re

def log(s):
    print(s)

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--debug',action='store_true',default=False)
    parser.add_argument('--map')


    nodes={}

    args = parser.parse_args()

    mapp = pickle.load(open(args.map))

    edges = []
    degrees = {}

    ss={}
    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=='#': continue
        c = l.strip().split()

        m = re.match("'(.*)'=='([\+-])(.*)' \((\S+) (\S+) (\S+) (\S+)\) (\S+)",l)
        if m:

            T,strand,Q = m.group(1),m.group(2),m.group(3)
            tstart,qstart,tstop,qstop,misses = list(map(int,[m.group(4),m.group(5),m.group(6),m.group(7),m.group(8)]))
            leng = abs(qstop-qstart)                                                                                                                                                                     
            pid = old_div(float(leng - misses + 1), leng)
            score = leng - misses
            hits = leng - misses

            a,b=mapp.map_coord(mapp.name_to_index[Q],qstart)
            c,d=mapp.map_coord(mapp.name_to_index[Q],qstop)
            mappedQ1 = mapp.print_name(a)
            mappedQ2 = mapp.print_name(c)
            if mappedQ1==mappedQ2:
                if b>d:
                    strand = "-"
                else:
                    strand = "+"
                print("'%s'=='%s%s' (%d %d %d %d) %d" % (T,strand,mappedQ1,tstart,b,tstop,d,misses)) #,"#", l.strip()


