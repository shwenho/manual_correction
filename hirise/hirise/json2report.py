#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import json
import re
import socket
import tempfile
from hirise.hirise_stats import stats_helpstrings
import shlex
import shutil
import subprocess
import random
import os
from hirise.plot_insert_fit import plot_insert_fit
from hirise.n50stats import n50
import datetime
import time

def org2pdf(fh,args):

     if not args.outfile: return
     if args.outfile.endswith(".pdf"):
                #emacs test.org --batch -f org-export-as-pdf --kill
          cmd = "emacs {} --batch -f org-export-as-pdf  --kill ".format(fh.name)
          if args.debug: print(cmd)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
          if args.debug:
               print(output)
               print(error)

          if args.outfile:
               #copy the final pdf from the tmpdir
               cmd = "cp {} {} ".format(re.sub(".org",".pdf",fh.name),args.outfile)
               output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
     else:
          cmd = "cp {} {}".format(fh.name,args.outfile)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
          

#plot_insert_fit(inserts,smooth,corrected,datamodel,outfile,hideraw)
def add_insert_fit_fig(rec,fh,tempdir):
     td=tempfile.mkdtemp(dir=tempdir)

     plotfile=tempfile.NamedTemporaryFile(suffix=".txt",dir=tempdir,delete=False)
     machine,path=rec.get('path').split(':')
     pdfid=int(1e6*random.random())
#      all.merged_histogram all_smoothed.merged_histogram all_smoothed_corrected.merged_histogram datamodel.out 


     hostname=socket.gethostname()
     if hostname==machine:
          cmd = "cp {}/{} {}".format(path,"all_smoothed.merged_histogram",td)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
          cmd = "cp {}/{} {}".format(path,"all_smoothed_corrected.merged_histogram",td)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
          cmd = "cp {}/{} {}".format(path,"datamodel.out",td)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
     else:

          cmd = "scp {}:{}/{} {}".format(machine,path,"all_smoothed.merged_histogram",td)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
          cmd = "scp {}:{}/{} {}".format(machine,path,"all_smoothed_corrected.merged_histogram",td)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
          cmd = "scp {}:{}/{} {}".format(machine,path,"datamodel.out",td)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()

     hideraw=True
     inserts="/dev/null"
     smooth="{}/all_smoothed.merged_histogram".format(td)
     corrected="{}/all_smoothed_corrected.merged_histogram".format(td)
     datamodel="{}/datamodel.out".format(td)
     outfile="{}/model_fit{}.pdf".format(tempdir,pdfid)
     plot_insert_fit(inserts,smooth,corrected,datamodel,outfile,hideraw)


     if not os.stat("{}/model_fit{}.pdf".format(tempdir,pdfid)).st_size == 0:
#          fh.write("*** Model fit:\n\n".encode())
          fh.write("\\includegraphics{{{}/model_fit{}.pdf}}\n".format(tempdir,pdfid).encode())


def add_iteration_table(rec,fh,tempdir):
     iterations = list(rec.get('all_iters_n50',{}).keys())
     iterations.sort(key=lambda x: iter_name_sort_key(x) )

     fh.write("\n\n".encode())
     fh.write(     "|     | N50 |\n".encode())
     for ii in iterations:
          
          fh.write("| ={}=  | {:0.3f} Mb |\n".format(ii,rec['all_iters_n50'][ii]/1000000).encode())
     fh.write("\n\n".encode())


def add_report_coverage_histogram(rec,fh,tempdir,latex = True):
     """This function uses gnuplot to plot a set of histograms, and adds them to the report."""
     histfile=tempfile.NamedTemporaryFile(suffix=".txt",dir=tempdir,delete=False)
     datadict=rec.get('coverage_histogram',{})
     ids = sorted(list(datadict.keys()))
     #print(ids)
     depth={}
     for id in ids:
          for k,v in datadict.get(id,{}).items():
               depth[int(k)]=1

     depths=list(depth.keys())
     depths.sort()
     for d in depths:
          v=[d]
          for id in ids:
               v.append(datadict[id].get(str(d),0))
          ob="\t".join(map(str,v))
          ob+="\n"
          #print(ob)
          histfile.write( ob.encode() )
     histfile.flush()
     #print(histfile.name)

     gpfile=tempfile.NamedTemporaryFile(suffix=".gp",dir=tempdir)
     pdfid=int(1e6*random.random())
     gpstr="""
set term "pdf"
set output "{}/coverage_hist{}.pdf"
set style line 1 lc rgb 'red'
set style line 2 lc rgb 'blue'
set style line 3 lc rgb 'violet'
set style line 4 lc rgb '#FF00FF'
set xlabel "depth"
set ylabel "counts"
""".format(tempdir,pdfid)
     gpfile.write(gpstr.encode())
     plot_clauses=[]
     i=2
     for id in ids:
          plot_clauses.append(""" "{}" using 1:{} w histeps ls {} lw 5 t "{}" """.format(histfile.name,i,i-1,id))
          i+=1
     gpcmd = "plot "+ ",".join(plot_clauses)+"\n"
     if args.debug: print(gpstr+gpcmd)
     gpfile.write(gpcmd.encode())
     gpfile.flush()

     cmd = "gnuplot {}".format(gpfile.name)
     output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()

     if not os.stat("{}/coverage_hist{}.pdf".format(tempdir,pdfid)).st_size == 0:
#          fh.write("*** Histogram of sampled chicago spanning coverage:\n\n".encode())
          fh.write("\\includegraphics{{{}/coverage_hist{}.pdf}}\n".format(tempdir,pdfid).encode())
          #print(rec.get('coverage_histogram'))
     if latex:
         gpstr="""
set term 'epslatex' size 7.5in,5in"
set output "coverage_hist.tex"
set style line 1 lc rgb 'red'
set style line 2 lc rgb 'blue'
set style line 3 lc rgb 'violet'
set style line 4 lc rgb '#FF00FF'
set xlabel "Physical coverage"
set ylabel "Number of sites"
set title "Chicago coverage histogram"
"""
         gpfile=tempfile.NamedTemporaryFile(suffix=".gp",dir=tempdir)
         gpfile.write(gpstr.encode())
         plot_clauses=[]
         i=2
         for id in ids:
              if id != "combined":
                  name = "Chicago Library %d" % (i-1)
              else:
                  name = id
              plot_clauses.append(""" "{}" using 1:{} w histeps ls {} lw 5 t "{}" """.format(histfile.name,i,i-1,name))
              i+=1
         gpcmd = "plot "+ ",".join(plot_clauses)+"\n"
         if args.debug: print(gpstr+gpcmd)
         gpfile.write(gpcmd.encode())
         gpfile.flush()

         cmd = "gnuplot {}".format(gpfile.name)
         output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()

def iter_name_sort_key(name):
     if name=="hirise.out": return(-1,-1)
     m=re.findall("hirise_iter(_broken)?_(\d+).out",name)
     return(int(m[0][1]),m[0][0])

def get_selected_round(r,round_id):
     if round_id:
          return round_id
     else:
          rounds = list(r.get('all_iters_n50',{}).keys())
          if len(rounds)==0: return 'hirise_iter_broken_3.out'
#          rounds.sort(key=lambda x: r['all_iters_n50'][x],reverse=True)  # this would take whichever has the highest N50
          rounds.sort(key=lambda x: iter_name_sort_key(x),reverse=True)
          return(rounds[0])

def get_maxN50_round(r):
          rounds = list(r.get('all_iters_n50',{}).keys())
          if len(rounds)==0: return None
          rounds.sort(key=lambda x: r['all_iters_n50'][x],reverse=True)  # this would take whichever has the highest N50
#          rounds.sort(key=lambda x: iter_name_sort_key(x),reverse=True)
          return(rounds[0])

#
# comparison of final N50s:
#


def add_report_n50_detail(rec,fh,tempdir):
     print("### N50s")
     i=0
     key={}
     hostname=socket.gethostname()
     plotfile=tempfile.NamedTemporaryFile(suffix=".txt",dir=tempdir,delete=False)
     clauses=[]
     plotcommand="""set term "pdf"
set output "{}/n50_detail.pdf"
set log xy
set title "HiRise Scaffold N50, before and after"
set xlabel "Max scaffold length x"
set ylabel "Fraction of length of scaffolds in those with L < x"
set grid xtics lc rgb "#bbbbbb" lw 1 lt 0
set key b
set key l
plot [100:][0.01:] """.format(tempdir)
     
     project=rec.get('project','?')
     machine,path=rec.get('path').split(':')
#     iter0_n50s = n50(open("{}/hirise.out".format(td)),N=False,filterstr="slen")

     print(rec.get('project','?'),rec.get('path',"?"))
#     key[i]={'project': rec.get('project','?'), 'path':rec.get('path',"?"),'td':td}

     td=tempfile.mkdtemp(dir=tempdir)    
     if not hostname==machine:
          try:
#hirise.gapclosed.fasta.length
               cmd = "scp {}:{}/{} {}".format(machine,path,"hirise.gapclosed.fasta.length",td)
               output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
               cmd = "scp {}:{}/{} {}".format(machine,path,"raw_lengths.txt",td)
               output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
               iter0_n50=n50(open("{}/hirise.gapclosed.fasta.length".format(td)),N=False,column=0)
               raw_n50=n50(open("{}/raw_lengths.txt".format(td)),N=False,column=1)
          except:
               print("# could not scp {}:{}/hirise.out".format(machine,path))
     else:
          iter0_n50=n50(open("{}/hirise.gapclosed.fasta.length".format(path)),N=False,column=0)
          raw_n50=n50(open("{}/raw_lengths.txt".format(path)),N=False,column=1)

     datafile1=tempfile.NamedTemporaryFile(suffix=".txt",dir=tempdir,delete=False)
     for r in iter0_n50:
          datafile1.write("{}\t{}\n".format(r[1],r[4]).encode())
     datafile1.flush()

     datafile2=tempfile.NamedTemporaryFile(suffix=".txt",dir=tempdir,delete=False)
     for r in raw_n50:
          datafile2.write("{}\t{}\n".format(r[1],r[4]).encode())
     datafile2.flush()

     clauses.append( "\"{}\" u 1:2 w steps t \"{}\"".format(datafile1.name,"Final") )
     clauses.append( "\"{}\" u 1:2 w steps lt 3 t \"{}\"".format(datafile2.name,"Starting") )

     clauses.append("0.5 lt 0 t ''")
     clauses.append("0.1 lt 0 t ''")
     if True:
          plotcommand+=" , ".join(clauses)
          print(plotcommand)
          print(plotfile)
          plotfile.write(plotcommand.encode())
          plotfile.flush()
          cmd = "gnuplot {}".format(plotfile.name)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
          print(output)
          print(error)
          #fh.write("\n".encode)
     #     fh.write("\n* Smoo\n".format(tempdir).encode())
          fh.write("\n\n\\includegraphics[width=50em]{{{}/n50_detail.pdf}}\n\n(Note that this N50 plot excludes scaffolds < 1kb.)\n".format(tempdir).encode())

#
# comparison of final N50s:
#


def add_report_n50_comparisons(records,fh,tempdir):
     print("### N50s")
     i=0
     key={}
     hostname=socket.gethostname()
     plotfile=tempfile.NamedTemporaryFile(suffix=".txt",dir=tempdir,delete=False)
     clauses=[]
     plotcommand="""set term "pdf"
set output "{}/n50_comparison.pdf"
set log xy
set title "HiRise Scaffold N50 comparison"
set xlabel "Max scaffold length x"
set ylabel "Fraction of length of scaffolds in those with L < x"
set grid xtics lc rgb "#bbbbbb" lw 1 lt 0
set key b
set key l
plot [100:][0.01:] """.format(tempdir)

     for rec in records:
          td=tempfile.mkdtemp(dir=tempdir)
          print(rec.get('project','?'),rec.get('path',"?"))
          key[i]={'project': rec.get('project','?'), 'path':rec.get('path',"?"),'td':td}
          fn = "{}/{}".format(td,"all_smoothed_corrected.merged_histogram")
          project=rec.get('project','?')
          machine,path=rec.get('path').split(':')
          if hostname==machine:
#               cmd = "ls -lrt {path}/hirise.out".format(path=path)
#               cmd = "cp {}/{} {}".format(path,"hirise.out",td)
#               cmd = "ls -lrt {path}/hirise.out".format(path=path)
#               output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
               #print(cmd,output)
               try:
                    key[i]['n50s']=n50(open("{}/hirise.out".format(path)),N=False,filterstr="slen")
               except:
                    print("# could not open {}/hirise.out".format(path))
          else:
#               print(output)
               try:
                    cmd = "scp {}:{}/{} {}".format(machine,path,"hirise.out",td)
                    output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
                    key[i]['n50s']=n50(open("{}/hirise.out".format(td)),N=False,filterstr="slen")
               except:
                    print("# could not scp {}:{}/hirise.out".format(machine,path))

          if 'n50s' in key[i]:
               datafile=tempfile.NamedTemporaryFile(suffix=".txt",dir=tempdir,delete=False)
               for r in key[i]['n50s']:
                    datafile.write("{}\t{}\n".format(r[1],r[4]).encode())
               datafile.flush()
          i+=1
          clauses.append( "\"{}\" u 1:2 w steps t \"{}\"".format(datafile.name,project) )
     #print(key)

     clauses.append("0.5 lt 0")
     clauses.append("0.1 lt 0")
     if True:
          plotcommand+=" , ".join(clauses)
          print(plotcommand)
          print(plotfile)
          plotfile.write(plotcommand.encode())
          plotfile.flush()
          cmd = "gnuplot {}".format(plotfile.name)
          output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
          print(output)
          print(error)
          #fh.write("\n".encode)
     #     fh.write("\n* Smoo\n".format(tempdir).encode())
          fh.write("\n\n\\includegraphics[width=70em]{{{}/n50_comparison.pdf}}\n\n(Note that this N50 plot excludes scaffolds < 1kb.)\n".format(tempdir).encode())



#
# comparison of insert distributions:
#


def add_report_histogram_comparisons(records,fh,tempdir):
     i=0
     key={}
     hostname=socket.gethostname()
     plotfile=tempfile.NamedTemporaryFile(suffix=".txt",dir=tempdir,delete=False)
     clauses=[]
     plotcommand="""set term "pdf"
set output "{}/histo_comparison.pdf"
set log xy
set title "Smoothed, contiguity-corrected Chicago pair separation counts"
set xlabel "Separation"
set ylabel "Density"
plot  """.format(tempdir)

     for rec in records:
          td=tempfile.mkdtemp(dir=tempdir)
          print(rec.get('project','?'),rec.get('path',"?"))
          key[i]={'project': rec.get('project','?'), 'path':rec.get('path',"?"),'td':td}
          fn = "{}/{}".format(td,"all_smoothed_corrected.merged_histogram")
          project=rec.get('project','?')
          machine,path=rec.get('path').split(':')
          if hostname==machine:
               cmd = "cp {}/{} {}".format(path,"all_smoothed_corrected.merged_histogram",td)
               output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
          else:
               cmd = "scp {}:{}/{} {}".format(machine,path,"all_smoothed_corrected.merged_histogram",td)
               output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()



          i+=1
          clauses.append( "\"{}\" u 1:2 w l t \"{}\"".format(fn,project) )
     print(key)
     plotcommand+=" , ".join(clauses)
     print(plotcommand)
     print(plotfile)
     plotfile.write(plotcommand.encode())
     plotfile.flush()
     cmd = "gnuplot {}".format(plotfile.name)
     output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
     print(output)
     print(error)
     #fh.write("\n".encode)
#     fh.write("\n* Smoo\n".format(tempdir).encode())
     fh.write("\n\n\\includegraphics[width=70em]{{{}/histo_comparison.pdf}}\n\n".format(tempdir).encode())
     fh.write("\n\n".encode())


if __name__=="__main__":
     import argparse

     parser = argparse.ArgumentParser()

     #parser.add_argument('-i','--input')
     parser.add_argument('-d','--debug'  ,default=False,action="store_true")
     parser.add_argument('--flat'        ,default=False,action="store_true",help="Dump a table only, not a report as PDF")
     parser.add_argument('--focus'       ,default=False,help="[Optional] Specify the path to the assembly that is the focus of the report in host_name:/path/to/run/dir, for example --focus bigdove1:/mnt/scratch2/projects/prairie_chicken/hirise_005")
     parser.add_argument('--external'    ,default=False,action="store_true",help="Produce a shorter version of the report, for sharing with outsiders.")
     parser.add_argument('-o','--outfile',default=False,help="Output file (pdf)")
     parser.add_argument('--iter',default=False,help="Which hirise.out file to focus on. If False, report the furthest along.")

     args = parser.parse_args()

     td=tempfile.mkdtemp()
     fh=tempfile.NamedTemporaryFile(suffix=".org",dir=td,delete=False)

     #import the definition strings of the metrics
     help_strings = stats_helpstrings()

     #
     # Read json-format lines from STDIN.  Each line corresponds to an assembly.
     #
     row_ids={}
     rr=[]
     while True:
          l=sys.stdin.readline()
          if not l: break
          if l[0]=='#': continue
          hh = json.loads(l.strip())
          rr.append(hh)
          #print(hh)
          for k in hh.keys(): row_ids[k] = row_ids.get(k,0)+1

#     print(head_counts)
     #
     # Sort them by run directory creation time.
     #
     rr.sort(key=lambda x: datetime.datetime.strptime(x.get('date',"Sat Apr 21 11:58:02 2012"), "%a %b %d %H:%M:%S %Y") )

     focal_rec={}
     if args.focus:
          for r in rr:
               print(r.get('path'))
               if r.get('path') == args.focus:
                    focal_rec = r

     if args.flat:
          for r in rr:
               for k,v in r.items():
                    print("{}\t{}".format(k,v))
          exit(0)

     #
     # This dictionary stores a tuple for each metric:  a sort key that determines the order in which they are listed,
     # and a function that formats the thing.
     #
     column_config={     
          "project"              :(1,False),
          "path"                 :(2,lambda x: re.sub("_","\_","..."+x[-20:])),
          "date"                 :(2.2,False),
          "user"                 :(2.6,False),
          "genome_size"          :(3,lambda x: "{:0.03f} Gb".format(x/1e9)),
          "chicago_coverage_1_50":(4,lambda x: "{:0.03f} X".format(x)),
          "chicago_coverage_5_15":(4,lambda x: "{:0.03f} X".format(x)),
          "input_n50"            :(5,lambda x: "{:0.03f} Kb".format(x/1e3)),
          "iter0_n50"            :(6,lambda x: "{:0.03f} Mb".format(x/1e6)),
          "selected_round_n50"   :(6.3,lambda x: "{:0.03f} Mb".format(x/1e6)),
          "improvement"          :(6.5,lambda x: "{:0.01f} X".format(x)),
          "maxN50seen"           :(6.4,lambda x: "{:0.03f} Mb".format(x/1e6)),
          "total_n_reads"        :(7,lambda x: "{:,}".format(x)),
          "N"                    :(8,lambda x: "{:0.02f} Million".format(x/1e6)),
          "Nn"                   :(9,lambda x: "{:0.02f} Million".format(x/1e6)),
          "pn"                   :(10,lambda x: "{:0.02f} %".format(x*100.0)),
          "total_n_chicago_links":(11,lambda x: "{:,}".format(x)),
          "hirise_version"       :(12,False), 
          "short_contigs_length" :(13,lambda x: "{:0.03f} Mb".format(x/1e6)),
          "n_chicago_links_e50k" :(14,lambda x: "{:,}".format(x)),
          "median_max_links"     :(15,False),

          "total_depth_masked"   :(16,lambda x: "{:0.03f} Mb".format(x/1e6)),
          "mean_shotgun_depth"   :(17,lambda x: "{:0.01f} X".format(x)),
          "modal_shotgun_depth"  :(18,False),

          "n_blacklisted_links"  :(19,lambda x: "{:,}".format(x)),
          "total_n_breaks"       :(20,lambda x: "{:,}".format(x)),
          "broken_n50"           :(21,lambda x: "{:0.03f} Kb".format(x/1e3)),
          "total_n_joins"        :(22,lambda x: "{:,}".format(x)),
          "all_iters_n50"        :(22,lambda x: str(x)[:15]),
          "coverage_quantiles"   :(23,False),
          "largest_partition_sizes": (24,lambda x: str(x)[:15])
     }

     column_display_subs={'iter0_n50':'First iteration N50', 'selected_round_n50':'Final N50', 'chicago_coverage_1_50': 'Est. Chicago depth (1-50kb)', 'maxN50seen':'Max intermediate N50'}
     external_stats=['project',"genome_size","input_n50","iter0_n50","improvement","total_n_breaks","chicago_coverage_1_50","selected_round_n50"]
     
     #
     # Add locally-computed combinations of the stats here:
     #
     sppl=['raspberry','human','chm1','cassava','prairie_chicken','gator','croc','hydra','polar_bear','miscanthus','whale','subset','hirise_Z', 
           'pigeon', 'ragweed','surfperch','vampirebat','tropicalis','vampire_bat','tumbler']
     for r in rr:
          tsp="-"
          for sp in sppl:
               if re.search(sp,r['path']):
                    tsp=sp
          r['project']=tsp
          r['final_iteration']    = get_selected_round(r,args.iter)
          r['selected_round_n50'] = r.get('all_iters_n50',{}).get(r['final_iteration'])
          if not 'all_iters_n50' in r: r['selected_round_n50'] = r['broken_n50']
          r['maxN50seen'] = r.get('all_iters_n50',{}).get( get_maxN50_round(r) )
          print(r['selected_round_n50'] )
          #r['improvement'] = r['iter0_n50'] / r.get('input_n50',1)
          r['improvement'] = r['selected_round_n50'] / r.get('input_n50',1)


     row_ids['project']=1
     row_ids['improvement']=1
     row_ids['selected_round_n50']=1
     row_ids['final_iteration']=1
     row_ids['maxN50seen']=1
#     row_ids['total_n_scaffolds']=1
#     row_ids['longest_scaffold_length']=1
     help_strings.append(['project',"Project name."])
     help_strings.append(['improvement',"Ratio of the scaffold N50 after HiRise assembly to the starting scaffold N50."])
     help_strings.append(['selected_round_n50',"N50 for the final HiRise scaffolding round."])

     columns = list(row_ids.keys())

     columns.sort(key=lambda x: column_config.get(x,(200,0))[0])

     def make_bold(s):
          return " *{}* ".format(s)
     def format_cell(s,c,rrec):
          fmt = lambda x: x
          if args.focus and args.focus == rrec.get('path'): fmt=make_bold
          if column_config.get(c,(False,False))[1]:
               try:
                    r = column_config[c][1](s)
               except:
                    r="-"
               return fmt(r)
          else:
               return fmt(str(s))


     #
     # Start writing the org-mode file:
     #
     fh.write("""#+OPTIONS:   H:0 num:t toc:nil \\n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+LaTeX_CLASS: article
#+LaTeX_CLASS_OPTIONS: [legalpaper,landscape,utopia,8pt,listings-sv,microtype,paralist,DIV=15,BCOR=15mm]
#+LaTeX_HEADER: \\usepackage[legalpaper, total={12in, 7in}]{geometry}

#+TITLE:

""".encode())

     if args.focus:
          fh.write("* HiRise Scaffolding Report for {}\n\n".format(focal_rec.get('project')).encode())          
     else:
          fh.write("* HiRise Scaffolding Report\n\n".encode())

#
# Write the big table:
#

     if args.focus:
          fh.write("* Summary statistics, compared to selected other assemblies:\n\n".encode())
     for rown in range(0,len(rr),6):
          fh.write( '\n\n'.encode() )
          for c0 in columns:
               if args.external and not c0 in external_stats: continue
               if c0 in column_display_subs:
                    c = column_display_subs[c0]
               else: 
                    c=c0
               if c in ['coverage_quantiles','coverage_histogram']: continue
               outstr= "| "+" | ".join(map(str,[c+":"]+[ format_cell( r.get(c0,'-'),c0,r) for r in rr[rown:rown+6] ])) + " |\n"
               outstr = re.sub("_"," ",outstr)
               #print(outstr)
               fh.write( outstr.encode() )

     

     if args.focus:
          add_report_n50_detail(focal_rec,fh,td)
     
     if not args.external:
          add_report_histogram_comparisons(rr,fh,td)

     try:
          add_report_n50_comparisons(rr,fh,td)
     except:
          pass

     #
     #  Add a subsection for each individual assembly:
     #  (td is the tempdir, fh is the handle for the report output
     #fh.write("\n\\newpage\n* Run details\n\n".encode())

     if not args.external:
          for report_row in rr:
               fh.write("\n\\newpage\n".encode())

               fh.write("* Run details: {} {}\n\n".format(report_row.get('project',"?"),report_row.get('path',"?")).encode())

               fh.write("** Iteration history:\n".encode())
               add_iteration_table(report_row,fh,td)

               fh.write("** Number of 5-15kb Chicago pairs covring sampled position:\n".encode())
               add_report_coverage_histogram(report_row,fh,td)

               fh.write("** Insert fit:\n".encode())
               add_insert_fit_fig(report_row,fh,td)     

     #
     # Add a table with the definitions of the metrics.
     #
     fh.write("\n\\newpage\n* Key to the metrics:\n\n".encode())

     for c0,h in help_strings:
          if args.external and not c0 in external_stats: continue
          if c0 in column_display_subs:
               c = column_display_subs[c0]
          else: 
               c=c0
          fh.write("|{}|{}|\n".format(re.sub("_"," ",c),h).encode()) 


     fh.flush()
     org2pdf(fh,args)

     try:
          pass
          if not args.debug: 
               fh.close()

               shutil.rmtree(td)  # delete directory
     except OSError as exc:
          print("wwtf?")
          if exc.errno != errno.ENOENT:  # ENOENT - no such file or directory
               raise  # re-raise exception

