#!/usr/bin/env python3


import re
import argh
import os
import shutil
from datetime import date
import subprocess
import sys
import gzip

def main(in_table="/dev/stdin",out_table="/dev/stdout",fasta="hirise_iter_broken_3.gapclosed.fasta"):
    in_fh = open(in_table,"r")
    out_fh = open(out_table,"w")
    run_id = "Sc"+get_id(fasta)
    for line in in_fh:
        parts = line.strip().split()
        scaf_id = re.search("\d+",parts[0]).group()
        new_id = "".join([run_id,"_",scaf_id])
        parts[0] = new_id
        print("\t".join(parts),file=out_fh)
    in_fh.close()
    out_fh.close()



def get_id(fastafile):
    if fastafile.endswith(".gz"):
        open_with = gzip.open
        mode = "rt"
    else:
        open_with = open
        mode = "r"
    for line in open_with(fastafile, mode):
        if line.startswith(">"):
            m=re.match(">Sc(.*)_",line)
            if m:
                return m.group(1)


if __name__ == "__main__":
    argh.dispatch_command(main)
