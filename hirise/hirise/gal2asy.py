#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
import sys
import re

#ATF     1                                       
#10      7                                       
#Type=GenePix ArrayList V1.0                                             
#BlockCount=3                                            
#BlockType=3                                             
#"Supplier=MYcroarray.com, Ann Arbor, MI, USA"                                           
#ArrayerSoftwareName=RotoSynth                                           
#ArrayerSoftwareVersion=1.0                                              
#ArrayName=CUST-90K-140131-Detroit-BrandonRice-DovetailGenomics-layout                                           
#"Block1=2279, 15800, 35, 162, 110, 187, 82"                                             
#"Block2=2139, 31235, 35, 162, 110, 190, 82"                                             
#"Block3=2032, 46895, 35, 162, 110, 187, 82"                                             
#Block   Row     Column  Name    ID      ControlType     GeneName
#1       1       1       MY-QC   noAa    pos     
#1       1       2       MY-QC   noCa    pos     
#1       1       3       MY-QC   noGa    pos     
#1       1       4       MY-QC   noTa    pos     
#1       1       5       MY-QC   Bota    pos     


data=False
block_info ={}
while True:
    l = sys.stdin.readline()
    if not l: break
    if not data:
        m = re.search("\"Block(\d)=(.*)\"",l)
        if m:
            x = list(map(int,m.group(2).split(', ')))
            print(x)
            block_info[int(m.group(1))]= x
        m = re.search("^Block\s+Row\s+Column",l)
        if m: data=True
    else:
        c = l.strip().split()
        if len(c)<4: break
        block,row,column = list(map(int,c[:3]))
#        print block,row,column,block_info[block]
        x0 = block_info[block][0]
        y0 = block_info[block][1]
        diam = block_info[block][2]
        x = x0 + column*block_info[block][4]
        y = y0 + row*block_info[block][6]
        print("\t".join(map(str,[x,y,c[3]])))

