#! /bin/bash

n=0
while read f
do 
  n=$((n+1));
  dir=`ls -d ${n}_*`
  cd $dir
  #nucmer -p ${dir} aligned_regions.fasta break_region.fasta &&  mummerplot --png -p ${dir} --filter ${dir}.delta > mummer.out 2> mummer.err && convert input_break_region.png final_break_region.png ${dir}.png combined.pdf  && 
  nucmer -p finalBreakRegions aligned_regions.fasta final_break_regions.fasta  && mummerplot --png -p finalBreakRegions --filter finalBreakRegions.delta && convert input_break_region.png final_break_region.png ${dir}.png finalBreakRegions.png combined.pdf &
  cd ..
done < <(cut -f 1 $1)
wait;
