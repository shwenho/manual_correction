#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
#!/usr/bin/env python3
import sys
import networkx as nx
import greedy_chicagoan as gs


def update_end_distance(end_distance,n,g):
    x=0
    q=[n]
    seen={}
    last=False
    while len(q)>0:
        m=q.pop(0)
        if last:
            x+=g[last][m]['length']
        last=m
        if (not m in end_distance) or end_distance[m]>x: end_distance[m]=x
        
        seen[m]=True
        for l in g.neighbors(m):
            if not l in seen:
                q.append(l)

#    print end_distance

#def llr(e1,e2):
#    return 1.0

L=200000.0

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-L','--links')
    parser.add_argument('-1','--first')
    parser.add_argument('-2','--second')
    parser.add_argument('-s','--scaffolds')
    parser.add_argument('-S','--alreadyDone')
    parser.add_argument('-b','--besthits')
    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')
    parser.add_argument('-F','--filter')
    parser.add_argument('-K','--slices',default=1,type=int)
    parser.add_argument('-k','--slice' ,default=0,type=int)

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    ll={}
    if args.lengths:
        f = open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            c=l.strip().split()
            ll[c[0]]=int(c[1])
        f.close()

    g=nx.Graph()
    if args.scaffolds:
        f=open(args.scaffolds)
        while True:
            l=f.readline()
            if not l: break
            c=l.strip().split()
            if c[0]=="#edge:":
                g.add_edge(c[1],c[2],eval(" ".join(c[3:])))
                print("#add edge",c[1],c[2],eval(" ".join(c[3:])))

    sys.stdout.flush()
    sc=1
    scaffold={}
    for c in nx.connected_components(g):
        for cc in c:
            scaffold[cc]=sc
            scaffold[cc[:-2]]=sc
        sc+=1

    end_distance={}
    ends={}
    for n in g.nodes():
        if g.degree(n)==1:
            update_end_distance(end_distance,n,g)
            ends[scaffold[n]] = ends.get(scaffold[n],[])+[n]

    print("#done setting edge distances")
    sys.stdout.flush()

    scaffold_pairs_tested={}
#Scaffold50016_1 Scaffold40593_1 ['Scaffold77744_1.5', 'Scaffold246520_1.5'] ['Scaffold111955_1.3', 'Scaffold216064_1.3'] 1141 455 1 15 1
    if args.alreadyDone:
        f=open(args.alreadyDone)
        while True:
            l=f.readline()
            if not l: break
            if l[0]=="#": continue
            if "link score"==l[:10]: continue
            c=l.strip().split()
            print("# skip",int(c[-5]),int(c[-4]))
            s1=scaffold[c[0]]
            s2=scaffold[c[1]]
            scaffold_pairs_tested[s1,s2]=True
            scaffold_pairs_tested[s2,s1]=True
        f.close()


    links={}
    if args.links:
        if args.links=="-":
            f = sys.stdin
        else:
            f = open(args.links)
        nlinks=0
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            c1,c2=c[0],c[1]
            if c1 in scaffold and c2 in scaffold and (not scaffold[c1] == scaffold[c2]) and ( min(end_distance[c1+".3"],end_distance[c1+".5"])<L ) and ( min(end_distance[c2+".3"],end_distance[c2+".5"])<L ) and (not ( scaffold[c1],scaffold[c2] ) in scaffold_pairs_tested):
                links[c1,c2]=eval(" ".join(c[5:]))
                nlinks+=1
                if nlinks%10000==1: print("#nlinks=",nlinks)
            else:
                pass
                #print c1,c2,ll[c1],ll[c2],eval(" ".join(c[5:]))
        if args.links=="-":
            pass
        else:
            f.close()
 


    link_scores={}
    gs.ll=ll
    gs.links=links
    pairs_to_test=list(links.keys())
    if args.first:
        pairs_to_test = [(args.first,args.second)]
#    for c1,c2 in links.keys():
#    for c1,c2 in [("Scaffold68143_1","Scaffold42944_1"),("Scaffold232867_1","Scaffold519668_1"),("Scaffold82730_1","Scaffold59156_1")]:
    for c1,c2 in pairs_to_test:
        if (scaffold[c1],scaffold[c2]) in scaffold_pairs_tested: continue
        if not scaffold[c1]%args.slices == args.slice : continue
        scaffold_pairs_tested[scaffold[c1],scaffold[c2]]=True
        scaffold_pairs_tested[scaffold[c2],scaffold[c1]]=True
#        scaffold_pairs_tested[c2,c1]=True
        print(c1,c2,ends[scaffold[c1]],ends[scaffold[c2]],scaffold[c1],scaffold[c2],scaffold[c1]%args.slices,scaffold[c2]%args.slices,args.slice)
        sys.stdout.flush()
        for e1 in ends[scaffold[c1]]:
            for e2 in ends[scaffold[c2]]:
#                link_scores[e1,e2]=[ gs.link_test(g,e1,e2,gap) for gap in (1, 1000,5000,10000, 20000, 35000, 50000, 80000, 100000,150000,200000,500000 ) ]
                link_scores[e1,e2]=[ gs.link_test(g,e1,e2,gap) for gap in ( 10000, 30000, 50000 ) ]
                print("link score",e1,e2,"\t".join(map(str,link_scores[e1,e2])))
                sys.stdout.flush()


#                            sc[x,y] = link_test(og,x,y)
#link score Scaffold68143_1.5 Scaffold42944_1.3 -12.0363618331   -8.50975484023  3.09050796551   13.1466146475   23.6521102192
#link score Scaffold232867_1.5 Scaffold519668_1.3 14.2053772843  17.5334920011   28.5300425318   38.1409186507   48.2455365211
#link score Scaffold82730_1.3 Scaffold59156_1.3 19.032925025     22.7060494622   34.76613982     45.137277127    55.5327333446
#link score Scaffold139910_1.5 Scaffold88540_1.5 18.4718988438   22.0553877336   33.8485414561   44.045490051    54.4456783858
#link score Scaffold264145_1.3 Scaffold163676_1.5 58.5394818429  61.4418869438   70.6603831852   77.9258584168   83.0945142366
#link score Scaffold48407_1.3 Scaffold136888_1.5 43.5898317791   46.6738612148   56.8244456982   65.4217034062   73.3441397183
#link score Scaffold113693_1.5 Scaffold61032_1.5 23.6894794909   27.0904366266   38.3018179122   47.9797590877   57.7542115065
#link score Scaffold125405_1.5 Scaffold158227_1.3 24.2942327515  27.8092174693   39.3813872363   49.3913743569   59.5690031451
