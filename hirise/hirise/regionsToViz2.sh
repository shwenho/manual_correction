#! /bin/bash


#parallel --dryrun --gnu "examine_layout.py {} -i assembly0.hra | vis.py | mpl_oovis.py --save break{#}/input_break_region.png; echo {} > break{#}/input_region.txt" :::: <(cut -f 1 $1)
#parallel --dryrun --gnu "examine_layout.py {} -i hirise_iter_broken_3.hra | vis.py -A hirise_iter_broken_3.hra | mpl_oovis.py --save break{#}/final_break_region.png; echo {} > break{#}/final_region.txt" :::: <(cut -f 2 $1)

#parallel --dryrun --gnu "region=$(echo {} | cut -f 1);breaks=$(echo {}|cut -f 2); examine_layout.py ${region} ${breaks} -i assembly0.hra | vis.py -i assembly0.hra | mpl_oovis.py --save input_break_region.png; echo {} > break{#}/input_region.txt" 

IFS=$'\n'
n=0
for f in $(cat $1);
do
    n=$((n+1))
    region=$(echo ${f} | cut -f 1)
    breaks=$(echo ${f} | cut -f 2)
    hra=$(echo ${f} | cut -f 3)
    in_scaf=$(echo ${f} | cut -f 4)
    echo "examine_layout.py ${region} ${breaks} -i ${hra} | vis.py -A ${hra} | mpl_oovis.py --save ${in_scaf}_${n}_break_region.png "
    echo ${breaks} | cat -T
    examine_layout.py ${region} ${breaks} -i ${hra} | vis.py -A ${hra} | mpl_oovis.py --save ${in_scaf}_${n}_break_region.png &
    if [ $((n%8)) = 0 ]
    then
       wait;
    fi 
done;

wait;
