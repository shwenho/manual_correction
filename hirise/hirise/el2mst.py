#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx


def printdot(g,c,n):
    gg = nx.subgraph(g,c)
    f=open("%d.txt" % (n), "wt")
    nn=1
    lab0={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]
        lab0[d]=lab0.get(d,nn)
        nn+=1
    lab={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]

        lab[x]=p + str(lab0[d])

    f.write( "graph G {\n")
    for e in gg.edges():
#        f.write( "\t\"%s\" -- \"%s\";\n" % (lab[e[0]],lab[e[1]]) ) #,gg[e[0]][e[1]]['weight'])
        f.write( "\t\"%s\" -- \"%s\" [label=\"%d\"];\n" % (lab[e[0]],lab[e[1]],int(gg[e[0]][e[1]]['weight'])))
    f.write( "};\n")

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-q','--minquality',default=10.0,type=float)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))

    n=0
#    for c in nx.connected_components(G):
#        if len(c)>4:
#            n+=1
#            print c
#            printdot(G,c,n)

    t= nx.minimum_spanning_tree(G)
    for c in nx.connected_components(t):
        if len(c)>4:
            n+=1
            print(c)
            printdot(t,c,n)
