#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math
import random

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

args=False


def random_range(G,slen,L):
#    print "X"
    r = G*random.random()
    t=0
    i=0
    while t+slen[i]<r:
        t+=slen[i]
        i+=1
    s = snam[i]
    l = slen[i]
    R = "%s:%d-%d"%(snam[i],int(r-t),int(r-t+L))
    print(R)
#    print "\t".join(map(str,[G,r,s,l,R]))

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-p','--progress',action='store_true',default=False)
    parser.add_argument('-d','--debug',action='store_true',default=False)
    parser.add_argument('-n','--nwindows',default=10,type=int)
    parser.add_argument('-s','--seed',default=None)
    parser.add_argument('-l','--length',default=3000000,type=int)


    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    random.seed(args.seed)

    sam=pysam.Samfile(args.bamfile)

    if args.progress: log( "opened %s" % args.bamfile )

    h=sam.header
    seqs=h['SQ']

    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]
    G = float(sum(slen))

    if args.progress: log(  "built length and name map arrays" )   

    for i in range(args.nwindows):
        random_range(G,slen,args.length)

