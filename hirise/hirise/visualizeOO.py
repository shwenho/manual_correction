#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from past.utils import old_div
import pysam
import sys
import math

N = 3
#nmask = 2**N - 1
nstates = 2**N
nmask = nstates -1


if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-S','--string',required=True)
    parser.add_argument('-l','--lengths',required=True)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

#    if args.progress: log( str(args) )

#    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))
#    print "Done reading edgelist"

    length={}    
    f = open(args.lengths)
    while True:
        l = f.readline()
        if not l:   break
        if l[0]=="#": continue
        c=l.strip().split()
        length[c[0]]=int(c[1])
#        print c[0],length[c[0]]
    f.close()

    f = open(args.string)
    strings = []
    start={}
    strand={}
    total=0.0
    nodes=[]
    gapsize=5000
    while True:
        l = f.readline()

        if not l:   break
        if l[0]=="#": continue

        c=l.strip().split()
        nodes.append(c[0])
        strand[c[0]]=int(c[1])
        start[c[0]]=total
        total+=length[c[0]]+gapsize
#        print c[0],strand[c[0]],start[c[0]],total
        
    f.close()

    scores={}
    for n1 in nodes:
        for n2 in nodes:
            scores[n1,n2]=[]

#scf896052939 scf895944060 7468 1941 2 [(6558, 1429), (6863, 1197)]

    while True:
        l = sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c = l.strip().split()
        if len(c)<5: continue
        if (c[0],c[1]) in scores:
            scores[c[0],c[1]]= eval(" ".join(c[5:]))
#            print scores[c[0],c[1]]
        if (c[1],c[0]) in scores:
            pp = eval(" ".join(c[5:]))
            scores[c[1],c[0]]= [ (p[1],p[0]) for p in pp ] 
            #            print scores[c[1],c[0]]

    def bp2mb(x):
        return old_div(float(x),1.0e6)

    print("size(2000);")
    for n in nodes:
        #print "#",map(bp2mb,(start[n],start[n],start[n]+length[n],start[n]+length[n]))
        if strand[n]==0:
            print("draw(({0},{0})--({1},{1}),EndArrow);".format( old_div(float(start[n]),1e4), old_div(float(start[n]+length[n]),1e4) )) 
        else:
            print("draw(({0},{0})--({1},{1}),BeginArrow);".format( old_div(float(start[n]),1e4), old_div(float(start[n]+length[n]),1e4) )) 
        print("draw((0,{0})--({1},{0}));".format( old_div(float(start[n]),1e4), old_div(total,1e4) )) 
        print("draw(({0},0)--({0},{1}));".format( old_div(float(start[n]),1e4), old_div(total,1e4) )) 
        print("label(\"{0}\",({1},{1}),SE);".format( n,old_div(float(start[n]+old_div(length[n],2)),1e4) ))
        
            #            print "draw((%f,%f)--(%f,%f),BeginArrow);"   % map(bp2mb,(start[n],start[n],start[n]+length[n],start[n]+length[n]))
            #            print "draw((%d,%d)--(%d,%d),BeginArrow)" % ()

#    for n1 

#    print "t=scale(0.1);"
    for n1 in nodes:
        for n2 in nodes:
            #scores[n1,n2]=[]
            for x,y in scores[n1,n2]:
                if strand[n1]==1:
                    x = length[n1]-x
                if strand[n2]==1:
                    y = length[n2]-y
                print("dot(({0},{1}),blue);".format( old_div((start[n1]+x),1e4), old_div((start[n2]+y),1e4) ))

