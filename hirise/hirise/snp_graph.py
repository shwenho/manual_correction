#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import range
#!/usr/bin/env python3
import pysam

import networkx as nx

def read_variant_positions(fn,region=False):
    d={}
    f=open(fn,"r")
    while True:
        l=f.readline()
        if not l:
            break
        c = l.strip().split()
        #print c
        x = int(c[1])
        if (not region) or ((c[0]==region[0]) and (  x>region[1][0] and x<region[1][1]  )):
            d[( c[0],int(c[1]) )]=c[0]+":"+c[1]
            d[ c[0]+":"+c[1] ] = int(c[1])

    
    return d



if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True,action="append",default=[])
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-r','--region',default=False)
    parser.add_argument('-g','--gff',default=False)
    parser.add_argument('-v','--vcf',default=False)

    args = parser.parse_args()

    if args.debug:
        print(args)

    region=False
    if args.region:
        c,r = args.region.split(":")
        s,t = r.split("-")
        region = (c,(int(s),int(t)))
        print(region)

    snps={}
    if args.vcf:
        snps=read_variant_positions(args.vcf,region)

    if args.debug:
        for c,p in snps:
            print(c,p)

    t2snp = {}
    hist={}

    for bamfile in args.bamfile:

        sam=pysam.Samfile(bamfile,"rb")

        if args.debug:
            print(sam.header)

        h=sam.header
        seqs=h['SQ']

        n=0


        for aln in sam.fetch(region=args.region,until_eof=True):
            if args.debug:
                print(aln.tlen,aln.pnext,seqs[aln.rnext]['SN'],seqs[aln.tid]['SN'],aln.qname,aln.rname,aln.inferred_length,aln.seq)
                print(aln)
    #            print dir(aln)

            c = seqs[aln.tid]['SN']
            pp=[]
            for p in aln.positions:
                if (c,p) in snps:
                    pp.append( c+":"+str(p) )
            if pp:
                if aln.qname not in t2snp:
                    t2snp[aln.qname] = []
                #print pp 
                t2snp[aln.qname].extend(pp)

            if args.insertHist and aln.rnext == aln.tid:
               hist[aln.tlen] = hist.get(aln.tlen,0)+1
            n+=1
            if args.head and n>args.head:
                if args.debug:
                    print("reached",args.head)
                break

        sam.close()

    g = nx.Graph()
    for pp in list(t2snp.values()):
        if len(pp)>1:
#            print pp
            for i in range(len(pp)):
                for j in range(i):
                    g.add_edge( pp[i],pp[j] )


    i=0
    for c in nx.connected_components(g): # connected_compontents(g):
        i+=1
        psn = [ snps[x] for x in c ]
        print("cc:",i,len(c),min(psn),max(psn),max(psn)-min(psn))
        
    if args.gff:
        f=open(args.gff,"wt")
        f.write("browser position chr%s\n" % (args.region))
        f.write("""browser hide all
track name=haplotypes description="Phased groups of SNPs"
visibility=2\n""")
        
        i=0
        for c in nx.connected_components(g): # connected_compontents(g):
            i+=1
            #psn = [ snps[x] for x in c ]
            print("cc:",i,len(c),min(psn),max(psn),max(psn)-min(psn))
            for x in sorted(c):
                ch,po = x.split(":")
                f.write("chr%s\tx\tSNP\t%s\t%s\t.\t.\t.\t%s\n" % (ch,po,po,"c"+str(i)))
        f.close()


    lengths = list(hist.keys())
    lengths.sort()
    for l in lengths:
        print("%d\t%d" %(l,hist[l]))
