#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import chicago_edge_scores as ces
from scipy.stats import poisson

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-M','--model')
parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-S','--sensitivity',default=False,type=float)
parser.add_argument('-Y','--specificity',default=False,type=float)
parser.add_argument('-n','--pn',default=False,type=float)

args = parser.parse_args()
if args.progress: print("#",args)


fmodel=open( args.model )
contents = fmodel.read()
try:
     fit_params=eval(contents)
except:
     "couldn't deal with option", args.param
fmodel.close
if args.pn: fit_params['pn']=args.pn
ces.set_exp_insert_size_dist_fit_params(fit_params)

l=args.length

n_bar   = ces.model.n_bar(l,l,0)
n_noise = ces.model.pn*ces.model.N*l*l/(ces.model.G**2)

noise_upper_cutoff =poisson.ppf( 1.0-args.specificity ,n_noise)
signal_lower_cutoff=poisson.ppf( args.sensitivity    ,n_bar)
fraction_not_obs   =poisson.cdf( noise_upper_cutoff, n_bar)

print("\t".join(map(str,["#L","G","pn","N","n_bar","n_noise","noise_upper_cutoff","signal_lower_cutoff","fraction_not_obs"])))
print("\t".join(map(str,[l,ces.model.G,ces.model.pn,ces.model.N,n_bar,n_noise,noise_upper_cutoff,signal_lower_cutoff,fraction_not_obs])))
