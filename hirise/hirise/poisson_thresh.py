#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from past.utils import old_div
import sys
import argparse
from scipy.stats import poisson
import math
parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-P','--percentile',default=0.99,type=float)
#parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

x=0.0001
delta = old_div(math.log(10),10.0)

while x < 101.0:
     print(x, poisson.ppf(args.percentile,x))
     x = math.exp( math.log(x)+delta )
