#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import os
import time
from hirise.coverageQuantiles import find_coverage_quantiles
from hirise.coverage_hist_tools import coverage_histogram
from hirise.n50stats import n50, n50list
from hirise.fasta2contiglengths import contig_lengths
from pwd import getpwuid
import glob
import json
import pysam

model={}
config={}


def total_masked_depth():
     x=0
     for line in open("deep_regions.txt"):
          c=line.strip().split()
          x+=int(c[2])-int(c[1])
     return x

def all_iters_n50():
     d={}
     for f in glob.glob("hirise_iter*[0-9].out"):
          d[f]=n50(open(f),50.0,filterstr="slen")
     return d
          

def iter0_n50():
     if os.path.isfile("hirise.out"):
          return n50(open("hirise.out"),50.0,filterstr="slen")
     elif os.path.isfile("scaffolder_test.out"):
          return n50(open("scaffolder_test.out"),50.0,filterstr="slen")
     else:
          return(-1)

def total_n_joins():
     if os.path.isfile("hirise_iter_broken_3.out"):
          fh=open("hirise_iter_broken_3.out")
     elif os.path.isfile("hirise.out"):
          fh=open("hirise.out")
     elif os.path.isfile("scaffolder_test.out"):
          fh=open("scaffolder_test.out")
     else:
          return(-1)

     nscaffolds=0
     ncontigs=0
     for l in fh:
          if "slen" in l:
               nscaffolds+=1
          if l[:2]=="p:":
               ncontigs+=0.5
     return int(ncontigs - nscaffolds)

def total_n_breaks():
     nraw=0
     nbroken=0
     for line in open("raw_lengths.txt"): nraw+=1
     for line in open("broken_lengths.txt"): nbroken+=1
     return nbroken-nraw

def short_contig_length():
     r=0
     for line in open("broken_lengths.txt"): 
          if line[0]=="#": continue
          c=line.strip().split()
          l=int(c[1])
          if l<=1000:
               r+=l
     return r

def n_blacklisted_links():
     n=0
     for f in glob.glob("links/*.blacklist"):
          for l in open(f):
               if l[0]=="#": continue
               c=l.strip().split()
               n+=int(c[4])
     return n

def total_n_reads():
     n=0
     for b in list(config.get('BAM_FILES',{}).values()) + list( config.get('bam_files',{}).values()):
          sam=pysam.Samfile(b)
          #print("#total n: {}\t{}".format(b,sam.mapped))
          n+=sam.mapped
     return n

def total_n_shotgun():
     n=0
     shotgun_bam = config.get('SHOTGUN_BAM',"")
     if not shotgun_bam:
         shotgun_bam = config.get('shotgun_bam',"")
     if shotgun_bam:
         sam = pysam.Samfile(shotgun_bam)
         n = int((sam.mapped + sam.unmapped)/2)
     return n

def n_raw_links():
     n=0
     for f in glob.glob("links/*.merged.rawlinks"):
          for l in open(f):
               if l[0]=="#": continue
               c=l.strip().split()
               n+=int(c[4])
     return n

def n_chicago_links_endfilt(w):
     nn=0
     for f in glob.glob("links/*.merged.links"):

          for l in open(f):
               if l[0]=="#": continue
               c=l.strip().split()
               n=int(c[4])
               l1=int(c[2])
               l2=int(c[3])
               if l1<=w and l2<w: 
                    nn+=n
                    continue
               links = eval(" ".join(c[5:]))
               for a,b in links:
                    if not ( ((a<w) or ((l1-a)<w)) and ((b<w)or((l2-b)<w)) ): continue
                    nn+=1
     return nn

def median_max_links():
     n=0
     vv=[]
     for f in glob.glob("links/*.merged.links"):
          max_links={}
          for l in open(f):
               if l[0]=="#": continue
               c=l.strip().split()
               n=int(c[4])
               if int(c[2])<1000: continue
               if max_links.get(c[0],0)<n:
                    max_links[c[0]]=n
          vv.extend(list(max_links.values()))
     vv.sort()
     return vv[int(len(vv)/2)]

def histogram_mean(fn):
     a=0
     nn=0
     for l in open(fn):
          c=l.strip().split()
          n=int(c[1])
          a+=n*int(c[0])
          nn+=n
     return a/nn

def histogram_mode(fn):
     a=0
     nn=0
     maxx=0
     maxy=-1
     mode=0
     for l in open(fn):
          c=l.strip().split()
          x,y = int(c[0]),int(c[1])
          if maxy<y:
               maxy=y
               mode=x

     return mode

def get_coverage_histogram():
     merged_bams ={}
     for k,v in config.get('BAM_FILES',{}).items():
          merged_bams[k]=v
     for k,v in config.get('bam_files',{}).items():
          merged_bams[k]=v
     
     if len(merged_bams.keys()) > 1:
	     return dict ( [ (bamid,coverage_histogram(5000,[pysam.Samfile(merged_bams[bamid])],1000,100000,10.0,5000)) for bamid in merged_bams.keys() ] + [("combined", coverage_histogram(5000,[pysam.Samfile(merged_bams[bamid]) for bamid in merged_bams.keys()],1000,100000,10.0,5000))])
     else:
	     return dict ( [ (bamid,coverage_histogram(5000,[pysam.Samfile(merged_bams[bamid])],1000,100000,10.0,5000)) for bamid in merged_bams.keys() ] )


def get_version_info():
     lines = open("version_info.txt").readlines()
#     return "{}, {}".format(lines[1].strip(),lines[0].strip())
     return "{}".format(lines[1].strip())

def largest_partition_sizes():
     sizes=[]
     for l in open("component_x.txt"):
          if not l.startswith("c:"): continue
          c=l.strip().split()
          n=int(c[3])
          sizes.append(n)
     sizes.sort(reverse=True)
     return (sizes[:10])

def contigN50(filename):
     buffer=list( contig_lengths(open(filename,"rt")) )
     return(n50list(buffer,50.0))


import socket

stats_handler_registry = {
     'date':                 [ lambda : time.ctime(os.path.getctime(os.getcwd())),          "Creation time of the run directory." ],  
     'path':                 [ lambda : "{}:{}".format(socket.gethostname(),os.getcwd()),   "hostname:filsystem path of the run directory." ],  
     'user':                 [ lambda : getpwuid(os.stat(os.getcwd()).st_uid).pw_name,      "Username of the owner of the run directory" ],  
     'total_n_joins':        [ lambda : total_n_joins(),                                    "How many joins were made to create iter0" ], 
     'total_n_shotgun':        [ lambda : total_n_shotgun(),                                "How many shotgun read pairs used." ], 
     'mean_shotgun_depth':   [ lambda : histogram_mean("raw_chunks/00.dhist"),              "Mean of the histogram of shotgun depths." ],  
     'modal_shotgun_depth':  [ lambda : histogram_mode("raw_chunks/00.dhist"),              "Mode of the histogram of shotgun depths." ],  

#     'mean_links_accepted_joins':  [ lambda : "to be implemented", "" ],  ## TODO
     'n_chicago_links_e50k': [ lambda : n_chicago_links_endfilt(50000),                     "How many chicago pairs in total span between contig ends (within 50kb of an end)" ], ## TODO

     'median_max_links':     [ lambda : median_max_links(),                                 "Median of the distribution of the number of chicago links contigs have to other contig they have most links to, after depth and blacklist filtering." ],  
     'total_n_chicago_links':[ lambda : n_raw_links(), "The total number of chicago pairs that span between contigs, pre-blacklist filtering." ], 
     'n_blacklisted_links':  [ lambda : n_blacklisted_links(), "The total number of blacklisted chicago pairs (because one or both reads falls in a over-connected 1kb window.)" ], 
     'total_n_reads':        [ lambda : total_n_reads(), "The total number of mapped chicago reads." ], 
     'short_contigs_length': [ lambda : short_contig_length(), "The total length of broken contigs with length <= 1000." ], 
     'pn':                   [ lambda : model["pn"], "The estimated probability that a Chicago pair comes from the noise (after discounting short innies)." ],
     'N':                    [ lambda : model["N"], "The estimated total number of 'effective' Chicago pairs, after discounting short innies." ],
     'Nn':                   [ lambda : model["Nn"], "The estimated total number of background noise read pairs." ],
     'genome_size':          [ lambda : model["G"], "The esimated genome size. (Total length of the starting assembly.)" ],
     'hirise_version':       [ lambda : get_version_info(), "The `git describe` string for the dovetail codebase used when the run was started." ],
     'coverage_quantiles':   [ lambda : find_coverage_quantiles(model,1000,200000,4),"For bins of equal estimated total clone coverage spanning the range 1-200Kb.  For each bin, the coverage, start of range in bp, end of range in bp and cumulative coverage are listed.  This is all based on the fit to the separation distribution."],
     'coverage_histogram':   [ lambda : get_coverage_histogram() ,"Histogram of the number of spanning chicago pairs with separations in the range 1kb-100kb."],
     'chicago_coverage_1_50':   [ lambda : find_coverage_quantiles(model,1000,50000,1)[0][0],"The total estimated clone coverage in 1-50kb chicago pairs, based on the model fit."],
     'chicago_coverage_5_15':   [ lambda : find_coverage_quantiles(model,5000,15000,1)[0][0],"The total estimated clone coverage in 5-15kb chicago pairs, based on the model fit."],
     'total_n_breaks':       [ lambda : total_n_breaks(),"The total number of breaks introduced into the starting contigs (misassembly detection)"],
     'input_contig_n50':     [ lambda : contigN50(contig_fasta_file()),"Contig N50 of the starting assembly."],
     'broken_contig_n50':    [ lambda : contigN50("broken.fa")        ,"Contig N50 of the starting assembly after breaking."],
     'iter0_contig_n50':     [ lambda : contigN50("hirise.fasta")     ,"Contig N50 of the iter0 fasta sequence."],
     'gapclose_contig_n50':  [ lambda : contigN50("hirise.gapclosed.fasta"),"Contig N50 of the gapclosed iter0 fasta sequence."],
     'input_n50':            [ lambda : n50(open("raw_lengths.txt"),50.0,column=1),"The N50 length of the starting assembly."],
     'broken_n50':           [ lambda : n50(open("broken_lengths.txt"),50.0,column=1),"The N50 length of the starting assembly after breaking."],
     'iter0_n50':            [ lambda : iter0_n50(),"The N50 length of the iter0 scaffolds."],
     'all_iters_n50':        [ lambda : all_iters_n50(),"N50s for all iterations."],
     'largest_partition_sizes':  [ lambda : largest_partition_sizes(),"The sizes of the largest contig partitions."],
     'total_depth_masked':   [ lambda : total_masked_depth(),"The total length in bp of the segments of the input contigs that is being masked due to excess depth of shotgun reads."]
}

def contig_fasta_file():
     if 'CONTIG_FASTA' in config:
          return config['CONTIG_FASTA']
     elif 'contig_fasta' in config:
          return config['contig_fasta']
     else:
          return None

def stats_helpstrings():
     r= [ [command, stats_handler_registry.get(command,["","?"])[1] ] for command in stats_handler_registry.keys()]
     return r

slow_ones=['total_n_chicago_links','n_blacklisted_links','median_max_links','n_chicago_links_e50k']
#slow_ones=['median_max_links','n_chicago_links_e50k']

def main():

     parser = argparse.ArgumentParser(description="""known stats: {}\n\n""".format(list(stats_handler_registry.keys()))+".  A brief description of each of the stats computed:  " + ".\n".join([ ": ".join( [command,stats_handler_registry.get(command,["","?"])[1]]) for command in stats_handler_registry.keys()]))
     parser.add_argument('commands', metavar='stat', nargs='*', help='the name of a stat to extract')
     parser.add_argument('-d','--debug',default=False,action="store_true")
     parser.add_argument('-v','--verbose',default=False,action="store_true",help='Show the description of each ')
     parser.add_argument('--all',default=False,action="store_true",help="Compute all the stats I know how to compute.")
     parser.add_argument('--json',default=False,action="store_true",help="Write the output as JSON.")
     parser.add_argument('-S','--skipslow',default=False,action="store_true",help="Skip the slow ones: {} [overrides --all]".format(",".join(slow_ones)))
     parser.add_argument('--cwd',metavar='dir',help="Give the path to a hirise run to test")
     parser.add_argument('-o','--outfile',metavar='file',help="Write output to file.")

     args = parser.parse_args()
     global config
     global model
     commands=[]
     if args.commands:
          commands = args.commands
     elif args.all:
          commands = list(stats_handler_registry.keys())

     if args.cwd: 
          os.chdir(args.cwd)

     f=open("datamodel.out")
     model=eval(f.read())

     try:
          config=json.loads(open("config.json").read())
     except:
          pass

#     print(config)
     results={}
     for command in commands:
          if args.skipslow and command in slow_ones: 
               if args.debug: print("#{} :: skipping as slow".format(command))
               continue
          if args.debug: print("#extract stat: {}".format(command))
          try:
               results[command]=stats_handler_registry.get(command,[ lambda: "Unrecognized stat" ] )[0]()
          except Exception as e:
               print("# exception getting stat %s: " % (command),e,file=sys.stderr)
               pass

     #print(results)


     if args.json:
          if args.outfile:
               fh=open(args.outfile,"wt")
               fh.write(json.dumps(results))
               fh.write("\n")
               fh.close()
          else:
               print(json.dumps(results))
     else:
          commands=sorted(results.keys())
          for command in commands:
               result=results[command]
               if args.verbose:
                    print("{}:\t{}\t{}".format(command,result,stats_handler_registry.get(command,["","?"])[1]))
               else:
                    print("{}:\t{}".format(command,result))

if __name__=="__main__":
     main()

