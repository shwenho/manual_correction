#!/usr/bin/env python3


import argh
import networkx as nx

def main(hra_file):
    graph = hra2graph(hra_file)
    for edge in graph.edges():
        print(edge, graph.edge[edge[0]][edge[1]]["length"], graph.edge[edge[0]][edge[1]]["contig"])


def hra2graph(hra_file):
    g = nx.Graph()
    for scaffold in parse_hra(hra_file):
        previous_contig = None
        previous_orientation = None
        previous_start = None
        broken_start = None
        for contig, orientation, broken_start, start in scaffold:
            contig_end = contig + "_" + str(broken_start) + "." + orientation
            if previous_contig:
                length_difference = start - previous_start
                is_contig=False
                if previous_contig == contig:
                    is_contig=True
                previous_end = previous_contig + "_" + str(previous_broken_start) +  "." + previous_orientation
                g.add_edge(contig_end, previous_end, 
                           {"contig": is_contig, "length": length_difference})
            previous_contig = contig
            previous_orientation = orientation
            previous_start = start
            previous_broken_start = broken_start

    return g

def parse_hra(hra):
    with open(hra) as handle:
        scaffold_contigs = []
        previous_scaffold = None

        for line in handle:
            if line.startswith("P "):
                scaffold = line.split()[1]
                contig = line.split()[2]
                broken_start = line.split()[3]
                orientation = line.split()[4]
                start = int(line.split()[5])
                if previous_scaffold != scaffold:
                    if previous_scaffold:
                        yield scaffold_contigs
                    previous_scaffold = scaffold
                    scaffold_contigs = []
                scaffold_contigs.append((contig, orientation, broken_start, start,))
        if previous_scaffold:
            yield scaffold_contigs

if __name__ == "__main__":
    argh.dispatch_command(main)
