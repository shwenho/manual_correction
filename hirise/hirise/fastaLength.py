#!/usr/bin/env python3
from __future__ import print_function
import sys

inp=sys.stdin

if len(sys.argv)>1:
#    print sys.argv[1]
    inp = open(sys.argv[1])

sname=0
leng=0
while True:
    l = inp.readline()
    if not l: break
    if l[0]==">":
        if sname: 
            print("%s\t%d" % (sname,leng))
            sys.stdout.flush()
        c=l.strip().split()
        sname = c[0][1:]
        leng=0
    else:
        leng+=len(l.strip())
print("%s\t%d" % (sname,leng))



