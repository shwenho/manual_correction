#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import sys
import colorsys

if __name__=="__main__":


    import argparse
    parser = argparse.ArgumentParser()


#    parser.add_argument('-m','--map')
    parser.add_argument('-b','--besthits')
#    parser.add_argument('-s','--scaffold')
    parser.add_argument('-d','--debug',action="store_true")

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    mapp=False
 
    if args.besthits:
        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
            f.close()

    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[:2]=="p:":
            c=l.strip().split()
            if c[2][:-2] in besthit:
                bh=besthit[c[2][:-2]]
                c[4]=bh[1]
                if c[2][-1]=="5":
                    if bh[2]=="-":
                        c[5]=bh[4]
                    else:
                        c[5]=bh[3]
                else:
                    if bh[2]=="-":
                        c[5]=bh[3]
                    else:
                        c[5]=bh[4]



                c[8]=str(bh)
                print("\t".join(c[:9]))
            else:
                print(l.strip())
            
