#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
import sys
import argparse
import glob
import re
import subprocess
import shlex
import os


if __name__=="__main__":

     parser = argparse.ArgumentParser()
     #parser.add_argument('-i','--input')
     parser.add_argument('-d','--debug',default=False,action="store_true")
     parser.add_argument('-p','--progress',default=False,action="store_true")
     parser.add_argument('--simple',default=False,action="store_true")
     parser.add_argument('-l','--lengthfile', metavar='FILENAME' ,default =[], action="append", help='Plot lengths from these files')
     parser.add_argument('outfile', metavar='FILENAME', nargs=1,help='name of the plot file to create')
     parser.add_argument('-t','--title',default=False,help="Plot title")
     parser.add_argument('--png',default=False,help="Export plot as .png instead of .pdf")
     parser.add_argument('--latex',default=False,action="store_true",help="Export plot as .tex instead of .pdf")

     args = parser.parse_args()
     if args.progress: print("#",args)

     outfile=args.outfile[0]

     scaffolder_outputs=[]
     for f in glob.glob("hirise.out") + glob.glob("hirise_iter_*[0-9].out"):
          scaffolder_outputs.append(f)
     scaffolder_outputs.sort()

     def plotfn(s):
          return '"< cat {0} | grep slen | n50stats.py" u 2:5 w steps t "{0}"'.format(s)

     def plotfn3(s):
          return '"< cat {0} | n50stats.py" u 2:5 w steps t "Final scaffolds" lt rgb "blue"'.format(s)

     def plotfn2(s):
          return '"< cat {0} | awk \'{{print $2}}\' | n50stats.py" u 2:5 w steps t "Input Scaffolds" lt rgb "red"'.format(s)

     input_stats = ['raw_lengths.txt','broken_lengths.txt']

     if args.title:
          print("set title '{}'".format(args.title.replace("_"," ").title()))
     print("set key b")
     if args.png:
         print("set term 'png'")
     elif args.latex:
         print("set term 'epslatex' size 8in,5in")
     else:
         print("set term 'pdf'")
     print("set output '{}'".format(outfile))
     print("set log y")
     print("set log x")
     print("set xlabel '{}'".format("X = Contig or scaffold minimum length. (bp)"))
     if args.latex:
         print("set ylabel '{}'".format("Fraction in pieces \\textless~ than X."))
     else:
         print("set ylabel '{}'".format("Fraction in pieces < than X."))
     print("set decimal locale")
     print("""set format x "%'g" """)
     print("set ytics scale 1.5,0.75")
     print("set log y2")
     print("set y2tics 1,1,2")
     print("set y2tics add ('N50 Length' 0.5) font '{,8}'")
     print("set y2tics add ('N90 Length' 0.1) font '{,8}'")
     if args.simple:
          input_stats = ['raw_lengths.txt']
          print("plot [1000:][0.01:] "+" , ".join(map(str,[plotfn2(f) for f in input_stats]))+" , "+" , ".join(map(str,[ plotfn3(f)  for f in args.lengthfile ])) + ', 0.5 t "" lt 0, 0.1 t "" lt 0')
     else:
          print("plot [1000:][0.01:] "+" , ".join(map(str,[plotfn2(f) for f in input_stats]))+" , "+" , ".join(map(str,[ plotfn(f)  for f in scaffolder_outputs ])) + ',  0.5 t "" lt 0, 0.1 t "" lt 0')
          

