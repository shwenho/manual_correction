#!/usr/bin/env python3

import argh
import random
import math


def main(output="/dev/stdout", update=1, temperature=1000, size=10):
    best_possible, initial_state = get_state(size)
    print("Initial score: ", score(initial_state))
    best = anneal(initial_state, temperature, update)
    print("initial:", score(initial_state), "final:", score(best), "best:", score(best_possible))

def anneal(initial_state, temperature, update):
    state = initial_state
    while temperature > 0:
        for update_round in range(update):

            new_state = perturb(state)
            new_score = score(new_state)
            old_score = score(state)
            #print("starting temp", temperature, "round", update_round, "new score", new_score, "old score", old_score)
            if new_score > old_score:
                state = new_state
                #print("new state: ", state)
                print("new score: ", new_score, "temperature", temperature)
            elif new_score < old_score:
                if accept(new_score, old_score, temperature):
                    state = new_state
                    #print("new state: ", state)
                    print("new score: ", new_score, "temperature", temperature)
                
        temperature -= 1
    return state
            
def perturb(state):
    i = random.randint(0, len(state)-1)
    j = random.randint(max(i-5, 0), min(i+5, len(state)-1))
    # new copy
    new_state = state[:]
    try:
        new_state[i], new_state[j] = state[j], state[i]
    except IndexError:
        print(i, j, len(state), len(new_state))
        raise IndexError
    return new_state

def get_state(size):
    vals = list(range(size))
    perturbed = vals[:]
    for i in range(1000):
        perturbed = perturb(perturbed)
    return vals, perturbed

def score(state):
    score = 0
    for i, j in zip(state, range(len(state))):
        if i == j:
            score += 100
        elif abs(i - j) < 10:
            score += 10 - abs(i - j)
    return score

def accept(new_score, old_score, temperature):
    test = random.random()
    cutoff = math.exp(-(old_score - new_score)/temperature)
    return test < cutoff


if __name__ == "__main__":
    argh.dispatch_command(main)
