#!/usr/bin/env python3
from builtins import object

class BamTags(object):

    @staticmethod
    def mate_mapq(line):
        try:
            return line.opt("xm")
        except KeyError:
            return line.opt("MQ")

    @staticmethod
    def mate_start(line):
        pos = line.mate_pos
        if line.mate_is_reverse:
            pos = pos + line.opt("xs")
        return pos

    @staticmethod
    def orientation(line):
        return line.opt("xt")

    @staticmethod
    def junction(line):
        return line.opt("xj")
