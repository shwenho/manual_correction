#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
from past.utils import old_div
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-R','--region',default=False)
parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

cname,xy=args.region.split(":")
x,y=list(map(int,xy.split("-")))

#print cname,x,y

state="out"
s=""
while True:
     l=sys.stdin.readline()
     if not l: break
     if l[0]==">":
          c=l.strip().split()
          name=c[0][1:]
          if name==cname and state=="out": 
               state="in"
          elif state=="in":
               break
     elif state=="in":
          s+=l.strip()

#print "#",len(s)
os= s[x-1:y-1]
sys.stderr.write(str(len(os))+"\n")

print(">"+args.region)
for i in range(old_div(len(os),100)):
     print(os[i*100:(i+1)*100])
print(os[((old_div(len(os),100)))*100:])
