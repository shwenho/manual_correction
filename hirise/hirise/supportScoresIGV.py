#!/usr/bin/env python3
import sys
import argparse
import hirise.chicago_edge_scores as ces
from chicago_support_bootstrap import pairs2support
from hirise_assembly import HiriseAssembly


def scaffoldSupportToIGV(scaffold,support,slen,fh,prefix=None):
    prev_score = 0
    prev_end = 0
    feature = "support"
    num_lines = 0
    if prefix:
        scaffold = prefix + scaffold
    for pos,score in support:
        print("\t".join(map(str,[scaffold,prev_end,pos,feature,prev_score])),file=fh)
        prev_end,prev_score = pos,score
    print("\t".join(map(str,[scaffold,prev_end,length,feature,prev_score])),file=fh)
    

comment="""def scaffoldSupportToIGV(scaffold,support,slen,fh,index_fh,index_step):
    prev_score = 0
    prev_end = 0
    feature = "support"
    num_lines = 0
    n_steps = slen/index_step
    steps = [index_step * i for i in range(n_steps-1,0,-1)]
    indices = []
    for pos,score in support:
        while True:
            if pos > steps[-1]:
                indices.append(fh.tell())
                steps.pop()
            else:
                break
        print("\t".join(map(str,[scaffold,prev_end,pos,feature,prev_score])),file=fh)
        prev_end,prev_score = pos,score
    print("\t".join(map(str,[scaffold,prev_end,length,feature,prev_score])),file=fh)
    steps = [index_step * i for i in range(n_steps)]
    prev_index = None
    prev_pos = None
    de_dup = []
    for index,pos in zip(indices,steps):
        if prev_index != index:


        prev_index = index
        prev_pos = pos"""

if __name__=="__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
    parser.add_argument('-q','--mapq',default=20,  type=float,help="Minimum map quality threshold.")
    parser.add_argument('-m','--minsep',default=500,  type=int,help="Hide short pairs for speed.")
    parser.add_argument('-X','--maxsep',default=200000,  type=float,help="Max pair sep.")

    parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
    parser.add_argument('-A','--hra',required=True,help="Assembly file.")
    parser.add_argument('-s','--scaffolds',action="append",help="Only output support for these scaffolds.")
    parser.add_argument('-o','--output',default="supportScores.igv",type=argparse.FileType('w'),help="Output file for support scores in IGV format.")
    parser.add_argument('--prefix',default=None,help="Prefix to prepend to scaffold IDs in HRA file (i.e. HiRise identifier).")
    parser.add_argument('-l','--logfile',default="pairs2support.log",type=argparse.FileType('w'),help="Output file for pairs2support logging info.")

    args = parser.parse_args()
    if args.progress: print("#",args)

    hra = HiriseAssembly()
    hra.load_assembly(args.hra)
    if len(hra.layout_lines) == 0:
        hra.make_trivial_layout_lines()
    ces.set_exp_insert_size_dist_fit_params(hra.model_params)
    model = ces.model
    if args.scaffolds:
        targets = [(scaffold,hra.scaffold_lengths[scaffold]) for scaffold in args.scaffolds]
    else:
        targets = hra.scaffold_lengths.items()

    print("\t".join(["Scaffold","Start","End","Feature","Support"]),file=args.output)
    for scaffold,length in targets:
        points=[]
        for point in hra.chicago_pairs_for_scaffolds(mapq=args.mapq,callback=False,bamfile=False,scaffolds=[scaffold],contigs=False,minsep=args.minsep,maxsep=args.maxsep,debug=args.debug):
            scaf,nscaf,xx,yy,c1,c2,query_name = point
            points.append((xx,yy))
        support = pairs2support(scaffold,points,model,slen=length,logfile=args.logfile,masked_segments=hra.make_scaffold_mask(scaffold),maxx=args.maxsep,minx=args.minsep,binwidth=1500,support_curve=True,debug=args.debug)
        scaffoldSupportToIGV(scaffold,support,length,args.output,args.prefix)
    
    args.output.close()
    args.logfile.close()
