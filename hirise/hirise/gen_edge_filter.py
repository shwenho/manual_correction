#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from past.utils import old_div
import sys
import networkx as nx


def shave_round(G):
    leaves=[]
    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if G.degree(n)==1:
            leaves.append(n)
#    print leaves

    round=1
#    print "#",leaves
    boundary=list(leaves)
    next=[]
    done=[]
    r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next):
                    next.append(nn)
#                    r[nn]=round
        for b in next: 
            if len( [nn for nn in G.neighbors(b) if nn not in r] )==1: r[b]=round
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if n in r: boundary.append(n)
#        next=[]
    for n in G.nodes():
        if n not in r: r[n]=round
    return r




if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-t','--threshold',default=0.0 ,  type=float)

    parser.add_argument('-H','--head',default=False,type=int)
#    parser.add_argument('-D','--savetreedots',default=False,action='store_true')
    parser.add_argument('-d','--debug',default=False,action='store_true')
#    parser.add_argument('-I','--nointerc',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-M','--maxdegree',default=False,type=int)
    parser.add_argument('-m','--minlength',default=500,type=int)

    parser.add_argument('-b','--besthits')
    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')



    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    ll={}
    if args.lengths:
        f = open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            ll[c[0]]=int(c[1])

        f.close()

    if args.besthits:
        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
    #            print c[1],besthit[c[1]]
            f.close()

    G=nx.Graph()
    SG=nx.Graph()

    if args.edgefile:
        f = open(args.edgefile)
    else:
        f=sys.stdin
    while True:
        l = f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        u,v,w = c[0],c[1],float(c[2])
        if ( not args.lengths ) or (ll[u]>=args.minlength and ll[v]>=args.minlength):
            G.add_edge(u,v,weight=-w)
            if w >= args.threshold:
                SG.add_edge(u,v,weight=-w)
    if args.edgefile:
        f.close()

    print("#Done reading edgelist")

    bad_nodes=[]
    total_discarded_length=0
    total_discarded_length1=0
    n_discarded1=0
    n_discarded2=0
    if args.maxdegree:
        for n in SG.nodes():
            print("#dg:", SG.degree(n))
            if SG.degree(n)>args.maxdegree:
                n_discarded1+=1
                bad_nodes.append(n)
                total_discarded_length += ll[n]
                print("#discard:",n,ll[n],SG.degree(n))
                for nn in SG.neighbors(n):
                    if SG.degree(nn)==1:
                        n_discarded2+=1
                        total_discarded_length1+=ll[nn]
        for n in bad_nodes:
            e_to_remove=[]
            for e in SG.edges([n]):
                e_to_remove.append(e)
            SG.remove_edges_from(e_to_remove)

    print("#total_discarded_length",n_discarded1,n_discarded2,old_div(float(total_discarded_length),1.0e6),old_div(float(total_discarded_length1),1.0e6),old_div(float(total_discarded_length+total_discarded_length1),1.0e6))

    strings = []
    ccn=1
    for c in nx.connected_components(SG):
        gg = nx.subgraph(G,c)
        t= nx.minimum_spanning_tree(gg)
        trim_level=shave_round(t)
        x={}
        for n in t.nodes():
            m = len([ nn for nn in t.neighbors(n) if trim_level[nn]>2 ])
            x[n]=m

        for n in list(x.keys()):
            if x[n]>2:
                print("#y:",n,x[n],[ nn for nn in t.neighbors(n) if trim_level[nn]>2 ])

        d=nx.all_pairs_shortest_path_length(t,cutoff=10)
        for kk in list(d.keys()):
            for kkk in list(d[kk].keys()):
                print(kk,kkk,d[kk][kkk])

        ccn+=1
        if args.head and ccn>args.head:
            break

#    last_group=0

#    for s in strings:
#        intercalate_strings(G,s,args.nointerc)


