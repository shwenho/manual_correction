#!/usr/bin/env python3


import argh
import os
import sys
import subprocess
import io
import json

def main(audit_merge, output="/dev/stdout", windows=[1000,5000,10000,50000,500000]):
    data = {}
    for window in windows:
        cmd = "audit_z.py -r {0} -L {1} -t 50 -T 0.95 ".format(audit_merge, window)
        p = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        num_misorient, num_misordered, total = get_misorient(p.stdout)
        window_data = {"misoriented": num_misorient,
                       "misordered": num_misordered,
                       "total": total}
        data[window] = window_data

    with open(output, 'w') as out:
        json.dump(data, out)

def get_misorient(output):
    num_misjoin = 0
    num_misorient = 0
    total = 0
    for line in output:
        line = line.decode("UTF-8")
        is_same_chrom = same_chrom(line)
        is_same_orientation = same_orientation(line)
        total += 1
        if not is_same_chrom:
            num_misjoin += 1
        if not is_same_orientation:
            num_misorient += 1
    return num_misorient, num_misjoin, total
            

def same_chrom(line):
     s = line.split()
     chrom1 = s[4].strip("(")
     chrom2 = s[10].strip("(")
     return chrom1 == chrom2

def same_orientation(line):
     #50537 49952 1 1 ('22_maternal', 42980547, '+') ('Sc_1', 6099, '+') ('22_maternal', 43031084, '-') ('Sc_1', 56051, '-')
     s = line.split()
     r1 = s[6]
     r2 = s[12]
     h1 = s[9]
     h2 = s[15]
     ref_match = r1==r2
     hirise_match = h1==h2
     return ref_match == hirise_match


if __name__ == "__main__":
    argh.dispatch_command(main)
