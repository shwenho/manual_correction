#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import str
from past.utils import old_div
import sys
import argparse
import glob
import re
import subprocess
import shlex
import os
import os.path
import json

def snakefile_params_parse(fn):
     
     f=open(fn)
     s=""
     while True:
          l=f.readline()
          if not l: break
          s+=l

     config = json.loads(s)

#     print config 
     return config

def audit_merge_orientation_errors(fn,log=False,contig_audit=False):
     lines=[]
     scountsP={}
     scountsM={}

     slistP={}
     slistM={}

     scpairs={}
     f=open(fn)
     while True:
          l=f.readline()
          if not l: break
          c=l.strip().split()
          scpairs[c[1],c[4]]=1
          if c[3]==c[6]:
               scountsP[c[1],c[4]]=scountsP.get((c[1],c[4]),0)+1

          else:
               scountsM[c[1],c[4]]=scountsM.get((c[1],c[4]),0)+1

     total=0
     wrong=0

     scaffold={}
     majority_rule_orientation={}

     for rc,ac in list(scpairs.keys()):
          if scountsP.get((rc,ac),0) + scountsM.get((rc,ac),0) >= 10 :
               wrong+= min( scountsP.get((rc,ac),0),  scountsM.get((rc,ac),0) )
               if log:
                    if scountsP.get((rc,ac),0) > scountsM.get((rc,ac),0):
                         majority_rule_orientation[rc,ac]="+"
                    elif scountsP.get((rc,ac),0) < scountsM.get((rc,ac),0):
                         majority_rule_orientation[rc,ac]="-"
                    else:
                         majority_rule_orientation[rc,ac]=0
#               if log:
#                    nwrong = min( scountsP.get((rc,ac),0),  scountsM.get((rc,ac),0) )
#                    if scountsP.get((rc,ac),0)==nwrong and nwrong > 0:
#                         for k in slistP[rc,ac]: log.write("{}\n".format(k))
#                    elif nwrong>0:
#                         for k in slistM[rc,ac]: log.write("{}\n".format(k))
                         
               total  +=  scountsP.get((rc,ac),0) + scountsM.get((rc,ac),0) 

     f.close()
     if log :
          f=open(fn)
          matches={}
          mismatches={}
          contigs={}
          while True:
               l=f.readline()
               if not l: break
               c=l.strip().split()
               contigs[c[4]]=c[1]
               #scaf = scaffold[c[4]]
               if c[3]==c[6]:
                    if majority_rule_orientation.get((c[1],c[4]),0)=="+":
                         matches[c[4]] = matches.get(c[4],0)+1
                    elif majority_rule_orientation.get((c[1],c[4]),0)=="-":
                         mismatches[c[4]] = mismatches.get(c[4],0)+1

               else:
                    if majority_rule_orientation.get((c[1],c[4]),0)=="-":
                         matches[c[4]] = matches.get(c[4],0)+1
                    elif majority_rule_orientation.get((c[1],c[4]),0)=="+":
                         mismatches[c[4]] = mismatches.get(c[4],0)+1
          f.close()
          for c in list(contigs.keys()):
               log.write("{}\t{}\t{}\n".format(c,matches.get(c,0),mismatches.get(c,0) ))
          log.close()
     return old_div(float(wrong),total),wrong,total

#TTCCATAACATTTCAATGGGGGATTGTAAGACATGGCTTGAAGACTTTTTGATATACTGGGAACAAATGCTGGATCCAACAAGTAAGTGAGAGGTGAAAAA   6_maternal      150315873       -       ScfDnTCn_1      117     +


def misjoinStats(fn):

     f=open(fn)
     lines=[]
     while True:
          l=f.readline()
          if not l: break
          c=l.strip().split()
          lines.append( ( c[1],int(c[2]) ) )

#     print lines
     lines.sort()
     seen={}
     total=0.0
     totaln=0
     for a,b in lines:
          totaln+=1
          if not a in seen:
               total+=b
#               print a,b,total
          seen[a]=1
     return [total,old_div(total,3e9),totaln]

def wcl(fn):
     return 1
     cmd = "wc -l {}".format(fn)
     output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
     return int(output.strip().split()[0])

def n50(fn):
     cmd = "n50stats.py -N 50 -i {}".format(fn)
     output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
     return float(output)


def mean(x):
     return old_div(float(sum(x)),len(x))

def correctness_Z(fn,tol=0.25):
     f=open(fn)
     ntotal=0
     ngood=0
     trues=[]
     while True:
          l=f.readline()
          if not l: break
          if l[0]=="#": continue
          c=l.strip().split()
          ntotal+=1
          if (abs(float(c[0])-float(c[1])))<tol*float(c[1]): ngood+=1
          trues.append(float(c[1]))
     
          
     return (mean(trues),old_div(float(ngood),ntotal))
#     return int(output.strip().split()[0])

def contiguity_stats_from_assembly(fn):
     results={}
     f=open(fn)
     lengths=[]
     while True:
          l=f.readline()
          if not l: break
          if "slen" in l:
               c=l.strip().split()
               l=int(c[0])
               lengths.append(l)
     lengths.sort(reverse=True)
     tl=sum(lengths)
     results['G']=tl

     rs=0
     i=0
     while rs<0.5*tl:
          rs+=lengths[i]
          i+=1
     results['n50']=lengths[i]

     while rs<0.9*tl:
          rs+=lengths[i]
          i+=1
     results['n90']=lengths[i]

     return results



def orgmode_header(title=""):
     header=r'''#+OPTIONS: toc:nil 
#+TITLE:  XXXTITLEXXX
#+LATEX_HEADER: \usepackage{amsmath}
#+LATEX_HEADER: \usepackage{xcolor,cancel}
#+AUTHOR: 
#+LATEX_HEADER:\newcommand\hcancel[2][black]{\setbox0=\hbox{$#2$}%
#+LATEX_HEADER:\rlap{\raisebox{.45\ht0}{\textcolor{#1}{\rule{\wd0}{1pt}}}}#2} 


'''
     return re.sub("XXXTITLEXXX",title,header)

def orgmode_escape(s):
     if not s: return ""
     return re.sub("_","\_",str(s))


def orgTableLine(l):
     s="| "
     s+= " | ".join(map(str,l))
     s+=" |"
     return s

def hash2table(h,formats={}):
#     print h
     rows = list(set([ i[0] for i in list(h.keys()) ]))
     cols = list(set([ i[1] for i in list(h.keys()) ]))
     #print("#"+str(rows))
     #print("#"+str(cols))
     rows.sort()
     cols.sort()
     print(orgTableLine( [ " " ] + cols ))
     for r in rows:
          print(orgTableLine( [ r ] + [ formats.get(c,"{:.3f}").format(h.get((r,c),0)) for c in cols] ))

def parse_misorient_count(f):
     with open(f) as handle:
          for line in handle:
               if line.startswith("d:"): continue
               total, number, rate = map(float, line.split())
               return rate, number, total

if __name__=="__main__":

     parser = argparse.ArgumentParser()
     #parser.add_argument('-i','--input')
     parser.add_argument('-d','--debug',default=False,action="store_true")
     parser.add_argument('-p','--progress',default=False,action="store_true")
     parser.add_argument('-L','--length',default=False,type=int)
     parser.add_argument('-A','--auditmers',default=False)

     args = parser.parse_args()
     if args.progress: print("#",args)

     total_nmers = wcl(args.auditmers)
     
     params=snakefile_params_parse("config.json")
     
     print(orgmode_header( "HiRise report for "+ orgmode_escape(os.getcwd()) ))

     print("* Assembly inputs:")
     print(" ")
     print("Extracted from "+os.path.join( os.getcwd() , "config.json" ))
     for k in list(params.keys()):
          if k=='BAMFILES': continue
          print("|\t"+"\t|\t".join(map(str,[orgmode_escape(k),str(orgmode_escape(params[k]))[:80]]))+"\t|")
     print(" ")
     
     print("Library BAM files:")
     bam_files = params.get('BAM_FILES')
     if not bam_files:
          bam_files = params.get('bam_files',{})
     keys = bam_files.keys()
     for k in list(keys):
          print(orgTableLine([orgmode_escape(k),orgmode_escape(bam_files[k])]))
     print(" ")
     

     print("* Model Fit")
     print("Fit to the library pair separation distribution:")
     print(" ")
     print("\includegraphics{model_fit.pdf}")
     print(" ")


     print("* Scaffolder outputs")
     print("** Contiguity")
     print(" ")
     #print("\includegraphics{n50plot.pdf}")
     print(" ")


     print("** Raw stats")


     scaffolder_outputs=[]
     for f in glob.glob("hirise.out") + glob.glob("hirise_iter_*[0-9].out"):
          scaffolder_outputs.append(f)

     scaffolder_outputs.sort()
     i=0
     for so in scaffolder_outputs:

          audit_mers_pat = re.sub(".out",".audit",so)
          audit_mers = glob.glob(audit_mers_pat)

          audit_mersZ_pat = re.sub(".out",".audit.*.txt",so)
          audit_mersZ = glob.glob(audit_mersZ_pat)

#          print i,so,contiguity_stats_from_assembly(so),[(wcl(x),float(wcl(x))/total_nmers) for x in audit_mers],[correctness_Z(fn) for fn in audit_mersZ]
          i+=1

     print("* Assembled vs True distances")
     print(" ")
     print("\includegraphics{50k_audit_hist.pdf}")
     print(" ")

     print("* Misjoin stats")
     print("The table shows the fraction of the total length of each assembly which falls in scaffolds with at least one global misjoin, as defined as having more than one stretch of audit mers of length at least w coming from more than one chromosome.")

     h={}
     h2={}
     h3={}
     for f in glob.glob("*.misjoins.W*.txt"):
#scaffolder_test_iter_3.misjoins.W5000.txt 
          m=re.match("(.*).misjoins.W(.*).txt",f)
          if m:
               ckpnt=m.group(1)
               minl=int(m.group(2))
               msjs=misjoinStats(f)
               lenf ="{}.fasta.length".format(m.group(1))
               if not os.path.isfile(lenf):
                    lenf = "{}.fa.length".format(m.group(1))
               n50x = n50(lenf)

               h[orgmode_escape(ckpnt),str(minl)]= msjs[1]
               h[orgmode_escape(ckpnt),'N50']= n50x
               h2[orgmode_escape(ckpnt),minl]= msjs[2]
               h3[orgmode_escape(ckpnt),minl]= old_div((old_div(3.0e9,(1+float(h2[orgmode_escape(ckpnt),minl])))),1e6)
#               print orgTableLine( [orgmode_escape(ckpnt),minl]+ misjoinStats(f) )
     print(" ")

     cols = [i[1] for i in list(h.keys())]
     

     hash2table( h )

     print("total number of misjoins:")
     hash2table( h2, dict( [(c,"{:0.0f}") for c in cols]  ) )

     print("Mean distance between misjoins (Mb):")
     hash2table( h3 )

     h4={}
     
     for f in glob.glob("*.audit_merge"):
          m=re.match("(.*).audit_merge",f)
          if m:
               ckpnt=orgmode_escape(m.group(1))
               #errr,w,t = 0.02,10.0,10.0 #audit_merge_orientation_errors(f)
               errr,w,t = audit_merge_orientation_errors(f)
               h4[ckpnt,"error rate"] = errr
               h4[ckpnt,"nwrong"] = w
               h4[ckpnt,"ntotal"] = t
     print("Fraction of 101-mers misoriented relative to majority for strand for scaffolds with at least 10 101-mers:")
     hash2table( h4, {'error rate':'{:0.4f}','nwrong':'{:0.0f}','ntotal':'{:0.0f}'} )

     h5 = {}
     for f in glob.glob("*.misorient_count.txt"):
          m=re.match("(.*).misorient_count.txt",f)
          if m:
               ckpnt=orgmode_escape(m.group(1))
               #errr,w,t = 0.02,10.0,10.0 #audit_merge_orientation_errors(f)
               errr,w,t = parse_misorient_count(f)
               h5[ckpnt,"switch error rate"] = errr
               h5[ckpnt,"nwrong"] = w
               h5[ckpnt,"ntotal"] = t
     print("Number of orientation switches:")
     
     hash2table( h5, {'error rate':'{:0.4f}','nwrong':'{:0.0f}','ntotal':'{:0.0f}'} )

     h6 = {}
     h7 = {}
     for f in glob.glob("*.misorient_auditmers.txt"):
          m=re.match("(.*).misorient_auditmers.txt",f)
          if m:
               ckpnt=orgmode_escape(m.group(1))
               with open(f) as handle:
                    data = json.load(handle)
               for window, values in data.items():
                    total = values["total"]
                    misorient = values["misoriented"]
                    misordered = values["misordered"]
                    if total == 0:
                         orient_fraction = 0
                         join_fraction = 0
                    else:
                         orient_fraction = misorient/total
                         join_fraction = misordered/total
                    
                    h6[ckpnt, window] = join_fraction
                    h7[ckpnt, window] = orient_fraction
                    
     print("fraction of misjoined auditmer pairs")
     cols = [i[1] for i in list(h6.keys())]
     hash2table(h6, dict( [(c,"{:0.4f}") for c in cols]  ) )

     print("fraction of misoriented auditmer pairs")
     cols = [i[1] for i in list(h7.keys())]
     hash2table(h7, dict( [(c,"{:0.4f}") for c in cols]  ) )
