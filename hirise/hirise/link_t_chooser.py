#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import sys
import networkx as nx
import hirise.chicago_edge_scores as ces


if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-m','--minlength',default=1000,type=int)
    parser.add_argument('-M','--maxdegree',default=10,type=int)
    parser.add_argument('-q','--quantile',default=0.99,type=float)
    parser.add_argument('-p','--pt',default=0.1,type=float)

    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')

    args = parser.parse_args()

    ll={}
    if args.lengths:
        f = open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            l = int(c[1])
            ll[c[0]]=int(c[1])

        f.close()


    n_links={}
    weights={}
    max_weight={}
    if args.edgefile:
        f = open(args.edgefile)
    else:
        f=sys.stdin
    while True:
        l = f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        u,v,w = c[0],c[1],float(c[2])
        if args.minlength > ll[u] or args.minlength > ll[v]: continue
        weights[u,v]=w
        n_links[u] = n_links.get(u,0.0)+1
        n_links[v] = n_links.get(v,0.0)+1
        if max_weight.get(u,0)<w: max_weight[u]=w
        if max_weight.get(v,0)<w: max_weight[v]=w

    if args.edgefile:
        f.close()


    promisc = {}
    for c,d in list(n_links.items()):
        if old_div(d,ll[c]) > args.pt:
            promisc[c]=True


    maxw = list(max_weight.values())
    nt=len(maxw)
    maxw.sort()
    z=int(old_div(nt,2))
    med= maxw[z]
    ntf=len([ x for x in maxw if x < old_div((2.0*med),5.0)])
    w=int(0.18*nt)
    t0=maxw[w]
    print(med,maxw[w], old_div((2.0*med),5.0),ntf,nt,old_div(float(ntf),nt),[ len([ x for x in maxw if x < (f*med)]) for f in (0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9) ], len([ x for x in maxw if x < 7 ]))


    def cc_sizes(weights,promisc,t,max_degree):
        g=nx.Graph()
        for (u,v),w in list(weights.items()):
            if u in promisc or v in promisc: continue
            if w >= t:
                g.add_edge(u,v)
        bad_nodes=[]
        for n in g.nodes():
            if g.degree(n)>max_degree:
                bad_nodes.append(n)
        for n in bad_nodes:
            e_to_remove=[]
            for e in g.edges([n]):
                e_to_remove.append(e)
            #G.remove_edges_from(e_to_remove)
            g.remove_edges_from(e_to_remove)

        ccns=[]
        for cc in nx.connected_components(g):
            ccns.append(len(cc))
        ccns.sort(reverse=True)
        return ccns

    results=[]
    for t in range(int(t0)-2,int(t0)+20,1):
        cc=cc_sizes(weights,promisc,t,args.maxdegree)
        print("t: {}\t{}\t{}\t{}\t{}".format(t,nt,old_div(float(cc[0]),nt),old_div(float(cc[0]),float(cc[1])),"\t".join(map(str,cc[:10]))))
        results.append( (t,old_div(float(cc[0]),nt),old_div(float(cc[0]),float(cc[1])),cc[:10]  ) )

    for r in results:
        print("\t".join(map(str,r)))


