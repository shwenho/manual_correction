#!/usr/bin/env python3

import argh
import json
import subprocess
import tempfile
import shlex
import os
import sys

def make_plot(datadict,ids,outfile,xmin,xmax,ymin,ymax):
     histfile=tempfile.NamedTemporaryFile(suffix=".txt")
     #print(ids)
     depth={}
     for id in ids:
          for k,v in datadict.get(id,{}).items():
               depth[int(k)]=1

     depths=list(depth.keys())
     depths.sort()
     for d in depths:
          v=[d]
          for id in ids:
               v.append(datadict[id].get(str(d),0))
          ob="\t".join(map(str,v))
          ob+="\n"
          #print(ob)
          histfile.write( ob.encode() )
     histfile.flush()
     #print(histfile.name)

     gpfile=tempfile.NamedTemporaryFile(suffix=".gp")
     gpstr="""
set term "pdf"
set output "{}"
set style line 1 lc rgb 'red'
set style line 2 lc rgb 'blue'
set style line 3 lc rgb 'violet'
set style line 4 lc rgb '#FF00FF'
set xlabel "depth"
set ylabel "counts"
""".format(outfile)
     gpfile.write(gpstr.encode())
     plot_clauses=[]
     i=2
     for id in ids:
          plot_clauses.append(""" "{}" using 1:{} w histeps ls {} lw 5 t "{}" """.format(histfile.name,i,i-1,id))
          i+=1
     gpcmd = "plot[{}:{}][{}:{}] ".format(xmin,xmax,ymin,ymax)+ ",".join(plot_clauses)+"\n"
     gpfile.write(gpcmd.encode())
     gpfile.flush()
     cmd = "gnuplot {}".format(gpfile.name)
     output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
     if not os.path.exists(outfile):
          print(error,file=sys.stderr)
          print("Error writing file, permissions?",file=sys.stderr)

def main(stats="stats.json", 
         xmin = "", 
         xmax = "",
         ymin = "", 
         ymax = "",
         separate = False,
         out_prefix="cov_hist"):
     fh = open(stats)
     rec = json.load(fh)
     datadict=rec.get('coverage_histogram',{})

     if separate:
         for key in datadict:
             ids = [key]
             outfile = out_prefix + "." + key + ".pdf"
             make_plot(datadict,ids,outfile,xmin,xmax,ymin,ymax)
     else:
          ids = sorted(list(datadict.keys()))
          make_plot(datadict,ids,out_prefix + ".pdf",xmin,xmax,ymin,ymax)



if __name__ == "__main__":
    argh.dispatch_command(main)
