#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import subprocess
import shlex
import tempfile


import os

data,outfile,title = sys.argv[1:]

f=open(datamodel)
model=eval(f.read())
fn=model["fn"]
print (inserts,smooth,corrected,datamodel,outfile,fn)

gnuplotcommand = """set terminal pdfcairo color
set term pdfcairo font \"Arial,8\"
set output '{outfile}'
set size square
set palette model RGB defined (0 'yellow', 1 'orange', 2 'red', 3 'purple',  4 'blue')
set colorbox
set xlabel 'Read 1 position (Mb)'
set ylabel 'Read 2 position (Mb)'
set title '{title}'
set label \"Colors above the diagonal indicate\nmin map quality.  (Scale at right.)\" at graph 0.05, graph 0.9 left front
set label \"Colors below the diagonal indicate haplotype phase.\n(Yellow indicate unknown phase.)\" at graph 0.9, graph 0.1 right front
plot '< cat {data} | sort -k3n | awk \"\\\$5 >= 0\"' u (\$1/1e6):(\$2/1e6):(\$1<\$2 ? \$5 : (1+\$3)*30)  w p palette pt 5 ps 0.05 t '$1', x+0.04 lt 0 t '40 Kb', x-0.04 lt 0 t ''
""".format( data=data, title=title, outfile=outfile )

print(  gnuplotcommand )
fh=tempfile.NamedTemporaryFile()

fh.write(gnuplotcommand.encode())
fh.flush()
print (fh.name)

cmd = "gnuplot {}".format(fh.name)
print(cmd)
output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE ).communicate()
print( output.decode('utf-8') )


