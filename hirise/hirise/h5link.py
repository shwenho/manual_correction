#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import pysam
from tables import *
import os
from itertools import chain

class ChicagoPair(IsDescription):
     contig1 = UInt32Col()
     contig2 = UInt32Col()
     x = UInt32Col()
     y = UInt32Col()

class Reference(IsDescription):
     id   = UInt32Col()
     name = StringCol(32)
     length = UInt32Col()


if __name__=="__main__":
     parser = argparse.ArgumentParser()
     #parser.add_argument('-i','--input')
     parser.add_argument('-d','--debug',default=False,action="store_true")
     #parser.add_argument('-b','--bam',required=True)
     parser.add_argument('-t','--table',required=True,help="Name of an hdf5 file created from bam with bam2table.py")
     parser.add_argument('-A','--alist',required=False,default=False,help="A name of a file containing a list of contigs to extract linking info for.")
     parser.add_argument('-a',required=False,default=False,help="The name of a contig to extract linking info for.")
     parser.add_argument('-b',required=False,default=False,help="The name of a contig; for use with -a to get linking info a particular pair a,b")
     parser.add_argument('-S','--segments',required=False,help="The name of a file containing segments into which the references in the bam file are divided.")
#     parser.add_argument('-q','--mapq',required=False,type=float,default=10)
     parser.add_argument('-m','--minl',required=False,type=float,default=False,help="The minimum length of contig for which information should be output")
#     parser.add_argument('-p','--progress',default=False,action="store_true")

     import re
     args = parser.parse_args()
#     if args.progress: print("#",args)

     from hirise.bamtags import BamTags

     h5file = open_file(args.table, mode = "r")

     groups=h5file.root._v_children
     tt=list(groups.keys())
     tt.sort()

     from hirise.chicago_edge_links2 import SegMapper

     aa,bb=args.a,args.b
     smap=False

     if args.segments:
          smap=SegMapper(args.segments)
          if args.a:
               aa=re.sub("_\d+$","",aa)
          if args.b:
               bb=re.sub("_\d+$","",bb)


     def pair_links(h5file,args_a,args_b):

          #tt=h5file.root._v_children

          l1=0
          l2=0
          nlinks=0
          llist=[]

          for gr in tt:
               b=groups[gr]

               index={}
               ll={}
               for r in b.references:
                    #print(r['id'],r['length'],r['name'])
                    name = r['name'].decode()
                    ll[name]=r['length']
                    index[name]=r['id']
               #print("#",b)

               ca=index.get(aa)
               cb=index.get(bb)

               #print("#",b.pairs.size_in_memory,b.pairs.size_on_disk)
               if smap:
                    l1 = smap.seg2len[args_a]
                    l2 = smap.seg2len[args_b]
               else:
                    l1=ll[args_a]
                    l2=ll[args_b]

               def handle(c1,c2,x,y):
                    #global nlinks,l1,l2
                    if smap:
                         c1,xx = smap.map_coord(aa,x)
                         if not c1 == args_a:
                              return False
                         c2,yy = smap.map_coord(bb,y)
                         if not c2 == args_b: 
                              return False
                    else:
                         c1,xx = args_a,x
                         c2,yy = args_b,y
                    #print("#",args_a,args_b,l1,l2,c1,c2,x,y,xx,yy)
                    llist.append((xx,yy))
                    return True

               print(ca,cb)
               for r in b.pairs.where('(contig1==ca)&(contig2==cb)',{'ca':ca,'cb':cb}):
                    c1,c2,x,y = r['contig1'],r['contig2'],r['x'],r['y']
                    print("#",c1,c2,x,y)
                    if not handle(c1,c2,x,y): continue
                    nlinks+=1

               for r in b.pairs.where('(contig1==ca)&(contig2==cb)',{'ca':cb,'cb':ca}):
                    c1,c2,x,y = r['contig2'],r['contig1'],r['y'],r['x']
                    print("#",c1,c2,x,y)
                    if not handle(c1,c2,x,y): continue
                    nlinks+=1

          return(args_a,args_b,l1,l2,nlinks,llist)


     def get_all_links(h5file,args_a):

          #tt=h5file.root._v_children

          l1=0
          l2=0
          nlinks=0
          llists={}
          l2s={}

          for gr in tt:
               b=groups[gr]


               index={}
               ll={}
               id2name={}
               for r in b.references:
                    name = r['name'].decode()
                    ll[name]=r['length']
                    index[name]=r['id']
                    id2name[r['id']]=name

               ca=index.get(aa)

               if smap:
                    l1 = smap.seg2len[args_a]
               else:
                    l1=ll[args_a]

               seen_already={}
               for r in chain( b.pairs.where('(contig1==ca)&(contig2==ca)',{'ca':ca}) , b.pairs.where('(contig1==ca)&(contig1!=contig2)',{'ca':ca}) , b.pairs.where('(contig2==ca)&(contig2!=contig1)',{'ca':ca}) ): 
#               for r in chain( b.pairs.where('(contig2==ca)&(contig1!=contig2)',{'ca':ca}) ):                    
#               for r in b.pairs.where('(contig1==ca)|(contig2==ca)',{'ca':ca}):
                    c1,c2,x,y = r['contig1'],r['contig2'],r['x'],r['y']
                    if c2==ca: 
                         c1,c2,x,y = c2,c1,y,x
#                    if c1==c2 and x>y: continue

#                    print("#",c1,c2,x,y,id2name[c1],id2name[c2],b)
                    bb = id2name[c2]

                    if smap:
                         c1,xx = smap.map_coord(aa,x)
                         if not c1 == args_a:
                              continue #return False
                         c2,yy = smap.map_coord(bb,y)
                    else:
                         c1,xx = args_a,x
                         c2,yy = bb,y

                    if not c2 in llists: llists[c2]=[]
                    
                    #
                    # The above query get links within the same input contig twice...
#                    if r['contig1']==r['contig2']:
#                         if (x,y) in seen_already: 
#                              print("##",seen_already)
#                              continue
#                         seen_already[x,y]=1

                    llists[c2].append((xx,yy))
                    if not c2 in l2s: 
                         if smap:
                              l2s[c2]=smap.seg2len[c2]
                         else:
                              l2s[c2]=ll[c2]

          return(args_a,l1,l2s,llists)

     if args.b:
          print("\t".join(map(str, pair_links(h5file,args.a,args.b) )))
     elif args.a:
          nh=0
          c1,l1,l2s,links=get_all_links(h5file,args.a)
          print("##",len(list(l2s.keys())))
          for c2 in l2s.keys():
               n = len(links[c2])
               l2 = l2s[c2]
               if c1==c2: continue
               if args.minl and (l1<args.minl or l2<args.minl): continue
               print("\t".join(map(str, [c1,c2,l1,l2,n,sorted(links[c2])] )))
               nh+=1
          print("##",nh)
     elif args.alist:
          contig_list = open(args.alist)
          while True:
               a=contig_list.readline()
               if not a: break
               a=a.strip()
#               print(a)
               aa=re.sub("_\d+$","",a)
               
               c1,l1,l2s,links=get_all_links(h5file,a)
#               print("##",len(list(l2s.keys())))
               for c2 in l2s.keys():
                    n = len(links[c2])
                    l2 = l2s[c2]
                    if c1==c2: continue
                    if args.minl and (l1<args.minl or l2<args.minl): continue
                    print("\t".join(map(str, [c1,c2,l1,l2,n,sorted(links[c2])] )))
#                    nh+=1
               
          
               
     h5file.close()


