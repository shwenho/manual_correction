#!/usr/bin/env python3

import argh

def main(hra, output="/dev/stdout"):
    hirise_scaffolds = set()
    with open(output, 'w') as output_handle:
        for line in read_hra(hra):
            if line.startswith("P"):
                split = line.split()
                scaffold = split[1]
                hirise_scaffolds.add(scaffold)
                split[1] = str(len(hirise_scaffolds))
                line = " ".join(split)
            print(line.rstrip(), file=output_handle)
                
                    
def read_hra(hra):
    with open(hra) as hra_handle:
        for line in hra_handle:
            yield line

if __name__ == "__main__":
    argh.dispatch_command(main)
