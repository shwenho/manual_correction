#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
#!/usr/bin/env python3

import pysam
import sys
import math
import networkx as nx
import random

from collections import deque
from functools import partial
from multiprocessing import Pool, Lock

lock = Lock()

debug=False 
sys.setrecursionlimit(3000)

human_accessions={'gi|568336001|gb|CM000685.2|': 'X', 'CM000669.2': '7', 'gi|568336013|gb|CM000673.2|': '11', 'gi|568336004|gb|CM000682.2|': '20', 'gi|568336011|gb|CM000675.2|': '13', 'gi|568336008|gb|CM000678.2|': '16', 'gi|568336019|gb|CM000667.2|': '5', 'gi|568336000|gb|CM000686.2|': 'Y', 'gi|568336007|gb|CM000679.2|': '17', 'CM000674.2': '12', 'gi|568336003|gb|CM000683.2|': '21', 'CM000675.2': '13', 'CM000663.2': '1', 'CM000678.2': '16', 'CM000665.2': '3', 'CM000664.2': '2', 'gi|568336010|gb|CM000676.2|': '14', 'gi|568336021|gb|CM000665.2|': '3', 'gi|568336002|gb|CM000684.2|': '22', 'CM000679.2': '17', 'gi|568336015|gb|CM000671.2|': '9', 'gi|568336012|gb|CM000674.2|': '12', 'CM000673.2': '11', 'gi|568336023|gb|CM000663.2|': '1', 'CM000681.2': '19', 'gi|568336022|gb|CM000664.2|': '2', 'CM000682.2': '20', 'gi|568336017|gb|CM000669.2|': '7', 'CM000672.2': '10', 'CM000671.2': '9', 'gi|568336016|gb|CM000670.2|': '8', 'gi|568336020|gb|CM000666.2|': '4', 'CM000686.2': 'Y', 'CM000683.2': '21', 'CM000680.2': '18', 'CM000667.2': '5', 'gi|568336009|gb|CM000677.2|': '15', 'gi|568336005|gb|CM000681.2|': '19', 'CM000666.2': '4', 'CM000670.2': '8', 'gi|568336018|gb|CM000668.2|': '6', 'CM000685.2': 'X', 'CM000684.2': '22', 'gi|568336014|gb|CM000672.2|': '10', 'CM000668.2': '6', 'CM000676.2': '14', 'gi|568336006|gb|CM000680.2|': '18', 'CM000677.2': '15'}


import re

import bisect

def min_intersections(alignment_list):
    # assumes alignment_list is ordered, returns the index of the first non-overlapping sequence for each supplied alignment
    finish = [float(l[2]) for l in alignment_list]
    start = [float(l[1]) for l in alignment_list]

    p = list()
    for i in range(len(alignment_list)):
        p.append(bisect.bisect_left(finish,start[i], 0, i) - 1) # for alignment i, return the index of the first non-overlapping sequence

    return p

def prev_soln(solns, i):
    if i >= 0:
        return solns[i]
    else:
        return 0


def pick_query_alignments(outputs, buffer_multiplier,minlength):
    # Use a weighted interval scheduling approach to see if we can pick multiple minimally-overlapping query alignments to maximize the total (alignment_bitscore)^3 of that query's alignments

    #to-do: this function can be optimized further because originally the "outputs" variable contained results from all the queries at the same time, not just from one query at a time. 
    align_map = {}
    # create the dictionary linking queries to their alignments which pass the minlength
    for line in outputs:
        ln = line.split() # we expect the input format of the lines to be "passed: query_name; summed_bitscore; subject_name; strandedness; subject_start; subject_end; query_start; query_end;  length_of_query (if supplied)"
        if float(ln[8])-float(ln[7]) >= minlength:
            align_length = float(ln[8])-float(ln[7])
            soft_edge = min(align_length, 10000)*buffer_multiplier # Try to see if you can have "soft edges" - e.g. artificially decrease size around each edge of the 
                                                                   # interval by (default) 4% of its size up to 400 bases (e.g. 10kB scaffolds will be allowed to have a 400 
                                                                   # base buffer on both sides, but 1000B bases will only allow up to 40 base buffer on both sides)
            align_map[ln[1]] = align_map.get(ln[1], []) + [[float(ln[2]), float(ln[7]) + soft_edge, float(ln[8])-soft_edge, line]]

    for query in align_map.keys():
        align_map[query].sort(key = lambda l : l[1])    # sort all alignments in the queries by ascending buffered-start coordinate:
        #for l in align_map[query]:
        #    print (l)
        p = min_intersections(align_map[query])
        #print("Hello1")
        # we'll do a bottom-up approach to building the dp table for this problem:
        solns = {}
        added = list()
        prev = list()
        prev.append(-1)
        added.append(True)
        weights = [(l[0])**3 for l in align_map[query]] # because bitscore is highly proportional to length, and we want to pick the largest alignments,
                                                        # we'll take the cube of the bitscore to make sure that we bias the larger alignments more. 
        solns[0] = weights[0]
        for i in range(1, len(align_map[query])):
            solns[i] = max(solns[i-1], prev_soln(solns, p[i]) + weights[i])
            if solns[i] > solns[i-1]: # if the current index's weight was added
                prev.append(p[i])
                added.append(True)
            else:
                prev.append(i-1)
                added.append(False)

        #print("Hello2")

        # go through our solution table to find the best path.
        best_path = list()

        curr = len(align_map[query]) -1 
        while curr != -1:
            if added[curr] == True:
                best_path.append(curr)
                curr = prev[curr]
            else:
                curr = curr - 1
        #print("Hello3")

        for i in best_path:
            print(align_map[query][i][3])
        #print("hello4")

def print(*args, **kwargs):
    msg = ' '.join(map(str, args)) + '\n'
    with lock:
        sys.stdout.write(msg)
        sys.stdout.flush()

def log(s):
    sys.stderr.write(  "# %s\n" %(s) )

def sta(l):
    if int(l[8])<int(l[9]):
        return 0
    else:
        return 1

def write_dot(G,filename,ni):
    f=open(filename,"wt")
    f.write("digraph G {")
#    for n in G.nodes():
#        f.write("{} [ label=\"{}\"];\n".format(n,str(map(int,ni[n][4:8]))))
    for e in G.edges():
        f.write("\t\"{0}\" -> \"{1}\"\n".format(e[0],e[1]))
#        f.write(" -- ".join( map(str,e) ))
#        f.write("\n")
    f.write("}\n")
    f.close()

def node_score(info,node):
    return info[node][5]-info[node][4]

def best_score(G,l,best,info):
#    Starting at node l, return the sum of alignment lengths in the longest path
#    if best.has_key(l):
#        print "wtf: not a DAG?"
#        exit(0)
    if not G.predecessors(l):
        best[l]=node_score(info, l)
        return(best[l])
    if l not in best:
        best[l] = node_score(info,l) + max( [ best_score(G,p,best,info) for p in G.predecessors(l) ]  )
    return best[l]
    #    return max( [ best_score(G,p,best) for p in G.predecessors(l) ]  )

def longest_path(G,info,multi):
    # roots=[]
    leaves=[]
    for n in G.nodes():
        # if G.in_degree(n)==0:
            # roots.append(n)
        if G.out_degree(n)==0:
            leaves.append(n)

    # Q=deque()
    # for r in roots:
        # Q.append(r)

    Q = nx.topological_sort(G)

    best={}
    visited={}
    best_score=False
    best_score_node=False
    while Q:
        n=Q.pop(0)
        predecessor_scores = [best[p] for p in G.predecessors_iter(n)]
        if not predecessor_scores:
            best[n] = node_score(info, n) + 0
        else:
            best[n] = node_score(info, n) + max(predecessor_scores)
        if (not best_score) or (best[n]>best_score):
            best_score = best[n]
            best_score_node=n
        for c in G.successors_iter(n):
            if not c in visited:
                visited[c]=True
                Q.append(c)
    # if not best_score_node in leaves:
        # print("WARNING: best scoring node not in leaves?",file=sys.stderr)

    # start from best leaf to be consistent with output from old mbChain
    nn=list(best.keys())
    max_score = max([best[l] for l in leaves])
    optimal_leaves = [l for l in leaves if best[l]==max_score]
    best_leaf = optimal_leaves[0]

    if multi:
        leaves_to_traverse = [l for l in nn if (best[l] > 0.01*max_score and best[l] > 500)] # we'll traverse all the leaves with at least 1% of the optimal score 
    else:
        leaves_to_traverse = [best_leaf]
    all_paths = list()
    for l in leaves_to_traverse:
        path=[l]
        n=l
        while G.predecessors(n):
            max_score = max( [ best[p] for p in G.predecessors(n) ] )
            optimal_next = [ p for p in G.predecessors(n) if best[p] == max_score ]
            n = optimal_next[0]
            path = [n]+path
        all_paths = all_paths + [path]
    if not leaves_to_traverse:   # e.g. if our best path was less than 500 (really small scaffolds), we still want at least one path as an output
        path=[best_leaf]
        n=best_leaf
        while G.predecessors(n):
            predecessor_scores = [best[p] for p in G.predecessors_iter(n)]
            max_score = max(predecessor_scores)
            optimal_next = [ p for p in G.predecessors_iter(n) if best[p] == max_score ]
            n = optimal_next[0]
            path = [n]+path
        all_paths = all_paths + [path]
    if debug: print("path:",path)
    return all_paths
    

def old_longest_path(G,info):
    leaves=[]
    for n in G.nodes():
        if G.out_degree(n)==0:
            leaves.append(n)

    #print("#", leaves)
    if not leaves: return([]) 
    best={}
    for l in leaves:
        best[l] = best_score(G,l,best,info)
    nn=list(best.keys())
    nn.sort(key = lambda x: best.get(x,0.0))
    if debug:
        for n in nn:
            print("b:",n,best[n])
    max_score = max([best[l] for l in leaves])
    optimal_leaves = [l for l in leaves if best[l]==max_score]
    best_leaf = optimal_leaves[0]
    
    path=[best_leaf]
    n=best_leaf
    while G.predecessors(n):
        max_score = max( [ best[p] for p in G.predecessors(n) ] )
        optimal_next = [ p for p in G.predecessors(n) if best[p] == max_score ]
        n = optimal_next[0]
        path = [n]+path
    if debug: print("path:",path)
    return path

#rang =1000.0
trim = 25.0

def process_hsps(q,s,p,b,rang,cds_mode,multi):
#    b is the list of all the hits with that (q,s,p), such that every row in b has 
#    format: pident length mismatch gapopen qstart qend sstart send evalue bitscore"

    b2=list(b)
    b.sort( key=lambda x: x[4]) # sort by query start
    b2.sort(key=lambda x: x[5]) # sort by query end

    #for bbb in b:
        #print("#1:",bbb)
    #for bbb in b2:
        #print("#2:",bbb)

    edges={}
    lists = {}
    for i in b:
        lists[i[-1]]=i # dictionary with key-value: id-hit
    
    i,j=0,0
    while i<len(b):  # for each HSP
        while j<=i and b2[j][5]<b[i][4]-rang: j+=1  # find the HSP with the farthest-away-to-the-left END that is still within 'rang' of the start of i 
        for k in range(j,i):  # for each HSP with END between j and i 
#            print("#i",b[i],"k",b[k]);
            if rang>(b[i][4]-b2[k][5]) and 0.0<(b[i][4]+trim-b2[k][5]+trim):  # end of k < start if i, after clipping off 'trim' bases
                edges[b2[k][-1],b[i][-1]]=1  
                if ( (b2[i][-1],b[k][-1]) in edges ):                    
                    raise Exception("bi-directional edge 1") 
                
        i+=1 

    rang2=rang
    max_intron=100000.0
    if cds_mode:
        rang2 = rang+max_intron
#        print "x", rang2
    if p=="+":
        b.sort( key=lambda x: x[6]) # sort by subject start
        b2.sort(key=lambda x: x[7]) # sort by subject end

        i,j=0,0
        while i<len(b):
            while j<=i and b2[j][7]<b[i][6]-rang2: j+=1
            for k in range(j,i):
                if rang2>(b[i][6]-b2[k][7]) and 0.0<(b[i][6]+trim-b2[k][7]+trim):
                    if (edges.get((b[i][-1],b2[k][-1]),0)>1):
                        print(b2[k],b[i])
                        raise Exception("bi-directional edge 2")
                    edges[b2[k][-1],b[i][-1]]=edges.get((b2[k][-1],b[i][-1]),0)+2# make an edge connecting the two node ID's
            i+=1 


    else:
        b.sort( key=lambda x: x[6], reverse=True)
        b2.sort(key=lambda x: x[7], reverse=True)

        i,j=0,0
        while i<len(b):
            while j<=i and b2[j][7]>b[i][6]+rang2: j+=1
            for k in range(j,i):
                if rang2>-(b[i][6]-b2[k][7]) and 0.0<(-b[i][6]+trim+b2[k][7]+trim):
                    if (edges.get((b[i][-1],b2[k][-1]),0)>1):
                        print(b2[k],b[i])
                        raise Exception("bi-directional edge 2")
                    edges[b2[k][-1],b[i][-1]]=edges.get((b2[k][-1],b[i][-1]),0)+2 # make an edge connecting the two node ID's
            i+=1 


    G=nx.DiGraph()
    for i in list(lists.keys()):
        G.add_node(i)
    
#    r = int(10000.0*random.random())
#    print "XXXXX",r,".dot"
    for e in list(edges.keys()):
        if edges[e]>2:
            if debug: print("edge:",q,s,p,e,edges[e],lists[e[0]],lists[e[1]])
            if not e[0]==e[1]:
                if not nx.has_path(G, e[1], e[0]):
                    G.add_edge(e[0],e[1])
                else:
                    raise Exception("Cycle found in HSP graph!?")
#    if len(G.edges())>0 : write_dot(G,str(r)+".dot",lists)
    output_merges = list()
    pas=longest_path(G,lists, multi) # this returns the paths with the longest sum of alignment lengths
    for pa in pas:
        sc = sum([ lists[pn][9] for pn in pa  ]) # this is the sum of the bitscore of all the alignments in the path
        if pa and not multi: # if we're not using multi, then pas will only have one element anyways. 
            print("merged:",q,s,p,sc,len(pa),"\t".join(map(str,lists[pa[0]])),"\t".join(map(str,lists[pa[-1]])))
        x=int(min( ( lists[pa[0]][6],lists[pa[0]][7],lists[pa[-1]][6],lists[pa[-1]][7] ) ))
        y=int(max( ( lists[pa[0]][6],lists[pa[0]][7],lists[pa[-1]][6],lists[pa[-1]][7] ) ))

        w=int(min( ( lists[pa[0]][4],lists[pa[0]][5],lists[pa[-1]][4],lists[pa[-1]][5] ) ))
        z=int(max( ( lists[pa[0]][4],lists[pa[0]][5],lists[pa[-1]][4],lists[pa[-1]][5] ) ))
        output_merges.append([sc,s,p,x,y,w,z])

#    w=min( ( pa[0][4],pa[0][5],pa[-1][4],pa[-1][5] ) )
#    z=max( ( pa[0][4],pa[0][5],pa[-1][4],pa[-1][5] ) )
    return output_merges

plusminus={True:"+", False:"-"}

def hsp2length(hsp):    
    return(min( abs(hsp[7]-hsp[6]),abs(hsp[9]-hsp[8]) ))

def process_lines(b,r,lengths={},rang=1000,cds_mode=False, multi= False, buffer_multiplier=.04, minlength=1000):
    # b is the line-by-line output of megablast, in a list-of-list format (~2d array) 
    # qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore
    last=""
    hits={}

    id=1
    for i in b:
        # we assume the format of i is "qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore"
        q,s,p = i[0],i[1],plusminus[(i[9]-i[8])>0]
        # q is query name
        # s is subject name
        # p is whether the match is a positive match or a reverse compliment match
        #        if debug: print q,s,p

        if (hsp2length(i)<2*trim): 
            continue

        s = human_accessions.get(s,s)
        if not ((q,s,p) in hits):
            hits[q,s,p]=[]
#        hits[q,s,p] = hits.get((q,s,p),[])+[i[2:]+[id]]
        hits[q,s,p].append( i[2:]+[id])
        id+=1

    by_q={}

    for q,s,p in list(hits.keys()):
        if debug: print(q,s,p,len(hits[q,s,p]))#,
        rs=process_hsps(q,s,p,hits[q,s,p],rang,cds_mode,multi)
        # "rs" is the output of process_hsps, which is a list of lists [[sc,s,p,x,y,w,z],...]
        for r in rs:
            by_q[q] = by_q.get(q,[])+ [r]

    for q in list(by_q.keys()):
        by_q[q].sort(reverse=True)  # this sorts by_q[q] and makes the "sc" parameter (sum of bitscores in the alignment path) the criteria for the best alignment for that query.
                                    # Note that this means that for every query, you only ever get one "best" hit.
        if not multi:
            print("\t".join(map(str,['best:',q]+by_q[q][0]+[lengths.get(q)]))) # this prints the "best" (aka top result of the sort) output.  
        else: 
            outputs = list()
            max_bitscore = by_q[q][0][0]
            bitscore_thresh = max_bitscore*.01                            
            for res in by_q[q]:
                if res[0] > bitscore_thresh:
                    outputs.append("\t".join(map(str,['passing:',q]+res+[lengths.get(q)]))) # this prints the output which is 1% of the best output. 
            pick_query_alignments(outputs, buffer_multiplier,minlength)

def process_line_range(line_range, filename, lengths, rang, cds, multi, buffer_multiplier, minlength, noself = False):
    offset = line_range[0]
    num_lines = line_range[1]
    blast_data={}
    b=[]
    n=0
    with open(filename) as f:
        f.seek(offset)
        while num_lines > 0:
            l = f.readline()
            if not l: break
            c = l.strip().split()
            if len(c)<10: break
            if noself and c[0] == c[1]:
                continue
            if args.flip:
                tz = [ c[0],c[1]]+list(map(float,c[2:])) 
                b.append(  [  tz[1],tz[0], tz[2], tz[3], tz[4], tz[5], tz[8],tz[9], tz[6],tz[7],tz[10],tz[11]  ]  )
            else:
                b.append( [ c[0],c[1]]+list(map(float,c[2:]))   )
            num_lines -= 1
    process_lines(b,blast_data,lengths,rang,cds,multi, buffer_multiplier, minlength)

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('--no-self',default=False,action='store_true',help="ignore hits where target equals query")
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('--cds',default=False,action='store_true')
    parser.add_argument('-r','--range',default=False,type=float)
    parser.add_argument('-F','--flip',default=False,action='store_true')
    parser.add_argument('-t','--trim',default=False,type=float)
    parser.add_argument('-b','--megablast' )
    parser.add_argument('-l','--length' )
    parser.add_argument('-j','--jobs',required=False,type=int,default=32,help="Amount of parallel workers")
    parser.add_argument('-m', '--multi_results', default=False, action='store_true',help="Allow multiple (minimally-overlapping) matches per query?")
    parser.add_argument('-x','--buffer_multiplier',default=.04,type=float)
    parser.add_argument('-s', '--minlength', default=1000,type=float)
    # head mode currently inactive
    #parser.add_argument('-H','--head', type=int )
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    # process all args
    args = parser.parse_args()
    if args.seed != -1 :
        random.seed(args.seed)
    if args.debug: debug=True
    if args.trim: trim=args.trim
    rang=1000.0
    if args.range: rang=args.range
    multi=args.multi_results
    buffer_multiplier = args.buffer_multiplier
    minlength = args.minlength
    # 
    lengths={}
    if args.length:
        f=open(args.length)
        while True:
            l=f.readline()
            if not l: break
            if l[0]=="#": continue
            c=l.strip().split()
            #lengths[c[0]]=int(c[1])
            lengths[c[1]]=int(c[0])
        f.close()

    # create list of "tasks" (ranges of lines to process)
    tasks = []
    with open(args.megablast) as f:
        lines_in_task = 0
        total_offset = 0
        range_offset = 0
        while True:
            l = f.readline()
            if not l: break
            # create task if we reach a comment
            if l[0] == "#":
                if lines_in_task > 0:
                    tasks.append((range_offset, lines_in_task))
                    if debug:
                        print("#queued ",len(tasks)," tasks, last with", lines_in_task,"lines")
                    lines_in_task = 0
                total_offset += len(l)
                range_offset = total_offset
            else:
                cols = l.strip().split()
                if len(cols) < 10: break
                # current line is part of next task
                lines_in_task += 1
                total_offset += len(l)
        # add final task if there is one
        if lines_in_task > 0:
            tasks.append((range_offset, lines_in_task))
            if debug:
                print("#queued ",len(tasks)," tasks, last with", lines_in_task,"lines")
            lines_in_task = 0

    # dispatch tasks using multiprocessing
    process_lines_parallel = partial(process_line_range, filename=args.megablast, lengths=lengths,
                            rang=rang, cds=args.cds, multi=multi, buffer_multiplier=buffer_multiplier, minlength=minlength, noself = args.no_self)
    with Pool(args.jobs) as p:
        p.map(process_lines_parallel, tasks, 1)

    if args.progress: log( str( "Done loading "+args.megablast ) )
