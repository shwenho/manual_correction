#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
#!/usr/bin/env python3
import glob
import sys
import bisect
import re
import networkx as nx

def tab(x):
    return "\t".join(map(str,x))

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--loci')
    parser.add_argument('-t','--tags')
    parser.add_argument('-m','--minlen',type=int,default=0)
    parser.add_argument('-M','--maxdegree',type=int,default=1e9)
    parser.add_argument('-L','--lengths')
    parser.add_argument('-R','--rearrangements')
#    parser.add_argument('-b','--blacklist')
    parser.add_argument('-d','--debug',action="store_true")
    args = parser.parse_args()
 
    last_scaffold=False
    edge_buffer=[]

    ll={}
    f=open(args.lengths,"rt")
    while True:
        l=f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        ll[c[0]]=int(c[1])
    f.close()

    f=open(args.loci,"rt")
    loci=[]
    i=0

    

    g=nx.Graph()

    end_nodes={}
    both_end_nodes={}
    print("graph {")
    while True:
        l=f.readline()
        if not l:break
        if l[0]=="#": continue
        c=l.strip().split()
        b,e = int(c[2]),int(c[3])
        loci.append((c[0],b,e))
#        print "# id",c[0],b,e,len(loci)-1
        if ( b<10000 ) or (ll[c[0]]-e<10000): 
            end_nodes[i]=1
        if ( b<10000 ) and (ll[c[0]]-e<10000): 
            both_end_nodes[i]=1
            
        i+=1
    f.close()
    loci.sort()

    for i in range(len(loci)):
        print("# id",i,loci[i])

    tag={}
    if args.tags:
#scaffold_6:10874979-10881604    internal   
        f=open(args.tags,"rt")
        while True:
            l=f.readline()
            if not l: break
            if l[0]=="#": continue
            c=l.strip().split()
            m=re.match("(.*):(\d+)-(\d+)",c[0])
            contig,start,end = m.groups()
            start=int(start)
            end=int(end)
            lo=(contig,)
            i=bisect.bisect_left(loci,lo)
            j=bisect.bisect_right(loci,lo)
            print("# fp",lo,loci[i:j+1])
#            print "# ",i,j,len(loci),lo#,loci[i-1:i+2],lo
            if i >= len(loci): continue
            while i <=j and i<len(loci):
                
#                print lo,i,j,len(loci),loci[i]

                if loci[i][0]==lo[0] and not ( (loci[i][2]<start) or ( loci[i][1]>end ) ):
                    tag[i]=c[3]
                i+=1

#        for i in tag.keys():
#            print loci[i],tag[i]

    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split('\t')
        if int(c[2])<args.minlen or int(c[3])<args.minlen: continue

#        print l.strip()

# treat self-hits with care?
#scaffold_6      scaffold_6      29412908        29412908        27      [(595, 24258), (73030, 91462), (187280, 271710)]        [(595, 5765), (204491, 225693)]

        loci1=[]
        for b,e in eval(c[6]):
            lo=(c[0],b,e)
            i=bisect.bisect_left(loci,lo)
            print("# fp",lo,loci[i-1:i+1])
            #print "# ",i,len(loci),loci[i-1:i+2],lo
            if i >= len(loci): continue
            if loci[i][0]==lo[0] and loci[i][1]<=lo[1] and loci[i][2]>=lo[2]:
                loci1.append(i)
            elif i>=0 and loci[i-1][0]==lo[0] and loci[i-1][1]<=lo[1] and loci[i-1][2]>=lo[2]:
                loci1.append(i-1)

        loci2=[]
        for b,e in eval(c[7]):
            lo=(c[1],b,e)
            i=bisect.bisect_left(loci,lo)
            print("# fp",lo,loci[i-1:i+1])

#            print "# ",loci[i-1:i+2],lo
            if i >= len(loci): continue       
            if loci[i][0]==lo[0] and loci[i][1]<=lo[1] and loci[i][2]>=lo[2]:
                loci2.append(i)
            elif i>=0 and loci[i-1][0]==lo[0] and loci[i-1][1]<=lo[1] and loci[i-1][2]>=lo[2]:
                loci2.append(i-1)
        counts={}
        for x,y in eval(c[5]):
            for l1 in loci1:
                if loci[l1][1]<= x and x<= loci[l1][2]:
                    i=l1
            for l2 in loci2:
                if loci[l2][1]<= y and y<= loci[l2][2]:
                    j=l2
            counts[i,j]=counts.get((i,j),0)+1

        print("#lp", loci1,loci2,[loci[x] for x in loci1], [loci[x] for x in loci2], counts)
        for i,j in list(counts.keys()):
            if counts[i,j]>3:
                g.add_edge(i,j)
            print("#edge:",i,j, loci[i],loci[j],counts[i,j])
#        for x in loci1:
#            for y in loci2:
#                g.add_edge(x,y)

    hidden_nodes=[]
    for n in g.nodes():
        if g.degree(n)>args.maxdegree:

            hidden_nodes.append(n)
    for f in hidden_nodes:
        print("#too connected:",f,loci[f],g.degree(f))

    g.remove_nodes_from(hidden_nodes)


    hidden_nodes=[]
    for n in g.nodes():
        if g.degree(n)==0:
            hidden_nodes.append(n)
    g.remove_nodes_from(hidden_nodes)

    def format_locus(l):
        return "{}:{}-{}".format(l[0],l[1]+1,l[2])

#    ofh=open("/dev/null","w")
    if args.rearrangements:
        ofh=open(args.rearrangements,"wt")
        printed={}
        for cc in nx.connected_components(g):
            if len(cc)==1: 
                print("disconnected",loci[cc[0]])
                continue
            if len(cc)==2: 
                print(cc,loci[cc[0]],loci[cc[1]],format_locus(loci[cc[0]]),format_locus(loci[cc[1]]))
                lstr= ",".join(map(format_locus,[loci[n] for n in cc]))
                ofh.write("{}\t{}\n".format(lstr,len(cc)))
#            if len(cc)>5: continue
            if len(cc)>=3: 
                #print ""
                degree2s=[i for i in cc if g.degree(i)==2]
                print(degree2s)
                for n in degree2s:
                    a,b = g.neighbors(n)
                    printed[a,n]=1
                    printed[n,a]=1
                    printed[n,b]=1
                    printed[b,n]=1
                    print(a,n,b,len(loci),len(cc))
                    lstr= ",".join(map(format_locus,[loci[m] for m in (a,n,b)]))
                    ofh.write("{}\t{}\n".format(lstr,len(cc)))

                h=g.subgraph(cc)
                for e in h.edges():
                    if not e in printed:
                        lstr= ",".join(map(format_locus,[loci[n] for n in e]))
                        ofh.write("{}\t{}\t{}\n".format(lstr,len(cc),"X"))
#                    ofh.write("{}\t{}\n".format(",".join(map(format_locus,[loci[m] for m in (a,n,b)]))),len(cc))


        ofh.close()
    for n in g.nodes():

        print("#neigh:", format_locus(loci[n]),[format_locus(loci[m]) for m in g.neighbors(n)])

        if args.tags:
            if n in both_end_nodes:
                shape="rect"
            elif n in end_nodes:
                shape="diamond"
            else:
                shape="oval"
            if n in tag and tag[n]=="global":
                print("{} [style=filled;fillcolor={};shape=\"{}\"] ; # {} {}".format(n,"red",shape,loci[n][0],ll[loci[n][0]]))
            elif n in tag and tag[n]=="internal":
                print("{} [style=filled;fillcolor={};shape=\"{}\"] ; # {} {}".format(n,"blue",shape,loci[n][0],ll[loci[n][0]]))
            else:
                print("{} [style=filled;fillcolor={};shape=\"{}\"] ; # {} {}".format(n,"white",shape,loci[n][0],ll[loci[n][0]]))
                
        else:
            if n in both_end_nodes:
                print("{} [style=filled;fillcolor={}] ; # {} {}".format(n,"orange",loci[n][0],ll[loci[n][0]]))
            elif n in end_nodes:
                print("{} [style=filled;fillcolor={}] ; # {} {}".format(n,"red",loci[n][0],ll[loci[n][0]]))
            else:
                print("{} [style=filled;fillcolor={}] ; # {} {}".format(n,"white",loci[n][0],ll[loci[n][0]]))


    for i in range(len(loci)-1):
        j=i+1
        if not g.has_node(i): continue
        while loci[j][0]==loci[i][0] and j<len(loci) and not g.has_node(j):
            j+=1
        if loci[i][0]==loci[j][0] and j<len(loci) and g.has_node(i) and g.has_node(j):
            print(" {} -- {} [color=blue];".format(i,j))
                
    for e in g.edges():
        print(" {} -- {} ; # {} {}".format(e[0],e[1],loci[e[0]],loci[e[1]]))
    
    print("}")
