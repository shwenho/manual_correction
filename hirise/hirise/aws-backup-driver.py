#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys
import json
import os
import rdflib
import re
import argparse
import datetime
import shutil
parser = argparse.ArgumentParser()
parser.add_argument('-l','--lims',default=os.path.join(os.path.expanduser("~/"),"lims"))
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-n','--nothing',default=False,action="store_true")
parser.add_argument('-R','--resume',default=False,action="store_true")
parser.add_argument('-r','--root',default="/mnt/nas/raw")
args = parser.parse_args()

dt = rdflib.namespace.Namespace("http://d-t.us/schema#")

sys.path.append( os.path.join( args.lims , "cgi-bin" ) )

import subprocess,shlex

cmd = "glacier-cmd --output json lsvault"
output,error = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
print("lsvault error:",error)
vaults = [ x['VaultName'] for x in json.loads(output) ] 
for v in vaults:
    print("vault:",v) #vaults

print(os.path.join( args.lims , "cgi-bin" ))
import rdflimsutils as r

print(args)

sys.path.append( os.path.join( os.path.expanduser("~/") , "Downloads", "treehash" ) )
from treehash import TreeHash
th = TreeHash()

g= r.slurp_rdf_from_tree( os.path.join(args.lims,"rdf"), conjunctive=True )

new_data_context = g.parse("/dev/null",publicID="new:",format="turtle")
#new_data_context.add((dt.test,dt.test,rdflib.Literal("x")))
print(new_data_context)
print(new_data_context.serialize(format='turtle'))
for s,p,o in new_data_context.triples((None,None,None)):
    print(s,p,o)

#for c in g.contexts():
#    print c

#import datetime
dirs = os.listdir(args.root)
print(dirs)

import hashlib


def create_vault(vault):
    cmd = "glacier-cmd --output json mkvault %s" % (vault)
    output,error = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
    print("mkvault error:",error)
    newvault =  json.loads(output) 
    print(newvault)

def upload_file(vault,filepath,fileURI):
    statinfo = os.stat(filepath)
    uploadid = False
    if args.resume:
        cmd = "glacier-cmd --output json listmultiparts %s" % (vault)
        output,error = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
        print(output,error)
        if output.strip() == '"No active multipart uploads."' :
            print("No multiparts in the repository")
        else:
            print("There are multiparts in the vault")
            multiparts = json.loads(output)
#            uploadid = False
            for m in multiparts:
#                print m
                print(m['ArchiveDescription'], filepath)
                if m['ArchiveDescription'] == filepath:
                    uploadid = m['MultipartUploadId']
                    print("##match")
            if uploadid:
                print("Try to resume the upload with id :::::::::::::::::::::::",uploadid)
            else:
                "####no match"
#            print output


    cmd = "glacier-cmd --output json upload --partsize 16 %s %s" % (vault,filename)
    if uploadid:
        cmd = "glacier-cmd --output json upload --partsize 16 --uploadid %s %s %s" % (uploadid,vault,filename)
        
    
    if args.nothing:
        print("do nothing:", cmd)
        return
    output,error = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr = subprocess.PIPE).communicate()
    if error:
        print("upload error:",error)
    upload_output =  json.loads(output) 
    new_data_context.add( (fileURI, dt.SHA256TreeHash, rdflib.Literal(upload_output['Archive SHA256 tree hash']) ))
    new_data_context.add( (fileURI, dt.awsArchiveId, rdflib.Literal(upload_output['Created archive with ID']) ))
    new_data_context.add( (fileURI, dt.awsCreationDate, rdflib.Literal(datetime.datetime.utcnow()) ))
    new_data_context.add( (fileURI, dt.size           , rdflib.Literal(statinfo.st_size) ))
    new_data_context.add( (fileURI, dt.awsVault , rdflib.Literal(vault) ))
    all_new_data_serialized = new_data_context.serialize(format='turtle')
    print(all_new_data_serialized)
    f=open("new_glacier_data_tmp.ttl","w")
    f.write( all_new_data_serialized )
    f.close()
    shutil.move("new_glacier_data_tmp.ttl","new_glacier_data.ttl")

### these are copied from amazon-glacier-cmd-interface
def tree_hash(fo):
    """
    Given a hash of each 1MB chunk (from chunk_hashes) this will hash
    together adjacent hashes until it ends up with one big one. So a
    tree of hashes.
    """
    hashes = []
    hashes.extend(fo)
    while len(hashes) > 1:
        new_hashes = []
        while True:
            if len(hashes) > 1:
                first = hashes.pop(0)
                second = hashes.pop(0)
                new_hashes.append(hashlib.sha256(first + second).digest())
            elif len(hashes) == 1:
                only = hashes.pop(0)
                new_hashes.append(only)
            else:
                break
        hashes.extend(new_hashes)
    return hashes[0]



def bytes_to_hex(str):
    return ''.join( [ "%02x" % ord( x ) for x in str] ).strip()
def file_tree_hash(fn):
    fh = open(os.path.join(args.root, d,f) ,"rb")
    hashes = [hashlib.sha256(part).digest() for part in iter((lambda:fh.read(1024 * 1024)), '')]
    fh.close()
    th_local = bytes_to_hex(tree_hash(hashes))
    return th_local.strip()

files_to_upload = []

for d in dirs:
    try:
        for f in os.listdir( os.path.join(args.root,d) ):
            
            vault = "dovetail"+  os.path.join(args.root,d)
            vault = re.sub("/","-",vault)
            filename = os.path.join(args.root, d,f)
            fileURI =  rdflib.URIRef( "file://" + filename)
            if args.debug: print(f)
            th_local = file_tree_hash( filename )
            matching=[]


            for s,p in g.subject_predicates(rdflib.Literal(th_local)):
#                print "same hash:",s,p
                matching.append(s)

            if not vault in vaults:
                print("Need new vault", vault)
                create_vault(vault)

            if args.debug: print(f,vault,filename,fileURI,th_local,matching)


            if matching:
                if fileURI in matching:
                    print("Already uploaded:",filename)
                else:
                    print("Same data already uploaded:",filename, matching)
            else:
                if fileURI in g.subjects():
                    print("Hash mismatch for file", filename, vault)
                    upload_file(vault,filename,fileURI)
                    files_to_upload.append( [filename,vault] )
                else:
                    print("File not uploaded",filename, vault)
                    upload_file(vault,filename,fileURI)
                    files_to_upload.append( [filename,vault] )

    except Exception as e:
        print(e)

for filename,vault in files_to_upload:
    print("upload:",vault,filename)


