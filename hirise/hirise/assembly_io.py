#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import object
#!/usr/bin/env python3
import sys
import argparse
import networkx as nx


def update_end_distance(end_distance,n,g):
    x=0
    q=[n]
    seen={}
    last=False
    while len(q)>0:
        m=q.pop(0)
        if last:
            try:
                x+=g[last][m]['length']
            except Exception as e:
                print("wtf?",last,m,x,n)
                print(math.log(-1.0))
        last=m
        if (not m in end_distance) or end_distance[m]>x: end_distance[m]=x
        
        seen[m]=True
        if len(g.neighbors(m))>2:
            print("topology error:",m,list(g.neighbors(m)))
            print(math.log(-1.0))
        for l in g.neighbors(m):
            if not l in seen:
                q.append(l)

    return x



class BestHitsReader(object):
     def __init__(self,besthits):
          besthit={}          
          f = open(besthits)
          while True:
               l = f.readline()
               if not l: break

               if not l[:5]=="best:": continue
               c=l.strip().split()
               besthit[c[1]]=c[2:]
          f.close()
          self.besthit=besthit


class AssemblyWriter(object):
     def __init__(self,g,ll,besthit,c2scaffold={},of=sys.stdout):

         #print c2scaffold
         end_distance={}
         sn=1
         
         for sg in nx.connected_component_subgraphs(g):
             ends=[]
             bh_stats={}
             for n in sg.nodes():
                 if sg.degree(n)==1:
                     ends.append(n)

             if len(ends)==0: 
                 print("why no ends?", sn) 
                 sn+=1
                 continue


             snn=c2scaffold.get(ends[0][:-2],sn)

             maxx=update_end_distance(end_distance,ends[0],sg)

             # ['34329.0', '3', '+', '71834554', '71853152', '1', '1', '18598']

             t=0
             gap_len=0
             for s1,s2 in sg.edges():
                 t+=sg[s1][s2]['length']
                 if not sg[s1][s2]['contig']: gap_len += sg[s1][s2]['length']
                 print("#",snn,s1,s2,sg[s1][s2]['length'],t)
             of.write(" ".join(map(str,[ t,n,"slen",gap_len,t-gap_len]))+"\n")

             node_tlen=0
             nodes_list = list(sg.nodes())
             nodes_list.sort(key=lambda x: end_distance[x])
             for n in nodes_list:
                 base_name,end_id=n[:-2],n[-1:]
                 #base_name,end_id=n.split('.')
                 if end_id=="5": node_tlen+= ll[base_name]
                 bh = besthit.get(base_name,False)
                 x=-1
                 chr="-"
                 if bh:
                     chr=bh[1]
                     if bh[2]=="+":
                         if n[-1:]=="5":
                             x=int(bh[3])
                         else:
                             x=int(bh[4])
                     if bh[2]=="-":
                         if n[-1:]=="5":
                             x=int(bh[4])
                         else:
                             x=int(bh[3])

                 of.write("\t".join(map(str,[ "p:",snn,n,end_distance[n],chr,x,t,ll[base_name],bh]))+"\n")
             print(node_tlen,"node_tlen")

             sn+=1


class AssemblyReader(object):
     def __init__(self,filename):
          self.filename=filename



     def edges2graph(self,g,ll=-1,c2scaffold=-1):
          f=sys.stdin
          if not self.filename=="-":
               f=open(self.filename,"rt")
          while True:
               l=f.readline()
               if not l: break
               #scaffold: Scaffold146005_1 1
               if type(c2scaffold)==dict and l[:9]=="scaffold:": 
                    c=l.strip().split()
                    c2scaffold[c[1]]=c[2]
#                    print c[1],":",c[2]
               if not l[:6]=="#edge:": continue
               c=l.strip().split('\t')
               d=eval(c[3])
#               print "edge",c[1],c[2],d
               g.add_edge(c[1],c[2],d)
               if (not ll==-1) and d['contig']==True:
                    contig=c[1][:-2]
                    ll[contig]=d['length']

class LinksReader(object):
#     links=aio.LinksReader(glob=args.links,scaffoldmap=c2scaffold,intra=1,mycontigs=mycontigs).readlinks()
     def __init__(self,glob=False,filename=False,filelist=False,mycontigs=False,intra=False,scaffoldmap=False):
          self.glob=glob
          self.filename=filename
          self.filelist=filelist
          self.scaffoldmap=scaffoldmap
          self.intra=intra
          self.mycontigs=mycontigs

     def readlinks(self):
          filenames=[]
          if self.glob:
               import glob
               filenames = list(glob.glob(self.glob))
          elif self.filelist:
               filenames=self.filelist
          elif self.filename:
               filenames=[self.filename]
          else:
               print("no input likss files speficied.")
               raise Exception 

          links={}
          nlinks=0
          for fn in filenames:
               print("#file:",fn)
               sys.stdout.flush()
               f=open(fn,"rt")
               while True:
                    l=f.readline()
                    if not l: break
                    if l[0]=="#": continue
                    c=l.strip().split()
                    if self.intra and self.scaffoldmap and not ( self.scaffoldmap.get(c[0],c[0])==self.scaffoldmap.get(c[1],c[1]) ): continue
                    if self.mycontigs and not (c[0] in self.mycontigs and c[1] in self.mycontigs): continue
                    pl=tuple(eval(" ".join(c[5:])))
                    links[c[0],c[1]]=pl
                    #                    links[c[1],c[0]]=tuple([ (y,x) for x,y in  pl])
                    nlinks+=1
               print("#nlink lines:",nlinks)
               sys.stdout.flush()
          return links
