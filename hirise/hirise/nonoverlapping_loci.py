#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
#!/usr/bin/env python3
import glob
import sys

def tab(x):
    return "\t".join(map(str,x))

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-p','--padding',type=int,default=0)
#    parser.add_argument('-l','--lengths')
#    parser.add_argument('-l','--links')
#    parser.add_argument('-b','--blacklist')
    parser.add_argument('-d','--debug',action="store_true")
    args = parser.parse_args()
 
    last_scaffold=False
    edge_buffer=[]

    ll={}

    footprints=[]

    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split('\t')
        ll[c[0]]=int(c[2])
        ll[c[1]]=int(c[3])
        for b,e in eval(c[6]):
            b= max(0,b-args.padding)
            e= min(ll[c[0]]-1,e+args.padding)
            footprints.append((c[0],b,e,c[1],int(c[4])))
        for b,e in eval(c[7]):
            b= max(0,b-args.padding)
            e= min(ll[c[1]]-1,e+args.padding)
            footprints.append((c[1],b,e,c[0],int(c[4])))

    footprints.sort()
    for contig,b,e,ox,yx in footprints:

        if last_scaffold and not contig==last_scaffold:
            rs=0
            edge_buffer.sort(key=lambda x: (x[0],-x[1])) # so that if position x has an end of a segment and the beginning of another one, the don't get split
            seg_start=-1
            nm=0
            info=[]
            for i in range(len(edge_buffer)):
                rs_d =edge_buffer[i][1]
                if rs_d>0: # another segment in the current island
                    nm+=1
                    info.append( tuple(edge_buffer[i][2:])  )
                if rs>0 and rs_d+rs==0:  # running sum comes back down to zero, marking the end of an island
                    print(tab([last_scaffold,ll[last_scaffold],seg_start,edge_buffer[i][0],nm,info]))
                    nm=0
                    info=[]
                elif rs==0 and rs_d>0:   # running sum rises above zero, marking the start of an island
                    seg_start=edge_buffer[i][0]
                rs+=rs_d
            edge_buffer=[]
            
        last_scaffold = contig

        edge_buffer.append((b,  1,ox,yx))
        edge_buffer.append((e, -1,ox,yx))

    rs=0
    edge_buffer.sort()
    seg_start=-1
    nm=0
    info=[]
    for i in range(len(edge_buffer)):

        rs_d =edge_buffer[i][1]
        if rs_d>0: 
            nm+=1
            info.append( tuple(edge_buffer[i][2:])  )
            #info.append( (edge_buffer[i][2], edge_buffer[i][3])  )
        if rs>0 and rs_d+rs==0:
            print(tab([last_scaffold,ll[last_scaffold],seg_start,edge_buffer[i][0],nm,info]))
            nm=0
            info=[]
        elif rs==0 and rs_d>0:
            seg_start=edge_buffer[i][0]
        rs+=rs_d
    edge_buffer=[]
