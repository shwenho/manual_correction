#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import print_function
import sys

edges={}

f=open(sys.argv[1])
while True:
    l=f.readline()
    if not l: break
    if l[0]=="#": continue
    c=l.strip().split()
    edges[c[0],c[1]]=1
f.close()

while True:
    l=sys.stdin.readline()
    if not l: break
    if l[0]=="#": continue
    c=l.strip().split()

    if (c[0],c[1]) in edges or (c[1],c[0]) in edges:
        print(l.strip())

