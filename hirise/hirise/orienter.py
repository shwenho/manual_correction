#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-s','--strings',required=True)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

#    if args.progress: log( str(args) )

#    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))
#    print "Done reading edgelist"
    
    f = open(args.strings)

    strings = []
    while True:
        l = f.readline()

        if not l:   break
        if l[0]=="#": continue
        if not l[:2]=="y:": continue
        c=l.strip().split()
        strings.append( eval( " ".join(c[1:])))
    f.close()

    scores={}
    for s in strings:
        for i in range(1,len(s)):
            scores[s[i-1],s[i]]=[0.0,0.0,0.0,0.0]

    while True:
        l = sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c = l.strip().split()
        if scores.get((c[0],c[1]),False):
            scores[c[0],c[1]]= list(map(float,c[8:12]))
#            print c[0],c[1],scores[c[0],c[1]]
            if args.debug: print("\t".join(map(str,[c[0],c[1]]+scores[c[0],c[1]]+[c[4]])))
        if scores.get((c[1],c[0]),False):
#            scores[c[1],c[0]]=
            scores[c[1],c[0]]= list(map(float,c[8:12]))
            if args.debug: print("\t".join(map(str,[c[1],c[0]]+scores[c[1],c[0]]+[c[4]])))



    
    def get_score(s1,s2,o1,o2):
        if   (o1,o2) == ("+","+"):
            return scores[s1,s2][0]
        elif (o1,o2) == ("+","-"):
            return scores[s1,s2][1]
        elif (o1,o2) == ("-","+"):
            return scores[s1,s2][2]
        elif (o1,o2) == ("-","-"):
            return scores[s1,s2][3]

    n=0
    sc={}
    bt={}
    for s in strings:
        n+=1
        sc[(0,'+')] = 0.0
        sc[(0,'-')] = 0.0


        print("\t".join(map(str,[n,0,s[0],sc[(0,'+')],sc[(0,'-')]])))
        for i in range(1,len(s)):

            pp = sc[(i-1,"+")] + get_score(s[i-1],s[i],"+","+")
            mp = sc[(i-1,"-")] + get_score(s[i-1],s[i],"-","+")
            pm = sc[(i-1,"+")] + get_score(s[i-1],s[i],"+","-")
            mm = sc[(i-1,"-")] + get_score(s[i-1],s[i],"-","-")

            sc[(i,'+')] = max( pp , mp )
            sc[(i,'-')] = max( pm , mm )

            if pp == max( pp , mp ): 
                bt[(i,'+')]='+'
            else:
                bt[(i,'+')]='-'
            if pm == max( pm , mm ): 
                bt[(i,'-')]='+'
            else:
                bt[(i,'-')]='-'
                

            print("\t".join(map(str,[n,i,s[i],sc[(i,'+')],sc[(i,'-')], bt[(i,'+')], bt[(i,'-')], scores[s[i-1],s[i]] ])))
        print("")


        i = len(s)-1
        strand = {}
        if sc[(i,'+')] >= sc[(i,'-')]:
            strand[i]='+'
        else:
            strand[i]="-"
        while i > 0:
            strand[i-1] = bt[(i,strand[i])]
            i=i-1

        for i in range(len(s)):
            print("o:",i,s[i],strand[i])
            
        
#            print i,s
       


