#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from past.utils import old_div
import sys
import argparse
import glob
import re
import subprocess
import shlex
import os
import math



if __name__=="__main__":

     parser = argparse.ArgumentParser()
     #parser.add_argument('-i','--input')
     parser.add_argument('-d','--debug',default=False,action="store_true")
     parser.add_argument('-p','--progress',default=False,action="store_true")
     parser.add_argument('-o','--outfile')
     parser.add_argument('-i','--infile')
     parser.add_argument("-g", "--good", default=False, action="store_true")
     parser.add_argument("-b", "--bad", default=False, action="store_true")
     parser.add_argument("-s", "--skipdiff", default=False, action="store_true")

     args = parser.parse_args()
     if args.progress: print("#",args)

     outfile=args.outfile
     infile=args.infile

     maxrange=0
     minrange=1e9
     files=[]
     for fn in glob.glob(args.infile):
          files.append(fn)
          f=open(fn)
          target_sep = []
          while len(target_sep)<100:
               l=f.readline()
               if not l: break
               target_sep.append(int( l.strip().split()[1] ))
                    
          if len(target_sep) == 0: target = 0
          else: target=old_div(float(sum(target_sep)),len(target_sep))
          maxrange = max(maxrange,10000*int(math.ceil(old_div((target*1.3),10000.0))))
          minrange = min(minrange,10000*int(math.floor(old_div((target*0.7),10000.0))))

     def plotfn(s):
          return '"< cat {0} | grep slen | n50stats.py" u 2:5 w steps t "{0}"'.format(s)

     def plotfn2(s):
          return '"< cat {0} | awk \'{{print $2}}\' | n50stats.py" u 2:5 w steps t "{0}"'.format(s)

     files.sort()
#     print "set key b"
     print("set term 'pdf'")
     print("set output '{}'".format(outfile))
#     print "set log y"
#     print "set log x"
     print("set xlabel '{}'".format("True sep."))
     print("set ylabel '{}'".format("N obs."))
     print("plot [{}:{}] ".format(minrange,maxrange)+" , ".join( [ ' "< cat {0} | histogram.py 1 100" u 1:4 w histeps t "{0}"'.format(fn) for fn in files] ))
 

