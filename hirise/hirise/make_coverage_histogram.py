#!/usr/bin/env python3

import sys
import json
import os

import pysam
import argh
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from hirise.coverage_hist_tools import coverage_histogram

def get_coverage_histograms(bams,nsamples,min_ins,max_ins,min_mapq):
    hists = [coverage_histogram(nsamples,[pysam.Samfile(bam)],min_ins,max_ins,min_mapq,5000) for bam in bams]
    if len(bams) > 1:
        hists.append(coverage_histogram(nsamples,[pysam.Samfile(bam) for bam in bams],min_ins,max_ins,min_mapq,5000))
    return hists

def make_plot(histograms,labels,output):
    for data,label in zip(histograms,labels):
        x = [depth for depth in sorted(data.keys())]
        y = [data[val] for val in x]
        plt.step(x,y,label=label,where='post')
    plt.title("Physical coverage histogram")
    plt.xlabel("Physical coverage")
    plt.ylabel("Number of sites")
    plt.legend(loc='upper left',fontsize='x-small')

    plt.savefig(output,dpi=1200)


@argh.arg('-l','--labels',nargs="+")
@argh.arg("bams",nargs="+")
def main(bams,
         labels:"Labels for BAM files"=None,
         nsamples:"Number of positions to sample."=5000,
         min_insert:"Minimum insert size."=1000,
         max_insert:"Maximum insert size."=100000,
         min_mapq:"Minimum mapq for reads to contribuite."=20,
         output:"Output plot name."="coverage_hist.png"):
    if labels and len(bams) != len(labels):
        print("ERROR: If specifying labels, must specify one for each BAM file",
                file=sys.stderr)
        sys.exit(1)
    if not labels:
        labels = [os.path.basename(bam) for bam in bams]
    if len(bams) > 1:
        labels.append("combined")
    hists = get_coverage_histograms(bams,nsamples,min_insert,max_insert,min_mapq)
    make_plot(hists,labels,output)


if __name__=="__main__":
     argh.dispatch_command(main)
