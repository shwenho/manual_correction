#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import json
import re

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

head_counts={}
rr=[]
while True:
     l=sys.stdin.readline()
     if not l: break
     if l[0]=='#': continue
     hh = json.loads(l.strip())
     rr.append(hh)
     #print(hh)
     for k in hh.keys(): head_counts[k] = head_counts.get(k,0)+1

column_ranks={     
"date"                 :1,
"user"                 :2,
"project"              :2.5,
"genome_size"          :3,
"chicago_coverage_1_50":4,
"input_n50"            :5,
"iter0_n50"            :6,
"improvement"          :6.5,
"total_n_reads"        :7,
"N"                    :8,
"pn"                   :9,
"total_n_chicago_links":10,
"hirise_version"       :11, 
"Nn"                   :12,
"path"                 :13,
"mean_shotgun_depth"   :14,
"short_contigs_length" :15,
"n_chicago_links_e50k" :16,
"total_depth_masked"   :17,
"median_max_links"     :18,
"modal_shotgun_depth"  :19,
"n_blacklisted_links"  :20,
"total_n_breaks"       :21,
"broken_n50"           :22,
"total_n_joins"        :23,
"coverage_quantiles"   :24
}


head_counts['project']=1
head_counts['improvement']=1
columns = list(head_counts.keys())

columns.sort(key=lambda x: column_ranks.get(x,200))

sppl=['human','cassava','prairie_chicken','gator','croc','hydra','polar_bear','miscanthus','whale']

print("#"+"\t".join(map(str,[c for c in columns])))
for r in rr:
     tsp="-"
     for sp in sppl:
          if re.search(sp,r['path']):
               tsp=sp
     r['project']=tsp
     r['improvement'] = r['iter0_n50'] / r.get('input_n50',1)
     print("\t".join(map(str,[r.get(c,'-') for c in columns])))
     
