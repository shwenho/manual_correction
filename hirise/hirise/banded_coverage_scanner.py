#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
from builtins import object
from past.utils import old_div
import pysam
import sys

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-f','--fig',default=False)
    parser.add_argument('-r','--region',default=False)
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-s','--savegraph',default=False)
    parser.add_argument('-S','--restoregraph',default=False)
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-L','--maxinsert',default=150000,type=int)
    parser.add_argument('-w','--binwidth',default=10000,type=int)
    parser.add_argument('-B','--breaks',default=False)
#parser.add_argument('-b','--bamfile',required=True,action="append",default=[])
    parser.add_argument('-M','--min_insert',default=1000,type=int)
    parser.add_argument('-e','--endsonly',default=False,action="store_true")
    parser.add_argument('-E','--endwindow',default=50000,type=int)
    parser.add_argument('-D','--dotfile',default=False)
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('-c','--cutoff',default=(old_div(200.0,1.0e13)),type=float)
    parser.add_argument('-m','--mapq',default=(3.0),type=float)
    parser.add_argument('-I','--ignoreDuplicates',default=False,action="store_true")
    parser.add_argument('--log_breaks',default=False,action="store_true")
    parser.add_argument('--min_total_coverage',default=0,type=int)

    nodes={}

    args = parser.parse_args()

    L=args.maxinsert

    if not args.breaks:        
        diagonal_width = args.binwidth
        n_diagonals = old_div(L, diagonal_width)
    else:
        breaks = list(map(int,args.breaks.split(",")))
        n_diagonals = len(breaks)-1

    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )
  
    sam=pysam.Samfile(args.bamfile)

    if args.progress: log( "opened %s" % args.bamfile )


    h=sam.header
#    seqs=h['SQ']

#    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

#    slen=[ s['LN'] for s in seqs ]
#    snam=[ s['SN'] for s in seqs ]

#    if args.progress: log(  "built length and name map arrays" )   


#    for i in range(len(seqs)):
#        if slen[i]>args.minlength:
#            nodes[i]=1

    n=0
    ne=0
    hist={}
    nr=0

    left_cursor = 0
    right_cursor= 0
    center_cursor=0
    current_seq = 0

    all_fore_queue=[]
    all_aft_queue=[]
    all_queue=[]
    max_len=150000

    aln = False


    class Pair(object):
        def __init__(self,a,b,name=False,tid=0):
            self.a = a
            self.b = b
            self.d = max(b-a,a-b)
            self.trashed = False
            self.name= name
            self.tid=tid

        def trash(self):
            self.trashed=True

        def __repr__(self):
            return str( (self.a,self.b,self.name,self.trashed) )

    class Diagonal(object):
        def __init__(self,x,y):
            assert (x<y)
            self.min_sep = x
            self.max_sep = y
            self.queue = []
            self.n = 0

        def __repr__(self):
            return("%d-%d"%(self.min_sep,self.max_sep))
            
        def enqueue(self,pair):
            if pair.d < self.min_sep:
                return False
            elif pair.d >= self.max_sep:
                return False
            
            self.queue.append(pair)
            self.n+=1

        def drain(self,x,L,tid):
            if args.debug: print("drain queue",self.min_sep, self.n)
            if len(self.queue)==0:
                if args.debug: print("queue already empty")
                return
            while (len(self.queue)>0) and((not self.queue[0].tid == tid) or (self.queue[0].a <  x-L)):
                p=self.queue.pop(0)
                if not p.trashed:
                    self.n-=1
            for p in self.queue:
                if p.a >= x - self.min_sep:
                    break
                if (p.b < x) and (not p.trashed):
                    p.trash()
                    self.n-=1
            if args.debug: print("drain if a<",x-L, "flag if b<",x, "now the count is", self.n, "queue=",self.queue)
            
    class pairGen(object):
        def __init__(self,sam,region=False,mapq=-1.0):
            if not region:
                self.sam_it = sam.fetch(until_eof=True)
            else:
                print(region)
                self.sam_it = sam.fetch(region=region)
            self.mapq=mapq

        def __iter__(self):
            return self

        def __next__(self):
            aln= next(self.sam_it)
#            print "x:",aln
            #print "y:",aln.next()
            while (args.ignoreDuplicates and aln.is_duplicate) or (aln.mapq < self.mapq) or (not ( aln.tid==aln.rnext)) or (aln.pos>aln.pnext):
                aln = next(self.sam_it)
                #                print "z:",aln
#            print aln.is_duplicate ,aln.tags
            return( Pair( aln.pos,aln.pnext, aln.qname, aln.tid ) )
#            return ((aln.tid,aln.pos,aln.pnext,aln.qname))


    pg=pairGen(sam,args.region,args.mapq)

    diagonals = []
    if args.breaks:
        for i in range(1,len(breaks)):
            diagonals.append(Diagonal( breaks[i-1],breaks[i] ))

    else:
        for i in range( n_diagonals):
            diagonals.append( Diagonal( i*diagonal_width, (i+1)*diagonal_width ) )

    def queue_pair(p):
        for d in diagonals:
            if d.enqueue(p):
                break

    def update_queues(x,tid):
        for d in diagonals:
            d.drain(x,L,tid)
        
    def init_start_new_scaffold():
        left_cursor = 0
        right_cursor= 0
        center_cursor=0
        current_seq = -1
        all_fore_queue=[]
        all_aft_queue=[]


    break_start=False

    def report_stats(c,x):
        ns = [ d.n for d in diagonals ]
        total_cover = sum(ns)
        if args.log_breaks:
            if total_cover < args.min_total_coverage:
                if not break_start:
                    break_start = x
            if break_start and not total_cover < args.min_total_coverage:
                print("break:",break_start,x)
                break_start = False

        print("\t".join(map(str, [c,x,total_cover]+ns)))



    print("#" + "\t".join(map(str,["chr","x"]+diagonals)))

    for p in pg:
        x= p.a
        queue_pair(p)
        update_queues(x,p.tid)
        report_stats(p.tid,x)
