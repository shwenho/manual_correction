#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from past.utils import old_div
import sys
import math
from scipy.stats import beta

conf_interval=0.95
nn = 922555
while True:
    l = sys.stdin.readline()
    if not l: break
    if l[0]=="#": continue

    c = l.strip().split()
    n = int(c[4])

    ci =  ( beta.ppf(old_div((1.0-conf_interval),2.0), n+1, nn-n+1 ) , beta.ppf((1.0-old_div((1.0-conf_interval),2.0)),n+1,nn-n+1))
    print(l.strip(),n,ci[0]*nn,ci[1]*nn)
