#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx


def printdot(g,c,n):
    gg = nx.subgraph(g,c)
    f=open("%d.txt" % (n), "wt")
    nn=1
    lab0={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]
        lab0[d]=lab0.get(d,nn)
        nn+=1
    lab={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]

        lab[x]=p + str(lab0[d])

    f.write( "graph G {\n")
    for e in gg.edges():
#        f.write( "\t\"%s\" -- \"%s\";\n" % (lab[e[0]],lab[e[1]]) ) #,gg[e[0]][e[1]]['weight'])
        f.write( "\t\"%s\" -- \"%s\" [label=\"%d\"];\n" % (lab[e[0]],lab[e[1]],int(gg[e[0]][e[1]]['weight'])))
    f.write( "};\n")

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-q','--minquality',default=10.0,type=float)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))

    n=0
#    for c in nx.connected_components(G):
#        if len(c)>4:
#            n+=1
#            print c
#            printdot(G,c,n)

    def linearize(t,c,component_index):
#        print c
#        print t
        saved_links={}
        deleted_hair_edges=[]
        done=False
        itern=0
        while not done and itern<2:
            itern+=1
            #print "iter:",itern
            done = True
            for n in c:
                d = t.degree(n)

                #print n,d
                if d==1:
                    e_to_remove=[]
                    for e in t.edges([n]):
                        e_to_remove.append(e)
                        done=False
                        if e[0]==n:
#                            saved_links[n]=e[1]
                            deleted_hair_edges.append(( t[e[0]][e[1]]['weight'], e[0],e[1] ))
                        else:
                            deleted_hair_edges.append(( t[e[0]][e[1]]['weight'], e[1],e[0] ))
                            
#                            saved_links[n]=e[0]
                    #print e_to_remove
                    t.remove_edges_from(e_to_remove)

            
        for n in c:
            d = t.degree(n)

            #print n,d
            if d>2:
                e_to_remove=[]
                for e in t.edges([n]):
                    e_to_remove.append(e)
                    done=False
                #print "branch:",e_to_remove
                t.remove_edges_from(e_to_remove)

        deleted_hair_edges.sort(reverse=True)

        for e in deleted_hair_edges:
            if t.degree(e[2])==1:
                t.add_edge(e[1],e[2])

        n=1
        for cc in nx.connected_components(t):
            cc.sort(key=lambda x:t.degree(x))
            h = cc[0]
            ll = list(nx.dfs_preorder_nodes(t,h))
            print(component_index,len(ll),ll)
            n+=1
    
    n=1
    for c in nx.connected_components(G):
        gg = nx.subgraph(G,c)
        t= nx.minimum_spanning_tree(gg)
        linearize(t,c,n)
        n+=1
#        if len(c)>4:
#            n+=1
#            print c
#            printdot(t,c,n)
