#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
args=False

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--debug',action="store_true")
    parser.add_argument('files', metavar='FILENAME', nargs='+' )

    args = parser.parse_args()


    fhl = [open(x,"rt") for x in args.files ]

    pairs={}
    for fh in fhl:
        while True:
            l = fh.readline()
            if not l: break
            if l[0]=="#": continue
            c=l.strip().split("\t")
            try:
                pairs[ tuple(c[:4]) ] = pairs.get( tuple(c[:4]), []) + eval( c[5] )
            except Exception as e:
                print(c)
                raise e
                
    
    for links in sorted(pairs.keys()):
        print("\t".join(map(str,list(links)+[ len(pairs[links]) ]+[pairs[links]])))
