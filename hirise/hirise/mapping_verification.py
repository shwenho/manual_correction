#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import map
#!/usr/bin/env python3
import sys
import hirise.mapper
import pickle as pickle
import argparse

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-p','--progress' ,default=False,action="store_true")
    parser.add_argument('-m','--map_file' ,default=False)
    parser.add_argument('-r','--rmap_file',default=False)

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    mapp = pickle.load(open(args.map_file))
    rmap = pickle.load(open(args.rmap_file))

    lengths={}
    pairs={}
    name2pair={}

    if args.debug: mapp.debug=True
    if args.debug: rmap.debug=True

    while True:
        l = sys.stdin.readline()
        if not l: break
        c=l.strip().split()
        if not c[0]=="p:": continue

        node,end = c[2].split('.')
        x = int(float(c[3]))
        n = rmap.find_node_by_name(node)

#        ori1 = rmap.find_original_coord(n,x)

        scaffold_name = "scaffold_{}".format(c[1])
        scaffold_node = mapp.find_node_by_name(scaffold_name)
        mapped2 = mapp.find_original_coord(scaffold_node,x)

#        mapped1 = mapp.map_coord(ori1[0],ori1[1])
        print("\t".join(map(str,[mapped2,c])))

#       print mapp.print_name(mapped2[0]),mapped2[1]        
#p: 1 Scaffold116820_1.3 0 - -1 32963728.0 4101 False
#p: 1 Scaffold116820_1.5 4101 - -1 32963728.0 4101 False
#p: 1 Scaffold23744_1.3 5101.0 - -1 32963728.0 35999 False
#p: 1 Scaffold23744_1.5 41100.0 - -1 32963728.0 35999 False
#p: 1 Scaffold8128_1.5 42100.0 - -1 32963728.0 3625 False
#p: 1 Scaffold8128_1.3 45725.0 - -1 32963728.0 3625 False
#p: 1 Scaffold291010_1.3 46725.0 - -1 32963728.0 10687 False
#p: 1 Scaffold291010_1.5 57412.0 - -1 32963728.0 10687 False

