#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
from chicago_support_bootstrap import pairs2support

def overlapping_regions(regions,span):
     i=-1
     result=[]
     for r in regions:
          i+=1
          if not span[0]==r[0]: continue
          a,b = span[1],span[2]
          x,y = r[1],r[2]
          if (x<a and a<y) or (x<b and b<y) or (a<x and x<b) or (a<y and y<b) or (x==a and y==b): 
               result.append(i)
     return result
     
def which_region(regions,x):
     i=-1
     for r in regions:
          i+=1
          if not x[0]==r[0]: continue
          if (min(x[1:])>=r[1] and min(x[1:])<=r[2]) or (max(x[1:])>=r[1] and max(x[1:])<=r[2]): return i
     raise Exception 

def handle_scaffold(scaffold,slen,pairs,outfile,model,hra,region_start,region_offset,t1=5,t2=20,maxx=200000,minx=500,binwidth=1500,nreject=2,debug=False):

     raw,clipped= pairs2support(scaffold,pairs,model,slen,logfile=outfile,masked_segments=hra.make_scaffold_mask(scaffold),t1=t1,t2=t2,binwidth=binwidth,maxx=maxx,minx=minx,buff=0,nreject=nreject,support_curve=True,debug=debug,clipped_curve=True)
     for x,score in raw: 
          plot_pos = x - region_start + region_offset
          if plot_pos < 0: continue
          print("score:",plot_pos,score)
     for x,score in clipped: 
          plot_pos = x - region_start + region_offset
          if plot_pos < 0: continue
          print("clipped_score:",plot_pos,score)
     outfile.flush()


if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-r','--raw',default=False,action="store_true",help="For viewing a single region in scaffold coordinates")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
     parser.add_argument('-t','--t1',type=float,default=5.0,help="Don't break in the trailing regions at the end of scaffolds where support never exceeds this number.")
     parser.add_argument('-T','--t2',type=float,default=20.0,help="Report all segments where support dips below this threshold, if they are not in the trailing ends.")
     parser.add_argument('-A','--hra',default=False,help="Assembly file.")
     parser.add_argument('-B','--buff',type=int,default=20000,help="Buffer between scaffolds in visualization. Defaults to 20 kb.")

     args = parser.parse_args()
     if args.progress: print("#",args)

     hra=False
     if args.hra:
          from hirise_assembly import HiriseAssembly

          hra = HiriseAssembly()
          hra.load_assembly(args.hra)
          if len(hra.layout_lines)==0: 
              hra.make_trivial_layout_lines(debug=args.debug,ocontig_ids=True)



     regions=[]
     contigs=[]
     breaks = []
#[['contig:', 'scaffold321', '1', '7', '0', '1408973', 'False']]

     points={}

     for line in sys.stdin:
          if line.startswith("region:"):
               c=line.strip().split('\t')
               regions.append((c[1],int(c[2]),int(c[3]),len(c)>4))
#               print(regions)
          elif line.startswith("contig:"):
               c=line.strip().split('\t')
               contig,base,scaffold,start,end = c[1],int(c[2]),c[3],int(c[4]),int(c[5])
               contigs.append((contig,base,scaffold,start,end))
#               print(contigs)
          elif line.startswith("break:"):
               c=line.strip().split('\t')
               scaffold,pos = c[1],int(c[2])
               breaks.append((scaffold,pos))
#               print(contigs)
          else:
               c=line.strip().split('\t')
               try:
                    s1,s2,x1,x2 = c[0],c[1],int(c[2]),int(c[3])
               except IndexError:
                    raise IndexError(line)
               r1,r2 = which_region(regions,(s1,x1)),which_region(regions,(s2,x2))
               if r2<r1:
                    s1,s2,x1,x2 = c[1],c[0],int(c[3]),int(c[2])
                    r1,r2 = which_region(regions,(s1,x1)),which_region(regions,(s2,x2))
                    
#               print(s1,s2,x1,x2,which_region(regions,(s1,x1)),which_region(regions,(s2,x2)))

               if not (r1,r2) in points:
                    points[r1,r2]=[]
               points[r1,r2].append((x1,x2))


     if False:
          rmin={}
          rmax={}
          for r1,r2 in points.keys():
               for x1,x2 in points[r1,r2]:
                    if not r1 in rmin:
                         rmin[r1]=x1
                         rmax[r1]=x1
                    if not r2 in rmin:
                         rmin[r2]=x2
                         rmax[r2]=x2
                    if x1 > rmax[r1]: rmax[r1]=x1
                    if x1 < rmin[r1]: rmin[r1]=x1
                    if x2 > rmax[r2]: rmax[r2]=x2
                    if x2 < rmin[r2]: rmin[r2]=x2

     roffset={}
     if args.buff > 1000:
         buff = args.buff
     else: buff = 1000
     x=-buff
     for i in range(len(regions)):
          x+=buff
          roffset[i] = x
          x+= regions[i][2]-regions[i][1]

     for contig,base,scaffold,start,end in contigs:
          for i in overlapping_regions(regions,(scaffold,start,end)):
               a,b = regions[i][1],regions[i][2]
               x,y = start,end
               xx = max(x,a)
               yy = min(y,b)
               if regions[i][3]:
                    dx = -xx + regions[i][2] + roffset[i]
                    dy = -yy + regions[i][2] + roffset[i]
               else:
                    dx =  xx - regions[i][1] + roffset[i]
                    dy =  yy - regions[i][1] + roffset[i]
               if args.raw:
                    print("c:",contig,i,scaffold,x,y,sep="\t")
               else:
                    print("c:",contig,i,scaffold,min(dx,dy),max(dx,dy),sep="\t")
     for scaf,pos in breaks:
         for i in overlapping_regions(regions,(scaf,pos,pos)):
             if regions[i][3]:
                 plot_pos = regions[i][2] + roffset[i] - pos
             else:
                 plot_pos = pos - regions[i][1] + roffset[i]
             print("break:",scaf,plot_pos,sep="\t")


     if args.hra:
          import hirise.chicago_edge_scores as ces
          #import hirise.chicago_edges_scores as ces
          ces.set_exp_insert_size_dist_fit_params(hra.model_params)
          model = ces.model
          outfile = sys.stdout

          for i in range(len(regions)):
               pairs =[]
               r=regions[i]
               scaffold = r[0]
               slen = hra.scaffold_lengths[scaffold]

               for x,y in points[i,i]:
                    pairs.append((x,y))
                    if args.debug: print("ppx:",x,y)
#                    pairs.append( (scaffold , x,y,"d1","d2",   0,  0,     0      , scaffold,     0,   0,  0,   scaffold,     0,   0, 0  ,"d3")  )
#                    pair_buffer[s1].append((s1,a  ,b  ,c1,c2,0,  0,     0      , s1,     0,   0,  0,   s2,     0,   0, 0  ,tid) )

               handle_scaffold(scaffold,slen,pairs,outfile,model,hra,regions[i][1],roffset[i],t1=args.t1,t2=args.t2,maxx=200000,minx=500,binwidth=1500,nreject=2,debug=args.debug)

          
     for r1,r2 in points.keys():
          for x1,x2 in points[r1,r2]:
               if regions[r1][3]:
                    dx1 =-x1 + regions[r1][2] + roffset[r1]
               else:
                    dx1 = x1 - regions[r1][1] + roffset[r1]
               if regions[r2][3]:
                    dx2 =-x2 + regions[r2][2] + roffset[r2]
               else:
                    dx2 = x2 - regions[r2][1] + roffset[r2]
               x=min(dx1,dx2)
               y=max(dx1,dx2)
               if args.raw:
                    print("x:",min(x1,x2),max(x1,x2),r1,r2,regions[r1][1],regions[r2][1],roffset[r1],roffset[r2])
               else:
                    print("x:",x,y,r1,r2,regions[r1][1],regions[r2][1],roffset[r1],roffset[r2])

