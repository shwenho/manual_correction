#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import re
import string
import numpy as np
import random 

nparts = 4

print("#",2**32)

k=20

asize = old_div(2**32,nparts)
print("#",asize)

nn1= k*3
n1m = list(range(nn1)) #np.zeros(nn1,dtype=np.dtype('u8'))

#print "#",n1m
print("#")
print("#")
n1m[0]=int(1)
n1m[1]=int(2)
n1m[2]=int(3)
for i in range(1,k):
    n1m[3*i  ]=int(n1m[3*(i-1)  ]<<2)
    n1m[3*i+1]=int(n1m[3*(i-1)+1]<<2)
    n1m[3*i+2]=int(n1m[3*(i-1)+2]<<2)

#n=0
#for i in n1m:
#    print n,i
#    n+=1

def neighbors1(i):
    a=[]
    for j in range(k*3):
        a.append(int(i)^int(n1m[j]))
    return a

mers = {}

tt = string.str.maketrans("ACTG","TGAC")

def rc(s):
    s = s.translate(tt)
    return s[::-1]

k=20
bases=["A","C","T","G"]
bits = {"A":0, "C":1, "T":2, "G":3}

def int2seq(x):
    s=""
    for i in range(k):
        b =  x & 3
        x = x>>2
        s = s+bases[b]
    return s

def seq2int(s):
    ii=int(0)
#    a= s.split()
    #for b in s.split():
    for i in range(k):
        ii = ii<<2
        b = s[k-1-i]
        #print "#",b,bits[b],ii
        ii += bits[b]
    return ii

bcf = "/mnt/nas/projects/array/detroit/barcodes-1224.90k.p5.25bp.txt"
f = open(bcf)
while True:
    l = f.readline()
    if not l: break
    c = l.strip().split()
    id = c[0]
    bc = rc(c[1][0:k])
#    m=bc.find("N")
#    print m
#    print id,bc #,rc(bc),len(bc),l.strip()
    x = seq2int(bc)
    mers[x]=(id,0,0)
#    print bc,mers[x],neighbors1(x)

    xxx = neighbors1(x)
    for i in range(len(xxx)):
        if x==xxx[i]:
            print("wtf?",x,xxx[i],i,old_div(i,3),int2seq(x),int2seq(xxx[i]),type(x),type(xxx[i]))
        mers[xxx[i]] = (id,1,i,old_div(i,3),i%3,bc)
f.close()

def deletions(s):
    ll = []
    ll.append( (s[1:k+1],0) )
    for i in range(1,k):
        ll.append(  ( s[:i]+s[i+1:k+1] ,i) ) 
    ll.append( (s[:k],k) )
#    for l in ll:
#        print s,l
    return(ll)

def insertions(s):
    ll = []
    ll.append( ("A"+s, 0))
    for i in range(1,k):
        ll.append(( s[:i]+"A"+s[i:] ,i))
    for i in range(1,k):
        ll.append(( s[:i]+"C"+s[i:] ,i))
    for i in range(1,k):
        ll.append(( s[:i]+"G"+s[i:] ,i))
    for i in range(1,k):
        ll.append(( s[:i]+"T"+s[i:] ,i))
    return(ll)

while True:
    l = sys.stdin.readline()
    if not l: break
    p= l.find("AAGATC")
    if p==k:
        bc = l[:k]
        if bc.find("N")>=0: 
#            print "<<#"
            continue
        x = seq2int(bc)
        print(bc,x,mers.get(x))
    elif p==k+1:
        print("##in")
        bc = l[:k+1]
        if bc.find("N")>=0: 
            print("<<#")
            continue
        vcb = deletions(bc)
        for v in vcb:
#            print v
            x = seq2int(v[0])
            if x in mers:
                print(bc,v,x,mers.get(x),"#in")
        print("<<#")
    elif p==k-1:
        print("##del")
        bc = l[:k-1]
        if bc.find("N")>=0: 
            print("<<#")
            continue
        vcb = insertions(bc)
        for v in vcb:
#            print v
            x = seq2int(v[0])
            if x in mers:
                print(bc,v,x,mers.get(x),"#del")
        print("<<#")
#        matches = []
        



