#!/usr/bin/env python3


import argh

@argh.arg("-i", "--hra", required=True)
@argh.arg("-l", "--llr", required=True)
def main(hra=None, scores=None, output="/dev/stdout"):
    llr = load_scores(scores)
    scaffolds = read_scaffolds(hra)
    with open(output, 'w') as output_handle:
        for scaffold in scaffolds:
            request_scores(scaffold, output_handle)

def read_scaffolds(hra):
    scaffolds = []
    this_scaffold = []
    with open(hra) as hra_handle:
        prev_scaffold = None
        this_contig = None
        contig_start = None
        for line in hra_handle:
            if line.startswith("P "):
                s = line.split()
                scaffold_name = s[1]
                contig_name = s[2]
                contig_start = int(s[5])
                contig_length = int(s[6])
                new_name = contig_name + "_" + s[3]
                if scaffold_name != prev_scaffold:
                    if prev_scaffold:
                        scaffolds.append(this_scaffold)
                    prev_scaffold = scaffold_name
                    this_scaffold = []

                this_scaffold.append((new_name, contig_start, contig_start + contig_length,))
                next(hra_handle)
        scaffolds.append(this_scaffold)
    return scaffolds

def load_scores(scores_file):
    scores = {}
    with open(scores_file) as handle:
        for line in handle:
#            print("orp",line.strip())
            if line.startswith("#"):
                continue
            split_line = line.split()
            scaffold1 = split_line[0]
            scaffold2 = split_line[1]
            gapsize = int(split_line[5])
            for orientation, score in zip(split_line[6:10], split_line[14:18]):
                score = float(score)

                o1, o2 = orientation
                if o1 == "<":
                    o1 = 1
                else:
                    o1 = 0
                if o2 == "<":
                    o2 = 1
                else:
                    o2 = 0
                
                key = make_key(scaffold1, scaffold2, o1, o2, gapsize)
                scores[key] = score
                #print("orp",orientation,scaffold1,scaffold2,score,key,score)
    return scores

if __name__ == "__main__":
    argh.dispatch_command(main)
