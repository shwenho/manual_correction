#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math
debug=False 


human_accessions={
"CM000683.2": "21",
"CM000668.2": "6",
"CM000669.2": "7",
"CM000680.2": "18",
"CM000677.2": "15",
"CM000663.2": "1",
"CM000674.2": "12",
"CM000684.2": "22",
"CM000686.2": "Y",
"CM000681.2": "19",
"CM000672.2": "10",
"CM000679.2": "17",
"CM000664.2": "2",
"CM000666.2": "4",
"CM000673.2": "11",
"CM000667.2": "5",
"CM000685.2": "X",
"CM000678.2": "16",
"CM000676.2": "14",
"CM000670.2": "8",
"CM000675.2": "13",
"CM000665.2": "3",
"CM000682.2": "20",
"CM000671.2": "9"
}

import re

N=3
nstates = 2**N
nmask = nstates -1
length={}
debug=False

def log(s):
    sys.stderr.write(  "# %s\n" %(s) )

def sta(l):
    if int(l[8])<int(l[9]):
        return 0
    else:
        return 1

def process_lines(b,r):
#    for i in b:
#        print i
    b.sort(key=lambda x: (x[0],float(x[11])), reverse=True )
    last=""
    for i in b:
        if not i[0]==last:
            m=re.match("gi\|(\d+)\|gb\|(.*)\|",i[1])
            id=human_accessions[m.group(2)]
#            print "\t".join([i[0],id,str(sta(i)) ])#,"\t".join(i)])
            r[i[0]] = ( id,sta(i) )
#        else:
#            print "#"+"\t".join([i[0],i[1],str(sta(i)),"\t".join(i)])
        last=i[0]
        

def pairs_to_lengths(pairs,ro,gap_length,l1,l2):
    for p in pairs:
        l=0
        if   ro==0:
            l= (1+gap_length + (l1-p[0])+p[1])
        elif ro==1:
            l= (1+gap_length + (l1-p[0])+(l2-p[1]))
        elif ro==2:
            l= (1+gap_length + p[0]+p[1])
        elif ro==3:
            l= (1+gap_length + p[0]+(l2-p[1]))
        if (l<=0):
            print("wtf? l<0",p,l,ro,gap_length,l1,l2,pairs)
        yield l

def logp(x):
    return(-1.0*math.log(x))

def get_score(s1,s2,relative_orientation,gap_length,scores):
#    print "get_score",relative_orientation
    lp = 0.0
    for frag_length in pairs_to_lengths(scores.get((s1,s2),[]),relative_orientation,gap_length,length[s1],length[s2]):
        lp += logp(frag_length)
    if debug: print(s1,s2,relative_orientation,gap_length,len(scores.get((s1,s2),[])),lp)
    return lp


def flip(x):
    if x==0:
        return 1
    if x==1:
        return 0
    print("error: unexpected strand digit")
    exit(0)

def score_gap(string,final,i,scores):
    r1=0.0
    r2=0.0

    for j in range(max(i-N,0),i):
        gap_length = sum( [ length[string[ii]] for ii in range(j+1,i) ] )
        ro = 2*final[j]+     final[i]
        r1 += get_score( string[j], string[i], ro , gap_length , scores )
        ro = 2*final[j]+flip(final[i])
        r2 += get_score( string[j], string[i], ro, gap_length ,scores )

    for j in range(i+1,min(i+N,len(string))):
        gap_length = sum( [ length[string[ii]] for ii in range(i+1,j) ] )
        ro = 2*final[i]+      final[j]
        r1 += get_score( string[i], string[j], ro , gap_length ,scores )
        ro = 2*flip(final[i])+final[j]
        r2 += get_score( string[i], string[j], ro , gap_length ,scores )
    return (r1-r2,r1,r2)

#def incr_chain_score(string,i,state):
#    r=0.0
#    bits = state
#    s2 = string[i]
#    for x in range(i-N,i):
#        s1 = string[x]
#        relative_orientation =  (((bits>>(i-x))&1)<<1) | (bits&1)

#        gap_length = sum( [ length[string[ii]] for ii in range(x+1,i) ] )
#        r += get_score(s1,s2,relative_orientation,gap_length)
#    return r


def comparative_score(st,bits,blast_data):
    orientation_comparison={}

    hits={}

    for i in range(len(st)):
        s=st[i]
        b=(bits>>i)
        bd =  blast_data.get(s,(-1,-1))

        orientation_comparison[s]=( bd[0] , bd[1] , (bits>>(len(st)-1-i)&1) )
        hits[bd[0]]=1

    n_implied_flips=0
    for k in list(hits.keys()):
        alts=[]
        for o in (0,1):
            n=0
            for s in st:
                x=orientation_comparison[s]
                if x[0]==k and (x[1]+o)%2==x[2]:
                    n+=1
            alts.append(n)
        n_implied_flips+=min(alts)

    for s in st:
        pass

    return(-5.0*n_implied_flips)

def incr_chain_score(string,i,state,scores,blast_data):

    bits = state
    r = comparative_score(string[i+1-N:i+1],bits,blast_data)
    s2 = string[i]
    for x in range(i-N,i):
        s1 = string[x]
        relative_orientation =  (((bits>>(i-x))&1)<<1) | (bits&1)

        gap_length = sum( [ length[string[ii]] for ii in range(x+1,i) ] )
        r += get_score(s1,s2,relative_orientation,gap_length,scores)
    return r

def init_chain_score(string,i,state,scores,blast_data):
    bits = state #& (2**i-1) 
    r = 0.0

    r += comparative_score(string[:i+1],bits>>(N-1-i),blast_data)

    for x in range( min(len(string),i) ): 
        s1 = string[x]
        for y in range( x+1,min(len(string),i+1) ):
            s2 = string[y]
            relative_orientation = (((bits>>(i-x))&1)<<1)  | ((bits>>(i-y))&1)
            #r += scores[s1,s2][relative_orientation]
            gap_length = sum( [ length[string[ii]] for ii in range(x+1,y) ] )
            r += get_score(s1,s2,relative_orientation,gap_length,scores)

    if debug: print("r:",r)
    return r

def orient_string(s,n,scores,blast_data={}):
    debug=False
    if debug: print("or:",s,n)
    shorty=False
    if len(s)<=N:
        print("#shorty", len(s))
        shorty=True

    #print "#",s
    sc={}
    bt={}
    for state in range(nstates): #range(min(len(s),nstates)):            
        nN = min(len(s)-1,N-1)
        sc[(nN,state)] = init_chain_score(s,nN,state,scores,blast_data)

    for i in range(N,len(s)):

        my_scores = {}
        for state in range(nstates):
            options = []
            for j in range(2):
                pstate = ( state >> 1) | (j << (N-1))
                options.append( (sc[(i-1,pstate)] + incr_chain_score(s,i,state,scores,blast_data), pstate) )
            options.sort(reverse=True)
            if debug: print(options)
            sc[(i,state)] = options[0][0]
            bt[(i,state)] = options[0][1]

        if debug: print("\t".join(map(str, ["or:",n,i,s[i]] + [ sc[i,state] for state in range(nstates) ] + [bt[i,state] for state in range(nstates)]   )))

    best_score = max( [ sc[len(s)-1, state ] for state in range(nstates)] )
    best_state = [ state for state in range(nstates) if sc[len(s)-1,state]==best_score ][0]


    i = len(s)-1
    final={}
    final[i] = best_state & 1
    if (i,best_state) not in bt:
        print("missing key", i,best_state,len(s),"#",bt)

    if shorty: print(i,best_score,best_state,final)

    if i>N:
        prev = bt[i,best_state]
    else:
        prev = best_state

    while i >N:
        i=i-1
        final[i] = prev&1
        prev = bt[i,prev]

    while i > 0:
        i=i-1
        final[i] = prev&1
        prev = prev >> 1

    return(final,score_gap)

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-s','--strings',required=True)
    parser.add_argument('-l','--lengths',required=True)
    parser.add_argument('-N','--nback',default=3,type=int)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-G','--gaps', default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
#    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-b','--megablast' )

    args = parser.parse_args()

    blast_data={}
    f=open(args.megablast)
    b=[]
    while True:
        l=f.readline()
        if not l: break
        if l[0]=="#":
            process_lines(b,blast_data)
            b=[]
        else:
            b.append(l.strip().split())
    f.close()
    if args.progress: log( str( "Done loading "+args.megablast ) )

    N=args.nback
    nstates = 2**N
    nmask = nstates -1
    debug=args.debug

    if args.debug:
        args.progress=True
        debug=True
#    if args.progress: log( str(args) )

#    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))
#    print "Done reading edgelist"

    length={}    
    f = open(args.lengths)
    while True:
        l = f.readline()
        if not l:   break
        if l[0]=="#": continue
        c=l.strip().split()
        length[c[0]]=int(c[1])
    f.close()

    f = open(args.strings)
    strings = []
    while True:
        l = f.readline()

        if not l:   break
        if l[0]=="#": continue
        if not l[:2]=="y:": continue
        c=l.strip().split()
        st =  eval( " ".join(c[1:]))
#        print st
        strings.append( st )
        
    f.close()

    scores={}
    for s in strings:
        for i in range(1,len(s)):
            for j in range(N):
                scores[s[max(0,i-j-1)],s[i]]=[]
                

#scf896052939 scf895944060 7468 1941 2 [(6558, 1429), (6863, 1197)]

    while True:
        l = sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c = l.strip().split()
        if len(c)<5: continue
        if (c[0],c[1]) in scores:
            scores[c[0],c[1]]= eval(" ".join(c[5:]))
#            print scores[c[0],c[1]]
        if (c[1],c[0]) in scores:
            pp = eval(" ".join(c[5:]))
            scores[c[1],c[0]]= [ (p[1],p[0]) for p in pp ] 
#            print scores[c[1],c[0]]

    n=0
    sc={}
    bt={}
    for s in strings:
        n+=1

        final,score_gap=orient_string(s,n,scores,blast_data)
        
        for i in range(len(s)):
            if args.gaps:
                r="\t".join(map(str,score_gap(s,final,i,scores)))
                print("\t".join(map(str,["x:",n,i,s[i],final[i],length[s[i]],len(s),r])))
            else:
                print("\t".join(map(str,["x:",n,i,s[i],final[i],length[s[i]],len(s)])))

