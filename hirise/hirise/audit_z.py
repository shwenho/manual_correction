#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import sys

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-t','--threshold',default=0.0 ,  type=float)
    parser.add_argument('-r','--ref')
    parser.add_argument('-a','--ass') 
    parser.add_argument('-L',default=10000.0,type=float) 
    parser.add_argument('-T','--tolerance',default=0.95,type=float) 
    parser.add_argument('-t','--thin',default=100,type=int) 
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    args = parser.parse_args()
    #ces.debug=args.debug
    if args.debug:
        args.progress=True
    if args.progress: log( str(args) )

#    targets = [5000,10000,50000,100000]

    thin=1
    p_buffer=[]
#    buffer_len=args.A
    lastx=0
    lasts=-1
    lp1=False
    lp2=False
    f=open(args.ref)
#    skipped=args.skip
    started=False
    while True:
        l=f.readline()
        if not l: break
#        if skipped<args.skip: 
#            skipped +=1
#            continue
#        else:
#            skipped=0
        c=l.strip().split()
        if len(c)<7: continue
        if c[2]=="None": c[2]="-1"

        p1=(c[1],int(c[2]),c[3]) #position 1
        p2=(c[4],int(c[5]),c[6]) #position 2

#        print len(p_buffer)

        while len(p_buffer)>0 and ( ( not p_buffer[0][1][0]==p2[0] ) or (( p2[1]-p_buffer[0][1][1] ) >args.L)): 
            p_buffer.pop(0)
            started=True

        if started and len(p_buffer)>0:
#        for i in range(min(args.B,len(p_buffer))):
            thin +=1
            if thin % args.thin == 0:
                lp1,lp2 = p_buffer[0]
                if lp2 and p2[0]==lp2[0]: # p2 same chromosome

                    if p1[0]==lp1[0] and (not p1[0]=="None") :
                        synt=1
                        sm=1
                        if lp2[2]==p2[2] and not lp1[2]==p1[2]: sm=0
                        if not lp2[2]==p2[2] and lp1[2]==p1[2]: sm=0
                        delta = abs(p2[1]-lp2[1])
                        if delta > args.tolerance * args.L:
                            print(abs(p1[1]-lp1[1]),abs(p2[1]-lp2[1]),sm,synt,lp1,lp2,p1,p2)
                    else:                 # p2 different chromosome
                        synt=0
                        sm=0
                        delta = abs(p2[1]-lp2[1])
                        if delta > args.tolerance * args.L:
                            #print abs(p1[1]-lp1[1]),abs(p2[1]-lp2[1]),sm,synt,lp1,lp2,p1,p2
                            print(1.0e10,abs(p2[1]-lp2[1]),sm,synt,lp1,lp2,p1,p2)

        p_buffer.append( (p1,p2) )

                
#        lp1=p1 #last p1
#        lp2=p2 #last p2
#            print "#",l.strip()
         #   continue

        
    f.close()

