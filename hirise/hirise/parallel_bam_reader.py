#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
from hirise.hirise_assembly import HiriseAssembly
import multiprocessing
from multiprocessing import Pool, Queue, Process, JoinableQueue
import hirise.chicago_edge_scores as ces
from chicago_support_bootstrap import pairs2support
import gc

def pair2bin2D(pair,binwidth):
     a,b=pair
     return( int(a/binwidth),int(b/binwidth) )

#def pairs2support(scaffold,pairs,model,slen=0,masked_segments=[],mapper=None,buff=500,debug=False,t1=20.0,t2=5.0,gpf=False,minx=1000,maxx=1e7,joins=[],logfile=False,binwidth=1000,nreject=2):


def handle_scaffold(q,myid,outfile,model,hra,maxx=200000,minx=500,binwidth=1500):
     ns=[0,0,0]
     buff=0
     while True:
          scaffold,slen,pairs = q.get()
          outfile.write("#{}\t{}\n".format(scaffold,slen))
          pairs2support(scaffold,pairs,model,slen,logfile=outfile,masked_segments=hra.make_scaffold_mask(scaffold))
          outfile.flush()
          q.task_done()

def read_pairs_from_bam(inq,i,bamfile,hra):
     print("x",i,bamfile)
     ns=[0,0,0]
     buff=0

     pair_buffer={}

     scaffold_contig_counts={}
     contig_scaffold_counts={}

     for c,s in hra.contig_scaffold.items():
          contig,base = c
          if not s in scaffold_contig_counts: 
               scaffold_contig_counts[s]={}
          if not contig in contig_scaffold_counts:
               contig_scaffold_counts[contig]={}

          scaffold_contig_counts[s][contig] = scaffold_contig_counts[s].get(contig,0)+1
          contig_scaffold_counts[contig][s] = contig_scaffold_counts[contig].get(s,0)+1

          #print(s,contig,scaffold_contig_counts[s][contig])

#     def process_scaffold(scaffold):
#          print("process:",scaffold,len(pair_buffer.get(scaffold,[])),pair_buffer.get(scaffold,[]),sep="\t")
#          print("process:",scaffold,len(pair_buffer.get(scaffold,[])),sep="\t")
#          q.put((scaffold,hra.scaffold_lengths[scaffold],pair_buffer.get(scaffold,[])))
#          print(multiprocessing.active_children())
#          if scaffold in pair_buffer: del pair_buffer[scaffold]

#     gc.disable()
     for row in hra.chicago_pairs(scaffold_contig_counts=scaffold_contig_counts,contig_scaffold_counts=contig_scaffold_counts,callback=False,bamfile=bamfile):
          if len(row)==7:
               s1,s2,a,b,c1,c2,tid=row
          else:
               print(i,row)
               done_scaffold=row[0]
               inq.put( (i,done_scaffold,pair_buffer.pop(done_scaffold,[])  ) )
               
          if not s1==s2: continue
          
#          print(s1,s2,x,y,scaffold_contig_counts.get(s1),sep="\t")
          if not s1 in pair_buffer: 
               pair_buffer[s1]=[]
          pair_buffer[s1].append((s1,a  ,b  ,c1,c2,0,  0,     0      , s1,     0,   0,  0,   s2,     0,   0, 0  ,tid) )
          if (len(pair_buffer[s1])+1)%50000==1: 
               print("#",i,bamfile,s1,len(pair_buffer[s1]),sep="\t")
#          yield( sca,z1a,z2p,c1,c2,seg,contig,ncontig,scaffold,z1a,z2a,z3a,nscaffold,z2p,x2p,z3p,aln.query_name )
#     gc.enable()

#     while True:
#          scaffold,slen,pairs = q.get()
#          pairs2support(scaffold,pairs,model,slen,logfile=outfile)
#          outfile.flush()
#          q.task_done()





if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")

     parser.add_argument('-i','--infile',help="File containing .hra formatted hirise assembly.")
     parser.add_argument('-o','--outfile',help="Output file name.")
     parser.add_argument('-j','--nthreads',help="Number of threads.",type=int,default=16)

     args = parser.parse_args()

     hra = HiriseAssembly()
     hra.load_assembly(args.infile)

     ces.set_exp_insert_size_dist_fit_params(hra.model_params)
     model=ces.model

     nbams = len(hra.bams)
     inq = JoinableQueue(maxsize=0)
     for i in range(nbams):
          reader = Process(target=read_pairs_from_bam, args=(inq,i,hra.bams[i],hra),daemon=False)
          reader.start()

     q = JoinableQueue(maxsize=0)
     outfiles = [ open("{}.part.{}".format(args.outfile,i),"wt") for i in range(args.nthreads) ]

     for i in range(args.nthreads):
          worker = Process(target=handle_scaffold, args=(q,i,outfiles[i],model,hra),daemon=True)
          worker.start()

     pair_buffer={}
     scaffold_count={}
     while True:
          procid,scaffold,pairs = inq.get()
          if not scaffold in pair_buffer:
               pair_buffer[scaffold]=[]
          pair_buffer[scaffold] += pairs
          scaffold_count[scaffold] = scaffold_count.get(scaffold,0)+1
          print("##############3",procid,scaffold,scaffold_count.get(scaffold),len(pairs),len(pair_buffer[scaffold]))
          
          if scaffold_count[scaffold] == nbams:
               print("#q.put",scaffold)
               q.put((scaffold,hra.scaffold_lengths[scaffold],pair_buffer.pop(scaffold,[])))

#          pairs2support(scaffold,pairs,model,slen,logfile=outfile)
#          outfile.flush()
          inq.task_done()

     q.join()
     inq.join()
#     for fh in outfiles:
#          print(fh)
#          fh.flush()
#          fh.close()
