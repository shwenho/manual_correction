#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
from hirise.hirise_assembly import HiriseAssembly
#import struct
#import hashlib
import re    
import pysam
#import BamTags
from hirise.bamtags import BamTags
import sys
 
def in_regions(regions,x):
    for r in regions:
        if not x[0]==r[0]: continue
        if r[2]==-1: return True
        if (min(x[1:])>=r[1] and min(x[1:])<=r[2]) or (max(x[1:])>=r[1] and max(x[1:])<=r[2]): return True
    return False

def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False

if __name__=="__main__":
    import sys
    import argparse

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
    parser.add_argument('-R','--regions',default=[],  action="append",help="Regions to examine.")
    parser.add_argument('-B','--breaks',default=[],nargs='+',  action="store",help="Breakpoints to draw.")
    parser.add_argument('-q','--mapq',default=20,  type=float,help="Minimum map quality threshold.")
    parser.add_argument('-r','--range',default=300000,  type=int,help="Default amount to show from a scaffold end.")
    parser.add_argument('-p','--pad',default=0,  type=int,help="Expand specified ranges by this amount.")
    parser.add_argument('-m','--minsep',default=0,  type=int,help="Hide short pairs for speed.")
    parser.add_argument('-X','--maxsep',default=1.0e20,  type=float,help="Max pair sep.")
    parser.add_argument('-i','--infile',default=False,help="Filename for serialised assembly input file.")
    parser.add_argument('-o','--outfile',default=False,help="Filename for output.")
    parser.add_argument('-b','--bamfile',default=[],action='append',help="Names of bamfiles, for reading directly from them.")

    args = parser.parse_args()
    print(args.regions, file=sys.stderr)
    if args.infile and args.bamfile:
        print("Specify bam files or a .hra file, not both.")
        exit()
#        raise Exception 

    if args.infile:
        if args.debug: print("Load in hra",file=sys.stderr)
        hra = HiriseAssembly()
        hra.load_assembly(args.infile)
        if args.debug: print("done loading in hra",file=sys.stderr)

        if len(hra.layout_lines)==0: 
              hra.make_trivial_layout_lines(debug=args.debug,ocontig_ids=True)

    if args.outfile:
        of=open(args.outfile,"wt")
    else:
        of=sys.stdout

    scaffolds={}
    ranges=[]

    if args.infile:

        #Parse scaffold ranges, store in 'ranges' list
        for i,region in enumerate(args.regions):
            result = re.findall("(.*):(\d*)-(\d+)(F?)$",region.strip())
            if result:
                ((scaffold,start,end,f),) = result 
                if args.debug: print("#",(scaffold,start,end,f))
                if start:
                    if args.pad:
                        start = max(0,int(start)-args.pad)
                        end = min(hra.scaffold_lengths.get(scaffold,1e10) ,int(end)+args.pad)
                else:
                    start = hra.scaffold_lengths.get(scaffold)-int(end)
                    end = hra.scaffold_lengths.get(scaffold)
                    if args.pad:
                        start-=args.pad
                        start=max(0,start)
            else:
                scaffold,start,end,close_to_start = hra.contig_end_to_region(region,args.range)
                f=False
                if i%2==0 and close_to_start     : f= True
                if i%2==1 and not close_to_start : f= True


    #        scaffold,start,end,f = hra.contig_end_to_region()
            print("region:",scaffold,start,end,f,sep="\t")
            scaffolds[scaffold]=1
            ranges.append((scaffold,int(start),int(end)))
        scaffolds=list(scaffolds.keys())
        if args.breaks:
            breaks = []
            for break_str in args.breaks:
                parts = break_str.split()
                breaks.extend(parts)
            for breakpoint in breaks:
                result = re.findall("(.*):(\d*)$",breakpoint.strip())
                if result:
                    ((scaffold,pos),) = result 
                    pos = int(pos)
                    if not in_regions(ranges,(scaffold,pos,pos)):
                        print("Specified breakpoint not in specified regions: %s" % (breakpoint),file=sys.stderr)
                        sys.exit(1)
                    else:
                        print("break:",scaffold,pos,sep="\t")


        #Find all contigs that are in the scaffold ranges specified
        contigs={}
        for cl in hra.contig_coords():

            if cl["scaffold"] in scaffolds:

                for r in ranges:
                    if r[0]==cl["scaffold"] and pairs_overlap(cl["span"],(r[1],r[2])):
                        pass
                        print("contig:",cl["contig"],cl["base"],cl["scaffold"],cl["span"][0],cl["span"][1],cl["flipped"],sep="\t")
                        contigs[cl["contig"]]=1
        contigs=list(contigs.keys())

        #If either read in pair mapped outside of specified regions, do not print
        #Filtering for duplicates, if second in pair, map quality, and mapping
        #to our specified contigs is done in the hra member function
        if args.debug: 
            print(scaffolds,contigs,sep="\t")

        for p in hra.chicago_pairs_for_scaffolds(mapq=args.mapq,callback=False,bamfile=False,scaffolds=scaffolds,contigs=contigs,minsep=args.minsep,maxsep=args.maxsep,debug=args.debug):
            if not in_regions(ranges,(p[0],p[2])): continue
            if not in_regions(ranges,(p[1],p[3])): continue
            print(*p,sep="\t")

    else:

        for i,region in enumerate(args.regions):
            result = re.findall("(.*):(\d*)-(\d+)(F?)$",region)
            if result:
                ((scaffold,start,end,f),) = result 
                print("#",((scaffold,start,end,f),))
                if True:
                    if start:
                        if args.pad:
                            start = max(0,int(start)-args.pad)
                            end   = int(end)+args.pad
    #                        start = max(0,int(start)-args.pad)
    #                        end = min(hra.scaffold_lengths.get(scaffold,1e10) ,int(end)+args.pad)
                    else:
                        start = hra.scaffold_lengths.get(scaffold)-int(end)
                        end = hra.scaffold_lengths.get(scaffold)
                        if args.pad:
                            start-=args.pad
                            start=max(0,start)
            else:
                print("Couldn't parse region \"{}\"".format(region))
                exit()
#                scaffold,start,end,close_to_start = hra.contig_end_to_region(region,args.range)
#                f=False
#                if i%2==0 and close_to_start     : f= True
#                if i%2==1 and not close_to_start : f= True


    #        scaffold,start,end,f = hra.contig_end_to_region()
            print("region:",scaffold,start,end,f,sep="\t")
            scaffolds[scaffold]=1
            ranges.append((scaffold,int(start),int(end)))
        scaffolds=list(scaffolds.keys())
        #Find all contigs that are in the scaffold ranges specified


        for region in ranges:
            print("#",region)
            #print("contig:",region[0],1,region[0],region[1],region[2],sep="\t")


        for region in ranges: #[ "{}:{}-{}".format(r[0],r[1],r[2]) for r in ranges ]:
            print("#",region)
            region_str="{}:{}-{}".format(region[0],region[1],region[2])
            for bamfile in args.bamfile:
                bam=pysam.Samfile(bamfile,"rb") 
                print("#try to get",bamfile,region_str)
#                for aln in bam.fetch(region="{}:{}-{}".format(*region)):
                for aln in bam.fetch(region=region_str):
#                    print("x:",aln.pos)
                    if not BamTags.mate_mapq(aln)>args.mapq: continue
                    if not aln.mapq    > args.mapq: continue
                    c1 = bam.getrname( aln.tid )
                    c2 = bam.getrname( aln.rnext )
                    if not c2 == region[0]: continue
                    if not (region[1]<=aln.pnext and aln.pnext < region[2]): continue
                    if not (region[1]<=aln.pos   and aln.pos   < region[2]): continue
                    if args.maxsep < abs(aln.pos-aln.pnext) : continue
                    if args.minsep > abs(aln.pos-aln.pnext) : continue
                    print(c1,c2,aln.pos,aln.pnext,(c1,1),(c2,1),aln.query_name,sep="\t")
                bam.close()

#        for bamfile in args.bamfile:
#                    yield( scaffold, nscaffold, xx, yy, c1, c2, aln.query_name )
