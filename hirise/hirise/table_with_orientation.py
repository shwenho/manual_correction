#!/usr/bin/env python3


import argh
import sys
from collections import Counter

def main(table, orientation, output="/dev/stdout"):
    orient_data = get_orient_data(orientation)
    seen = Counter()
    with open(output, 'w') as output_handle:
        with open(table) as table_handle:
            for line in table_handle:
                contig, start, end = parse_table(line)
                key = (contig, start, end,)
                orientation_info   = orient_data.get(key, 1.0)
                seen[key] += 1
                print(line.rstrip(), orientation_info, sep="\t", file=output_handle)

    for contig_seg in orient_data.keys():
        count = seen[contig_seg]
        if count != 1:
            print("Count should be one, not {1} for segment {0}".format(contig_seg, count), file=sys.stderr)
            sys.exit(1)

def parse_table(line):
    s = line.split()
    contig = s[1]
    start  = s[2]
    end    = s[3]
    return contig, start, end

def get_orient_data(orientation):
    data = {}
    with open(orientation) as handle:
        for line in handle:
            s = line.split()
            contig = s[1]
            start  = s[2]
            end    = s[3]
            key = (contig, start, end,)
            orientation = s[9]
            data[key] = orientation
    return data


if __name__ == "__main__":
    argh.dispatch_command(main)
