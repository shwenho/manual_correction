#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import sys
import random
#import hirise.chicago_edge_scores as ces
import pysam
#from random import random
from bisect import bisect

cum_weights=[]
total_weight=0.0

def weighted_choice(l,weighting):
    global cum_weights
    global total_weight
    if len(cum_weights)==0:
        total = 0
        for i in l:
            total += weighting[i]
            cum_weights.append(total)
        total_weight=total
    x = random.random() * total_weight
    i = bisect(cum_weights, x)
    return l[i]

def get_nlinks_dict(c1,bams,name2index,args):
    count={}
#    index2=name2index[c2]
    for sam in bams:
        for aln in sam.fetch(region=c1):
            if (not aln.is_duplicate) and (aln.mapq >= args.mapq) and (aln.opt('xm') >= args.mapq):
                count[aln.rnext]=count.get(aln.rnext,0)+1
    return count

def get_nlinks(r1,r2,bams,name2index,args):
#    l1=ll[c1]
#    l2=ll[c2]
#    if l2<l1:
#        x = c1
#        c1= c2
#        c2= x
    c1,a1b1 = r1.split(":")
    c2,a2b2 = r2.split(":")
    a1,b1 = list(map(int,a1b1.split("-")))
    a2,b2 = list(map(int,a2b2.split("-")))
    count=0
    index2=name2index[c2]
    for sam in bams:
        nn=0
        for aln in sam.fetch(region=r1):
            nn+=1
            if (aln.rnext==index2) and (not aln.is_duplicate) and (aln.mapq >= args.mapq) and (aln.opt('xm') >= args.mapq):
                if aln.pnext >a2 and aln.pnext < b2:
                    count+=1
#        print "#",nn
    return count

def get_nlinks_spanning(c1,xx,ll,bams,name2index,args,gap):
    l1=ll[c1]

    count=0
#    index2=name2index[c2]
    for sam in bams:
        for aln in sam.fetch(region=c1):
            if (aln.rnext==aln.tid) and (not aln.is_duplicate) and (aln.mapq >= args.mapq) and (aln.opt('xm') >= args.mapq) and (aln.pos<xx-old_div(gap,2)) and (aln.pnext>xx+old_div(gap,2)):
                count+=1
    
    return count

def main():

    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-l','--links')
    parser.add_argument('-P','--param')
    parser.add_argument('-o','--outfile')
    parser.add_argument('-b','--bamfiles',required=True)
    parser.add_argument('-N','--ncontigs',default=25,type=int)
    parser.add_argument('-W','--window',default=100000.0,type=float)
    parser.add_argument('-q','--mapq',default=10,type=int)
    parser.add_argument('-G','--genomesize',default=3e9,type=float)
#    parser.add_argument('-n','--ncontigs',type=int,required=True)
    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()
    if args.seed != -1 :
        random.seed(args.seed)


    #ces.debug=args.debug
    if args.debug:
        args.progress=True

    
    oname={}
    bams=[]
    for bamfile in args.bamfiles.split(','):
        bams.append( pysam.Samfile(bamfile,"rb") )
        oname[bams[-1]]=bamfile
    h=bams[0].header
    seqs=h['SQ']

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]
    contigs = snam
    ncontigs = len(snam)

    ll={}
    name2index={}
    G=0.0
    for i in range(len(snam)):
        ll[snam[i]]=float(slen[i])
        G+=ll[snam[i]]
        name2index[snam[i]]=i

#    G=args.genomesize

    def area(l1,l2):
        return (old_div(l1,1000.0))*(old_div(l2,1000.0))

    rate=0.0
    totaln=0.0
    n_pairs=0
    gap=300.0

    while n_pairs < args.ncontigs:
        n_pairs+=1
        c1 = weighted_choice(contigs,ll)
        c2 = weighted_choice(contigs,ll)
        while c2==c1:
            c2 = weighted_choice(contigs,ll)

        x1 = random.random()*( ll[c1]-args.window )
        x2 = random.random()*( ll[c2]-args.window )
        l1,l2=ll[c1],ll[c2]
        
        r1= "{}:{}-{}".format(c1,int(x1),int(x1+args.window))
        r2= "{}:{}-{}".format(c2,int(x2),int(x2+args.window))
        nl=get_nlinks(r1,r2,bams,name2index,args) 
        print(nl,c1,c2,x1,x2,l1,l2,r1,r2)

if __name__=="__main__":
    main()
