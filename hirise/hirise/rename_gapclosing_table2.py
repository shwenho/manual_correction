#!/usr/bin/env python3


import re
import argh
import os
import shutil
from datetime import date
import subprocess
import sys
import gzip

def main(table,in_gc="/dev/stdin",out_gc="/dev/stdout"):
    in_fh = open(in_gc,"r")
    out_fh = open(out_gc,"w")
    name_map = get_name_map(table)
    for line in in_fh:
        parts = line.strip().split()
        scaf_id = re.search("\d+",parts[0]).group()
        new_id = name_map[scaf_id] 
        parts[0] = new_id
        print("\t".join(parts),file=out_fh)
    in_fh.close()
    out_fh.close()



def get_name_map(table):
    fh = open(table)
    name_map = {}
    last_scaf = None
    for line in fh: 
        parts = line.split()
        scaf_name = parts[0]
        m=re.match("Sc(.*);HRSCAF=(\d+)",scaf_name)

        if not m:
            continue
        hr_id = m.group(2)
        name_map[hr_id] = scaf_name
    return name_map


if __name__ == "__main__":
    argh.dispatch_command(main)
