#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import pysam
from tables import *
import os

class ChicagoPair(IsDescription):
     contig1 = UInt32Col()
     contig2 = UInt32Col()
     x = UInt32Col()
     y = UInt32Col()

class Reference(IsDescription):
     id   = UInt32Col()
     name = StringCol(32)
     length = UInt32Col()

def read_bam_into_new_h5group(bamfile,h5file):
     bamname = bamfile.filename
     enrows=bamfile.mapped/2
#     print("# {}".format(bamname))
 #    help(h5file)
     groups=[]
     for group in h5file.walk_groups():
          groups.append(group)
#          print("####################  "+str(x))
     for gg in groups:
          print(gg)
     groupn = len(groups)
     groupname = "bam{}".format(groupn)
     filters = Filters(complevel=1, complib='blosc', fletcher32=True)
     group = h5file.create_group("/", groupname, 'Chicago pairs from {}'.format(bamname.decode()),filters=filters)

     table = h5file.create_table(group, 'pairs', ChicagoPair, "Chicago pairs",expectedrows=enrows)
     chicago_pair = table.row

     alniter = bamfile.fetch(until_eof=True)
     for a in alniter:
          if a.mapq<args.mapq: continue
          if a.is_duplicate: continue
          if BamTags.mate_mapq(a)<args.mapq: continue
          c1,c2,x,y = a.tid,a.rnext,a.pos,a.pnext
          if not (c1,x)<(c2,y): continue
          chicago_pair['contig1'] = c1
          chicago_pair['contig2'] = c2
          chicago_pair['x'] = x
          chicago_pair['y'] = y
          chicago_pair.append()
     table.flush()
     table.cols.contig1.create_index()
     table.cols.contig2.create_index()

     table2 = h5file.create_table(group, 'references', Reference, "Reference info")
     
     h=bamfile.header
     seqs=h['SQ']

#     if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )
     ref = table2.row
     for i in range(len(seqs)):
          ref['id']=i
          ref['name']=seqs[i]['SN']
          ref['length']=seqs[i]['LN']
          ref.append()
     table2.flush()
 

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-b','--bam',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-q','--mapq',required=False,type=float,default=10)
parser.add_argument('-p','--progress',default=False,action="store_true")

args = parser.parse_args()
if args.progress: print("#",args)

from hirise.bamtags import BamTags

h5file = open_file(args.outfile, mode = "w", title = "Chicago pairs from {}".format(args.bam))


bams = args.bam.split(",")
print(bams)
for bamfilename in bams:
     bam = pysam.Samfile(bamfilename,"rb")
     print("############")
     read_bam_into_new_h5group(bam,h5file)


for group in h5file.walk_groups():
     print(group)


h5file.close()


