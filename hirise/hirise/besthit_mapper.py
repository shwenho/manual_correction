#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import map
#!/usr/bin/env python3
import sys
import hirise.mapper
import pickle as pickle
import argparse

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

parser = argparse.ArgumentParser()
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('--map_file',default=False)
#parser.add_argument('--refmap',default=False)

args = parser.parse_args()
if args.debug:
    args.progress=True

if args.progress: log( str(args) )

mapp = pickle.load(open(args.map_file))
#rmap = pickle.load(open(args.refmap))

lengths={}
pairs={}
name2pair={}

final_mapping={}

while True:
    l = sys.stdin.readline()
    if not l: break

#    print l.strip()

    if l[0]=='#': continue
    if not l[:5]=="best:": continue
    c = l.strip().split()
    if len(c)==0: 
        print("#XXXX:",l.strip())
        continue
    s1 = c[1]
    score=float(c[2])
    chro=c[3]
    strand=c[4]
    start=int(c[5])
    end=int(c[6])
    
    n = mapp.find_node_by_name(s1)
    r = n.root
#    print l.strip(),mapp.print_name(r),r
    new_name = mapp.print_name(r)
    final_mapping[new_name] = final_mapping.get(new_name,[]) + [ (start,end,chro,strand,n.strand) ] 
#    o_strand = 

ss2st={('+',1):"+", ('-',-1):"+", ('+',-1):"-", ('-',1):"-"}

for new_name in list(final_mapping.keys()):
    hh=final_mapping[new_name]
#    print hh
    sc={}
    for h in hh:
        sc[h[3],h[4]] = sc.get((h[3],h[4]),0)+1
    ss = list(sc.keys())
    ss.sort(key=lambda x: sc[x], reverse=True)
    strand = ss2st[ss[0]]
#    print strand,ss[0],sc,hh
    chro = hh[0][2]
    start = min( [ h[0] for h in hh ] + [ h[1] for h in hh] )
    end = max( [ h[0] for h in hh ] + [ h[1] for h in hh] )
    print("\t".join(map(str,["best:",new_name,1,chro,strand,start,end,1,1,end-start])))
