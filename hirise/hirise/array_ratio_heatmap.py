#!/usr/bin/env python3
from builtins import map
from builtins import range
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import random
import sys
#566 97 33
#566 98 22
#566 99 67
#nput@bigdove1:~$ cat count_matrix.txt  | sort -k2n | tail 
#99 162 0



#mat = np.zeros((163,567))
mat1 = np.zeros((567,163))
mat2 = np.zeros((567,163))
#mat = np.zeros((100,100))

for i in range(567):
    for j in range(163):
        mat2[i,j]=1

f1 = sys.argv[1]
f2 = sys.argv[2]

f = open(f1)
while True:
    l = f.readline() #sys.stdin.readline()
    if not l:break
    x,y,n = list(map(int,l.strip().split()))
    mat1[x,y]=n+1
f.close()
f = open(f2)
while True:
    l = f.readline() #sys.stdin.readline()
    if not l:break
    x,y,n = list(map(int,l.strip().split()))
    mat2[x,y]=n+1
f.close()

mat = np.divide(mat1,mat2 )

#plt.pcolor(mat,norm=LogNorm(vmin=0.001))
#plt.colorbar()
#plt.show()
if False:
        plt.pcolor(mat)
        plt.colorbar()
        plt.show()
else:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax=ax.imshow(mat,interpolation="nearest",norm=LogNorm(vmin=0.01))
#        cax=ax.imshow(mat,interpolation="nearest")
        fig.colorbar(cax,shrink=0.5)
        fig.savefig("heatmap_ratio.png")
#        if args.debug: print "saved to out.png"
#        plt.figure().savefig("out.png")
