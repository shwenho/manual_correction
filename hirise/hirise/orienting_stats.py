#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import str
from past.utils import old_div
import re

human_accessions={
"CM000683.2": "21",
"CM000668.2": "6",
"CM000669.2": "7",
"CM000680.2": "18",
"CM000677.2": "15",
"CM000663.2": "1",
"CM000674.2": "12",
"CM000684.2": "22",
"CM000686.2": "Y",
"CM000681.2": "19",
"CM000672.2": "10",
"CM000679.2": "17",
"CM000664.2": "2",
"CM000666.2": "4",
"CM000673.2": "11",
"CM000667.2": "5",
"CM000685.2": "X",
"CM000678.2": "16",
"CM000676.2": "14",
"CM000670.2": "8",
"CM000675.2": "13",
"CM000665.2": "3",
"CM000682.2": "20",
"CM000671.2": "9"
}

def log(s):
    sys.stderr.write(  "# %s\n" %(s) )

def sta(l):
    if int(l[8])<int(l[9]):
        return 1
    else:
        return -1
        
def process_lines(b,r):
#    for i in b:
#        print i
    b.sort(key=lambda x: (x[0],float(x[11])), reverse=True )
    last=""
    for i in b:
        if not i[0]==last:
            m=re.match("gi\|(\d+)\|gb\|(.*)\|",i[1])
            id=human_accessions[m.group(2)]
#            print "\t".join([i[0],id,str(sta(i)) ])#,"\t".join(i)])
            r[i[0]] = ( id,sta(i) )
#        else:
#            print "#"+"\t".join([i[0],i[1],str(sta(i)),"\t".join(i)])
        last=i[0]
        



if __name__=="__main__":

    import argparse
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--debug'   ,default=False,action='store_true')
    parser.add_argument('-r','--oriented' )
#    parser.add_argument('-l','--lengths' )
    parser.add_argument('-b','--megablast' )
    parser.add_argument('-l','--label' )

    parser.add_argument('-p','--progress',default=False,action='store_true')




    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    blast_data={}

#    f = open(args.lengths)
#    ll={}
#    while True:
#        l = f.readline()
#        if not l: break
#        if not l[0]=="#": continue
#        c=l.strip().split()
#        ll[c[0]]=int(c[1])
#    f.close()

    f = open(args.oriented)
    strings=[]
    b=[]
    last=""
    while True:
        l = f.readline()
        if not l: break
        if not l[0:2]=="x:": continue

        c=l.strip().split()
        if not c[1]== last:
            strings.append(b)
            b=[]
        
        b.append( tuple(c[1:])  )

#        print c
        last = c[1]
#        ll[c[0]]=int(c[1])
    strings.append(b)
    f.close()

    if args.progress: log( str( "Done loading "+args.oriented ) )
    
    f=open(args.megablast)
    b=[]
    while True:
        l=f.readline()
        if not l: break
        if l[0]=="#":
            process_lines(b,blast_data)
            b=[]
        else:
            b.append(l.strip().split())
    f.close()

    if args.progress: log( str( "Done loading "+args.megablast ) )

    lengths=[]
    nper=[]
    total_tag_counts={}
    total_tag_lengths={}

    for s in strings:
        chr_counts={}
        lengths.append(sum([ int(c[4]) for c in s ] ))
        nper.append( len(s) )
        for c in s:

            sc=c[2]
#            m=re.match("(.*)_\d+",sc)
#            osc=m.group(1)
            osc=sc
            bd = blast_data.get(osc,(-1,0))

            #print "#",c,blast_data.get(osc)
            chr_counts[bd[0]]= chr_counts.get( bd[0],0)+1

        l = list( chr_counts.items() )
        
        l.sort( key=lambda x: x[1], reverse=True )

        if len(s)<3: continue
        best_chr = l[0][0]

        str_counts={}
        for c in s:

            sc=c[2]
#            m=re.match("(.*)_\d+",sc)
#            osc=m.group(1)
            osc=sc
            bd = blast_data.get(osc,(-1,0))

            if bd[0]==best_chr:
                p = (c[3],bd[1])
                str_counts[p] = str_counts.get(p,0)+1

        sl = list( str_counts.items() )
        sl.sort(key=lambda x: x[1], reverse=True)

        p1 = sl[0][0]
        if len(sl)>1:
            p2 = sl[1][0]
        else:
            p2=p1
        hh=""

        match={}
        if 0==len(sl) or ((not p1[0]==p2[0]) and (not p1[1]==p2[1])):
            match[p1]=True
            match[p2]=True
        else:
            print("No consensus stranding found")
            continue

        good=[]
        bad_strand =[]
        bad_chr=[]
        tag_counts={}
        for c in s:

            sc=c[2]
            osc=sc
#            m=re.match("(.*)_\d+",sc)
#            osc=m.group(1)

            bd = blast_data.get(osc,(-1,0))

            tag = "x"
            if bd[0]==best_chr:
                p = (c[3],bd[1])
                if match.get(p,False):
#                    good.append(c)
                    tag = "good"
                else:
#                    bad_strand.append(c)
                    tag="bad_strand"
            else:
#                bad_chr.append(c)
                tag="bad_chr"
            tag_counts[tag] = tag_counts.get(tag,0)+1
            total_tag_counts[tag] = total_tag_counts.get(tag,0)+1
            total_tag_lengths[tag] = total_tag_lengths.get(tag,0)+int(c[4])
            print("\t".join(list(c)+[str(bd),tag]))



        print("#",l,sl,tag_counts)

    lengths.sort()

    def n50(l):
        l.sort(reverse=True)
        total=sum(l)
        i=0
        s=0
        while s<0.5*total and i<len(l):
            s+=l[i]
            i+=1
        if i==len(l): return -1
        return l[i]

    def n3g(l,tt):
        l.sort(reverse=True)
#        total=sum(l)
        i=0
        s=0
        while s<tt and i < len(l):
            s+=l[i]
            i+=1
        if i==len(l): return -1
        if i> len(l): 
            print("wtf?",len(l),i)
            return -1
        return l[i]

    f = old_div(float(total_tag_lengths.get('good',0)),(0.1+sum(total_tag_lengths.values())))
    print("#", args.label,sum(lengths),sum(nper),n50(lengths),n3g(lengths,1.5e9),f,len([ x for x in nper if x>=3]),len([x for x in nper if x==1]),total_tag_counts,total_tag_lengths)
    
#        for c in good:
#            print "\t".join(list(c)+["good"])
#        for c in bad_strand:
#            print "\t".join(list(c)+["bad_strand"])
#        for c in bad_chr:
#            print "\t".join(list(c)+["bad_chr"])




