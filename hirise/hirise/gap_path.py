#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
#!/usr/bin/env python3
import sys
import argparse
import networkx as nx

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-K','--kmer',default=19,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

k=args.kmer

def path2seq(p):
     if not p: return ""
     s=p[0]
     for i in range(1,len(p)):
          if not p[i-1][1:]==p[i][:-1]:
               print("#error",i,p[i-1],p[i],len(p))
               return False
          else:
               s+=p[i][-1]
               #exit(1)
     return s

def greedy_dfs(g,n,t):
     pathlengths=[]
     path = []
     savedpath=[]
     savedpath2=[]
     q=[n]
     visited={}
     depth={}
     depth[n]=0
     lastd=0
     parent={}
     root=n
     while q:
          n=q.pop()
          visited[n]=1
          d=depth[n]
          if d<=lastd :
               path=path[:-(1+lastd-d)]
#               print [depth[i] for i in path+[n]]
          lastd=d
          path.append(n)
          w=-1
          if g.has_edge(parent.get(n),n):
               w = g[parent.get(n)][n]['weight']
          if args.debug: print("#:",n,d,w)  #,path,q

          if n==t: 
               #return depth[t]
               pathlengths.append(depth[t])
               savedpath=list(path)
               savedpath2=[n]
               while not n==root:
                    if args.debug:  print("##bt:",parent.get(n),n,g[parent.get(n)][n]['weight'])
                    n=parent.get(n,False)
                    
                    savedpath2 = [n]+savedpath2
               break
          nn=list(g.neighbors(n))
          nn.sort(key=lambda x: g[n][x]['weight'], reverse=True)
#          print [ (nnn,g[n][nnn]['weight']) for nnn in nn ]
#          appended=False
          for nnn in nn:
               if not nnn in visited and g[n][nnn]['weight']>1:
                    #if not nnn in depth: depth[nnn]=len(path)
                    depth[nnn]=len(path)
                    parent[nnn]=n
                    q.append(nnn)
#                    appended=True
#          if not appended: path.pop()
     if pathlengths==[]: pathlengths=[None]
     if args.debug: 
          print("##",len(savedpath) ,savedpath)
          print("##",len(savedpath2),savedpath2)
     seq=path2seq(savedpath2)
     #print seq, len(seq)-19
     #print savedpath
     return seq #pathlengths

n_attempted=0
n_closed=0
while True:
     l=sys.stdin.readline()
     if not l: break
     if l[0]=="#": continue
     c=l.strip().split()
#     374     Scaffold100_1.3 GTTCAAGCGATTCTCCTGC     Scaffold117987_1.5      TTTCACCATGGTGGCCAGG     5       200     TCCCGGGT
     scaffold,c1,k1,c2,k2 = c[:5]
     try:
          ming=int(c[5])
          maxg=int(c[6])
     except:
          print("error converting to int:",c)
          exit(0)
     g=nx.DiGraph()
     g.add_node(k1)
     for rd in c[7:]:
          rl=rd.find(":")
          rs=rd[:rl]
#          print rs
          for i in range(len(rs)-k-1):
               ek1= rs[i:i+k]
               ek2= rs[i+1:i+k+1]
               if not g.has_edge(ek1,ek2):
                    g.add_edge(ek1,ek2,weight=1)
               else:
                    g[ek1][ek2]['weight']+=1
#                    print ek1,ek2,g[ek1][ek2]['weight']
#     sp=nx.single_source_shortest_path_length(g,k1)
     seq=greedy_dfs(g,k1,k2)
     #if d == None: d=[None]
#          print sp
#     print "\t".join(map(str,[scaffold,c1,c2,k1,k2,sp.get(k2)]))
     n_attempted+=1
     if seq:
          n_closed+=1
          print("\t".join(map(str,[scaffold,c1,k1,c2,k2,seq])))
     else:
          print("\t".join(map(str,["#",scaffold,c1,k1,c2,k2,seq])))

#     leng=int(leng)
#     for i in range(1+leng/args.length):
#          print id,i*args.length

print("#{}\t{}\t{}".format(n_attempted,n_closed,n_closed/n_attempted))
