#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
import sys
import math
import random

one_over_sqrt_2_pi = old_div(1.0,math.sqrt(2.0*3.14159 ))

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

def fx(x,model):
    a=model['a']
    Ne=model['Ne']
    mu=model['mu']
    si=model['si']
    n=len(a)
#    print a,mu,si
    if not len(mu)==n: 
        raise Exception 
    p= Ne*( sum( [ a[i]*(old_div(one_over_sqrt_2_pi,si[i]))*math.exp(old_div(-(x-mu[i])**2.0, (2.0*si[i]*si[i]))) for i in range(n)] ) )
#    print p
#    if p < 1.0e-9: p = 1.0e-9
    return ( p ) 


def model_string(model,params):
    fparams=dict(params)
    fparams['N']=model['Ne']
    fparams['a']=model['a']
    fparams['mu']=model['mu']
    fparams['si']=model['si']
    Ne=model['Ne']
    #fparams['beta']=model['b']
    #fparams['alpha']=model['a']
    fparams['fn']="{}*(".format(Ne) + " + ".join(["{}*exp(-(x-{})**2.0/{})".format( model['a'][i]*one_over_sqrt_2_pi/model['si'][i],model['mu'][i],(model['si'][i]**2.0)*2.0 ) for i in range(n) ])+")"
    return(str(fparams))

def mutate(model,stepsize):
    model2=dict(model)
    x=random.random()
    if x<0.5:
        n=len(model['a'])
        n2=[(random.random()-0.5)*2.0*stepsize for i in range(n) ]
        n3 = [ max(0.0,n2[i] + model['a'][i]) for i in range(n) ]
        a=sum(n3)
        model2['a'] = [old_div(n3[i],a) for i in range(n)] 
    elif x<0.8:
        stepsize=10

        n=len(model['a'])
        n2=[(random.random()-0.5)*2.0*stepsize for i in range(n) ]
        n3 = [ max(0.0,n2[i] + model['si'][i]) for i in range(n) ]
        a=sum(n3)
        model2['si'] = [old_div(n3[i],a) for i in range(n)] 
    elif x<0.9:
        stepsize=10

        n=len(model['a'])
        n2=[(random.random()-0.5)*2.0*stepsize for i in range(n) ]
        n3 = [ max(0.0,n2[i] + model['mu'][i]) for i in range(n) ]
        a=sum(n3)
        model2['mu'] = [old_div(n3[i],a) for i in range(n)] 
    else:
        model['Ne'] += (random.random()-0.5)*10000.0
    return model2

    
def chisq(model,data,xmin,xmax=1000):
    chi=0
    for x,y in list(data.items()):
        if x<xmin: continue
        if x>xmax: continue

#        print "#",x,y,model
        z=fx(x,model)
#        if y<=0.0: 
#            print "#wtf y",x,y
#            raise Exception 
#        if z<=0.0: 
#            print "#wtf z",z,x,y
#            print model
#            raise Exception 
#        chi+=(math.log(y)-math.log(z))**2.0
        chi+=((y)-(z))**2.0
    return chi

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-o','--outfile')
    parser.add_argument('-N','--Ne',required=False,type=float,help="Total number of reads")
    parser.add_argument('-G','--genomesize',type=float,default=3.0e9,help="Genome size; overrides contents of -P 'G'")
    parser.add_argument('-s','--steps',required=False,type=int,default=3000)
    parser.add_argument('-C','--nexponential_components',required=False,type=int,default=10)
    parser.add_argument('-m','--min',required=False,type=float,default=100.0)
    parser.add_argument('-x','--max',required=False,type=float,default=600.0)
    parser.add_argument('-P','--param',required=False,default="{}")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()

    if args.seed != -1:
        random.seed(args.seed)





    fit_params={}
    try:
        fit_params = eval(args.param)
    except Exception as e:
        f=open(args.param)
        contents = f.read()

        try:
            fit_params=eval(contents)
        except:
            "couldn't deal with option", args.param
            #exit(0)
        f.close


    f={}

    G=fit_params.get('G',3.0e9)
    Nn=fit_params.get('Nn',0.0)
#    if args.nnoise:
#        Nn=args.nnoise
    if args.genomesize:
        G=args.genomesize

    while True:
        l=sys.stdin.readline()
        if not l:break
        c=l.strip().split()
        f[int(float(c[0]))]=float(c[1])

    nu=old_div(Nn,G)

    n=args.nexponential_components
    xmin=args.min
    incr = old_div(( args.max - xmin),n)
    mu = tuple([ (xmin + (incr*i))  for i in range(n) ]  )
    si = tuple( [60.0] *n)
    a = tuple([  old_div(1.0,n)        for i in range(n)] )

    model={ 'a':a, 'mu':mu,'si':si, 'n':n , 'Ne': args.Ne}

#    maxstep=100
    step=0
    chi = chisq(model,f,0,1000)
    while step<args.steps:
        model2 = mutate(model,0.05)
        chi2 = chisq(model2,f,0,1000)
        if chi2<chi:
            model=model2
            chi=chi2
        #pn=1.0/(1.0+model['l']/(model['nu']*G))
        print("#",step,chi,model,model_string(model,fit_params))
#        sys.stdout.flush()
        step+=1

    xs=list(f.keys())
    xs.sort()
    for x in xs:
        y=f.get(x,0)
        print(x,y,fx(x,model))
        z=x
    z=int(z)
#    for x in range(z+50000,int(5e6),50000):
#        y=f.get(x,0)
#        print x,y,fx(x,model)
        
#    pn= 1.0/(1.0+model['l']/(model['nu']*G))
#    Ne= model['nu']*G/pn
#    print "#",pn,Ne,Ne*pn/G


    if args.outfile:
        f=open(args.outfile,"wt")
        fit_params['chisq']=chi
#        f.write(model_string(fit_params))
        f.write(model_string(model,fit_params))
        f.write("\n")
        f.close()

