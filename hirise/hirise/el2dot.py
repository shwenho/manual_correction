#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-q','--minquality',default=10.0,type=float)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    G = nx.read_edgelist(sys.stdin,(('weight',float)))

    for c in nx.connected_components(g):
        print(c)
