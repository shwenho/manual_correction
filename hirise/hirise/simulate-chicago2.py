#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import str
from builtins import range
from past.utils import old_div
import random
import argparse
import networkx
import math
import re

#parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--microarray',      default=False,action="store_true"                  ,help="Use 'read set' data")
parser.add_argument('-m','--max',        default=150000,type=float                          ,help="Cutoff separation")
parser.add_argument('--powerlaw',        default=False,action="store_true"                  ,help="Use read pairs with powerlaw pair separation")
parser.add_argument('-l','-length',      default=7400,type=float                            ,help=" ")
parser.add_argument('-c','-chromosomes',required=True                                       ,help="Lengths of chromosomes")
parser.add_argument('-n','-nreads',     required=True,type=int                              ,help="Number of reads")
parser.add_argument('-N','--nexplicit',  default=0,type=int                                 ,help=" ")
parser.add_argument('-r','-readlength',  default=100,type=int                               ,help=" ")
parser.add_argument('-v','-vcfFile',     default=False                                      ,help=" ")
parser.add_argument('-g','--gff',        default=False                                      ,help=" ")
parser.add_argument('-s','-saturate',    default=-1.0,type=float                            ,help=" ")
parser.add_argument('--readsperset',     default=10,type=int                                ,help=" ")
parser.add_argument('--seed',type=int                                                       ,help=" ")
parser.add_argument('-d','-debug',       default=False,action='store_true',dest="debug"     ,help=" ")
args = parser.parse_args()

print("#",args)

if args.debug:
    print(args)
    print(args.c)

if args.seed:
    random.seed(args.seed)

gr=networkx.Graph()

def load_genes(filename):
    """read in a gff file and just keep the extents of the genes"""
    d={}
    f=open(filename)
    while True:
        l=f.readline()
        if not l:
            break
        m=re.search("ID=([^;]+)",l)
        c=l.strip().split()
        if m:
#            print m.group(1)
            s=min(int(c[3]),int(c[4]))
            t=max(int(c[3]),int(c[4]))
            d[m.group(1)]=(c[0],s,t)
#            print m.group(1),c[0],int(c[3]),int(c[4])
    f.close()
    return d

if args.gff:
    genes = load_genes(args.gff)
    print("loaded genes")

def build_gene_to_snp_index(snps,genes):
    """sort snps by position, and genes by starting point, and scan through them in step, building a list of snps overlapping each gene"""
    snp_index={}

    sorted_genes = sorted(genes , key= lambda key: (genes[key][0],genes[key][1]) )
    print("sorted genes")
    sorted_snps  = sorted(snps  , key= lambda key: (snps[key][0],snps[key][1]))
    print("sorted snps")

    gene_buffer=[]
    gene_i=0
    gene_buffer.append(sorted_genes[gene_i])
    gene_i+=1

    print(gene_buffer)
    for snp in sorted_snps:
        if args.debug:
            print("snp_index_debug",snp, snps[snp],gene_buffer,[genes[i] for i in gene_buffer],gene_i,genes[sorted_genes[gene_i]])
#        x=int(snps[snp][1])

        #throw genes out of the buffer that are no longer relevant
        while 0<len(gene_buffer) and ((genes[gene_buffer[0]][0] < snps[snp][0] ) or genes[gene_buffer[0]][2] < snps[snp][1]) :
            gene_buffer.pop(0)
        
        #don't even add genes to the buffer that haven't gone in yet, but are already old news
        while len(gene_buffer)==0 and ((genes[sorted_genes[gene_i]][0]<snps[snp][0]) or ((genes[sorted_genes[gene_i]][0]==snps[snp][0]) and (genes[sorted_genes[gene_i]][2] < snps[snp][1]))):
            gene_i+=1
            if gene_i >= len(sorted_genes):
                break

        if gene_i >= len(sorted_genes):
            break
        
        #add genes to the buffer
        while (genes[sorted_genes[gene_i]][0] == snps[snp][0] ) and genes[sorted_genes[gene_i]][1] < snps[snp][1] :
            gene_buffer.append(sorted_genes[gene_i])
            gene_i+=1
#            gene_buffer.pop(0)
        for g in gene_buffer:
            if (genes[g][0]==snps[snp][0]) and (genes[g][1] <= snps[snp][1]) and (genes[g][2]>= snps[snp][1]):
#                print "snp",snp,"overlaps gene",g, len(gene_buffer),gene_i,genes[g]
                if g not in snp_index:
                    snp_index[g]=[]
                snp_index[g].append(snp)
    return snp_index

def build_chromosome_dict(filename):
    """read a two-column text file mapping chromosomes to lengths and return a dictionary."""
    d={}
    f=open(filename)
    while True:
        l=f.readline()
        if not l:
            break
        c=l.strip().split()
        d[c[0]]=int(c[1])

    f.close()
    return d

def weighted_random_chromosome(l={},g=1,L=[]):
    """Chose a chromosome at random, with probability proportional to their lengths."""
    if not L:
        if args.debug:
            print("init chooser")
        s=0.0
        for chr in sorted(l.keys()):
            x=old_div(float(l[chr]),g)
            s+=x
            L.append([chr,s])
        if args.debug:
            print(L)

    r=random.random()
    for l in L:
        if r<l[1]:
            return l[0]

read_length=args.r
chromLen=build_chromosome_dict(args.c)
genome_length = sum(chromLen.values())
if args.debug:
    print(genome_length)
    print(chromLen)

def p_lib(d,lib,r,g,cache={}):

    if not cache:
        cache['norm']=0.0
        if lib['distro']=="powerlaw":
            cache['norm']= old_div(float(r),(g*(math.log(old_div(float(args.max),100.0)))))

        if lib['distro']=="exp":
            cache['norm']= (float(r)*r/g)*(old_div(1.0,(lib['mean']*(1.0-math.exp(old_div(-args.max,lib['mean']))))))
        

    if lib['distro']=="powerlaw":
        return ((math.log(old_div(float(d),(d-r)))))*cache['norm']

    if lib['distro']=="exp":
        return cache['norm']*math.exp(old_div(-float(d),lib['mean']))

    elif lib['distro']=="triangular":
        if d > lib['setsize']:
            return(0.0)
        return float((lib['setsize']-d))/g * ((1.0-math.exp(-r*float(lib['reads_per_set'])/lib['setsize']))**2.0)

    elif lib['distro']=="saturated":
        if d<lib['cutoff']:
            return 1.0
        else:
            return 0.0

    else:
        print("error")
        exit(0)

liblist=[]
if args.microarray:
    liblist=[{'distro':'triangular', 'setsize':args.max, 'reads_per_set':args.readsperset,'n':args.n}]
elif args.powerlaw:
    liblist=[{'distro':'powerlaw', 'n':args.n}]
else:
    liblist=[{'distro':'exp','mean':args.l,'n':args.n}]    
print(liblist,read_length,genome_length,args.s,args.max,args.r)

def p_edge(d,libraries=liblist,r=read_length,g=genome_length,cache={}):
    if d in cache:
        return(cache[d])
    
    log_p_no_edge=0.0

    for lib in libraries:

        log_p_no_edge -= lib['n'] * p_lib(d,lib,r,g)
        if log_p_no_edge < -100.0:
            cache[d]=1.0
            return cache[d]

    p=1.0-math.exp(log_p_no_edge)

    cache[d]=p
    return cache[d]

if args.debug:
    ccy=0.0
    for d in range(150000):
        p=p_edge(d)
        ccy+=p
        print("#debug p(d): ",d,p,ccy)

def parse_vcf_to_dict(filename):
    """read in a vcf file and return a dictionary where the keys are chr%s:%d, keeping only variable snps """
    if args.debug:
        print("parse vcf file",filename)
    dict_of_snps={}
    buff=[]
    f=open(filename)
    n_edges_added_probabilistically=0
    while True:
        l=f.readline()
        if not l:
            break
        c=l.strip().split()
        if c[9][:3]=="1|1" or c[9][:3]=="0|0":
            continue

        x="chr%s:%s"%(c[0],c[1])

        #maintain a buffer of all the SNPs within args.max of the current SNP, so edges can be added to them.
        if True:
            if buff and not ( buff[0][1] == c[0]):
                buff=[]
            y=float(c[1])
            while buff and ((c[0]!=buff[0][1]) or ((y-buff[0][2])>args.max)):
                buff.pop(0)
            thisx = float(c[1])
            for os in buff:
                d=int(thisx-os[2])
                if d<0:
                    print("#error:  input SNPs not sorted", d,thisx,c,buff)
                    exit(0)
                if args.s > d:
                    gr.add_edge(os[0],x)
                elif args.n>0:
                    if (random.random()<p_edge(d) ):
                        n_edges_added_probabilistically+=1
                        gr.add_edge(os[0],x)
            buff.append((x,c[0],thisx))

        c[0] = "chr"+c[0]
        c[1] = int(c[1])
        dict_of_snps[x]=tuple(c)

    f.close()
    print("#edges added: ",n_edges_added_probabilistically)
    return dict_of_snps

snps={}
if args.v:
    snps=parse_vcf_to_dict(args.v)
    if args.debug:
        print("parsed "+args.v+", kept "+str(len(list(snps.keys()))))

snp_index = build_gene_to_snp_index(snps,genes)
if args.debug:
    print("build gene to snp index with ",len(list(snp_index.keys())),"keys")

def random_genome_location():
    """return a 2-tuple with chromosome and position for base in the genome selected uniformly at random."""
    chromosome=weighted_random_chromosome(chromLen,genome_length)
    x=int(random.random()*chromLen[chromosome])
    return((chromosome,x))

if args.debug:
    print(weighted_random_chromosome(chromLen,genome_length))

n=1
lam=old_div(1.0,args.l)
for x in range(args.nexplicit):
    p1 = random_genome_location()
    insert_size=int(random.expovariate(lam))
    p2=(p1[0],p1[1]+insert_size)
    if args.debug:
        print("r."+str(n),p1,p2,insert_size,random.choice((0,1)))
    if args.v:
        linked_snps={}
        hit1=0
        for i in range(p1[1],p1[1]+args.r):
            x="%s:%d"%(p1[0],i)
            if x in snps:
#                print "r"+str(n),"f",snps[x]
                hit1+=1
#                linked_snps.append([x]+list(snps[x]))
                linked_snps[x]=1
        hit2=0
        for i in range(p2[1],p2[1]-args.r,-1):
            x="%s:%d"%(p1[0],i)
            if x in snps:
#                print "r"+str(n),"r",snps[x]
                hit2+=1
#                linked_snps.append([x]+list(snps[x]))
                linked_snps[x]=1
        ls=list(linked_snps.keys())
        if len(ls)>1:
#            print "# phasing info: ","r"+str(n),hit1,hit2,insert_size,linked_snps
            for i in range(len(ls)):
                for j in range(i):
                    gr.add_edge(ls[i],ls[j])
            
#        if hit1>0 and hit2>0:
    n+=1

if args.debug:
    print(gr.nodes())

ni=0
snp_to_haplotype={}
for c in networkx.connected_components(gr):
    ni+=1
    minx=min([ int(snps[snp][1]) for snp in c ])
    maxx=max([ int(snps[snp][1]) for snp in c ])
    print("block:","\t".join(map(str,[ni,len(c),maxx-minx,c[0],minx,maxx,snps[c[0]],c])))
#    print "#","\t".join(map(str,[n,"chr"+str(snps[c[0]][0]),len(c),minx,maxx,maxx-minx]))
    for snp in c:
        snp_to_haplotype[snp]=ni
    sg = gr.subgraph(c)
    for (i,j) in sg.edges():
        pass
#        print i,j
#        print ni," ".join(map(str,snps[i]))," ".join(map(str,snps[j]))
#        print ni," ".join(map(str,snps[j]))," ".join(map(str,snps[i]))
#    print ""

single_hap_genes=0
for g in list(genes.keys()):
    haps={}
    nsingle=1

    if g not in snp_index:
        print("nhaps:",g,0)
        single_hap_genes+=1
        continue
    for snp in snp_index[g]:
        if snp not in snp_to_haplotype:
            haps["single"+str(nsingle)]=1
            nsingle+=1
        else:
            haps[snp_to_haplotype[snp]]=1
    print("nhaps:",g,len(list(haps.keys())),genes[g],list(haps.keys()))
    if len(list(haps.keys()))<=1:
        single_hap_genes+=1

f=old_div(float(single_hap_genes),len(list(genes.keys())))
print("#single_hap_genes:",single_hap_genes,f,args)


