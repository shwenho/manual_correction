#!/usr/bin/env python3

import argparse
import sys
import re
import random
import gzip

from fasta_fastq_parser import read_fasta
from barebones_fastq import read_fastq

lower_re = re.compile("[a-z]+")

def gzipFile(filename):
    """A convenience function used to open a normal or gzipped file.

    This function defines a custom type for the argparse.add_argument() function,
    which serves the same purpose as 'type=argparse.FileType('r')' except with
    the added functionality of using the gzip module to open gzipped files for reading.
    Gzipped files are detected by the suffix ".gz" at the end of the filename.
    """
    return gzip.open(filename, 'rt') if filename.endswith(".gz") else open(filename,'r')

def parse_args():
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--input', '-i', action = 'store',
        dest='input', 
        help=("Specifies an input FASTA or FASTQ file "),required=True,type=gzipFile)
    argparser.add_argument('--output', '-o', action = 'store',
        dest='output', 
        help=("Specifies a output FASTQ file"),required=True,type=argparse.FileType('w'))
    argparser.add_argument('--phred64',action='store_true',
        default=False,help=("Read quality scores in Phred+64 instead of "
        "Phred+33."))
    options = argparser.parse_args()
    return options


def get_qual_string(scores,phred):
    qual = ""
    for score in scores: qual += chr(score+phred)
    return qual


def main(args):
    opt = parse_args()
    if opt.phred64: phred = 64
    else: phred = 33
    qual_char = chr(30+phred) 
    if opt.input.name.endswith(".fastq") or opt.input.name.endswith(".fq"):
        reads = read_fastq(opt.input)
    else:
        reads = read_fasta(opt.input)
    for read in reads:
        qual_string = qual_char * len(read.sequence)
        read_name = read.identifier +" " + read.comment
        print("@%s\n%s\n+\n%s" % 
            (read_name,read.sequence,qual_string),file=opt.output)
    opt.input.close()
    opt.output.close()

if __name__ == "__main__":
    sys.exit(main(sys.argv))
