#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
import sys
import re

minl = int(sys.argv[1])

while True:
    l=sys.stdin.readline()
    if not l: break
    m=re.match("'(.*)'=='([+-])(.*)' \((\d+) (\d+) (\d+) (\d+)\) (\d+)",l)
    if m:
        chromosome = m.group(1)
        scaffold = m.group(3)
#        strand = m.group(3)=="+"?1:-1
        start1 = int(m.group(4))
        end1 = int(m.group(6))
        start2= int(m.group(5))
        end2 = int(m.group(7))
        score=int(m.group(8))
        if m.group(2)=="-": 
            strand = -1
            d= -(start1-end2)
        else:
            strand = 1
            d= start1-start2
        if (end1-start1) > minl:
            print("\t".join(map(str,[chromosome,scaffold,strand,start1,end1,start2,end2,score,end1-start1,strand*(end2-start2),d])))
