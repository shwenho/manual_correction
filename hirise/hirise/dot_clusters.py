#!/usr/bin/env python3
from __future__ import print_function
from builtins import range
#!/usr/bin/env python3
import pysam

import networkx as nx
import re
from hirise.bamtags import BamTags

debug=False

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True,action="append",default=[])
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('-q','--mapq',type=int,default=20)
    parser.add_argument('-p','--padding',type=int,default=0)
    parser.add_argument('-L','--length',type=int,default=1000000)
    parser.add_argument('-s','--minsize',type=int,default=5)
    parser.add_argument('-c','--cutoffd',type=int,default=2000)
    parser.add_argument('-m','--minsep',type=int,default=10000)
    parser.add_argument('-x','--maxsep',type=int,default=500000)
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-r','--region',default=False)
    parser.add_argument('-g','--gff',default=False)
    parser.add_argument('-v','--vcf',default=False)

    args = parser.parse_args()

    cutoffd=args.cutoffd
    min_sep=args.minsep
    max_sep=args.maxsep
    mapq = args.mapq

    if args.debug:
        print(args)


    region=False
    if args.region:
        c,r = args.region.split(":")
        sp = r.split("-")
        if len(sp)==2:
            s,t = sp
        else:
            s=sp[0]
            t=int(s)+args.length
#        region = (c,(int(s),int(t)))
        region = (c,(max(1,int(s)-args.padding),int(t)+args.padding))
        args.region = "{}:{}-{}".format( region[0],region[1][0],region[1][1] )
        if args.debug: print("#",region)

    t2snp = {}
    hist={}

    chro,xy = args.region.split(":")

    points=[]
    for bamfile in args.bamfile:

        sam=pysam.Samfile(bamfile,"rb")

#        if args.debug:
#            print sam.header

#        h=sam.header
#        seqs=h['SQ']

        n=0

        for aln in sam.fetch(region=args.region,until_eof=True):
            if aln.is_duplicate : continue
            if not aln.tid == aln.rnext: continue
            if not aln.pos < aln.pnext: continue
            if not aln.mapq    > mapq: continue
            if not BamTags.mate_mapq(aln)>mapq: continue
            if not (region[1][0]<aln.pnext  and aln.pnext<region[1][1]): continue
            p=(aln.pos,aln.pnext,aln.tid)
            d=aln.pnext-aln.pos
            if (min_sep<=d and d<= max_sep):
                points.append(p)
#                print aln.pos,aln.pnext,phase,aln.mapq,":xy:"
#                print aln.pnext,aln.pos,phase,BamTags.mate_mapq(aln),":xy:"


        sam.close()
    points.sort()
    g=nx.Graph()
    j=0

    def dist(x,y):
        return max(abs(x[0]-y[0]),abs(x[1]-y[1]))

    for i in range(len(points)):
        a,b,c=points[i]
        while (not c == points[j][2]) or a-points[j][0] > cutoffd: j+=1
        for k in range(j,i):
            if dist(points[i],points[k])<cutoffd:
                g.add_edge(i,k)
    
    print("#",g.number_of_nodes())
    for c in nx.connected_components(g):
        if len(c)<args.minsize: break
        start = min([points[i][0] for i in c])
        end   = max([points[i][1] for i in c])
        print(len(c),start,end,end-start,min([ points[i][1]-points[i][0] for i in c ]),max([ points[i][1]-points[i][0] for i in c ]), [ points[i] for i in c ])






