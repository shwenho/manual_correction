#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys

#xy: 1 Scaffold66386_1 0 1 2 Scaffold88134_1 Scaffold69949_34 Scaffold111941_1 [46316, 6357, 28908, 38939] ['8', 'X', '8', '8'] (1, 1, 1) 20 505828 [(4408, 48914), (4406, 48914), (4404, 48887), (4402, 48914), (50059, 78746), (47216, 75575), (44482, 74937), (48774, 72767), (46600, 67828), (36131, 67743), (43644, 64756), (48326, 64560), (46532, 57706), (31028, 56446), (48492, 56396), (45310, 56190), (35511, 55143), (47461, 55143), (33422, 54284), (30970, 53557)]

ys={}

while True:
    l=sys.stdin.readline()
    if not l: break
    if not l[:3]=="xy:": continue
    c = l.strip().split()
#    print " ".join(c[1:])
    n=c[2]
    id=int(c[1])
    lens = eval(" ".join(c[9:13]))
    chrs = eval(" ".join(c[13:17]))
    strs = eval(" ".join(c[17:20]))
    nl = int(c[20])
    sl = int(c[21])
    xys = eval(" ".join(c[22:]))
#    print id,n,chrs,lens,strs,xys
    ys[id] = ys.get(id,[]) + [ {'n':n,'chrs':chrs,'xy': xys} ]


for id in list(ys.keys()):
#    print id, len(ys[id]), ys[id]
    for at in ys[id]:
        chrs = at['chrs']
        label='bad'
        if chrs[0]==chrs[1] and chrs[0] == chrs[2]: label='good'
        for x,y in at['xy']:
            print(id,y-x,label,x,y)
