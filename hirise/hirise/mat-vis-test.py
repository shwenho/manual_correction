#!/usr/bin/env python3
from builtins import range
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import random

mat = np.zeros((30,30))
for i in range(30):
    for j in range(30):
        mat[i,j]=random.random()

plt.pcolor(mat,norm=LogNorm(vmin=0.001))
plt.colorbar()
plt.show()
