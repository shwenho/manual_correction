#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys

inp=sys.stdin

if len(sys.argv)>1:
#    print sys.argv[1]
    inp = open(sys.argv[1])

def printfasta(n,os):
    print(">"+n)
    for i in range(old_div(len(os),100)):
        print(os[i*100:(i+1)*100])
    print(os[((old_div(len(os),100)))*100:])


sname=0
leng=0
s=""
while True:
    l = inp.readline()
    if not l: break
    if l[0]==">":
        if sname and len(s)<1000: 
            printfasta(sname,s)
#            print ">%s" % (sname)  # \t%d" % (sname,leng)
            
#            sys.stdout.flush()
        c=l.strip().split()
        sname = c[0][1:]
        leng=0
        s=""
    else:
        leng+=len(l.strip())
        s+=l.strip()
print("%s\t%d" % (sname,leng))



