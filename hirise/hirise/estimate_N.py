#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
import sys
import random
import hirise.chicago_edge_scores as ces

#from random import random
from bisect import bisect

cum_weights=[]
total_weight=0.0

def weighted_choice(l,weighting):
    global cum_weights
    global total_weight
    if len(cum_weights)==0:
        total = 0
        for i in l:
            total += weighting[i]
            cum_weights.append(total)
        total_weight=total
    x = random.random() * total_weight
    i = bisect(cum_weights, x)
    return l[i]

def main():

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--links')
    parser.add_argument('-P','--param')
    parser.add_argument('-o','--outfile')
    parser.add_argument('-n','--ncontigs',type=int,required=True)
    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()
    if args.seed != -1 :
      random.seed(args.seed)

    #Ces.debug=args.debug
    if args.debug:
        args.progress=True

    print("# ncontigs:",args.ncontigs)

#    print args.param
    fit_params={}
    try:
        fit_params = eval(args.param)
    except Exception as e:
#        print e
#    if not type(fit_params)==type({}):
#        print "#treating",args.param,"as a filename"
        f=open(args.param)
        contents = f.read()
#        print "contents=",contents,eval(contents)
        try:
            fit_params=eval(contents)
        except:
            "couldn't deal with option", args.param
            #exit(0)
        f.close

#    print "params:", fit_params

    fit_params["N"]=1.0

    alpha = fit_params['alpha']
    beta=fit_params['beta']
    pn=fit_params['pn']
    G=fit_params['G']
    meanL = sum([old_div(alpha[i],beta[i]) for i in range(len(alpha))])
    print("#meanL",meanL)
    ces.set_exp_insert_size_dist_fit_params(fit_params)

    G=fit_params["G"]
    pn=fit_params["pn"]

    def hit_p(l1,l2,G,pn,Nc):
        return ((old_div(1.0,Nc))*ces.s_approx_expsum(l1,l2,G,0.0,pn)/G + (1.0-old_div(1.0,Nc))*pn*(old_div(l1,G))*(old_div(l2,G)))
# but this is effectively the same as, since there are a lot of contigs.
#        return ( pn*(l1/G)*(l2/G))

#    print hit_p(1000,1000,500e6,0.5,50000)

    links={}
    ll={}
    my_contigs={}
    f=open(args.links)
    while True:
        l=f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        c1,c2,l1,l2=c[0],c[1],int(c[2]),int(c[3])
        my_contigs[c1]=1
        n_links = int(c[4])
        links[c1,c2]=n_links
        ll[c1]=l1
        ll[c2]=l2
    f.close()
    contigs = list(my_contigs.keys())

    rate=0.0
    totaln=0.0
    n_pairs=0
    while n_pairs < 50000:
        n_pairs+=1
        c1 = weighted_choice(contigs,ll)
        c2 = weighted_choice(contigs,ll)
        rated = hit_p(ll[c1],ll[c2],G,pn,args.ncontigs)
        totalnd=0.0+max(links.get((c1,c2),0), links.get((c2,c1),0))
        totaln+=totalnd
        rate+=rated
    print("#",c1,c2,ll[c1],ll[c2],rated,totalnd,rate,totaln,old_div(totaln,rate),meanL*(old_div(totaln,rate))*(1.0-pn)/G)
    print(old_div(totaln,rate))

    fit_params['N'] =  old_div(totaln,rate)
    print(fit_params)

    if args.outfile:
        f=open(args.outfile,"wt")
        f.write(str(fit_params))
        f.write("\n")
        f.close()

if __name__=="__main__":
    main()
