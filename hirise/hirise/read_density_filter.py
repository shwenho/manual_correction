#!/usr/bin/env python3
from __future__ import print_function


import pysam

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-m','--minqual',default=0,type=int)
    parser.add_argument('-s','--step',default=500)
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-w','--binwidth',default=10000,type=int)
    parser.add_argument('-l','--low',  default=10,type=int)
    parser.add_argument('-u','--upper',default=100,type=int)

    nodes={}

    args = parser.parse_args()

    sam=pysam.Samfile(args.bamfile)

    h=sam.header
    seqs=h['SQ']

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]

    state = 'out'
    start=-1
    queue = []
    next_report = (0, args.step)

    print("#",snam[0])
    def report(x):
        global state
        global start
        while queue and (queue[0]< (x[0],x[1]-args.binwidth) ):
            queue.pop(0)
        ql = len(queue)
        
        if not (args.low <= ql and ql <= args.upper) :
            if state=='out':
                state = 'in'
#                print x[0],x[1],len(queue),'enter'
                start=x
            else:
                if args.debug: print(x[0],x[1],len(queue),'<<<<<')
        else:
            if state=='in':
                state='out'
#                print x[0],x[1],len(queue),'exit'
                print(x[0],start[1]-args.step,x[1]-args.step)
            else:
                if args.debug: print(x[0],x[1],len(queue),'......')
                

    for a in sam.fetch(until_eof=True):
        x = (a.tid,a.pos)
#        if args.debug: print "mapq:", a.tid, a.pos, a.mapq, args.minqual, args.minqual <= a.mapq
        while x > next_report:
            report(next_report)
            next_report = (next_report[0],next_report[1]+args.step)
            if next_report[1] > slen[next_report[0]]:
                if state=='in':
                    print(start[0],start[1]-args.step,slen[next_report[0]])
                    state='out'
                print("#",snam[next_report[0]+1])
                next_report = ( next_report[0]+1,args.step)

        if args.minqual <= a.mapq:
            queue.append(x)
        
        if args.head and a.pos > args.head:
            exit(0)


