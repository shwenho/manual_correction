#!/usr/bin/env python3

import sys

def build_chromosome_dict(filename):
    """read a two-column text file mapping chromosomes to lengths and return a dictionary."""
    d={}
    f=open(filename)
    while True:
        l=f.readline()
        if not l:
            break
        c=l.strip().split()
        d[c[0]]=int(c[1])

    f.close()
    return d
