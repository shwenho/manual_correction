#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
from builtins import object
#!/usr/bin/env python3
import glob
import sys
import bisect
import re
import networkx as nx
import numpy as np
import hirise.chicago_edge_scores as ces
import math
import random
import itertools
import recombinings2


debug=False

def tab(x):
    return "\t".join(map(str,x))


def mcmc_optimize(bps,dataset,r1,active_breaks,stepSize=2000.0,nreps_max=500):
#    nreps_max=500
    nreps=0

    L=dataset.llr(r1)

    bestL=L
    best_breaks = list(map(tuple,bps.breaks))

    if debug: print("mcmc",bestL,best_breaks)

    while nreps<nreps_max:
        nreps+=1
        i=random.choice(active_breaks)
        dx=int(2.0*stepSize*random.random() - stepSize)
        oldx = bps.breaks[i][1]
        bps.move_breakpoint(i, dx)
        dataset.update_tilemap()# (bps.breaks[i][0],oldx),bps.breaks[i] )
        
        if debug: print("ready to calc llr")
        sys.stdout.flush()
        L1=dataset.llr(r1)
        if debug: print("done calc llr")
        sys.stdout.flush()

        if L1>bestL:
            bestL=L1
            best_breaks = list(map(tuple,bps.breaks))
            if debug: print("bestL",bestL,best_breaks)

        qr=random.random()
        if debug: print("mcmc",L,i,dx,L1,L1>L,qr,math.exp(L1-L),qr<math.exp(L1-L))
        if not ((L1>L) or (qr<math.exp(L1-L)) ):
            #revert
            #bps.move_breakpoint(i, dx)
            newx=bps.breaks[i][1]
            bps.breaks[i][1]=oldx
            dataset.update_tilemap()# (bps.breaks[i][0],oldx),(bps.breaks[i][0],newx) )
        
        else:
            L=L1
        if debug: print("llr:",L,i,dx,bps.breaks)
    return bestL,best_breaks


def merge_intervals(l):

    merged_intervals=[]

    edges=[]
    for a,b in l:
        edges.append((min(a,b),1))
        edges.append((max(a,b),-1))

    edges.sort()
    rs=0
    state=0
    for i in range(len(edges)):
        rs+=edges[i][1]
        x=edges[i][0]
        if state==0 and rs>0:
            xstart=x
            state=1
        elif state==1 and rs<=0:
            xend=x
            state=0
            merged_intervals.append((xstart,xend))

    return merged_intervals


class SequenceSet(object):
    def __init__(self,names,lengthdict,loci):
        self.sequences = list(names)
        self.sequences.sort()
        self.loci = dict(loci)  # maps scaffold name to min,max pair list
        self.length = dict([ (n,lengthdict[n]) for n in names ])
        self.locus_list = []
        self.locus2id={}
        self.has_end_in_play={}
        for l in loci:
            for lll in loci[l]:
                self.locus_list.append( (l,lll) )
                self.locus2id[(l,lll)]="{}:{}-{}".format(l,lll[0],lll[1])
                if lll[0]<10000 or self.length[l]-lll[1] <10000:
                    self.has_end_in_play[l]=True
        print("locus list:",self.locus_list)
        print("locus2id:",self.locus2id)
        


class BreakpointSet(object):

    def __init__(self,sequenceset,weakpoints=-1):  # weakpoint list in the form, for eg: [('scaffold_77', (297577, 298585)), ('scaffold_82', (9672216, 9673314)), ('scaffold_82', (9676647, 9677653))]
        self.sequences=sequenceset

        if weakpoints==-1:
            weakpoints=sequenceset.locus_list

        self.break2scaffold={}

        self.break_bound_min = []
        self.break_bound_max = []

        self.breaks=[]

        for s,(minX,maxX) in weakpoints:
            slen = self.sequences.length[s]
            if maxX<100 and minX<100: continue
            if minX>slen-100 and maxX>slen-100: continue
            minX = min(minX,maxX)
            maxX = max(minX,maxX)
            minX = max(minX,100)
            maxX = min(maxX,slen-100)
            self.breaks.append( [s,  int(0.5*sum((minX,maxX))) ] )
            self.break_bound_min.append(minX)
            self.break_bound_max.append(maxX)
            self.break2scaffold[ len(self.breaks)-1 ] = s


    def __oldinit__(self,sequenceset,whichtobreak=False):
        self.sequences=sequenceset
        if not whichtobreak:
            whichtobreak = list(sequenceset.sequences)

        self.break2scaffold={}

        self.break_bound_min = []
        self.break_bound_max = []

        self.breaks=[]
        for s in whichtobreak:
            for l in sequenceset.loci[s]:
                self.breaks.append( [s, int(0.5*sum(l) ) ] )
                self.break_bound_min.append(min(l))
                self.break_bound_max.append(max(l))
                self.break2scaffold[ len(self.breaks)-1 ] = s

    def move_breakpoint(self,b,dx):
        if debug: print("move breakpoint",b,"dx:",dx)
        if b<0 or b >= len(self.breaks): raise Exception
        scaffold = self.breaks[b][0]
        x=self.breaks[b][1]
        if dx>0:
            if b+1<len(self.breaks) and self.breaks[b+1][0]==scaffold:
                if x+dx>=self.breaks[b+1][1]:
                    dx = self.breaks[b+1][1] - x - 1
            elif x+dx>=self.sequences.length[scaffold]:
                if debug: print("slen:",x+dx,self.sequences.length[scaffold])
                dx = self.sequences.length[scaffold] - x -1
        elif dx<0:
            if b-1<len(self.breaks) and self.breaks[b-1][0]==scaffold:
                if x+dx<=self.breaks[b-1][1]:
                    dx = self.breaks[b-1][1] - x + 1
            elif x+dx<=0:
                dx = - x +1
        if debug: print(dx)
        self.breaks[b][1]+=dx
        if self.breaks[b][1]<self.break_bound_min[b]: self.breaks[b][1]=self.break_bound_min[b]
        if self.breaks[b][1]>self.break_bound_max[b]: self.breaks[b][1]=self.break_bound_max[b]
   
class Segments(object):

    def __init__(self,sequenceset,breakpoints):
        breaks = []
        self.breaks = breaks
        self.sequenceset = sequenceset
        self.breakpointset = breakpoints
        ends=[]
        self.ends = ends

        for s in self.sequenceset.sequences:
            breaks.append( [s,1])  
            breaks.append( [s,self.sequenceset.length[s]])  

        for b in self.breakpointset.breaks:
            breaks.append(b)
            
        self.breaks.sort()

        self.segments=[]
        for i in range(len(breaks)-1):
            if breaks[i][0]==breaks[i+1][0]:
                self.segments.append((i,i+1))

        self.intervals=[]
        for s,e in self.segments:
            i=len(ends)
            ends.append( self.breaks[s] )
            ends.append( self.breaks[e] )
            self.intervals.append(( i,i+1 ))

        self.original_joins=[]
        for i in range(len(ends)):
            for j in range(i+1,len(ends)):
                if self.ends[i] == self.ends[j]:
                    self.original_joins.append((i,j))

        self.tiles=[]
        for i in range(len(self.intervals)):
            for j in range(i,len(self.intervals)):
                self.tiles.append((self.intervals[i],self.intervals[j]))
        self.nends = len(self.ends)

    def find_tile(self,point):
        (s1,x),(s2,y)=point
        for t in self.tiles:
            (a,b),(c,d) =t
            A=self.ends[a]
            B=self.ends[b]
            C=self.ends[c]
            D=self.ends[d]
            #print (s1,x),(s2,y), t,A,B,C,D,"?"
            if s1==A[0] and s2==C[0]:
                if A[1]<=x and x<=B[1] and C[1]<=y and y<=D[1]:
                    return t
        return False
            #if s1==


class Dataset(object):
    def __init__(self,segments,score_fn):
        self.data=[]
        self.datapoints={}
        self.segments = segments
        self.xindex=[]
        self.yindex=[]
        self.point2tile={}
        self.point2d   ={}
        self.indices_valid=False
        self.get_score = score_fn
#        self.breakpoint_positions={}
        self.breakpoints_cache = tuple([ tuple(i) for i in self.segments.breakpointset.breaks ])

    def add_datapoint(self,d):

        a,b = d
        if not a<=b:
            c=a
            a=b
            b=c
            #        t=tuple(d)
        t=((a[0],a[1]+1),(b[0],b[1]+1))
        if t in self.datapoints: return
        self.datapoints[t]=1
        self.data.append(t)
        self.point2tile[t]=self.segments.find_tile(t)
        self.point2d[t]   = {}
        self.indices_valid=False

    def rebuild_tilemap(self):
        for d in self.data:
            self.point2tile[d]=self.segments.find_tile(d)
        self.breakpoints_cache = tuple([ tuple(i) for i in self.segments.breakpointset.breaks ])

    def update_indices(self):
        self.xindex = [ (self.data[i][0][0],self.data[i][0][1], i ) for i in range(len(self.data)) ]
        self.xindex.sort()
        self.yindex = [ (self.data[i][1][0],self.data[i][1][1], i ) for i in range(len(self.data)) ]
        self.yindex.sort()
        self.indices_valid = True

    def update_tilemap(self):
        if not self.indices_valid: self.update_indices()
        nmoved=0
        
        
        for breakpoint_i in range(len(self.segments.breakpointset.breaks)):
            if not self.breakpoints_cache[breakpoint_i][1] == self.segments.breakpointset.breaks[breakpoint_i][1]:
                if debug: print("update tilemap, breakpoint:",breakpoint_i,self.breakpoints_cache[breakpoint_i],self.segments.breakpointset.breaks[breakpoint_i][1])
                old_breakpoint = self.breakpoints_cache[breakpoint_i]
                new_breakpoint = self.segments.breakpointset.breaks[breakpoint_i]

                start = min(tuple(old_breakpoint),tuple(new_breakpoint))
                end   = max(tuple(old_breakpoint),tuple(new_breakpoint))

                if debug: print("move breakpoint:",start,end)
                i = bisect.bisect_left( self.xindex, start )
                j = bisect.bisect_left( self.xindex, end   )
                if debug: print(i,j,len(self.xindex))

                while i<len(self.xindex) and i>0 and self.xindex[i][0] == start[0] and self.xindex[i][1]>=start[1]: i-=1
                while j<len(self.xindex) and j>0 and self.xindex[j][0] == end[0] and self.xindex[j][1]<=end[1]: j+=1
                #print self.xindex[i],self.xindex[j]

                if debug: print(i,j)
                for k in range(i,j):
                    d=self.data[self.xindex[k][2]]
                    #            print d,self.point2tile[ d ] ,self.segments.find_tile( d )
                    new_tile = self.segments.find_tile( d )
                    if not new_tile == self.point2tile[d]: nmoved+=1
                    self.point2tile[ d ] = new_tile

                i = bisect.bisect_left( self.yindex, start )
                j = bisect.bisect_left( self.yindex, end   )
                while i<len(self.xindex) and i>0 and self.yindex[i][0] == start[0] and self.yindex[i][1]>=start[1]: i-=1
                while j<len(self.yindex) and j>0 and self.yindex[j][0] == end[0] and self.yindex[j][1]<=end[1]: j+=1

                if debug: print(i,j)
                for k in range(i,j):
                    d=self.data[self.yindex[k][2]]
        #            print d,self.point2tile[ d ] ,self.segments.find_tile( d )
        #            self.point2tile[ d ] = self.segments.find_tile( d )

                    new_tile = self.segments.find_tile( d )
                    if not new_tile == self.point2tile[d]: nmoved+=1
                    self.point2tile[ d ] = new_tile

        if debug: print("nmoved",nmoved)
        self.breakpoints_cache = tuple([ tuple(i) for i in self.segments.breakpointset.breaks ])
        if debug: print("breakpoint_cache",self.breakpoints_cache)
        sys.stdout.flush()

    def breakpoint_likelihood_scan(self,breakpoints,ii,rearrangement,L0,deltaL=30.0):
        scaffold,oldx=breakpoints.breaks[ii]
        length = self.segments.sequenceset.length

        if not self.indices_valid: self.update_indices()

        minx = breakpoints.break_bound_min[ii]
        maxx = breakpoints.break_bound_max[ii]
        
        i = bisect.bisect_left( self.xindex, (scaffold,minx) )
        while i<len(self.xindex)   and i>0 and self.xindex[i-1][0] == scaffold and self.xindex[i][1]>=minx: i-=1

        j = bisect.bisect_left( self.xindex, (scaffold,maxx) )
        while j<len(self.xindex)-1 and j>0 and self.xindex[j+1][0] == scaffold and self.xindex[j][1]<=maxx: j+=1

        nmoved=0
        if debug: print(i,j,len(self.xindex))

        curve=[]
        edges=[]

        state=1
        for k in range(i,j):
            if debug: print(k,self.xindex[k])
            x=self.xindex[k][1]
            breakpoints.breaks[ii][1]=x
            self.update_tilemap()# (scaffold,oldx),(scaffold,x))

            llr = self.llr(rearrangement)
            
            if state==1 and llr <(L0-deltaL): edges.append((x+500,-1))
            if state==0 and llr>=(L0-deltaL): edges.append((x-500, 1))

            if llr<L0-deltaL:
                state=0
            if llr>=L0-deltaL:
                state=1
#            curve.append((x,llr))
            if debug: print("bpscan",ii,scaffold,x,llr)
            sys.stdout.flush()
            oldx = x

        x0=self.xindex[i][1]
        edges.sort()

        rs=0
        state=0        
        lastx= x0
        range_start=lastx
        candidate_regions=[]
        for e in edges:
            x=e[0]
            if rs==0 and rs+e[1] <0 and state==1:  #end of poorly-supported region
                range_end=e[0]
                state=0
                candidate_regions.append((range_start,x))
            if rs< 0 and rs+e[1]>=0 and state==0:  #start of poorly-supported region
                range_start=e[0]
                state=1
            rs+=e[1]
            lastx=x
#        if rs==0:
#            candidate_regions.append((range_start,x))
            

        if debug: print("candidate_regions:",candidate_regions)
        return candidate_regions

    def breakpoint_likelihood_scan2(self,scaffold,minx,maxx,deltaL=30.0):

        length = self.segments.sequenceset.length

        #scaffold,oldx=breakpoints.breaks[ii]
        if not self.indices_valid: self.update_indices()

        #minx = breakpoints.break_bound_min[ii]
        #maxx = breakpoints.break_bound_max[ii]

        curve=[]
        edges=[]
        data_point_seen={}
        
        i = bisect.bisect_left( self.xindex, (scaffold,minx) )
        while i<len(self.xindex)   and i>0 and self.xindex[i-1][0] == scaffold and self.xindex[i][1]>=minx: i-=1

        j = bisect.bisect_left( self.xindex, (scaffold,maxx) )
        while j<len(self.xindex)-1 and j>0 and self.xindex[j+1][0] == scaffold and self.xindex[j][1]<=maxx: j+=1

#        nmoved=0
        if debug: print("index x:",i,j,len(self.xindex))


        for k in range(i,j):
            if self.xindex[k][2] in data_point_seen: continue
            data_point_seen[self.xindex[k][2]]=1
            d=self.data[ self.xindex[k][2] ]
            (s1,x1),(s2,x2) = d
            if  s1==s2: 
                dx=abs(x2-x1)
                dllr=(self.get_score(dx) - self.get_score(1e9))
                edges.append( (x1,dllr) ) 
                edges.append( (x2,-dllr) ) 
        if debug: print("edges: ",len(edges))
        i = bisect.bisect_left( self.yindex, (scaffold,minx) )
        while i<len(self.yindex)   and i>0 and self.yindex[i-1][0] == scaffold and self.yindex[i][1]>=minx: i-=1

        j = bisect.bisect_left( self.yindex, (scaffold,maxx) )
        while j<len(self.yindex)-1 and j>0 and self.yindex[j+1][0] == scaffold and self.yindex[j][1]<=maxx: j+=1

#        nmoved=0
        if debug: print("index y:",i,j,len(self.yindex))


        for k in range(i,j):
            if self.yindex[k][2] in data_point_seen: continue
            data_point_seen[self.yindex[k][2]]=1
            d=self.data[ self.yindex[k][2] ]
            (s1,x1),(s2,x2) = d
            if debug: print(d)
            if  s1==s2: 
                dx=abs(x2-x1)
                dllr=(self.get_score(dx) - self.get_score(1e9))
                edges.append( (x1,dllr) ) 
                edges.append( (x2,-dllr) ) 
        if debug: print("edges: ",len(edges))
            
        if len(edges)==0: 
            candidate_regions=[(minx,maxx)]
            if debug: print("candidate_regions:",candidate_regions)
            return candidate_regions
        edges.sort()


        rs=0
        state=0        
        lastx= edges[0][0]-1
        range_start=lastx
        candidate_regions=[]
        for e in edges:
            x=e[0]
            if rs<deltaL and rs+e[1] >=deltaL and state==1:  #end of poorly-supported region
                range_end=e[0]
                state=0
                candidate_regions.append((range_start,x))
            if rs>=deltaL and rs+e[1]<deltaL and state==0:  #start of poorly-supported region
                range_start=e[0]
                state=1
            rs+=e[1]
#            print "newll curve:",scaffold,x,rs
            lastx=x
#        if rs==0:
#            candidate_regions.append((range_start,x))
            
        candidate_regions = merge_intervals( [ (max(0,x-500),min(length[scaffold],y+500)) for x,y in candidate_regions ] )
        

        if debug: print("candidate_regions:",candidate_regions)
        return candidate_regions


    def llr(self,rearrangement):
        llr=0.0
        tag=int(random.random()*10000)
        tile_totals={}
        for d in self.data:
            tile = self.point2tile[d]
            if not tile:
                print("wtf? no tile for ",d)
                for t in self.segments.tiles:
                    #pass
                    print(t,self.segments.ends[t[0][0]],self.segments.ends[t[0][1]],self.segments.ends[t[1][0]],self.segments.ends[t[1][1]])
            else:
                (s1,x),(s2,y)=d
    #            dx = abs(np.dot([[1,-1,0]], rearrangement.tiletransforms[tile]([[x],[y],[1]]) )[0][0]) 
                dx = rearrangement.tiletransforms[tile](x,y) 
                dllr=self.get_score(dx)
                #print "dllr",tag,d,tile,dx,dllr
                tile_totals[tile] = tile_totals.get(tile,0.0)+dllr
                llr += dllr

        if debug: print("tiletotals:",tile_totals)
        sys.stdout.flush()
        return(llr)

#        print "d2",s1,s2,x,y,tile,scaffold_offset[s1]+x-scaffold_origin[s1], scaffold_offset[s2]+y-scaffold_origin[s2],dd[0],dd[1],r0.tiletransforms[tile],r1.tiletransforms[tile]
def has_a_cycle(end_joins,segments):
        g = nx.Graph()

        for i in range(segments.nends):
#            print end
            g.add_node(i)
            
        for a,b in segments.intervals:
            g.add_edge(a,b)
    
        for p in end_joins:
            if len(p)>1:
                a,b=p
                g.add_edge(a,b)
                
                if nx.cycle_basis(g): 
                    #print "bad scenario:  makes a circular fragment"
                    return True
        return False

def coord_transf(xx,yy,cAmC,cBmD,segments,e1,e2,ivs):
    if len(ivs)>0:
        return abs(cAmC*xx + cBmD*yy - cAmC*segments.ends[e1][1] - cBmD*segments.ends[e2][1] + sum( [ abs( segments.ends[aa][1] - segments.ends[bb][1] ) for aa,bb in ivs  ] ) )
    else:
        return abs(cAmC*xx + cBmD*yy - cAmC*segments.ends[e1][1] - cBmD*segments.ends[e2][1]  )

from functools import partial

class Rearrangement(object):
    def __init__(self,segments,end_joins):
        self.segmentset=segments
        self.end_joins = end_joins
        length={}
        g = nx.Graph()
        self.new_contig={}
        self.new_coordinate={}

        end_count_sanity={}
        for r in end_joins:
            for rr in r:
                end_count_sanity[rr]=end_count_sanity.get(rr,0)+1
        for rr in list(end_count_sanity.keys()):
            if end_count_sanity[rr]>1:
                print("bad rearrangemnt with end re-use:",end_joins,end_count_sanity)
                exit(0)

        for i in range(segments.nends):
#            print end
            g.add_node(i)

        for a,b in segments.intervals:
            g.add_edge(a,b)
            length[a,b] = 0.1+abs(segments.ends[a][1]-segments.ends[b][1])
            length[b,a]= length[a,b]
        #print length

        for p in end_joins:
            if len(p)>1:
                a,b=p
                g.add_edge(a,b)
                length[a,b]=1.0
                length[b,a]=1.0
                if nx.cycle_basis(g): 
                    print("bad scenario:  makes a circular fragment",end_joins)
                    raise Exception 
        
        i=1

        intervening_segments={}
        if debug: 
            for e in g.edges(): print("edge",e)
        for c in nx.connected_components(g):
            
            c.sort()
            #print [segments.ends[j] for j in c]
            k=0
            while not g.degree(c[k])==1: k+=1
            for cc in c:
                self.new_contig[cc]=i
            self.new_coordinate[c[k]]=0.0
            if debug: print("traverse:",k,c[k],c,[ g.degree(kk) for kk in c ])
            queue = [c[k]]
            color={}
            while queue:
                n=queue.pop()
                color[n]=1
                for m in [ j for j in g.neighbors(n) if not j in color ]:
                    self.new_coordinate[m] = self.new_coordinate[n] + length[n,m]
                    queue.append(m)
            c.sort(key=lambda x: self.new_coordinate[x])
            if debug: print("sorted ends",c, [self.new_coordinate[x] for x in c])
            for k in range(1,len(c),2):
                for l in range(k+1,len(c),2):
                    intervening_segments[ c[k],c[l] ] = [  ]
                    for j in range(k+1,l,2):
                        intervening_segments[ c[k],c[l] ].append( tuple(sorted([c[j],c[j+1]])) )
                    if debug: print("intervening segments:", k,l,c[k],c[l],intervening_segments[ c[k],c[l] ])
                    intervening_segments[ c[l],c[k] ]=intervening_segments[ c[k],c[l] ]
            i+=1

#        print self.new_coordinate
#        print self.new_contig

        self.tiletransforms={}

        rot=["CCW","180","0","CW"]
        rotation_matrix = {
            "0":   [[1,0,0], [0,1,0] ,[0,0,1]],
            "CCW": [[0,1,0], [-1,0,0],[0,0,1]],
            "CW":  [[0,-1,0],[1,0,0], [0,0,1]],
            "180": [[-1,0,0],[0,-1,0],[0,0,1]],
        }

        for t in segments.tiles:
            (a,b),(c,d) = t
#            print ((a,b),(c,d))
            if (a,b)==(c,d):
#                print t,"diagonal"
#                self.tiletransforms[t]=lambda x: x
                self.tiletransforms[t]=lambda x,y: abs(x-y)
            elif self.new_contig[a]==self.new_contig[c]:
#                print t,"linked"
                sb=[(a,c),(a,d),(b,c),(b,d)]
                sbs=list(sb)
                sbs.sort(key=lambda x: abs(self.new_coordinate[x[0]]-self.new_coordinate[x[1]]))
                #print sbs
                x=sbs[0]
                #print x
                #print sb.index(x)
                #print "origin corner:",x,sb.index(x), (segments.ends[x[0]],segments.ends[x[1]]) , "offset:", (self.new_coordinate[x[0]],self.new_coordinate[x[1]]), "rot:", rot[ sb.index(x) ]
                #self.tiletransforms[t] = ((segments.ends[x[0]],segments.ends[x[1]]),rot[ sb.index(x) ],(self.new_coordinate[x[0]],self.new_coordinate[x[1]]))
#                self.tiletransforms[t] = [[1,0,-segments.ends[x[0]][1] ],[0,1,-segments.ends[x[1]][1]],[0,0,1]]
#                print self.tiletransforms[t]
#                self.tiletransforms[t] = np.dot( rotation_matrix[ rot[sb.index(x)] ],self.tiletransforms[t] ) 
#                print self.tiletransforms[t]
#                self.tiletransforms[t] = np.dot( [[1,0, self.new_coordinate[x[0]] ],[0,1,self.new_coordinate[x[1]]],[0,0,1]] ,self.tiletransforms[t] ) 
#                print self.tiletransforms[t]
                rm=rotation_matrix[ rot[sb.index(x)] ]

#                tf =np.dot(  np.array([1,-1,0]) ,  np.dot( [[1,0, self.new_coordinate[x[0]] ],[0,1,self.new_coordinate[x[1]]],[0,0,1]] , np.dot( rm, [[1,0,-segments.ends[x[0]][1] ],[0,1,-segments.ends[x[1]][1]],[0,0,1]] ) ))
                cAmC = rm[0][0] - rm[1][0]
                #cB = rm[0][1]
                cBmD = rm[0][1] - rm[1][1]
                #rD = rm[1][1]
#                diagonal_offset = lambda x,y,z,w: (cA-cC)*z[1] + (cB-cD)*w[1]
#                self.tiletransforms[t] = lambda xxxx: np.dot( np.dot( [[1,0, self.new_coordinate[x[0]] ],[0,1,self.new_coordinate[x[1]]],[0,0,1]] , np.dot( rm, [[1,0,-segments.ends[x[0]][1] ],[0,1,-segments.ends[x[1]][1]],[0,0,1]] ) ), xxxx)
                ivs =  intervening_segments[ x[0],x[1] ]
                if debug: print("ivs",ivs, [ abs( segments.ends[aa][1] - segments.ends[bb][1] ) for aa,bb in ivs  ])
                if debug: print("ivs",ivs, [( segments.ends[aa][1], segments.ends[bb][1] ) for aa,bb in ivs  ])
                self.tiletransforms[t] = partial(coord_transf,cAmC=cAmC,cBmD=cBmD,segments=segments,e1=x[0],e2=x[1],ivs=ivs)
#                if len(ivs)>0:
#lambda xx,yy: abs(cAmC*xx + cBmD*yy - cAmC*segments.ends[x[0]][1] - cBmD*segments.ends[x[1]][1] + sum( [ abs( segments.ends[aa][1] - segments.ends[bb][1] ) for aa,bb in ivs  ] ) )
#                else:
#                    self.tiletransforms[t] = 
#lambda xx,yy: abs(cAmC*xx + cBmD*yy - cAmC*segments.ends[x[0]][1] - cBmD*segments.ends[x[1]][1]  )

            else:
                #print t,"unlinked"
#                self.tiletransforms[t]=lambda x: np.dot(0,x)
                self.tiletransforms[t]=lambda x,y: 0



def next_join(frozen,dataset,forbidden,originals,blocked_ends,segments,bps2,L0,end2break,exclude_originals=True):
    optimal_single_joins=[]
    used_ends = []    
    frozen_breakpoints=[]
    for f in frozen:
        for ff in f:
            used_ends.append(ff)
            if segments.ends[ff] in bps2.breaks:
                frozen_breakpoints.append( bps2.breaks.index(segments.ends[ff]) )

    for i,j in itertools.combinations(list(range(len(segments.ends))),2):
        if debug: print("combo:",i,j)
        if i in blocked_ends: 
            if debug: print("blocked",i)
            continue
        if j in blocked_ends: 
            if debug: print("blocked",i)
            continue
        if (i,j) in forbidden: 
            if debug: print("forbidden")
            continue
        if (i,j) in frozen or i in used_ends or j in used_ends: 
            if debug: print("not available",(i,j) in frozen,i in used_ends,j in used_ends)
            continue
        breaks_to_optimize = list(set([ bps2.breaks.index( segments.ends[k] ) for k in [i,j] if (not end2break[k]==k) and segments.ends[k] in  bps2.breaks and not bps2.breaks.index( segments.ends[k] ) in frozen_breakpoints ]))

        ends_used = []
        for xi,xj in frozen + [(i,j)] :
            ends_used.append(xi)
            ends_used.append(xj)
        originals_to_keep = [ o for o in originals if (not o[0] in ends_used) and (not o[1] in ends_used) ]        

        cycle = has_a_cycle( tuple(frozen + [(i,j)] + originals_to_keep) ,segments)
        print("xz:",i,j,breaks_to_optimize,[bps2.breaks[k] for k in breaks_to_optimize ],cycle)
        if cycle: continue

        rB=Rearrangement(segments, tuple(frozen + [(i,j)] + originals_to_keep) )
        if (i,j) in originals:            
            L=dataset.llr(rB)
            print("native:",(i,j),L)
            if not exclude_originals: optimal_single_joins.append((L,(i,j),tuple(),tuple()))
        elif not breaks_to_optimize: 
            L=dataset.llr(rB)
            print("end_to_end:",(i,j),L)                
            optimal_single_joins.append((L,(i,j),tuple(),tuple()))
        else:
            pass
            optimized=mcmc_optimize(bps2,dataset,rB,breaks_to_optimize)
            print("best_was:",optimized,breaks_to_optimize)
            optimal_single_joins.append((optimized[0],(i,j),optimized[1],breaks_to_optimize))
    optimal_single_joins.sort(reverse=True,key=lambda x: (x[0],-len(x[-1])) )  # break ties by taking the one that uses the fewest breaks to avoid degnerate cases where you add a break 1 base from an existing end; 
    print("sorted options:")
    for i in optimal_single_joins:
        print("zzz:",i[0]-L0,i)
    if optimal_single_joins:
        return optimal_single_joins[0]
    return [L0-1e9]
    
def overlap(pair1,pair2):
    if max(pair1)<min(pair2):return False
    if max(pair2)<min(pair1):return False
    return True

class RearrangableLocusSet(object):
    def __init__(self,locus_list,ll,connected_pairs=False):
        
        scaffolds={}
        scaffold_loci={}

        scaffolds_in_order=[]

        ends_in_play={}
        self.ends_in_play=ends_in_play

        for locus in locus_list:
            m=re.match("(.*):(-?\d+)-(-?\d+)",locus)
            scaffold,start,end = m.groups()
            scaffolds_in_order.append(scaffold)
            start=max(0,int(start))
            end=min(int(end),ll[scaffold])
            scaffolds[scaffold]=1
            scaffold_loci[scaffold]= scaffold_loci.get(scaffold,[])+[(start,end)]

            ends_in_play[scaffold]=[]
            if start < 10000:            ends_in_play[scaffold].append("5prime")
            if ll[scaffold]-end < 10000: ends_in_play[scaffold].append("3prime")

        if not connected_pairs:
            self.connected_pairs =  [ (locus_list[i],locus_list[i+1]) for i in range(len(locus_list)-1) ]
            self.connected_pairs += [ (locus_list[i+1],locus_list[i]) for i in range(len(locus_list)-1) ]
        else:
            self.connected_pairs = connected_pairs

        print(scaffolds)
        print(scaffold_loci)

        self.scaffolds=scaffolds
        self.scaffold_loci=scaffold_loci
        sequenceset = SequenceSet(scaffolds,ll,scaffold_loci)
        self.sequenceset=sequenceset 

        bps = BreakpointSet(sequenceset)  #trivial; no breaks
        self.bps=bps

        segments=Segments(sequenceset,bps)
        self.segments=segments

        self.r0=Rearrangement(segments,segments.original_joins)
        self.rB=Rearrangement(segments,[])

        self.dataset=Dataset(segments,get_score)

    def point2locus(self,p):
        scaffold,x=p
        for a,b in self.scaffold_loci[scaffold]:
            if a<=x and x<=b:
                return "{}:{}-{}".format(scaffold,a,b)
        return False

    def add_data(self,peaks,diagonal_data):
        scaffolds=self.scaffolds
        scaffold_loci=self.scaffold_loci
        dataset=self.dataset
        
        for scaffold in scaffolds:
            for scaffold1,scaffold2,ranges1,ranges2,pairs in peaks.get(scaffold,[]):
                if scaffold2 in scaffolds: print("ndata",scaffold1,scaffold2,scaffold2 in scaffolds,len(pairs))
                keep2=False
                keep1=False

                for r in ranges1:
                    for p in scaffold_loci[scaffold1]:
                        if overlap(r,p):
                            keep1=True

                if scaffold2 in scaffolds:
                    for r in ranges2:
                        for p in scaffold_loci[scaffold2]:
                            if overlap(r,p):
                                keep2=True
                if keep1 and keep2: 
                    for x,y in pairs:
                        dataset.add_datapoint( ((scaffold1,x),(scaffold2,y))  )
            print("ndata points:",scaffold,len(dataset.data))


        for scaffold in scaffolds:
    #    if False:
    #            diagonal_data[scaffold1] = diagonal_data.get(scaffold1,[])+[ ((start,end),tuple(  eval(" ".join(c[2:])) ))   ]
            for rangex,pairs in diagonal_data.get(scaffold):
    #            print "ndata",scaffold,rangex,len(pairs), overlap((start,end),scaffold_loci[scaffold][0])

                scaffold1=scaffold
                start,end = rangex

                ranges1=[(start,end)]

                keep1=False

                for p in scaffold_loci[scaffold]:
                    if overlap((start,end),p):
                        keep1=True
                if keep1:                 
                    for x,y in pairs:
                        if  (start<=x and x<=end and start<=y and y<=end):
                            dataset.add_datapoint( ((scaffold1,x),(scaffold1,y))  )
            print("ndata points:",scaffold,len(dataset.data))
        print("tileset:",self.segments.tiles)
        self.L0=self.dataset.llr(self.r0)

    def find_weakspots(self):
        bps = self.bps
        dataset=self.dataset
        segments=self.segments
        length=segments.sequenceset.length
        rB=self.rB
        L0=self.L0

        weakspots = []
        self.weakspots = weakspots

        for scaffold in list(self.scaffold_loci.keys()):
            for minx,maxx in self.scaffold_loci[scaffold]:
                print("scan",scaffold)
                #def breakpoint_likelihood_scan2(self,scaffold,minx,maxx,deltaL=30.0):

                wss = dataset.breakpoint_likelihood_scan2(scaffold,minx,maxx)

                for ws in wss:
                    if not (ws[0]<1000 and (length[scaffold]-ws[1])<1000) :
                        if ws[0]<100:   # don't let the breaks go within 100 bp of the ends -- degenerate cases mess things up.
                            if ws[1]>100:
                                weakspots.append( ( scaffold ,(100,ws[1])) )
                        elif ws[1]>length[scaffold]-100:
                            if ws[0]<length[scaffold]-100:
                                weakspots.append( ( scaffold ,(ws[0],length[scaffold]-100)) )
                        else:
                                weakspots.append( ( scaffold ,ws) )
                            
        print("ws:",weakspots)

    def test_weakspot_rearrangements(self):
        length=self.segments.sequenceset.length

        weakspots = list(self.weakspots)
        
        has_a_weakspot={}
        for s,w in weakspots:
            has_a_weakspot[s]=True
        print("w",weakspots)
        for s in self.segments.sequenceset.sequences:
            if not s in has_a_weakspot and not s in self.segments.sequenceset.has_end_in_play:
                for l in self.segments.sequenceset.loci[s]:
                    weakspots.append( (s,l) )
        print("w",weakspots)
        # set up the breakpoints
        self.bps2 = BreakpointSet(self.sequenceset,weakspots)
        bps2=self.bps2
        segments=Segments(self.sequenceset,bps2)
        self.segments=segments

        # switch the dataset over to the new 
        self.dataset.segments = segments
        self.dataset.update_indices()
        self.dataset.rebuild_tilemap()
        dataset = self.dataset

        # set up indices; figure out which ends are blocked, which joins are allowed, etc
        bp2locus={}
        blocked_ends=[]
        end2break={}
        for i in range(len(self.segments.ends)):
            end2break[i]=i

        originals=[]
        for i in range(1,len(self.segments.ends)-1,2):
            if self.segments.ends[i]==self.segments.ends[i+1]:
                originals.append((i,i+1))
                end2break[i]=str((i,i+1))
                end2break[i+1]=str((i,i+1))
            else:
                end2break[i]=i

        for i in range(len(self.segments.ends)):
#            end2break[i]=i
            scaffold,x = self.segments.ends[i]
            endok=False
            blocked=False
            locus = self.point2locus(self.segments.ends[i])
            if x==1                and "5prime" in self.ends_in_play[scaffold]: 
                endok=True
                locus = "{}:{}-{}".format(*min( [ (scaffold,a,b) for a,b in self.scaffold_loci[scaffold] ] ))
                #print "zz",i,locus
                #end2break[i]=i
            elif x==1:
                #end2break[i]=i
                pass
            elif x==length[scaffold] and "3prime" in self.ends_in_play[scaffold]: 
                endok=True
                locus = "{}:{}-{}".format(*max( [ (scaffold,a,b) for a,b in self.scaffold_loci[scaffold] ] ))
                #print "zz",i,locus
                #end2break[i]=i
            elif x==length[scaffold]:
                pass
                #end2break[i]=i
            elif locus:
                pass
                #end2break[i]=locus
            else:
                print("wtf?",i,scaffold,x,locus)
            bp2locus[i]=locus
            if not locus: blocked=True
            print(i,self.segments.ends[i],locus,endok,blocked)
            if blocked:
                blocked_ends.append(i)

        forbidden=[]
        for i in range(len(self.segments.ends)):
            for j in range(i+1,len(self.segments.ends)):
                if (not bp2locus[i]==bp2locus[j]) and not (bp2locus[i],bp2locus[j]) in self.connected_pairs:
                    forbidden.append( (i,j) )

        for i in range(0,len(self.segments.ends),2):
            forbidden.append((i,i+1))


        print("connected pairs:", self.connected_pairs)
        print("forbidden", forbidden)
        print("blocked", blocked_ends)
        print("end2break", end2break)
        print("originals", originals)

#        from recombinings2 import recombinings

        configs=[]
        recombinings2.printed={}
        recombinings2.color={}


        r0=Rearrangement(segments, originals )
        L0=dataset.llr(r0)
        lastL=L0
        frozen=[]
        best_next_join=next_join(frozen,dataset,forbidden,originals,blocked_ends,segments,bps2,L0,end2break,exclude_originals=True)
        print("bnj:",best_next_join[0]-L0, best_next_join)
        nextL=best_next_join[0]
        while nextL > lastL:
            frozen.append(best_next_join[1])
            if len(best_next_join)==4:
                for i in best_next_join[3]:
                    x= best_next_join[2][i][1]
                    dx=x-bps2.breaks[i][1]
                    bps2.move_breakpoint(i, dx)
                    
            lastL = best_next_join[0]
            best_next_join=next_join(frozen,dataset,forbidden,originals,blocked_ends,segments,bps2,L0,end2break,exclude_originals=False)
            if len(best_next_join)==1:break
            nextL=best_next_join[0]
            print("bnj:", best_next_join)

        used_ends = []    
        used_breaks=[]
        preserved_originals=[]
        for f in frozen:
            if f in originals:
                preserved_originals.append(f)
            else:
                for ff in f:
                    used_ends.append(ff)
                    if segments.ends[ff] in bps2.breaks and not end2break[ff]==ff:
                        used_breaks.append( bps2.breaks.index(segments.ends[ff]) )

        print("\t".join(map(str,["frozen:",lastL-L0,L0,lastL,tuple(set(frozen)-set(preserved_originals)),bps2.breaks,segments.ends,used_ends,list(set(used_breaks))])))

        return()

        configs=[]
        recombinings2.printed={}
        recombinings2.color={}

        if len(originals)<=recombinings2.max_n_breaks:
            forced_iter=iter([tuple()])
        else:
            forced_iter=itertools.combinations( originals, len(originals)-recombinings2.max_n_breaks )

        for forced in forced_iter:
            print("forced:",forced)
            recombinings2.recombinings(list(range(len(self.segments.ends))),set(blocked_ends),forbidden,len(self.segments.ends),end2break,originals,configs,forced=forced)
        configs.sort()
        print("nconfigs:",len(configs))
        configs_with_L=[]
        L0=0.0
        for m in configs:
#            print "config:"+"\t".join(map(str,m))
            
            rB=Rearrangement(segments,m[2])
            L=dataset.llr(rB)
            if m[0]==0: L0=L
            print("\t".join(map(str,["config:",L]+list(m))))
            configs_with_L.append( (L,m[0],m[1],m[2]) )
        if len(configs_with_L)==0:
            print("no configs.",configs)
            return (None,None,None)
        configs_with_L.sort(reverse=True)
        Lbest=configs_with_L[0][0]
        if configs_with_L[0][0]>L0:
            for m in configs_with_L:
                if Lbest-m[0]<20.0: print("\t".join(map(str,m))+"\t###")
        print("         L0=\t",L0)
        print("      Lbest=\t",Lbest)
        print("improvement=\t",Lbest-L0)
        print(bps2.breaks)
        return (Lbest-L0,configs_with_L[0],bps2.breaks)

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--loci')
    parser.add_argument('-A','--locuslist')
    parser.add_argument('-L','--lengths')
    parser.add_argument('-p','--peaks')
    parser.add_argument('-P','--pairs')
    parser.add_argument('-B','--max_n_breaks',type=int,default=3)
    parser.add_argument('-d','--debug',action="store_true")
    parser.add_argument('-M','--set_insert_size_dist_fit_params')
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    
    args = parser.parse_args()
    if args.seed != -1 :
        random.seed(args.seed)



    recombinings2.max_n_breaks=args.max_n_breaks

    fmodel=open( args.set_insert_size_dist_fit_params )
    contents = fmodel.read()
    try:
        fit_params=eval(contents)
    except:
        "couldn't deal with option", args.param
    fmodel.close
    ces.set_exp_insert_size_dist_fit_params(fit_params)

    cache_cutoff = 150000
    noise_level = ces.insert_size_dist(150000)
    log_score_cache={}
    log_noise_level = math.log(noise_level)
    log_score_cache[0] = log_noise_level
    for i in range(1,cache_cutoff):
        log_score_cache[i]=math.log(ces.insert_size_dist(i) + noise_level)

    def get_score(d):
        if d < cache_cutoff:
            x=log_score_cache[d]
            if x>0: print("wtf?",x,d)
            return x #log_score_cache[d]
        else:
            return log_noise_level
#            x = math.log(ces.insert_size_dist(d)+noise_level)
#            if x>0: print "wtf2?",x,d
#            return x #math.log(ces.insert_size_dist(d)+noise_level)

    

    ll={}
    f=open(args.lengths)
    while True:
        l=f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        ll[c[0]]=int(c[1])
    f.close()



    data = []
    datapoints={}

    peaks={}
    for fn in glob.glob(args.peaks):
        f =open(fn,"rt")
#        print fn
        nl=0
        while True:
            l=f.readline()
            if not l: break
            nl+=1
            #scaffold_101    scaffold_1775   7460270 14658   29      [(1127320, 0), (1129535, 0), (1130329, 31), (1130433, 123), (1128283, 240), (1131546, 372), (1129490, 409), (1130269, 585), (1130274, 601), (1121018, 624), (1129241, 797), (1123508, 850), (1126082, 1011), (1130245, 1016), (1131550, 1021), (1128986, 1097), (1125320, 1295), (1129018, 1400), (1131745, 1436), (1133801, 1474), (1130345, 1519), (1137758, 1645), (1139834, 1645), (1129279, 1685), (1130223, 1696), (1129394, 3817), (1128027, 3832), (1130196, 6463), (1130281, 8976)]    [(1121018, 1139834)]    [(0, 8976)]
            c=l.strip().split("\t")
            scaffold1,scaffold2,len1,len2,nlinks,ranges1,ranges2 = c[0],c[1],int(c[2]),int(c[3]),int(c[4]),eval(c[6]),eval(c[7])
            pairs = eval(c[5])
            peaks[scaffold1] = peaks.get(scaffold1,[]) + [[scaffold1,scaffold2,ranges1,ranges2,pairs                            ]]
            peaks[scaffold2] = peaks.get(scaffold2,[]) + [[scaffold2,scaffold1,ranges2,ranges1,[ (xx[1],xx[0]) for xx in pairs ]]]
        f.close()# =open(fn,"rt")

    diagonal_data={}
    for fn in glob.glob(args.pairs):
        f =open(fn,"rt")
#        print fn
        nl=0
        while True:
            l=f.readline()
            if not l: break
            nl+=1
            ##pairs scaffold_1:1-28187 [(6810, 133), (3426, 142), (855, 161), (995
            c=l.strip().split()
            locus=c[1]
            m=re.match("(.*):(-?\d+)-(-?\d+)",locus)
            scaffold1,start,end = m.groups()
            diagonal_data[scaffold1] = diagonal_data.get(scaffold1,[])+[ ((int(start),int(end)),tuple(  eval(" ".join(c[2:])) ))   ]
        f.close()# =open(fn,"rt")

    print("Done loading the data")
    sys.stdout.flush()


#            peaks[scaffold1] = peaks.get(scaffold1,[]) + [[scaffold1,scaffold2,ranges1,ranges2,pairs                            ]]


    if args.loci:
        locus_iter = iter([args.loci])
    elif args.locuslist:
        locus_file=open(args.locuslist)
        locus_iter = locus_file.readlines()

    for locus1 in locus_iter:
        c = locus1.strip().split()
        locus=c[0]
        print("\t".join(map(str,["LOCUS:"]+locus.split(','))))
        sys.stdout.flush()
        complex=RearrangableLocusSet(locus.split(','),ll)
        complex.add_data(peaks,diagonal_data)
        complex.find_weakspots()
        print(complex.ends_in_play)
        print("n_ws:",locus,len(complex.weakspots),complex.weakspots)
        if len(complex.weakspots)>5: continue 
        r=complex.test_weakspot_rearrangements()
        print("improvement:","\t".join(map(str,[locus]+list(r))))

    exit(0)
    L0=complex.dataset.llr(r0)
    print(L0)


    i=2
    dx=9671896-bps.breaks[i][1]
    oldx = bps.breaks[i][1]
    bps.move_breakpoint(i, dx)
    dataset.update_tilemap()#  (bps.breaks[i][0],oldx),bps.breaks[i] )

    i=1
    dx=300000-bps.breaks[i][1]
    oldx = bps.breaks[i][1]
    bps.move_breakpoint(i, dx)
    dataset.update_tilemap()#  (bps.breaks[i][0],oldx),bps.breaks[i] )

#9671896

#    print "llr:",dataset.llr(r0), dataset.llr(r1)
    print("breaks:",bps.breaks)

#    print "llr:",L



