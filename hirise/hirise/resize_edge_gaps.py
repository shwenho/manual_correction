#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-g','--gap',default=100,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

#edge:  Scaffold35797_1.3       Scaffold139065_1.3      {'length': 1000, 'contig': False}
while True:
     l=sys.stdin.readline()
     if not l: break
     c=l.strip().split("\t")
     if not c[0]=="#edge:": 
          print(l.strip())
          continue
     h=eval(c[3])
     if not h['contig']: h['length']=args.gap

     print("\t".join(map(str,[ "#edge:",c[1],c[2],h ])))

