#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
import sys
import argparse
import random

parser = argparse.ArgumentParser()
parser.add_argument('-c','--chromosomes',required=True)
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-n','--nbreaks',default=100,type=int)
parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
args = parser.parse_args()

if args.seed != -1 : 
    random.seed(args.seed)

def build_chromosome_dict(filename):
    """read a two-column text file mapping chromosomes to lengths and return a dictionary."""
    d={}
    f=open(filename)
    while True:
        l=f.readline()
        if not l:
            break
        c=l.strip().split()
        d[c[0]]=int(c[1])

    f.close()
    return d

def weighted_random_chromosome(l={},g=1,L=[]):
    """Chose a chromosome at random, with probability proportional to their lengths."""
    if not L:
        if args.debug:
            print("init chooser")
        s=0.0
        for chr in sorted(l.keys()):
            x=old_div(float(l[chr]),g)
            s+=x
            L.append([chr,s])
        if args.debug:
	    print(L)

    r=random.random()
    for l in L:
        if r<l[1]:
	    return l[0]


def random_genome_location():
    """return a 2-tuple with chromosome and position for base in the genome selected uniformly at random."""
    chromosome=weighted_random_chromosome(chromLen,genome_length)
    x=int(random.random()*chromLen[chromosome])
    return((chromosome,x))

chromLen=build_chromosome_dict(args.chromosomes)
genome_length = sum(chromLen.values())

breaks={}
locations_hit={}
for n in range(args.nbreaks):
    b=random_genome_location()
    if b[0] not in breaks:
        breaks[b[0]]=[]
    if b not in locations_hit:
        breaks[b[0]].append(b[1])
        locations_hit[b]=1

segment={}
for chrom in list(breaks.keys()):
    last_break=0.0
    i=1
    for break_point in sorted(breaks[chrom]):
        segment[chrom+"."+str(i)]=(chrom,last_break,break_point)
        i+=1
        last_break = break_point
    segment[chrom+"."+str(i)]=(chrom,last_break,chromLen[chrom])

i=1
for s in list(segment.keys()):
#    print s,segment[s]
    chrom=segment[s][0]
    slen = segment[s][2]-segment[s][1]
    print("chain",1,chrom,chromLen[chrom],"+",segment[s][1],segment[s][2],s,slen,"+",0,slen,i)
    print(" ",slen)
    i+=1

segment_offset={}
sum_of_segments={}
orig={}



