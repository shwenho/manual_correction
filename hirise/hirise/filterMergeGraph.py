#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-L','--maxlength',default=20000.0,type=float)
    parser.add_argument('-o','--endlog')
    parser.add_argument('-b','--besthits')
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    besthit={}
    if args.besthits:
#        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
    #            print c[1],besthit[c[1]]
            f.close()



    ends={}

    while True:
        l= sys.stdin.readline()
        if not l: break
        if l.strip()[-2:]=="i:": 
            c = l.strip()[:-3].split("\t")
#            print c
            l1,l2=list(map(float,[c[8],c[9]]))
            s1,s2=list(map(int  ,[c[4],c[5]]))
            if args.endlog:
                e1=eval(c[2])
                e2=eval(c[3])
                ends[s1]=e1
                ends[s2]=e2
#            print s1,l1
#            print s2,l2
            if l1 < args.maxlength and l2 < args.maxlength:
                scls=[]
                smax=0.0
                for i in range(4):

                    l=sys.stdin.readline().strip()
                    c=l.strip().split()
                    scls.append(c)
                    d1 = c.pop(0)
                    d2 = c.pop(0)
                    se1 = c.pop(0)
                    se2 = c.pop(0)
#                    print se1,se2,scls
                    smax = max([smax]+list(map(float,c)))
                print("\t".join(map(str,[s1,s2,smax]))) #,scls


    if args.endlog:
        f=open(args.endlog,"wt")
        for s in list(ends.keys()):
            f.write( "\t".join([str(s),ends[s][0],ends[s][1]])+"\n")
        f.close()


