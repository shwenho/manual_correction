#!/usr/bin/env python3

import argh

def main(hra, bed, output="/dev/stdout"):
    scaffolds = set()
    with open(hra) as hra_handle:
        for line in hra_handle:
            if line.startswith("P "):
                scaffold = line.split()[2]
                scaffolds.add(scaffold)

    with open(bed) as bed_handle:
        empty = True
        with open(output, 'w')  as output_handle:
            for line in bed_handle:
                scaffold = line.split()[0]
                if scaffold in scaffolds:
                    print(line.rstrip(), file=output_handle)
                    empty = False
            if empty:
                first = list(scaffolds)[0]
                print(first, 0, 1, "deep", sep="\t", file=output_handle)
if __name__ == "__main__":
    argh.dispatch_command(main)
