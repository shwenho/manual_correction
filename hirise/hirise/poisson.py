#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
#!/usr/bin/env python3
import sys
import argparse
from scipy.stats import poisson

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-N',default=500,type=float)
parser.add_argument('-m','--mu',default=1.0,type=float)
parser.add_argument('-M','--maxx',default=False,type=float)

args = parser.parse_args()
if args.progress: print("#",args)


rv = poisson(args.mu)

maxx=int(10*args.mu)
if args.maxx: maxx = int(args.maxx)
for i in range(0,maxx+1):
     print("\t".join(map(str,[i,rv.pmf(i),rv.pmf(i)*args.N,rv.cdf(i)])))

