#! /bin/bash

hirise_iter=$1
num=$2
size=$3

mkdir -p visualizations

random_loci.py --lengths ${hirise_iter}.fasta.lengths -f -n ${num} -l ${size} |  parallel --gnu "examine_layout.py -R {} -i ${hirise_iter}.hra | vis.py -A ${hirise_iter}.hra | mpl_oovis.py --save visualizations/${hirise_iter}_region_{}_vis.png"
