#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
from builtins import object
from past.utils import old_div
import sys
import networkx as nx
#import hirise.chicago_edge_scores as ces
from scipy.special import gammaincc
from scipy import nan
import math
import random
import signal

class ChicagoModel(object):
    def __init__(self,p,nnn=500000.0):
        self.p= p
#        self.variable = ["alpha" ,"f","d","pn"]
        self.xmin=p["xmin"]
        self.n = p["n"]
        n=self.n
        self.nnn=nnn
        self.a = [1.0]*p['n']
        self.zeta = 10.0
        #step = (100000.0 - 1000.0) / (self.n+1)
        incr = (2.0*100000.0 / self.xmin)**(old_div(1.0,(n-1)))
#        print incr

#2347.85782182914 1499.70650098425 10000.0 7000.0
        self.b = [ old_div(2.0,(self.xmin * (incr**i)))  for i in range(n) ]   # [0.000985265482483, 0.000231672269421, 5.47258115216e-05] #[1.0/1128.21525930566, 1.0 / 3000.0 , 1.0/5877.85005860888] # [1.0/1499.70650098425, 1.0/2347.85782182914, 1.0/7000.0 , 1.0/10000.0]
        self.a = [  old_div(1.0,(-math.log(self.b[i]))) for i in range(self.n)]  # [0.000299694942969/self.b[0], 7.95638116899e-05/self.b[1] , 1.92848588223e-05/self.b[2]] #[ 7671.66586595616, -1592.57384177803, 1016.2641695871, -190.208025776531 ] 
#gnuplot> print a,a2,a3,a4
#-1592.57384177803 7671.66586595616 -190.208025776531 1016.2641695871

#        self.b = [ 1.0/200, 1.0/1000, 1.0/5000, 1.0/10000 ] # [ 1.0/(2000.0 * (incr**i))  for i in range(n) ] 
#        print [ 1.0/self.b[i] for i in range(n)]
#        for i in range(n):
#            self.a[i] = 1.0
        self.pn = p[ "pn" ]
        self.G  = p[ "G"  ]
        self.normalize()

    def __repr__(self):
        str = "{} + (1.0-{})*(".format(old_div(self.pn,self.G),self.pn) + " + ".join([ "{}*exp(-(x)/{})".format(self.a[i]*self.b[i],old_div(1.0,self.b[i])) for i in range(self.n)]) + ")"
        dictr={ "pn":self.pn, "G": self.G, "alpha":self.a, "beta":self.b, "fn":str }
        return dictr.__repr__()

# \int_{\x_{m}}^{G} p_n/G + (1-p_n)\sum_i a_i e^{-b_i x} dx
# \bigg[ x p_n /G - (1-p_n) \sum_i a_i/b_i e^{-b_i x} \bigg] \bigg|_{x_m}^{G}
# p_n ( 1 - x_m / G ) + (1-p_n) \sum_i a_i/b_i e^{-b_i x_m}

    def logprior(self):
#        return(0.0)
        return sum( [-self.a[i]*self.zeta*math.log(old_div(1.0,self.b[i])) for i in range(self.n)] )

    def normalize(self):
        nn = sum( [self.a[i]      for i in range(self.n) ] )
        self.a = [ old_div(self.a[i], nn) for i in range(self.n) ]

        self.A = fi(self.xmin,self.a,self.b,self.pn,self.G,1.0) # self.pn - (self.xmin*self.pn)/(self.G) + (1.0-self.pn) *sum([self.a[i]*math.exp(-self.b[i]*self.xmin) for i in range(self.n)])

#def fi(x,a,b,pn,G,A):
#    n=len(a)
#    if not len(b)==n: 
#        raise Exception 
 #   return ((  pn - (x)*pn/G + (1.0-pn)* sum( [ (a[i])*( math.exp(-b[i]*(x))) for i in range(n)] )  )/A)


    def clone(self):
        m2 = ChicagoModel(dict(self.p))
        m2.a = list(self.a)
        m2.b = list(self.b)
        m2.pn = self.pn
        m2.G = self.G
        m2.nnn=self.nnn
        return m2

    def stepP(self):
        m2=self.clone()
        m2.pn *= 1.0 + random.uniform(-0.1, 0.1)
        while not (m2.pn > 0.0 and m2.pn < 1.0):
            m2.pn = self.pn *( 1.0 + random.uniform(-0.1, 0.1))
        m2.normalize()
        return m2
        

    def stepA(self):        
        m2 = self.clone()
        i = random.choice(list(range(self.n)))
        m2.a[i] *= 1.0 + random.uniform(-0.1, 0.1)
#        if random.random()<0.05: m2.a[i]*=-1.0
        while not m2.a[i] > 0.0:
            m2.a[i] = self.a[i]*( 1.0 + random.uniform(-0.1, 0.1))
        m2.normalize()
        return m2

    def stepB(self):        
        m2 = self.clone()
        i = random.choice(list(range(self.n)))
        m2.b[i] *= 1.0 + random.uniform(-0.1, 0.1)
#        print i
        sys.stdout.flush()
        try:
            while  not (m2.b[i]>0.0 and ( i==0 or (m2.b[i] < m2.b[i-1]) ) and ((i==self.n-1) or (m2.b[i] > m2.b[i+1])) and ((old_div(1.0,m2.b[i]))<self.nnn and (old_div(1.0,m2.b[i]))>old_div(self.xmin,3.0)) ) :
                m2.b[i] = self.b[i]*(1.0 + random.uniform(-0.1, 0.1))
        except KeyboardInterrupt:
            print("interrupted while trying to find an acceptable b[i]",i,self.n,old_div(1.0,m2.b[i]),self.nnn,m2.b, [m2.b[i]>0.0,i==0 or ( m2.b[i] < m2.b[i-1]),i==self.n-1 or (m2.b[i] > m2.b[i+1]),(old_div(1.0,m2.b[i]))<self.nnn,(old_div(1.0,m2.b[i]))>old_div(self.xmin,3.0)])
            print(self)
            print(m2)
            raise KeyboardInterrupt

        m2.normalize()
        return m2

    def step(self):        
        if random.random()<0.1:
            return self.stepP()

        else:
            if random.random()<0.5:
                return self.stepA()
            else:
                return self.stepB()


#G=3.0e9
N=100000000.0
pn=0.446120771371
G=3000000000.0
from scipy.special import gammaincc

def fx(x,a,b,pn,G,A):
    n=len(a)
#    print n
#    for i in range(n):
#        print  a[i]*math.exp(-b[i]*x),pn/G,1.0-pn,b[i],b[i]*x
    if not len(b)==n: 
        raise Exception 
    p= old_div(( old_div(pn,G) + (1.0-pn) * sum( [ a[i]*b[i]*math.exp(-b[i]*(x)) for i in range(n)] ) ), A)
    if p < 1.0e-9: p = 1.0e-9
    return ( p ) 

def fi(x,a,b,pn,G,A):
    n=len(a)
    if not len(b)==n: 
        raise Exception 
    return (old_div((  pn - (x)*pn/G + (1.0-pn)* sum( [ (a[i])*( math.exp(-b[i]*(x))) for i in range(n)] )  ),A))

def lnL(model,hist,hist2,mind,nnn,args):
    lll=model.logprior()
    #print model.A
    for x in range(mind,nnn,50):
        lll += hist[x] * math.log( fx(x,model.a, model.b, model.pn, model.G ,model.A) ) 
        lll += hist2[x]* math.log( fi(x,model.a, model.b, model.pn, model.G, model.A ) )
    return lll

#params=False


def main():
    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-H','--histogram')
    parser.add_argument('-v','--var', default=False)
    parser.add_argument('-o','--outfile', default=False)
    parser.add_argument('-P','--param', default="{}")
    parser.add_argument('-m','--mind', type=int, default=1000)
    parser.add_argument('--itmax', type=int, default=5000)
    parser.add_argument('-N','--ncomps', type=int, default=4)
    parser.add_argument('-G','--genomesize', type=float, default=3.0e9)
#    parser.add_argument('-c','--climb', action="store_true")

    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()
    if args.seed != -1 :
      random.seed(args.seed)


    #ces.debug=args.debug
    if args.debug:
        args.progress=True
#    if args.progress: log( str(args) )

#    command_line_params = eval(args.param)


    fit_params={}
    try:
        fit_params = eval(args.param)
    except Exception as e:
        f=open(args.param)
        contents = f.read()

        try:
            fit_params=eval(contents)
        except:
            "couldn't deal with option", args.param
            #exit(0)
        f.close

    fit_params["N"]=1.0
    fit_params["n"]=args.ncomps

    alpha = fit_params['alpha']
    beta=fit_params['beta']
    fit_params["n"]=len(alpha)
    pn=fit_params['pn']
    G=fit_params['G']
    meanL = sum([old_div(alpha[i],beta[i]) for i in range(len(alpha))])
    print("#meanL",meanL)
#    ces.set_exp_insert_size_dist_fit_params(fit_params)

    G=fit_params["G"]
    pn=fit_params["pn"]
    fit_params["xmin"]=1000.0

    fff=open(args.histogram)

    hist={}
    hist2={}
    
    while True:
        l=fff.readline()
        if not l: break
        l,h,h2 = list(map(int,l.strip().split() ))
        hist[l]=h
        hist2[l]=h2
    nnn = max( list(hist.keys()) + list(hist2.keys()) )+1

    mind=args.mind
    params = ChicagoModel( fit_params , nnn=nnn) #  { "a":a, "b":b, "c":c, "d":d,"f":f, "pn":pn, "G":G } )
    params.a = alpha
    params.b= beta

    A=0.0
    B=0.0
    for i in range(mind,nnn):
        print(i,hist.get(i,0),hist2.get(i,0),fx(float(i),params.a,params.b,params.pn,params.G,params.A),fi(float(i),params.a,params.b,params.pn,params.G,params.A),float(hist2.get(i,0))**2.0,float(hist.get(i,0))**2.0)
        A+=(old_div(float(hist2.get(i,0))**2.0,fi(float(i),params.a,params.b,params.pn,params.G,params.A)) +  old_div(float(hist.get(i,0))**2.0,fx(float(i),params.a,params.b,params.pn,params.G,params.A)))
        B+=(hist.get(i,0)+hist2.get(i,0))
    print(old_div(A,B),A,B)

    exit(0)

#    print "#",params.a, params.b

#    params.nnn = nnn

    accept_count =0
    attempt_count = 0
    ll = lnL(params,hist,hist2,mind,nnn,args) 

    def interrupt_handler(signum,frame):
        print('Signal handler called with signal', signum)
        print(attempt_count, accept_count, ll, " ".join(map(str,[params.pn, params.G] + params.a+params.b)))
        print("#",params)
        raise KeyboardInterrupt

    signal.signal(signal.SIGINT  , interrupt_handler)
    signal.signal(signal.SIGTERM , interrupt_handler)

    #params.normalize()
    #print ll
    if True: #args.climb: 

        for q in range(50):
            attempt_count +=1
            p = params.stepP()
            ll2 = lnL(p,hist,hist2,mind,nnn,args) 
#            if math.isnan(ll2) : continue
            #print "#", ll,ll2 ,ll2-ll, math.exp( min(0.0,ll2 - ll) )
            if ll2>ll or ( random.random() < math.exp( ll2 - ll ) ):
                params = p
                accept_count += 1
                ll=ll2
                print(attempt_count, accept_count, ll, " ".join(map(str,[params.pn, params.G] + params.a+params.b)))
                print("#",params)



        while attempt_count < 1050 and attempt_count < args.itmax:
            attempt_count +=1
            p = params.stepA()
            ll2 = lnL(p,hist,hist2,mind,nnn,args) 
#            if math.isnan(ll2) : continue
            #print "#", ll,ll2 ,ll2-ll, math.exp( min(0.0,ll2 - ll) )
            if ll2>ll or ( random.random() < math.exp( ll2 - ll ) ):
                params = p
                accept_count += 1
                ll=ll2
                print(attempt_count, accept_count, ll, " ".join(map(str,[params.pn, params.G] + params.a+params.b)))
                print("#",params)


        while attempt_count < args.itmax:
            attempt_count +=1
            p = params.step()
            ll2 = lnL(p,hist,hist2,mind,nnn,args) 
#            if math.isnan(ll2) : continue
#            print "#", ll,ll2 ,ll2-ll, math.exp( min(0.0,ll2 - ll) )
            if ll2>ll or ( random.random() < math.exp( ll2 - ll ) ):
                params = p
                accept_count += 1
                ll=ll2
                print(attempt_count, accept_count, ll, " ".join(map(str,[params.pn, params.G] + params.a+params.b)))
                print("#", params)

        if args.outfile:
            f=open(args.outfile,"wt")
            f.write(str(params))
            f.write("\n")
            f.close()

if __name__ == '__main__':
    main()

#    import signal

#    try:
#        main()
#    except Interrupt:
#        print 'Killed by user'
#        print params
#        raise KeyboardInterrupt
#        sys.exit(0)
