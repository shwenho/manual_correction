#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import re
import string
import numpy as np
import random 

nparts = 4

print(2**32)

k=16

asize = old_div(2**32,nparts)
print(asize)

nn1= k*3
n1m = np.zeros(nn1,dtype=np.dtype('u4'))
print(n1m)
n1m[0]=1
n1m[1]=2
n1m[2]=3
for i in range(1,k):
    n1m[3*i  ]=n1m[3*(i-1)  ]<<2
    n1m[3*i+1]=n1m[3*(i-1)+1]<<2
    n1m[3*i+2]=n1m[3*(i-1)+2]<<2

def neighbors1(i):
    a=[]
    for j in range(k*3):
        a.append(i^n1m[j])
    return a

mers = {}

bcf = "/mnt/nas/projects/array/detroit/barcodes-1224.90k.p5.25bp.txt"

tt = string.str.maketrans("ACTG","TGAC")

def rc(s):
    s = s.translate(tt)
    return s[::-1]

k=16
bases=["A","C","T","G"]
bits = {"A":0, "C":1, "T":2, "G":3}

def int2seq(x):
    s=""
    for i in range(k):
        b =  x & 3
        x = x>>2
        s = s+bases[b]
    return s

def seq2int(s):
    ii=0
#    a= s.split()
    #for b in s.split():
    for i in range(k):
        ii = ii<<2
        b = s[k-1-i]
        #print "#",b,bits[b],ii
        ii += bits[b]
    return ii

f = open(bcf)
while True:
    l = f.readline()
    if not l: break
    c = l.strip().split()
    id = c[0]
    bc = rc(c[1][4:20])
#    print id,bc #,rc(bc),len(bc),l.strip()
    x = seq2int(bc)
    mers[x]=(id,0,0)
#    print bc,mers[x],neighbors1(x)

    xxx = neighbors1(x)
    for i in range(len(xxx)):
        mers[xxx[i]] = (id,1,i)
f.close()

def deletions(s):
    ll = []
    ll.append( (s[1:17],0) )
    for i in range(1,16):
        ll.append(  ( s[:i]+s[i+1:17] ,i) ) 
    ll.append( (s[:16],16) )
#    for l in ll:
#        print s,l
    return(ll)

def insertions(s):
    ll = []
    ll.append( ("A"+s, 0))
    for i in range(1,16):
        ll.append(( s[:i]+"A"+s[i:] ,i))
    return(ll)

while True:
    l = sys.stdin.readline()
    p= l.find("GATC")
    if p==16:
        bc = l[:16]
        if bc.find("N")>=0: continue
        x = seq2int(bc)
        print(bc,x,mers.get(x))
    elif p==17:
        print("##in")
        bc = l[:17]
        vcb = deletions(bc)
        for v in vcb:
#            print v
            x = seq2int(v[0])
            if x in mers:
                merss = mers.get(x)
                print("in:",merss[0],merss[1],bc)
                print(bc,v,x,mers.get(x),"#in")
        print("<<#")
    elif p==15:
        print("##del")
        bc = l[:15]
        vcb = insertions(bc)
        for v in vcb:
#            print v
            x = seq2int(v[0])
            if x in mers:
                print(bc,v,x,mers.get(x),"#del")
        print("<<#")
#        matches = []
        



