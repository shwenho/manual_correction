#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse


if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
     parser.add_argument('-L','--length',default=False,type=int,help="File containing lenghts.")

     args = parser.parse_args()
     if args.progress: print("#",args)

     while True:
          l=sys.stdin.readline()
          if not l: break
          id,leng = l.strip().split()
          leng=int(leng)
          for i in range(1+old_div(leng,args.length)):
               print(id,i*args.length)
