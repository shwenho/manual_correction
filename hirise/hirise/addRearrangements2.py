#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import map
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx
import hirise.mapper
import pickle as pickle
import re

def log(s):
    print(s)

#['Scaffold1', 'CONTIG1', '+isotig2133903', '1', '105', '16.5636363636364']
#['Scaffold1', 'GAP1', '57', '14']
#['Scaffold1', 'CONTIG2', '+isotig1165818', '163', '348', '33.0517647058824']
#['Scaffold1', 'GAP2', '-22', '1']
#['Scaffold1', 'CONTIG3', '+isotig1090484', '327', '475', '19.6567676767677']
#['Scaffold1', 'GAP3', '31', '11']
#['Scaffold1', 'CONTIG4', '+isotig1523405', '507', '676', '38.6750833333333']
#['Scaffold2', 'CONTIG1', '+isotig2311227', '1', '158', '46.1021296296296']
#['Scaffold2', 'GAP1', '169', '5']
#['Scaffold2', 'CONTIG2', '-isotig2268088', '328', '476', '38.3435353535354']

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--map',required=True)
    parser.add_argument('--outmap',required=True)
    parser.add_argument('-d','--debug',default=False,action="store_true")

    nodes={}

    args = parser.parse_args()

    rl = sys.getrecursionlimit()
#    print rl

    rl = sys.setrecursionlimit(50000)

    if args.map:
        mapp = pickle.load( open(args.map)    )

#    for r in mapp.roots:
#        print "root",r

    ls=False
    ss=-1
#    mapp.obsoleted={}

    newLocus=False
    used_loci={}
    breaks_to_make=[]
    joins_to_make=[]
    break_locations={}
    end_nodes={}
    originals=tuple()
    scaffold_lines={}
    configs=[]
    ends={}
    breaks=[]
    locus=""
    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=='#': continue
        c = l.strip().split('\t')

        #frozen:166.81622947-8931.60413858-8764.78790911[(6, 8), (3, 5), (1, 4)][['scaffold_364', 2727], ['scaffold_2277', 1318], ['scaffold_909', 29600]][['scaffold_2277', 1], ['scaffold_2277', 1318], ['scaffold_2277', 1318], ['scaffold_2277', 10875], ['scaffold_364', 1], ['scaffold_364', 2727], ['scaffold_364', 2727], ['scaffold_364', 1196552], ['scaffold_909', 1], ['scaffold_909', 29600], ['scaffold_909', 29600], ['scaffold_909', 132339]][6, 8, 3, 5, 1, 4][0, 0, 1]


        if c[0]=="frozen:" and float(c[1])>0.0 :


            dL,L0,L = list(map(float,c[1:4]))
            joins = eval(c[4])
            breaks=list(map(tuple,eval(c[5])))
            ends=list(map(tuple,eval(c[6])))
            used_ends=eval(c[7])
            used_breaks=eval(c[8])
            locus=eval(c[9])

            newLocus=True
            for lo in locus:
                if lo in used_loci:
                    newLocus=False
            if not newLocus: 
                print("skipping",locus)
                continue

            for lo in locus:
                used_loci[lo]=True

            implement=joins
            print(implement)
            break_products={}

            print("used breaks:",used_breaks)
            for b in used_breaks:
                
                scaffold,x = breaks[b]
                scaffold_node = mapp.find_node_by_name(scaffold)
                print("find break position in original coords:",scaffold,x,scaffold_node)
                mappedX = mapp.find_original_coord(scaffold_node,x,50)
                break_locations[scaffold,x] = mappedX
                ends_produced = [ i for i in range(len(ends)) if breaks[b]==ends[i] ]            
                breaks_to_make.append(((scaffold,x),"{}.{}".format((scaffold,x),ends_produced[0]),"{}.{}".format((scaffold,x),ends_produced[1])))
                for e in ends_produced: break_products[e]=True
            for p in joins:
                if len(p)==1: continue
                for e in p:
                    if not e in break_products:
                        scaffold_node = mapp.find_node_by_name(ends[e][0])
                        if ends[e][1]==1: end_nodes[ "{}.{}".format(ends[e],e) ]= scaffold_node.root
                        else:             end_nodes[ "{}.{}".format(ends[e],e) ]= scaffold_node.leaf
                joins_to_make.append(("{}.{}".format(ends[p[0]],p[0]),"{}.{}".format(ends[p[1]],p[1])))


    for b in breaks_to_make:
        breakl = break_locations[b[0]]
        print("Xbreak:",b,breakl,type(breakl[0]),type(breakl[1]))

        if len(breakl)>2 and breakl[2]=="gap":
            end_nodes[b[1]],end_nodes[b[2]] = breakl[0],breakl[1]
            mapp.unjoin(end_nodes[b[1]],end_nodes[b[2]])
#        elif breakl[2]=="gap-":
#            end_nodes[b[1]],end_nodes[b[2]] = breakl[1],breakl[0]
#            mapp.unjoin(end_nodes[b[1]],end_nodes[b[2]])
        else:
            end_nodes[b[1]],end_nodes[b[2]] = mapp.add_oriented_break(*breakl)
        print(end_nodes[b[1]],end_nodes[b[2]])
#                        break_locations[ends[p[0]]] = mapped2
#                        breaks_to_make.append((ends[p[0]],"{}.{}".format(ends[p[0]],p[0]),"{}.{}".format(ends[p[1]],p[1])))

        #break_locations[ends[p[0]]] = mapped2
        #breaks_to_make.append((ends[p[0]],"{}.{}".format(ends[p[0]],p[0]),"{}.{}".format(ends[p[1]],p[1])))

        #print 

    for b in joins_to_make:
        print("Xjoin:",b,end_nodes[b[0]],end_nodes[b[1]])
        mapp.add_join( end_nodes[b[0]],end_nodes[b[1]],1000 )

    mapp.write_map_to_file(args.outmap)


