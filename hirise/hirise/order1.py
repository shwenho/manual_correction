#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
import pysam
import sys
import math
import networkx as nx

debug=False


def shave_round(G):
    leaves=[]
    for n in G.nodes():
        print("#",n, G.degree(n),G.degree(n)==1)
        if G.degree(n)==1:
            leaves.append(n)
#    print leaves

    round=1
    print("#",leaves)
    boundary=list(leaves)
    next=[]
    done=[]
    r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next):
                    next.append(nn)
#                    r[nn]=round
        for b in next: 
            if len( [nn for nn in G.neighbors(b) if nn not in r] )==1: r[b]=round
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if n in r: boundary.append(n)
#        next=[]
    for n in G.nodes():
        if n not in r: r[n]=round
    return r



def printdot(g,c,n,ll,bh,annot):
#    print ll

    if bh:
        sb=[]
        for cc in c:
            bhi = bh.get(cc,[0,0,0,0,0,0])
            sb.append( ( bhi[1],old_div((float(bhi[3])+float(bhi[4])),2.0),bhi[2],cc ) )
        sb.sort()

    

    gg = nx.subgraph(g,c)
    f=open("%d.txt" % (n), "wt")
    nn=1
    lab0={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]
#        lab0[d]=lab0.get(d,nn)
        lab0[d]=lab0.get( d, float(ll.get(d,0)))
        print(x,d,p,lab0[d])
        nn+=1
    lab={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]

        bhi = bh.get(x,[0,0,0,0,0,0])
        lab[x]="{:.1f} {}{}\\n{:.2f}-{:.2f}".format( old_div(lab0.get(d,0.0),1000), bhi[1],bhi[2],old_div(float(bhi[3]),1.0e6),old_div(float(bhi[4]),1.0e6))

    f.write( "graph G {\n")
#    f.write( "node [margin=0 fontcolor=blue fontsize=32 width=0.5 shape=circle style=filled]")
    f.write( "node [margin=0 fontsize=6 shape=box];\n")
    f.write( "edge [ fontsize=6 ];\n")

    for x in list(lab.keys()):
        f.write( "{0} [label=\"{1}\"] ; \n".format(x,lab[x]) )

    if bh:
        last=False
        lastx=0.0
        lastc=0
        for c in sb:
            if last and c[0]==last and (c[1]-lastx)<1000000:
                f.write("\t \"{}\" -- \"{}\" [weight=2] ;\n".format(lastc,c[-1]))
            last=c[0]
            lastx=c[1]
            lastc=c[-1]

    for e in gg.edges():
#        f.write( "\t\"%s\" -- \"%s\";\n" % (lab[e[0]],lab[e[1]]) ) #,gg[e[0]][e[1]]['weight'])
        color="black"
        if e in annot: color="red"
        f.write( "\t \"%s\" -- \"%s\" [label=\"%d\" weight=1 color=%s];\n" % ( e[0],e[1],int(abs(gg[e[0]][e[1]]['weight'])),color))

    f.write( "}\n")


def order_score(tlist,g):
    #
    sc = 0.0
    for i in range(len(tlist)):
        for j in range(i+1,len(tlist)):
            if g.has_edge(tlist[i],tlist[j]):
                sc -= g[tlist[i]][tlist[j]]['weight']*(j-i)
    return(sc)

context_len = 6
def find_best_position(singleton,string,pair_pos,g):
    first = max(0,pair_pos - context_len)
    last  = min(len(string),pair_pos + context_len)
    
#    sg = nx.subgraph(g,[singleton]+)

    score=[]
    for i in range(first,last+1):
        test_list = string[:i] + [singleton] + string[i:]
        if debug: print(i,test_list)
        sc = order_score(test_list,g)
        score.append((sc,i))
        
#    score.sort(reverse=True)    
    score.sort()
    if debug: print(score)
    return score[0][1]

def intercalate_strings(G,strings,skip):

    if skip:
        for s in strings:
            if len(s)>1:
                print("y:", s)
        return

    #print "strings:",strings
    sl={}
    singletons=[]
    string_index={}
    pos={}
    n=0
    for s in strings:
        #print "x",s
        l = len(s)
        m=0
        for c in s:
            sl[c]=l
            string_index[c]=n
            pos[c]=m
            if l==1:
                singletons.append(c)
            m+=1
        n+=1
    sg = nx.subgraph(G,list(sl.keys()))

    placers=[]
    for s in singletons:
        for e in sg.edges(s):
            if e[0]==s:
                if sl[e[1]]>1:
                    placers.append((sg[e[0]][e[1]]['weight'] , e[0],e[1]))
            else:
                if sl[e[0]]>1:                    
                    placers.append((sg[e[0]][e[1]]['weight'] , e[1],e[0]))
    placers.sort()
    
    placed={}
    for e in placers:
        st=False
        
        singleton = e[1]
        if singleton not in placed:
            st = string_index[e[2]]

            pair_pos = strings[st].index(e[2])
            string = strings[st]

            if debug: print(e, sl[e[1]], sl[e[2]], st, strings[st].index(e[2]), strings[st])
            best_position = find_best_position(singleton,string,pair_pos,sg)
            strings[st] = string[:best_position] + [singleton] + string[best_position:]
            placed[singleton]=True
    for s in strings:
        if len(s)>1:
            print("y:", s)
        else:
            if not placed.get(s[0],False):
                print("y:", s)


def n_neighborhood(G,n,k):
    q=[n]
    l={}
    l[n]=0
    r=[]
    while len(q)>0:
        n=q.pop(0)
        r.append(n)
        for nn in G.neighbors(n):
#            print q,[l[i] for i in q]
            l[nn] = min(l.get(nn,10000), l[n]+1)
            if (not nn in q+r) and (l[nn]<=k):
                q.append(nn)
    return set(r)


def annotate_edges0(t,G,node_list):
    an={}
    nn= len(list(t.edges()))
    i=0.0
    for a,b in t.edges():

        i+=1.0
        n1 = n_neighborhood(t,a,5)
        n2 = n_neighborhood(t,b,5)
        
        neigh = n1.union(n2)
        sg = nx.subgraph(G,neigh)
        sg.remove_edge(a,b)
        if 1< len(nx.connected_components(sg)): 
            an[a,b]="local_bridge"
            an[b,a]="local_bridge"
    return an

def independent_path(G,a,b,k,t):
    q=[a]
    l={}
    l[a]=0
    r=[]
    while len(q)>0:
#        print a,b,G[a][b],q,r
        n=q.pop(0)
        r.append(n)
        for nn in G.neighbors(n):
            if (n==a and nn==b) or (n==b and nn==a): continue
            if G[n][nn]['weight']>-t: continue
            if nn==b: return True
#            print q,[l[i] for i in q]
            l[nn] = min(l.get(nn,10000), l[n]+1)
            if (not nn in q+r) and (l[nn]<=k):
                q.append(nn)
    return False
    
def annotate_edges(t,G,node_list):
    an={}
#    nn= len(list(t.edges()))
#    i=0.0
    for a,b in t.edges():

        if not independent_path(G,a,b,4,2): 
            an[a,b]="local_bridge"
            an[b,a]="local_bridge"
            
    return an

def linearize(G,c,component_index,rounds=2,save_trees=False,length_dict={},hit_info={}):
    #        print c
    #        print t
    node_list = list(c)
    deleted_hair_edges=[]
    if debug: print("component_index",component_index)

    for iternx in range(1):
        gg = nx.subgraph(G,node_list)
        t= nx.minimum_spanning_tree(gg)

        edge_annotations = annotate_edges(t,gg,node_list)

        if save_trees: printdot(t,node_list,component_index,length_dict,hit_info,edge_annotations)

        saved_links={}
        deleted_hair_edges=[]
#        done=False
        itern=0
        while itern<rounds:
            itern+=1
            #print "iter:",itern
#            done = True
            for n in node_list:
                d = t.degree(n)

                #print n,d
                if d==1:
                    e_to_remove=[]
                    for e in t.edges([n]):
                        e_to_remove.append(e)
#                        done=False
                        if e[0]==n:
                            #                            saved_links[n]=e[1]
                            deleted_hair_edges.append(( t[e[0]][e[1]]['weight'], e[0],e[1] ))
                        else:
                            deleted_hair_edges.append(( t[e[0]][e[1]]['weight'], e[1],e[0] ))

                            #                            saved_links[n]=e[0]
                    #print e_to_remove
                    t.remove_edges_from(e_to_remove)


        branch_nodes = []
        for n in node_list:
            d = t.degree(n)
            #print n,d
            if d>2:
                branch_nodes.append(n)
        for n in branch_nodes:
            node_list.remove(n)
            print("xz: remove",n)

            e_to_remove=[]
            for e in t.edges([n]):
                e_to_remove.append(e)
#                    done=False
#                #print "branch:",e_to_remove
            t.remove_edges_from(e_to_remove)

    deleted_hair_edges.sort()

    for e in deleted_hair_edges:
        if t.degree(e[2])==1:
            t.add_edge(e[1],e[2])

    n=1
    stringsx=[]
    for cc in nx.connected_components(t):
        cc.sort(key=lambda x:t.degree(x))
        h = cc[0]
        ll = list(nx.dfs_preorder_nodes(t,h))
        stringsx.append(ll)
#        print component_index,len(ll),ll
        n+=1

    for n in branch_nodes:
        stringsx.append([n])

    return (stringsx)


if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-q','--minquality',default=10.0,type=float)
    parser.add_argument('-t','--threshold',default=3.0,  type=float)
#    parser.add_argument('-s','--strings',required=True)
    parser.add_argument('-H','--head',default=False,type=int)
    parser.add_argument('-D','--savetreedots',default=False,action='store_true')
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-I','--nointerc',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-M','--maxdegree',default=False,type=int)
    parser.add_argument('-m','--minlength',default=500,type=int)

    parser.add_argument('-b','--besthits')
    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')



    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    f = open(args.lengths)
    ll={}
    while True:
        l = f.readline()
        if not l: break
        if l[0]=="#": continue

        c=l.strip().split()
        ll[c[0]]=int(c[1])

    f.close()

    besthit={}
    if args.besthits:
        f = open(args.besthits)
        while True:
            l = f.readline()
            if not l: break
            
            if not l[:5]=="best:": continue
            c=l.strip().split()
            besthit[c[1]]=c[2:]
#            print c[1],besthit[c[1]]
        f.close()

    G=nx.Graph()
    SG=nx.Graph()

    if args.edgefile:
        f = open(args.edgefile)
    else:
        f=sys.stdin
    while True:
        l = f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        u,v,w = c[0],c[1],float(c[2])
        if ll[u]>=args.minlength and ll[v]>=args.minlength:
            G.add_edge(u,v,weight=-w)
            if w >= args.threshold:
                SG.add_edge(u,v,weight=-w)
    if args.edgefile:
        f.close()

#    G0 = nx.read_edgelist(sys.stdin,data=(('weight',float),))
    print("Done reading edgelist")

#    G  =nx.Graph( [ (u,v,{'weight': -d['weight']}) for u,v,d in G0.edges(data=True) if  ll[u]>=args.minlength and ll[v]>=args.minlength ] )
#    SG =nx.Graph( [ (u,v,d) for u,v,d in G.edges(data=True) if d['weight']<=(-args.threshold)] )
#    for u,v,d in SG.edges(data=True):
#        print u,v,d

    bad_nodes=[]
    total_discarded_length=0
    total_discarded_length1=0
    n_discarded1=0
    n_discarded2=0
    for n in SG.nodes():
        print("dg:", SG.degree(n))
        if SG.degree(n)>args.maxdegree:
            n_discarded1+=1
            bad_nodes.append(n)
            total_discarded_length += ll[n]
            print("discard:",n,ll[n],SG.degree(n))
            for nn in SG.neighbors(n):
                if SG.degree(nn)==1:
                    n_discarded2+=1
                    total_discarded_length1+=ll[nn]
    for n in bad_nodes:
        e_to_remove=[]
        for e in SG.edges([n]):
            e_to_remove.append(e)
        SG.remove_edges_from(e_to_remove)
        
    print("total_discarded_length",n_discarded1,n_discarded2,old_div(float(total_discarded_length),1.0e6),old_div(float(total_discarded_length1),1.0e6),old_div(float(total_discarded_length+total_discarded_length1),1.0e6))

    strings = []
    n=1
    for c in nx.connected_components(SG):
#        t= nx.minimum_spanning_tree(gg)
        strings.append(linearize(G,c,n,save_trees=args.savetreedots,length_dict=ll,hit_info=besthit))
        n+=1
        if args.head and n>args.head:
            break

    last_group=0

    for s in strings:
        intercalate_strings(G,s,args.nointerc)


