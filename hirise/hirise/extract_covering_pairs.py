#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import map
from builtins import range
from builtins import object
from past.utils import old_div
import pysam
import sys
import hirise.mapper
from collections import deque
#import heapq
from itertools import chain
import bisect

from polygon_area_intersector import segments_intersect
from polygon_area_intersector import Polygon,Intersector

class LastRegion(Exception):
    pass

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

if __name__=="__main__":
    import sys

#    print " ".join(sys.argv)

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-F','--fig',default=False)
    parser.add_argument('-r','--region',default=False)
    parser.add_argument('-R','--contiglist',default=False)
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-f','--featurefile',required=True)
    parser.add_argument('-s','--savegraph',default=False)
    parser.add_argument('-S','--restoregraph',default=False)
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('-C','--clusterfilt',default=False,action="store_true")
    parser.add_argument('-K','--clusterThresh',default=10,type=int)
    parser.add_argument('-W','--clusterWindow',default=5000,type=int)
    parser.add_argument('-Z','--clusterLog')
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-L','--maxinsert',default=150000,type=int)
    parser.add_argument('-w','--binwidth',default=10000,type=int)
    parser.add_argument('-B','--breaks',default=False)
#parser.add_argument('-b','--bamfile',required=True,action="append",default=[])
    parser.add_argument('-M','--min_insert',default=1000,type=int)
    parser.add_argument('-e','--endsonly',default=False,action="store_true")
    parser.add_argument('-E','--endwindow',default=50000,type=int)
    parser.add_argument('-D','--dotfile',default=False)
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('-A','--trackarea',default=False,action="store_true")
    parser.add_argument('-c','--cutoff',default=(old_div(200.0,1.0e13)),type=float)
    parser.add_argument('-m','--mapq',default=(3.0),type=float)
    parser.add_argument('-I','--ignoreDuplicates',default=False,action="store_true")
    parser.add_argument('--log_breaks',default=False,action="store_true")
    parser.add_argument('--min_total_coverage',default=1,type=int)
    parser.add_argument('--map_file',default=False)
    parser.add_argument('--lengths',default=False)

    nodes={}

    args = parser.parse_args()

    cluster_out =sys.stdout
    if args.clusterLog:
        cluster_out=open(args.clusterLog,"wt")

    figfile=False
    if args.fig:
        figfile = open(args.fig,"w")

    contiglist=False
    if args.contiglist:
        contiglist=deque()
        f=open(args.contiglist)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            contiglist.append(l.strip())
        f.close()

    #print contiglist
    ll={}
    if args.lengths:
        f=open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            c=l.strip().split()
            ll[c[0]]=int(c[1])
        f.close()

    if not args.breaks:        
        L=args.maxinsert
        diagonal_width = args.binwidth
        n_diagonals = old_div(L, diagonal_width)
    else:
        breaks = list(map(int,args.breaks.split(",")))
        n_diagonals = len(breaks)-1
        L = max(breaks)
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )
  
    sam=pysam.Samfile(args.bamfile,"rb")
    snam={}
    mapp = False
    if args.map_file:
        import pickle as pickle
        mapp = pickle.load(open(args.map_file))

    else:
        h = sam.header
        seqs=h['SQ']
        slen=[ s['LN'] for s in seqs ]
        ll=slen
#        snam = {}
        for i in range(len(seqs)):
            snam[i] = seqs[i]['SN']
#        snam=dict([ s['SN'] for s in seqs ])
#        mapp=mapper.map_graph( snam, slen )

    if args.progress: log( "opened %s" % args.bamfile )
    
    features = []
    f=open(args.featurefile,"rt")

    if args.region:
        xxx = args.region.split(':')
        if len(xxx)>1:
            xx = xxx[1].split('-')
            print(xxx,xx)
            x = int(xx[0])
            y = int(xx[1])
        else:
            x = 0
            y = ll[xxx[0]]
        #        if (not args.region) or ():
        ch=False
        while True:
            l=f.readline()
            if not l:
                break
            if l[0]=="#":
                ch=l.strip().split()[1]
                continue
            
            c=l.strip().split()
#            print xxx[0],ch
            if ch==xxx[0]:
                z=int(c[1])
#                print z
                if z>=x and z<=y:
                    features.append(list(map(int,c[:3])))

    else:
        while True:
            l=f.readline()
            if not l:
                break
            if l[0]=="#":
                continue
            c=l.strip().split()
            if c[0][0] in ['0','1','2','3','4','5','6','7','8','9']:
                features.append(list(map(int,c[:3])))
            else:
                features.append((c[0],int(c[1]),int(c[2])))

    features.sort()
#    if args.debug: print features

    h=sam.header

    n=0
    ne=0
    hist={}
    nr=0

    left_cursor = 0
    right_cursor= 0
    center_cursor=0
    current_seq = 0

    all_fore_queue=deque()
    all_aft_queue=deque()
    all_queue=deque()
    max_len=150000

    aln = False

    class MaskIter(object):
        def __init__(self,masker,ch,x):
            self.m = masker
            self.x = x
            self.ch = ch

        def __iter__(self):
            return self

        def __next__(self):
            m=self.m
            m.cache_up_to(self.x)

    def intervals_overlap(xxx_todo_changeme, xxx_todo_changeme1):
        (a,b) = xxx_todo_changeme
        (c,d) = xxx_todo_changeme1
        if (a<c) and (c<b): return True
        if (a<d) and (d<b): return True
        if (c<a) and (a<d): return True
        if (c<b) and (b<d): return True
        return False

    def interval_overlaps_point(xxx_todo_changeme2,c):
        (a,b) = xxx_todo_changeme2
        if (a<c) and (c<b): return True
        return False

    class Rectangle(object):
        """A class to represent rectanular regions of the two-point genome matrix.  Rectangles with r.vertical true are ones that can be extended 'vertically' (negative x direction) witout changing the meaning; those with r.horizontal can be extended 'horizontally' (positive y direction)"""

        def __repr__(self):
            return str(self.vertices)

        def __init__(self,p1,p2,p3,p4,vertical=False,horizontal=False):
            self.vertices=(p1,p2,p3,p4)
            
            x = set( [p[0] for p in self.vertices ] )
            y = set( [p[1] for p in self.vertices ] )
            if not len(list(x))==2:
                raise Exception("rectangle with too many x-coordintes" + str(self.vertices))
            if not len(list(y))==2:
                raise Exception("rectangle with too many y-coordintes" + str(self.vertices))

            self.edges=((p1,p2),(p2,p3),(p3,p4),(p4,p1))
            self.i=0
            self.vertical=vertical
            self.horizontal=horizontal

            self.maxmin=False
            self.set_maxmin()
            self.a = (self.ymax-self.ymin)*(self.xmax-self.xmin)

        def area(self):
            return self.a

        def asy(self,scale=1.0,pic=False):
            s=""
            if pic:
                s+= "fill(%s,t*( " %(pic)
            else:
                s+= "fill(t*( "
            s += " -- ".join( [ "(%f,%f)" % (x[0]*scale,x[1]*scale) for x in self.vertices ]  )
            s += " -- cycle "
            s += "),f);"
            return s

        def isLeft(self,v1,v2):
            if not self.maxmin:
                self.set_maxmin()
            if self.ymax < min([x[1] for x in (v1,v2)]) :
                return True
            else:
                return False

        def isRight(self,v1,v2):
            if not self.maxmin:
                self.set_maxmin()
            if args.debug: print("isRight?",max([x[1] for x in (v1,v2)]),self.ymin,max([x[1] for x in (v1,v2)]) < self.ymin)
            if max([x[1] for x in (v1,v2)]) < self.ymin :
                return True
            else:
                return False

        def isBelow(self,v1,v2):
            if not self.maxmin:
                self.set_maxmin()
            if args.debug: print("isBelow?",max([x[0] for x in (v1,v2)]),self.xmin,max([x[0] for x in (v1,v2)]) < self.xmin)
            if max([x[0] for x in (v1,v2)]) < self.xmin :
                return True
            else:
                return False

        def set_maxmin(self):
            self.xmin = min( [ p[0] for p in self.vertices ] ) 
            self.xmax = max( [ p[0] for p in self.vertices ] ) 
            self.ymin = min( [ p[1] for p in self.vertices ] ) 
            self.ymax = max( [ p[1] for p in self.vertices ] ) 
            self.maxmin=True

        def __iter__(self):
            return self.vertices.__iter__()

        def overlaps_rectangle(self,r):
            """Tests whether rectangle overlaps with other rectangle"""
            
            if ( intervals_overlap( (self.xmin,self.xmax),(r.xmin,r.xmax) ) ) and (  intervals_overlap( (self.ymin,self.ymax),(r.ymin,r.ymax) )  ) :
                return True
            return False

        def encloses_any_vertices(self,r):
            for p in r:
                if interval_overlaps_point((self.xmin,self.xmax),p[0]) and interval_overlaps_point((self.ymin,self.ymax),p[1]):
                    return True
            return False

        def overlaps_segment(self,p1,p2):
            """ for testing whether the leading or trailing edge of a band overlaps a masked box. """
            if self.encloses_any_vertices((p1,p2)):
                return True

            if self.xmax < min((p1[0],p2[0])):
                return False
            if self.xmin > max((p1[0],p2[0])):
                return False

            if self.ymax < min((p1[1],p2[1])):
                return False
            if self.ymin > max((p1[1],p2[1])):
                return False

            for e in self.edges:
                if segments_intersect(e,(p1,p2)):
                    return True
            return False

        def intersect(self,r):
            """ Take two overlapping rectangles and return a set of non-overlapping rectangles.  WARNING:  application-specific assumptions about the shapes of these rectangles are made -- not a general routine. """
            if not self.overlaps_rectangle(r):
                if args.debug: print("no intersection even necessary??")
                return([self,r])
            while self.encloses_any_vertices(r):
                # this isn't for general rectangles:  extend the bands past where they were cutoff before  i.e. make minx smaller for the vertical band, and maxy bigger for the horizontal one.
                if self.vertical:
                    x0=r.minx
                    p1,p2,p3,p4 = self.vertices
                    self.vertices=((x0-1,p1[1]),(x0-1,p2[1]),p3,p4)
                    self.set_maxmin()
                    
                if self.horizontal:
                    y0=r.maxy
                    p1,p2,p3,p4 = self.vertices
                    p2 = (p2[0],y0+1)
                    p3 = (p3[0],y0+1)
                    self.vertices=(p1,p2,p3,p4)
                    self.set_maxmin()
                    
                if r.vertical:
                    x0=self.minx
                    p1,p2,p3,p4 = r.vertices
                    r.vertices=((x0-1,p1[1]),(x0-1,p2[1]),p3,p4)
                    r.set_maxmin()
                    
                if r.horizontal:
                    y0=self.maxy
                    p1,p2,p3,p4 = r.vertices
                    p2 = (p2[0],y0+1)
                    p3 = (p3[0],y0+1)
                    r.vertices=(p1,p2,p3,p4)
                    r.set_maxmin()
                    
            if True:
                ## these two rectangles overlap like two long strips, away from the ends.  Break them into five pieces...
                #         a -----b
                #         |      |
                #   c-----d------e---------f
                #   |     |      |         |
                #   g-----h------i---------j
                #         |      |
                #         |      |
                #         k------l


                if ( r.xmax - r.xmin ) > ( self.xmax-self.xmin ) : 
                    tall_one = self
                    wide_one = r
                else:
                    tall_one = r
                    wide_one = self
                c = ( wide_one.xmin, wide_one.ymin )
                g = ( wide_one.xmin, wide_one.ymax )

                f = ( wide_one.xmax, wide_one.ymin )
                j = ( wide_one.xmax, wide_one.ymax )

                a = ( tall_one.xmin, tall_one.ymin )
                d = ( tall_one.xmin, wide_one.ymin )
                h = ( tall_one.xmin, wide_one.ymax )
                k = ( tall_one.xmin, tall_one.ymax )

                b = ( tall_one.xmax, tall_one.ymin )
                e = ( tall_one.xmax, wide_one.ymin )
                i = ( tall_one.xmax, wide_one.ymax )
                l = ( tall_one.xmax, tall_one.ymax )

                wide_left = Rectangle(c,d,h,g)
                wide_right = Rectangle(e,f,j,i,horizontal = wide_one.horizontal)
                tall_top = Rectangle(a,b,e,d, vertical=tall_one.vertical)
                tall_bottom = Rectangle(h,i,l,k)
                center = Rectangle(d,e,i,h)
                if args.debug: print("new rectangles:", [wide_left,wide_right,tall_top,tall_bottom,center])

                return([wide_left,wide_right,tall_top,tall_bottom,center])


    class Masker(object):
        def __init__(self,features,L,chr=False,x=False):
            self.features = features
            self.L=L
            self.feature_cursor=0
            self.segment_buffer=[]
            self.box_buffer=[]

        def process_feature(self,feature):
            if args.debug: print("process feature",feature)
            if args.debug: print("process feature",self.box_buffer)

#   [[ x increases down the page;  y increases to the right   ]]
#  
#  (a-L,a)-(a-L,b)
#    |      |
#    |      |
#\   |      |
# \  |      |
#  \ |      |
#   \|      |
#  (a,a)--(a,b)--------(a,b+L)
#     \     |           |
#      \    |           |
#       \   |           |
#        \  |           |
#         \ |           |
#        (b,b)---------(b,b+L)
#           \

            """Take the interval to mask, 
            extend it into a range of rows and a range of columns of the position-pair matrix, 
            intersect these matrix boxes with the boxes already in the box buffer, 
            and add them to the box buffer. """
            a,b = feature[1],feature[2]
            L=self.L
            if args.debug: print(a,b,L)
            new_rectangles = [ Rectangle((a,a),(b,a),(b,b),(a,b)), Rectangle( (a-L*2,a),(a,a),(a,b),(a-L*2,b), vertical=True ), Rectangle( (a,b), (b,b),(b,b+L*2), (a,b+L*2), horizontal=True)]
            
            while len(new_rectangles)>0:
                r = new_rectangles.pop()
                if args.debug: print("test for conflicts with ",r)
                overlap_hit=False
                overlappers=[]
                for i in range(len(self.box_buffer)):
                    if args.debug: print(self.box_buffer[i], "overlap with?", r, r.overlaps_rectangle(self.box_buffer[i]))
                    if r.overlaps_rectangle(self.box_buffer[i]):
                        b = self.box_buffer.pop(i)
                        overlap_hit = True
                        break
                if overlap_hit:
                    new_rectangles.extend( r.intersect(b) )
                else:
                    self.box_buffer.append(r)
            if args.debug: 
                print("box buffer:",self.box_buffer)
                
                print("var t=rotate(-90);")
                print("size(1000,1000);")
                print("pen Dotted(pen p=currentpen) {return linetype(new real[] {0,3})+2*linewidth(p);}")
                print("pen v = Dotted();")

                for b in self.box_buffer:
                    print(b.asy())


        def drain_up_to(self,ch,x):
            m=self
            while (m.feature_cursor < len(m.features)) and ( m.features[m.feature_cursor][0] < ch ):
                m.feature_cursor += 1

            while (m.feature_cursor < len(m.features)) and ( m.features[m.feature_cursor][0] == ch ) and (m.features[m.feature_cursor][1] < x  ):
                m.feature_cursor += 1
            
        def cache_up_to(self,ch,x):
            m=self
            while (m.feature_cursor < len(m.features)) and ( m.features[m.feature_cursor][0] < ch ):
                m.feature_cursor += 1

            while (m.feature_cursor < len(m.features)) and ( m.features[m.feature_cursor][0] == ch ) and (m.features[m.feature_cursor][1] < x + m.L ):
                self.process_feature( m.features[m.feature_cursor] )
                m.feature_cursor += 1
            

        def boxes_up_to(self,ch,x):
            return MaskIter(self,ch,x)


    class Pair(object):
        def __init__(self,a,b,name=False,tid=0,rnext=False,pnext=False):
            self.a = a
            self.b = b
            self.d = max(b-a,a-b)
            self.trashed = False
            self.name= name
            self.tid=tid
            self.rnext = rnext
            self.pnext = pnext

        def trash(self):
            self.trashed=True

        def __repr__(self):
            return str( (self.a,self.b,self.name,self.trashed) )

    class Diagonal(object):
        def __init__(self,x,y,L):
            assert (x<y)
            self.min_sep = x
            self.max_sep = y
            self.queue = []
            self.n = 0
            self.internal_masked_area=0.0
            self.mask_pre=deque()     # boxes which overlap the ditaconal should be added to pre as soon as they are known about.
            self.onramp=deque()  #  boxes move to the onramp when they start to overalp the leading edge:  box.ymin < x+max_sep < box.ymax and box.xmin<x < box.xmax 
            self.mask_queue=deque()   #  when a box is above (box.xmax < leading edge x) or left of ( box.ymax < x+ min_sep )  the diagonal's leading edge, move from onrame to here
            self.offramp=deque() #  when a box starts overalping the diagonal's trailing edge, move to offramp.  this happens when ymin < x && ymax > x
                                 #  when a box passes out of the windown entriely, we can forget it:  ymax < x

            self.total_area =   Polygon((( 0,self.max_sep ),(0,self.min_sep),(-self.min_sep,0),(-self.max_sep,0))).area()
            

        def area_of_intersection(self,box):
#      \         \    
#       \   +--b  \
#        \  |  |   \
# \       \ a--+    \       
#  \       \         \
# (x,x)              y-x=max_sep
#    \        
            a = (box.xmax,box.ymin)
            b = (box.xmin,box.ymax)
            if ((a[1]-a[0])>self.min_sep) and ((b[1]-b[0])<self.max_sep):
                return box.area()
                
            else:
                a = Intersector(Polygon((( x,x+self.max_sep ),(x,x+self.min_sep),(x-self.min_sep,x),(x-self.max_sep,x))),Polygon(box.vertices)).area2()
                if args.debug: print("overlap area:",a)
                return a
#                print "not calculating area yet"
#                return 1.0

        def masked_area(self,x):
            a=self.internal_masked_area
            for b in chain(self.onramp, self.offramp):
                
                da=Intersector(Polygon((( x,x+self.max_sep ),(x,x+self.min_sep),(x-self.min_sep,x),(x-self.max_sep,x))),Polygon(b.vertices)).area2()
                #print "da:",da
                a+=da
            if args.debug: print("total a:",a)
            return a

        def unmasked_area(self,x):
            return self.total_area - self.masked_area(x)
            
        def drain_offramp(self,x,L,tid,masker):
            for i in range(len(self.offramp)):
                b = self.offramp.popleft()
                if not b.isLeft((x,x),(x-L,x)):
                    self.offramp.append(b)
                else:
                    if args.debug: print("drain from offramp",b,self,x,"transX")

        def drain_onramp(self,x,L,tid,masker):
            for i in range(len(self.onramp)):
                b = self.onramp.popleft()
                if not b.isLeft( (x,x+self.min_sep),(x,x+self.max_sep) ):
#                if b.overlaps_segment((x,self.min_sep),(x,self.max_sep)):
                    if args.debug: print("keep",b,"in onramp for",self,"at",x)
                    self.onramp.append(b)
                else:
                    if args.debug: print("drain",b,"from onramp for ",self,"at",x,"transX")
                    a = self.area_of_intersection(b)
                    self.mask_queue.append((a,b))
                    self.internal_masked_area += a

        def drain_mask_queue(self,x,L,tid,masker):
            for i in range(len(self.mask_queue)):
                a,b = self.mask_queue.popleft()
                if not b.overlaps_segment((x-self.min_sep,x),(x-self.max_sep,x)):
                    self.mask_queue.append((a,b))
                else:
                    self.offramp.append(b)
                    self.internal_masked_area -= a
                    if args.debug: print("offramp",b,self,x,"transX")

        def prequeue(self,box):
            if intervals_overlap((self.min_sep,self.max_sep), ( min( [ x[1]-x[0] for x in box.vertices ] ) ,  max( [ x[1]-x[0] for x in box.vertices ] ) )):
                if args.debug: print("prequeue",b,self)
                self.mask_pre.append(box)

        def onramp_boxes(self,x,L,tid,masker):
            if args.debug: print("onramp?",self,self.mask_pre)
            for i in range(len(self.mask_pre)):
                b = self.mask_pre.popleft()
                if not (b.isRight(  (x,x+self.min_sep),(x,x+self.max_sep)  ) or b.isBelow( (x,x+self.min_sep),(x,x+self.max_sep) )):
#                if not (( min( x[0] for x in b.vertices )>x ) and ( min( x[1] for x in b.vertices )>x+self.max_sep )) :
#                if self.overlaps(b,x):
                    if args.debug: print("onramp",b,"to",self,x,"transX")
                    self.onramp.append(b)
                else:
                    if args.debug: print("not ready to onramp",b,"to",self,x)
                    self.mask_pre.append(b)

        def overlaps(self,box,x):
            a=Intersector(Polygon((( x,x+self.max_sep ),(x,x+self.min_sep),(x-self.min_sep,x),(x-self.max_sep,x))),Polygon(box.vertices)).area2()
            print(a)
            #a=Intersector(Polygon((( x,x+self.min_sep ),(x,x+self.max_sep),(x-self.max_sep,x),(x-self.min_sep,x))),Polygon(box.vertices)).area()
            if a >0:
                return True
            return False

        def __repr__(self):
            return("%d-%d"%(self.min_sep,self.max_sep))

            
        def enqueue(self,pair):
            if pair.d < self.min_sep:
                return False
            elif pair.d >= self.max_sep:
                return False
            
            self.queue.append(pair)
            self.n+=1

        def drain(self,x,L,tid):
            if args.debug: print("drain queue",self.min_sep, self.n)
            if len(self.queue)==0:
                if args.debug: print("queue already empty")
                return
            while (len(self.queue)>0) and((not self.queue[0].tid == tid) or (self.queue[0].a <  x-L)):
                p=self.queue.pop(0)
                if not p.trashed:
                    self.n-=1
            for p in self.queue:
                if p.a >= x - self.min_sep:
                    break
                if (p.b < x) and (not p.trashed):
                    p.trash()
                    self.n-=1
            if args.debug: print("drain if a<",x-L, "flag if b<",x, "now the count is", self.n, "queue=",self.queue)
            
    class pairGen(object):
        def __init__(self,sam,region=False,mapq=-1.0,coordinate_mapper=False,contiglist=False):
            self.mapp = coordinate_mapper
            self.contiglist = contiglist
            self.regions=False
            #print len(self.contiglist)
            if not region:
                if not contiglist:
                    self.sam_it = sam.fetch(until_eof=True)
                else:
                    if self.mapp:
                        pass

                        self.regions = []
                        print("mapped list:")
                        for contig in self.contiglist:
                            for r in self.mapp.region_list(self.mapp.find_node_by_name(contig)):
                                if features:
                                    fcL=bisect.bisect_left(features,(r[2],r[3]))
                                    fcR=bisect.bisect_right(features,(r[2],r[4]))
                                    # if this region intersects high-depth features, clip, filter of split it up here:
                                    print(fcL,fcR,len(features))
                                    if ((fcL>=0 and fcL<len(features) and features[fcL][0]==r[2]) or (fcR>=0 and fcR<len(features) and features[fcR][0]==r[2]) or ( fcL>=1 and fcL<=len(features) and features[fcL-1][0]==r[2])  or (fcR+1>=0 and fcR+1<len(features) and features[fcR+1][0]==r[2])):
                                        #print "#fi",r,fcL,fcR,features[fcL-1],features[fcL],features[fcR-1],features[fcR]


                                        edge_buffer = [(r[3],1),(r[4],-1)]
                                        while fcL<fcR: #min(fcR,len(features)):
                                            print("fcL:",fcL,len(features),r)
                                            if features[fcL][0]==r[2] :
                                                edge_buffer.append((features[fcL][1],-1))
                                                edge_buffer.append((features[fcL][2],1))
                                            fcL+=1

                                        edge_buffer.sort()
                                        
                                        rs=0
                                        range_start=False
                                        region_buffer = []
                                        for i in range(len(edge_buffer)):
                                            d_rs = edge_buffer[i][1]
                                            if rs==0 and rs+d_rs>0:
                                                range_start = edge_buffer[i][0]
                                            elif rs>0 and rs+d_rs==0:
                                                range_end = edge_buffer[i][0]
                                                region_buffer.append(( "{}:{}-{}".format(r[2],range_start,range_end) ,r[1]))

                                        print("#rb:",r[0],region_buffer)

                                        flipit=r[1]==-1
                                        

                                        
        

                                        if flipit:
                                            for rr in reversed(region_buffer):
                                                self.regions.append((rr[0],-1))
                                        else:
                                            for rr in region_buffer:
                                                self.regions.append((rr[0],1))
                                            
                                    else:
                                        self.regions.append((r[0],r[1]))
                                        
                                else:
                                    self.regions.append((r[0],r[1]))
                        
                        
                        self.next_region()


                    else:
                        self.sam_it = sam.fetch(region=self.contiglist.popleft())
                    
            else:
                print(region)
                self.sam_it = sam.fetch(region=region)
            self.mapq=mapq


        def next_region(self):
            if not self.regions:
                raise LastRegion  
            r = self.regions.pop(0)
            sys.stderr.write("#next region = {} leaving {} more\n".format(r,len(self.regions)))
            if r[1]==1:
                self.sam_it=sam.fetch(region=r[0])
            else:
                self.aln_buffer=[]
                for aln in sam.fetch(region=r[0]):
                    self.aln_buffer.append(aln)
                self.sam_it = reversed(self.aln_buffer)
                sys.stderr.write("#buffered {} alignments\n".format(len(self.aln_buffer)))

        def __iter__(self):
            return self

        def __next__(self):
            try:
                aln= next(self.sam_it)
                #print aln
            except StopIteration as e:
#                print len(self.contiglist)
                #print len(self.contiglist),self.contiglist[:4]

                while True:
                    if self.mapp and self.contiglist:
                        try:
                            self.next_region()
                            aln = next(self.sam_it)
                        except StopIteration as e:
                            continue
                        except LastRegion:
                            raise StopIteration
                        break

                    else:
                        if not len(self.contiglist)>0: raise e
                        try:
#                            if len(self.contiglist)==0: raise e
                            #if not next_region: 
                            self.sam_it = sam.fetch(region=self.contiglist.popleft())
                            aln = next(self.sam_it)
                        except StopIteration as e:
                            continue
                        break

                #print "x:",aln
                #print "y:",aln.next()
            except Exception as e:
                    print("unexpected error",e)
                    print(sys.exec_into()[0])

#            print aln.rnext, "t1"
#            sys.stdout.flush()
            mapped_tid,mapped_pos,mapped_rnext,mapped_pnext=[0,0,1,1]
            if self.mapp:
                mapped_tid,mapped_pos = self.mapp.map_coord(aln.tid,aln.pos)
                mapped_rnext,mapped_pnext = self.mapp.map_coord(aln.rnext,aln.pnext)        
#                print mapp.print_name(mapped_tid), "z1"
#                sys.stdout.flush()

            usable=False
            if ( not self.mapp and ( (not ( aln.tid==aln.rnext)) or aln.tlen>200000 )) or (self.mapp and ( (not mapped_tid == mapped_rnext ) or (abs(mapped_pos-mapped_pnext)>200000))):
                usable=True
            elif ( not self.mapp and ( (aln.pos<=aln.pnext))) or (self.mapp and ( (mapped_pos<=mapped_pnext) )):
                usable=True
                
            while (args.ignoreDuplicates and aln.is_duplicate) or (aln.mapq < self.mapq) or aln.rnext==-1 or not usable :
                try:
                    aln= next(self.sam_it)

                    #print aln.tid,aln.mapq,aln.rnext,aln.pos,aln.pnext
#                    print aln.rnext, "t2"
#                    sys.stdout.flush()
                except StopIteration as e:
                    #print len(self.contiglist),self.contiglist[:4]

                    while True:
                        if self.mapp and self.contiglist:
                        #if self.regions:
                            try:
                                self.next_region()
                                aln = next(self.sam_it)
                            except StopIteration as e:
                                continue
                            except LastRegion:
                                raise StopIteration
                            break
                        else:
                            if not self.contiglist: raise e
                            try:
                                self.sam_it = sam.fetch(region=self.contiglist.popleft())
                                aln = next(self.sam_it)
                            except StopIteration as e:
                                continue
                            break
                        #print aln.tid,aln.mapq,aln.rnext,aln.pos,aln.pnext

                except Exception as e:
                    print("unexpected error",e)
                    print(sys.exec_into()[0])
#                    raise e
                if self.mapp:
                    #print aln.rnext,"t3"
                    mapped_tid,mapped_pos = self.mapp.map_coord(aln.tid,aln.pos)
                    mapped_rnext,mapped_pnext = self.mapp.map_coord(aln.rnext,aln.pnext)
                    #print mapp.print_name(mapped_tid), "z1"
                    #sys.stdout.flush()

                usable=False
                if ( not self.mapp and ( (not ( aln.tid==aln.rnext)) or aln.tlen>200000 )) or (self.mapp and ( (not mapped_tid == mapped_rnext ) or (abs(mapped_pos-mapped_pnext)>200000))):
                #if ( not self.mapp and ((not ( aln.tid==aln.rnext)))) or (self.mapp and ( not mapped_tid == mapped_rnext )):
                    usable=True
                elif ( not self.mapp and ( (aln.pos<=aln.pnext))) or (self.mapp and ( (mapped_pos<=mapped_pnext) )):
                    usable=True

            #print "got one", aln
            

            if ( not self.mapp and ( (not ( aln.tid==aln.rnext)) or aln.tlen>200000 )) or (self.mapp and ( (not mapped_tid == mapped_rnext ) or (abs(mapped_pos-mapped_pnext)>200000))):
            #if ( not self.mapp and ((not ( aln.tid==aln.rnext)))) or (self.mapp and ( not mapped_tid == mapped_rnext )):

                if self.mapp:
                    return( Pair(mapped_pos,mapped_pnext,aln.qname,mapped_tid, mapped_rnext, mapped_pnext ) )
                else:
                    return( Pair( aln.pos,aln.pnext, aln.qname, aln.tid, aln.rnext , aln.pnext ) )

            elif  ( not self.mapp and ( (aln.pos<=aln.pnext))) or (self.mapp and ( (mapped_pos<=mapped_pnext) )):

                if self.mapp:
    #                a,b,c,d = aln.pos,aln.pnext, aln.qname, aln.tid 
    #                print [a,b,c,d]
    #                d,a = self.mapp.map_coord(d,a)
    #                D,A = self.mapp.map_coord(aln.rnext,aln.pnext)

                    #d,a = self.mapp.map_coord(d,a)
                    return( Pair(mapped_pos,mapped_pnext,aln.qname,mapped_tid) )
                else:
                    return( Pair( aln.pos,aln.pnext, aln.qname, aln.tid ) )
    #            return ((aln.tid,aln.pos,aln.pnext,aln.qname))

    if args.region:
        xxx = args.region.split(':')
        if len(xxx)>1:
            xx = xxx[1].split('-')
            x = int(xx[0])
        masker = Masker(features,L,xxx[0],x)

    else:
        masker = Masker(features,L)
        

    pg=pairGen(sam,args.region,args.mapq,mapp,contiglist)

    diagonals = []
    if args.breaks:
        for i in range(1,len(breaks)):
            diagonals.append(Diagonal( breaks[i-1],breaks[i] , L ))

    else:
        for i in range( n_diagonals):
            diagonals.append( Diagonal( i*diagonal_width, (i+1)*diagonal_width ) )

    def queue_pair(p):
        for d in diagonals:
            if d.enqueue(p):
                break

    def update_queues(x,tid):
        for d in diagonals:
            d.drain(x,L,tid)
            d.onramp_boxes(x,L,tid,masker)
            d.drain_onramp(x,L,tid,masker)
            d.drain_mask_queue(x,L,tid,masker)
            d.drain_offramp(x,L,tid,masker)

    def init_start_new_scaffold():
        left_cursor = 0
        right_cursor= 0
        center_cursor=0
        current_seq = -1
        all_fore_queue=[]
        all_aft_queue=[]

    from operator import itemgetter

    def peak_search(links,s1,s2,w=10000,t=10):
    #links starts off sorted by the first element of each pair
        n=len(links)
        j=0
        good_window_edges1=[]
        for i in range(n):  # scan i through the list of x,y pairs, sorted on x
            while links[i][0]-links[j][0]>w: #update j so that it trails i and defines the trailing edge of sliding window of size w
                j+=1
            if i-j+1>=t:  # if the number of links in the window exceeds threshold t
                good_window_edges1.append( (links[j][0],1) )  # record the start of the window as a rising edge
                good_window_edges1.append( (links[i][0],-1) ) # and the end of the window as a falling edge
        good_window_edges1.sort()  # sort the edges into ascending order of position

        merged_windows1=[]
        rs=0
        for i in range(len(good_window_edges1)):  
            rs1=rs+good_window_edges1[i][1]        # compute the running sum of edges.  when this is >0, we're inside a merged window.
            if rs==0 and rs1>0:
                wstart=good_window_edges1[i][0]
            elif rs>0 and rs1==0:
                merged_windows1.append((wstart,good_window_edges1[i][0]))  # when we get to the end of a merged window, add it to the list.
            rs=rs1

        #print "# good windows1:", merged_windows1

        windows1={}
        window_cursor=0
        i=0
        while i<n and window_cursor<len(merged_windows1):  # scan through the links in order, advancing the merged windows as we go, in step.
            while links[i][0] > merged_windows1[window_cursor][1]: 
                window_cursor+=1
                if window_cursor >= len(merged_windows1): break
            if window_cursor >= len(merged_windows1): break
            #print i,links[i],merged_windows1[window_cursor],(links[i][0] >= merged_windows1[window_cursor][0] and links[i][0] <= merged_windows1[window_cursor][1])
            if links[i][0] >= merged_windows1[window_cursor][0] and links[i][0] <= merged_windows1[window_cursor][1]: # for links in the current window, tag them
                windows1[links[i]]=window_cursor
            i+=1

        # now in the other dimension:

        links.sort(key=itemgetter(1))

        j=0
        good_window_edges2=[]
        for i in range(n):
            while links[i][1]-links[j][1]>w:
                j+=1
            #print "window:",i,j,i-j+1,links[j][1],links[i][1],i-j+1>=t
            if i-j+1>=t:
                good_window_edges2.append( (links[j][1],1) )
                good_window_edges2.append( (links[i][1],-1) )
        good_window_edges2.sort()

        merged_windows2=[]
        rs=0
        for i in range(len(good_window_edges2)):
            rs1=rs+good_window_edges2[i][1]
            if rs==0 and rs1>0:
                wstart=good_window_edges2[i][0]
            elif rs>0 and rs1==0:
                merged_windows2.append((wstart,good_window_edges2[i][0]))
            rs=rs1

        #print "# good windows2:", merged_windows2

        windows2={}
        window_cursor=0
        i=0
        while i<n and window_cursor<len(merged_windows2):
            while links[i][1] > merged_windows2[window_cursor][1]: 
                window_cursor+=1
                if window_cursor >= len(merged_windows2): break
            if window_cursor >= len(merged_windows2): break
            if links[i][1] >= merged_windows2[window_cursor][0] and links[i][1] <= merged_windows2[window_cursor][1]:
                windows2[links[i]]=window_cursor
            i+=1
        
        #print "w1:",windows1
        #print "w2:",windows2

        box_counts={}
        for link in links:
            if  link in windows1 and link in windows2:
                box = (windows1[link], windows2[link])
                box_counts[box] = 1 + box_counts.get(box,0)   # for pairs of windows on the two scaffolds, tally the number of links in the "box"
#        print box_counts
        filtered_links= [ link for link in links if link in windows1 and link in windows2 and box_counts[ windows1[link], windows2[link] ] >= t  ]

        if len(filtered_links)>0:
            cluster_out.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(s1,s2,ll[s1],ll[s2],len(filtered_links),filtered_links,merged_windows1,merged_windows2))
            cluster_out.flush()

#        print s1,s2
#        for x,y in [ link for link in links if link in windows1 and link in windows2 and box_counts[ windows1[link], windows2[link] ] >= t  ]:
#            print x,y
#        print "#rejected:", [ link for link in links if not ( link in windows1 and link in windows2 and box_counts[ windows1[link], windows2[link] ] >= t )] 

    break_start = (-2,False)
    last_scaffold=-1

    other_contig_buffer=deque()
    other_contig_counts={}
    other_contig_coords={}
    otc_window=args.clusterWindow
    cluster_window_state=False
    cluster_others=[]

    def report_stats(c,x):
        global break_start
        global last_scaffold
        ns = [ d.n for d in diagonals ] + [ d.unmasked_area(x) for d in diagonals ]
        total_cover = sum(ns[:len(diagonals)])

        if args.log_breaks:
            if not c == last_scaffold:
                break_start = (-2,False)
            if break_start[0]==-2 and total_cover > args.min_total_coverage:
                break_start = (-1,False)
            if total_cover < args.min_total_coverage:
                if break_start[0]==-1:
                    break_start = (c,x)

            if (break_start[0]>-1) and not total_cover < args.min_total_coverage:
                if break_start[0] == c:
                    print("break:",c,break_start[1],x)
                break_start = (-1,False)
            last_scaffold=c

        if args.map_file:
            c = mapp.print_name(c)

#        print other_contig_buffer, other_contig_counts


        for other,count in list(other_contig_counts.items()):
            if count > args.clusterThresh:
                if not other in cluster_others:
                    print("#found a peak to:",other, other_contig_counts[other])
                    # we have entered a peak of links to "other"
                    cluster_others.append(other)
                else:
                    # we are still in a peak of links to "other"
                    pass
#            else:
#                if other in cluster_others:
#                    # we have just left a peak of links to "other"
#                    print "#done with peak to",other, other_contig_counts.get(other,0)
#                    peak_search( other_contig_coords[other], c,other )
#                    pass
#                    del other_contig_coords[other]
#                    cluster_others.remove(other) 

        for other in cluster_others:
            if not other in other_contig_counts or other_contig_counts[other]==0:
    
                print("#done with peak to",other, other_contig_counts.get(other,0))
                if not other in other_contig_coords:
                    print(other_contig_coords)
                    print(cluster_others)
                    print(c)
                    print(other)
                peak_search( other_contig_coords[other], c,other )
                pass
                del other_contig_coords[other]
                cluster_others.remove(other) 

        if False:
            other_contigs = list(other_contig_counts.keys())
            other_contigs.sort(reverse=True,key=lambda x: other_contig_counts[x])
            print("###",len(list(other_contig_counts.keys())),len(list(other_contig_coords.keys())),len(cluster_others))
            print("#",[(i,other_contig_counts[i]) for i in other_contigs])
            print("#",[(i,other_contig_coords.get(i)) for i in other_contigs])
            print("##",cluster_others)

        otlinks = list(other_contig_counts.values())
        if not otlinks:
            otlinks=(0,)
        max_other_links=max(otlinks)

        if max_other_links > args.clusterThresh : 
            cluster_window_state = True
        else:
            cluster_window_state = False

        print("\t".join(map(str, [c,x,total_cover,max_other_links,len(list(other_contig_counts.keys())), len(cluster_others)]+ns)))
        if args.debug:
            for d in diagonals:
                print(d.mask_queue)
                print(d.onramp)
                print(d.offramp)
                print(d.internal_masked_area)
                print(d.masked_area(x))

    print("#" + "\t".join(map(str,["chr","x"]+diagonals)))

    if args.fig:
        figfile.write("var f=orange;\n")
        figfile.write("var t=rotate(-45);\n")
        figfile.write("picture mask;\n")
        figfile.write("picture dots;\n")
        figfile.write("picture lines;\n")
        if args.region:
            xxx = args.region.split(':')
            x,y=0,1000000
            if len(xxx)>1:
                xx = xxx[1].split('-')
                x,y = float(xx[0]),float(xx[1])
            for d in diagonals:
#                figfile.write("draw(lines,t*((%f,%f)--(%f,%f)));\n" % ( (x-d.min_sep)/1000,x/1000 ,y/1000,(y+d.min_sep)/1000))
#                figfile.write("draw(lines,t*((%f,%f)--(%f,%f)));\n" % ( (x-d.max_sep)/1000,x/1000 ,y/1000,(y+d.max_sep)/1000))
                figfile.write("draw(t*((%f,%f)--(%f,%f)));\n" % ( old_div((x-d.min_sep),1000),old_div(x,1000) ,old_div(y,1000),old_div((y+d.min_sep),1000)))
                figfile.write("draw(t*((%f,%f)--(%f,%f)));\n" % ( old_div((x-d.max_sep),1000),old_div(x,1000) ,old_div(y,1000),old_div((y+d.max_sep),1000)))

#    if args.fig:
#        figfile.write("add(mask);\n")
#        figfile.write("add(dots);\n")
#        figfile.write("add(lines);\n")
#    print "#iter"

    last_s=-1
    last_length=0
    for p in pg:

        x= p.a

        if (not p.tid == last_s) and (not last_s ==-1): 
            #print "#new scaffold", last_s, p.tid
            other_contig_counts={}
            other_contig_buffer=deque()
            report_stats(last_s,last_length)
#            other_contig_counts={}
            other_contig_coords={}
            cluster_others=[]
        last_s = p.tid


        if p.rnext:
            c=p.rnext
            if args.map_file:
                c = mapp.print_name(c)

    
            other_contig_buffer.append((p.a,c))
            while len(other_contig_buffer)>0 and ((x-other_contig_buffer[0][0])>otc_window or  (x-other_contig_buffer[0][0])<0 ):
                xx,departing=other_contig_buffer.popleft()
                other_contig_counts[departing]-=1
                if other_contig_counts[departing]==0:
                    del other_contig_counts[departing]
                    if departing in other_contig_coords and not departing in cluster_others :
                        del other_contig_coords[departing]

            for txx in list(other_contig_coords.keys()):
                if not txx in other_contig_counts and not txx in cluster_others:
                    print("wtf?", txx)

            other_contig_counts[c]=other_contig_counts.get(c,0)+1
#            other_contig_coords[c]=other_contig_coords.get(c,heapq()).heappush( (p.pnext,x) ) 
            if not c in other_contig_coords:
                other_contig_coords[c]=[]
            other_contig_coords[c].append( (x,p.pnext))
#            heapq.heappush( other_contig_coords[c] , (p.pnext,x))
#            other_contig_coords[c]=heapq.heappush( other_contig_coords.get(c,[]),  (p.pnext,x) )
#            print "#",p.a,c
            continue

        
        if args.fig:

            if (p.b-p.a) < L:
                figfile.write("dot(t*(%f,%f),blue);\n" % ( old_div(float(p.a),1000) , old_div(float(p.b),1000) ))
                figfile.flush()
            
        masker.cache_up_to(p.tid,x+L)  # this object processes intervals along the genome to be masked into "box" regions of the 2-point space to be masked, and queues them up.  "update queues" needs to be updated to add add boxes that overlap with each diagonal....

        while len(masker.box_buffer)>0:
            b = masker.box_buffer.pop()
            if args.fig: figfile.write(b.asy(0.001)+"\n")
            if args.trackarea:
                for d in diagonals:
                    d.prequeue(b)
        
        queue_pair(p)
        update_queues(x,p.tid)
        report_stats(p.tid,x)
        last_s = p.tid
        if mapp:
            last_length = ll[mapp.print_name(last_s)]
        else:
            last_length = slen[last_s]

    if args.fig:
        figfile.close()


