#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-f','--file')

#374     Scaffold100_1.3         Scaffold117987_1.5      GTTCAAGCGATTCTCCTGC     TTTCACCATGGTGGCCAGG     98
#74      Scaffold100157_1.3      Scaffold223136_1.3      CGGTCCCTCCTGTCCCGGC     TCCCGCCGGGAGTGGCCAG     151
#74      Scaffold100167_1.5      Scaffold15004_1.5       GGGTGACAGAGTAAGACTC     TCCCATTACTGGGCATATA     402
#328     Scaffold100599_1.5      Scaffold68096_1.3       ACATGCCTGTAATCCCAGC     CTCAAAAAAAAAAAAAGTA     128

args = parser.parse_args()
if args.progress: print("#",args)

sep={}
while True:
     l=sys.stdin.readline()
     if not l: break
     c=l.strip().split()
     if not c[5]=="None":
          sep[c[1],c[2]]=int(c[5])

f=open(args.file)
while True:
     l=f.readline()
     if not l: break
     #74      Scaffold101144_1.3      ACTTGGGAGGCTGAGGCAG     Scaffold186332_1.3      TCACTTGAACCCAGGAGGT     ACTTGGGAGGCTGAGGcaggagaatcaCTTGAACCCAGGAGGT
     c=l.strip().split()
     print("\t".join(map(str,[c[1],c[3],len(c[5]),sep.get((c[1],c[3]),-9999),len(c[5])-sep.get((c[1],c[3]),-9999)-19])))
