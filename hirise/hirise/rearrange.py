#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import object
#!/usr/bin/env python3
import sys
import random
import argparse
import subprocess, shlex
import re
import numpy as np

class Pair(object):
    def __init__(self,p1,p2):
        self.p1 = p1
        self.p2 = p2

class Point(object):
    def __init__(self,seq,x):
        self.s=seq
        self.x=x

    def __repr__(self):
        return str((self.s,self.x))

class Region(object):
    def __init__(self,seq1,seq2,vertices,rot,new_origin):
        self.s1=seq1
        self.s2=seq2
        self.vertices = vertices
        self.rot = rot
        self.new_origin = new_origin

    def contains(self,p):
        if not (p.p1.s == self.s1 and p.p2.s == self.s2): return False
        return polygon_contains(self.vertices,p)

    def transf(self,p):
        return self.new_origin + self.rot( p - self.vertices[0] )

class Rearrangement(object):
#    def __init__(self,regions):
#        self.regions = regions

    def transform_pair(self,p):
        pp=p
        for r in self.regions:
            if r.contains(p):
                pp = r.transf(p)
                break  # this means the regions can overlap, and the first match will be the one that's used.  Letting the regions overlap helps keep definitions simple, for example with inversios.
        return pp


## but because we need the breakpoints to be mobile...  ?? use closures?


class Inversion(Rearrangement):
    def __init__(self,breakpoints):
        s = breakpoints[0].s
        self.regions = [ Region(s,s,[],flipX,), Region() ]
        

def same_fragment(l):
    if len(l)<2: return True
    if not l[0].s == l[1].s: return False
    i=2
    while i < len(l):
        if not l[0].s == l[i].s: return False
    return True


def all_rearrangements(breakpoints,lengths):
    if len(breakpoints) >2:
        print("so far, I don't know how to handle complicated rarrangements with more than 2 breakpoints.")
        exit(1)

    if len(breakpoints)==2:
        if same_fragment(breakpoints):
            print("either invert or delete:", breakpoints)
        else:
            print("reciprocal translocation:", breakpoints)


if __name__=="__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('-H',default=3   ,type=int, help="longest homopolymer stretch allowed")
    parser.add_argument('-N',default=1   ,type=int, help="Number of distinct oligos to pick")
    parser.add_argument('-n',default=10   ,type=int,help="Length of oligos")
    parser.add_argument('-g',default=0.5,type=float,help="Fraction of GC bases on average in generated oligos")
    parser.add_argument('-M',default=0.0,type=float,help="Minimum Tm for oligos")
    parser.add_argument('-E',default=False,action='store_true',help="No edit-distance-one pairs allowed")
    parser.add_argument('-X',default=100.0,type=float,help="Maximum Tm for oligos")

    args = parser.parse_args()

    all_rearrangements( (point(1,10),point(2,10)) ,{1:20,2:20})
    all_rearrangements( (point(1,10),point(1,15)) ,{1:20,2:20})
