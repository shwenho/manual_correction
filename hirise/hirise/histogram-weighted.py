#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from past.utils import old_div
import sys
import math

column=int(sys.argv[1])
binsize=float(sys.argv[2])
wcol=int(sys.argv[3])

counts={}

n=0
total=0.0
while True:
    line = sys.stdin.readline()
    if not line:
        break
    if line[0]=="#":
        continue
    n+=1
    c = line.strip().split()
    if len(c)<column: continue
    x=float(c[column-1])
    total+=x
    bin = math.floor(old_div(x,binsize))
    if bin not in counts:
        counts[bin]=0
    counts[bin]+=int(c[wcol-1])
#    print x

bins = list(counts.keys())
bins.sort()

r=[ min(counts.values()),max(counts.values()) ]
#total=sum(counts.values())
xfact = old_div(50.0,r[1])
#print xfact

print("# n:",n)
print("# sum:", total)
print("# mean:",old_div(float(total),n))

cum=0.0
lastb=False
for bini in bins:
    cum+=counts[bini]
    if lastb:
        if bini-lastb>1:
            print("#   ...")
    print("\t".join(map(str,[bini*binsize,"...", (bini+1)*binsize, counts[bini],  "%0.4f" %( old_div(float(cum),n)) ,"|"+"X"*int(counts[bini]*xfact) ])))
    lastb=bini
