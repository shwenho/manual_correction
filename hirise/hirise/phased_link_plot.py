#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import pysam

import networkx as nx
import re
from hirise.bamtags import BamTags

debug=False
def read_variant_positions(fn,region=False):
    d={}
    b2h={}
    f=open(fn,"r")
    while True:
        l=f.readline()
        if not l:
            break
            # ['6', '33099129', 'rs3116992', 'G', 'A', '.', 'PASS', '.', 'GT:GQ:DP', '0|0:99:64']            
        c = l.strip().split()
        x = int(c[1])-1
        if (region) and not ( ((c[0]==region[0]) and (  x>region[1][0] and x<region[1][1]  ))): continue

        m=re.match("([01])(.)([01])",c[9])
        if m:
            g=m.groups()
            if g[0]==g[2]: continue
        if debug and m:
            print(m.groups())
        #print c

        d[( c[0],x )]=c[0]+":"+str(x)
        d[ c[0]+":"+str(x) ] = x
        b2h[ c[0]+":"+str(x) ] = { c[3]:g[0], c[4]:g[2] }
        if debug: print("#",c,b2h[ c[0]+":"+str(x) ]) 

    
    return d,b2h



if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True,action="append",default=[])
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('-q','--mapq',type=int,default=20)
    parser.add_argument('-p','--padding',type=int,default=0)
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-r','--region',default=False)
    parser.add_argument('-g','--gff',default=False)
    parser.add_argument('-v','--vcf',default=False)


    args = parser.parse_args()

    mapq = args.mapq

    if args.debug:
        print(args)

    region=False
    if args.region:
        c,r = args.region.split(":")
        s,t = r.split("-")
        region = (c,(int(s)-args.padding,int(t)+args.padding))
        args.region = "{}:{}-{}".format( region[0],region[1][0],region[1][1] )
        print(region)

    

    snps={}
    b2h={}
    if args.vcf:
        snps,b2h=read_variant_positions(args.vcf,region)

    if args.debug:
        for k in list(snps.keys()):
            print(k,snps[k],b2h.get(k))

    t2snp = {}
    hist={}

    chro,xy = args.region.split(":")

    for bamfile in args.bamfile:

        sam=pysam.Samfile(bamfile,"rb")

        if args.debug:
            print(sam.header)

        h=sam.header
        seqs=h['SQ']

        n=0

        template_phase={}

        for pileupcolumn in sam.pileup(region=args.region):
#            print 
            if not (chro,pileupcolumn.pos) in snps: continue
            this_snp="{}:{}".format(chro,pileupcolumn.pos)
            print(pileupcolumn.pos, pileupcolumn.n,snps[this_snp],this_snp,b2h[this_snp])
            for pileupread in pileupcolumn.pileups:
                mybase=pileupread.alignment.seq[pileupread.qpos]
                
                print('\t%d base in read %s = %s phase=%s' %  (pileupcolumn.pos,pileupread.alignment.qname,mybase,b2h.get(this_snp,{}).get(mybase) ))

                if pileupread.alignment.qname in template_phase:
                    if not template_phase[pileupread.alignment.qname] == b2h.get(this_snp,{}).get(mybase):
                        print("#phase conflict for", pileupread.alignment.qname) 
                    else:
                        print("#phase confirmation for", pileupread.alignment.qname) 
                template_phase[pileupread.alignment.qname] = b2h.get(this_snp,{}).get(mybase) 


        colormap={None: "0x000000", "0":"0xff0000", "1":"0x000055" }
        for aln in sam.fetch(region=args.region,until_eof=True):
            if not aln.mapq    > mapq: continue
            if not BamTags.mate_mapq(aln)>mapq: continue
            if not (region[1][0]<aln.pnext  and aln.pnext<region[1][1]): continue
            if aln.tid == aln.rnext and aln.pos < aln.pnext:
                phase = template_phase.get(aln.qname,-1)
                print(aln.pos,aln.pnext,phase,aln.mapq,     min(aln.mapq,BamTags.mate_mapq(aln)),max(aln.mapq,BamTags.mate_mapq(aln)),":xy:",aln.qname)
                print(aln.pnext,aln.pos,phase,BamTags.mate_mapq(aln),max(aln.mapq,BamTags.mate_mapq(aln)),min(aln.mapq,BamTags.mate_mapq(aln)),":xy:",aln.qname)
#                print aln.pnext,aln.pos,phase,BamTags.mate_mapq(aln),":xy:"


        sam.close()
