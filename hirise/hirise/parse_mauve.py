#!/usr/bin/env python3

import argh


def main(mauve_file, output="/dev/stdout"):
    dump_lengths = []
    running_total = 0

    with open(output, 'w') as output_handle:
        for scaffold, end1, end2, length in parse_mauve(mauve_file):
            
            print("P", 1, scaffold, 1, end1, running_total, length, file=output_handle)
            running_total += length
            print("P", 1, scaffold, 1, end2, running_total, length, file=output_handle)
            running_total += 100
            dump_lengths.append((scaffold, length,))

        for scaffold, length in dump_lengths:
            print("L", scaffold, length, file=output_handle)

def parse_mauve(mauve_file):
    started = False
    with open(mauve_file) as handle:
        for line in handle:
            if line.startswith("Ordered Contigs"):
                started = True
                continue
            if line.startswith("Contigs with conflicting ordering information"):
                break
            if started and line.startswith("contig"):
                name = line.split()[1]
                start = int(line.split()[4])
                end   = int(line.split()[5])
                orientation = line.split()[3]
                if orientation == "forward":
                    end1 = "5"
                    end2 = "3"
                elif orientation == "complement":
                    end1 = "3"
                    end2 = "5"
                else:
                    raise ValueError
                    
                yield name, end1, end2, abs(end-start)

if __name__ == "__main__":
    argh.dispatch_command(main)
