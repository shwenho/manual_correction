#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from builtins import object
from past.utils import old_div
import sys
import networkx as nx
#import hirise.chicago_edge_scores as ces
from scipy.special import gammaincc
from scipy import nan
import math
import random

class ChicagoModel(object):
    def __init__(self,p):
        self.p= p
        self.variable = ["alpha" ,"f","d","pn"]
    def normalize(self):
        self.p["a"] = old_div(( 0.5 - self.p["c"]*self.p["d"] ), (self.p["f"]**(1.0-self.p["b"])))

    def scan(self,var,f1,f2,step):
        f=f1
        while f<f2:
            m2 = ChicagoModel( dict(self.p) )
            m2.p[var] = self.p[var]*f
            m2.normalize()
            #print "#",f
            yield m2
            f+=step

    def step(self):        
        m2 = ChicagoModel( dict(self.p) )
        for k in [random.choice(m2.variable)]:
            m2.p[k] *= 1.0 + random.uniform(-0.1, 0.1)
        return m2



#G=3.0e9
N=100000000.0
pn=0.3
G=3000000000.0
from scipy.special import gammaincc

def fx(x,alpha,b,d,f):
    return (old_div((0.5-alpha),(f**(1-b))))*(x**(-b))*math.exp(old_div(-x,f)) + (old_div(alpha,d))*math.exp(old_div(-x,d))

def fi(x,alpha,b,d,f,args):
    if args.debug:
        print("::",x,gammaincc(1.0-b,old_div(x,f)),a,(f**(1-b)),a*(f**(1-b))*gammaincc(1.0-b,old_div(x,f)) , c*d*math.exp(old_div(-x,d)))
        print(":::",a*(f**(1-b))*gammaincc(1.0-b,old_div(x,f)) + c*d*math.exp(old_div(-x,d)))
    return (0.5-alpha)*gammaincc(1.0-b,old_div(x,f)) + alpha*math.exp(old_div(-x,d))
#    gammaincc(1.0-b,x2/f)

def lnL0(a,b,c,d,f,pn,G,hist,hist2,mind,nnn,args):
    lll=0.0
    #print X
    for x in range(mind,nnn,50):
        lll += hist[x] * math.log( (2.0*pn/G + 2.0*(1.0-pn)*fx(x,a,b,c,d,f) ) )
        if args.debug:
            print(":x:",pn,2.0*(pn*x/G), 2.0*(1.0-pn)*fi(x,a,b,c,d,f,args))
        lll += hist2[x]* math.log( (pn - 2.0*(pn*x/G) + 2.0*(1.0-pn)*fi(x,a,b,c,d,f,args))  )
    return lll

def lnL(model,hist,hist2,mind,nnn,args):
    lll=0.0
    #print X
    for x in range(mind,nnn,50):
        lll += hist[x] * math.log( (2.0*model.p["pn"]/model.p["G"] + 2.0*(1.0-model.p["pn"])*fx(x,model.p["alpha"],model.p["b"],model.p["d"],model.p["f"]) ) )
        #if args.debug:
        #    print ":x:",pn,2.0*(pn*x/G), 2.0*(1.0-pn)*fi(x,a,b,c,d,f,args)
        lll += hist2[x]* math.log( (model.p["pn"] - 2.0*(model.p["pn"]*x/model.p["G"]) + 2.0*(1.0-model.p["pn"])*fi(x,model.p["alpha"],model.p["b"],model.p["d"],model.p["f"] ,args))  )
    return lll

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-H','--histogram')
    parser.add_argument('-v','--var', default=False)
    parser.add_argument('-P','--param', default="{}")
    parser.add_argument('-m','--mind', type=int, default=1000)
    parser.add_argument('-c','--climb', action="store_true")
    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()
    if args.seed != -1 :
        random.seed(args.seed)


    #ces.debug=args.debug
    if args.debug:
        args.progress=True
#    if args.progress: log( str(args) )

    command_line_params = eval(args.param)

    fff=open(args.histogram)

    hist={}
    hist2={}

    a=old_div(11.2, 105.290713211105)
    b=0.84
    c=old_div(0.0029, 105.290713211)
    c*=0.5
    d=19448.6

    while c*d>0.5:
        d*=0.8

    alpha = c*d

    f=10000.0
    G=3.0e9
    pn=0.3

    a = old_div(( 0.5 - c*d ), (f**(1.0-b)))

#    parameters =  { "alpha":alpha,  "a":a, "b":b, "c":c, "d":d,"f":f, "pn":pn, "G":G }
    parameters =  { "alpha":alpha, "b":b, "c":c, "d":d,"f":f, "pn":pn, "G":G }

    for k in list(command_line_params.keys()):
        parameters[k] = command_line_params[k]

    params = ChicagoModel( parameters ) #  { "a":a, "b":b, "c":c, "d":d,"f":f, "pn":pn, "G":G } )
    print("#",params.p)

    x=0.0
#    print x,fi(x,a,b,c,d,f,args)

    while True:
        l=fff.readline()
        if not l: break
        l,h,h2 = list(map(int,l.strip().split() ))
        hist[l]=h
        hist2[l]=h2

    nnn = max( list(hist.keys()) + list(hist2.keys()) )
    #X=pn - a*(f**(1-b))-c*d
    #    print X

    pn0=pn
    G0=G
    d0=d
    b0=b
    c0=c
    mind=args.mind

    if args.var:
        for p in params.scan(args.var,0.5,1.5,0.05): 
            print("\t".join(map(str,[p.p[i] for i in [ args.var ]]  + [ lnL(p,hist,hist2,mind,nnn,args) ] )))


    accept_count =0
    attempt_count = 0
    ll = lnL(params,hist,hist2,mind,nnn,args) 
    #print ll
    if args.climb: 
        while True:
            attempt_count +=1
            p = params.step()
            ll2 = lnL(p,hist,hist2,mind,nnn,args) 
            if math.isnan(ll2) : continue
#            print "#", ll,ll2 ,ll2-ll, math.exp( min(0.0,ll2 - ll) )
            if ll2>ll or ( random.random() < math.exp( ll2 - ll ) ):
                params = p
                accept_count += 1
                ll=ll2
                print(attempt_count, accept_count, ll, " ".join(map(str,[ params.p[i] for i in params.variable ])))
