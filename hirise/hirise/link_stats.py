#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from past.utils import old_div
import sys
import argparse
import random
import gzip
import glob

if __name__=="__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('-i','--input',default="gj4.out")
    parser.add_argument('-b','--besthits')
    parser.add_argument('-c','--contiglist')
    parser.add_argument('-l','--links')
    parser.add_argument('-o','--outfile')
    parser.add_argument('-I','--internal',action="store_true")
#    parser.add_argument('-c','--chr',default="3")
    parser.add_argument('-m','--min',default=0.0   ,type=float)
    parser.add_argument('-a','--max',default=10.0e6,type=float)
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()
    if args.seed != -1 :
        random.seed(args.seed)


    print(args)

    contigs={}
    if args.contiglist:
        f=open(args.contiglist)
        while True:
            c=f.readline()
            if not c: break
            cc=c.strip().split()
            contigs[cc[0]] = (cc[1],cc[2])
        f.close()

    besthit={}
    if args.besthits:
#        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
    #            print c[1],besthit[c[1]]
            f.close()
    #if args.progress: print "#Done reading besthits"


    if False:
        scaffold_hues={}
        scaffold_saturations={}
        coords={}
        coord_map={}
        scaffold={}
        contig_strand={}
        bh={}
        f=open(args.input)
        sc_max={}
        sc_min={}
        ch_max={}
        ch_min={}
        while True:
            l=f.readline()
            if not l: break
            if l[:2]=="p:":
                c=l.strip().split()
                #print c
                v=eval(" ".join(c[8:]))
                strand="+"
                if v:
                    strand = v[2]
                sc,contig_end,x,chr,y = int(c[1]),c[2],float(c[3]),c[4],float(c[5])
                scaffold_hues[sc]=1
                contig,end = contig_end[:-2],contig_end[-1:]
                contig_strand[contig]=strand
                #print "\t".join(map(str,[sc,contig_end,contig,end,x,y,chr,strand]))
                coords[contig,end]=(x,y,chr)
                coord_map[contig] = 1
                scaffold[contig]=sc
                bh[contig]=v
                sc_max[sc,chr] = max( sc_max.get((sc,chr),-1),   x )
                sc_min[sc,chr] = min( sc_min.get((sc,chr),1e10), x )
                ch_max[sc,chr] = max( ch_max.get((sc,chr),-1),   y )
                ch_min[sc,chr] = min( ch_min.get((sc,chr),1e10), y )

        import colorsys
        scaffold_rgb={}
        for s in list(scaffold_hues.keys()):
            #scaffold_hues[s]        = 360.0*random.random()
            #scaffold_saturations[s] = 0.5*random.random() + 0.5
            scaffold_rgb[s] = colorsys.hls_to_rgb( random.random(), 0.5, 0.5*random.random() + 0.5)
            #scaffold_rgb[s] = '%02x%02x%02x' % (255.0*rgb[0], 255.0*rgb[1], 255.0*rgb[2] )

    cm={}
    cs={}
    cb={}
    cc={}
    #print "1"
    sys.stdout.flush()
    if False:
        for contig in list(coord_map.keys()):
            sc = scaffold[contig]
            cr=-1
            if bh[contig]:
                cr = bh[contig][1]
            for e in ("3","5"):
                cm[sc,cr] = cm.get((sc,cr),0.0) + coords[contig,e][1]
                cb[sc,cr] = cb.get((sc,cr),0.0) + coords[contig,e][0]
                cc[sc,cr] = cc.get((sc,cr),0)   + 1
        sys.stdout.flush()

        for sc,cr in list(cm.keys()):
            cm[sc,cr] /= float(cc[sc,cr])
            cb[sc,cr] /= float(cc[sc,cr])            
        sys.stdout.flush()

        for contig in list(coord_map.keys()):
            sc = scaffold[contig]
            cr=-1
            if bh[contig]:
                cr = bh[contig][1]
                #        cr = bh[contig][1]
            for e in ("3","5"):
                x,y,chr = coords[contig,end]
                if (y-cm[sc,cr] > 0 and x-cb[sc,cr] > 0) or ( y-cm[sc,cr]<0 and x-cb[sc,cr]<0) : 
                    cs[sc,cr] = cs.get((sc,cr),0) + 1
                else: 
                    cs[sc,cr] = cs.get((sc,cr),0) - 1

        sys.stdout.flush()

    
    def map_coord(a,contig,l,coords,bh):
        cr=-1
        if bh[contig]:
            cr = bh[contig][1]
        sc=scaffold[contig]

        d = cm[sc,cr] - cb[sc,cr]

        x5,y5,chr5 = coords[contig,"5"]
        z5 = cm[sc,cr] + (x5 - cb[sc,cr])
        if cs[sc,cr]<0:
            z5 = cm[sc,cr] - (x5 - cb[sc,cr])

        x3,y3,chr3 = coords[contig,"3"]
        z3 = cm[sc,cr] + (x3 - cb[sc,cr])
        if cs[sc,cr]<0:
            z3 = cm[sc,cr] - (x3 - cb[sc,cr])

        f=old_div(float(a),l)
        z = z5+f*(z3-z5)
        y = y5+f*(y3-y5)
        return(z,y)

        pass
    def draw_link(a,b,s1,s2,l1,l2,coords,bh,outf,sameScaffold):
        za,ya = map_coord(a,s1,l1,coords,bh)
        zb,yb = map_coord(b,s2,l2,coords,bh)
        outf.write("dot(t*({},{}),black);\n".format(old_div(za,1000.0),old_div(ya,1000.0)))
        outf.write("dot(t*({},{}),black);\n".format(old_div(zb,1000.0),old_div(yb,1000.0)))
        if abs(ya-yb) < 500000.0:
            if sameScaffold:
                outf.write("draw(t*(({},{})--({},{})--({},{})),black);\n".format(old_div(za,1000.0),old_div(ya,1000.0),old_div(zb,1000.0),old_div(ya,1000.0),old_div(zb,1000.0),old_div(yb,1000.0)))
            else:
                outf.write("draw(t*(({},{})--({},{})--({},{})),red);\n".format(old_div(za,1000.0),old_div(ya,1000.0),old_div(zb,1000.0),old_div(ya,1000.0),old_div(zb,1000.0),old_div(yb,1000.0)))
        else:
            #outf.write("dot(t*({},{}),red);\n".format(zb/1000.0,ya/1000.0))
            pass

    links={}

    counts={}
    for c in list(contigs.keys()):
        counts[c]={}

    if args.links:
        for fn in glob.glob(args.links):
#            print fn
            if fn[-3:]==".gz":
                f = gzip.open(fn)
            else:
                f = open(fn)

            while True:
                l=f.readline()
                if not l: break
                if l[0]=="#": continue
                c=l.strip().split()
                s1,s2,l1,l2,nl = c[0],c[1],int(c[2]),int(c[3]),int(c[4])
                if s1 in contigs and nl>1:# or s2 in contigs:
#                    print s1,s2,nl,contigs[s1],besthit.get(s2),besthit.get(s1)
                    bh = besthit.get(s2,[0,0])
                    counts[s1][bh[1]] = counts[s1].get(bh[1],0)+nl
                if s2 in contigs and nl>1:# or s2 in contigs:
#                    print s2,s1,nl,contigs[s2],besthit.get(s1),besthit.get(s2)
                    bh = besthit.get(s1,[0,0])
                    counts[s2][bh[1]] = counts[s2].get(bh[1],0)+nl
#                    v=eval(" ".join(c[5:]))
#                    links[s1,s2]=v

    for c in list(contigs.keys()):
        print(c, contigs[c], counts[c])
    

    
