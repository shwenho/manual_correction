#!/usr/bin/env python3


import re
import argh
import os
import shutil
from datetime import date
import subprocess
import sys
import gzip

def main(customer_dir, fasta_file, table_file, report,misjoins, project="", gapclosed="/dev/null"):
    customer_id =   project+"_"+date.today().strftime("%d%b%Y")+"_"+get_id(fasta_file)
    dirname = customer_id

    print(customer_id, file=sys.stderr)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    if fasta_file.endswith(".gz"):
        print("Moving {} to {}".format(fasta_file, customer_id + ".fasta.gz"))
        shutil.copy(fasta_file, os.path.join(dirname, customer_id+".fasta.gz"))
    else:
        print("Moving {} to {}".format(fasta_file, customer_id + ".fasta"))
        shutil.copy(fasta_file, os.path.join(dirname, customer_id+".fasta"))

    shutil.copy(table_file, os.path.join(dirname, customer_id+".table.txt"))
    shutil.copy(report, os.path.join(dirname, customer_id+"_report.pdf"))
    shutil.copy(misjoins, os.path.join(dirname, customer_id+".input_breaks.txt"))
    if gapclosed != "/dev/null":
        shutil.copy(gapclosed, os.path.join(dirname, customer_id+".gapclosing_table.txt"))

    print("Moving {} to {}".format(table_file, customer_id + ".table.txt"))
    print("Moving {} to {}".format(report, customer_id + "_report.pdf"))
    print("Making manifest.")
    make_manifest(dirname,project,gapclosed)


def make_manifest(customer_id,project,gapclosed):
    out = open(os.path.join(customer_id,"manifest.txt"),"w")
    print(project.replace("_"," ").title() + " Manifest",file=out)
    print(date.today().strftime("%B %d, %Y"),file=out)
    print("",file=out)
    print("    " + customer_id+".fasta.gz",file=out)
    print("        FASTA file containing the final, gapclosed Hirise scaffolds.\n",file=out)
    print("    " + customer_id+".input_breaks.txt",file=out)
    print("        Tab-delimited table describing positions of breaks made in the input\n" 
          "        assembly scaffolds. Follows the same format as the table file\n"
          "        described below.\n",file=out)
    print("    " + customer_id+"_report.pdf",file=out)
    print("        Hirise scaffolding report, contains Dovetail library insert \n"
          "        distribution(s), contiguity plots, and statistics for\n"
          "        the input and output assemblies.\n",file=out)
    if gapclosed != "/dev/null":
        print("    " +  customer_id+".gapclosing_table.txt",file=out)
        print("        Tab-delimited table describing gaps successfully closed in\n"
              "        the final HiRise assembly. The table has the following format: \n\n"
              "            1. HiRise scaffold number\n"
              "            2. Left contig ID\n"
              "            3. Left contig flanking sequence\n"
              "            4. Right contig ID\n"
              "            5. Right contig flanking sequence\n"
              "            6. Gap-filled sequence\n\n"
              "        where each contig ID ends with '.5' or '.3' to indicate the\n"
              "        the 5' or 3' end of the contig respectively.\n",file=out)
    print("    " + customer_id+".table.txt",file=out)
    print("        Tab-delimited table describing positions of input assembly scaffolds\n"
          "        in the Hirise scaffolds. The table has the following format: \n\n"
          "            1. HiRise scaffold name\n"
          "            2. Input sequence name\n"
          "            3. Starting base (zero-based) of the input sequence\n"
          "            4. Ending base of the input sequence\n"
          "            5. Strand (- or +) of the input sequence in the scaffold\n"
          "            6. Starting base (zero-based) in the HiRise scaffold\n"
          "            7. Ending base in the HiRise scaffold\n\n"
#          "            8. Confidence in orientation assignment, see below for details\n\n"
          "        where '-' in the strand column indicates that the sequence is reverse\n"
          "        complemented relative to the input assembly."
#          "        The orientation confidence values range from 0 to 1 and represent the\n"
#          "        ratio of the overall scaffold likelihood with the contig in the current\n"
#          "        orientation versus the likelihood with the contig in the alternate\n"
#          "        orientation. A value of 0.5 indicates that either orientation is supported\n"
#          "        by the data. A value less than 0.5 could be interpreted as meaning that we\n"
#          "        should flip that contig, but it's also possible that flipping the contig\n"
#          "        would reduce the confidence for other contigs in that scaffold.\n"
          ,file=out)
    out.close()


def get_id(fastafile):
    if fastafile.endswith(".gz"):
        open_with = gzip.open
        mode = "rt"
    else:
        open_with = open
        mode = "r"
    for line in open_with(fastafile, mode):
        if line.startswith(">"):
            m=re.match(">Sc(.*)_",line)
            if m:
                return m.group(1)


if __name__ == "__main__":
    argh.dispatch_command(main)
