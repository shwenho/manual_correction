#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from builtins import object
from past.utils import old_div
import math

"""Chicago likelihood model calculations as described in ... """

debug=False
one_over_sqrt_2_pi = old_div(1.0,math.sqrt(2.0*3.14159 ))

def fragment_size(l1,l2,o1,o2,coords,g=0):
    """o1 and o2 indicate the orientations of the two contigs being used  0 means 5' to 3'; 1 means flipped"""
    x,y = coords[0],coords[1]
    if (o1,o2) == (0,0):     #         -------1----->    ----2------>
        d= (y+l1-x+g)
    if (o1,o2) == (0,1):     #         -------1----->   <---2-------
        d= (l2-y+l1-x+g)
    if (o1,o2) == (1,0):     #        <-------1-----     ----2------>
        d= (x+y+g)
    if (o1,o2) == (1,1):     #        <-------1----->   <----2------
        d= (l2-y+x+g)
    if d<0: print("warning: d<0 in fragment_size",d,[l1,l2,o1,o2,coords,g])
    return d
class ChicagoModel(object):

    def __init__(self,params):
        self.alpha = params.get('alpha',[1.0])
        self.mu    = params.get('mu',[1.0])
        self.sigma = params.get('sigma',[1.0])
        self.pn    = params.get('pn',0.5)
        self.G     = params.get('G',3000000000)
        self.N     = params.get('N',100000000)
        self.nexps = len(self.alpha)
        self.readlength=0.0

    def set_params(self,params):
        self.alpha = params.get('alpha')
        self.mu    = params.get('mu'   )
        self.sigma = params.get('sigma')
        self.pn    = params.get('pn'   )
        self.G     = params.get('G'    )
        self.N     = params.get('N'    )
        self.nexps = len(self.alpha)
        self.dominant_at_large_x=0
        
#        x=self.p_insert_raw_i(1000,self.dominant_at_large_x)
#        for i in range(1,self.nexps):
#            if x<self.p_insert_raw_i(1000,i):
#                x=self.p_insert_raw_i(1000,i)
#                self.dominant_at_large_x=i
#        print "d:",self.dominant_at_large_x

        self.lnFinf = math.log(self.pn)-math.log(self.G)
        self.logN = math.log(self.N)
        self.logG = math.log(self.G)


    def __repr__(self):
        return(str({ 'pn': self.pn, 'G':self.G, 'N':self.N, 'a':self.alpha, 'mu':self.mu, 'sigma':self.sigma, 'n':self.nexps }))

### These are the things that depend on the model:
#
    def f0(self,x):
        """P_{raw}(x)= \sum \alpha_i \beta_i e^{-x \beta_i}  """
        #return sum( [ self.alpha[i]*self.beta[i]*math.exp(-x*self.beta[i]) for i in range(self.nexps) ] )
        return sum( [ (self.alpha[i]*one_over_sqrt_2_pi/self.sigma[i])                     *math.exp(old_div(-(x-self.mu[i])**2.0,(2.0*self.sigma[i]**2.0))) for i in range(self.nexps) ] )

    def f0_prime(self,x):
        return sum( [ (-self.alpha[i]*one_over_sqrt_2_pi*(x-self.mu[i])/self.sigma[i]**3.0)*math.exp(old_div(-(x-self.mu[i])**2.0,(2.0*self.sigma[i]**2.0))) for i in range(self.nexps) ] )

    def f0_double_prime(self,x):
        return sum( [ (self.alpha[i]*one_over_sqrt_2_pi/self.sigma[i]**3.0)*math.exp(old_div(-(x-self.mu[i])**2.0,(2.0*self.sigma[i]**2.0)))*((old_div((x-self.mu[i])**2.0,self.sigma[i]**2.0))-1.0) for i in range(self.nexps) ] )


    def F0(self,d):
        n=self.nexps
        return  self.pn*d/self.G+(1.0-self.pn)*sum( [ (-0.5*self.alpha[i]*math.erf(old_div((self.mu[i]-d),(self.sigma[i]*math.sqrt(2.0))))) for i in range(self.nexps) ]) 

    def H0(self,d):
        n=self.nexps
        return 0.5*d*d*self.pn/self.G+(1.0-self.pn)*sum([ -0.5*self.mu[i]*self.alpha[i]*math.erf(old_div((self.mu[i]-d),(self.sigma[i]*math.sqrt(2.0)))) - self.alpha[i]*self.sigma[i]*one_over_sqrt_2_pi*math.exp(old_div(-(d-self.mu[i])**2.0,(2.0*self.sigma[i]**2.0))) for i in range(n)])
#
###

    def F(self,d):
        return self.F0(d)-self.F0(0)

    def H(self,d):
        return self.H0(d)-self.H0(0)

    def f(self,x,cache={}):   #cache this
        if x in cache: return cache[x]
        r= old_div(self.pn,self.G) + (1.0-self.pn) * self.f0(x)
        cache[x]=r
        return r

    def f_prime(self,x):
        return (1-self.pn)*self.f0_prime(x)

    def f_double_prime(self,x):
        return (1-self.pn)*self.f0_double_prime(x)


### These are for backwards compatability with the old implementation
#
    def p_insert_raw(self,x):
        return self.f0(x)

    def p_insert_effective(self,x):
        """ p_n/G + (1-p_n) P_{raw}(x) """
        return  self.f(x)

    def ddg_p(self,x):
        return self.f0_prime(x)

    def d2dg2_p(self,x):                                                                                                                                                     
        return self.f0_double_prime(x)

    def ll(self,l1,l2,o1,o2,links,g,p0=-1):
        return self.lnL(l1,l2,o1,o2,g,links)

    def p0(self,l1,l2,g):
        return 1.0-(old_div(self.n_bar(l1,l2,g),self.N))
#
###

    def omega(self,l1a,l2a,d):
        l1=min(l1a,l2a)
        l2=max(l1a,l2a)
        if     d<0:  return 1 # seems weird...  but sometimes reads map apparently dangling off the ends of contgs... when we're virtually trimming the contigs for end-effect mapability
        elif   d<l1: return d+1
        elif   d<l2: return l1+1
        elif   d<=l1+l2: return l1+l2-d+1
        else: return 0

    def osigma(self,l1a,l2a,d):
        l1=min(l1a,l2a)
        l2=max(l1a,l2a)
        if     d<0:  return 0
        elif   d<l1: return 1.0
        elif   d<l2: return 0
        elif   d<l1+l2: return -1.0
        else: return 0

    def lnF(self,x,cache={}):   # Cache this
        if x in cache: return cache[x]
        r=math.log( self.f(x) )
        cache[x]=r
        return r  #math.log( self.f(x) )

    def S(self,d,cache={}):  # Cache this
        if d in cache: return cache[d]
        #\sum_{i=0}^{d} (d-i) f(i) \approx 
        x= d*self.F(d) - self.H(d)
        cache[d]=x
        return x

    def p(self,l1,l2,g):
        p = old_div((self.S(l1+l2+g)+self.S(g)-self.S(l1+g)-self.S(l2+g)),self.G)
        return p
        
    def p_prime(self,l1,l2,g):
        p = old_div((self.F(l1+l2+g)+self.F(g)-self.F(l1+g)-self.F(l2+g)),self.G)
        return p

    def p_double_prime(self,l1,l2,g):
        ss = self.f(l1+l2+g)+self.f(g)-self.f(l1+g)-self.f(l2+g)
        return old_div(ss,self.G)

    def R(self,l1,l2,g,x):
        # P''/P - (P'/P)^2
        return ( old_div(self.f_double_prime(x),self.f(x))  - (self.Q(x))**2.0 )
        
    def n_bar(self,l1,l2,g):
        p = self.p(l1,l2,g)
        return self.N*p

    def lnL(self,l1,l2,o1,o2,g,links):
        n=len(links)
        n_bar = self.n_bar(l1,l2,g)
        try:
            r= n*self.logN - n_bar - n*self.logG + sum( [ math.log(self.omega(l1,l2,fragment_size(l1,l2,o1,o2,links[i],g)-g)) + self.lnF( fragment_size(l1,l2,o1,o2,links[i],g) ) for i in range(n) ] )
        except Exception as e:
            print(e)
            print("l1,l2",l1,l2)
            print("fragment sizes g=0:", [fragment_size(l1,l2,o1,o2,links[i],0) for i in range(n)])
            print("fragment sizes g=g:", [fragment_size(l1,l2,o1,o2,links[i],g) for i in range(n)])
            print("omega=",[self.omega(l1,l2,fragment_size(l1,l2,o1,o2,links[i],0)) for i in range(n)])
            print("f=",[ self.f( fragment_size(l1,l2,o1,o2,links[i],g)) for i in range(n)])
            raise e
        return r

    def lnL0(self,l1,l2,o1,o2,links):
        n=len(links)
        n_bar = self.N*self.pn*l2*l1/(self.G**2)
        return n*self.logN - n_bar - n*self.logG + sum( [ math.log(self.omega(l1,l2,fragment_size(l1,l2,o1,o2,links[i],0))) + self.lnFinf for i in range(n) ] )  

    def score(self,l1,l2,o1,o2,links,g,p0=0):
        return self.lnL(l1,l2,o1,o2,g,links) - self.lnL0(l1,l2,o1,o2,links)

    def Q(self,x):
#        return (self.omega(l1,l2,x-g)*self.f_prime(x) + self.osigma(l1,l2,x-g)*self.f(x) )/( self.omega(l1,l2,x-g)*self.f(x) )
        return (old_div(self.f_prime(x),self.f(x)))

    def ddg_lnL(  self,l1,l2,o1,o2,g,links):
        n=len(links)
        try:
            r =  -self.N * self.p_prime(l1,l2,g) + sum([ self.Q(fragment_size(l1,l2,o1,o2,links[i],g) ) for i in range(n)])
        except Exception as e:
            print(e)
            print("l1,l2,g",l1,l2,g)
            print("n_bar:",self.n_bar(l1,l2,g))
            print("p:",self.p(l1,l2,g))
            print("p_prime:",self.p_prime(l1,l2,g)) 
            raise e
        return r

    def d2dg2_lnL(self,l1,l2,o1,o2,g,links):
        n=len(links)
        return ( -self.N * self.p_double_prime(l1,l2,g) + sum([ self.R(l1,l2,g,fragment_size(l1,l2,o1,o2,links[i],g) ) for i in range(n) ]) )

    def d2dg2_llr(self, l1,l2,o1,o2,links,g,seen={}):
        if not "warned" in seen: 
            sys.stderr.write("using deprecated interface to likelihood model code.\n")
            seen["warned"]=1
        return self.d2dg2_llr(l1,l2,o1,o2,links,g)

    def ddg_llr(self, l1,l2,o1,o2,links,g,seen={}):
        if not "warned" in seen: 
            sys.stderr.write("using deprecated interface to likelihood model code.\n")
            seen["warned"]=1
        return self.ddg_llr(l1,l2,o1,o2,links,g)

    def ml_gap(self,l1,l2,o1,o2,links,g0):
        gap=g0
        G=self.G
        N=self.N
        pn=self.pn

        last_gap=g0
        for i in range(100):
            #p0 = self.p0(      l1,l2,gap)
            x1=  self.ddg_lnL(  l1,l2,o1,o2,gap,links) #1st derivative of the likelihood wrt. gap size
            x2=  self.d2dg2_lnL(l1,l2,o1,o2,gap,links) #2nd derivative of the likelihood wrt. gap size
            score=self.score(l1,l2,o1,o2,links,gap)
#            if debug: print "\t".join(map(str,[ "#it",i,o1,o2,gap,score,gap-x1/2,x1,x2,x1/x2]))
            if x2==0: 
                break
                print("hit x2==0 after",i)
            gap = int( gap - old_div(x1,x2) )
            if gap<-1000.0:
                gap=-1000

            if gap>200000.0:
                gap=200000

            if abs(old_div(x1,x2))<0.1: break
            if abs(gap-last_gap)<1.0: break
            last_gap=gap

        score=self.score(l1,l2,o1,o2,links,gap) #l1,l2,o1,o2,G,pn,links,N,gap,p0)
        
#        if gap<0.0:
#            print "wtf? negative gap"
#            raise Exception 

        return gap,score
    
model=ChicagoModel({})

def insert_size_dist(x):
    return model.p_insert_raw(x)

def set_exp_insert_size_dist_fit_params(fit_params):
    model.set_params(fit_params)
    print("#",model)

def p_not_a_hit(l1,l2,GenomeSize,gaplen,pn):
    return model.p0(l1,l2,gaplen)

def llr_v0(l1,l2,o1,o2,GenomeSize,pn,links,N,gaplen,p0 ):
    return model.score(l1,l2,o1,o2,links,gaplen,p0)

def ll(l1,l2,o1,o2,GenomeSize,pn,links,N,gaplen,p0=-1):
    return model.ll(l1,l2,o1,o2,links,gaplen,p0)

def ml_gap(l1,l2,o1,o2,G,pn,links,N,g0):
    return model.ml_gap(l1,l2,o1,o2,links,g0)



if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-M','--set_insert_size_dist_fit_params',default=False )
    parser.add_argument('-d','--debug',default=False )

    args = parser.parse_args()
    debug=args.debug

    fmodel=open( args.set_insert_size_dist_fit_params )
    contents = fmodel.read()
    try:
        fit_params=eval(contents)
    except:
        "couldn't deal with option", args.param
    fmodel.close
    set_exp_insert_size_dist_fit_params(fit_params)
    
    rsf=0.0
    rsS=0.0
    last=0.0
    lastp=0.0
    for x in range(1,2000,1):
        f=model.f(x)
        rsf+=f
        rsS+=rsf
        p=model.p(5000,5000,x)
        pp=model.p_prime(5000,5000,x)
        dp=p-lastp
        lastp=p
#        print x,f,model.f_prime(x),model.S(x),model.F(x),model.H(x),rsf,rsS,f-last
#        print x,model.N*p,model.N*pp,model.N*dp,model.lnL(5000,5000,1,0,x,[(1,1)]), model.ddg_lnL(5000,5000,1,0,x,[(1,1)])
        print(x,model.lnL(5000,5000,1,0,x,[(1,1)]), model.ddg_lnL(5000,5000,1,0,x,[(1,1)]),model.lnL(5000,5000,1,0,x,[(300,100)]), model.ddg_lnL(5000,5000,1,0,x,[(300,100)]) ,model.omega(800,1200,x),model.osigma(800,1200,x),model.omega(1200,800,x),model.osigma(1200,800,x))
        last=f
#    def lnL(self,l1,l2,o1,o2,g,links):

