#!/usr/bin/env python3
#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import print_function
import sys

c=0
if len(sys.argv)>1:
    c = int(sys.argv[1])-1

s=0.0
while True:
    l = sys.stdin.readline()
    if not l: break
    if l[0]=="#": continue
    try:
        n=float(l.strip().split()[c])
    except:
        print(l)
    s+=n
print(s)
