#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from past.utils import old_div
import sys

buffer=[]
tot=0.0
while True:
    l = sys.stdin.readline()
    if not l:
        break
    x=float(l.strip())
    tot+=x
    buffer.append(x)

buffer.sort(reverse=True)

c=0.0
i=0
st=[]
for n in buffer:
    c+=n
    i+=1
    st.append([i,n,c,tot-c,1.0-old_div(c,tot)])
#    print i,n,c,tot-c,1.0-c/tot

i=0
while st[i][4]>0.5:
    i+=1
print("\t".join(map(str,st[i])))



