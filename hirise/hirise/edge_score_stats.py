#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
import sys

score_file = sys.argv[1]
edges_file = sys.argv[2]

edges={}

f=open(score_file)
while True:
    l=f.readline()
    if not l: break
    if l[0]=="#": continue
    c=l.strip().split()
    if len(c)<3: continue
    edges[c[0],c[1]]=float(c[2])
    edges[c[1],c[0]]=float(c[2])
f.close()


def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False


def qdist(x,y):
    if (not x) or (not y): return (-1)
    if x[0]==y[0]:
        x,y,w,z = list(map(int,[x[2],x[3],y[2],y[3]]))
        ol = pairs_overlap((x,y),(w,z))
        if ol:
            return(-1*min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
        else:
            return(min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
    else:
        return(1.0e12)



f=open(edges_file)
while True:
    l=f.readline()
    if not l: break
    if l[0]=="#": continue
    c=l.strip().split("\t")
#    print c
    a,b = c[0],c[1]
    l1,l2,n,d1,d2,td1,td2,xa,xb,tl1,tl2 = list(map(int,c[2:13]))
    bl1=eval(c[13])
    bl2=eval(c[14])
    if (c[0],c[1]) in edges:
        score=edges.get((c[0],c[1]),0.0)
        tags=eval(c[-1])
        print("\t".join(map(str,[a,b,score,l1,l2,n,qdist(bl1,bl2),bl1,bl2,tags])))

#            if not args.silent: print "\t".join(map(str,[a,b,ll[a],ll[b],int(-G[a][b]['weight']),G.degree(a),G.degree(b),t.degree(a),t.degree(b),x[a],x[b],trim_level[a],trim_level[b],aa,bb, st.get(a,False), st.get(b,False), leafDist.get(a,-1), leafDist.get(b,-1), yDist.get(a,-1), yDist.get(b,-1) , get_tags(a,b) ]))

f.close()
