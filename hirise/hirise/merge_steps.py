#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import object
#!/usr/bin/env python3
import sys
import argparse

if __name__=="__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('-1','--f1')
    parser.add_argument('-2','--f2')

    args = parser.parse_args()
    print(args)

#    f1=open(args.f1)
#    f2=open(args.f2)

    class Merger(object):
        def __init__(self,f1,f2):
            self.f1 = open(f1)
            self.f2 = open(f2)

            while True:
                self.l1 = self.f1.readline()
                if self.l1.find("mapped list:")==0: continue
                elif self.l1[0]=="#":
                    self.nd = len(self.l1.strip().split())-2
                else:
                    self.b1 = self.l1.strip().split()
                    break
            while True:
                self.l2 = self.f2.readline()
                if self.l2.find("mapped list:")==0: continue
                elif self.l2[0]=="#":
                    self.nd = len(self.l2.strip().split())-2
                else:
                    self.b2 = self.l2.strip().split()
                    break
            self.current_scaffold=self.b1[0]
            #self.b1 = 
            #self.b2 =

        def next_line(self):
            if self.b1[0] == self.b2[0]:
                if self.b1[1]<self.b2[1]:
                    self.current_b1 = self.b1
                    self.b1 = self.f1.readline().strip().split()
                    self.current_x=self.b1[1]
                else:
                    self.current_b2= self.b2
                    self.b2 = self.f2.readline().strip().split()
                    self.current_x=self.b2[1]




    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=="#": 
            nd=len(l.strip().split())-2
            print("nd:",nd)
            continue
        c=l.strip().split()
        s=c[0]
        x=int(c[1])
        counts=list(map(int,c[2:2+nd]))
        print(s,x,counts)
        

