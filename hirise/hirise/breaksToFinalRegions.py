#! /usr/bin/env python3
import argh
import os
import re
import sys
from collections import defaultdict

def read_breaks(break_fn):
    breaks = [] 
    fh = open(break_fn,"r")
    i = 0
    for line in fh:
        if line.startswith('#'): continue
        hr_scaf,in_scaf,in_start,in_end,strand,hr_start,hr_end = line.strip().split()[:7]
        hr_start = int(hr_start)
        hr_end = int(hr_end)
        in_start = int(in_start)
        in_end = int(in_end)
        breaks.append((hr_scaf,hr_start,hr_end,in_scaf,in_start,in_end))
        i += 1
    print("read in %d breaks" % (i),file=sys.stderr)
    return breaks 

def write_regions(breaks,lengths,max_contig_size = 500000):
    for hr_scaf,hr_start,hr_end,in_scaf,in_start,in_end in breaks:
        scaf_id = hr_scaf.split("_")[-1]
        if hr_end - hr_start < max_contig_size:
            plot_start,plot_end = get_region(hr_start,hr_end,250000,lengths[hr_scaf])
            print("-R %s:%d-%d\t%s\t%d\t%d\tall" % (scaf_id,plot_start,plot_end,in_scaf,in_start,in_end)) 
        else:
            plot_start,plot_end = get_region(hr_start,hr_start,500000,lengths[hr_scaf])
            print("-R %s:%d-%d\t%s\t%d\t%d\t5" % (scaf_id,plot_start,plot_end,in_scaf,in_start,in_end)) 
            plot_start,plot_end = get_region(hr_end,hr_end,500000,lengths[hr_scaf])
            print("-R %s:%d-%d\t%s\t%d\t%d\t3" % (scaf_id,plot_start,plot_end,in_scaf,in_start,in_end)) 

            
def get_region(begin,end,flank,upper,lower=0):
    left = lower if begin - flank < lower else begin - flank
    right = upper if end + flank > upper else end + flank
    return (left,right)

def get_lengths(lengths_fn):
    fh = open(lengths_fn,"r")
    lengths = {}
    for line in fh:
        length,scaf = line.split()
        length = int(length)
        lengths[scaf] = length
    return lengths

def main(breaks_filename:"A .input_breaks.txt file.",
         lengths = "A .lengths file for the final HiRise assembly"):
    breaks = read_breaks(breaks_filename)
    length_dict = get_lengths(lengths)
    write_regions(breaks,length_dict)


if __name__ == "__main__":
    argh.dispatch_command(main)

