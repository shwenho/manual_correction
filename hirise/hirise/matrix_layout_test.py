#!/usr/bin/env python3
from __future__ import print_function
from builtins import range
#!/usr/bin/env python3
import sys
import argparse
import numpy as np
from numpy.linalg import solve

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)


n=4
k=np.zeros((n+1,n+1))
m=np.zeros((n+1,n+1))
d=np.zeros((n+1,n+1))

#   0
#   o  k=1
#   1 oooo 2 oooo 3 oooo 4 
#

k[1,0]=k[0,1]=1.0
k[2,1]=k[1,2]=1.0
k[3,2]=k[2,3]=1.0
k[4,3]=k[3,4]=1.0

d[0,1]=0.0
d[1,2]=-1.0
d[2,3]=-1.0
d[3,4]=-1.0

for i in range(n+1):
     for j in range(i):
          d[i,j]=-d[j,i]

print("equilibrium distance matrix d=")
print(d)
print("")

for i in range(n+1):
     for j in range(n+1):
          if not i==j:
               m[i,j]=k[i,j]
          else:
               m[i,j]=-sum([ k[i,l] for l in range(n+1) if not l==i ])

#m[0,0]=m[0,0]=0.0

print("matrix m:")
print(m)
print("") 

y=np.zeros(n+1)

for i in range(n+1):
     y[i] = -sum([ k[i,j]*d[i,j] for j in range(n+1) if not j==i ])

m[0,0]+=1.0

print(m)
print("")

#print np.dot(m,y)

print("rhs vector:")
print(y)
print("")
x=solve(m,y)
print("solution:",x)

print(np.dot(m,x))




