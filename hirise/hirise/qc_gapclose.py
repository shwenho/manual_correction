#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys
import argparse
#from string import str.maketrans 
tr = str.maketrans("ACTGactg","TGACtgac")

def rc(s):
    r=s[::-1]
    rc = r.translate(tr)
    return(rc)

parser = argparse.ArgumentParser()

#p:      1       Scaffold103781_1.5      0       6       160563742       106895697       3372    ['6424.0', '6', '-', '160560371', '160563742', '1', '3372', '3372']

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
#parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-p','--pad',default=False,type=int)
parser.add_argument('-g','--gapclose')

args = parser.parse_args()
#if args.progress: print  "#",args

xc={}
while True:
     l=sys.stdin.readline()
     if not l: break
     if l[0]=="#": continue
     c=l.strip().split()
     end,ch,x = c[2],c[4],int(c[5]) 
     xc[end] = (ch,x)

f=open(args.gapclose)
bumper=args.pad
while True:
     l=f.readline()
     if not l: break
     if l[0]=="#": continue
     sc,e1,k1,e2,k2,closure = l.strip().split()
     chr1,x1 = xc.get(e1,[-1,-1])
     chr2,x2 = xc.get(e1,[-1,-1])
     if chr1==chr2:
          if x1<x2:
               print("GRCh38_chr{}:{}-{}".format(chr1,x1-bumper,x2+bumper),closure,e1,e2)
          else:
               print("GRCh38_chr{}:{}-{}".format(chr1,x2-bumper,x1+bumper),rc(closure),e1,e2)
               
#     print e1,"\t".join(map(str,xc.get(e1,[-1,-1]))),e2,"\t".join(map(str,xc.get(e2,[-1,-1]))),closure

#139     Scaffold100298_1.3      GTAAACAGTTAAAAAGTTTCTTCTCAACCTATTTTTTTTTTTTTTTTTTTT     Scaffold6275_1.5        TTTTTTTTTTTTAGATGGAGTTTCACTCTTGTTGCCCAGGCTGGAGTGCAT     GTAAACAGTTAAAAAGTTTCTTCTCAACCTATTTTTTTTTTTTTTTTTTTTAGATGGAGTTTCACTCTTGTTGCCCAGGCTGGAGTGCAT     
