#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys

d = {}
f = {}

group_col = int(sys.argv[1])-1
val_col = int(sys.argv[2])-1

while True:
    l = sys.stdin.readline()    
    if not l: break
    if l[0]=='#':
        continue
    c = l.strip().split()
    if c[group_col] not in d or float(c[val_col]) > d[c[group_col]]:
        d[c[group_col]] = float(c[val_col])
        f[c[group_col]] = l.strip()

for v in list(f.keys()):
    print(f[v])
