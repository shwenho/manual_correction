#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from builtins import object
from past.utils import old_div

debug=True
debug=False


def vertex_list_to_edge_list(vertex_list):
    el = []
    if not vertex_list:
        return ()
    for i in range(1,len(vertex_list)):
        el.append( (vertex_list[i-1],vertex_list[i]) )
    el.append( (vertex_list[i],vertex_list[0]) )
    return tuple(el)

def isLeft(e,p):
    return( (e[1][0]-e[0][0])*(p[1]-e[0][1]) - (p[0]-e[0][0])*(e[1][1]-e[0][1]) )

def area_under_segment(p1,p2):
    base = p2[0]-p1[0]
    height = 0.5*(p2[1]+p1[1])
    return height*base
#    return a

def polygon_area(vertex_list):
    el = vertex_list_to_edge_list(vertex_list)
    a=0.0
    for p1,p2 in el:
        base = p2[0]-p1[0]
        height = 0.5*(p2[1]+p1[1])
        a += base*height
    return a

class Polygon(object):
    def __init__(self,vertex_list):
        self.vertices = tuple(vertex_list)
        if self.area()<0.0:
            self.vertices = tuple(vertex_list[::-1])
        if self.area()<0.0:
            print("wtf?")
            exit(0)
#            print vertex_list
#            print "negative area"
#            exit(0)
        self.edges=vertex_list_to_edge_list(self.vertices)

    def __repr__(self):
        return str(self.vertices)

    def area(self):
        return polygon_area(self.vertices)

    def encloses(self,p):
        wn=0
        for e in self.edges:
            if e[0][1] <= p[1]:
                if e[1][1] > p[1]:
                    if isLeft(e,p)>0:
                        wn+=1
            else:
                if e[1][1]<=p[1]:
                    if isLeft(e,p)<0:
                        wn-=1
        if wn==0:
            return False
        return True

    def rescale(self,minx,miny,maxx,maxy,f,g):

        self.scale_x = old_div(g,(maxx-minx))
        self.scale_y = old_div(g,(maxy-miny))
        bits=5

        fudged_vertices = []
        
        i=0
        for x,y in self.vertices:
            x = float(x)-minx
            y = float(y)-miny


            x = (old_div(x,(maxx-minx)))
            y = (old_div(y,(maxy-miny)))


            x = int(g*x)
            y = int(g*y)

            # This fudges the coordinates in a way that guaruntees there aren't degenerate cases.
            x = x&~7 | f | 1&i
            y = y&~7 | f 
            i+=1
            fudged_vertices.append( (x,y ) )
        fudged_vertices[0] = ( fudged_vertices[0][0], fudged_vertices[0][1]+ (len(self.vertices)&1) )
        self.vertices = tuple( [ (float(v[0])*(maxx-minx)/g,float(v[1])*(maxy-miny)/g) for v in fudged_vertices] )
#        self.vertices = tuple( [ tuple(v) for v in fudged_vertices ])
        #print self.vertices
        self.edges=vertex_list_to_edge_list(self.vertices)
        
        


def cross_product(s1,s2):
    return( float(s1[0])*s2[1] - float(s1[1])*s2[0]  )

def dot_product(s1,s2):
    return( float(s1[0])*s2[0] + float(s1[1])*s2[1]  )

def scalar_product(x,s2):
    return( (float(x)*s2[0] , float(x)*s2[1])  )

def vdifference(s1,s2):
    return( (s1[0]-s2[0] , s1[1]-s2[1])  )

def vsum(s1,s2):
    return( (s1[0]+s2[0] , s1[1]+s2[1])  )

def segments_to_new_segments(s1,s2):
    """ returns ((new segments replacing 1),(new segments replacing 2),(intersection vertices)) """


    p = s1[0]
    r = vdifference(s1[1],s1[0])
    pr=s1[1]
    q = s2[0]
    s = vdifference(s2[1],s2[0])
    qs = s2[1]

    if p==pr:
        return((),(),())
    if q==qs:
        return((),(),())

    new_segments=[]
#    result=[]

    if cross_product(r,s) == 0.0:  # lines are parallel
        if cross_product(vdifference( q,p ),r)==0.0:  # lines are co-linear
            q_vs_pr  = dot_product(vdifference(q ,p),r)            
            qs_vs_pr = dot_product(vdifference(qs,p),r) 
            q_captured=False
            qs_captured=False

            if 0.0 <= q_vs_pr and q_vs_pr <= dot_product(r,r):
                q_captured=True

            if 0.0 <= qs_vs_pr and qs_vs_pr <= dot_product(r,r):
                qs_captured=True

            p_vs_qs  = dot_product(vdifference(p ,q),s)            
            pr_vs_qs = dot_product(vdifference(pr,q),s) 
            p_captured=False
            pr_captured=False
            if 0.0 <= p_vs_qs and p_vs_qs <= dot_product(s,s):
                p_captured=True

            if 0.0 <= pr_vs_qs and pr_vs_qs <= dot_product(s,s):
                pr_captured=True

            if debug: print((q_captured , qs_captured, p_captured, pr_captured ))
            if (q_captured , qs_captured, p_captured, pr_captured ) == (True,True,False,False): #  p-------------------->pr
#               return( ((p,q),(q,qs),(qs,pr) ) ,(q,qs))                                        #       q------>qs
                return( ((p,q),(q,qs),(qs,pr)),((q,qs),) ,(q,qs))                                      #       q------>qs

            if (q_captured , qs_captured, p_captured, pr_captured ) == (False,False,True,True): #  q--------------------->qs
#               return( ((q,p),(p,pr),(pr,qs) ), (p,pr) )                                       #       p--------->pr
                return( ((p,pr),),((q,p),(p,pr),(pr,qs) ), (p,pr) )                                       #       p--------->pr

            if (q_captured , qs_captured, p_captured, pr_captured ) == (True,False,False,True): #   p------------------>pr
#               return( ((p,q),(q,pr),(pr,qs) ) ,(q,pr))                                        #        q------------------->qs
                return( ((p,q),(q,pr)),((q,pr),(pr,qs) ) ,(q,pr))                                   #        q----------------->qs

            if (q_captured , qs_captured, p_captured, pr_captured ) == (False,True,True,False): #   p------------------->pr
#               return( ((q,p),(p,qs),(qs,pr) ) , (p,qs) )                                      # q----------->qs
                return( ((p,qs),(qs,pr)),((q,p),(p,qs)),  (p,qs) )                                      # q----------->qs

            if (q_captured , qs_captured, p_captured, pr_captured ) == (False,True,False,True): #       pr<-------------p
#               return( ((q,pr),(qs,pr),(pr,qs),(p,qs) ) , (pr,qs))                             # q----------->qs
                return( ((p,qs),(qs,pr)),((q,pr),(pr,qs)), (pr,qs))                             # q----------->qs

            if (q_captured , qs_captured, p_captured, pr_captured ) == (True,False,True,False): #   pr<-------------------p
#               return( ((p,q),(q,pr),(q,p),(p,qs) ) ,( p,q ) )                                 #                    q----------->qs
                return( ((p,q),(q,pr)),((q,p),(p,qs) ) ,( p,q ) )                                 #                    q----------->qs

    else:
        if pr==q:
            return (((p,pr),),((q,qs),),(pr,))
        if p==qs:
            return (((p,pr),),((q,qs),),(qs,))
        if p==q:
            return (((p,pr),),((q,qs),),(p,))
        if pr==qs:
            return (((p,pr),),((q,qs),),(qs,))
        t = old_div(cross_product(vdifference(q,p),s),cross_product(r,s))
        u = old_div(cross_product(vdifference(q,p),r),cross_product(r,s))
        if 0.0<=t and t<=1.0 and 0.0<=u and u<=1.0:
            i = vsum(p,scalar_product(t,r))
            if debug: print("WW:",t,u,i)
            return( ((p,i),(i,pr)),((q,i),(i,qs)), (vsum(p,scalar_product(t,r)),) )
            #    return( ((p,pr),(q,qs)),tuple([]))
    return (False,False,False)





def segment_intersection_vertices_no_degeneracy(s1,s2):
#    print s1,s2
    p = s1[0]
    r = vdifference(s1[1],s1[0])
    q = s2[0]
    s = vdifference(s2[1],s2[0])
    A = cross_product(r,s)

    if A == 0.0:  # lines are parallel
        return (False,False)
        raise Exception("tried to find intersection of parallel segments using a routine that assumes they aren't parallel")
    else:
        t = old_div(cross_product(vdifference(q,p),s),A)
        u = old_div(cross_product(vdifference(q,p),r),A)
        if 0.0<=t and t<=1.0 and 0.0<=u and u<=1.0:
            return (  (vsum(p,scalar_product(t,r)), A)  )
    return (False,False)

def segment_intersection_vertices(s1,s2):
#    print s1,s2
    p = s1[0]
    r = vdifference(s1[1],s1[0])
    q = s2[0]
    s = vdifference(s2[1],s2[0])

    result=[]

    if cross_product(r,s) == 0.0:  # lines are parallel
        if cross_product(vdifference( q,p ),r)==0.0:  # lines are co-linear
            q_vs_pr  = dot_product(vdifference(q,p),r)            
            qs_vs_pr = dot_product(vdifference(s2[1],p),r) 
            if 0.0 <= q_vs_pr and q_vs_pr <= dot_product(r,r):
                result.append( q )
            if 0.0 <= qs_vs_pr and qs_vs_pr <= dot_product(r,r):
                result.append( s2[1] )
            
            p_vs_qs  = dot_product(vdifference(p,q),s)            
            pr_vs_qs = dot_product(vdifference(s1[1],q),s) 
            if 0.0 <= p_vs_qs and p_vs_qs <= dot_product(s,s):
                result.append( p )
            if 0.0 <= pr_vs_qs and pr_vs_qs <= dot_product(s,s):
                result.append( s1[1] )
    else:
        t = old_div(cross_product(vdifference(q,p),s),cross_product(r,s))
        u = old_div(cross_product(vdifference(q,p),r),cross_product(r,s))
        if 0.0<=t and t<=1.0 and 0.0<=u and u<=1.0:
            result.append(  vsum(p,scalar_product(t,r))  )
    return result

def segments_intersect(s1,s2):
    a1 = polygon_area( ( s1[0],s1[1],s2[0]) )
    a2 = polygon_area( ( s1[0],s2[1],s1[1]) )
    a3 = polygon_area( ( s2[0],s2[1],s1[1]) )
    a4 = polygon_area( ( s2[0],s1[0],s2[1]) )
    if a1 > 0 and a2 > 0 and a3 > 0 and a4 > 0:
        return True
    if a1 < 0 and a2 < 0 and a3 < 0 and a4 < 0:
        return True
    return False


class Intersector(object):
    def __init__(self,polygon1,polygon2):
        self.polygon1 = polygon1
        self.polygon2 = polygon2
        self.intersections = self.edge_intersections()
        self.a="Not computed"
        self.int="Not computed"



    def area2(self):

        if debug: print("area2")


        minx = min( [ x[0] for x in self.polygon1.vertices + self.polygon2.vertices ] )
        maxx = max( [ x[0] for x in self.polygon1.vertices + self.polygon2.vertices ] )
        miny = min( [ x[1] for x in self.polygon1.vertices + self.polygon2.vertices ] )
        maxy = max( [ x[1] for x in self.polygon1.vertices + self.polygon2.vertices ] )
        if debug: print(((minx,miny),(maxx,maxy)))

        g = 100000000.0
        self.polygon1.rescale(minx,miny,maxx,maxy,0,g)
        self.polygon2.rescale(minx,miny,maxx,maxy,1,g)

        if debug:
            print("var t=rotate(-90);")
            print("size(1000,1000);")
            print("pen Dotted(pen p=currentpen) {return linetype(new real[] {0,3})+2*linewidth(p);}")
            print("pen v = Dotted();")
            for e in self.polygon1.edges:
                print("draw(t*(",e[0],"--",e[1],"),black,arrow=Arrow);");
            for e in self.polygon2.edges:
                print("draw(t*(",e[0],"--",e[1],"),black,arrow=Arrow);");



        a = 0.0
        inside = {}
        for p1,p2 in self.polygon1.edges:
#            print self.polygon2.vertices, v
            if self.polygon2.encloses(p1):
                inside[p1]=1
                #print "aus",p1,p2
                a += area_under_segment(p1,p2)
                if debug: print("draw(t*(",p1,"--",p2,"),red+2,arrow=Arrow);");

#        print "a:",a


        for p1,p2 in self.polygon2.edges:
#            print self.polygon2.vertices, v
            if self.polygon1.encloses(p1):
                inside[p1]=1
                #print "aus",p1,p2
                a += area_under_segment(p1,p2)
                if debug: print("draw(t*(",p1,"--",p2,"),red+2,arrow=Arrow);");

#        if debug: print inside

        for p1,p2 in self.polygon1.edges:
            for e in self.polygon2.edges:                    
#                if debug: print p1,p2,e
                v,A = segment_intersection_vertices_no_degeneracy((p1,p2),e) #segment_intersection_vertices( (p1,p2),e )
                if v and (A<0):
#                    print "v1:",v,p2,e[1],A
                    a -= area_under_segment(v,p2)
                    a += area_under_segment(v,e[1])
                    if debug: print("draw(t*(",v,"--",e[1],"),green,arrow=Arrow);");
                    if debug: print("draw(t*(",v,"--",p2,"),v+1,arrow=Arrow);");

        for p1,p2 in self.polygon2.edges:
            for e in self.polygon1.edges:
#                if debug: print p1,p2,e
                v,A = segment_intersection_vertices_no_degeneracy((p1,p2),e) #segment_intersection_vertices( (p1,p2),e )
                if v and (A<0):
#                    print "v2:",v,p2,e[1],A
                    a -= area_under_segment(v,p2)
                    a += area_under_segment(v,e[1])
                    if debug: print("draw(t*(",v,"--",e[1],"),green,arrow=Arrow);");
                    if debug: print("draw(t*(",v,"--",p2,"),v+1,arrow=Arrow);");

#        print "a:",a

        #print inside
        return a


        

    def edge_intersections(self):
        intersections = []
        for e1 in self.polygon1.edges:
            for e2 in self.polygon2.edges:
                if debug: print(e1,e2)
                r = segment_intersection_vertices(e1,e2)
                if debug: print(r)
                intersections.extend( r)

        return intersections

    def intersection(self):
        debug=False
        if debug: print("i",self.polygon1, self.polygon2)
        for e in self.polygon1.edges:
            print("draw(t*(",e[0],"--",e[1],"),yellow,arrow=Arrow);");
        for e in self.polygon2.edges:
            print("draw(t*(",e[0],"--",e[1],"),orange,arrow=Arrow);");
            
        vv = set(self.surrounded_vertices())
        if vv == set(self.polygon1.vertices):
            if debug: print("all polygon1 vertices enclosed")
            self.int = self.polygon1.vertices
            return self.polygon1.vertices
        if vv == set(self.polygon2.vertices):
            if debug: print("all polygon1 vertices enclosed")
            self.int = self.polygon2.vertices
            return self.polygon2.vertices

        intersection_edges,intersection_vertices = self.combined_edges()
        if debug: print("vv",vv)
        if debug: print("z",intersection_edges)
        if debug: print("z",intersection_vertices)
        if (not vv) and (not intersection_vertices):
            if debug: print("v",vv)
            if debug: print("i",intersection_vertices)
            self.int = tuple([])
            return 
        vv.update(intersection_vertices)
        
        if debug: print("q",vv)
        vvv=[]
        vv = list(vv)
        if debug: 
            for v in sorted(vv):
                #print "vertices of the intersection polygon:",v
                print("draw(circle(t*(",v,"),1000),purple);");
            for e in sorted(intersection_edges):
                if e[0] in vv:# or e[1] in vv:
                    print("draw(t*(",e[0],"--",e[1],"),green,arrow=Arrow);");

        vvv.append(vv.pop(0))
        while len(vv)>0:
            if debug:            print("vvv:",vvv,vv)
            p=False
            for e in intersection_edges:
                if e[0]==vvv[-1] and e[1] in vv:
                    p=e[1]
            if not p:
                print("ERROR")
                exit(0)
            else:
                vv.remove(p)
                vvv.append(p)
        self.int = tuple(vvv)
        debug=False
        return self.int

    def area(self):
        if self.int == "Not computed":
            self.intersection()
        if self.a == "Not computed":
            self.a = polygon_area(self.int)
        if debug: print(polygon_area(self.polygon1.vertices))
        if debug: print(polygon_area(self.polygon2.vertices))    
        return self.a
        
    def surrounded_vertices(self):
        vv=[]
        for v in self.polygon1.vertices:
            if self.polygon2.encloses(v):
                vv.append(v)
        for v in self.polygon2.vertices:
            if self.polygon1.encloses(v):
                vv.append(v)
        if debug: print(vv)
        return vv
        
    def combined_edges(self):
        to_remove=set([])
        intersections = []
        intersection_vertices=set([])
        combined_edges1=set(self.polygon1.edges)
        combined_edges2=set(self.polygon2.edges)
        e1b = list(self.polygon1.edges)
        checked_edges1=[]        
        while len(e1b)>0:
            e1 = e1b.pop(0)
            e2b = list(combined_edges2)
            checked_edges2=[]
            while len(e2b)>0:
                e2 = e2b.pop(0)
                if debug: print(e1,e2)
                print(len(e1b),len(e2b),e1,e2)
                new_edges1,new_edges2,new_nodes = segments_to_new_segments(e1,e2)
                if new_edges1:
                    new_edges1 = list(new_edges1)
                    new_edges2 = list(new_edges2)
                    print("##",new_edges1,new_edges2,new_nodes)
                    intersection_vertices.update(new_nodes)
                    combined_edges1.difference_update([e1])
                    combined_edges2.difference_update([e2])
                    combined_edges1.update(new_edges1)
                    combined_edges2.update(new_edges2)
                    e1 = new_edges1.pop()
                    for e in new_edges1:
                        if not e == e1:
                            e1b.append(e)
                            print("+++1",e)
                    e2 = new_edges2.pop()
                    for e in new_edges2:
                        if not e == e2:
                            e2b.append(e)
                            print("+++2",e)
        print("XXX")
                            
        combined_edges = set([])
        combined_edges.update(combined_edges1)
        combined_edges.update(combined_edges2)

#        old_edges.update(self.polygon1.edges)
#        old_edges.update(self.polygon2.edges)
#        print "old_edges:", old_edges
        if debug: print("#",[e for e in combined_edges if e[0]==e[1] ])
        self_edges = set([e for e in combined_edges if e[0]==e[1] ] )
#        old_edges.difference_update(to_remove)
#        old_edges.update(combined_edges)
#        old_edges.difference_update(self_edges)
        if debug: print("to remove",to_remove)
        if debug: print("intersection vertices:",intersection_vertices)
        if debug: print("combined edges:",combined_edges)
        return(combined_edges,intersection_vertices)

if __name__=="__main__":
    p1 = Polygon(  ((60061, 110061), (10061, 60061), (-39939, 60061), (60061, 160061))) 
    p2 = Polygon(  ((10000, 160000), (10000, 163000), (160000, 163000), (160000, 160000)))

    p1 = Polygon( ((0.0,0.0),(1.0,0.0),(1.0,1.0),(0.0,1.0)) )
    p2 = Polygon( ((0.5,0.5),(1.5,0.5),(1.5,1.5),(0.5,1.5)) )


    i = Intersector(p1,p2)
    print(i.area2())

    exit(0)


    i.combined_edges()
    i.surrounded_vertices()
    print(i.intersection())
    print(i.intersection())
    print(i.area())
    
    print(i.area2())

    exit(0)
    p1 = Polygon( ((0.0,0.0),(1.0,0.0),(1.0,1.0),(0.0,1.0)) )
    p2 = Polygon( ((0.0,0.5),(0.5,0.5),(0.5,0.9),(0.0,0.9)) )

    i = Intersector(p1,p2)
    i.combined_edges()
    i.surrounded_vertices()
    print(i.intersection())
    print(i.intersection())
    print(i.area())


