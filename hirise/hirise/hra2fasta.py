#!/usr/bin/env python3


import argh
import pysam

def main(fasta, hra, output="/dev/stdout"):

    fasta_obj = pysam.FastaFile(fasta)
    with open(output, 'w') as handle:
        for number, scaffold in enumerate(parse_hra(hra)):
            print(">scaffold_{}".format(number), file=handle)
            for contig, rc in scaffold:
                print("c,rc:",contig,rc)
                seq = get_seq(contig, fasta_obj)
                if rc:
                    seq = reverse_complement(seq)
                with_lines = insert_lines(seq)
                print(with_lines, file=handle)

def reverse_complement(seq):
    trans = dict(zip("ATGCNatgcn\n", "TACGNtacgn\n"))
    return ''.join([trans[base] for base in seq[::-1]])

def insert_lines(seq, line_len=50):
    lines = []
    for i in range(0, len(seq), line_len):
        lines.append(seq[i:i+line_len])
    return "\n".join(lines)

def get_seq(seq, fasta_obj):
    return fasta_obj.fetch(seq)

def parse_hra(hra):
    with open(hra) as handle:
        scaffold_contigs = []
        previous_scaffold = None

        for line in handle:
            if line.startswith("P "):
                scaffold = line.split()[1]
                contig = line.split()[2]
                end = int(line.split()[4])
                if previous_scaffold != scaffold:
                    if previous_scaffold:
                        yield scaffold_contigs
                    previous_scaffold = scaffold
                    scaffold_contigs = []
                scaffold_contigs.append((contig, end==3,))
                next(handle)
        if previous_scaffold:
            yield scaffold_contigs

if __name__ == "__main__":
    argh.dispatch_command(main)
