#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from past.utils import old_div
import sys
import argparse
import numpy as np
import numpy.linalg as la
import networkx as nx
import assembly_io as aio
import hirise.chicago_edge_scores as ces
from greedy_chicagoan2 import traverse_and_layout
from greedy_chicagoan2 import strand_check
import numpy as np
from numpy.linalg import solve
import struct
import hashlib

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-E','--edgefile',default="-")
parser.add_argument('-b','--besthits',default=False)
parser.add_argument('-N','--nchunks',default=128,type=int)
parser.add_argument('-f','--nfact',default=1.0,type=float)
parser.add_argument('-t','--threshold',default=5000.0,type=float)
parser.add_argument('-n','--mychunk',default=1,type=int)
parser.add_argument('-l','--nearestNeighborsOnly',default=False,action="store_true")
parser.add_argument('-M','--set_insert_size_dist_fit_params',default=False )
parser.add_argument('-L','--links')

args = parser.parse_args()
if args.progress: print("#",args)

ces.debug=args.debug

fmodel=open( args.set_insert_size_dist_fit_params )
contents = fmodel.read()
try:
     fit_params=eval(contents)
     fit_params['N']*=args.nfact
except:
     "couldn't deal with option", args.param
fmodel.close
ces.set_exp_insert_size_dist_fit_params(fit_params)

ah=aio.AssemblyReader(args.edgefile)
g=nx.Graph()
ll={}
c2scaffold={}
ah.edges2graph(g,ll,c2scaffold)

besthit={}
if args.besthits:
     besthit=aio.BestHitsReader(args.besthits).besthit

def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False



def qdist(x,y):
    if (not x) or (not y): return (-1)
    if x[0]==y[0]:
        x,y,w,z = list(map(int,[x[2],x[3],y[2],y[3]]))
        ol = pairs_overlap((x,y),(w,z))
        if ol:
            return(-1*min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
        else:
            return(min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
    else:
        return(1.0e12)



def true_gap(c1,c2,bh):
     last_bhi = bh.get(c1,False)
     this_bhi = bh.get(c2,False)
     if this_bhi and last_bhi :
          aa = tuple(last_bhi[1:5])
          bb = tuple(this_bhi[1:5])
          qd = qdist(aa,bb)
          return qd
     return 1e12

to_delete=[]
for e1,e2,data in g.edges(data=True):
     if not data['contig']:
          c1=e1[:-2]
          c2=e2[:-2]
          true = true_gap(c1,c2,besthit)
          if true > args.threshold:
               to_delete.append((e1,e2))

print("n_bad edges:",len(to_delete))
g.remove_edges_from(to_delete)

lengths=[]
for x in nx.connected_components(g):
     l=0
     for e1,e2,data in g.edges(x,data=True):
          l+=data['length']
     lengths.append(l)
lengths.sort(reverse=True)
print(lengths[:10])
G=sum(lengths)
rs=0
i=0
while rs<old_div(G,2):
     rs+=lengths[i]
     i+=1
print("N50:",i,lengths[i])
          

