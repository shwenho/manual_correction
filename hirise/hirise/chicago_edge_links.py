#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import map
from builtins import range
from builtins import object
from past.utils import old_div
import pysam
import sys
import math
import hirise.mapper
from hirise.bamtags import BamTags

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

args=False
links = {}
lastt = 0
slen=[]# s['LN'] for s in seqs ]
snam=[]# s['SN'] for s in seqs ]
G=0.0

def omega(l1,l2,g,d):
    la = min(l1,l2)
    lb = max(l1,l2)
    if d<g:
        if args.debug: print("#omega=0",l1,l2,g,d)
        return 0
    if d<la+g:
        return d-g
    if d<lb+g:
        return la
    if d < la+lb+g:
        return la+lb+g-d
    if args.debug: print("#omega=0 * ",l1,l2,g,d)
    return 0

mean_fragment_size = 15000.0
inverse_fragment_size = old_div(1.0,mean_fragment_size)
one_minus_inverse_fragment_size = 1.0 - old_div(1.0,mean_fragment_size)

minp=1.0e-9

def insert_size_dist(d):
    #if args.debug: print "p(d):",inverse_fragment_size,one_minus_inverse_fragment_size,d,one_minus_inverse_fragment_size**d
    return (inverse_fragment_size)*(one_minus_inverse_fragment_size**d)

#00:  l1-x + y
#01:  l1-x + l2-y
#10:  x+y
#11:  x+l2-y
def fragment_size(l1,l2,o1,o2,coords,g=0):
    x,y = coords[0],coords[1]
    if (o1,o2) == (0,0):
        return (y+l1-x+g)
    if (o1,o2) == (0,1):
        return (l2-y+l1-x+g)
    if (o1,o2) == (1,0):
        return (x+y+g)
    if (o1,o2) == (1,1):
        return (l2-y+x+g)

def logp_lengths(l1,l2,o1,o2,G,pn,links,N,g=0):
    n = (len(links))
    llg = -n * math.log(G)
    for i in range(n):
        d = fragment_size(l1,l2,o1,o2,links[i],g)
        #p_d = max(minp,insert_size_dist(d))
        p_d = insert_size_dist(d)
        if args.debug: print("insert, p_ins:",d,p_d)
        o = omega(l1,l2,g,d)
        if p_d > 0.0 and o > 0.0:
            llg += math.log( old_div(pn,G) + (1.0-pn)*p_d )
            llg += math.log( omega(l1,l2,g,d) )
    return llg

max_frag = 150000
p_not_hit_min = -1

p_not_cache={}

def p_not_a_hit(l1a,l2a,G,g,pn):
    l1,l2 = l1a,l2a
#    r = 1.0
#    if l1==2246
#    r -= pn * (l1/G) * (l2/G)

    r = pn*(1.0 - (old_div(l1,G)) * (old_div(l2,G)))


    r2=0.0
    l1 = min(l1a,max_frag)
    l2 = min(l2a,max_frag)
    if (l1,l2,g) in p_not_cache:
        r2 = p_not_cache[l1,l2,g]
    else:
        r2=1.0
        for d in range(g,l1+l2+g):
            om = float(omega(l1,l2,g,d))
    #        if l1==2246 or om<0.0 or (l1+l2+g-d)<100 : print "pnah",l1,l2,d,r,omega(l1,l2,g,d)
            r2 -= insert_size_dist(d)*(old_div(om,G))
        p_not_cache[l1,l2,g] = r2
    r3=r+(1.0-pn)*r2
#    r=1.0-r
    if args.debug: print("p_not_a_hit:",r,r2,r3)
#    if r < 0.96:        exit(0)

    return r3


#00:  l1-x + y
#01:  l1-x + l2-y
#10:  x+y
#11:  x+l2-y
def link_end_filter(links,o1,o2,l1,l2):
    if (o1,o2)==(0,0):
        links2 = [ (l[0]-max(0,(l1-args.endwindow)),l[1]                    ) for l in links if ((l1-l[0])<args.endwindow) and (l[1]      <args.endwindow) ] 
        return links2
    if (o1,o2)==(0,1):
        links2 = [ (l[0]-max(0,(l1-args.endwindow)),l[1]-max(0,(l2-args.endwindow))) for l in links if ((l1-l[0])<args.endwindow) and ((l2-l[1]) <args.endwindow) ] 
        return links2
    if (o1,o2)==(1,0):
        links2 = [ l for l in links if (l[0]     <args.endwindow) and (l[1]      <args.endwindow) ] 
        return links2
    if (o1,o2)==(1,1):
        links2 = [ (l[0],l[1]-max(0,(l2-args.endwindow))) for l in links if (l[0]     <args.endwindow) and ((l2-l[1]) <args.endwindow) ] 
        return links2

def get_mapq(tags):
    try:
        return tags["xm"]
    except KeyError:
        return tags["MQ"]

def llr_v0(l1,l2,o1,o2,G,pn,links,N,g,p0):
    n = len(links)
    if l1*l2 == 0:
        return 0.0

    #   Optimization:  this term is negligible
    #    p0=0.999999
    #print math.log(p0)
    if args.endsonly:
        if args.debug: print("########################## end only")
        links2 = link_end_filter(links,o1,o2,l1,l2)
        n=len(links2)
        l1 = min(l1,args.endwindow)
        l2 = min(l2,args.endwindow)
        p0 = p_not_a_hit(l1,l2,G,0,pn)
        if args.debug:
            print("")
            print("###",l1,l2,o1,o2,G,pn,N,g,n,links2,p0)
            print("n:",n)
            print("p0:",p0)
            print("log(G*G /  l1*l2):",math.log(G*G /  (l1*l2)))
            print("log(G*G /  l1*l2*pn):",math.log(G*G /  (l1*l2*pn)))
            print("log(p0):",math.log(p0))
            print("log(G*G /  l1*l2*pn)-log(p0):",math.log(G*G /  (l1*l2*pn)) - math.log(p0))
            print("log(n!):",math.log(math.factorial(n)))
            print("( math.log(p0) + pn * (l1/G) * (l2/G))",( math.log(p0) + pn * (old_div(l1,G)) * (old_div(l2,G))))
            print("N *( math.log(p0) + pn * (l1/G) * (l2/G))",N *( math.log(p0) + pn * (old_div(l1,G)) * (old_div(l2,G))))
            print("N *( math.log(p0) )",N *( math.log(p0)))
            print("(n*( (math.log(G*G /  (l1*l2*pn)) - math.log(p0))):",(n*( (math.log(G*G /  (l1*l2*pn)) - math.log(p0)))))
            print("logp_lengths(l1,l2,o1,o2,G,pn,links2,N)",logp_lengths(l1,l2,o1,o2,G,pn,links2,N))
            print("llr_v0:",(n*( (math.log(G*G /  (l1*l2*pn)) - math.log(p0))) + math.log( math.factorial(n)) + N *( math.log(p0) + pn * (old_div(l1,G)) * (old_div(l2,G))) + logp_lengths(l1,l2,o1,o2,G,pn,links2,N)))


        return (n*( (math.log(G*G /  (l1*l2*pn)) - math.log(p0))) + math.log( math.factorial(n)) + N *( math.log(p0) + pn * (old_div(l1,G)) * (old_div(l2,G))) + logp_lengths(l1,l2,o1,o2,G,pn,links2,N))

    else:
        if args.debug:
            print("")
            print("###",l1,l2,o1,o2,G,pn,N,g,links,p0)
            print("n:",n)
            print("p0:",p0)
            print("log(G*G /  l1*l2):",math.log(G*G /  (l1*l2)))
            print("log(G*G /  l1*l2*pn):",math.log(G*G /  (l1*l2*pn)))
            print("log(p0):",math.log(p0))
            print("log(G*G /  l1*l2*pn)-log(p0):",math.log(G*G /  (l1*l2*pn)) - math.log(p0))
            print("log(n!):",math.log(math.factorial(n)))
            print("( math.log(p0) + pn * (l1/G) * (l2/G))",( math.log(p0) + pn * (old_div(l1,G)) * (old_div(l2,G))))
            print("N *( math.log(p0) + pn * (l1/G) * (l2/G))",N *( math.log(p0) + pn * (old_div(l1,G)) * (old_div(l2,G))))

        return (n*( (math.log(G*G /  (l1*l2*pn)) - math.log(p0))) + math.log( math.factorial(n)) + N *( math.log(p0) + pn * (old_div(l1,G)) * (old_div(l2,G))) + logp_lengths(l1,l2,o1,o2,G,pn,links,N))

N=1000000
pn=0.2
G=1000000000
def process(lastt,links):
#    print "#",snam[lastt]
    for x in list(links.keys()):
        if not lastt == x:
            if len(links[x])>0:
                l1,l2 =0,0
                if mapp:
                    l1 = mapp.length[lastt] 
                    l2 = mapp.length[x] 
                else:
                    l1 = slen[lastt]
                    l2 = slen[x]

                if not mapp:
                    print("\t".join(map(str,[snam[lastt]           ,snam[x]           ,l1,l2,len(links[x]),links[x]   ]))) 
                else:
                    print("\t".join(map(str,[mapp.print_name(lastt),mapp.print_name(x),l1,l2,len(links[x]),links[x]   ]))) 

read_length = 100

class readpair(object):
    def __init__(self,a):
        self.tid = a.tid
        self.pos = a.pos
        self.rnext = a.rnext
        self.pnext = a.pnext

        self.otid = a.tid
        self.opos = a.pos
        self.ornext = a.rnext
        self.opnext = a.pnext

        self.is_duplicate = a.is_duplicate
        self.mapq = min(a.mapq, BamTags.mate_mapq(a))
        self.tags = a.tags
    def map(self,mapp):
        x = mapp.map_coord(self.tid,self.pos)
        self.tid = x[0]
        self.pos = x[1]
        x = mapp.map_coord(self.rnext,self.pnext)
        self.rnext = x[0]
        self.pnext = x[1]
    def mask_test(self,mask_ranges,snam):
        if snam[ self.otid ] in mask_ranges :
            for a,b in mask_ranges[ snam[ self.otid ] ]:
                if a<self.opos and self.opos + read_length < b:
                    return True
        if snam[ self.ornext ] in mask_ranges :
            for a,b in mask_ranges[ snam[ self.ornext ] ]:
                if a<self.opnext and ( self.opnext + read_length) < b:
                    return True
        return False

    def __repr__(self):
        return "\t".join(map(str,[self.tid,self.pos,self.rnext,self.pnext,self.mapq,self.is_duplicate,self.tags]))


def mask_test(a,mask_ranges,snam):
        if snam[ a.tid ] in mask_ranges :
            for z,b in mask_ranges[ snam[ a.tid ] ]:
                if z<a.pos and a.pos + read_length < b:
                    print("MASKED!")
                    return True
        if snam[ a.rnext ] in mask_ranges :
            for z,b in mask_ranges[ snam[ a.rnext ] ]:
                if z<a.pnext and ( a.pnext + read_length) < b:
                    print("MASKED!")
                    return True
        return False

    

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-s','--savegraph',default=False)
    parser.add_argument('-R','--mask',default=False)
    parser.add_argument('-S','--restoregraph',default=False)
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-m','--minlength',default=1000,type=int)
    parser.add_argument('-M','--min_insert',default=1000,type=int)
    parser.add_argument('-e','--endsonly',default=False,action="store_true")
    parser.add_argument('-E','--endwindow',default=150000,type=int)
    parser.add_argument('-D','--dotfile',default=False)
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('-c','--cutoff',default=(old_div(200.0,1.0e13)),type=float)
    parser.add_argument('-q','--minquality',default=10.0,type=float)
    parser.add_argument('-L','--contiglist',default=False)
    parser.add_argument('--map_file',default=False)

    nodes={}
    links = {}
    lastt = 0


    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    mask_ranges={}
    if args.mask:
        f = open(args.mask)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            c=l.strip().split()
            mask_ranges[c[0]] = mask_ranges.get(c[0],[]) + [(int(c[1]),int(c[2]))] 
#            cl.append(l.strip())

    cl = []
    if args.contiglist:
        f = open(args.contiglist)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            cl.append(l.strip())
#    print "#",cl

    sam=pysam.Samfile(args.bamfile)
    snam={}
    mapp = False
    if args.map_file:
        import pickle as pickle
        mapp = pickle.load(open(args.map_file,"rb"))

    if args.progress: log( "opened %s" % args.bamfile )

    h=sam.header
    seqs=h['SQ']

    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]

#    name2id={}
#    if args.mask:
#        for i in range(len(snam)):
#            name2id[ snam[i] ] = i

    G = float(sum(slen))

    if args.progress: log(  "built length and name map arrays" )   

    for i in range(len(seqs)):
        if slen[i]>args.minlength:
            nodes[i]=1

    n=0

    if mapp:
        if not args.contiglist:
            def next_alignment():
                for r in mapp.roots:
                    print("#r:", r, type(r))
                for r in mapp.roots:
                    originals = mapp.originals(r)
                    for o in originals:
                        #print "XXXXXXXX",r,o,snam[o]
                        for a in sam.fetch(o):
                            a = readpair(a)
                            a.map(mapp)
                            if a.tid == (r.s,r.x):
                                if (not args.mask) or (not a.mask_test(mask_ranges,snam) ) :
                                    yield a


        else:
            roots = []
            debug_root_count={}
            for c in cl:
                r = mapp.find_node_by_name(c)
                debug_root_count[r]=debug_root_count.get(r,0)+1
                if debug_root_count[r]>1: print("#wtf? two of these?",c,r,debug_root_count[r])
                roots.append(r)
#            print "##",roots
            def next_alignment():
                global links
                for r in roots:
                    print("##",mapp.print_name((r.s,r.x)))
                    originals = mapp.originals(r)
                    for o in originals:
                        #print "ZZZZZZZZZZZZ",r,o,snam[o]
                        for a in sam.fetch(snam[o]):
                            a = readpair(a)
                            a.map(mapp)

                            if a.tid == (r.s,r.x):
                                if (not args.mask) or (not a.mask_test(mask_ranges,snam) ) :
                                    yield a

                    process(lastt,links)
                    links={}


    else:
        if not args.contiglist:
            def next_alignment():
                for a in sam.fetch(until_eof=True):
                    if (not args.mask) or (not mask_test(a,mask_ranges,snam) ) :
                        yield a
        else:
            def next_alignment():
                global links
                for c in cl:
                    if c == '' : continue
                    print("##",c)
                    for a in sam.fetch(c):
                        if (not args.mask) or (not mask_test(a,mask_ranges,snam) ) :
                            yield a
                    process(lastt,links)
                    links={}

    n_processed=0
    for a in next_alignment():
        n+=1

        if a.tid != lastt:
            process(lastt,links)
            n_processed+=1
            if args.head and n_processed > args.head: break
            links={}

        if a.mapq>=args.minquality:
            try:
                if a.tid and a.rnext and (not a.tid == a.rnext) and (not a.is_duplicate) : 
                    d= dict(a.tags)                
                    if get_mapq(d) >=args.minquality:
                        links[a.rnext] = links.get( a.rnext, []) + [(a.pos,a.pnext)] # .append( (a.pos,a.pnext) )
            except:
                sys.stderr.write("{},{},{}\n".format(a.tid,a.rnext,a))
                raise Exception 
                exit(-1)
        lastt = a.tid
    process(lastt,links)


