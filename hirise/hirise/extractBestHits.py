#! /usr/bin/env python3

import pysam
import argh
import os
import sys
from operator import itemgetter
from collections import Counter

def write_fasta(seqs,output,headers,length=100):
    for seq,header in zip(seqs,headers):
        print(">%s" % (header),file=output)
        pos = 0
        seq_len = len(seq)
        while True:
            if pos + length >seq_len:
                print(seq[pos:],file=output)
                break
            print(seq[pos:pos+length],file=output)
            pos += length

def parse_hits(bam):
    curr_query = None
    hits=Counter()
    for aln in bam.fetch(until_eof=True):
       if aln.is_unmapped: continue
       if not curr_query or curr_query.query_name != aln.query_name:
           if hits:
               yield curr_query,sorted(hits.items(),key=itemgetter(1),reverse=True)
               hits = Counter()
           curr_query = aln
       hits[bam.getrname(aln.reference_id)] += 1
    if hits:
        yield curr_query,sorted(hits.items(),key=itemgetter(1),reverse=True)

def get_dir(query):
    if "_" in query:
        query = query.split("_")[0]
    for root,dirnames,filenames in os.walk(os.getcwd()):
        for dirname in dirnames:
            if query in dirname: return dirname
    return "not found"

def check_fasta_index(fn):
    index_path = fn + ".fai"
    if not os.path.isfile(index_path):
        print("No index file found for fasta file: %s\nCreating index." % (fn),file=sys.stderr)
        subprocess.check_call("samtools faidx %s" % (fn),shell=True)
    return

def write_fastas(break_dir,hits,query,ref):
    query_fasta = open(os.path.join(break_dir,"break_region.fasta"),"w")
    hits_fasta = open(os.path.join(break_dir,"aligned_regions.fasta"),"w")
    debug = open(os.path.join(break_dir,"hits.txt"),"w")
    write_fasta([query.query_sequence],query_fasta,[query.query_name])
    if len(hits) == 1:
        targets = [ref.fetch(reference=hits[0][0])]
        headers = [hits[0][0]]
    else:
        targets = [ref.fetch(reference=hits[0][0]),ref.fetch(reference=hits[1][0])]
        headers = [hits[0][0],hits[1][0]]
    write_fasta(targets,hits_fasta,headers)
    print(hits,file=debug)
    query_fasta.close()
    hits_fasta.close()
    debug.close()
    

def main(bam_file,reference):
    bam = pysam.AlignmentFile(bam_file,mode='rb')
    check_fasta_index(reference)
    ref = pysam.FastaFile(reference)
    for query,hits in parse_hits(bam):
        break_dir = get_dir(query.query_name)
        print(break_dir,hits)
        write_fastas(break_dir,hits,query,ref)
    bam.close()
    ref.close()



if __name__ == "__main__":
    argh.dispatch_command(main)
