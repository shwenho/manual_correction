#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse

forward_map={}
reverse_map={}

if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-r','--reverse',default=False,action="store_true",help="Map from integer ids to contig names.  Default is names to ids")
     parser.add_argument('-i','--hra',required=True,default=False,help="File containing '.hra' formatted mapping between (broken) contigs and integer scaffold ids).")
     parser.add_argument("-o", "--output", default="/dev/stdout", help="Output file")
     parser.add_argument("-m", "--map_file", default="/dev/stdin", help="File to map")
     parser.add_argument("-n", "--num", default=2, type=int, help="Number of columns to map.")
     args = parser.parse_args()

     for line in open(args.hra):
          if line.startswith("P"):
               c=line.strip().split()
               contig="{}_{}".format(c[2],c[3])
               scaffold=c[1]
               scaffold_int = int(c[1])
               forward_map[contig] = scaffold
               reverse_map[scaffold] = contig

     with open(args.output, 'w') as output_handle:
          with open(args.map_file) as input_handle:
               for l in input_handle:
                    if l.startswith("#"):
                         continue
                    c = l.strip().split()
                    if args.reverse:
                         lookup = reverse_map
                    else:
                         lookup = forward_map
                    for i in range(args.num):
                         c[i]=lookup[c[i]]
                    print(*c,sep="\t", file=output_handle)
