#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
import pysam
import networkx as nx
import sys
from hirise.bamtags import BamTags


def log(s):
    sys.stderr.write(  "%s\n" %(s) )

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-d','--debug',default=False,action="store_true")
    args = parser.parse_args()

    sam=pysam.Samfile(args.bamfile)

    alniter = sam.fetch(until_eof=True)
    
    lastr=-1
    query_hash={}
    for aln in alniter:
        if lastr>=0 and not lastr==aln.tid:
            print(aln.tid)
            query_hash={}

        if aln.query_name in query_hash:
            if query_hash[aln.query_name]|aln.is_duplicate:
                print(query_hash[aln.query_name],aln.is_duplicate,aln.pnext,aln.pos,aln.is_read1,aln,sep="\t")

        query_hash[aln.query_name]=aln.is_duplicate

        lastr=aln.tid
