#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

ml={}
while True:
     l=sys.stdin.readline()
     if not l: break
     if l[0]=="#": continue
     c = l.strip().split()
     if ml.get(c[0],0)<int(c[4]):
          ml[c[0]]=int(c[4])
for k in list(ml.keys()):
     print(k,ml[k])
