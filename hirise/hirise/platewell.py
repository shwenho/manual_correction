#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys
import argparse

parser = argparse.ArgumentParser()

#oligo:  3       4       f       CACTGACATCGCGGAAGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
#oligo:  3       4       r       ACACTCTTTCCCTACACGACGCTCTTCCGATCTTCCGCGATGTCA   
#oligo:  3       5       f       CACCAGCGTGGGTCAAGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
#


#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

wells = ['A','B','C','D','E','F','G','H']

colmap={ 
     (1,'f'):1,
     (1,'r'):2,
#3
     (2,'f'):4,
     (2,'r'):5,
#6
     (3,'f'):7,
     (3,'r'):8,
#9
     (4,'f'):10,
     (4,'r'):11


  }

while True:
     l=sys.stdin.readline()
     if not l: break
     c = l.strip().split()
     wellid = "{}{}".format(wells[int(c[2])-1],colmap[int(c[1]),c[3]] )
     name   = "{}{}{}".format( c[1],c[3],c[2])
     mod = "/5Phos/"
#     if c[3]=="f" and c[1]=="1":  mod=""
     if c[3]=="r" and c[1]=="3":  mod=""
#     if c[3]=="r" and c[1]=="4":  mod=""
     oligo = c[4]
     if c[3]=="r" and c[1]=="1": 
          oligo = oligo[:-1] + "*/3ddC/"

     print("\t".join([wellid,name,mod+oligo])) 

