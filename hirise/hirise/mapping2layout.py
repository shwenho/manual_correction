#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import map
#!/usr/bin/env python3
import sys
import colorsys
import random

if __name__=="__main__":


    import argparse
    parser = argparse.ArgumentParser()


    parser.add_argument('-m','--map')
    parser.add_argument('-b','--besthits')
    parser.add_argument('-s','--scaffold')
    parser.add_argument('-d','--debug',action="store_true")

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    mapp=False
    if args.map:
        import pickle as pickle
        mapp = pickle.load(open(args.map))

    if args.scaffold:
        n=mapp.find_node_by_name(args.scaffold)
        print("#",n)

    if args.besthits:
        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
            f.close()

#    layout={}

    if args.scaffold:
        it = iter([n])
    else:
        it = mapp.roots

    for r in it:
        rootname=mapp.print_name(r)
#        print "#",rootname

#        print r,r.s,r.x,mapp.print_name(r)

        layout ={}
        chromosomes={}
        n2c={}
        for a,b in mapp.nodepair_list(r):
            if a.x>b.x:
                c=a
                a=b
                b=c

            n=mapp.print_name(a)
            x=a.mapped_coord
            y=b.mapped_coord
            d=y-x
            print("#",a,b,n,x,y,d)
            layout[n]=(x,d)
#            print n,layout[n],besthit.get(n)
            chrom = besthit.get(n,(-1,-1))[1]
            chromosomes[chrom]=1
            n2c[n]=chrom

        color={}
        for c in list(chromosomes.keys()):
            color[c]=colorsys.hls_to_rgb( random.random(), 0.5, 0.5*random.random() + 0.5)
        color[-1]=(0.0,0.0,0.0)

        nn=list(layout.keys())
        nn.sort(key=lambda x: layout[x])
        i=0
        for n in nn:
            i+=1
            print("\t".join(map(str,[layout[n][0],layout[n][1],i%2,'0x{:02x}{:02x}{:02x}'.format( *[int(x*255) for x in color.get(n2c.get(n))]),n,rootname, n2c.get(n), len(layout)])))
            


