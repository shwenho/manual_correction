#!/usr/bin/env python3
from __future__ import print_function


n=1000

total_counts=0.0
n_seps=0.0
while True:
    l=sys.stdin.readline()
    if not l: break
    c = l.strip().split()
    d=int(c[0])
    count=int(c[1])
    
    total_counts += count
    n_seps += 1

    if total_counts >= n:
        print() 

        
