#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
#!/usr/bin/env python3
import sys

w = int(sys.argv[1])

while True:
    l = sys.stdin.readline()
    if not l: break
    if l[0]=='#': continue
    c = l.strip().split()
    if len(c)==0: 
        print("#XXXX:",l.strip())
        continue
    s1 = c[0]
    s2 = c[1]
    l1 = int(c[2])
    l2 = int(c[3])
    n = int(c[4])
    v = eval(" ".join(c[5:]))
    nn=0
    for x,y in v:
        if ((x<w) or ((l1-x)<w)) and ((y<w) or ((l2-y)<w)): nn+=1  
    if nn>0:
        print("\t".join([s1,s2,str(nn)]))




