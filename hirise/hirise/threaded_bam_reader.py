#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import queue
from threading import Thread
import pysam
from hirise.bamtags import BamTags
import gc

def check_references_consistency(bams):
     
     names = []

     for bh in bams:
#          bh = sam=pysam.Samfile(bam)

          h=bh.header
          seqs=h['SQ']
     
#          slen=[ s['LN'] for s in seqs ]
          snam=[ s['SN'] for s in seqs ]
          names.append(tuple(snam))
     
     for i in range(1,len(names)):
          print(i)
          if not names[i]==names[0]:
               raise Exception 

def read_bam_chunk(bam,max_lines=False,max_tid=False,seek=False):
     buffer=[]
     gc.disable()
     if seek: bam.seek(seek)
     for aln in bam.fetch(until_eof=True):
          buffer.append( aln2tuple(aln) )
          if (max_tid and aln.tid > max_tid) or (max_lines and len(buffer)>=max_lines): break

     gc.enable()
     return(bam.tell(),aln.tid,buffer)

def aln2tuple(aln,bam):
     rname = bam.getrname(aln.tid)
     if aln.rnext>=0:
          nextrname = bam.getrname(aln.rnext)
     else:
          nextrname="-"
     return(aln.query_name,aln.tid,aln.pos,aln.rnext,aln.pnext,rname,nextrname,aln.mapq,BamTags.mate_mapq(aln))
     
CHUNK_LINES=1000000

class ChunkedBam:
     def __init__(self,bam,chunk_size=100000):
          self.bam=bam
          self.offset=bam.tell()
          self.chunk_size = chunk_size
          self.last_tid= -1
     def get_next_chunk(self):
          #print(self.bam,self.offset)
          self.bam.seek(self.offset)
          
          gc.disable()
          buffer=[]
          bell=False
          last_tid=False
          for aln in self.bam.fetch(until_eof=True):
               if bell and not aln.tid == last_tid: break
               buffer.append( aln2tuple(aln,self.bam) )
#               print(buffer[-1])
               self.offset=self.bam.tell()

               if len(buffer)>self.chunk_size: 
                    bell=True
                    last_tid = aln.tid
          #print(len(buffer))
          self.last_tid = last_tid
          gc.enable()
          #print(buffer[:3])
          return buffer

     def get_up_to_tid(self,tid):
          #print(self.bam,self.offset)
          self.bam.seek(self.offset)
          
          gc.disable()
          buffer=[]
          last_tid=self.last_tid
          for aln in self.bam.fetch(until_eof=True):
               if aln.tid > tid: break
               buffer.append( aln2tuple(aln,self.bam) )
               self.offset=self.bam.tell()               
               last_tid = aln.tid
          gc.enable()
          self.last_tid = last_tid
          #print(buffer[:3])
          return buffer

def count_reads(q):
     ns=[0,0,0]
     while True:
          buffers = q.get()
#          for i in range(len(buffers)):
          ns = [ ns[i] + len(buffers[i]) for i in range(nbams)  ]
          print( [b[:2] for b in buffers] )
          print(ns)

          q.task_done()


if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
     parser.add_argument('-N','--nthreads',default=8,type=int,help="Number of threahds.")
     parser.add_argument('-b','--bamfiles',action="append",help="Number of threahds.")

     args = parser.parse_args()
     if args.progress: print("#",args)

     bhs          = [ pysam.Samfile(bam) for bam in args.bamfiles ] 
     chunked_bams = [ ChunkedBam(bam) for bam in bhs ] 

     q = queue.Queue(maxsize=0)

     for i in range(args.nthreads):
          worker = Thread(target=count_reads, args=(q,))
          worker.setDaemon(True)
          worker.start()

     nbams=len(bhs)

     ns=[0]*nbams
     while True:
          buffers=[[]]*nbams
          buffers[0]=chunked_bams[0].get_next_chunk()
          for i in range(1,nbams):
               buffers[i] = chunked_bams[i].get_up_to_tid( chunked_bams[0].last_tid )
          q.put(buffers)
#          ns = [ ns[i] + len(buffers[i]) for i in range(nbams)  ]
#          print(ns)

