#!/usr/bin/env python3
from __future__ import print_function
from builtins import map

G=3.0e9
pn=0.3

#def s_approx(l1a,l2a,G,g,pn):


import hirise.chicago_edge_scores as ces

for l1 in (1000,5000,10000,20000,50000):
    for l2 in (1000,5000,10000,20000,50000):
        for g in (1000,2000,5000,10000):
            print(l1,l2,g,ces.s_exact(l1,l2,G,g,pn), "\t".join(map(str,ces.s_approx(l1,l2,G,g,pn))))
