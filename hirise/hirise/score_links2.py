#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
from past.utils import old_div
import sys
import networkx as nx
import hirise.chicago_edge_scores as ces

#G=3.0e9
N=100000000.0
pn=0.3
G=3000000000.0


def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False

def qdist(x,y):
    if (not x) or (not y): return (-1)
    if x[0]==y[0]:
        x,y,w,z = list(map(int,[x[2],x[3],y[2],y[3]]))
        ol = pairs_overlap((x,y),(w,z))
        if ol:
            return(-1*min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
        else:
            return(min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
    else:
        return(1.0e12)


def true_gap(c1,c2,bh):
    last_bhi = bh.get(c1,False)
    this_bhi = bh.get(c2,False)
#    print last_bhi
#    print this_bhi
#    blast_label=str(last_bhi) + str(this_bhi)
    if this_bhi and last_bhi :
        aa = tuple(last_bhi[1:5])
        bb = tuple(this_bhi[1:5])
        qd = qdist(aa,bb)
        return qd
    return 1e12

import math
if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-t','--threshold',default=0.0 ,  type=float)
    parser.add_argument('-b','--besthits')
    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('-g','--estimateGapsizes',default=False ,  action='store_true')
    parser.add_argument('-P','--plot',default=False ,  action='store_true')
    parser.add_argument('-M','--set_insert_size_dist_fit_params',default=False )
    parser.add_argument('-G','--maxTrueGap',type=int,default=False)
    parser.add_argument('-N','--maxN',type=int,default=False)

    args = parser.parse_args()
    ces.debug=args.debug
    if args.debug:
        args.progress=True
    if args.progress: log( str(args) )

    fmodel=open( args.set_insert_size_dist_fit_params )
    contents = fmodel.read()
    try:
        fit_params=eval(contents)
    except:
        "couldn't deal with option", args.param
    fmodel.close
    ces.set_exp_insert_size_dist_fit_params(fit_params)


    besthit={}
    if args.besthits:
#        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
    #            print c[1],besthit[c[1]]
            f.close()
    if args.progress: print("#Done reading besthits")

    n_done=0
    while (not args.maxN) or n_done<args.maxN:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c = l.strip().split("\t")
        s1,s2,l1,l2,n,links = c[0],c[1],int(c[2]),int(c[3]),int(c[4]),eval(c[5])

        truth=max(1,true_gap(s1,s2,besthit))

        if args.maxTrueGap and besthit and truth>args.maxTrueGap: continue
            
        n_done+=1

#        print true_gap(s1,s2,besthit)

        best_gap=0.0
        if args.estimateGapsizes:
            scores = []
            best=(-500.0,-1,(-1,-1))
            for (o1,o2) in ((0,0),(0,1),(1,0),(1,1)):
    #        for (o1,o2) in ((1,1),):
                gap,score=ces.ml_gap(l1,l2,o1,o2,G,pn,links,N,50)
                print("#",gap,score)
                if score>best[0]:
                    best=(score,gap,(o1,o2))
            
            best_gap=best[1]
            print("#best:",s1,s2,l1,l2,n,best[0],best[1],best[2],truth,best[1]-truth, old_div((best[1]-truth),truth))


        if args.plot:
            orientations=[(0,0),(0,1),(1,0),(1,1)]
            l={}
            for (o1,o2) in orientations:
                l[o1,o2]= ces.ll( l1,l2,o1,o2,G,pn,links,N,gaplen=0 )

            for gaplen in range(1,50000,1000):
                p0 = ces.p_not_a_hit(l1,l2,G,gaplen,pn)
                s={}
#                l={}
                for (o1,o2) in orientations:
                    s[o1,o2]= ces.llr_v0( l1,l2,o1,o2,G,pn,links,N,gaplen,p0 )
#                    l[o1,o2]= ces.ll(     l1,l2,o1,o2,G,pn,links,N,gaplen,p0 )
                orientations.sort(key=lambda x: s[x],reverse=True)
                o1,o2 = orientations[0]
                llL = ces.ll(     l1,l2,o1,o2,G,pn,links,N,gaplen,p0 ) -l[o1,o2]
                
                ddg_llr  =ces.model.ddg_llr(   l1,l2,o1,o2,links,gaplen,p0)
                d2dg2_llr=ces.model.d2dg2_llr( l1,l2,o1,o2,links,gaplen,p0)

                print("\t".join(map(str,["gp:",s1,s2,l1,l2,len(links),max(s.values()),llL,gaplen,best_gap,truth,ddg_llr,d2dg2_llr])))
            print("gp:\t\t")
            print("gp:\t\t")


#        for gaplen in range(1,5000,100):# + range(5000,500000,5000): # [ 0, 500, 1000, 2000 , 10000, 20000, 30000, 40000, 100000, 200000, 500000 ]:
        orientations=[(0,0),(0,1),(1,0),(1,1)]
        for gaplen in [50]:

            p0 = ces.p_not_a_hit(l1,l2,G,gaplen,pn)
 
            s={}
            t={}
            q={}
#            for (o1,o2) in (best[2],):
            for (o1,o2) in ((0,0),(0,1),(1,0),(1,1)):
                s[o1,o2]= ces.llr_v0( l1,l2,o1,o2,G,pn,links,N,gaplen,p0 )


#                t[o1,o2]= ces.ddg_llr(l1,l2,o1,o2,G,pn,links,N,gaplen,p0)
#                q[o1,o2]= ces.d2dg2_ll(l1,l2,o1,o2,G,pn,links,N,gaplen,p0)
#                print s1,s2,l1,l2,true_gap(s1,s2,besthit),len(links),o1,o2,gaplen,s[o1,o2] #,t[o1,o2],q[o1,o2]
#            scores.append( max(s.values()))
            orientations.sort(key=lambda x: s[x], reverse=True)
            bo=orientations[0]
            nbo=orientations[1]
            print("\t".join(map(str,[s1,s2,s[bo],l1,l2,true_gap(s1,s2,besthit),len(links),bo,s[bo]-s[nbo]])))


#        print [s1,s2,p0,max(s.values()),l1,l2,n,links]
#        print "\t".join(map(str,[s1,s2,max(scores)]+scores))
