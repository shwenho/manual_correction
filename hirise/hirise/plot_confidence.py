#!/usr/bin/env python3

import sys
import argh
import numpy
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def main(confidence, output="confidence.png"):
    x, y = parse_confidence(confidence)
    plot(x, y, output)

def parse_confidence(confidence_file):
    x = []
    y = []
    with open(confidence_file) as handle:
        for line in handle:
            if line.startswith("#"):
                continue
            s = line.split()
            start = int(s[2])
            end   = int(s[3])
            length = end - start + 1
            conf = float(s[9])
            x.append(length)
            y.append(conf)
    return x, y


def plot_subsection(pdf, x, y, min_val=0, max_val=sys.maxsize):
    short_conf = [j for i, j in zip(x, y) if min_val <= i < max_val]
    if len(short_conf) > 1:
        plt.hist(short_conf, 100)
        plt.xlabel("confidence")
        plt.title("Confidence in {0} <= contig segment length < {1}".format(min_val, max_val))
        pdf.savefig()
        plt.close
        plt.clf()
    

def plot(x, y, output):
    with PdfPages(output) as pdf:
        plt.scatter(x, y, alpha=0.01)
        plt.xscale("log")
        plt.xlabel("Length of Contig")
        plt.ylabel("Confidence")
        plt.title("Confidence in orientation")
        pdf.savefig()
        plt.close()

        plot_subsection(pdf, x, y, 0, 1000)
        plot_subsection(pdf, x, y, 1000, 2000)
        plot_subsection(pdf, x, y, 2000, 5000)
        plot_subsection(pdf, x, y, 5000, 10000)
        plot_subsection(pdf, x, y, 10000, 100000)
        plot_subsection(pdf, x, y, 100000)

if __name__ == "__main__":
    argh.dispatch_command(main)
