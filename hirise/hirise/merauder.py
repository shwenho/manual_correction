#!/usr/bin/env python3

import subprocess
import argh
import hirise
import os

@argh.arg("args")
def main(args):
    hirise_dir = os.path.dirname(hirise.__file__) 
    audit = os.path.join(hirise_dir, "merauder")
    cmd = [audit] + args.split()
    subprocess.Popen(cmd)

if __name__ == "__main__":
    argh.dispatch_command(main)
