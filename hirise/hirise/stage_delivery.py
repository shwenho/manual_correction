#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import glob
import re
import json
import os
import shutil

import subprocess

from hirise.n50stats import n50, n50list

from datetime import date

import subprocess


from hirise.n50stats import n50, n50list, l50


def get_id(fastafile):
     for line in open(fastafile):
          if line.startswith(">"):
               m=re.match(">Sc(.*)_",line)
               if m:
                    return m.group(1)

def add_readme_file(dirname,id):
     f=open( os.path.join(dirname,"README.txt") ,"wt")
     f.write("""FILES:

{0}.fasta:

This file contains the sequences of HiRise scaffolds for the Danish tumbler project, in FASTA format.

{0}.table

This file contains the layout of input contigs in the scaffolds.  The columns are:

1. HiRise scaffold name
2. Input sequence name
3. Starting base (zero-based) of the input sequence
4. Ending base of the input sequence
5. Strand (- or +) of the input sequence in the scaffold
6. Starting base (zero-based) in the HiRise scaffold
7. Ending base in the HiRise scaffold

""".format(id))
     f.close()

def make_rst(project, stats, config, dirname, id):
     rst = os.path.join(dirname, id + "report.rst")
     latex = os.path.join(dirname, id + "report.tex")
     orig_pdf = os.path.join(dirname, id + "report.pdf")

     with open(rst, 'w') as out:
          print(project, file=out)
          print(100*"=", file=out)
          print("Dovetail Assembly", file=out)
          print(100*"-", file=out)
          print(".. raw:: latex\n\n    \\newpage\n", file=out)


          final_n50 = n50(open(fasta_file+".length"),50.0,column=0)
          final_n90 = n50(open(fasta_file+".length"),90.0,column=0)
          total_length_final = sum([ int(c.split()[0]) for c in open(fasta_file+".length") ])
          starting_n50 = n50(open("raw_lengths.txt"),50.0,column=1)
          starting_n90 = n50(open("raw_lengths.txt"),90.0,column=1)



def make_rst(project, stats, config, dirname, id, url, logo):
     rst = os.path.join(dirname, id + "_report.rst")
     latex = os.path.join(dirname, id + "_report.tex")
     orig_pdf = os.path.join(dirname, id + "_report.pdf")

     with open(rst, 'w') as out:
          title = " ".join([word.title() for word in project.split("_")])
          print(".. class:: center\n\n", file=out)
          print(title, file=out)
          print(100*"=", file=out)
          print("\n\n", file=out)
          print("Dovetail Assembly", file=out)
          print(100*"-", file=out)
          print("\n\n", file=out)

          final_n50 = n50(open(fasta_file+".length"),50.0,column=0)
          final_l50 = l50(open(fasta_file+".length"),50.0,column=0)
          final_n90 = n50(open(fasta_file+".length"),90.0,column=0)
          final_l90 = l50(open(fasta_file+".length"),90.0,column=0)
          total_length_final = sum([ int(c.split()[0]) for c in open(fasta_file+".length") ])
          starting_n50 = n50(open("raw_lengths.txt"),50.0,column=1)
          starting_l50 = l50(open("raw_lengths.txt"),50.0,column=1)
          starting_n90 = n50(open("raw_lengths.txt"),90.0,column=1)
          starting_l90 = l50(open("raw_lengths.txt"),90.0,column=1)

          total_length_start = sum([ int(c.split()[1]) for c in open("raw_lengths.txt") ])


          print("Improvement Results", file=out)
          print(100*"-", file=out)
          print("\n\n\n", file=out)

          chicago_coverage = stats['chicago_coverage_1_50']

          print("Estimated Dovetail library coverage (1-50 Kb pairs): ", round(chicago_coverage, 1), file=out)
          print("\n\n", file=out)


          table = [["Statistic", "Starting Assembly", "Final Assembly"], 
                   ["Total length", str(total_length_start), str(total_length_final)],
                   ["N50 Length", str(starting_n50), str(final_n50)],
                   ["N90 Length", str(starting_n90), str(final_n90)]]

          print(make_table(table, invert=False), file=out)
          print("\n\n\n", file=out)
          print(".. raw:: latex\n\n    \\newpage\n\n", file=out)

          print(".. figure::", "n50splot.pdf\n", file=out)
          print(".. raw:: latex\n\n    \\newpage\n\n", file=out)

          print("Data access: ", file=out)

     process = subprocess.Popen(["rst2latex.py", rst, latex])

          print("Estimated Dovetail library coverage (1-50 Kb pairs): ", str(round(chicago_coverage, 1)) + "X", file=out)
          print("\n\n", file=out)


          template = "{0} scaffolds ; min length {1}"
          table = [["Statistic", "Starting Assembly", "Final Assembly"], 
                   ["Total length", to_megabase(total_length_start), to_megabase(total_length_final)],
                   ["N50 Length", template.format(starting_l50, to_megabase(starting_n50)), template.format(final_l50, to_megabase(final_n50)) ],
                   ["N90 Length", template.format(starting_l90, to_megabase(starting_n90)), template.format(final_l90, to_megabase(final_n90))]]

          print(make_table(table, invert=False), file=out)
          print("\n\n\n", file=out)

          print(".. figure::", "n50splot.pdf\n", file=out)

          if url:
               print("\n\nDropbox: (final scaffolds, sequence and contig layout", file=out)
               full_url = subprocess.Popen(["dropbox puburl", url])
               print(full_url, file=out)

          print("\n\n\n\n\n", file=out)
          print(".. image:: ", logo, file=out)
          print("    :height: 50px", file=out)
          print("    :width: 100px", file=out)
          print("    :align: left", file=out)
          
     process = subprocess.Popen(["rst2latex.py", "--latex-preamble=\\usepackage[margin=0.5in]{geometry}", rst, latex])

     process.wait()
     process = subprocess.Popen(["pdflatex", "--output-directory", dirname, latex])
     process.wait()



     for intermediate_file_suffix in ("_report.rst","_report.tex","_report.out","_report.aux","_report.log"):
          os.remove(os.path.join(dirname,id+intermediate_file_suffix))

def to_megabase(num):
     num_mb = round(num / 1000000, 3)
     if num_mb > 1:
          num_mb =  round(num_mb, 2)
     if num_mb > 10:
          num_mb = round(num_mb, 1)

     return str(num_mb) + " Mb"



def make_table(grid, invert=False):
    if invert:
        grid = list(zip(*grid))
    max_cols = [max(out) + 5 for out in map(list, zip(*[[len(item) for item in row] for row in grid]))]
    rst = table_div(max_cols, 1)

    for i, row in enumerate(grid):
        header_flag = False
        if i == 0 or i == len(grid)-1:
            header_flag = True
        rst += normalize_row(row,max_cols)
        rst += table_div(max_cols, header_flag )
    return rst

def table_div(max_cols, header_flag=1):
    out = ""
    if header_flag == 1:
        style = "="
    else:
        style = "-"

    for max_col in max_cols:
        out += max_col * style + " "

    out += "\n"
    return out


def normalize_row(row, max_cols):
    r = ""
    for i, max_col in enumerate(max_cols):
        r += row[i] + (max_col  - len(row[i]) + 1) * " "

    return r + "\n"


if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument("-u", "--url", default=None, help="Path to dropbox Public directory")
     parser.add_argument("-l", "--logo", default="/mnt/nas/copy_ext_data/dovetail/dovetail-logo.png", help="Dovetail logo")
     parser.add_argument("-f", "--fasta", default=None, help="Fasta file")
     args = parser.parse_args()

     config = json.load(open("config.json"))
     stats = json.load(open("stats.json"))

     project=config['project']
     print(project)

     fasta_file = args.fasta
     if not fasta_file:
          fasta_files=[]
          for f in glob.glob("hirise_*.gapclosed.fasta"):
               #          print(f)
               fasta_files.append(f)

          if not fasta_files:
               for f in glob.glob("hirise_*.fasta"):
                    #          print(f)
                    fasta_files.append(f)
     
          fasta_file = fasta_files[0]
     table_file = re.sub(".fasta$",".table",fasta_file)
     print(table_file)


     print(stats['input_n50'])
     final_n50 = n50(open(fasta_file+".length"),50.0,column=0)
     final_n90 = n50(open(fasta_file+".length"),90.0,column=0)
     total_length_final = sum([ int(c.split()[0]) for c in open(fasta_file+".length") ])
     starting_n50 = n50(open("raw_lengths.txt"),50.0,column=1)
     starting_n90 = n50(open("raw_lengths.txt"),90.0,column=1)
     total_length_start = sum([ int(c.split()[1]) for c in open("raw_lengths.txt") ])
     chicago_coverage = stats['chicago_coverage_1_50']


     print(final_n50,final_n90,starting_n50,starting_n90)
     print(total_length_final,total_length_start)
     print(chicago_coverage)

     print(fasta_files)

     id =   project+"_"+get_id(fasta_file)


     id =   project+"_"+date.today().strftime("%d%b%Y")+"_"+get_id(fasta_file)


     print(id)
     
     dirname=id
     os.mkdir(dirname)
     shutil.copy(fasta_file,os.path.join(dirname,id+".fasta"))
     shutil.copy(table_file,os.path.join(dirname,id+".table"))

     add_readme_file(dirname,id)
     
     print("Copy {} to {}".format(fasta_file,id+".fasta"))
     print("Copy {} to {}".format(table_file,id+".table"))
     
     print("make RST")

     make_rst(project, stats, config, dirname, id)

     make_rst(project, stats, config, dirname, id, args.url, args.logo)

     print(id)


#     cmd="make_n50s_plot.py --simple -l hirise_iter_broken_3.fasta.length n50splot.pdf | gnuplot"
