#!/usr/bin/env python3
#!/usr/bin/env python3
"""
Author:         Jonathan Stites 
Date:           21 January 2015
Company:        Dovetail Genomics

Takes as input a file of edges and outputs the smallest min_link_t
threshold such that the largest connected component contains less than
f (default 0.05) fraction of all contigs (if threshold given is 'auto'. 
Otherwise, simply writes given threshold to file.).

Writes min_link_t to min_link_t.txt. 
"""
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import str
from past.utils import old_div


import sys
import networkx as nx
import hirise.chicago_edge_scores as ces

    
def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False

def qdist(x,y):
    if (not x) or (not y): return (-1)
    if x[0]==y[0]:
        x,y,w,z = list(map(int,[x[2],x[3],y[2],y[3]]))
        ol = pairs_overlap((x,y),(w,z))
        if ol:
            return(-1*min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
        else:
            return(min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
    else:
        return(1.0e12)



def pledge_singletons2(g,sg,thresh=2):
    singletons=[]
    ccn=0
    comp={}
    for c in nx.connected_components(sg):
        ccn+=1
        if len(c)==1: singletons.append(c[0])
        for cc in c: comp[cc]=ccn
    for s in singletons:
        total_weight_by_comp={}
        links_by_comp=[]
        exemplar_by_comp={}
        for n in g.neighbors(s):
            ncomp = comp.get(n,-1)
            w = abs(g[s][n]['weight'])
            if w >= thresh:
                links_by_comp.append( (w,ncomp) )
                exemplar_by_comp[ncomp]=n

        links_by_comp.sort(reverse=True)
        #print "#pledge_stat:",links_by_comp
        if len(links_by_comp)==0: continue
        if len(links_by_comp)==1 or links_by_comp[0][1]==links_by_comp[1][1]:
            n = exemplar_by_comp[links_by_comp[0][1]]
            sg.add_edge( s, n, {'weight': -1} )
            

def get_args():

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-t','--threshold',default="auto")
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-M','--maxdegree',default=False,type=int)
    parser.add_argument('-m','--minlength',default=500,type=int)

    parser.add_argument('-K','--cutPromisc',default=False,action='store_true')
    parser.add_argument('-C','--cheat',default=False,action='store_true')
    parser.add_argument('-B','--blacklist')

    parser.add_argument('-b','--besthits')
    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')
    parser.add_argument('-L','--maxLength',type=float,default=150000.0)
    parser.add_argument('-P','--promisc',type=float,default=0.023)
    parser.add_argument('-o','--output',default="min_link_t.txt")
    parser.add_argument("-f", default=0.05)
    args = parser.parse_args()
    if args.debug:
        args.progress=True
    if args.progress: log( str(args) )
    #print "#"+str(args)



    return args



def make_graph(args):
    G=nx.Graph()
    SG=nx.Graph()

    ll = get_lengths(args, G, SG)
    print("lengths done")

    besthit = get_besthits(args, G, SG)
    print("besthits done")

    add_edges(args, G, SG, ll)
    print("adding edges done")

    n_discarded1, n_discarded2, total_discarded_length = remove_bad_edges(args, G, SG, ll)
    print("removed bad edges")

    cut_promisc(args, G, SG, ll, n_discarded1, total_discarded_length)
    print("cut promisc done")

    blacklist(args, G, SG)
    print("blacklist done")

    cheat(args, G, SG, besthit)
    print("cheat done")

    print_promisc(args, G, SG)
    pledge_singletons2(G,SG,2)

    return G, SG

def print_promisc(args, S, SG):
    if not args.debug:
        return
    promisc = {}
    for n in SG.nodes():
        if args.debug: print("#r:",old_div(float(SG.degree(n)),ll[n]))
        if (old_div(float(SG.degree(n)),ll[n]))>args.promisc: promisc[n]=True


def get_lengths(args, G, SG):
    ll={}
    if args.lengths:
        f = open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            l = int(c[1])
            ll[c[0]]=int(c[1])
            if l>= args.minlength:
                G.add_node(c[0])
                SG.add_node(c[0])
        f.close()
    if args.progress: print("#Done reading lengths")
    return ll


def get_besthits(args, G, SG):
    besthit={}
    if args.besthits:
#        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
    #            print c[1],besthit[c[1]]
            f.close()
    return besthit
    if args.progress: print("#Done reading besthits")


def add_edges(args, G, SG, ll):
    if args.edgefile:
        f = open(args.edgefile)
    else:
        f=sys.stdin
    while True:
        l = f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        u,v,w = c[0],c[1],float(c[2])
        if ( not args.lengths ) or (ll[u]>=args.minlength and ll[v]>=args.minlength):
            G.add_edge(u,v,weight=-w)
            SG.add_node(u)
            SG.add_node(v)
            if w >= args.threshold:
                SG.add_edge(u,v,weight=-w)
    if args.edgefile:
        f.close()
    if args.progress: print("#Done reading edgelist")

def remove_bad_edges(args, G, SG, ll):

    bad_nodes=[]
    total_discarded_length=0
    total_discarded_length1=0
    n_discarded1=0
    n_discarded2=0
    if args.maxdegree:
        for n in SG.nodes():
            #print "#dg:", SG.degree(n)
            if SG.degree(n)>args.maxdegree:
                n_discarded1+=1
                bad_nodes.append(n)
                total_discarded_length += ll[n]
                
                #print "#discard:",n,ll[n],SG.degree(n)
                for nn in SG.neighbors(n):
                    if SG.degree(nn)==1:
                        n_discarded2+=1
                        total_discarded_length1+=ll[nn]
        for n in bad_nodes:
            e_to_remove=[]
            for e in SG.edges([n]):
                e_to_remove.append(e)
            SG.remove_edges_from(e_to_remove)

    return n_discarded1, n_discarded2, total_discarded_length

def cut_promisc(args, G, SG, ll, n_discarded1, total_discarded_length):
    if args.cutPromisc:
        bad_nodes=[]
        for n in SG.nodes():
            #print "#ps:", float(SG.degree(n))/ ll[n], args.promisc,SG.degree(n),ll[n]
            #print "#pr:", float(G.degree(n))/ ll[n], args.promisc,G.degree(n),ll[n]
            if (old_div(float(G.degree(n)), ll[n]))>args.promisc : # G.degree(n)/ll[n]>args.maxdegree:
                n_discarded1+=1
                bad_nodes.append(n)
                total_discarded_length += ll[n]
                #print "#discard:",n,ll[n],G.degree(n)
#                for nn in G.neighbors(n):
#                    if G.degree(nn)==1:
#                        n_discarded2+=1
#                        total_discarded_length1+=ll[nn]
        for n in bad_nodes:
            e_to_remove=[]
            for e in SG.edges([n]):
                e_to_remove.append(e)
            #G.remove_edges_from(e_to_remove)
            SG.remove_edges_from(e_to_remove)
    if args.progress: print("#total_discarded_length",n_discarded1,n_discarded2,old_div(float(total_discarded_length),1.0e6),old_div(float(total_discarded_length1),1.0e6),old_div(float(total_discarded_length+total_discarded_length1),1.0e6))

def blacklist(args, G, SG):
    if args.blacklist:
        f=open(args.blacklist)
        e_to_remove=[]
        while True:
            l=f.readline()
            if not l: break
            c=l.strip().split()
            e_to_remove.append((c[1],c[2]))
        G.remove_edges_from(e_to_remove)
        SG.remove_edges_from(e_to_remove)

def cheat(args, G, SG, besthit):
    if args.cheat:
        e_to_remove=[]
        for a,b in SG.edges():
            if not ( a in besthit and b in besthit):
                e_to_remove.append((a,b))
            else:
                aa = tuple(besthit[a][1:5])
                bb = tuple(besthit[b][1:5])
                qd = qdist(aa,bb)
                if qd >= args.maxLength : 
                    e_to_remove.append((a,b))
        SG.remove_edges_from(e_to_remove)


def print_cc(args, G, SG):
    f_name = "components_x_" + str(int(args.threshold)) + ".txt"
    with open(f_name, 'w') as f:
        for c in sorted(list(nx.connected_components(SG)), reverse=True, key=lambda x: len(x)):
            f.write("c:" +  str(len(c)) + "\n")

def get_fraction_biggest(args):
    G, SG = make_graph(args)
    print_cc(args, G, SG)
    cc = sorted(list(nx.connected_components(SG)), reverse=True, key=lambda x: len(x))
    biggest = cc[0]
    total_contigs = sum([len(c) for c in cc])
    fraction_biggest = old_div(float(len(biggest)),total_contigs)
    return fraction_biggest, biggest, total_contigs

def find_min_link(args):
    print("starting")
    if args.threshold == "auto":

        # Sample some thresholds, choose the biggest threshold where biggest 
        # connected component has over f percent of all contigs.
        start = 0
        for i in [15, 10, 7, 5, 3]:
            print("beginning with " + str(i))
            args.threshold = i
            fb, biggest, total = get_fraction_biggest(args)
            print(args.threshold, fb, len(biggest), total)
            if fb > args.f:
                start = i
                break
                
        if start == 0:
            print("Cannot find threshold with over " + str(args.f) + " percent of contigs. Using 3.")
            return args.threshold

        # Increment threshold until the biggest connected component 
        # has less than f percent of all contigs. This is the lowest 
        # threshold that (we think) is safe to use.
        args.threshold = start
        fraction_biggest = 1
        while fraction_biggest > args.f :
            args.threshold += 1
            fraction_biggest, biggest, total = get_fraction_biggest(args)
            print(args.threshold, fraction_biggest, len(biggest), total)
            if args.threshold > 30:
                print("Cannot find threshold under 30.")
                sys.exit(0)
                
    return args.threshold


def main():
    args = get_args()
    threshold = find_min_link(args)
    with open(args.output, 'w') as o:
        o.write(str(threshold))


if __name__ == "__main__":
    main()

