#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import pysam
import os
parser = argparse.ArgumentParser()
from hirise.p2fa import fastaWriter

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-S','--segments',default=False)
parser.add_argument('-F','--fasta',default=False)
parser.add_argument('-o','--outfile',default=False)
#parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

if not os.path.exists(args.fasta+".fai"):
     pysam.faidx(args.fasta)
fa = pysam.Fastafile(args.fasta)

outfasta = fastaWriter(args.outfile)

if args.segments:
     f=open(args.segments)
     while True:
          l=f.readline()
          if not l: break
          dummy,scaffold,x,y,leng = l.strip().split()
          x=int(x)
          y=int(y)
          leng=int(leng)
          y=min(y,leng+1)
          seq = fa.fetch(scaffold,x-1,y-1)
          print(seq)
          outfasta.next("{}_{} {}".format(scaffold,x,len(seq)))
          outfasta.write(seq)
outfasta.flush()
outfasta.close()

