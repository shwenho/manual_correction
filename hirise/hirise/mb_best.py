#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
import sys
import re

human_accessions={
"CM000683.2": "21",
"CM000668.2": "6",
"CM000669.2": "7",
"CM000680.2": "18",
"CM000677.2": "15",
"CM000663.2": "1",
"CM000674.2": "12",
"CM000684.2": "22",
"CM000686.2": "Y",
"CM000681.2": "19",
"CM000672.2": "10",
"CM000679.2": "17",
"CM000664.2": "2",
"CM000666.2": "4",
"CM000673.2": "11",
"CM000667.2": "5",
"CM000685.2": "X",
"CM000678.2": "16",
"CM000676.2": "14",
"CM000670.2": "8",
"CM000675.2": "13",
"CM000665.2": "3",
"CM000682.2": "20",
"CM000671.2": "9"
}


def sta(l):
    if int(l[8])<int(l[9]):
        return 1
    else:
        return -1
        
def process_lines(b):
#    for i in b:
#        print i
    b.sort(key=lambda x: (x[0],float(x[11])), reverse=True )
    last=""
    for i in b:
        if not i[0]==last:
            m=re.match("gi\|(\d+)\|gb\|(.*)\|",i[1])
            id=human_accessions[m.group(2)]
            print("\t".join([i[0],id,str(sta(i)) ]))#,"\t".join(i)])
#        else:
#            print "#"+"\t".join([i[0],i[1],str(sta(i)),"\t".join(i)])
        last=i[0]
        

b=[]
while True:
    l=sys.stdin.readline()
    if not l: break
    if l[0]=="#":
        process_lines(b)
        b=[]
    else:
        b.append(l.strip().split())
