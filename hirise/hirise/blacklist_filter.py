#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
import glob

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--links')
    parser.add_argument('-b','--blacklist')
    parser.add_argument('-d','--debug',action="store_true")
    args = parser.parse_args()

    blacklist={}
    for fn in glob.glob(args.blacklist):
        #print "#",fn
        if fn[-3:]==".gz":
            f = gzip.open(fn)
        else:
            f = open(fn)

        while True:
            l=f.readline()
            if not l: break
            if l[0]=="#": continue
            c=l.strip().split()
            #print c
            bl=eval(" ".join(c[5:]))
            blacklist[c[0],c[1]] = blacklist.get((c[0],c[1]),[]) + bl 
            blacklist[c[1],c[0]] = blacklist.get((c[1],c[0]),[]) + [ (y,x) for x,y in bl ] 
            
        f.close()

    fh=open(args.links)
    
    while True:
        l=fh.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        if (c[0],c[1]) in blacklist:
            links = eval(" ".join(c[5:]))
            f=[]
            for x,y in links:
                if not (x,y) in blacklist[c[0],c[1]]:
                    f.append( (x,y) )
            n=len(f)
            if n>0:
                print("\t".join(map(str,[ c[0],c[1],c[2],c[3],len(f),f ])))
        else:
            print(l.strip())

            
