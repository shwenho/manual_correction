#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import map
from past.utils import old_div
import pysam
import sys
import math

N = 3
#nmask = 2**N - 1
nstates = 2**N
nmask = nstates -1


if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-f','--frag')
    parser.add_argument('-r','--ref')
    parser.add_argument('-o','--orienter')
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

#Scaffold1       CONTIG1 +isotig2312874  1       109     14.9491525423729
#Scaffold2       CONTIG1 +isotig1832665  1       116     7.83333333333333
#Scaffold2       GAP1    -47     1
#Scaffold2       CONTIG2 +isotig1068957  70      375     23.8673046875
#Scaffold2       GAP2    55      5
#Scaffold2       CONTIG3 -diplotig302227 431     1589    27.9496590909091

    mult={}
    frag_scaffold={}
    f = open(args.frag)
    while True:
        l = f.readline()
        if not l: break
        c = l.strip().split()
        if c[1][:6]=="CONTIG":
            frag_scaffold[ c[2][1:] ] = (c[0], c[2][0], old_div((int(c[3])+int(c[4])),2) )
            mult[c[2][1:]] = mult.get(c[2][1:],0)+1
    f.close()

    
    ref_scaffold={}
    f = open(args.ref)
    while True:
        l = f.readline()
        if not l: break
        c = l.strip().split()
        if c[1][:6]=="CONTIG":
            ref_scaffold[ c[2][1:] ] = (c[0], c[2][0], old_div((int(c[3])+int(c[4])),2))
    f.close()

#x: 1 0 Scaffold29262 1
#x: 1 1 Scaffold4036 0
#x: 1 2 Scaffold95578 0
#x: 1 3 Scaffold57210 1
#x: 1 4 Scaffold4215 0
#x: 1 5 Scaffold102158 0

    oo_order={}
    f = open(args.orienter)
    while True:
        l = f.readline()
        if not l: break
        if not l[:2]=="x:": continue 
        c = l.strip().split()
        oo_order[c[3]] = (c[1],c[2],c[4])
    f.close()

    for t in list(ref_scaffold.keys()):
        
        print(t+"\t"+str(mult[t])+"\t"+"\t".join(map(str,ref_scaffold[t])) +"\t"+"\t".join(map(str,frag_scaffold[t]))+"\t"+"\t".join(oo_order.get(frag_scaffold[t][0],())))
        

