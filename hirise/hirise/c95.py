#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from past.utils import old_div
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

v=[]
while True:
     l=sys.stdin.readline()
     if not l: break
     c=l.strip().split()
     v.append(float(c[0]))

v.sort()
n=len(v)
i=0
while i<0.05*n:
     j=int(i+0.95*n)
     print(n,i,j,j-i,old_div(float(i),n),old_div(float(n-j),n),old_div(float(j-i),n),v[i],v[j],v[j]-v[i],max([abs(v[i]),abs(v[j])]))
     i+=1
