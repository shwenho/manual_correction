#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import random
from hirise.p2fa import fastaWriter

tr = str.maketrans("ACTGactg","TGACtgac")
bases=['A','C','T','G']

def rc(s):
    r=s[::-1]
    rc = r.translate(tr)
    return(rc)

#@read:0:246/1
#GGCACGCTGGGTTCCCTACTCACCTATGTGCCACGACCCGCAACCAGCGGTTGGTGTTGACGGGCACGGTGGAACGCAGCACCACGGGCTGGGAGCCCAG
#+
#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
#@read:0:246/2
#GGCCCTGCCCAGCCACCCTGGACGTGACCGTATCCCTCTGCCACACCCCAGGCCCTGCGAGGGGCTATCGAGAGGAGCTCACTGTGGGATGGGGTTGACC
#+
#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM


class FakeFastqWriter:
    def __init__(self,filename):

        self.f=open(filename,"w")
        self.counter=0
        self.buff=""
        self.x=0
        self.name=""

    def write(self,s1,s2):
        name1 = "@read:0:{}/1".format(self.counter)
        name2 = "@read:0:{}/2".format(self.counter)
        qual1 = "M" * len(s1)
        qual2 = "M" * len(s2)
        self.f.write( name1 + "\n" )
        self.f.write( s1 + "\n" )
        self.f.write( "+\n" )
        self.f.write( qual1 + "\n" )

        self.f.write( name2 + "\n" )
        self.f.write( s2 + "\n" )
        self.f.write( "+\n" )
        self.f.write( qual2 + "\n" )

        self.counter+=1

    def close(self):
        self.f.close()

    def flush(self):
        self.f.flush()

if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
     parser.add_argument('-n','--ncontigs',default=3,type=int,help="Number of contigs.")
     parser.add_argument('--scheme',default=0,type=int,help="Style of read sampling.")
     parser.add_argument('-L','--length',default=5000,type=int,help="True scaffold length.")
     parser.add_argument('-C','--contigs',help="Filename for contigs fasta to write.")
     parser.add_argument('-R','--reads',help="Filename for reads fastq to write.")

     args = parser.parse_args()

     scaffold_seq = ""
     for i in range(args.length):
          scaffold_seq += random.choice(bases) 
     print(scaffold_seq)

     breaks = []
     for i in range(args.ncontigs-1):
          br = random.randint(0,args.length)
          while br in breaks:
               br = random.randint(0,args.length)
          breaks.append(br)
     breaks.sort()

#make fake FASTA for the contigs
     outfasta=fastaWriter(args.contigs)
     print(breaks)
     i=0
     n=1
     for j in range(len(breaks)):
          contig_seq = scaffold_seq[i:breaks[j]]
          outfasta.next("contig{:05d}".format(n))
          outfasta.write(contig_seq)
          n+=1
          print(":: ",len(contig_seq),contig_seq,sep="\t")
          i=breaks[j]
     
     contig_seq = scaffold_seq[i:]
     outfasta.next("contig{:05d}".format(n))
     outfasta.write(contig_seq)
     print(":: ",len(contig_seq),contig_seq,sep="\t")
     
     outfasta.flush()
     outfasta.close()

     
#make fake FASTQ for fake gap-spanning read pairs

     insert_size = 400
     read_length = 100

     outfastq=FakeFastqWriter(args.reads)
     print(breaks)
     i=0
     n=1
     for j in range(len(breaks)):
         
         a = breaks[j]-int(insert_size/2)
         b = a+read_length
         d = breaks[j]+int(insert_size/2)
         c = d-read_length

         read1 = scaffold_seq[a:b]
         read2 = rc(scaffold_seq[c:d])
         outfastq.write(read1,read2)
         i=breaks[j]
     
     outfastq.flush()
     outfastq.close()

     

