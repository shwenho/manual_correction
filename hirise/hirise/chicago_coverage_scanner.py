#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from builtins import object
from past.utils import old_div
import pysam
import sys

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-s','--savegraph',default=False)
    parser.add_argument('-S','--restoregraph',default=False)
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-m','--minlength',default=1000,type=int)
    parser.add_argument('-M','--min_insert',default=1000,type=int)
    parser.add_argument('-e','--endsonly',default=False,action="store_true")
    parser.add_argument('-E','--endwindow',default=50000,type=int)
    parser.add_argument('-D','--dotfile',default=False)
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('-c','--cutoff',default=(old_div(200.0,1.0e13)),type=float)

    nodes={}

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )
  
    sam=pysam.Samfile(args.bamfile)

    if args.progress: log( "opened %s" % args.bamfile )


    h=sam.header
    seqs=h['SQ']

    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]

    if args.progress: log(  "built length and name map arrays" )   


    for i in range(len(seqs)):
        if slen[i]>args.minlength:
            nodes[i]=1

    n=0
    ne=0
    hist={}
    nr=0

    left_cursor = 0
    right_cursor= 0
    center_cursor=0
    current_seq = 0

    all_fore_queue=[]
    all_aft_queue=[]
    all_queue=[]
    max_len=150000

    aln = False


    class diagonal(object):
        def __init__(self,x,y):
            assert (x<y)
            self.min_sep = x
            self.max_sep = y
            
            
    class pairGen(object):
        def __init__(self,sam):
            print("init")
            self.sam_it = sam.fetch(until_eof=True)
#            print self.sam_it, dir(self.sam_it)
#            print "z"
#            x = self.sam_it.next()
#            print x
#            print dir(x)
            

        def __next__(self):
            aln= next(self.sam_it)
#            print "x:",aln
            #print "y:",aln.next()
            while not ( aln.tid==aln.rnext):
                aln = next(self.sam_it)
#                print "z:",aln
            return ((aln.tid,aln.pos,aln.pnext,aln.qname))


    pg=pairGen(sam)
    print(pg)
    print(dir(pg))
    print("q")
    print(next(pg))


    def init_start_new_scaffold():
        left_cursor = 0
        right_cursor= 0
        center_cursor=0
        current_seq = -1
        all_fore_queue=[]
        all_aft_queue=[]

    def report_stats():
#        print [x for x in all_fore_queue if min(x[1:])<center_cursor and max(x[1:])>center_cursor and (max(x[1:])-min(x[1:]))>args.min_insert ]
#        print [x for x in all_aft_queue if min(x[1:])<center_cursor and max(x[1:])>center_cursor and (max(x[1:])-min(x[1:]))>args.min_insert ]
        l1 =  [x[3] for x in all_fore_queue if min(x[1:3])<center_cursor and max(x[1:3])>center_cursor and (max(x[1:3])-min(x[1:3]))>args.min_insert ]
        l2 =  [x[3] for x in all_aft_queue if min(x[1:3])<center_cursor and max(x[1:3])>center_cursor and (max(x[1:3])-min(x[1:3]))>args.min_insert ]
#        print all_fore_queue, all_aft_queue
        print(l1,l2)
        coverage = len(set(l1).union(set(l2)))
        s1 =  len(l1)
        s2 =  len(l2)

        print("r:",current_seq,center_cursor,len(all_fore_queue),len(all_aft_queue),s1,s2,coverage)

    if True:
        if args.progress: log("about to iterate over bamfile alignments")

        pair=False
        while True:

            center_cursor += 1000
#            print "x"
#            pair = pg.next()
            
#            print pair
            nr+=1
            if nr%200000 == 0:
                sys.stderr.write("%d\n"%nr)

            if not pair:
                pair = next(pg)

            print(pair)


            while pair[0]==current_seq and pair[1] < center_cursor + max_len:
                all_fore_queue.append( pair )
                pair = next(pg)
                print("<<", pair[1],current_seq,pair[0],center_cursor, center_cursor+max_len,slen[current_seq],snam[current_seq])
#            all_fore_queue.append( pair )

#            print all_fore_queue


            while len(all_fore_queue)>0 and ((all_fore_queue[0][0] < current_seq) or (center_cursor > all_fore_queue[0][1])):
                all_aft_queue.append( all_fore_queue.pop(0) )



            while len(all_aft_queue)>0 and ((all_aft_queue[0][0]<current_seq) or (center_cursor-max_len > all_aft_queue[0][1])):
                all_aft_queue.pop( 0 )


            report_stats()

            if len(all_fore_queue)==0:
                pair=next(pg)
                current_seq = pair[0]
                center_cursor=0
                init_start_new_scaffold()
                print("next seq",pair,slen[current_seq])
                
