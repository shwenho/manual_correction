#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
import sys

bb = ["A","C","G","T"]
bn = list(bb)
for b1 in bb:
    for b2 in bb:
        bn.append(b1+b2)

def comp_stats(s):
    n=len(s)
    c={}
    for i in range(n):
        c[s[i]] = c.get(s[i],0)+1
    for i in range(1,n):
        c[s[i-1:i+1]] = c.get(s[i-1:i+1],0)+1
    return c

print("#"+"\t".join(["id","sequence"]+bn))
while True:
    l = sys.stdin.readline()
    if not l:break
    c = l.strip().split()
    bc = c[1][4:20]
    x = comp_stats(bc)
    print("\t".join([c[0],bc,"\t".join(map(str,[x.get(b,0) for b in bn]))]))
