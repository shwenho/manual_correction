#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import map
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx
import hirise.mapper
import pickle as pickle
import re

def log(s):
    print(s)

#['Scaffold1', 'CONTIG1', '+isotig2133903', '1', '105', '16.5636363636364']
#['Scaffold1', 'GAP1', '57', '14']
#['Scaffold1', 'CONTIG2', '+isotig1165818', '163', '348', '33.0517647058824']
#['Scaffold1', 'GAP2', '-22', '1']
#['Scaffold1', 'CONTIG3', '+isotig1090484', '327', '475', '19.6567676767677']
#['Scaffold1', 'GAP3', '31', '11']
#['Scaffold1', 'CONTIG4', '+isotig1523405', '507', '676', '38.6750833333333']
#['Scaffold2', 'CONTIG1', '+isotig2311227', '1', '158', '46.1021296296296']
#['Scaffold2', 'GAP1', '169', '5']
#['Scaffold2', 'CONTIG2', '-isotig2268088', '328', '476', '38.3435353535354']

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--map',required=True)
    parser.add_argument('--outmap',required=True)
    parser.add_argument('-d','--debug',default=False,action="store_true")

    nodes={}

    args = parser.parse_args()

    rl = sys.getrecursionlimit()
#    print rl

    rl = sys.setrecursionlimit(50000)

    if args.map:
        mapp = pickle.load( open(args.map)    )

#    for r in mapp.roots:
#        print "root",r

    ls=False
    ss=-1
#    mapp.obsoleted={}

    newLocus=False
    used_loci={}
    breaks_to_make=[]
    joins_to_make=[]
    break_locations={}
    end_nodes={}
    originals=tuple()
    scaffold_lines={}
    configs=[]
    ends={}
    breaks=[]
    locus=""
    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=='#': continue
        c = l.strip().split()
        if c[0]=="LOCUS:": 

            newLocus=True
            for lo in locus:
                if lo in used_loci:
                    newLocus=False

            if configs and newLocus:
                
                for lo in locus: used_loci[lo]=True

                print("process ",locus)
                print(originals)
                configs.sort(reverse=True)
                L0=configs[0][0]
                i=0
                while i<len(configs) and  L0-20.0 < configs[i][0]:
#                    print configs[i]
                    i+=1
                print("n configs",i)
                good_confs=configs[:i]
                good_confs.sort(key=lambda x: (x[0],x[2],-x[1]),reverse=True)

                implement = good_confs[0][3]
                print(implement)
                break_products={}
                for p in originals:
                    if p in implement:
                        print("don't break",p)
                    else:

                        scaffold_node = mapp.find_node_by_name(ends[p[0]][0])
                        mapped2 = mapp.find_original_coord(scaffold_node,ends[p[0]][1])
                        break_locations[ends[p[0]]] = mapped2
                        breaks_to_make.append((ends[p[0]],"{}.{}".format(ends[p[0]],p[0]),"{}.{}".format(ends[p[1]],p[1])))
                        print("break:",ends[p[0]],"->",("{}.{}".format(ends[p[0]],p[0]),"{}.{}".format(ends[p[1]],p[1])),break_locations[ends[p[0]]]) 

                        for pp in p:
                            break_products[pp]=True
                for p in implement:
                    if len(p)==2 and not p in originals:
                        for ppp in p:
                            pp=ends[ppp]
                            if not pp in break_products:
                                scaffold_node = mapp.find_node_by_name(pp[0])
                                if pp[1]==1:                                    
                                    end_nodes[ "{}.{}".format(pp,ppp) ]= scaffold_node.root
                                else: 
                                    end_nodes[ "{}.{}".format(pp,ppp) ]= scaffold_node.leaf
                        print("new join:",p,("{}.{}".format(ends[p[0]],p[0]),"{}.{}".format(ends[p[1]],p[1])),(break_products.get(p[0],False),break_products.get(p[1],False)))  
                        joins_to_make.append(("{}.{}".format(ends[p[0]],p[0]),"{}.{}".format(ends[p[1]],p[1])))
                    

                for cf in good_confs: print(cf)
                print(breaks)
                print(ends)
            locus = c[1:]
            configs=[]
            ends={}
            print(l.strip())
            
        m= re.match("(\d+) (\[.*\])",l)
        if m:
            eid=int(m.group(1))
#            print l.strip()
            ends[eid]=tuple(eval(m.group(2)))
            print(eid,ends[eid])
#config: -3988.43500651  1       2       ((0,), (1, 2), (3, 6), (4, 9), (5,), (7, 8))
        if c[0]=="originals":
            originals=eval(" ".join(c[1:]))

        if c[0]=="improvement:": 
            print(l.strip())
            c = l.strip().split('\t')
            lll=eval(c[3])
            if lll==None: lll=[]
            #print lll
            breaks= list(map(tuple,lll))
        if c[0]=="config:":
            c=l.strip().split('\t')
            configs.append([float(c[1]),int(c[2]),int(c[3]),eval(c[4])])
#        scaffold=int(c[1])
#        scaffold_lines[scaffold] = scaffold_lines.get(scaffold,[])+[c]    



    for b in breaks_to_make:
        breakl = break_locations[b[0]]
        print("Xbreak:",b,breakl,type(breakl[0]),type(breakl[1]))

        if breakl[2]=="gap":
            end_nodes[b[1]],end_nodes[b[2]] = breakl[0],breakl[1]
            mapp.unjoin(end_nodes[b[1]],end_nodes[b[2]])
#        elif breakl[2]=="gap-":
#            end_nodes[b[1]],end_nodes[b[2]] = breakl[1],breakl[0]
#            mapp.unjoin(end_nodes[b[1]],end_nodes[b[2]])
        else:
            end_nodes[b[1]],end_nodes[b[2]] = mapp.add_oriented_break(*breakl)
        print(end_nodes[b[1]],end_nodes[b[2]])
#                        break_locations[ends[p[0]]] = mapped2
#                        breaks_to_make.append((ends[p[0]],"{}.{}".format(ends[p[0]],p[0]),"{}.{}".format(ends[p[1]],p[1])))

        #break_locations[ends[p[0]]] = mapped2
        #breaks_to_make.append((ends[p[0]],"{}.{}".format(ends[p[0]],p[0]),"{}.{}".format(ends[p[1]],p[1])))

        #print 

    for b in joins_to_make:
        print("Xjoin:",b,end_nodes[b[0]],end_nodes[b[1]])
        mapp.add_join( end_nodes[b[0]],end_nodes[b[1]],1000 )

    mapp.write_map_to_file(args.outmap)


