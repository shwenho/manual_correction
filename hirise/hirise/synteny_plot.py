#!/usr/bin/env python3


import argh
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import sys
import numpy 


def main(dots, ref_lengths, hirise_lengths, ref_min_length=2000000, hirise_min_length=200000,
         conservative=True):
    ref_length_shift = get_length_shift(ref_lengths, ref_min_length)
    hirise_length_shift = get_length_shift(hirise_lengths, hirise_min_length)
    transcripts = get_transcripts(dots)
    ref_order = get_ref_order(ref_lengths, ref_min_length)
    get_hirise_order(transcripts, ref_order, conservative)


def get_hirise_order(transcripts, ref_order, conservative):
    hirise_scaffold_counter = {}
    for transcript in transcripts:
        hirise_scaffold = transcript[3]
        ref_scaffold = transcript[1]
        ref_midpoint = transcript[2]
        transcript_lists = hirise_scaffold_counter.setdefault(hirise_scaffold, {})
        transcript_lists.setdefault(ref_scaffold, []).append(ref_midpoint)

    ref_scaffold_best = {}
    for hirise_scaffold, ref_hits in hirise_scaffold_counter.items():
        sorted_hits = sorted(ref_hits, key=lambda x: len(x))
        best_ref_scaff = sorted_hits[0]
        #if conservative:
            #if len(ref_hits[best_ref_scaff]) < 3:
                #continue
        median = numpy.median(ref_hits[best_ref_scaff])
        ref_scaffold_best.setdefault(best_ref_scaff, {})[median] = hirise_scaffold


    hirise_scaffold_order = []
    for ref_scaffold in ref_order:
        try:
            scaffs = ref_scaffold_best[ref_scaffold]
            
        except KeyError:
            pass
            
    


def get_transcripts(dots):
    transcripts = []
    with open(dots) as dots_handle:
        for line in dots_handle:
            s = line.split()
            transcript_name = s[1]
            hirise_scaff = s[3]
            hirise_strand = s[4]
            hirise_start = int(s[5])
            hirise_end = int(s[6])
            hirise_midpoint = int((hirise_start + hirise_end)/2)
            ref_scaff = s[11]
            ref_start = int(s[12])
            ref_end = int(s[13])
            ref_midpoint = int((ref_start + ref_end)/2)
            transcripts.append((transcript_name, ref_scaff, ref_midpoint, hirise_scaff, hirise_midpoint, hirise_strand,))
    return transcripts

def get_lengths(lengths_file, min_length):
    """Take file of length\tname\n and return dictionary of lengths"""
    lengths = {}
    with open(lengths_file) as l_handle:
        for line in l_handle:
            s = line.split()
            name = s[1]
            length = int(s[0])
            if length >= min_length:
                lengths[name] = length
    return lengths

def get_length_shift(lengths_file, min_length):
    """Take file of length\tname\n and return dictionary of cumulative lengths"""
    lengths = get_lengths(lengths_file, min_length)
    length_shift = {}
    sorted_lengths = sorted(lengths.items(), key=lambda x: x[1], reverse=True)
    return sorted_lengths

def get_ref_order(lengths_file, min_length):
    lengths = get_lengths(lengths_file, min_length)
    sorted_lengths = sorted(lengths.keys(), key=lambda x: lengths[x], reverse=True)
    return sorted_lengths

if __name__ == "__main__":
    argh.dispatch_command(main)
