#!/usr/bin/env python3

import argh

@argh.arg("--hra", required=True)
def main(hra=None, output="/dev/stdout", scores=None, debug=False, name=None):
    llr = None
    if scores:
        llr = load_scores(scores)
    scaffolds = read_scaffolds(hra)
    with open(output, 'w') as output_handle:
        for scaffold in scaffolds:
            score = request_scores(scaffold, output_handle, llr, debug, name)


def request_scores(scaffold, output_handle, llr, debug, scaffold_name):
    score = 0
    for i, first_contig in enumerate(scaffold):
        for j in range(i+1, len(scaffold)):
            first_name = first_contig[0]
            first_end = first_contig[2]
            first_tip = first_contig[3]
            second_contig = scaffold[j]
            second_name = second_contig[0]
            second_start = second_contig[1]
            second_tip = second_contig[3]
            gap = second_start - first_end
            if not llr:
                print(first_name, second_name, gap, sep="\t", file=output_handle)
            else:
                o1 = get_orientation(first_tip)
                o2 = get_orientation(second_tip)
                this_score = lookup_score(llr, first_name, second_name, o1, o2, gap)
                score += this_score
                if debug:
                    print("#", first_name, first_tip, second_name, second_tip, o1, o2, gap, this_score, score, sep="\t", file=output_handle)
    if llr:
        print(scaffold_name, score, sep="\t", file=output_handle)
    return score

def get_orientation(contig_tip):
    if contig_tip == "5":
        return 0
    elif contig_tip == "3":
        return 1
    else:
        raise ValueError(contig_tip)
    

def lookup_score(scores, scaffold1, scaffold2, o1, o2, gaplen):
    key = (scaffold1, scaffold2, o1, o2, gaplen,)
    #print("lus",scaffold1,scaffold2,o1,o2,gaplen,scores.get(key),sep="\t")
    try:
        return scores[key]
    except KeyError:
        scaffold1, scaffold2 = scaffold2, scaffold1
        o1, o2 = o2, o1
        o1 = 1 - o1
        o2 = 1 - o2
        key = (scaffold1, scaffold2, o1, o2, gaplen,)
        #print("lRR",scaffold1,scaffold2,o1,o2,gaplen,scores.get(key),sep="\t")
        return scores[key]

def load_scores(scores_file):
    scores = {}
    with open(scores_file) as handle:
        for line in handle:
#            print("orp",line.strip())
            if line.startswith("#"):
                continue
            split_line = line.split()
            scaffold1 = split_line[0]
            scaffold2 = split_line[1]
            gapsize = int(split_line[5])
            for orientation, score in zip(split_line[6:10], split_line[14:18]):
                score = float(score)

                o1, o2 = orientation
                if o1 == "<":
                    o1 = 1
                else:
                    o1 = 0
                if o2 == "<":
                    o2 = 1
                else:
                    o2 = 0
                
                key = (scaffold1, scaffold2, o1, o2, gapsize,)
                scores[key] = score
                #print("orp",orientation,scaffold1,scaffold2,score,key,score)
    return scores

def read_scaffolds(hra):
    scaffolds = []
    this_scaffold = []
    with open(hra) as hra_handle:
        prev_scaffold = None
        this_contig = None
        contig_start = None
        orientation = None
        for line in hra_handle:
            if line.startswith("P "):
                s = line.split()
                scaffold_name = s[1]
                contig_name = s[2]
                which_end = s[4]
                contig_start = int(s[5])
                contig_length = int(s[6])
                new_name = contig_name + "_" + s[3]
                if scaffold_name != prev_scaffold:
                    if prev_scaffold:
                        scaffolds.append(this_scaffold)
                    prev_scaffold = scaffold_name
                    this_scaffold = []
                    
                    
                this_scaffold.append((new_name, contig_start, contig_start + contig_length, which_end))
                next(hra_handle)
        scaffolds.append(this_scaffold)
    return scaffolds

if __name__ == "__main__":
    argh.dispatch_command(main)
