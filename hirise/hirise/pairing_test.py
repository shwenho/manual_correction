#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import itertools

#x=set(["a","b","c","d","e","f","0a","0b","0c","0d"])
x=set(["a","b","c","d","e","f","1","2"])

repr={"a":"a", "b":"b","c":"c", "d":"d","e":"e","f":"f", "1":"0","2":"0"}

skip_pairs=[("a","b"),("c","d"),("e","f"),("a","e"),("a","f"),("b","e"),("b","f")]

reverse_pairs=[]
for a,b in skip_pairs:
    reverse_pairs.append((b,a))

skip_pairs += reverse_pairs
#print skip_pairs

for p in itertools.combinations(x,2):
#    print p
    if p in skip_pairs: continue
    y=x.difference(set(p))
    for q in itertools.combinations(y,2):
        if q in skip_pairs: continue
        z=y.difference(set(q))
        
        for r in itertools.combinations(z,2):
            if r in skip_pairs: continue
#            print p,q,r

            w=z.difference(set(r))
            #            print x,y,z,w
            for s in itertools.combinations(w,2):
                if s in skip_pairs: continue
                tup = [ (repr[i[0]],repr[i[1]]) for i in [p,q,r,s]]
                tup.sort()
                print(tup)

if False:

                b=w.difference(set(s))
                for t in itertools.combinations(b,2):
                    if s in skip_pairs: continue
                    #w=z.difference(set(r))
                    print(p,q,r,s,t)
