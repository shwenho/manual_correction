#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx


def printdot(g,c,n):
    gg = nx.subgraph(g,c)
    f=open("%d.txt" % (n), "wt")
    nn=1
    lab0={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]
        lab0[d]=lab0.get(d,nn)
        nn+=1
    lab={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]

        lab[x]=p + str(lab0[d])

    f.write( "graph G {\n")
    for e in gg.edges():
#        f.write( "\t\"%s\" -- \"%s\";\n" % (lab[e[0]],lab[e[1]]) ) #,gg[e[0]][e[1]]['weight'])
        f.write( "\t\"%s\" -- \"%s\" [label=\"%d\"];\n" % (lab[e[0]],lab[e[1]],int(gg[e[0]][e[1]]['weight'])))
    f.write( "};\n")


def order_score(tlist,g):
    #
    sc = 0.0
    for i in range(len(tlist)):
        for j in range(i+1,len(tlist)):
            if g.has_edge(tlist[i],tlist[j]):
                sc -= g[tlist[i]][tlist[j]]['weight']*(j-i)
    return(sc)

context_len = 3
def find_best_position(singleton,string,pair_pos,g):
    first = max(0,pair_pos - context_len)
    last  = min(len(string),pair_pos + context_len)
    
#    sg = nx.subgraph(g,[singleton]+)

    score=[]
    for i in range(first,last+1):
        test_list = string[:i] + [singleton] + string[i:]
        print(i,test_list)
        sc = order_score(test_list,g)
        score.append((sc,i))
        
    score.sort(reverse=True)
    print(score)
    return score[0][1]

def handle_strings(G,strings):
    #print "strings:",strings
    sl={}
    singletons=[]
    string_index={}
    pos={}
    n=0
    for s in strings:
        #print "x",s
        l = len(s)
        m=0
        for c in s:
            sl[c]=l
            string_index[c]=n
            pos[c]=m
            if l==1:
                singletons.append(c)
            m+=1
        n+=1
    sg = nx.subgraph(G,list(sl.keys()))

    placers=[]
    for s in singletons:
        for e in sg.edges(s):
            if e[0]==s:
                if sl[e[1]]>1:
                    placers.append((sg[e[0]][e[1]]['weight'] , e[0],e[1]))
            else:
                if sl[e[0]]>1:                    
                    placers.append((sg[e[0]][e[1]]['weight'] , e[1],e[0]))
    placers.sort(reverse=True)
    
    placed={}
    for e in placers:
        st=False
        
        singleton = e[1]
        if singleton not in placed:
            st = string_index[e[2]]

            pair_pos = strings[st].index(e[2])
            string = strings[st]

            print(e, sl[e[1]], sl[e[2]], st, strings[st].index(e[2]), strings[st])
            best_position = find_best_position(singleton,string,pair_pos,sg)
            strings[st] = string[:best_position] + [singleton] + string[best_position:]
            placed[singleton]=True
    for s in strings:
        if len(s)>1:
            print("y:", s)

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-q','--minquality',default=10.0,type=float)
    parser.add_argument('-s','--strings',required=True)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))
    print("Done reading edgelist")
    
    f = open(args.strings)
    last_group=0
    strings = []
    while True:
        l = f.readline()
#        print l
        if not l:   break
        if l[0]=="#": continue
        c=l.strip().split()
        group = int(c[0])
        if group > last_group:
            handle_strings(G,strings)
            strings=[]
        strings.append( eval( " ".join(c[2:] )))
        last_group = group
    handle_strings(G,strings)

