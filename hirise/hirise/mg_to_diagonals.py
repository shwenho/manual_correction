#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from past.utils import old_div
import sys
import pysam

lastq = ""
bw=1000
bin_counts = {}

sam=pysam.Samfile(sys.argv[1])
h=sam.header
seqs=h['SQ']
#slen=[ s['LN'] for s in seqs ]
#snam=[ s['SN'] for s in seqs ]

length={}
for s in seqs:
    length[ s['SN'] ] = s['LN']

def report_best_bin(q,bin_counts):
    if not bin_counts: return
    m = max(bin_counts.values())
    b = [k for k, v in list(bin_counts.items()) if v == m]         
    cr,x,s=b[0]
    print("\t".join(map(str,[q,m,len(b),cr,x,s, b])))

while True:
    l = sys.stdin.readline()
    if not l: break
    if l[0] == "#": continue

    c = l.strip().split()
    
    q=c[0]
#    if not q == lastq:
#        report_best_bin(bin_counts)
#        bin_counts = {}
        
    lastq = q
    if q not in bin_counts:
        bin_counts[q]={}
    t=c[1]
    x = list(map(int,c[6:10]))
    s=1
    if x[3]-x[2]<0: s=-1
    #    print q,t,x[2]-s*x[0],x[1]-x[0],x[3]-x[2]
    b = int(old_div(float(x[2]-s*x[0]+s*length[q]/2 ),bw)) * bw
    bin_counts[q][t,b,s]= bin_counts[q].get((t,b,s),0)+x[1]-x[0]

for q in list(bin_counts.keys()):
    report_best_bin(q,bin_counts[q])
