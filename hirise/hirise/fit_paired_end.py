#!/usr/bin/env python3

import numpy
from scipy import asarray, exp
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import argh
from collections import Counter
import math
from pprint import pprint
import json
import ast


def main(histogram, plot_output="inserts.pdf", nn_estimate=None, xmax=0, model_out="/dev/stdout", orientation_file=None, total=0):
    counts = load_histogram(histogram)
    mean = get_mean(counts)
    sigma = get_std(counts, mean)
    x = asarray(list(counts.keys()))
    y = asarray(list(counts.values()))
    popt, pcov = curve_fit(gauss, x, y, p0=[1, mean, sigma])
    plot_fit(x, y, popt, xmax, plot_output)
    write_params(popt, counts, nn_estimate, model_out, orientation_file, total)

def write_params(popt, counts, nn_estimate, model_out, orientation_file, total):
    model_params = {}
    if nn_estimate:
        with open(nn_estimate) as handle:
            model_params = ast.literal_eval(handle.read())

    if orientation_file:
        with open(orientation_file) as handle:
            orientation = handle.readline().strip().rstrip()
            model_params["orientation"] = orientation

    model_params["mean"] = abs(popt[1])
    model_params["sdev"] = abs(popt[2])
    if total > 0:
        N = total
    else:
        N = sum(counts.values())
    model_params["N"] = N
    model_params["model_type"] = "shotgun"
    noise = max(model_params["Nn"] / N, 0.01)
    #if noise > 1:
    #    raise Exception("Noise cannot be {}".format(noise))
    model_params["pn"] = max(model_params["Nn"] / N, 0.01)
    with open(model_out, 'w') as out:
        json.dump(model_params, out)

def plot_fit(x, y, popt, xmax, output):
    plt.plot(x,y,'bo',label='data')
    plt.plot(x,gauss(x,*popt),'ro',label='fit')
    if xmax > 0:
        plt.xlim(xmax=xmax)
    else:
        plt.xlim(xmax=popt[1]+5*popt[2], xmin=popt[1]-5*popt[2])
    plt.legend()
    plt.title('Paired End Fit')
    plt.xlabel('Insert')
    plt.ylabel('count')
    plt.savefig(output)

def load_histogram(histogram):
    counts = Counter()
    with open(histogram) as handle:
        for line in handle:
            x1, y1 = map(float, line.split())
            counts[x1] += y1
    return counts

def get_mean(counts):
    sum_of_numbers = sum(number*count for number, count in counts.items())
    total_n = sum(count for count in counts.values())
    return sum_of_numbers / total_n

def get_std(counts, mean):
    total_n = sum(count for count in counts.values())
    total_squares = sum(number*number*count for number, count in counts.items())
    variance = total_squares / total_n
    std = math.sqrt(variance)
    return math.sqrt(variance)

# Define model function to be used to fit to the data above:
def gauss(x, a, x0, sigma):
    return a*exp(-(x-x0)**2/(2*sigma**2))

def gauss_with_noise(x, a, x0, sigma, noise):
    if x0 < 0 or sigma < 0 or noise < 0:
        return 1e10
    return gauss(x, a, x0, sigma) + [100000*noise/6000000 for i in range(len(x))]

def sum_of_contigs(contigs):
    pass

# Define some test data which is close to Gaussian
if __name__ == "__main__":
    argh.dispatch_command(main)


