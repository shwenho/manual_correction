#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import sys
args=False

#w=100

debug=False

min_links=2

def write_links(links,ll,fh=sys.stdout):
    for c1,c2 in sorted(links):
        if len(links[c1,c2])>0:
            fh.write( "\t".join(map(str,[c1,c2,ll[c1],ll[c2],len(links[c1,c2]),links[c1,c2]])) +"\n" )
    
def process2(links,ll,w=1000,min_links=2,max_others=5,blacklistfh=False,segments=False,percentile=False):
    filtered_links={}
    blacklist={}
    l=[]
    for c1,c2 in list(sorted(links.keys())):
        #print c1,c2,links[c1,c2]
        filtered_links[c1,c2]=[]
        blacklist[c1,c2]=[]
        for x1,x2 in links[c1,c2]:
            l.append( (x1,c2,x2) )
    l.sort()
    #print l
    i=0
    j=0
    buff_stats={}
    bad_ones={}
    bad_steps=[]
    while i < len(l):
        #print j,i,l[j],l[i],ll[l[j][1]],ll[c1] #,filtered_links[c1,l[j][1]]
        buff_stats[l[i][1]] = buff_stats.get(l[i][1],0)+1
        while (l[i][0]-l[j][0])>w :
            buff_stats[l[j][1]] -= 1
            if buff_stats[l[j][1]]==0:
                del buff_stats[l[j][1]]
#            filtered_links[c1,l[j][1]].append( (l[j][0],l[j][2]) )
            j+=1
        ncs = sum( [ 1 for k in list(sorted(buff_stats.keys())) if buff_stats[k]>=min_links ] )
        i+=1
        if ncs>max_others:
#            for k in range(j,i):
#                bad_ones[k]=1
            bad_steps.append((j,+1))
            bad_steps.append((min(i,len(l)-1),-1))
            if debug: print("#bad:", j,i)

    bad_steps.sort()

    x=0
    last=0
    rs=0
    state="out"
    startx=0


    for i in range(len(bad_steps)):
        #print(i,bad_steps[i],len(l))
        y =bad_steps[i][0]
        x=l[y][0]
        if rs>0 and state=="out":
            state="in"
            startx=x
        rs+=bad_steps[i][1]        
        if rs<=0 and state=="in":
            if segments: segments.write("{} {} {}\n".format(c1,startx,x))
            state="out"

    bad_cover={}
    x=0
    last=0
    for i in range(len(bad_steps)):
        y =bad_steps[i][0]
        if debug: print(i,bad_steps[i],x,last,y,list(range(last,y+1)))
        for j in range(last,y+1):
            bad_cover[j]=x
        x+=bad_steps[i][1]
        
        last = y

    if debug: print(bad_steps)
    

    for j in range(len(l)):
        if debug: print(j,bad_cover.get(j,0))
        if bad_cover.get(j,0)<=0:
            filtered_links[c1,l[j][1]].append(( l[j][0],l[j][2] ))
        else:
            blacklist[c1,l[j][1]].append(( l[j][0],l[j][2] ))

    write_links(filtered_links,ll)
    if blacklistfh:
        write_links(blacklist,ll,blacklistfh)

def process(links,ll,w=1000,min_links=2,max_others=5,outfilefh=False,cache=[],segments=False,percentile=99.0):

    bins={}
    
#    for i in range( ll[c1]/w):
#        print bins[i]={}
    
    for c1,c2 in list(sorted(links.keys())):
        if not min_links <= len(links[c1,c2]): continue 
        print("#",c1,c2,len(links[c1,c2]),links[c1,c2],min_links)
        bins_hit= {}
        for p1,p2 in links[c1,c2]:
            binn = int(old_div(p1,w))
            bins_hit[binn]= bins_hit.get(binn,0) + 1
        for binn in list(sorted(bins_hit.keys())):
            if bins_hit[binn] >= min_links:
                bins[binn]= bins.get(binn,0)+1
    for i in range( old_div(ll[c1],w)):
        print(bins.get(i,0), i*w, c1,"histo")
        cache.append(bins.get(i,0))
    cache.sort()
    lll=len(cache)
    tt=min(lll-1,int((percentile/100)*lll))
    if tt>=0:
        nth_percentile=cache[tt]
    else:
        nth_percentile=100        
    print("#th:\t{}\t{}".format(int(nth_percentile),lll))

if __name__=="__main__":

    import sys
    import argparse
    import numpy as np
    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--links')
    parser.add_argument('-o','--output')
    parser.add_argument('-m','--minlinks',type=int,default=2)
    parser.add_argument('-M','--maxothers',type=int)
    parser.add_argument('-S','--segments',default=False,help="A file for saving the extents of the masked segments")
    parser.add_argument('--model')
    parser.add_argument('-L','--lengths')
    parser.add_argument('-w','--window',type=int,default=1000)
    parser.add_argument('-p','--percentile',type=float,default=99.0)
    parser.add_argument('-d','--debug',action="store_true")
    parser.add_argument('-H','--histogram',action="store_true",default=False)
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()

    if args.seed != -1 :
        np.random.seed(args.seed);
    last_contig=False

    ll={}
    if args.lengths:
        f=open(args.lengths)
        while True:
            l=f.readline()
            if not l: break
            c=l.strip().split()
            ll[c[0]]=int(c[1])
    
    model={}
    if args.model:
        f=open(args.model)
        l=f.read()
        model=eval(l.strip())
    print("#",model)

    nexp=0
    if True:# args.maxothers:
        maxothers = args.maxothers    

    else:
        Nn=model['Nn']
        G=model['G']
        w=args.window

        nexp=0.0
        from scipy.stats import poisson
        ii=0
        nl=len(list(ll.values()))
        min_links=args.minlinks
        for l in list(ll.values()):
            ii+=1
            mu = Nn*w*l/(G*G)
            #p_threeplus = 1.0-poisson.cdf(min_links-1,mu)  # probability of min_links or more
            p_threeplus = poisson.sf(min_links-1,mu)  # probability of min_links or more
            nexp+=p_threeplus
#            print "#",float(ii)/nl,l,mu,p_threeplus,nexp

        maxothers = 2+poisson.isf(0.0001,nexp)
    print("# maxothers:", maxothers,nexp) #,poisson.isf(0.001,nexp)
    bloh=False

    of=False
    if args.segments:
        of=open(args.segments,"wt")

    if args.histogram:
        handler = process
    else:
        handler = process2

        bloh = open(args.output,"wt")
    ll={}
    links={}
    if args.links:
        f=open(args.links)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            c = l.strip().split()
            if len(c)<5: continue
 
            pp = eval(" ".join(c[5:]))
#            links[c[1],c[0]]= [ (p[1],p[0]) for p in pp ] 

            if last_contig and not last_contig == c[0]:
                l2=handler(links,ll,args.window,args.minlinks,maxothers,bloh,segments=of,percentile=args.percentile)
#                if l2:
                links={}
                ll={}
            ll[c[0]]=int(c[2])
            ll[c[1]]=int(c[3])
            links[c[0],c[1]]= pp
            last_contig = c[0]
  
        l2=handler(links,ll,args.window,args.minlinks,maxothers,bloh,segments=of,percentile=args.percentile)
