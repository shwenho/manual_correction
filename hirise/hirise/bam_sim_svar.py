#!/usr/bin/env python3
from builtins import str
from builtins import map
#!/usr/bin/env python3
import pysam
import sys
import math
import random
import os


def log(s):
    sys.stderr.write(  "%s\n" %(s) )

args=False

def modify_coords(a,t,start,end):
    l = end-start
    x = a.pos
    y = a.pnext
    if t=="none":
        return True
    if t=="deletion":
        if x > start:
            a.pos   += l
            #a.aend   -= l            
        if a.pnext > start and a.tid == a.rnext:
            a.pnext += l
#        a.validate()
        return True
    if t=="insertion":

        if x > start and x < end:
            return False
        if x > end:
            a.pos -= l

        if a.tid == a.rnext and a.pnext > start and a.pnext < end:
            a.rnext += 1

        if a.tid == a.rnext and a.pnext > end:
            a.pnext -= l
        
        return True
    if t=="inversion":
        if x > start and x < end:
            a.pos = start + ( end - x )
        if (a.tid == a.rnext) and y > start and y < end:
            a.pnext = start + ( end - y )
        
        return True        

    if t=="duplication":

        if x > end:
            a.pos -= l

        if a.tid == a.rnext and a.pnext > end:
            a.pnext -= l
        
        return True

    
    raise Exception
    

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-r','--region',required=True)
    parser.add_argument('-o','--outfile',required=True)
    parser.add_argument('-p','--progress',action='store_true',default=False)
    parser.add_argument('-d','--debug',action='store_true',default=False)
    parser.add_argument('-n','--nwindows',default=10,type=int)
    parser.add_argument('-t','--type',required=True,help="deletion,insertion,inversion,duplication")
    parser.add_argument('-H','--het',default=False,help="Only apply to half the reads, at random",action="store_true")
    parser.add_argument('-A','--start',required=True,type=int)
    parser.add_argument('-B','--end',required=True,type=int)
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")


    args = parser.parse_args()
    if args.debug:
        args.progress=True

    chr,range = args.region.split(':')
    start,stop = list(map(int,range.split("-")))

#    print chr,start,stop

    if args.progress: log( str(args) )

    if args.seed != -1 : 
      random.seed(args.seed)

    sam=pysam.Samfile(args.bamfile)

    if args.progress: log( "opened %s" % args.bamfile )

    h=sam.header
    seqs=h['SQ']

    tmpfile = args.outfile
    if True:  #args.type == "duplication" or args.type == "inversion":
        tmpfile += ".tmp"

    out=pysam.Samfile(tmpfile,"wb",header=h)


    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]
    G = float(sum(slen))

    if args.progress: log(  "built length and name map arrays" )   

    hap = {}
    h=0

    for a in sam.fetch(region=args.region):
        if args.het:
            if a.qname not in hap: hap[a.qname] = random.choice([0,1])
            h = hap[a.qname]

        if h==1:  # the unmodified haplotype gets a pass
            out.write(a)
            continue

        if modify_coords(a,args.type,args.start,args.end):  # returns false for reads falling on inserted sequence:  thrown out.
            out.write(a)
#            modify_coords(a,args.type,args.start,args.end)

#        print a.qname
#        out.write(a)

    out.close()
    if True: #args.type == "duplication" or args.type == "inversion":
        sortedbam = args.outfile
        if sortedbam[-4:] == ".bam":
            sortedbam = sortedbam[:-4]
        #print tmpfile,sortedbam
        s=pysam.sort( tmpfile , sortedbam )
#        print s
#        print s.getMessages()
        os.remove(tmpfile)
    #print args.outfile
    pysam.index(args.outfile)
#    print i
#    print i.getMessages()

