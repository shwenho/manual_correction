#!/bin/bash

#set palette  model RGB defined (-2 'black', -1 'black' ,-1 'orange', 0 'orange', 0 'red' , 1 'red', 10 'yellow', 60 'dark-green')


echo "set terminal pdfcairo color
set term pdfcairo font \"Arial,8\"
set output '$2'
set size square
set palette model RGB defined (0 'yellow', 1 'orange', 2 'red', 3 'purple',  4 'blue')
set colorbox
set xlabel 'Read 1 position (Mb)'
set ylabel 'Read 2 position (Mb)'
set title '$3'
set label \"Colors above the diagonal indicate\nmin map quality.  (Scale at right.)\" at graph 0.05, graph 0.9 left front
set label \"Colors below the diagonal indicate haplotype phase.\n(Yellow indicate unknown phase.)\" at graph 0.9, graph 0.1 right front
plot '< cat $1 | sort -k3n | awk \"\\\$5 >= 0\"' u (\$1/1e6):(\$2/1e6):(\$1<\$2 ? \$5 : (1+\$3)*30)  w p palette pt 5 ps 0.05 t '$1', x+0.04 lt 0 t '40 Kb', x-0.04 lt 0 t ''" | gnuplot

#plot '$1' u 1:2:(\$1<\$2 ? \$5 : 1/0 )  w p palette pt 5 ps 0.05, '' u 1:2:( \$1<\$2 ? 1/0 : \$3 ) w p palette2 pt 5 ps 0.05 , x+40000 lt 0, x-40000 lt 0" | gnuplot
#plot '< cat $1 | grep xy | sort -k3n' u 1:2:5 w p palette pt 5 ps 0.01, x+40000 lt 0, x-40000 lt 0" | gnuplot

