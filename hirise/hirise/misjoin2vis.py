#!/usr/bin/env python3


import argh

def main(misjoins, output="/dev/stdout", size=None):
    with open(output, 'w') as output_handle:
        for misjoin in parse_misjoins(misjoins, size):
            print(misjoin.scaffold, misjoin.left, misjoin.right, misjoin.title, file=output_handle, sep="\t")

def parse_misjoins(misjoins, size):
    with open(misjoins) as misjoin_handle:
        for line in misjoin_handle:
            yield Misjoin.from_line(line, size=size)

class Misjoin:

    def __init__(self, scaffold, left, right, title):
        self.scaffold = scaffold
        self.left = left
        self.right = right
        self.title = title

    def from_line(line, window=100000, size=None):
        s = line.split()
        scaffold = s[1].split("_")[1]
        left = max(0, int(s[-1])-window)
        right = int(s[-1]) + window
        if not size:
            title = "{0}_{1}-{2}".format(
                scaffold, left, right)
        else:
            title = "{0}_{1}-{2}_size_{3}".format(
                scaffold, left, right, size)
            
        instance = Misjoin(scaffold, left, right, title)
        return instance

if __name__ == "__main__":
    argh.dispatch_command(main)
