#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math
import random

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

args=False


def random_range(G,slen,L,force_length=False):
#    print "X"
    done = False
    while not done:
        r = G*random.random()
        t=0
        i=0
        while t+slen[i]<r:
            t+=slen[i]
            i+=1
        s = snam[i].split("_")[1]
        l = slen[i]
        if force_length and l < L: continue
        else: done = True        
        R = "%s:%d-%d"%(s,int(r-t),int(r-t+L))


    print(R)
#    print "\t".join(map(str,[G,r,s,l,R]))

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile')
    parser.add_argument('--lengths')
    parser.add_argument('-p','--progress',action='store_true',default=False)
    parser.add_argument('-d','--debug',action='store_true',default=False)
    parser.add_argument('-f','--force-length',action='store_true',default=False,help="Force region to be of specified size (i.e. won't viz a scaffold less than 'length').")
    parser.add_argument('-n','--nwindows',default=10,type=int)
    parser.add_argument('-s','--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    parser.add_argument('-l','--length',default=3000000,type=int,help="Length of the randomly selected region.")


    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    if args.seed != -1 :
        random.seed(args.seed)
    if args.bamfile and args.lengths:
        print("ERROR: Only specify a bam file or a lengths file, not both",file=sys.stderr)
        sys.exit(1)
    if not args.bamfile and not args.lengths:
        print("ERROR: Must specify a bam file or a lengths file, but not both",file=sys.stderr)
        sys.exit(1)
    if args.bamfile:
        sam=pysam.Samfile(args.bamfile)

        if args.progress: log( "opened %s" % args.bamfile )

        h=sam.header
        seqs=h['SQ']

        if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

        slen=[ s['LN'] for s in seqs ]
        snam=[ s['SN'] for s in seqs ]
    if args.lengths:
        slen = []
        snam = []
        fh = open(args.lengths,"r")
        for line in fh:
            length,scaf = line.split()
            slen.append(int(length))
            snam.append(scaf)
    G = float(sum(slen))
    if args.progress: log(  "built length and name map arrays" )   

    for i in range(args.nwindows):
        random_range(G,slen,args.length,args.force_length)

