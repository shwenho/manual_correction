#!/usr/bin/env python3
from __future__ import print_function
from builtins import range
#!/usr/bin/env python3
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

human_accessions={'gi|568336001|gb|CM000685.2|': 'X', 'CM000669.2': '7', 'gi|568336013|gb|CM000673.2|': '11', 'gi|568336004|gb|CM000682.2|': '20', 'gi|568336011|gb|CM000675.2|': '13', 'gi|568336008|gb|CM000678.2|': '16', 'gi|568336019|gb|CM000667.2|': '5', 'gi|568336000|gb|CM000686.2|': 'Y', 'gi|568336007|gb|CM000679.2|': '17', 'CM000674.2': '12', 'gi|568336003|gb|CM000683.2|': '21', 'CM000675.2': '13', 'CM000663.2': '1', 'CM000678.2': '16', 'CM000665.2': '3', 'CM000664.2': '2', 'gi|568336010|gb|CM000676.2|': '14', 'gi|568336021|gb|CM000665.2|': '3', 'gi|568336002|gb|CM000684.2|': '22', 'CM000679.2': '17', 'gi|568336015|gb|CM000671.2|': '9', 'gi|568336012|gb|CM000674.2|': '12', 'CM000673.2': '11', 'gi|568336023|gb|CM000663.2|': '1', 'CM000681.2': '19', 'gi|568336022|gb|CM000664.2|': '2', 'CM000682.2': '20', 'gi|568336017|gb|CM000669.2|': '7', 'CM000672.2': '10', 'CM000671.2': '9', 'gi|568336016|gb|CM000670.2|': '8', 'gi|568336020|gb|CM000666.2|': '4', 'CM000686.2': 'Y', 'CM000683.2': '21', 'CM000680.2': '18', 'CM000667.2': '5', 'gi|568336009|gb|CM000677.2|': '15', 'gi|568336005|gb|CM000681.2|': '19', 'CM000666.2': '4', 'CM000670.2': '8', 'gi|568336018|gb|CM000668.2|': '6', 'CM000685.2': 'X', 'CM000684.2': '22', 'gi|568336014|gb|CM000672.2|': '10', 'CM000668.2': '6', 'CM000676.2': '14', 'gi|568336006|gb|CM000680.2|': '18', 'CM000677.2': '15'}

while True:
     l=sys.stdin.readline()
     if not l: break
     c=l.strip().split()
     cc=[]
     for i in range(len(c)):
          if c[i] in human_accessions:
               cc.append( human_accessions[c[i]] )
          else:
               cc.append(c[i])
     print("\t".join(cc))
