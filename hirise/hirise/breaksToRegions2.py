#! /usr/bin/env python3
import argh
import os
import re
import sys
from collections import defaultdict

def read_breaks(break_fn):
    input_scafs = defaultdict(list) 
    fh = open(break_fn,"r")
    i = 0
    for line in fh:
        if line.startswith('#'): continue
        hr_scaf,in_scaf,in_start,in_end,strand,hr_start,hr_end = line.strip().split()[:7]
        in_start = int(in_start)
        in_end = int(in_end)
        # assert in_start is always the break point
        if in_start == 0:  continue
        input_scafs[in_scaf].append((strand,in_start,in_end))
        i += 1
    print("read in %d breaks" % (i),file=sys.stderr)
    return input_scafs

            
def get_region(begin,end,flank,upper,lower=0):
    left = lower if begin - flank < lower else begin - flank
    right = upper if end + flank > upper else end + flank
    return (left,right)

def dedupRegions(regions):
    found = {}
    dedup = []
    for region in regions:
        if region not in found:
            dedup.append(region)
        found[region] = 1
    return dedup

def get_hras(hr_dir):
    hras = []
    regex = "hirise_iter_broken_[0-9]+.hra"
    for entry in os.listdir(hr_dir):
        if re.match(regex,entry):
            hras.append(entry)
    hras.sort(key=lambda x: int(x.split("_")[3].split(".")[0]))
    return hras

def identify_break_iters(breaks,hras,hr_dir):
    break_iters = defaultdict(dict)
    j = 0
    for hra in hras:
        fh = open(os.path.join(hr_dir,hra),"r")
        if not breaks:
            break
        for line in fh:
            if not line.startswith("P"): continue
            derp,scaf,contig,c_start,end,hr_pos,seg_length = line.split()
            c_start = int(c_start)
            seg_length = int(seg_length)
            if contig not in breaks or end == "3": continue
            start = c_start - 1
            end = start + seg_length
            for i,(strand,b_start,b_end) in enumerate(breaks[contig]):
                if start == b_start and end == b_end:
                    if contig not in break_iters[hra]:
                        break_iters[hra][contig] = []
                    break_iters[hra][contig].append((start,end))
                    j += 1
                    del breaks[contig][i]
                    if not breaks[contig]:
                        del breaks[contig]
                    continue
    print("identified %d break iters" % (j),file=sys.stderr)
    return break_iters

def get_break_positions(break_iters,hras,hr_dir):
    break_positions = defaultdict(dict)
    lengths = defaultdict(dict)
    unbroken_hras = [("hirise_iter_%d.hra" % (x)) for x in range(len(hras))]
    print(hras,unbroken_hras)
    i = 0
    for unbroken_hra,hra in zip(unbroken_hras,hras):
        if not hra in break_iters: continue
        fh = open(os.path.join(hr_dir,unbroken_hra),"r")
        for line in fh:
            if not line.startswith("P"): continue
            line2 = next(fh)
            derp,scaf,contig,c_start,end,hr_pos,seg_length = line.split()
            derp,scaf2,contig2,c_start2,end2,hr_pos2,seg_length2 = line2.split()
            c_start = int(c_start)
            seg_length = int(seg_length)
            hr_pos  = int(hr_pos)
            hr_pos2 = int(hr_pos2)

            if not scaf in lengths[unbroken_hra]:
                lengths[unbroken_hra][scaf] = 0
            if hr_pos > lengths[unbroken_hra][scaf]:
                lengths[unbroken_hra][scaf] = hr_pos
            if hr_pos2 > lengths[unbroken_hra][scaf]:
                lengths[unbroken_hra][scaf] = hr_pos2
            c_start -= 1
            c_end = c_start + seg_length
            if contig not in break_iters[hra]: continue
            for start,stop in break_iters[hra][contig]:
                if c_start <= start and stop <= c_end:
                    if end == "3":
                        hr_break_pos = hr_pos2 - (start - c_start)
                    else:
                        hr_break_pos = hr_pos + (start - c_start)
                    if scaf not in break_positions[unbroken_hra]:
                        break_positions[unbroken_hra][scaf] = []
                    break_positions[unbroken_hra][scaf].append((contig,start,hr_break_pos))
                    i += 1
    print("found %d break positions" % (i),file=sys.stderr)
    return break_positions,lengths


def write_region(scaf,contig,begin,end,breaks,max_dist,hra,length):
    if begin:
        if not end:
            end = begin
        mid = (begin + end) / 2
        plot_begin = max(mid - max_dist,0)
        plot_end = min(mid + max_dist,length)
        region = "-R %s:%d-%d" % (scaf,plot_begin,plot_end)
        print("\t".join(map(str,[region," ".join(map(str,breaks)),hra,contig, mid,length])))

def breaks_to_regions(break_positions,max_window,lengths,hr_dir):
    max_dist = max_window / 2
    i = 0
    for unbroken_hra in break_positions:
        hra_path = os.path.join(hr_dir,unbroken_hra)
        for scaf in break_positions[unbroken_hra]:
            begin = end = None
            breaks = []
            for contig,start,pos in break_positions[unbroken_hra][scaf]:
                if begin and pos - begin > max_dist:
                    write_region(scaf,contig,begin,end,breaks,max_dist,hra_path,lengths[unbroken_hra][scaf])
                breaks.append("-B %s:%d" %(scaf,pos))
                i += 1
                if not begin:
                    begin = pos
                else:
                    end = pos
            if begin:
                write_region(scaf,contig,begin,end,breaks,max_dist,hra_path,lengths[unbroken_hra][scaf])
    print("wrote %d breaks as regions" % (i), file = sys.stderr)


def main(breaks_filename:"A .input_breaks.txt file.",
  hirise_dir:"Directory containing HRA files for each HiRise iteration"=".",
  max_window:"Maximum size of visualization, breaks within this distance will be "
             "shown in a single plot" = 2000000,
  viz_output:"Name of output file contains regions for visualization, one per line."="vizRegions.txt"):
    breaks_by_scaf = read_breaks(breaks_filename)
    broken_hras = get_hras(hirise_dir)
    break_iters = identify_break_iters(breaks_by_scaf,broken_hras,hirise_dir)
    break_positions,lengths = get_break_positions(break_iters,broken_hras,hirise_dir)
    breaks_to_regions(break_positions,max_window,lengths,hirise_dir)


if __name__ == "__main__":
    argh.dispatch_command(main)

