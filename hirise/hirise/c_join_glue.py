#!/usr/bin/env python3

import argh
from pprint import pprint

@argh.arg("-l", "--links", nargs="+", required=True)
def main(hra, links=None, output="/dev/stdout", uncorrected=False):
    scaff_lookup = get_scaff_lookup(hra)
    output_handle = open(output, 'w')
    for link in links:
        joins = get_joins(link)
        for join in joins:
            if uncorrected:
                ll = join.likelihood
            else:
                ll = join.corrected_likelihood
            print(
                "link score",
                scaff_lookup[join.scaff_1][join.scaff_1_orientation], 
                scaff_lookup[join.scaff_2][join.scaff_2_orientation],
                ll, file=output_handle
            )
    output_handle.close()

def get_scaff_lookup(hra):
    lookup = {}
    with open(hra) as hra_handle:
        prev_num = None
        prev_contig = None
        prev_start = 0
        prev_orientation = None

        for line in hra_handle:
            if line.startswith("P"):
                number = int(line.split()[1])
                name = line.split()[2]
                start = line.split()[3]
                orientation = line.split()[4]
                if number != prev_num:
                    if prev_num:
                        lookup[prev_num]["3"] = prev_contig + "_" + str(prev_start) + "." + prev_orientation
                    lookup[number] = {}
                    lookup[number]["5"] = name + "_" + str(start) + "." + orientation
                    
                prev_num = number
                prev_contig = name
                prev_start = start
                prev_orientation = orientation
    lookup[prev_num]["3"] = prev_contig + "_" + prev_start + "." + prev_orientation
    return lookup

def get_joins(joins):
    with open(joins) as join_handle:
        for line in join_handle:
            if line.startswith("#"):
                continue
            for i in range(4):
                join = HiriseJoin.from_line(line, i)
                yield join

class HiriseJoin:
    
    def __init__(self, scaffoldA, scaffoldB, scaffoldA_ornt, scaffoldB_ornt, likelihood, c_likelihood):
        self.scaff_1 = scaffoldA
        self.scaff_2 = scaffoldB
        self.scaff_1_orientation = scaffoldA_ornt
        self.scaff_2_orientation = scaffoldB_ornt
        self.likelihood = likelihood
        self.corrected_likelihood = c_likelihood
        
    @classmethod
    def from_line(cls, line, join_num):
        s = line.split()
        scaff_1 = int(s[0])
        scaff_2 = int(s[1])
        orient_1, orient_2 = cls.get_orientation(
            join_num, line)
        likelihood = float(s[10+join_num])
        c_likelihood = float(s[14+join_num])
        instance = cls(scaff_1, scaff_2, orient_1, orient_2,
                       likelihood, c_likelihood)
        return instance

    def get_orientation(join_num, line):
        given_orient = line.split()[6+join_num]
        
        if given_orient[0] == ">":
            orient_1 = "3"
        elif given_orient[0] == "<":
            orient_1 = "5"
        else:
            raise ValueError(given_orient)
        if given_orient[1] == "<":
            orient_2 = "3"
        elif given_orient[1] == ">":
            orient_2 = "5"
        else:
            raise ValueError(given_orient)
        return orient_1, orient_2

    def __str__(self):

        return "\t".join(map(str, 
            [
                self.scaff_1, self.scaff_2, self.scaff_1_orientation,
                self.scaff_2_orientation, self.likelihood, self.corrected_likelihood
            ]))
        
if __name__ == "__main__":
    argh.dispatch_command(main)
