#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
#!/usr/bin/env python3
import pysam
import sys
import math
import random

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

args=False

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-r','--region',required=True)
    parser.add_argument('-o','--outfile',required=True)
    parser.add_argument('-p','--progress',action='store_true',default=False)
    parser.add_argument('-d','--debug',action='store_true',default=False)
    parser.add_argument('-n','--nwindows',default=10,type=int)
    parser.add_argument('-s','--seed',default=None)
    parser.add_argument('-l','--length',default=3000000,type=int)

    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")

    args = parser.parse_args()
    if args.seed != -1 :
      random.seed(args.seed)

    if args.debug:
        args.progress=True

    chr,range = args.region.split(':')
    start,stop = list(map(int,range.split("-")))

    print(chr,start,stop)

    if args.progress: log( str(args) )

    random.seed(args.seed)

    sam=pysam.Samfile(args.bamfile)

    if args.progress: log( "opened %s" % args.bamfile )

    h=sam.header
    seqs=h['SQ']

    out=pysam.Samfile(args.outfile,"wb",header=h)


    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]
    G = float(sum(slen))

    if args.progress: log(  "built length and name map arrays" )   

    for a in sam.fetch(region=args.region):
        out.write(a)

