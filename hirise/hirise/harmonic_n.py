#!/usr/bin/env python3
from __future__ import print_function
from math import factorial as fac
from fractions import Fraction as mpq
one = 1
mpz = int

def _harmonic5(a, b):
    if b-a == 1:
        return one, mpz(a)
    m = (a+b)//2
    p, q = _harmonic5(a,m)
    r, s = _harmonic5(m,b)
    return p*s+q*r, q*s

def harmonic5(n):
    return mpq(*_harmonic5(1,n+1))

if __name__=="__main__":
    import sys
    print(harmonic5(float(sys.argv[1])))
