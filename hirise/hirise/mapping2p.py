#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import map
#!/usr/bin/env python3
import sys
import colorsys
import random

#p: 17 Scaffold82510_1.3 5308535.0 - -1 16106514.0 19690 False
#p:      1       Scaffold23744_1.3       5101.0  3       137948910       32963728.0      35999   ['5850.0', '3', '-', '137948910', '137953116', '1', '4130', '4129']
#p:      1       Scaffold23744_1.5       41100.0 3       137953116       32963728.0      35999   ['5850.0', '3', '-', '137948910', '137953116', '1', '4130', '4129']

#327413 1 0 Scaffold327414
#327413 168 167 Scaffold327414
#318801 1 0 Scaffold318802
#318801 878 877 Scaffold318802
#277437 1 0 Scaffold277438

if __name__=="__main__":

    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-m','--map')
    parser.add_argument('-b','--besthits')
    parser.add_argument('-s','--scaffold')
    parser.add_argument('-d','--debug',action="store_true")
    parser.add_argument('--seed',required=False,type=int,default=1, help="Seed for random number generation, use -1 for no seed")
    args = parser.parse_args()
    if args.seed != -1 :
        random.seed(args.seed)
    if args.debug:
        args.progress=True

    mapp=False
    if args.map:
        import pickle as pickle
        mapp = pickle.load(open(args.map))

    if args.scaffold:
        n=mapp.find_node_by_name(args.scaffold)
        print("#",n)


    if args.besthits:
        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
            f.close()

#    layout={}

    if args.scaffold:
        it = iter([n])
    else:
        it = mapp.roots

    lastn=False
    for r in it:
        rootname=mapp.print_name(r)
        slen=abs(r.root.mapped_coord - r.leaf.mapped_coord)
        if slen<1000: continue
        i=0
        for n in mapp.nodes_dfs(r):
            print("#X",rootname,mapp.originals_names[n.s],n.x,n.mapped_coord)
#p:      1       Scaffold116820_1.5      4101    3       137917018       32963728.0      4101    ['7498.0', '3', '-', '137912918', '137917018', '1', '4101', '4101']
            if i%2==1:
                if lastn.x < n.x:
                    cname="{}_{}".format(mapp.originals_names[lastn.s],lastn.x)
                    end1="5"
                    end2="3"
                else:
                    cname="{}_{}".format(mapp.originals_names[n.s],n.x)
                    end1="3"
                    end2="5"
                #print n.s, n.x, n.mapped_coord, mapp.originals_names[n.s]            
                bh = besthit.get(cname,[0,"-","+",-1,-1]) 
                if bh[2]=="-":
                    tx=bh[3]
                    bh[3]=bh[4]
                    bh[4]=tx
                if lastn.x >= n.x:
                    tx=bh[3]
                    bh[3]=bh[4]
                    bh[4]=tx


                print("\t".join(map(str,[ "p:", rootname, "{}.{}".format( cname,end1 ), lastn.mapped_coord, bh[1], bh[3], abs(n.root.mapped_coord - r.leaf.mapped_coord), abs(lastn.x-n.x)+1, besthit.get(cname) ])))
                print("\t".join(map(str,[ "p:", rootname, "{}.{}".format( cname,end2 ),     n.mapped_coord, bh[1], bh[4], abs(n.root.mapped_coord - r.leaf.mapped_coord), abs(lastn.x-n.x)+1, besthit.get(cname) ])))
#                print "\t".join(map(str,[ "p:", rootname, "{}.{}".format( cname,end2 )  ]))
            lastn=n
            i+=1
#295643 1 0 Scaffold295644
#295643 118 117 Scaffold295644
#299913 1 0 Scaffold299914
#299913 113 112 Scaffold299914
#319815 1 0 Scaffold319816
#319815 294 293 Scaffold319816

#p: 17 Scaffold93140_1.5 5309535.0 - -1 16106514.0 8971 False

#p:      1       Scaffold116820_1.3      0       3       137912918       32963728.0      4101    ['7498.0', '3', '-', '137912918', '137917018', '1', '4101', '4101']
#p:      1       Scaffold116820_1.5      4101    3       137917018       32963728.0      4101    ['7498.0', '3', '-', '137912918', '137917018', '1', '4101', '4101']
