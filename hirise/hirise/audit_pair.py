#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
import sys

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-t','--threshold',default=0.0 ,  type=float)
    parser.add_argument('-r','--ref')
    parser.add_argument('-a','--ass') 
    parser.add_argument('-p','--progress',default=False ,  action='store_true')
    parser.add_argument('-d','--debug',default=False ,  action='store_true')
    args = parser.parse_args()
    #ces.debug=args.debug
    if args.debug:
        args.progress=True
    if args.progress: log( str(args) )

    refx={}
    f=open(args.ref)
    while True:
        l=f.readline()
        if not l: break
        c=l.strip().split()
        if len(c)<5: 
#            print "#",l.strip()
            continue
        refx[c[0]]=(c[1],int(c[3]),c[4])
        #print c, refx[c[0]]
    f.close()

    f=open(args.ass)
    while True:
        l=f.readline()
        if not l: break
        c=l.strip().split()
        if len(c)<5: 
#            print "#",l.strip()
            continue
        p=(c[1],int(c[3]),c[4])
        print("\t".join( map(str, [c[0]]+ list( refx.get(c[0],(None,None,None))+ p) )))
    f.close()
