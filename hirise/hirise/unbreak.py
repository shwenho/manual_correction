#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
import sys
import argparse
import re

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False)

args = parser.parse_args()
if args.progress: print("#",args)

ll={}
b2c={}
f=open(args.length)
cl={}
x={}
nlost={}
while True:
     l=f.readline()
     if not l: break
     id,leng = l.strip().split()
     leng=int(leng)
     ll[id]=leng
     m=re.search("^(.*)_(\d+)$",id)
#     print id,m.group(1),m.group(2)
     b2c[ id ]= m.group(1)
#     print id,b2c[ id ]
     cl[m.group(1)] = cl.get(m.group(1),[])+[id]
     x[id]=int(m.group(2))
     if ll[id]<1000:
          nlost[m.group(1)]=nlost.get(m.group(1),0)+1
f.close()

for c in list(cl.keys()):
     cl[c].sort( key=lambda z: x[z] )
#     print c, "\t".join(cl[c]),  "\t".join( map(str,map( lambda z: ll[z], cl[c]) ))

lasts=False
lastc=False

while True:
     l=sys.stdin.readline()
     if not l : break
     c=l.strip().split()
     if not c[0]=="p:":
          continue
     
     contig,end = c[2][:-2],c[2][-1:]
     scaffold=c[1]

     if scaffold==lasts and (not contig==lastc):
          if b2c.get(contig) == b2c.get(lastc): 
               print("XXX", b2c.get(contig) , b2c.get(lastc))
          elif nlost.get(b2c[contig],0)>0:
               print("YYY",b2c[contig],nlost.get(b2c[contig],0),len(cl[b2c[contig]]),cl[b2c[contig]],[ll[z] for z in cl[b2c[contig]]])

     print("\t".join(map(str,[lasts,contig,end,b2c[contig]])),"\t","\t".join(c))

     lasts = c[1]
     lastc = contig


