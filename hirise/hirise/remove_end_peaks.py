#!/usr/bin/env python3

import argh
import sys

def main(peaks, output="/dev/stdout"):
    leaders = get_leaders(peaks)
    with open(output, 'w') as output_handle:
        for peak in parse_peaks(peaks):
            if peak.is_good(leaders):
                print(peak, file=output_handle)

def get_leaders(peaks):
    leaders = {}
    with open(peaks) as peak_handle:
        for line in peak_handle:
            if line.startswith("leader"):
                add_scaffold_end(leaders, line)
    return leaders

def add_scaffold_end(leaders, line):
    scaffold, leader_type, llr_type, position = get_leader(line)
    scaffold_end = leaders.get(scaffold, ScaffoldEnd(scaffold))
    scaffold_end.update(llr_type, leader_type, position)
    leaders[scaffold_end.scaffold] = scaffold_end

def get_leader(line):
    s = line.split()
    scaffold = int(s[1])
    leader_type = s[0]
    llr_type = s[3]
    position = int(s[2])
    return scaffold, leader_type, llr_type, position

def parse_peaks(peaks):
    with open(peaks) as peak_handle:
        for line in peak_handle:
            if line.startswith("leader"):
                continue
            elif line.startswith("support:"):
                continue
            yield Peak.from_line(line)
    
class Peak:

    def __init__(self, scaffold, left, right, middle, score, clip_diff, clip_score, llr_type):
        self.scaffold = scaffold
        self.left = left
        self.right = right
        self.middle = middle
        self.score = score
        self.clip_diff = clip_diff
        self.clip_score = clip_score
        self.llr_type = llr_type

    #1       148     171     148     12.3253 200000  -0.844443       rawLLR
    def from_line(line):
        s = line.split()
        scaffold = int(s[0])
        left = int(s[1])
        right = int(s[2])
        mid = int(s[3])
        score = float(s[4])
        clip_diff = float(s[5])
        clip_score = float(s[6])
        llr_type = s[7]
        return Peak(
            scaffold, left, right, mid, score, clip_diff,
            clip_score, llr_type)

    def is_good(self, leaders):
        scaffold_end = leaders[self.scaffold]
        left_cutoff = scaffold_end.leaders.get((self.llr_type, "leader1",), sys.maxsize)
        right_cutoff = scaffold_end.leaders.get((self.llr_type, "leader2",), 0)
        if self.left > left_cutoff and self.right < right_cutoff:
            return True
        return False

    def __str__(self):
        return "\t".join([
            str(self.scaffold), str(self.left), str(self.right),
            str(self.middle), str(self.score), str(self.clip_diff),
            str(self.clip_score), self.llr_type])

class ScaffoldEnd:

    def __init__(self, scaffold):
        self.scaffold = scaffold
        self.leaders = {}

    def update(self, leader_type, llr_type, position):
        self.leaders[(leader_type, llr_type,)] = position

    def get(self, leader_type, llr_type):
        return self.leaders[(leader_type, llr_type)]

    def __str__(self):
        return "\t".join([str(self.scaffold)] + [str(v) for k, v in self.leaders.items()])

if __name__ == "__main__":
    argh.dispatch_command(main)
