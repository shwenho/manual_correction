#!/usr/bin/env python3                                                                                                                                                                                                                                                         

import argh
import sys
from operator import itemgetter
import matplotlib.pyplot as plt
from matplotlib import collections
import numpy as np

spans = {}
ll={}

def classify_misjoins(merge_file,thresh,mj_thresh,span_filter):
    l1=None
    l2=None
    possible_break = None
    pre_break_point = None
    l1 = next(merge_file)
    last_refpos = l1.split()
    region_start = int(last_refpos[5])
    misjoins = []
    num_mers = 0.0
    for line in merge_file:
        if line.startswith("#"): continue
        c1 = l1.split()
        c2 = line.split()
        l1 = line
        num_mers += 1
        sep1 = abs(int(c2[2]) - int(c1[2]))
        sep2 = abs(int(c2[5]) - int(c1[5]))
        #If audit mers on different chromosomes, or the difference between
        #reference and assembly distance is above the threshold, then possible
        #misjoin
        if abs(sep2-sep1) > thresh or c1[4] != c2[4] or c1[1] != c2[1]:
            #First check if misjoin length is above threshold
            misjoin_length = abs(region_start - int(c1[5]))
            if not span_filter:
                insert_span(last_refpos[4],last_refpos[5],c1[5],last_refpos[2],c1[2])
            if misjoin_length >= mj_thresh:
                yield (c1[4],misjoin_length,region_start,c1,c2)
                if span_filter and misjoin_length > 0:
                    insert_span(last_refpos[4],last_refpos[5],c1[5],last_refpos[2],c1[2])

            region_start = int(c2[5])
            last_refpos = c2
    misjoin_length = abs(region_start - int(c1[5]))
    if not span_filter:
        insert_span(last_refpos[4],last_refpos[5],c1[5],last_refpos[2],c1[2])
    if misjoin_length >= mj_thresh:
        yield (c1[4],misjoin_length,region_start,c1,c2)
        if span_filter and misjoin_length > 0:
            insert_span(last_refpos[4],last_refpos[5],c1[5],last_refpos[2],c1[2])


def insert_span(scaf,scaf_start,scaf_end,chr_start,chr_end):
    scaf_start,scaf_end,chr_start,chr_end = map(int,(scaf_start,scaf_end,chr_start,chr_end))
    inserted = False
    if scaf not in spans:
        spans[scaf] = []
    scaf_spans = spans[scaf]
    for num,(x_start,x_end,y_start,y_end) in enumerate(scaf_spans):
        if chr_start  < y_start:
            scaf_spans.insert(num,(scaf_start,scaf_end,chr_start,chr_end))
            inserted = True
            break
    if not inserted: scaf_spans.append((scaf_start,scaf_end,chr_start,chr_end))

def same_region(audit1,audit2,thresh):
    if audit1[1] != audit2[1]: return False
    sep1 = abs(int(audit1[2])-int(audit2[2]))
    sep2 = abs(int(audit1[5])-int(audit2[5]))
    discrepancy = abs(sep1-sep2)
    if discrepancy > thresh: return False
    else: return True



def onoSpans(spans,block_thresh=10000):
    new_spans = []
    for scaf,scaf_spans in spans.items():
        total_bases = 0
        minus_bases = 0
        blocks = []
        longest_block = [0,0,0]
        for scaf_start,scaf_end,ref_start,ref_end in scaf_spans:
            flipped_start,flipped_end = ref_start,ref_end
            if ref_start > ref_end: 
                minus_bases += (scaf_end-scaf_start)
                flipped_start,flipped_end = ref_end,ref_start
            total_bases += (scaf_end - scaf_start)
            added_to_block = False
            for block in blocks:
                right_dif = abs(flipped_start - block[1])
                left_dif = abs(block[0] - flipped_end)
                if right_dif < block_thresh or left_dif < block_thresh:
                    if flipped_start < block[0]: block[0] = flipped_start
                    if flipped_end > block[1]: block[1] = flipped_end
                    block_len = block[1] - block[0]
                    if block_len > longest_block[0]: 
                        longest_block[0] = block_len
                        longest_block[1] = block[0]
                        longest_block[2] = block[1]
                    added_to_block = True
                    break
            if not added_to_block:
                blocks.append([flipped_start,flipped_end])
                block_len = flipped_end - flipped_start
                if block_len > longest_block[0]: 
                    longest_block[0] = block_len
                    longest_block[1] = flipped_start
                    longest_block[2] = flipped_end
        if minus_bases/float(total_bases) > 0.5:
            oriented_spans = []
            scaf_length = ll[scaf]
            for scaf_start,scaf_end,ref_start,ref_end in scaf_spans:
                x1 = scaf_length - scaf_end  
                x2 = scaf_length - scaf_start   
                y1 = ref_end
                y2 = ref_start
                oriented_spans.append((x1,x2,y1,y2))
        else:
            oriented_spans = scaf_spans
        new_spans.append((scaf,oriented_spans,longest_block[1],longest_block[2]))
    return sorted(new_spans,key=itemgetter(2))
        

def ordered_span_plot(spans,show,save,chr_length=52000000,scaf_numbers=False,block_thresh=20000):
    span_ref_pts = []
    span_collection = []
    tot_scaf_length = 0
    curr_scaf = None
    scaf_boundaries = []
    ax = plt.axes()
    #Assumes spans are sorted by scaffold!!!
    oriented_spans = onoSpans(spans,block_thresh)
    for num,(scaf,scaf_spans,sort_start,sort_end) in enumerate(oriented_spans):
        curr_scaf=scaf
        scaf_length = ll[scaf]
        scaf_id = scaf.split("_")[1]
        if scaf_numbers:
            annotate_x = tot_scaf_length + (scaf_length/2.0)  
            annotate_y = (sort_start + sort_end)/2.0
            if annotate_y > (chr_length/2): annotate_y -= chr_length/10
            else: annotate_y += chr_length/10
            ax.annotate(scaf_id,xy=(annotate_x,annotate_y),fontsize=10)
        for scaf_start,scaf_end,ref_start,ref_end in scaf_spans:
            x_start = int(scaf_start) + tot_scaf_length
            x_end = int(scaf_end) + tot_scaf_length
            span_collection.append([(x_start,ref_start),(x_end,ref_end)])
        tot_scaf_length += ll[curr_scaf]
        scaf_boundaries.append([(tot_scaf_length,0),(tot_scaf_length,chr_length)])
    lc = collections.LineCollection(span_collection)
    bounds = collections.LineCollection(scaf_boundaries,linestyles=u'dashed',linewidths=0.5)
    bounds.color("black")
    ax.add_collection(bounds)
    ax.add_collection(lc)
    ax.set_xlabel('Assembly')
    ax.set_ylabel('Reference')
    ax.autoscale()
    if save:
        plt.savefig(save)
    if show:
        plt.show()


def print_ordered_spans(ordered_spans):
    span_out = open("spans.tab","w")
    print("scaf\tscaf_start\tscaf_end\tchr_start\tchr_end",file=span_out)    
    for scaf,spans in ordered_spans:
        for scaf_start,scaf_end,chr_start,chr_end in spans:
            print("\t".join(map(str,[scaf,scaf_start,scaf_end,chr_start,chr_end])),file=span_out)
    span_out.close()


def main(merge_filename:"An .audit_merge file to look for misjoins in.",thresh:"Minimum discrepancy in audit-mer separation in reference and assembly to be called a misjoin." =10000,mj_thresh:"Minimum length of misjoin to report" =5000,report_spans:"Outputs spans in a tab-delimimted file." =False,length_filename:"Specifies the .length file giving the length of each scaffold."=None,show_plot:"Displays a plot of spans to visualize misjoins and orientation errors. Requires a .length file."=False,save_plot:"Saves the plot of spans to the specified file."=None,span_filter:"Only reports/plots spans longer than the minimum misjoin length specified by 'mj_thresh'"=False,annotate_scafs:"Labels each scaffold with its integer ID number."=False,ref_length:"Length of reference assembly in bp. Defaults to approximate length of chr22."=52000000,block_thresh:"Maximum separation between two spans to be considered a single block. Used to determine scaffold's sort order in the plot, scaffolds are sorted by the starting reference coordinate of their largest block."=20000):
    merge_file = open(merge_filename,"r")
    num_misjoins = 0
    total_length = 0.0
    largest_misjoin = (None,None,0)
    last_scaf = None
    last_break = None
    if length_filename:
        lf = open(length_filename,"r")
        for line in lf:
            length,scaf = line.split()
            ll[scaf]=int(length)
    if (show_plot or save_plot) and not length_filename:
        print("Error: Can't generate span plot without length file. Specify length file with -l option", file = sys.stderr)
        sys.exit(1)
    for scaf,mj_len,mj_start,pre_break,post_break in classify_misjoins(merge_file,thresh,mj_thresh,span_filter):
        if last_scaf and last_scaf == scaf:
            if same_region(last_break,pre_break,thresh): continue
            num_misjoins += 1
            total_length += mj_len
            if mj_len > largest_misjoin[2]:
                largest_misjoin = (scaf,mj_start,mj_len)
            print("\t".join(map(str,[num_misjoins,scaf,mj_len,mj_start,last_break[1],pre_break[1]])))
        last_scaf = scaf
        last_break = pre_break

    print("Total number of misjoins detected: %d" % (num_misjoins))
    if num_misjoins != 0:
        print("Total length of misjoins: %d\nAverage misjoin length: %g" % (total_length,total_length/num_misjoins))
        print("Largest misjoin:\nScaffold\tStart\tLength\n%s" %("\t".join(map(str,largest_misjoin))))
    if report_spans:
            ordered_spans = sorted(list(spans.items()),key=lambda x: x[1][0][3])
            print_ordered_spans(ordered_spans)
    if show_plot or save_plot:
        if not length_filename:
            print("Error: must provide a length file to produce span plot.",file=sys.stderr)
            sys.exit(1) 
        ordered_span_plot(spans,show_plot,save_plot,ref_length,annotate_scafs,block_thresh)

if __name__ == "__main__":
    argh.dispatch_command(main)

