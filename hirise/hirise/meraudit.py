#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-i','--input')
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-N','--nparts',default=False,type=int)
parser.add_argument('-n','--part',default=False,type=int)
parser.add_argument('merfile',nargs=1)

args = parser.parse_args()
if args.progress: print("#",args)

k=101

#from string import str.maketrans
tr = str.maketrans("ACTGactg","TGACtgac")

def rc(s):
     t=s[::-1]
     rc = t.translate(tr)
     return(rc)
 
def audit(mers,buff,name):
     for i in range(0,len(buff)-k):
         m = buff[i:i+k].upper()
         if 'N' in m: continue
         c=rc(m).upper()
         if m in mers:
             print(m,name,i,i+old_div(k,2),"+")
         if c in mers:
             print(c,name,i,i+old_div(k,2),"-")
 



f=open(args.merfile[0])
if args.progress: print("#opened {}".format(args.merfile[0]))
mers={}
while True:
    l=f.readline()
    if not l : break
    if l[0]=="#": continue
    mers[l.strip()]=1
f.close()

if args.progress: print("#read kmers")

fh=sys.stdin
if args.input:
    fh = open(args.input)

from balanced_fasta_reader import seqs

if args.nparts:
    for s in seqs(fh,args.nparts,args.part):
        print(s['name'],len(s['seq']))
        audit(mers,s['seq'],s['name'])

else:
    sname=False
    while True:
        l = fh.readline()
        if not l: break
        if l[0]==">":
            if sname: 
                print("%s\t%d" % (sname,len(buff)))
                audit(mers,buff,sname)
            c=l.strip().split()
            sname = c[0][1:]
            leng=0
            buff=""
        else:
            buff+=l.strip()

    print("%s\t%d" % (sname,len(buff)))
    audit(mers,buff,sname)
