#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import print_function
import sys

f=open(sys.argv[1])

broken_contigs=[]
while True:
    l=f.readline()
    if not l: break
    c=l.strip().split()
    broken_contigs.append(c[0])

while True:
    l=sys.stdin.readline()
    if not l: break
    c=l.strip().split("_")[0]
    if c in broken_contigs:
        print(l.strip())
    
