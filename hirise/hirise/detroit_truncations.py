#!/usr/bin/env python3
from __future__ import print_function
from builtins import range
import sys
import re
import string
import numpy as np
import random 


mers = {}

tt = string.str.maketrans("ACTG","TGAC")

def rc(s):
    s = s.translate(tt)
    return s[::-1]


bases=["A","C","T","G"]
bits = {"A":0, "C":1, "T":2, "G":3}

def int2seq(x):
    s=""
    for i in range(k):
        b =  x & 3
        x = x>>2
        s = s+bases[b]
    return s

def seq2int(s):
    ii=int(0)
#    a= s.split()
    #for b in s.split():
    for i in range(k):
        ii = ii<<2
        b = s[k-1-i]
        #print "#",b,bits[b],ii
        ii += bits[b]
    return ii

k=16
bcf = "/mnt/nas/projects/array/detroit/barcodes-1224.90k.p5.25bp.txt"
f = open(bcf)
while True:
    l = f.readline()
    if not l: break
    c = l.strip().split()
    id = c[0]
    bc = rc(c[1][4:4+k])
    x = seq2int(bc)
    mers[x]=(id,0,0)

#    xxx = neighbors1(x)
#    for i in range(len(xxx)):
#        if x==xxx[i]:
#            print "wtf?",x,xxx[i],i,i/3,int2seq(x),int2seq(xxx[i]),type(x),type(xxx[i])
#        mers[xxx[i]] = (id,1,i,i/3,i%3,bc)
f.close()

def extensions2(s):
    for b1 in ["A","C","T","G"]:
        for b2 in ["A","C","T","G"]:
            yield s+b1+b2

while True:
    l = sys.stdin.readline()
    if not l: break
    s = l[:14]
    for bc in extensions2(s):
        if seq2int(bc) in mers:
            print(bc,bc[-2:])
