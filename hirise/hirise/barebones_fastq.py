#! /usr/bin/env python3.3
#coding: utf8
import re

class fastq_seq:
    """A convenience class for storing a FASTQ sequence and its associated data.

    fastq_seq is a simple convenience class containing no methods gathering 
    together the variables used to store a FASTQ sequence, quality scores, 
    sequence ID, and any other comments. The fields contained in each instance 
    of the class are described below:
   
        self.sequence   - A string containing the FASTQ sequence itself, with each 
                          symbol of the original sequence as a character.

        self.identifier - A string containing the sequence ID that was on the header
                         line of the original FASTQ entry. The sequence ID is 
                         defined as everything on the header line after the beginning 
                         '@' and up to the first whitespace character or comma. A
                          sequence with no ID will have an empty string in this field.

        self.comment    - A string containg the comment from the header line of the
                          original FASTA entry. The comment is defined as everything
                          on the header line after the first whitespace character or
                          comma. An entry with no comment will have an empty string 
                          as a comment.

        self.scores    -  A list of integers representing the Phred quality scores 
                          for each symbol in the sequence. Specifically, there is 
                          guaranteed to be one score for each character in 
                          self.sequence, where self.scores[i] is the quality score for 
                          self.sequence[i].
    """
    def __init__(self,comment,identifier,sequence,qual_scores):
        self.comment = comment
        self.identifier = identifier
        self.sequence = sequence
        self.scores=qual_scores

def header_split(header):
    """A utility function taking in a FASTA/FASTQ header line and returning the ID and comment.

    Input:
        header  - a FASTA or FASTQ header line as a string

    Output:
        (seq_ID,comment) - A tuple containing the sequence ID as its first element
                           and any associated comments as its second element.

    header_split strips off any trailing newline, as well as the starting '>' or '@' 
    characters, and then finds the first occurence of a whitespace character or a comma 
    in the input text. The text is then split around this separator, discarding the 
    separator and storing everything before the separator as 'seq_ID' and everything
    after the separator as 'comment', with both of these variables returned as a tuple.
    """
#Remove any trailing white space or new lines
    header = header.strip()
#Use the regular expression "[\s,]" which matches any whitespace character or a comma 
#to get index of first whitespace or comma.
    match = re.search("[\s,]",header)
#If no match occurs, return the header without the initial character as the sequence ID
    if match is None: return(header[1:],"")
#Otherwise split the header about the beginning of the first match to get ID and comment
    seq_id=header[1:match.start(0)]
    comment=header[match.start(0)+1:]
    return (seq_id,comment)

def read_fastq(input,phred=33):
    while True:
        try:
            header = next(input).strip()
        except StopIteration:
            return
        seq = next(input).strip()
        plus = next(input).strip()
        qual = next(input).strip()
        identifier,comment = header_split(header)
        yield fastq_seq(comment,identifier,seq,qual)
