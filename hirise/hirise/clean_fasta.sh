#! /bin/bash

num_bad=`grep '^>' $1 | cut -f 1 -d ' '  | grep -c '[:-]'`

echo "Found ${num_bad} bad scaffold IDs"

if [ $num_bad -ne 0 ];
then
  echo "Replacing ':' with '|' and '-' with '_'."
  cp $1 ${1}.original
  #sed 'y/[:-]/[|_]/' ${1}.original > ${1}
  awk -F ' ' '$1 !~ "^>"{print $0; next;} {modify=$1;gsub(":","|",modify);gsub("-","_",modify); if(NF>1) print modify,$2; else print modify; print substr($1,2),substr(modify,2) > "/dev/stderr";}' ${1}.original > ${1} 2> clean_fasta.log
fi
