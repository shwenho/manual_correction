#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from past.utils import old_div
import sys
import argparse
#import random
import gzip
import glob

if __name__=="__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('-i','--input',default="gj4.out")
    parser.add_argument('-l','--links')
    parser.add_argument('-w','--window',type=int,default=4)
    parser.add_argument('-o','--outfile')
    parser.add_argument('-I','--internal',action="store_true")
    #parser.add_argument('-c','--chr',default="3")
    parser.add_argument('-m','--min',default=0.0   ,type=float)
    parser.add_argument('-a','--max',default=10.0e6,type=float)

    args = parser.parse_args()
    #print args
    
    scaffold_hues={}
    scaffold_saturations={}
    coords={}
    coord_map={}
    scaffold={}
    contig_strand={}
    bh={}
    f=open(args.input)
    sc_max={}
    sc_min={}
    ch_max={}
    ch_min={}

    n=args.window
    chr_buff=[-1]*(4*n+2)
    x_buff=[-1]*(4*n+2)
    y_buff=[-1]*(4*n+2)
    last_sc=-2
    n_int=0
    n_g=0


    def process_lines(line_buffer):
        global n_int
        global n_g

        filtered_buffer=[]

        internal_baddies={}
        chr_buff=[-1]*(4*n+2)        
        x_buff=[-1]*(4*n+2)        
        y_buff=[-1]*(4*n+2)        
        e_buff=[-1]*(4*n+2)        
        i=-2*(n+1)
        for sc,contig_end,x,chro,y,strand in line_buffer:
            i+=1
            chr_buff.pop(0)
            chr_buff.append(chro)

            x_buff.pop(0)
            x_buff.append(x)

            y_buff.pop(0)
            y_buff.append(y)

            e_buff.pop(0)
            e_buff.append(contig_end)

            if sc.find("caff")>=0:
                sc_prefix=""
            else:
                sc_prefix="scaffold_"

            #print i,chr_buff, tuple([chr_buff[0]]*(2*n)),tuple(chr_buff[:2*n])
            #if tuple([chr_buff[0]]*(2*n))==tuple(chr_buff[:2*n]) and tuple([chr_buff[2*n]]*(2+2*n))==tuple(chr_buff[2*n:]) and (not chr_buff[0]==chr_buff[2*n]) and (not chr_buff[0]==-1) and (not chr_buff[0]=='-') and (not chr_buff[2*n]=='-'):
            #    print "\t".join([str(i),"{}{}:{}-{}".format(sc_prefix,sc,x_buff[2*n-1],x_buff[2*n]),"{}:{}".format(chr_buff[2*n-1],y_buff[2*n-1]),"{}:{}".format(chr_buff[2*n],y_buff[2*n]),"global"]+chr_buff)
            #    n_g+=1

            if tuple([chr_buff[0]]*(2*n))==tuple(chr_buff[:2*n]) and tuple([chr_buff[2+2*n]]*(2*n))==tuple(chr_buff[2+2*n:]) and (chr_buff[0]==chr_buff[2+2*n]) and (not chr_buff[0]==-1) and (not chr_buff[0]=='-') and (not chr_buff[2*n]=='-') and (not chr_buff[0]==chr_buff[2*n]):
                print("\t".join([str(i),"{}{}:{}-{}".format(sc_prefix,sc,x_buff[2*n-1],x_buff[2*n+2]),"{}:{}".format(chr_buff[2*n-1],y_buff[2*n-1]),"{}:{}".format(chr_buff[2*n],y_buff[2*n]),"internal"]+chr_buff+e_buff+[e_buff[2*n-1],e_buff[2*n+2]]))
                n_int+=1
                internal_baddies[old_div(i,2)]=1

        chr_buff=[-1]*(4*n+2)        
        i=-2*(n+1)
        for sc,contig_end,x,chro,y,strand in line_buffer:
            i+=1
            if not (old_div((i+2*n+1),2)) in internal_baddies:
                chr_buff.pop(0)
                chr_buff.append(chro)

                x_buff.pop(0)
                x_buff.append(x)

                y_buff.pop(0)
                y_buff.append(y)

                if sc.find("caff")>=0:
                    sc_prefix=""
                else:
                    sc_prefix="scaffold_"

                #print i,chr_buff, tuple([chr_buff[0]]*(2*n)),tuple(chr_buff[:2*n])
                if tuple([chr_buff[0]]*(2*n))==tuple(chr_buff[:2*n]) and tuple([chr_buff[2*n]]*(2+2*n))==tuple(chr_buff[2*n:]) and (not chr_buff[0]==chr_buff[2*n]) and (not chr_buff[0]==-1) and (not chr_buff[0]=='-') and (not chr_buff[2*n]=='-'):
                    print("\t".join([str(i),"{}{}:{}-{}".format(sc_prefix,sc,x_buff[2*n-1],x_buff[2*n]),"{}:{}".format(chr_buff[2*n-1],y_buff[2*n-1]),"{}:{}".format(chr_buff[2*n],y_buff[2*n]),"global"]+chr_buff+e_buff+[e_buff[2*n-1],e_buff[2*n]]))
                    n_g+=1

                #if tuple([chr_buff[0]]*(2*n))==tuple(chr_buff[:2*n]) and tuple([chr_buff[2+2*n]]*(2*n))==tuple(chr_buff[2+2*n:]) and (chr_buff[0]==chr_buff[2+2*n]) and (not chr_buff[0]==-1) and (not chr_buff[0]=='-') and (not chr_buff[2*n]=='-') and (not chr_buff[0]==chr_buff[2*n]):
                #    print "\t".join([str(i),"{}{}:{}-{}".format(sc_prefix,sc,x_buff[2*n-1],x_buff[2*n+2]),"{}:{}".format(chr_buff[2*n-1],y_buff[2*n-1]),"{}:{}".format(chr_buff[2*n],y_buff[2*n]),"internal"]+chr_buff)
                #    n_int+=1
                #    internal_baddies[i/2]=1


    line_buffer=[]
    while True:
        l=f.readline()
        if not l: break
        if l[:2]=="p:":
            
            c=l.strip().split()
            #print c
            v=eval(" ".join(c[8:]))
            strand="+"
            if v:
                strand = v[2]
            sc,contig_end,x,chro,y = c[1],c[2],int(float(c[3])),c[4],int(float(c[5]))

            if not sc==last_sc:
                process_lines(line_buffer)
                line_buffer=[]

            line_buffer.append( (sc,contig_end,x,chro,y,strand)  )


            last_sc=sc


    process_lines(line_buffer)


    print("#global:",n_g)
    print("#internal:",n_int)
