#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import subprocess
import shlex
import tempfile


def plot_insert_fit(inserts,smooth,corrected,datamodel,outfile,hideraw):

        f=open(datamodel)
        model=eval(f.read())
        fn=model["fn"]
        #print (inserts,smooth,corrected,datamodel,outfile,fn)

        if hideraw:
            gnuplotcommand = """set terminal pdfcairo color
            set term pdfcairo font \"Arial,8\"
            set output '{}'
            set log xy
            set xlabel 'Separation (bp)'
            set ylabel 'Number of pairs'
            plot '{}' u 1:2 w l lt 4 t 'Smoothed frequency', '{}' u 1:2 w l lt 3 t 'Smoothed, contiguity-corrected frequency', {} lw 3 lt 0 t 'Model fit'
            """.format(outfile,smooth,corrected,fn)
        else:
            gnuplotcommand = """set terminal pdfcairo color
            set term pdfcairo font \"Arial,8\"
            set output '{}'
            set log xy
            set xlabel 'Separation (bp)'
            set ylabel 'Number of pairs'
            plot '{}' u 1:2 w l t 'Raw pair separation frequency', '{}' u 1:2 w l lt 4 t 'Smoothed frequency', '{}' u 1:2 w l lt 3 t 'Smoothed, contiguity-corrected frequency', {} lw 3 lt 0 t 'Model fit'
            """.format(outfile,inserts,smooth,corrected,fn)

        #print(  gnuplotcommand )
        fh=tempfile.NamedTemporaryFile()

        fh.write(gnuplotcommand.encode())
        fh.flush()
        #print (fh.name)

        cmd = "gnuplot {}".format(fh.name)
        #print(cmd)
        output,error = subprocess.Popen(shlex.split(cmd),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.PIPE ).communicate()
        #print( output.decode('utf-8') )

        #plot '$1' u 1:2:(\$1<\$2 ? \$5 : 1/0 )  w p palette pt 5 ps 0.05, '' u 1:2:( \$1<\$2 ? 1/0 : \$3 ) w p palette2 pt 5 ps 0.05 , x+40000 lt 0, x-40000 lt 0" | gnuplot
        #plot '< cat $1 | grep xy | sort -k3n' u 1:2:5 w p palette pt 5 ps 0.01, x+40000 lt 0, x-40000 lt 0" | gnuplot



if __name__=="__main__":
    import argparse

    parser = argparse.ArgumentParser()

    #parser.add_argument('-i','--input')
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-H','--hideraw',default=False,action="store_true")
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('inserts',default=False)
    parser.add_argument('smooth',default=False)
    parser.add_argument('corrected',default=False)
    parser.add_argument('datamodel',default=False)
    parser.add_argument('outfile',default=False)

    args = parser.parse_args()
    hideraw = args.hideraw
    import os

    inserts,smooth,corrected,datamodel,outfile = args.inserts,args.smooth,args.corrected,args.datamodel,args.outfile

    plot_insert_fit(inserts,smooth,corrected,datamodel,outfile,hideraw)
