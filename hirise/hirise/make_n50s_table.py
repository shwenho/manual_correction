#!/usr/bin/env python3


import argh
import json
import csv
from hirise.n50stats import n50, n50list, l50


def main(final_lengths=None, raw_lengths=None, output="/dev/stdout"):
    stats = get_stats(raw_lengths, final_lengths)
    print_stats(stats, output)


def get_stats(raw_lengths, final_lengths):

    starting_n50 = get_nX(raw_lengths, column=1)
    final_n50 = get_nX(final_lengths, column=0)

    starting_l50 = get_lX(raw_lengths, column=1)
    final_l50 = get_lX(final_lengths, column=0)
    
    starting_n90 = get_nX(raw_lengths, X=90.0, column=1)
    final_n90 = get_nX(final_lengths, X=90.0, column=0)

    starting_l90 = get_lX(raw_lengths, X=90.0, column=1)
    final_l90 = get_lX(final_lengths, X=90.0, column=0)

    total_length_start = get_total_length(raw_lengths, column=1)
    total_length_final = get_total_length(final_lengths, column=0)

    n50s = (starting_n50, final_n50)
    n90s = (starting_n90, final_n90)
    l50s = (starting_l50, final_l50)
    l90s = (starting_l90, final_l90)

    lengths = (total_length_start, total_length_final)
    

    return n50s, n90s, l50s, l90s, lengths
    
    
def get_nX(lengths, X=50.0, column=0):
    return to_megabase(n50(open(lengths), X, column=column))

def get_lX(lengths, X=50.0, column=0):
    return l50(open(lengths), X, column=column)

def get_total_length(lengths, column=0):
    total_length = sum([ int(c.split()[column]) for c in open(lengths) ])
    total_length = to_megabase(total_length)
    return total_length

def to_megabase(num):
     num_mb = round(num / 1000000, 3)
     if num_mb > 1:
          num_mb =  round(num_mb, 2)
     if num_mb > 10:
          num_mb = round(num_mb, 1)
     return format(num_mb,",") + " Mb"


def print_stats(stats, output):
    n50s, n90s, l50s, l90s, lengths = stats
    nX_template = "{0:,d} scaffolds ; min length {1}"
    with open(output, "w") as csvfile:
        writer = csv.writer(csvfile, delimiter="|", 
                            quotechar="'", quoting=csv.QUOTE_MINIMAL)
        writer.writerow(["", "Starting Assembly", "Dovetail HiRise Assembly"])
        writer.writerow(["Total Length", lengths[0], lengths[1]])
        writer.writerow(["N50 Length", nX_template.format(l50s[0], n50s[0]),
                         nX_template.format(l50s[1], n50s[1])])
        writer.writerow(["N90 Length", nX_template.format(l90s[0], n90s[0]),
                         nX_template.format(l90s[1], n90s[1])])
    

if __name__ == "__main__":
    argh.dispatch_command(main)
