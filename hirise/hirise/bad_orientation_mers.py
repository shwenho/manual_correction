#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys
import argparse
from hirise_report_gen import audit_merge_orientation_errors

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-c','--contig_audit',default=False)
parser.add_argument('-o','--outfile',default="-")

args = parser.parse_args()
if args.progress: print("#",args)

ofh = sys.stdout
if not args.outfile=="-":
     ofh = open(args.outfile,"w")

audit_merge_orientation_errors(args.infile,log=ofh,contig_audit=args.contig_audit)

