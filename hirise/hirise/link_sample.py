#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import map
from past.utils import old_div
import sys
import random

def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False


def qdist(x,y):
    if (not x) or (not y): return (-1)
    if x[0]==y[0]:
        x,y,w,z = list(map(int,[x[2],x[3],y[2],y[3]]))
        ol = pairs_overlap((x,y),(w,z))
        if ol:
            return(-1*min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
        else:
            return(min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
    else:
        return(1.0e12)


contigs={}
counts={}
lengths={}

besthit={}
f = open("broken2_GRCh38.mb")
while True:
    l = f.readline()
    if not l: break

    if not l[:5]=="best:": continue
    c=l.strip().split()
    besthit[c[1]]=c[2:]
f.close()
#if args.progress: print "#Done reading besthits"

while True:
    l = sys.stdin.readline()
    if not l: break
    if l[0]=="#": continue
    c =l.strip().split()
    c1,c2 = c[0],c[1]
    contigs[c[0]]=1
    contigs[c[1]]=1
    n=float(c[4])
    counts[c[0],c[1]]=n
    counts[c[1],c[0]]=n
    lengths[c[0]]= min(150000.0, float(c[2]))
    lengths[c[1]]= min(150000.0, float(c[3]))
    qd = qdist( besthit.get(c1,(0,)*10 )[1:5], besthit.get(c2,(1,)*10 )[1:5]  )
    print(c1,c2,int(n),qd,old_div((n),(lengths[c1]*lengths[c2])),lengths[c1],lengths[c2])

contigs=list(contigs.keys())

nout=0
while nout < int(sys.argv[1]):
    c1,c2 = random.sample(contigs,2)
    n=counts.get((c1,c2),0.0)
    if n==0: continue
    nout+=1
    print(c1,c2,n,old_div((n),(lengths[c1]*lengths[c2])))
