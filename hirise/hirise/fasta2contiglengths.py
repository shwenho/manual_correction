#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys

import re

def gap_lengths(fh):
     current_contig_length=0
     while True:
          l=fh.readline()
          if not l: break
          l=l.strip()
     #     print(l)
          if l[0]==">":
               if current_contig_length>0: 
                    yield(current_contig_length)
               current_contig_length=0
          elif re.match("^[Nn]+$" , l.strip()):
               current_contig_length += len(l)
          else:
               c=re.split("[^nN]+",l)
               cc = c.pop(0)
               current_contig_length += len(cc)
               for cc in c:
      #              if current_contig_length > 0: print("\t\t\t\t\t",current_contig_length,c)
                    if current_contig_length > 0: yield(current_contig_length)
                    current_contig_length = len(cc)

     yield(current_contig_length)          



def contig_lengths(fh):
     current_contig_length=0
     while True:
          l=fh.readline()
          if not l: break
          l=l.strip()
     #     print(l)
          if l[0]==">":
               if current_contig_length>0: 
                    yield(current_contig_length)
               current_contig_length=0
          elif not "n" in l.lower():
               current_contig_length += len(l)
          else:
               c=re.split("[nN]+",l)
               cc = c.pop(0)
               current_contig_length += len(cc)
               for cc in c:
      #              if current_contig_length > 0: print("\t\t\t\t\t",current_contig_length,c)
                    if current_contig_length > 0: yield(current_contig_length)
                    current_contig_length = len(cc)

     yield(current_contig_length)          

if __name__=="__main__":


     import argparse

     parser = argparse.ArgumentParser()

     #parser.add_argument('-i','--input')
     parser.add_argument('-g','--gaps',default=False,action="store_true")
     parser.add_argument('-d','--debug',default=False,action="store_true")
     parser.add_argument('-p','--progress',default=False,action="store_true")
     parser.add_argument('-L','--length',default=False,type=int)

     args = parser.parse_args()
     if args.progress: print("#",args)

     if not args.gaps:
          for leng in contig_lengths(sys.stdin):
               print(leng)
     else:
          for leng in gap_lengths(sys.stdin):
               print(leng)
