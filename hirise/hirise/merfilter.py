#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-i','--input')
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('merfile',nargs=1)

args = parser.parse_args()
if args.progress: print("#",args)

k=101

#from string import str.maketrans
tr = str.maketrans("ACTGactg","TGACtgac")

def rc(s):
     t=s[::-1]
     rc = t.translate(tr)
     return(rc)
 
def audit(mers,buff,name):
     for i in range(0,len(buff)-k):
         m = buff[i:i+k].upper()
         if 'N' in m: continue
         c=rc(m).upper()
         if m in mers:
             print(m,name,i,i+old_div(k,2),"+")
         if c in mers:
             print(c,name,i,i+old_div(k,2),"-")
 
f=open(args.merfile[0])
if args.progress: print("#opened {}".format(args.merfile[0]))
mers={}
while True:
    l=f.readline()
    if not l : break
    if l[0]=="#": continue
    mers[l.strip()]=1
f.close()

if args.progress: print("#read kmers")

fh=sys.stdin
if args.input:
    fh = open(args.input)

while True:
     l=fh.readline()
     if not l: break
     c=l.strip().split()
     if c[0] in mers:
          print(c[0],c[1],c[2],c[3],"+")
     elif rc(c[0]) in mers:
          print(rc(c[0]),c[1],c[2],c[3],"-")

