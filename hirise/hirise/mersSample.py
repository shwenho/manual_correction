#!/usr/bin/env python3
#!/usr/bin/env python3


from __future__ import division
from __future__ import print_function
from builtins import range
#from string import str.maketrans
import hashlib
import functools
from multiprocessing import Pool

tr = str.maketrans("ACTGactg","TGACtgac")

def rc(s):
    r=s[::-1]
    rc = r.translate(tr)
    return(rc)

nb=5
dn=float(1<<(4*(nb)))
def process(name,seq,args, output_handle):
    data = (mer for mer in get_mers(name, seq, args))
    output_handle.write(''.join(data))

def process_threaded(to_process,output_handle,threads,fxn,workers):
    tot_results = workers.map(fxn,to_process)
    for thread_results in tot_results:
        for result in thread_results:
            output_handle.write(result)

#data like (name,seq,pos)
def get_mers2(data,fn_args={}):
    name, seq, pos= data
    results = []
    for line in get_mers(name,seq,fn_args,pos):
        results.append(line)
    return results

def get_mers(name, seq, args,pos = 0):
    seq2 = seq.translate(tr)
    template = "{0} {1} {2} {3} {4} {5}\n"
    for i in range(len(seq)-args.wordsize+1):
        mer=seq[i:i+args.wordsize]
        merrc=seq2[i:i+args.wordsize][::-1]
        st="+"
        if mer>merrc: 
            mer=merrc
            st="-"
        m=hashlib.md5(mer.encode("utf-8"))
        d=m.hexdigest()
        f = int(d[0:nb],16)/dn
        if f < args.fraction :
            yield template.format(name, i+pos, mer, st, d, f)


if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-w','--wordsize',default=101  ,  type=int)
    parser.add_argument('-f','--fraction',default=0.001 ,  type=float)
    parser.add_argument('-i','--input'   ,default="-"   )
    parser.add_argument("-o", "--output", default="/dev/stdout")
    parser.add_argument("-t", "--threads", default=16, type=int)
    parser.add_argument("-m", "--maxlen", default=10000000, type=int)

    args = parser.parse_args()
    
    if args.input == "-":
        fa=sys.stdin

    else:
        fa=open(args.input)
        
    seq=""
    name=""
    pos = 0
    i = 0

    process_fxn = functools.partial(get_mers2,fn_args=args)
    workers = Pool(args.threads)

    with open(args.output, 'w') as output_handle:
        to_process = []
        while True:
            l = fa.readline()
            i += 1
            if not l: break
            if l[0]=="#": continue
            if l[0]==">":
                c=l[1:].strip().split()
                if name: 
                    to_process.append((name,seq.upper(),pos))
                    if len(to_process) == args.threads:
                        process_threaded(to_process,output_handle,args.threads,process_fxn,workers)
                        to_process = []
                    #process(name,seq.upper(),args, output_handle)
                seq=""
                pos = 0
                name=c[0]
            else:
                seq += l.strip() 
                if len(seq) >= args.maxlen:
                    to_process.append((name,seq[:args.maxlen].upper(),pos))
                    seq = seq[args.maxlen-args.wordsize+1:]
                    pos += args.maxlen - args.wordsize + 1
                    if len(to_process) == args.threads:
                        process_threaded(to_process,output_handle,args.threads,process_fxn,workers)
                        to_process = []
    
        to_process.append((name,seq.upper(),pos))
        process_threaded(to_process,output_handle,args.threads,process_fxn,workers)
