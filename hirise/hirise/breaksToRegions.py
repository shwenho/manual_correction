#! /usr/bin/env python3
import argh
import sys
import os
import subprocess
import pysam
from operator import itemgetter
from collections import Counter,defaultdict


def read_breaks(break_fn):
    input_scafs = defaultdict(list) 
    fh = open(break_fn,"r")
    for line in fh:
        if line.startswith('#'): continue
        hr_scaf,in_scaf,in_start,in_end,strand,hr_start,hr_end = line.strip().split()[:7]
        input_scafs[in_scaf].append((hr_scaf,int(hr_start),int(hr_end),strand,int(in_start),int(in_end)))
    return input_scafs

def check_fasta_index(fn):
    index_path = fn + ".fai"
    if not os.path.isfile(index_path):
        print("No index file found for fasta file: %s\nCreating index." % (fn),file=sys.stderr)
        subprocess.check_call("samtools faidx %s" % (fn),shell=True)
    return

def get_lengths(fasta_fn):
    lengths_fn = fasta_fn + ".lengths"
    if not os.path.isfile(lengths_fn):
        print("Could not find .lengths file for the fasta file: %s\nMaking .lengths file.",file=sys.stderr)
        subprocess.check_call("fastalength %s > %s" % (fasta_fn,lengths_fn),shell=True)
    fh = open(lengths_fn,"r")
    lengths = {}
    for line in fh:
        length,scaf = line.split()
        length = int(length)
        lengths[scaf] = length
    return lengths
            
def get_region(begin,end,flank,upper,lower=0):
    left = lower if begin - flank < lower else begin - flank
    right = upper if end + flank > upper else end + flank
    return (left,right)

def dedupRegions(regions):
    found = {}
    dedup = []
    for region in regions:
        if region not in found:
            dedup.append(region)
        found[region] = 1
    return dedup


def write_viz_regions(viz_regions,fn):
    hr_id = lambda s: s.split("_")[1]
    fh = open(fn,"w")
    for (in_scaf,in_start,in_stop),hr_regions,breaks in viz_regions:
        parts = []
        bparts = []
        for scaf,start,stop in hr_regions:
             parts.append("%s:%d-%d" % (hr_id(scaf),start,stop))
        hr_region_str = "-R "+ " -R ".join(parts)
        in_region_str = "-R %s:%d-%d" % (in_scaf,in_start,in_stop)
        for pos in breaks:
            bparts.append("%s:%d" % (in_scaf,pos))
        break_str="-B " + " -B ".join(bparts)
        print("%s\t%s\t%s\t%s"%(in_scaf,in_region_str,break_str,hr_region_str),file=fh)
    fh.close()
            

def get_dir(query):
    if "." in query:
        query = query.split(".")[0]
    for root,dirnames,filenames in os.walk(os.getcwd()):
        for dirname in dirnames:
            if query in dirname: return dirname
    return "not found"

def scaffold_to_regions(scaf,scaf_breaks,max_window,input_lengths,final_lengths):
    #breaks like: [hr_scaf,hr_start,hr_end,strand,in_start,in_end]
    first_break = scaf_breaks[0] 
    last_break = None
    prev_piece = scaf_breaks[0]
    curr_piece = None
    viz_regions = []
    viz_by_scaf = []
    in_breaks = []
    in_len  = input_lengths[scaf]
    #Run through pairs of input scaf regions to process each break
    for i in range(1,len(scaf_breaks)):
        curr_piece = scaf_breaks[i]
        prev_size = prev_piece[5] - prev_piece[4]
        curr_size = curr_piece[5] - curr_piece[4]
        in_breaks.append(curr_piece[4])
        if not viz_regions:
            left,right = get_region(prev_piece[2],prev_piece[2],max_window,final_lengths[prev_piece[0]])
            viz_regions.append((prev_piece[0],left,right))
        #get region of HiRise scaffolds to visualize
        left,right = get_region(curr_piece[1],curr_piece[1],max_window,final_lengths[curr_piece[0]])
        viz_regions.append((curr_piece[0],left,right))
        if curr_size > max_window:
            left,right = get_region(first_break[5],curr_piece[4],max_window,in_len)
            uniq_regions = dedupRegions(viz_regions)
            viz_by_scaf.append(((scaf,left,right),uniq_regions,in_breaks))
            in_breaks = []
            viz_regions = []
            first_break = curr_piece
        prev_piece = curr_piece
    if viz_regions:
        left,right = get_region(first_break[5],curr_piece[4],max_window,in_len)
        uniq_regions = dedupRegions(viz_regions)
        viz_by_scaf.append(((scaf,left,right),uniq_regions,in_breaks))
    return viz_by_scaf


def main(breaks_filename:"A .input_breaks.txt file.",
  final_assembly:"Specifies the FASTA file of the final HiRise assembly."=None,
  input_assembly:"Specifies the FASTA file of the input assembly."=None,
  max_window:"For breaks less than 'max_window' apart on the same scaffold, only output one region. Defaults to 1 Mb"=1000000,
  viz_output:"Name of output file contains regions for visualization, one per line."="vizRegions.txt"):
    il = get_lengths(input_assembly)
    fl = get_lengths(final_assembly)
    breaks_by_scaf = read_breaks(breaks_filename)
    visualizations = []
    for scaf in sorted(breaks_by_scaf,key=lambda x: il[x],reverse=True):
        viz_regions= scaffold_to_regions(scaf,breaks_by_scaf[scaf],max_window,il,fl)
        visualizations.extend(viz_regions)
    write_viz_regions(visualizations,viz_output)


if __name__ == "__main__":
    argh.dispatch_command(main)

