#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse


if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
     parser.add_argument('-L','--length',default=False,type=int,help="File containing lenghts.")

     args = parser.parse_args()
     if args.progress: print("#",args)

     last_name=1
     i=1
     while True:
          line=sys.stdin.readline()
          if not line: break
          #s:      gi|443583300|ref|NW_004174482.1|        2480    3278    32057

          dummy,name,a,b,l = line.strip().split()
          if name==last_name:continue

          last_name=name
          a=int(a)
          b=int(b)
          l=int(l)
          
          #p: 216 gi|443579030|ref|NW_004178751.1|_1852.5 10816 - -1 3191896 10816 False

          print("\t".join(map(str,[ "p:",i,name+".5",1,"-",-1,l,l,False ])))
          print("\t".join(map(str,[ "p:",i,name+".3",l,"-",-1,l,l,False ])))
          
          

          i+=1
