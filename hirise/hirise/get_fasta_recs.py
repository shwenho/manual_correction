#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import print_function
import sys

f=open(sys.argv[1])

list_contigs=[]
while True:
    l=f.readline()
    if not l: break
    c=l.strip().split()
    list_contigs.append(c[0])

output_on=False

while True:
    l=sys.stdin.readline()
    if not l: break
    if l[0]==">": 
        c=l[1:].strip().split()[0]
        if c in list_contigs:
            output_on=True
        else:
            output_on=False
    if output_on:
        print(l.strip())
            
