#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#samtools view -h -q10 -F 0x0400 whale_mbo.md.sorted.bam | awk '$7=="=" {print $3,$4,$8,$9}'  | less
import sys
#@SQ     SN:scf895938377 LN:341

ll={}
while True:
    l=sys.stdin.readline()
    if not l :break
    if l[:3]=="@SQ":
#        print l.strip()
        c=l.strip().split()
        ll[c[1][3:]]=int(c[2][3:])
    elif l[0]=="@": 
        continue
    else:
        c=l.strip().split()
        if c[6]=="=":
            x = min([int(c[3]),int(c[7])] )
            if ll[c[2]]>300000 and x>100000 and x<ll[c[2]]-100000:
                print("\t".join(map(str,[c[2],c[3],c[7],abs(int(c[8])),ll[c[2]]])))
    
        
