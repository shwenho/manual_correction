#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
from hirise.mapper import map_graph
import sys
import pysam


import sys
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-b','--buffer',default=1000,type=int)
parser.add_argument('-m','--minlen',default=2000,type=int)
parser.add_argument('-t','--thresh',default=1.0,type=float)
parser.add_argument('-L','--lengths')
parser.add_argument('-n','--dryrun',default=False,action="store_true")
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('files', metavar='FILENAME', nargs='*', help='input and output files')  
args = parser.parse_args()



name2len={}

if args.lengths:
    f=open(args.lengths)
    while True:
        l=f.readline()
        if not l: break
        id,leng = l.strip().split()
        leng=int(leng)
        name2len[id]=leng
    f.close()

if not args.dryrun:
    sam=pysam.Samfile(args.files[0])
    h=sam.header
    seqs=h['SQ']

    #    seqs=h['SQ']
    #    if args.progress: log(  "%s %d"% ( "number of sequences in the reference:", len(seqs) )   )

    slen=[ s['LN'] for s in seqs ]
    snam=[ s['SN'] for s in seqs ]

    #print snam

    for i in range(len(seqs)):
        name2len[snam[i]]=slen[i]

    if not args.dryrun:
        m=map_graph( snam, slen )

#Scaffold304904 330 338 1350 -8.881784197e-16                                                                                                    

handled={}

def process_end(contig,lastx,x):
    handled[contig]=1
    if not lastx==1: lastx+=1
    x+=1
    print("\t".join(map(str,["s:",contig,lastx,x,name2len[contig]])))

last_scaffold=False
lastx=1
while True:
    l = sys.stdin.readline()
    if not l: break
    if l[0]=="#": continue
    c = l.strip().split()
    if not len(c)==5: continue
    scaffold,startx,endx,length,minl = c[0],int(c[1])+1,int(c[2])+1,int(c[3]),float(c[4])
    name2len[scaffold]=length
    if length        < args.minlen: 
        if args.debug: print("too short",scaffold,length)
        continue
    if startx        < args.buffer: 
        if args.debug: print("too short",scaffold,length,startx)
        continue
    if (length-endx) < args.buffer: 
        if args.debug: print("too close to the end",scaffold,length-endx,length,endx)
        continue
    if minl > args.thresh         : 
        if args.debug: print("filtered out",scaffold,length,minl,args.thresh)
        continue

    if last_scaffold and not scaffold==last_scaffold :
        process_end(last_scaffold,lastx,name2len[last_scaffold])
        #add_break
        lastx=1

    print("x:",scaffold,startx)
    #add_break(contig,startx,lastx)
    process_end(scaffold,lastx,startx)
    lastx=startx

    if endx-startx>500:
        print("x:",scaffold,endx)
        process_end(scaffold,lastx,endx)
        lastx=endx
        
    if not args.dryrun:
        i = m.name_to_index[scaffold]
        a,b=m.add_break_in_original(i,startx)
        m.flip(a)
        if endx-startx>500:
            a,b=m.add_break_in_original(i,endx)
            m.flip(a)

    last_scaffold=scaffold    
if args.debug: print("#",scaffold,lastx,name2len[scaffold])

if last_scaffold:
    process_end(scaffold,lastx,name2len[scaffold])

for scaffold in name2len.keys():
    if not scaffold in handled:
        print("\t".join(map(str,["s:",scaffold,1,name2len[scaffold]+1,name2len[scaffold]])))

if not args.dryrun:
    m.write_map_to_file(args.files[1])
    

