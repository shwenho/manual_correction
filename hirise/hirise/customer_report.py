#!/usr/bin/env python3


import argh
import sys
from hirise.n50stats import n50, n50list, l50
import json


def main(
         stats="stats.json",
         n50splot="n50splot.tex",
         n50scsv="n50s.csv",
         config="config.json",
         logo="/mnt/nas/copy_ext_data/dovetail/Dovetail-Genomics-Logo-Vertical-Outlines-150DPI.png",
         banner="/mnt/nas/copy_ext_data/dovetail/Dovetail-Genomics-Logo-Horizontal-Outlines-300DPI.png",
	 #logo="/mnt/nas/copy_ext_data/dovetail/dovetail-logo.png",
         class_file="/mnt/nas/copy_ext_data/dovetail/customerreport",
         output="/dev/stdout",
	 insert_distribution="insert-distribution.png",
         synteny="no_synteny.txt",
         misc_stats="misc-stats.csv",
         compare_stats="compare-stats.csv",
         coverage_hist="coverage_hist.tex"
         ):

    stats_data = json.load(open(stats))
    config_data = json.load(open("config.json"))
    project = config_data['project'].replace("_"," ").title()
    contact = config_data['contact'].replace("&","\&")
    try:
        institution = config_data['institution'].replace("&","\&")
    except KeyError:
        institution = None
    coverage = stats_data['chicago_coverage_1_50']
    num_libs = 0
    with open(misc_stats,"r") as stats:
        for line in stats: 
            description,value = line.split("|")
            lines = (len(description) % 50) + 1
            num_libs += lines
    with open(output, 'w') as out:
        print("""\\documentclass{{{0}}}
\\placelogo{{{1}}}
\\renewcommand{{\\thefootnote}}{{\\fnsymbol{{footnote}} }}
\\DTglossary
\\begin{{document}}""".format(class_file,logo),file=out)

        if institution:
            print("\\makeDTtitle{{{0}}}{{{1}}}{{{2}}}{{{3}}}".format(banner,project,contact,institution),file=out)
        else:
            print("\\makeDTtitleNoInstitution{{{0}}}{{{1}}}{{{2}}}".format(banner,project,contact),file=out)
        print("""\\project{{{3}}}               
\\dovetail{{Dovetail Assembly}}        
\\vspace{{50px}}
\\coverage{{ {0:.1f} }}
\\stats{{ {1} }}
\\contiguityploteps{{{2}}}
\\newpage
\\coveragehisteps{{{5}}}
\\newpage
\\insertdistribution{{{4}}}""".format(
            coverage, 
            n50scsv, 
            n50splot, 
            project.replace("_"," "),
            insert_distribution,
            coverage_hist),file=out)

        if num_libs < 6:
            print("\\comparestats{{{0}}}\n\\miscstats{{{1}}}".format(compare_stats,misc_stats),file=out)
        else:
            print("\\comparestatssmall{{{0}}}\n\\miscstatssmall{{{1}}}".format(compare_stats,misc_stats),file=out)
        if synteny == "made_synteny.txt":
            print("""\\newpage\n\\syntenyplot{synteny_plot.pdf}""", file=out)
        print("""
\\newpage
\\glsaddall
\\printglossary""",file=out)
        print("""\\end{document}""", file=out)


if __name__ == "__main__":
    argh.dispatch_command(main)
