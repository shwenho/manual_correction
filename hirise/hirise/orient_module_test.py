#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
import orienterN4 as orN4


if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-s','--strings',required=True)
    parser.add_argument('-l','--lengths',required=True)
    parser.add_argument('-N','--nback',default=3,type=int)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-G','--gaps', default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
#    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-b','--megablast' )

    args = parser.parse_args()

    blast_data={}
    if args.megablast:
        f=open(args.megablast)
        b=[]
        while True:
            l=f.readline()
            if not l: break
            if l[0]=="#":
                orN4.process_lines(b,blast_data)
                b=[]
            else:
                b.append(l.strip().split())
        f.close()
        if args.progress: log( str( "Done loading "+args.megablast ) )

    N=args.nback
    nstates = 2**N
    nmask = nstates -1


    if args.debug:
        args.progress=True
        debug=True
#    if args.progress: log( str(args) )

#    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))
#    print "Done reading edgelist"

    length={}    
    f = open(args.lengths)
    while True:
        l = f.readline()
        if not l:   break
        if l[0]=="#": continue
        c=l.strip().split()
        length[c[0]]=int(c[1])
    f.close()

    f = open(args.strings)
    strings = []
    while True:
        l = f.readline()

        if not l:   break
        if l[0]=="#": continue
        if not l[:2]=="y:": continue
        c=l.strip().split()
        st =  eval( " ".join(c[1:]))
#        print st
        strings.append( st )
        
    f.close()

    scores={}
    for s in strings:
        for i in range(1,len(s)):
            for j in range(N):
                scores[s[max(0,i-j-1)],s[i]]=[]
                

#scf896052939 scf895944060 7468 1941 2 [(6558, 1429), (6863, 1197)]

    while True:
        l = sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c = l.strip().split()
        if len(c)<5: continue
        if (c[0],c[1]) in scores:
            scores[c[0],c[1]]= eval(" ".join(c[5:]))
#            print scores[c[0],c[1]]
        if (c[1],c[0]) in scores:
            pp = eval(" ".join(c[5:]))
            scores[c[1],c[0]]= [ (p[1],p[0]) for p in pp ] 
#            print scores[c[1],c[0]]

    n=0
    sc={}
    bt={}

    orN4.length=length
    for s in strings:
        n+=1
        shorty=False
        if len(s)<=N:
            print("#shorty", len(s))
            shorty=True
#            continue

        final,score_gap=orN4.orient_string(s,n,scores,blast_data)
        
        for i in range(len(s)):
            if args.gaps:
                r="\t".join(map(str,score_gap(s,final,i,scores)))
                print("\t".join(map(str,["x:",n,i,s[i],final[i],length[s[i]],len(s),r])))
            else:
                print("\t".join(map(str,["x:",n,i,s[i],final[i],length[s[i]],len(s)])))

