#!/usr/bin/env python3
from __future__ import print_function
import networkx as nx

#G=nx.path_graph(10)
G = nx.Graph()
G.add_edge(0,1)
G.add_edge(1,2)
G.add_edge(2,3)
G.add_edge(3,4)
G.add_edge(4,5)
G.add_edge(5,6)
G.add_edge(6,7)
G.add_edge(7,8)
G.add_edge(8,9)

G.add_edge(5,"A")
G.add_edge("A","B")
G.add_edge("C","B")
G.add_edge("Z",1)

#round=1

def shave_round(G):
    leaves=[]
    for n in G.nodes():
        print("#",n, G.degree(n),G.degree(n)==1)
        if G.degree(n)==1:
            leaves.append(n)
#    print leaves

    round=1
    print("#",leaves)
    boundary=list(leaves)
    next=[]
    done=[]
    r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next):
                    next.append(nn)
#                    r[nn]=round
        for b in next: 
            if len( [nn for nn in G.neighbors(b) if nn not in r] )==1: r[b]=round
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if n in r: boundary.append(n)
#        next=[]
    for n in G.nodes():
        if n not in r: r[n]=round
    return r

r=shave_round(G)


x={}
for n in list(r.keys()):
    m = len([ nn for nn in G.neighbors(n) if r.get(nn,round)>2 ])
    x[n]=m

print("graph G {")
for n in list(r.keys()):
    color="red"
    if x[n]>2: color="blue"
#,style='filled',fillcolor='blue',
    print(" {} [label=\"{}\" style=\"filled\" fillcolor=\"{}\"];".format(n,r[n],color))

for e in G.edges():
    print(" {} -- {} ;".format(*e))
print("}")
