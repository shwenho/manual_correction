#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math
 

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-s','--strings',required=True)
    parser.add_argument('-l','--lengths',required=True)
    parser.add_argument('-N','--nback',default=3,type=int)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-G','--gaps', default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()

    N=args.nback
    nstates = 2**N
    nmask = nstates -1

    if args.debug:
        args.progress=True

#    if args.progress: log( str(args) )

#    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))
#    print "Done reading edgelist"

    length={}    
    f = open(args.lengths)
    while True:
        l = f.readline()
        if not l:   break
        if l[0]=="#": continue
        c=l.strip().split()
        length[c[0]]=int(c[1])
    f.close()

    f = open(args.strings)
    strings = []
    while True:
        l = f.readline()

        if not l:   break
        if l[0]=="#": continue
        if not l[:2]=="y:": continue
        c=l.strip().split()
        st =  eval( " ".join(c[1:]))
#        print st
        strings.append( st )
        
    f.close()

    scores={}
    for s in strings:
        for i in range(1,len(s)):
            for j in range(N):
                scores[s[max(0,i-j-1)],s[i]]=[]
                

#scf896052939 scf895944060 7468 1941 2 [(6558, 1429), (6863, 1197)]

    while True:
        l = sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c = l.strip().split()
        if len(c)<5: continue
        if (c[0],c[1]) in scores:
            scores[c[0],c[1]]= eval(" ".join(c[5:]))
#            print scores[c[0],c[1]]
        if (c[1],c[0]) in scores:
            pp = eval(" ".join(c[5:]))
            scores[c[1],c[0]]= [ (p[1],p[0]) for p in pp ] 
#            print scores[c[1],c[0]]

    def pairs_to_lengths(pairs,ro,gap_length,l1,l2):
        for p in pairs:
            l=0
            if   ro==0:
                l= (1+gap_length + (l1-p[0])+p[1])
            elif ro==1:
                l= (1+gap_length + (l1-p[0])+(l2-p[1]))
            elif ro==2:
                l= (1+gap_length + p[0]+p[1])
            elif ro==3:
                l= (1+gap_length + p[0]+(l2-p[1]))
            if (l<=0):
                print("wtf? l<0",p,l,ro,gap_length,l1,l2,pairs)
            yield l

    def logp(x):
        return(-1.0*math.log(x))

    def get_score(s1,s2,relative_orientation,gap_length):
        lp = 0.0
        for frag_length in pairs_to_lengths(scores.get((s1,s2),[]),relative_orientation,gap_length,length[s1],length[s2]):
            lp += logp(frag_length)
        if args.debug: print(s1,s2,relative_orientation,gap_length,len(scores.get((s1,s2),[])),lp)
        return lp
#        pass

    def flip(x):
        if x==0:
            return 1
        if x==1:
            return 0
        print("error: unexpected strand digit")
        exit(0)

    def score_gap(string,final,i):
        r1=0.0
        r2=0.0

        for j in range(max(i-N,0),i):
            gap_length = sum( [ length[string[ii]] for ii in range(j+1,i) ] )
            ro = 2*final[j]+     final[i]
            r1 += get_score( string[j], string[i], ro , gap_length  )
            ro = 2*final[j]+flip(final[i])
            r2 += get_score( string[j], string[i], ro, gap_length  )

        for j in range(i+1,min(i+N,len(string))):
            gap_length = sum( [ length[string[ii]] for ii in range(i+1,j) ] )
            ro = 2*final[i]+      final[j]
            r1 += get_score( string[i], string[j], ro , gap_length  )
            ro = 2*flip(final[i])+final[j]
            r2 += get_score( string[i], string[j], ro , gap_length  )
        return (r1-r2,r1,r2)

    def incr_chain_score(string,i,state):
        r=0.0
        bits = state
        s2 = string[i]
        for x in range(i-N,i):
            s1 = string[x]
            relative_orientation =  (((bits>>(i-x))&1)<<1) | (bits&1)
#            r+=scores[s1,s2][relative_orientation]
            gap_length = sum( [ length[string[ii]] for ii in range(x+1,i) ] )
            r += get_score(s1,s2,relative_orientation,gap_length)
        return r

    def init_chain_score(string,i,state):
        bits = state 
        r = 0.0
        for x in range( 0,i ): 
            s1 = string[x]
            for y in range( x+1,i+1 ):
                s2 = string[y]
                relative_orientation = (((bits>>(i-x))&1)<<1)  | ((bits>>(i-y))&1)
                #r += scores[s1,s2][relative_orientation]
                gap_length = sum( [ length[string[ii]] for ii in range(x+1,y) ] )
                r += get_score(s1,s2,relative_orientation,gap_length)
#                if args.debug: print "\t".join( map(str,[i,x,y,'{0:04b}'.format(bits),s1,s2,relative_orientation,scores[s1,s2][relative_orientation] ]))
        if args.debug: print("r:",r)
        return r

    n=0
    sc={}
    bt={}
    for s in strings:
        n+=1
        if len(s)<=N:
            continue
        sc={}
        bt={}
        for state in range(nstates):            
#            for i in range(N+1):
            sc[(N-1,state)] = init_chain_score(s,N-1,state)

#        print "\t".join(map(str,[n,0,s[0],sc[(0,'+')],sc[(0,'-')]]))
        for i in range(N,len(s)):

            my_scores = {}
            for state in range(nstates):
                options = []
                for j in range(2):
                    pstate = ( state >> 1) | (j << (N-1))
                    options.append( (sc[(i-1,pstate)] + incr_chain_score(s,i,state), pstate) )
                options.sort(reverse=True)
                if args.debug: print(options)
                sc[(i,state)] = options[0][0]
                bt[(i,state)] = options[0][1]
                
            print("\t".join(map(str, [n,i,s[i]] + [ sc[i,state] for state in range(nstates) ] + [bt[i,state] for state in range(nstates)]   )))

        best_score = max( [ sc[len(s)-1, state ] for state in range(nstates)] )
        best_state = [ state for state in range(nstates) if sc[len(s)-1,state]==best_score ][0]

        i = len(s)-1
        final={}
        final[i] = best_state & 1
        if (i,best_state) not in bt:
            print("missing key", i,best_state,len(s),"#",bt)

        prev = bt[i,best_state]
        while i >N:
            i=i-1
            final[i] = prev&1
            prev = bt[i,prev]

        while i > 0:
            i=i-1
            final[i] = prev&1
            prev = prev >> 1

        for i in range(len(s)):
            if args.gaps:
                r="\t".join(map(str,score_gap(s,final,i)))
                print("\t".join(map(str,["x:",n,i,s[i],final[i],length[s[i]],r])))
            else:
                print("\t".join(map(str,["x:",n,i,s[i],final[i],length[s[i]]])))

