#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math

N = 3
#nmask = 2**N - 1
nstates = 2**N
nmask = nstates -1


if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-s','--strings',required=True)
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()
    if args.debug:
        args.progress=True

#    if args.progress: log( str(args) )

#    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))
#    print "Done reading edgelist"
    
    f = open(args.strings)

    strings = []
    while True:
        l = f.readline()

        if not l:   break
        if l[0]=="#": continue
        if not l[:2]=="y:": continue
        c=l.strip().split()
        strings.append( eval( " ".join(c[1:])))
    f.close()

    scores={}
    for s in strings:
        for i in range(1,len(s)):
            for j in range(N):
                scores[s[max(0,i-j-1)],s[i]]=[0.0,0.0,0.0,0.0]

    while True:
        l = sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c = l.strip().split()
        if scores.get((c[0],c[1]),False):
            scores[c[0],c[1]]= list(map(float,c[8:12]))
#            print c[0],c[1],scores[c[0],c[1]]
#            if args.debug: print "\t".join(map(str,[c[0],c[1]]+scores[c[0],c[1]]+[c[4]]))
        if scores.get((c[1],c[0]),False):
#            scores[c[1],c[0]]=
            xx = list(map(float,c[8:12]))

            scores[c[1],c[0]]= [ xx[3],xx[1],xx[2],xx[0] ]
#            if args.debug: print "\t".join(map(str,[c[1],c[0]]+scores[c[1],c[0]]+[c[4]]))



    
    def get_score(s1,s2,o1,o2):
        if   (o1,o2) == ("+","+"):
            return scores[s1,s2][0]
        elif (o1,o2) == ("+","-"):
            return scores[s1,s2][1]
        elif (o1,o2) == ("-","+"):
            return scores[s1,s2][2]
        elif (o1,o2) == ("-","-"):
            return scores[s1,s2][3]




    def incr_chain_score(string,i,state):
        r=0.0
        bits = state
        s2 = string[i]
        for x in range(i-N,i):
            s1 = string[x]
            relative_orientation =  (((bits>>(i-x))&1)<<1) | (bits&1)
            r+=scores[s1,s2][relative_orientation]
        return r

    def init_chain_score(string,i,state):
        bits = state 
        r = 0.0
        for x in range( 0,i ): 
            s1 = string[x]
            for y in range( x+1,i+1 ):
                s2 = string[y]
                relative_orientation = (((bits>>(i-x))&1)<<1)  | ((bits>>(i-y))&1)
                r += scores[s1,s2][relative_orientation]
                if args.debug: print("\t".join( map(str,[i,x,y,'{0:04b}'.format(bits),s1,s2,relative_orientation,scores[s1,s2][relative_orientation] ])))
        if args.debug: print("r:",r)
        return r

    n=0
    sc={}
    bt={}
    for s in strings:
        n+=1
        if len(s)<N:
            continue
        for state in range(nstates):            
#            for i in range(N+1):
            sc[(N-1,state)] = init_chain_score(s,N-1,state)

#        print "\t".join(map(str,[n,0,s[0],sc[(0,'+')],sc[(0,'-')]]))
        for i in range(N,len(s)):

            my_scores = {}
            for state in range(nstates):
                options = []
                for j in range(2):
                    pstate = ( state >> 1) | (j << (N-1))
                    options.append( (sc[(i-1,pstate)] + incr_chain_score(s,i,state), pstate) )
                options.sort(reverse=True)
                if args.debug: print(options)
                sc[(i,state)] = options[0][0]
                bt[(i,state)] = options[0][1]

            print("\t".join(map(str, [n,i,s[i]] + [ sc[i,state] for state in range(nstates) ] + [bt[i,state] for state in range(nstates)]   )))

                


        best_score = max( [ sc[len(s)-1, state ] for state in range(nstates)] )
        best_state = [ state for state in range(nstates) if sc[len(s)-1,state]==best_score ][0]

        i = len(s)-1
        final={}
        final[i] = best_state & 1
        prev = bt[i,best_state]
        while i >N:
            i=i-1
            final[i] = prev&1
            prev = bt[i,prev]

        while i > 0:
            i=i-1
            final[i] = prev&1
            prev = prev >> 1

        for i in range(len(s)):
            print("x:",n,i,s[i],final[i])
