#!/usr/bin/env python3
from __future__ import print_function
import hirise.mapper
import sys
import pickle

mapp = pickle.load(open(sys.argv[1]))


for r in mapp.roots:
    print(mapp.print_name((r.s,r.x)))
