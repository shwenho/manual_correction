#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx



def printdot(g,c,n):
    gg = nx.subgraph(g,c)
    f=open("%d.txt" % (n), "wt")
    nn=1
    lab0={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]
        lab0[d]=lab0.get(d,nn)
        nn+=1
    lab={}
    for x in c:
        p=""
        d=x
        if x[0]=="-": 
            p="-"
            d=x[1:]

        lab[x]=p + str(lab0[d])

    f.write( "graph G {\n")
    for e in gg.edges():
#        f.write( "\t\"%s\" -- \"%s\";\n" % (lab[e[0]],lab[e[1]]) ) #,gg[e[0]][e[1]]['weight'])
        f.write( "\t\"%s\" -- \"%s\" [label=\"%f\"];\n" % (lab[e[0]],lab[e[1]],gg[e[0]][e[1]]['weight']))
    f.write( "};\n")

if __name__=="__main__":


    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-q','--minquality',default=10.0,type=float)
    parser.add_argument('-s','--sortfile',default=False, help="A file containing a list of scaffolds in a format with four columns:  sort index, scaffold name, strand ([01]) and chromosome.")
    parser.add_argument('-c','--chr',default='chr10')
    parser.add_argument('-e','--edgesfile',default='-')
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-l','--links',default=False,action="store_true")

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    oo_info={}
    if args.sortfile:
        f = open(args.sortfile)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            c = l.strip().split()
            #1       Scaffold145693  0       chr1
            #2       Scaffold211250  0       chr1
            #3       Scaffold216395  1       chr1
            oo_info[c[1]] = { 'i': int(c[0]), 'strand': int(c[2]), 'chr':c[3] }
            if args.progress: print(c[1],oo_info[c[1]])

        
    ein = False
    if args.edgesfile=="-":
        ein = sys.stdin
        if args.debug: print("read from stdin")
    else:
        ein = open(args.edgesfile)
        if args.debug: print("read from file",ein)
        

    edges = {}
    s={}
    while True:
        l = ein.readline()
        if not l: break
        if l[0]=="#": continue
        c = l.strip().split()
        #Scaffold384798  Scaffold17269   1476    2038    1       -5.07534447359e-08      5.81851130827   -0.0507533688087        5.81851130827   5.24222929821   5.37436703619   5.74850897484 [(505, 261)]
# isotig800977    isotig1982865   1115    721     1       -1.49003601679e-08      6.1869685253    -0.0149003382353        5.68227740672   6.14070038448   6.1869685253    5.75587975176 4 1 1     1  11

        if args.debug: print(c[:3],oo_info.get(c[0]) , oo_info.get(c[1]))
        if oo_info.get(c[0]) and oo_info.get(c[1]) and oo_info[c[0]]['chr']==args.chr and oo_info[c[1]]['chr']==args.chr:
            #print c
            s[c[0]]=1
            s[c[1]]=1
            if args.links:
                edges[(c[0],c[1])] = tuple( map(float,c[12:16]) )                
            else:
                edges[(c[0],c[1])] = tuple( map(float,c[8:12]) )
            if args.debug: print(c[0],c[1],edges[(c[0],c[1])]) 
            #print c[0],c[1],edges[(c[0],c[1])]

    ss = list(s.keys())
    ss.sort( key=lambda x: oo_info[x]['i'] )

    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.colors import LogNorm
    import random

    minscore = -3.0
    maxscore = 10
    mat = np.zeros((2*len(ss),2*len(ss)))
    for i in range(len(ss)):
        for j in range(len(ss)):
            mat[2*i  ,2*j  ]=max(minscore,min(maxscore,edges.get((ss[i],ss[j]),(0,0,0,0))[0]))
            mat[2*i+1,2*j  ]=max(minscore,min(maxscore,edges.get((ss[i],ss[j]),(0,0,0,0))[1]))
            mat[2*i  ,2*j+1]=max(minscore,min(maxscore,edges.get((ss[i],ss[j]),(0,0,0,0))[2]))
            mat[2*i+1,2*j+1]=max(minscore,min(maxscore,edges.get((ss[i],ss[j]),(0,0,0,0))[3]))

#            mat[2*j  ,2*i  ]=max(-30,edges.get((ss[i],ss[j]),(0,0,0,0))[0])
#            mat[2*j  ,2*i+1]=max(-30,edges.get((ss[i],ss[j]),(0,0,0,0))[1])
#            mat[2*j+1,2*i  ]=max(-30,edges.get((ss[i],ss[j]),(0,0,0,0))[2])
#            mat[2*j+1,2*i+1]=max(-30,edges.get((ss[i],ss[j]),(0,0,0,0))[3])

    for i in range(len(ss)):
        for j in range(len(ss)):
            if edges.get((ss[i],ss[j])) and edges.get((ss[j],ss[i])):
                e1 = edges.get((ss[j],ss[i]))
                e2 = tuple((e1[3],e1[1],e1[2],e1[0]))
                #print "d",ss[i],ss[j],edges.get((ss[i],ss[j])),tuple((e1[3],e1[1],e1[2],e1[0])), e2 == edges.get((ss[i],ss[j]))
#            print "d",i,j,ss[i],ss[j],2*i  , 2*j  , mat[2*i  ,2*j  ] , mat[2*j , 2*i]     , mat[2*i  ,2*j  ] - mat[2*j , 2*i]   
#            print "d",i,j,ss[i],ss[j],2*i+1, 2*j  , mat[2*i+1,2*j  ] , mat[2*j , 2*i+1]   ,mat[2*i+1,2*j  ] - mat[2*j , 2*i+1] 
#            print "d",2*i  , 2*j+1, mat[2*i  ,2*j+1] , mat[2*j+1 , 2*i]   , mat[2*i  ,2*j+1] - mat[2*j+1 , 2*i] 
#            print "d",2*i+1, 2*j+1, mat[2*i+1,2*j+1] , mat[2*j+1 , 2*i+1] , mat[2*i+1,2*j+1] - mat[2*j+1 , 2*i+1]



#    plt.pcolor(mat,norm=LogNorm(vmin=0.001))
    if False:
        plt.pcolor(mat)
        plt.colorbar()
        plt.show()
    else:
        fig = plt.figure()
        ax = fig.add_subplot(111)
#        cax=ax.imshow(mat,interpolation="nearest",norm=LogNorm(vmin=0.001))
        cax=ax.imshow(mat,interpolation="nearest")
        fig.colorbar(cax,shrink=0.5)
        fig.savefig("out.png")
        if args.debug: print("saved to out.png")
#        plt.figure().savefig("out.png")

    exit(0)

    if args.progress: log( str(args) )

    G = nx.read_edgelist(sys.stdin,data=(('weight',float),))

    n=0
#    for c in nx.connected_components(G):
#        if len(c)>4:
#            n+=1
#            print c
#            printdot(G,c,n)

    t= nx.minimum_spanning_tree(G)
    for c in nx.connected_components(t):
        if len(c)>4:
            n+=1
            print(c)
            printdot(t,c,n)
