#!/usr/bin/env python3
import sys
import networkx as nx


def shave_round(G):
    leaves=[]
    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if G.degree(n)==1:
            leaves.append(n)
#    print leaves

    round=1
#    print "#",leaves
    boundary=list(leaves)
    next=[]
    done=[]
    r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next):
                    next.append(nn)
#                    r[nn]=round
        for b in next: 
            if len( [nn for nn in G.neighbors(b) if not r.has_key(nn)] )==1: r[b]=round
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if r.has_key(n): boundary.append(n)
#        next=[]
    for n in G.nodes():
        if not r.has_key(n): r[n]=round
    return r



def link_coords(sc,ll,links):
    ns = len(sc)
    offsets = [ sum([ll[sc[i][0]] for i in range(j)]) for j in range(ns)  ]
    print [ll[sc[i][0]] for i in range(ns)]
    print offsets
    for i in range(ns):
        si,pi = sc[i]
        for j in range(i,ns):
            sj,pj = sc[j]
#            inter_length = sum( [ ll[sc[k][0]] for k in range(i+1,j) ] )
            print "#",si,sj,pi,pj
            for link in links.get((si,sj),[]):
                if pi==0:
                    x=offsets[i] + link[0]
                else:
                    x = offsets[i] + ll[si] - link[0]

                if pj==0:
                    y=offsets[j] + link[1]
                else:
                    y = offsets[j] + ll[sj] - link[1]

                yield (x,y)
#            print "--"
            for link in links.get((sj,si),[]):
                if pi==0:
                    x=offsets[i] + link[1]
                else:
                    x = offsets[i] + ll[si] - link[1]

                if pj==0:
                    y=offsets[j] + link[0]
                else:
                    y = offsets[j] + ll[sj] - link[0]

                yield (x,y)




if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-t','--threshold',default=0.0 ,  type=float)

    parser.add_argument('-H','--head',default=False,type=int)
#    parser.add_argument('-D','--savetreedots',default=False,action='store_true')
    parser.add_argument('-d','--debug',default=False,action='store_true')
#    parser.add_argument('-I','--nointerc',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-M','--maxdegree',default=False,type=int)
    parser.add_argument('-m','--minlength',default=500,type=int)

    parser.add_argument('-L','--links')
    parser.add_argument('-b','--besthits')
    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')
    parser.add_argument('-F','--filter')



    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    ll={}
    if args.lengths:
        f = open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            ll[c[0]]=int(c[1])

        f.close()

    links={}
    if args.filter:
        f = open(args.filter)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            x=[c[0],c[1]]
            x.sort()
            links[x[0],x[1]]=tuple()

        f.close()

    if args.links:
        f=open(args.links)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue
            c = l.strip().split()
            if len(c)<5: continue
            if links.has_key((c[0],c[1])):
                links[c[0],c[1]]= eval(" ".join(c[5:]))
    #            print scores[c[0],c[1]]
            if links.has_key((c[1],c[0])):
                pp = eval(" ".join(c[5:]))
                links[c[1],c[0]]= [ (p[1],p[0]) for p in pp ] 
        f.close()
    
    if args.besthits:
        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
    #            print c[1],besthit[c[1]]
            f.close()

    G=nx.Graph()
    SG=nx.Graph()

    if args.edgefile:
        f = open(args.edgefile)
    else:
        f=sys.stdin
    while True:
        l = f.readline()
        if not l: break
        if l[0]=="#": continue
        c=l.strip().split()
        u,v,w = c[0],c[1],float(c[2])
        if ( not args.lengths ) or (ll[u]>=args.minlength and ll[v]>=args.minlength):
            G.add_edge(u,v,weight=-w)
            if w >= args.threshold:
                SG.add_edge(u,v,weight=-w)
    if args.edgefile:
        f.close()

    print "#Done reading edgelist"

    bad_nodes=[]
    total_discarded_length=0
    total_discarded_length1=0
    n_discarded1=0
    n_discarded2=0
    if args.maxdegree:
        for n in SG.nodes():
            print "#dg:", SG.degree(n)
            if SG.degree(n)>args.maxdegree:
                n_discarded1+=1
                bad_nodes.append(n)
                total_discarded_length += ll[n]
                print "#discard:",n,ll[n],SG.degree(n)
                for nn in SG.neighbors(n):
                    if SG.degree(nn)==1:
                        n_discarded2+=1
                        total_discarded_length1+=ll[nn]
        for n in bad_nodes:
            e_to_remove=[]
            for e in SG.edges([n]):
                e_to_remove.append(e)
            SG.remove_edges_from(e_to_remove)

    print "#total_discarded_length",n_discarded1,n_discarded2,float(total_discarded_length)/1.0e6,float(total_discarded_length1)/1.0e6,float(total_discarded_length+total_discarded_length1)/1.0e6

    strings = []
    ccn=1
    strandings = [ (0,0,0),(0,0,1),(0,1,0),(0,1,1),(1,0,0),(1,0,1),(1,1,0),(1,1,1) ]    
    ynum=0
    for c in nx.connected_components(SG):
        gg = nx.subgraph(G,c)
        t= nx.minimum_spanning_tree(gg)
        trim_level=shave_round(t)
        trimmed_degree={}
        for n in t.nodes():
            m = len([ nn for nn in t.neighbors(n) if trim_level[nn]>2 ])
            trimmed_degree[n]=m

        for n in trimmed_degree.keys():
            if trimmed_degree[n]>2:
                neigh = [ nn for nn in t.neighbors(n) if trim_level[nn]>2 ]
                print "#y:",n,trimmed_degree[n],neigh
                if trimmed_degree[n]==3:
                    ynum+=1
                    chrs_hit={}
                    for nn in [ n,neigh[0],neigh[1],neigh[2]] ]: chrs_hit[nn] = chrs_hit.get(nn,0)+1
                    nchrs_hit = len(chrs_hit.keys())
                    
                    for i,j,k in ((0,1,2),(0,2,1),(1,2,0)):
                        pairs={}
                        for p,q,r in strandings: #((0,0,0),(0,0,1),(0,1,0),(0,1,1),(1,0,0),(1,0,1),(1,1,0),(1,1,1)):
                            print "|\t",p,neigh[i],q,n,r,neigh[j]
                            pairs[p,q,r] = list( link_coords([(neigh[i],p),(n,q),(neigh[j],r)],ll,links) ) 
                        strandings.sort( key=lambda z: sum( [ y-x for x,y in pairs[z] ] ) )
                        chrs = [ besthit.get(nn,[0,0,0])[1] for nn in [n,neigh[i],neigh[j],neigh[k]]  ] 
                        lens = [ ll[nn] for nn in [n,neigh[i],neigh[j],neigh[k]]  ] 
                        print "xy:",ynum,n,i,j,k,neigh[i],neigh[j],neigh[k],nchrs_hit,lens,chrs,strandings[0],len(pairs[strandings[0]]), sum( [ y-x for x,y in pairs[strandings[0]] ] ) ,pairs[strandings[0]]
#                            for x,y in link_coords([(neigh[i],p),(n,q),(neigh[j],r)],ll,links):
#                                print n,neigh[i],neigh[j],p,q,r,besthit.get(n),besthit.get(neigh[i]),besthit.get(neigh[j]),x,y

        ccn+=1
        if args.head and ccn>args.head:
            break



