#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-a',type=int)
parser.add_argument('-b',type=int)

args = parser.parse_args()
if args.progress: print("#",args)

maxs={}
while True:
     l=sys.stdin.readline()
     if not l: break
     c=l.strip().split()
     aa=c[args.a-1]
     bb=float(c[args.b-1])
     
     if not aa in maxs or maxs[aa]<bb:
          maxs[aa]=bb

for aa,bb in maxs.items():
     print(aa,bb)
