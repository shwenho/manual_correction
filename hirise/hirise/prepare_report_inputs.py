#!/usr/bin/env python3

import json
import argh
import os
import sys

import pysam
import h5py

def bp_fmt(x, pos=None):
    if x >= 1e9:
        return "%0.2f Gbp" % (x/1e9)
    if x >= 1e6:
        return "%0.2f Mbp" % (x/1e6)
    if x >= 1e3:
        return "%0.2f kbp" % (x/1e3)
    if x < 0:
        return "%d" % x
    else:
        return "%d bp" % x

def to_mb(bp,precision = 2):
    return "{:,.2f}".format(bp/1000000, p = precision)

def to_kb(bp,precision = 2):
    return "{:,.{p}f}".format(bp/1000, p = precision)

def commas(val):
    return "{:,.2f}".format(val)

def commas_int(val):
    return "{:,d}".format(val)

def get_scaffold_stats(report_stats,
                       assembly_json,
                       stage,
                       insert_sum ):
    stats = json.load(open(assembly_json))
    scaf_stats = stats["scaffolds"]
    contig_stats = stats["contigs"]

    report_stats['phys_cov'] = commas(insert_sum / scaf_stats["Total length"])


    report_stats[stage+"_total"] = to_mb(scaf_stats["Total length"])
    report_stats[stage+"_n50_scaf"] = commas_int(scaf_stats["L50"])
    report_stats[stage+"_n50_min_len"] = to_mb(scaf_stats["N50"])
    report_stats[stage+"_n90_scaf"] = commas_int(scaf_stats["L90"])
    report_stats[stage+"_n90_min_len"] = to_mb(scaf_stats["N90"])
    report_stats[stage+"_long_scaf_len"] = commas_int(scaf_stats["Largest contig"])
    report_stats[stage+"_num_scaf"] = commas_int(scaf_stats["# contigs"])
    report_stats[stage+"_num_scaf_gt_kb"] = commas_int(scaf_stats["# contigs (>= 1000 bp)"])
    report_stats[stage+"_n50"] = to_kb(contig_stats["N50"])
    report_stats[stage+"_gaps"] = commas_int(stats["num_gaps"])
    report_stats[stage+"_gap_percent"] = stats["percent_gaps"]

def get_insert_sum(archive,min_ins = 1000, max_ins = 100000, step_size = 1000):
    data = h5py.File(archive)
    lib_stats = data['library_stats']
    min_index = min_ins / step_size
    max_index = max_ins / step_size
    tot = 0
    for lib in lib_stats:
        ins_cdf = lib_stats[lib]['insert_size_cdf']
        tot += ins_cdf[max_index] - ins_cdf[min_index]
    return tot

def num_gaps_closed(gapclose_file):
    num_closed = 0
    fh = open(gapclose_file)
    for line in fh:
        num_closed += 1
    return num_closed

def count_joins_and_breaks(table_file):
    fh = open(table_file)
    n_joins = 0
    n_breaks = 0
    for l in fh:
        parts = l.split()
        if parts[5] != "0":
            n_joins += 1
        if parts[2] != "0":
            n_breaks += 1
    return n_joins,n_breaks

def get_bam_stats(bams):
    stats = {}
    for bam in bams:
        fh = pysam.AlignmentFile(bam)
        this_stats = {}

        readlen = 0
        # divide by 2M to convert from reads to read pairs
        this_stats["nreadsM"] = int((fh.mapped + fh.unmapped)/2000000) 
        for i,aln in enumerate(fh):
            readlen = len(aln.seq) if len(aln.seq) > readlen else readlen
            if i == 10000:
                break
        this_stats["readlen"] = readlen
        stats[os.path.basename(bam)] = this_stats
    return stats

@argh.arg('-a','--archive',required=True)
@argh.arg('--input-stats',required=True)
@argh.arg('--final-stats',required=True)
@argh.arg('-t','--table-file',required=True)
@argh.arg('--cov-hist',required=True)
@argh.arg('--contiguity-plot',required=True)
@argh.arg('-b','--bams',nargs='+',required=True)
@argh.arg('-g','--gapclose')
@argh.arg('-p','--project',required=True)
@argh.arg('-c','--customer',required=True)
@argh.arg('--institution',required=True)
@argh.arg('-o','--output',required=True)
def main(archive = None,
         input_stats = None,
         final_stats = None,
         table_file = None,
         cov_hist = None,
         insert_dist = None,
         contiguity_plot = None,
         bams = None,
         gapclose = None,
         link_density = None,
         min_insert = 1000,
         max_insert = 100000,
         project="Derp",
         customer="Derpy McDerp",
         institution = "University of Derp",
         output = "report_metrics.json"):
    final_json = {}

    hdf5_fh = h5py.File(archive)

    if gapclose:
        num_closed = num_gaps_closed(gapclose)
        final_json['num_closed'] = num_closed
    
    insert_sum = get_insert_sum(archive,min_ins=min_insert,max_ins=max_insert)
    get_scaffold_stats(final_json, input_stats, "pre", insert_sum)
    get_scaffold_stats(final_json, final_stats, "post", insert_sum)

    njoins,nbreaks = count_joins_and_breaks(table_file)
    final_json["num_joins"] = njoins
    final_json["num_breaks"] = nbreaks

    final_json["libraries"] = get_bam_stats(bams)

    final_json["coverage_plot"] = os.path.abspath(cov_hist)
    final_json["insert_dist"] = os.path.abspath(insert_dist)
    final_json["n50splot"] = os.path.abspath(contiguity_plot)
    if link_density:
        final_json["link_density"] = os.path.abspath(link_density)

    final_json["project"] = project
    final_json["person"] = customer
    final_json["organization"] = institution

    final_json["min_ins_kb"] = to_kb(min_insert,0)
    final_json["max_ins_kb"] = to_kb(max_insert,0)

    oh = open(output,"w")
    json.dump(final_json,oh,sort_keys=True,indent=0)

if __name__ == "__main__":
    argh.dispatch_command(main)
