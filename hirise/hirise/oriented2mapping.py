#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
import pysam
import sys
import math
import networkx as nx
import hirise.mapper
import pickle as pickle

def log(s):
    print(s)

#['Scaffold1', 'CONTIG1', '+isotig2133903', '1', '105', '16.5636363636364']
#['Scaffold1', 'GAP1', '57', '14']
#['Scaffold1', 'CONTIG2', '+isotig1165818', '163', '348', '33.0517647058824']
#['Scaffold1', 'GAP2', '-22', '1']
#['Scaffold1', 'CONTIG3', '+isotig1090484', '327', '475', '19.6567676767677']
#['Scaffold1', 'GAP3', '31', '11']
#['Scaffold1', 'CONTIG4', '+isotig1523405', '507', '676', '38.6750833333333']
#['Scaffold2', 'CONTIG1', '+isotig2311227', '1', '158', '46.1021296296296']
#['Scaffold2', 'GAP1', '169', '5']
#['Scaffold2', 'CONTIG2', '-isotig2268088', '328', '476', '38.3435353535354']

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--map',required=True)
    parser.add_argument('--outmap',required=True)
    parser.add_argument('-d','--debug',default=False,action="store_true")

    nodes={}

    args = parser.parse_args()

    rl = sys.getrecursionlimit()
    print(rl)

    rl = sys.setrecursionlimit(50000)

#    if args.progress: log( str(args) )

    mapp = pickle.load( open(args.map)    )

    ls=False
    ss=-1
    mapp.obsoleted={}
    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=='#': continue
        c = l.strip().split()
        #print c
        if c[0]=="x:":
            if c[1]==ls:
                x=mapp.find_node_by_name(lx)
                y=mapp.find_node_by_name(c[3])
                #print x,y
                if ss  =="0": 
                    x = x.leaf
#                else:
#                    x = x.root
                if c[4]=="1": 
                    y = y.leaf
#                else:
#                    y = y.root
                print("join",lx,ss,c[3],c[4],x,y) #,x.leaf,y.leaf                
                #print x,y
                mapp.add_join( x , y )
            lx=c[3]
            ls=c[1]
            ss=c[4]

    mapp.update_roots()

    #    f=open(args.outmap,"wt")
    mapp.write_map_to_file(args.outmap)

    #    pickle.dump(mapp, f)
    #    f.close()

