#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import pysam
from hirise.coverage_hist_tools import coverage_histogram

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False,type=int)
parser.add_argument('-N','--nsamples',default=10000,type=int)
parser.add_argument('-m','--minL',default=2000,type=int)
parser.add_argument('-x','--maxL',default=7000,type=int)
parser.add_argument('-i','--interval',default=1000,type=int)
parser.add_argument('-q','--mapq',default=10.0,type=float)
parser.add_argument('-b','--bamfiles',required=True)

args = parser.parse_args()

bams=[]
for bf in args.bamfiles.split(','):
     bams.append( pysam.Samfile(bf,"rb") )

nsamples=args.nsamples
minL=args.minL
maxL=args.maxL
mapq=args.mapq
interval = args.interval

#from hirise.coverage_hist_tools import coverage_histogram
histogram = coverage_histogram(nsamples,bams,minL,maxL,mapq,interval)

coverages = list(histogram.keys())
coverages.sort()
for c in coverages:
     print("{}\t{}".format(c,histogram[c]))

