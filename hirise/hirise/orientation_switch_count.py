#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse


if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Print progress info")
     parser.add_argument('-L','--length',default=False,type=int,help="File containing lenghts.")

     args = parser.parse_args()
     if args.progress: print("#",args)

     total=0
     switches=0
     last_c=[' ']*7
     last_bp=[0,0]
     while True:
          l=sys.stdin.readline()
          if not l: break
          if l.startswith('#'): continue

#          AGCAGAGGCTCTCCATGGGCACCCCACCCCTGCAGCAAACTTCTTGCCTGGGCATCCAGGTGTTTCCATACATCCTCTGAAATCTAGGCGAAGGTTTCCCA2_maternal90160581+ScbYBnM_707759+
#          AGCAGAGGCTCTCCATGGGCACCCCACCCCTGCAGCAAACTTCTTGCCTGGGCATCCAGGTGTTTCCATACATCCTCTGAAATCTAGGCGAAGGTTTCCCA2_maternal90160581+ScbYBnM_707759+
#          CCTTGGCAGCTTCCACGTGGTATTGAGCCTGCAAGTGCACAGAAGTCAAGAATTGGGGTTGGGAAACCTTCGCCTAGATTTCAGAGGATGTATGGAAACAC2_maternal90160640-ScbYBnM_707818-
#          GGCTTGGGTCCTCTGAAGCCACAGCCTGAGCTCTACATTGGCCCCTTTTGGCCATGGCTGGAAGAGCTGGGACACAGGCCAACAAGTCCTTAGTCTGCACA2_maternal90160739+ScbYBnM_707917+
#          AATTCAAATGAAAGCATGAATCAAGAAAATCAGAGGAAACAAGACGGTAGGTTTAACCCTAACTTTACAATAATTGCATCGTAAATAAACAAATTTTTCCA2_maternal89372709-ScbYBnM_7071377-
#          AAAAAATCTATAATTAAACTAATTCAAATGAAAGCATGAATCAAGAAAATCAGAGGAAACAAGACGGTAGGTTTAACCCTAACTTTACAATAATTGCATCG2_maternal89372729-ScbYBnM_7071397-

          c = l.strip().split()

#          if total%100000==0:
#               print(*last_c,sep="\t")
#               print(*c,sep="\t")
#               print((c[3]==c[6]),(last_c[3]==last_c[6]),sep="\t")
#               print(total,switches,switches / (1+total),sep="\t")
          if not (c[1]==last_c[1] and c[4]==last_c[4]): 
               last_c=c
               continue
          total+=1
          if ( (c[3]==c[6]) and not (last_c[3]==last_c[6]) ) \
             or (not (c[3]==c[6]) and (last_c[3]==last_c[6]) ): 
#               print(*last_c,sep="\t")
#               print(*c,sep="\t")
               switches+=1
               if c[4]==last_bp[0]:
                    print("d:",int(c[5])-int(last_bp[1]))
               last_bp = (c[4],c[5])
          last_c=c
     print(total,switches,switches / total,sep="\t")
