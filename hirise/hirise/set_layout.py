#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
from hirise.hirise_assembly import HiriseAssembly

if __name__=="__main__":
     import sys
     import argparse

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False  ,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-X','--explode',default=False  ,action="store_true",help="Explode the layout to one scaffold per broken contig.")
     parser.add_argument('--implode',default=False  ,action="store_true",help="Implode the layout to one scaffold total.")
     parser.add_argument('-L','--layout',default=False ,help="A file containing a layout of contigs.")
     parser.add_argument('-i','--infile',default=False ,help="Filename for serialised assembly input file.")
     parser.add_argument('-o','--outfile',default=False,help="Filename for writing a list of segments on the raw contigs to mask for being promiscuous in linking.")

     args = parser.parse_args()

     if args.infile:
          asf = HiriseAssembly()
          asf.load_assembly(args.infile)

     if args.layout:
          asf.load_playout(args.layout)

     if args.explode:
          asf.explode_layout()
          
     if args.implode:
          asf.implode_layout()
     
     if args.outfile:
          asf.save_assembly( args.outfile )

