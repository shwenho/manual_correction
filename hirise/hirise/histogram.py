#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import sys


column=int(sys.argv[1])
binsize=float(sys.argv[2])

counts={}

n=0
total=0.0
while True:
    line = sys.stdin.readline()
    if not line:
        break
    if line[0]=="#":
        continue
    n+=1
    c = line.strip().split()
    if (len(c)<column): continue
    x=float(c[column-1])
    total+=x
    if x>=0:
        bin = int(old_div(x,binsize))
    else:
        bin = int(old_div(x,binsize))-1
    if bin not in counts:
        counts[bin]=0
    counts[bin]+=1
#    print x

bins = list(counts.keys())
bins.sort()

r=[ min(counts.values()),max(counts.values()) ]
#total=sum(counts.values())
xfact = old_div(50.0,r[1])
#print xfact

print("# n:",n)
print("# sum:", total)
print("# mean:",old_div(float(total),n))

import sys

cum=0.0
lastb=False
for bini in bins:
    try:
        if lastb:
            if bini-lastb>1:
                if bini-lastb<5:
                    for interbin in range(lastb+1,bini):
                        print("\t".join(map(str,["{: 10f}".format(interbin*binsize),"...", "{: 10f}".format((interbin+1)*binsize), 0,  "%0.6f" %( old_div(float(cum),n)) ,0.0,"|" ])))

                else:
                    interbin=lastb+1
                    print("\t".join(map(str,["{: 10f}".format(interbin*binsize),"...", "{: 10f}".format((interbin+1)*binsize), 0,  "%0.6f" %( old_div(float(cum),n)) ,0.0,"|" ])))

                    print("#   ...")
                    interbin=bini-1
                    print("\t".join(map(str,["{: 10f}".format(interbin*binsize),"...", "{: 10f}".format((interbin+1)*binsize), 0,  "%0.6f" %( old_div(float(cum),n)) ,0.0,"|" ])))

        cum+=counts[bini]

        print("\t".join(map(str,["{: 10f}".format(bini*binsize),"...", "{: 10f}".format((bini+1)*binsize), counts[bini],  "%0.6f" %( old_div(float(cum),n)) ,"%0.6f" %(old_div(float(counts[bini]),n)),"|"+"X"*int(counts[bini]*xfact) ])))
        lastb=bini
    except BrokenPipeError as e:
        break
