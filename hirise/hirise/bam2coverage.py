#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import sys
import argparse
import pysam

if __name__=="__main__":

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-p','--progress',default=False,action="store_true",help="Turn on progress ouput")
     parser.add_argument('-H','--histogram',default=False,action="store_true",help="Compute histogram")
     parser.add_argument('-C','--coverage',default=False,action="store_true",help="Dump coverage")
     parser.add_argument('-b','--bamfile',default=[],help="Bam file.",action="append")
     parser.add_argument('-s','--scaffolds',default=[],help="Scaffolds to examine.",action="append")
     parser.add_argument('-q','--mapq',default=10,type=int,help="Mapq.")
     parser.add_argument('-L','--minl',default=0,type=int,help="Min scaffold length to include.")
     parser.add_argument('-o','--outhist')

     min_sep=1000.0   ;
     max_sep=50000.0;

     args = parser.parse_args()

     bamfiles=[]
     for bf in args.bamfile:
          print("#",bf)
          bamfiles.append(pysam.Samfile(bf))

     h=bamfiles[0].header
     seqs=h['SQ']

     slen=[ s['LN'] for s in seqs ]
     snam=[ s['SN'] for s in seqs ]

     llen={}
     for i in range(len(snam)):
          llen[snam[i]]=slen[i]
     
     out=0
     in1=1
     in2=2

     state=out

     h={}

     last_scaffold=False
     last_x="-"
     start_x=0

#     for c in mycontigs:
#          if llen[c]<1000: continue
#          db=[]
#          nr=0

     tl=0
     nr=0
     last_tid= -1

     scaffolds=snam
     if len(args.scaffolds)>0:
          scaffolds = args.scaffolds

     histogram={}
     total=0
     for scaffold in scaffolds:
          if args.minl > llen[scaffold] : continue
          total+=llen[scaffold]
          if args.progress: print("#",scaffold,llen[scaffold],total,sep="\t")
          buf=[(0,0),(llen[scaffold],0)]
          for bamfile in bamfiles:
#               print(bamfile)
               for aln in bamfile.fetch(region=scaffold,until_eof=True):
                    if aln.is_duplicate     : continue
                    if aln.mapq < args.mapq : continue
                    
                    if (aln.tid!=last_tid) :
                         scaffold=bamfile.getrname(aln.tid)
                         
          #          print(dir(aln))
                    if aln.tlen<0: continue
                    if (abs(aln.tlen)<min_sep): continue
                    if (abs(aln.tlen)>max_sep): continue
                    if (aln.tid!=aln.rnext): continue

#                    print(aln.tid,aln.pos,aln.pnext,sep="\t")
                    buf.append( ( aln.pos, +1 ) )
                    buf.append( ( aln.pnext, -1 ) )
          buf.sort()
          rs=0
          if args.coverage:
               print(scaffold,0,0,sep="\t")

          lastx=0
          for x,dy in buf:

               if args.histogram:
                    histogram[rs]=histogram.get(rs,0) + x-lastx
               rs+=dy
               if args.coverage:
                    print(scaffold,x,rs,sep="\t")
               lastx=x

     if args.histogram:
          for x in sorted(histogram.keys()):
               print("f:",x,histogram[x],sep="\t")
