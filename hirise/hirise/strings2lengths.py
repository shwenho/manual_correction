#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
if __name__=="__main__":

    import sys

    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-l','--lengths')
    parser.add_argument('-s','--strings')

    parser.add_argument('-d','--debug',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')

    args = parser.parse_args()

    f = open(args.lengths)


    ll={}
    while True:
        l = f.readline()
        if not l: break
        if l[0]=="#": continue

        c=l.strip().split()
        ll[c[0]]=int(c[1])

    f.close()

    f=open(args.strings)
    while True:
        l = f.readline()
        if not l: break
        if l[0]=="#": continue
        if not l[:2]=="y:": continue
        st = eval( l[2:])
#        print l

#        print st
        x=0
        for s in st:
            if s not in ll:
                print("wtf?",s,l)
            x+=ll[s]
            if args.debug: print("#l:",ll[s], s)
            print("#",s,x)
        print(x)
                   

    f.close()
    


