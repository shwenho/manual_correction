#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import pysam
import sys
import math
import networkx as nx
import hirise.mapper
import pickle

def log(s):
    print(s)

#['Scaffold1', 'CONTIG1', '+isotig2133903', '1', '105', '16.5636363636364']
#['Scaffold1', 'GAP1', '57', '14']
#['Scaffold1', 'CONTIG2', '+isotig1165818', '163', '348', '33.0517647058824']
#['Scaffold1', 'GAP2', '-22', '1']
#['Scaffold1', 'CONTIG3', '+isotig1090484', '327', '475', '19.6567676767677']
#['Scaffold1', 'GAP3', '31', '11']
#['Scaffold1', 'CONTIG4', '+isotig1523405', '507', '676', '38.6750833333333']
#['Scaffold2', 'CONTIG1', '+isotig2311227', '1', '158', '46.1021296296296']
#['Scaffold2', 'GAP1', '169', '5']
#['Scaffold2', 'CONTIG2', '-isotig2268088', '328', '476', '38.3435353535354']

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--map')
    parser.add_argument('--outmap')
    parser.add_argument('-d','--debug',default=False,action="store_true")

    nodes={}

    args = parser.parse_args()

#    if args.progress: log( str(args) )

    mapp = pickle.load( open(args.map)    )

    ls=False
    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=='#': continue
        c = l.strip().split()
        print(c)
        if c[1][0:3]=="CON":
            if c[0]==ls:
                print("join",lx,c[2],int(c[3])-lc)

            lx=c[2]
            ls=c[0]
            lc=int(c[4])
