#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from past.utils import old_div
from scipy.stats import poisson
import math

def b3(n,m,G,l):
    return n*(1-(1+old_div(l,(old_div((m*G),n))))*math.exp(old_div(-l,(old_div((m*G),n)))))

def s3(n,m,c):
    return m*c*(1-math.exp(old_div(-n,(m*c)))*(1 + (old_div(n,(m*c))) +0.5*(old_div(n,(m*c)))**2 ))

l=150000.0
#n=1e7
m=90000.0
G=3e9
c=10000

n=1e7

pv=0
while pv<0.05:
    b =  b3(n,m,G,l)
    s =  s3(n,m,c)
    pv = 1.0-poisson.cdf(int(s)-1,b)
    print(n,pv)
    n=n*0.99
