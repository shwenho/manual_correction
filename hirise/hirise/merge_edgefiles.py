#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
#!/usr/bin/env python3
args=False

def process(l1,l2):
    if len(l1)==0 and len(l2)==0: return
    if args.debug: print("xy:",len(l1),len(l2))
    n=False
    o={}
    h1 = {}
    for l in l1:
        c = l.strip().split('\t')
        h1[c[1]]=c[2:]
        n=c[0]
        o[c[1]]=True
    h2 = {}
    for l in l2:
        c = l.strip().split('\t')
        h2[c[1]]=c[2:]
        n=c[0]
        o[c[1]]=True

    for k in list(o.keys()):
        if h1.get(k,False) and not h2.get(k,False):
            print("\t".join(map(str,[n,k,"\t".join(h1[k])])))
        elif h2.get(k,False) and not h1.get(k,False):
            print("\t".join(map(str,[n,k,"\t".join(h2[k])])))
        else:
            t = int(h1[k][2]) + int(h2[k][2])
#            print "#j:"
            print("\t".join(map(str,[n,k,h1[k][0],h1[k][1],t,eval("\t".join(h1[k][3:]))+eval("\t".join(h2[k][3:]))])))
#            print n,k,h1[k][0],h1[k][1],t,eval("\t".join(h1[k][3:])),eval("\t".join(h2[k][3:]))
    pass

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-1','--file1')
    parser.add_argument('-2','--file2')
    parser.add_argument('-d','--debug',action="store_true")
    args = parser.parse_args()

    f1 = open(args.file1)
    f2 = open(args.file2)


    lb1 = []
    lb2 = []
    f1done=False
    f2done=False
    while True:
        if not f1done: l = f1.readline()
        if not l: 
            f1done=True
            
        while (not f1done) and (l[0]!="#"):
            lb1.append(l.strip())
            l = f1.readline()
            if not l:
                f1done=True

        
        if not f2done: l2 = f2.readline()
        if not l2: 
            f2done=True
        while (not f2done) and (l2[0]!="#"):
            lb2.append(l2.strip())
            l2 = f2.readline()
            if not l2: 
                f2done=True

        
        process(lb1,lb2)
        if f1done and f2done: break
        s1 = l[3:].strip()
        s2 = l2[3:].strip()
        print(l.strip(), s1,s2)
        lb1=[]
        lb2=[]

#    process(lb1,lb2)
