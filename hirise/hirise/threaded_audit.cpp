#include <iostream>
#include <hash_map>
#include <fstream>
#include <pthread.h>
#include <sstream>
#include <string>
#include <cstring>
#include <algorithm>
#include <unistd.h>
//#include <thread>
//#include <chrono>

// compile with:
// g++  hmap3.cpp -lpthread  -std=gnu++11 

const int MAX_KMER = 256;

using namespace std;
using namespace __gnu_cxx;

namespace __gnu_cxx {
  template<>
  struct hash<std::string>
  {
    hash<char*> h;
    size_t operator()(const std::string &s) const
    {
      return h(s.c_str());
    };
  };

  //  template<>
  //struct hash<std::cstring>
  //{
  //  hash<char*> h;
  //  size_t operator()(const std::cstring &s) const
  //  {
  //    return h(s);
  //  };
  //};
}

string missing = "CACCTTGGCCTCCCAAAGTGCTGGGATTCCAGGCGTGAGCCCCAGTGTCTGGTCCCCTACCCCTTTTATGCCCAGCTTTTAAAGGCACATAACATCACTTC";

/*struct eqstr {
  bool operator()(const char* s1, const char* s2, int kmersize) const
  {
    return strncmp(s1, s2,kmersize) == 0;
  }
  };*/

struct arg_struct {
  string sn;
  string seq;
  int k;
  ofstream fh;
  hash_map<string, int> *hmap;
  //hash_map<const char*, int, hash<const char*>, eqstr> *hmap;
  char rc[MAX_KMER];
  int id;
  int* done;
};


static char complement[256];
 
void set_complement(int c1, int c2) {
  complement [c1] = c2;
  complement [c2] = c1;
  complement [c1 | 0x20] = c2;
  complement [c2 | 0x20] = c1;
}
 

void reverse_complement(unsigned char * ptr1, unsigned char * ptr2) {
  unsigned char c;
 
  while (ptr1 <= ptr2) {
 
    if (*ptr1 == 10) {
      ptr1++;
      continue;
    }
 
    if (*ptr2 == 10) {
      ptr2--;
      continue;
    }
 
    c = complement[*ptr1];
    *ptr1++ = complement [*ptr2];
    *ptr2-- = c;
  }
}
 

void *handle( void* arguments) {
  


  struct arg_struct *args = (struct arg_struct *)arguments; 

  //char * k1 = new char [ args->k +1 ] ;  
  //char * rc = new char [ args->k +1 ] ;  

  //  cout << "in thread for:"  << args->sn << " " << args->seq.length() << " " <<  args->seq.substr(0,100) << endl ; 

  if (args->k < args->seq.length()) {

  int n=0;
  for (int i=0; i<=args->seq.length()-args->k; i++) {
    n++;
    string kmer = args->seq.substr(i,args->k);

    size_t found = kmer.find("N");
    if (found!=string::npos) { //cout << kmer << endl; 
      continue; }
    //    if (kmer == missing) {  
    //args->fh << "spotted " << kmer << " " << n << " " <<  args->sn << endl;
      //};

    //    strcpy(k1,kmer.c_str());
    strcpy(&args->rc[0],kmer.c_str());
    reverse_complement((unsigned char *) &args->rc[0],(unsigned char *) &args->rc[100]);

    //    args->fh << "f:"<< kmer.c_str() << "." << (args->hmap->find( kmer.c_str() )!=args->hmap->end())  << endl; args->fh.flush();
    //args->fh << "r:"<<rc << "." << ( args->hmap->find( rc ) != args->hmap->end() ) << endl;   args->fh.flush();
    //    args->fh << kmer ;
    //args->fh << "  XXXXX"  ; 
    //args->fh << endl;

    hash_map<string, int>::iterator itt = args->hmap->find(kmer);

    if ( itt != args->hmap->end() ) {
    //   if ( 1!=0 ) {
      args->fh << kmer ;
      args->fh << " " << args->sn << " " << i << " " << i+50 << " +" ; // << " " << itt->second ; 
      args->fh << endl;
    }  else {
      itt = args->hmap->find( string(args->rc) );
      if ( itt != args->hmap->end() ) {
       	args->fh << args->rc ;
	args->fh << " " << args->sn << " " << i << " " << i+50 << " -" ; // << " " << itt->second ; 
      	args->fh << endl;
	
      }
      
    }
  }
  
  
  //cout << "done thread for:"  << args->sn << " " << n <<endl ; 

  //  delete[] k1;
  //  delete[] rc;

  args->fh.flush();
  }

  *args->done = 1;

}


typedef struct {
  int x;
  int y;
} point;

int main(int argc, char *argv[])
{
  int ii=1;
  int positional = 0;
  fstream in_stream;
  fstream in_stream2;
  std::string output;
  std::string input1;
  std::string input2;
  int kmersize = 101;

  if(argc < 3) {
    cout << "Usage: threaded_audit audit_mers genome.fasta genome.fasta.audit.part -k <kmersize>" << endl;
    exit(1);
  }


  while (ii<argc) {
    if ( strcmp(argv[ii],"-h")==0 ) {
      cout << "Usage: threaded_audit audit_mers genome.fasta genome.fasta.audit.part -k <kmersize>" << endl;
      exit(1);
    } else if  ( strcmp(argv[ii],"-k")==0 ) {
      ii++;
      kmersize = std::stoi(argv[ii]);
    } else {
      if (positional == 0) {
	input1 = argv[ii];
      }
      else if (positional == 1) {
	input2 = argv[ii];
      }
      else if (positional == 2) {
	output = argv[ii];
      }
      else {
	std::cout << "Too many positional arguments." << positional  << endl;
	cout << "Usage: threaded_audit audit_mers genome.fasta genome.fasta.audit.part -k <kmersize>" << endl;
	exit(1);
      }
      positional++;
    }
    ii++;
  }

  //declaration
  //hash_map<const char*, int, hash<const char*>, eqstr> hmap;
  hash_map<string, int> hmap;



  int n=0;
  string line2;
  string line;

  set_complement('A','T');
  set_complement('T','A');
  set_complement('G','C');
  set_complement('C','G');
  set_complement('N','N');

  in_stream2.open(input1,std::fstream::in);
  if (in_stream2.fail()) {
    cout << in_stream.good() << "failed to open " << input1 << endl;
    exit(1);    
  }
 
  in_stream.open(input2,std::fstream::in);

  if (in_stream.fail()) {
    cout << in_stream.good() << "failed to open " << input2 << endl;
    exit(1);
  }
 

  vector<std::string> strings;
  int done[32];
  string name = "";

  pthread_t x;
  pthread_t threads[32];
  int n_threads=32;
  int j=0;

  struct arg_struct targs[32];

  char buff[1024];
  //  std::string buffAsStdStr = buff;

  for (int i=0;i<n_threads;i++) {
    done[i]=1;
    //    string filename = str( format("outfile.%1%.txt\n") % i );
    sprintf(buff, "%s.%d", output.c_str(), i);

    std::string filename = buff; 
    //targs[i].sn = filename;
    //cout << filename << endl;
    targs[i].fh.open(buff);

    if (targs[i].fh.fail()) {
      cout << targs[i].fh.good() << "failed to open " << buff << endl;
      exit(1);
    } else {
      cout << "opened for writing: " << buff << endl ;
    }

    targs[i].hmap = &hmap;
    targs[i].k = kmersize;
  }

  char * k1 = new char [MAX_KMER ] ;  

  while(!in_stream2.eof())
    {
      n++;
      in_stream2 >> line2;
      //list.push_back(line);
      //      cout << line.c_str() <<  "." << hmap.hash_funct()(  line.c_str()) << endl;
      //strcpy(k1,line.c_str());
      //hmap[k1] = 1;
      //cout << line << " " << n << endl;
      hmap[line2] = n;
    }

  in_stream2.close();

  cout << "number of kmers " << hmap.size() << endl ;


  //cout << "X" << endl;
  //cout << "X" << endl;

  const char* const delim = "";

  while(!in_stream.eof())
    {
      n++;
      in_stream >> line;

      //      cout << "Y" << endl;
      //cout << line << endl;
      //cout << "Z" << endl;

      if (line.compare(0,1,">")==0) {
	//cout << line << endl;
	if (name.length() > 0) {

	  ostringstream imploded;
	  copy(strings.begin(), strings.end(), ostream_iterator<string>(imploded, delim));


	  int i=0;
	  while (done[i]==0) { if (i==n_threads-1) {
	      usleep(1);
	    }; i=(i+1)%n_threads; }
	  pthread_join(threads[i],NULL);

	  //cout << "fasta record:" << name << " " << strings.size() << " " << imploded.str().length() << " ; use thread: " << i << endl;
	
	  targs[i].sn = name;
	  targs[i].seq = imploded.str();
	  done[i]=0;
	  targs[i].done = &done[i];
	  pthread_create(&(threads[i]), NULL, &handle, (void *)&(targs[i]));

	  //cout << x.substr(0,100) << endl;
	
	  //j=(j+1)%n_threads;
	}
	strings.clear();
	name = line.substr(1,line.length()-1);
	//cout << name.length() << "\n";
      } else {
	transform(line.begin(), line.end(),line.begin(), ::toupper);
	strings.push_back(line);
	//	cout << strings.size() << "\n";
      }
      //list.push_back(line);
      //hmap[line.c_str()] = 1;
    }

	  ostringstream imploded;
	  copy(strings.begin(), strings.end(), ostream_iterator<string>(imploded, delim));


	  int i=0;
	  while (done[i]==0) { if (i==n_threads-1) {
	      usleep(1);
	    }; i=(i+1)%n_threads; }
	  cout << "fasta record:" << name << " " << strings.size() << " " << imploded.str().length() << " ; use thread: " << i<< endl;
	
	  targs[i].sn = name;
	  targs[i].seq = imploded.str();
	  done[i]=0;
	  targs[i].done = &done[i];
	  pthread_create(&(threads[i]), NULL, &handle, (void *)&(targs[i]));
  

  in_stream.close();
  
  int all_done=0;

  while (all_done==0) {
    all_done=1;
    int n_running=0;
    for (int i=0;i<n_threads; i++) {
      if (done[i]==0) {all_done = 0; n_running++; }
    }
    cout << "still running: " << n_running << endl ; 
    sleep(10);
  }

  for (int i=0;i<n_threads;i++) {
    targs[i].fh.close();
  }


  return EXIT_SUCCESS;
  }
