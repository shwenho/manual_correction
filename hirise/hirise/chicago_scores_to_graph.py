#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import map
from past.utils import old_div
import pysam
import sys
import math
import networkx as nx
import hirise.mapper
import pickle

def log(s):
    print(s)

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()
#    parser.add_argument('-b','--bamfile',required=True)
    parser.add_argument('-s','--savegraph',default=False)
    parser.add_argument('-S','--restoregraph',default=False)
    parser.add_argument('-H','--head',type=int,default=False)
    parser.add_argument('--insertHist',default=False,action="store_true")
    parser.add_argument('-d','--debug',default=False,action="store_true")
    parser.add_argument('-m','--minlength',default=0,type=int)
    parser.add_argument('-M','--min_insert',default=1000,type=int)
    parser.add_argument('-K','--maxdegree',default=1000000000,type=int)
    parser.add_argument('-e','--endsonly',default=False,action="store_true")
    parser.add_argument('-L','--contiglist',default=False)
    parser.add_argument('-E','--endwindow',default=50000,type=int)
    parser.add_argument('-D','--dotfile',default=False)
    parser.add_argument('-p','--progress',default=False,action="store_true")
    parser.add_argument('-B','--best',default=False,action="store_true")
    parser.add_argument('-l','--links',default=False,action="store_true")
    parser.add_argument('-c','--cutoff',default=(old_div(200.0,1.0e13)),type=float)
    parser.add_argument('-q','--minquality',default=10.0,type=float)
    parser.add_argument('--refmap')
    parser.add_argument('--map')
    parser.add_argument('--outmap')

    nodes={}

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )

    rmap = False
    if args.refmap:
        rmap = pickle.load( open(args.refmap) )
    mapp = pickle.load( open(args.map)    )

    edges = []
    degrees = {}

    ss={}
    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=='#': continue
        c = l.strip().split()
        s1,s2 = c[0],c[1]
        l1,l2 = int(c[2]),int(c[3])

        r1,r2 = False,False
         

        if rmap:
            # rmap contains a representation of the scaffolds that were fed into 
            # the edge calculator.  

            s1r   = rmap.find_node_by_name(s1)
            s2r   = rmap.find_node_by_name(s2)

            s1l  = r1.leaf
            s2l  = r2.leaf
 
            r1  = mmap.find_node_by_pair((s1r.s,s1r.x))
            r2  = mmap.find_node_by_pair((s2r.s,s2r.x))

            l1 = r1.leaf.mapped_coord
            l2 = r2.leaf.mapped_coord
        
        both_long = False
        try: 
            both_long = ((l1>args.minlength) and (l2>args.minlength))
        except:
            print(l)
            exit(0)
        if not both_long: continue
        s00,s01,s10,s11 = list(map(float,c[8:12]))
        if args.links:
            s00,s01,s10,s11 = list(map(int,c[14:]))

        if rmap:

            l1  = mmap.find_node_by_pair((s1l.s,s1l.x))
            l2  = mmap.find_node_by_pair((s2l.s,s2l.x))

        else:

            r1 = mapp.find_node_by_name(c[0])
            r2 = mapp.find_node_by_name(c[1])
            l1 = r1.leaf
            l2 = r2.leaf

            
        if args.debug: print([s00,s01,s10,s11],max([s00,s01,s10,s11]),float(c[7]),l.strip())

        maxs =  max([s00,s01,s10,s11])
        if maxs>args.cutoff:
            ss[c[0]]=1
            ss[c[1]]=1

#            print "x1",r1,c[0] ,l1
#            print "x2",r2,c[1] ,l2

            if s00>args.cutoff and ((not args.best) or s00==maxs ):
#                mapp.add_join(l1,r2,1000)
#                edges.append( (c[0],"-"+c[1],s00) )
                edges.append((l1,r2,s00))
#                degrees[    c[0]] = degrees.get(    c[0],0)+1
#                degrees["-"+c[1]] = degrees.get("-"+c[1],0)+1
                degrees[l1] = degrees.get(l1,0)+1
                degrees[r2] = degrees.get(r2,0)+1
#                print "\t".join(map(str,[c[0],"-"+c[1],s00]))
            if s01>args.cutoff and ((not args.best) or s01==maxs ):
#                mapp.add_join(l1,l2,1000)
#                edges.append( (c[0],c[1],s01) )
                edges.append((l1,l2,s01))
#                degrees[    c[0]] = degrees.get(    c[0],0)+1
#                degrees[    c[1]] = degrees.get(    c[1],0)+1
                degrees[l1] = degrees.get(l1,0)+1
                degrees[l2] = degrees.get(l2,0)+1
#                print "\t".join(map(str,[c[0],c[1],s01]))
            if s10>args.cutoff and ((not args.best) or s10==maxs ):
#                mapp.add_join(r1,r2,1000)
#                edges.append( ("-"+c[0],"-"+c[1],s10) )
                edges.append((r1,r2,s10))
#                degrees["-"+c[0]] = degrees.get("-"+c[0],0)+1
#                degrees["-"+c[1]] = degrees.get("-"+c[1],0)+1
                degrees[r1] = degrees.get(r1,0)+1
                degrees[r2] = degrees.get(r2,0)+1
#                print "\t".join(map(str,["-"+c[0],"-"+c[1],s10]))
            if s11>args.cutoff and ((not args.best) or s11==maxs ):
#                mapp.add_join(r1,l2,1000)
#                edges.append( ("-"+c[0],c[1],s11) )
                edges.append((r1,l2,s11))
#                degrees["-"+c[0]] = degrees.get("-"+c[0],0)+1
#                degrees[    c[1]] = degrees.get(    c[1],0)+1

                degrees[l2] = degrees.get(l2,0)+1
#                print "\t".join(map(str,["-"+c[0],c[1],s11]))

    for e in edges:
        if (degrees[e[0]]<=args.maxdegree) and (degrees[e[1]]<=args.maxdegree) and (e[0]<e[1]):
            print("\t".join(map(str,e+(degrees[e[0]],degrees[e[1]]))))
            mapp.add_join(e[0],e[1],1000)


    mapp.write_dot("testoo.dot")
    mapp.write_map_to_file(args.outmap)

#    for s in ss.keys():
##        if degrees.get(s,0)<=args.maxdegree:
#            print s,"-"+s,100

