#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from builtins import object
from past.utils import old_div
import sys
import string

tt = string.str.maketrans("ACTG","TGAC")

class fastaWriter(object):
    def __init__(self,filename,linelen=60):
        print(filename)
        self.f=open(filename,"w")
        print(self.f)
        self.linelen=linelen
        self.buff=""
        self.x=0
        self.name=""

    def write(self,s):
        self.buff+=s
        self.x+=len(s)
        print(len(self.buff),self.linelen)
        #        while len(self.buff)>self.linelen:
        wrtn=0
        for i in range( 0, self.linelen*(int(old_div(len(self.buff),self.linelen))) , self.linelen ):
            wrtn+=self.linelen
            self.f.write( self.buff[i:i+self.linelen]+"\n" )
#                print "#",self.buff[i:i+self.linelen]
#                sys.stdout.flush()
        if (len(self.buff)%self.linelen)==0:
            self.buff=""
        else:
            self.buff= self.buff[ -(len(self.buff)%self.linelen) :]
        print(len(self.buff),wrtn)
        self.f.flush()
        
    def flush(self):
#        while len(self.buff)>self.linelen:
        print(len(self.buff),self.linelen,"flush")
        wrtn=0
        for i in range( 0, self.linelen*(int(old_div(len(self.buff),self.linelen))) , self.linelen ):
            wrtn+=self.linelen
            self.f.write( self.buff[i:i+self.linelen]+"\n" )
        if len(self.buff)%self.linelen>0:
            wrtn+=(len(self.buff)%self.linelen)
            print("flush",self.buff[ -(len(self.buff)%self.linelen) :])
            self.f.write( self.buff[ -(len(self.buff)%self.linelen) :] +"\n")
        self.buff=""
        print(len(self.buff),wrtn)
        self.f.flush()
        
    def next(self,name):
        if self.x>0:
            print("#.",self.name,self.x)
        self.x=0
        self.flush()
        print(name)
        sys.stdout.flush()
        self.f.write(">{}\n".format(name))
        self.f.flush()
        self.name=name

    def close(self):
        self.f.close()

def rc(s):
    s = s.translate(tt)
    return s[::-1]

def slurp_fasta(x):
    seqs={}
    f=open(x)
    b=""
    name=""
    while True:
        l=f.readline()
        if not l: break
        if l[0]==">":
            if len(b)>0:
                seqs[name]=b
                b=""
            c=l[1:].strip().split()
            name=c[0]
        else:
            b=b+l.strip()
    seqs[name]=b
    f.close()
    return seqs

fastafile=sys.argv[1]
seqs=slurp_fasta(fastafile)
print("slurped fastafile")

outfasta=fastaWriter(sys.argv[2])
print("opened outfile")

name_prefix=""
if len(sys.argv)>3:
    name_prefix=sys.argv[3]
else:
    import random
    name_prefix="Scf"
    for i in range(5):
        name_prefix += random.choice( "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz" )

last_c=-1
last_x=0
last_s=0
cl=0

n=4

while True:
    l=sys.stdin.readline()
    if not l: break
#    print l.strip()
    c=l.strip().split("\t")

    focus = c[ 4*n+2 ]
    
    contigs = c[4*n+3:4*n+3+2*n+1 ]
    hits=c[4*n+3+2*n+1:]

    print(focus)
    print(contigs)
    print(hits)

    print(focus)
    for i in range(len(contigs)):
        if contigs[i] ==focus:
            outfasta.next( contigs[i] + " " + str( len(seqs[contigs[i]]) )+ " XX " + hits[i] )
        else:
            outfasta.next( contigs[i] + " "+ str( len(seqs[contigs[i]]) ) + " " +hits[i] )
        outfasta.write( seqs[contigs[i]] )

        



outfasta.close()
