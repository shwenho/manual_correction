#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from past.utils import old_div
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False)
parser.add_argument('-m','--maxlen',default=5000,type=float)

args = parser.parse_args()
if args.progress: print("#",args)

ll={}
G=0
f=open(args.length)
while True:
     l=f.readline()
     if not l: break
     c=l.strip().split()
     ll[c[1]]=int(c[0])
     G+=ll[c[1]]

def kmer_blocks(min_size):

     last_refC=False
     last_assC=False
     n=0
     xstart=0
     ystart=0

     while True:
          l=sys.stdin.readline()
          if not l: break
          kmer,refC,refp,refs,assC,assp,asss = l.strip().split()
          if refp=="None": continue
          #print (kmer,refC,refp,refs,assC,assp,asss)
          refp=int(refp)
          
          assp=int(assp)
     #     print kmer,refC,refp,refs,assC,assp,asss

          if last_refC and ((not last_refC==refC) or (last_assC != assC)):# and (last_assC==assC):

               xdelta=last_assp-xstart+len(kmer)
               ydelta=last_refp-ystart+len(kmer)

               if xdelta>=min_size:
                    yield( last_assC,last_refC,n,xdelta,ydelta,old_div(float(xdelta),ydelta),xstart,last_assp)
                    

               n=0
               xstart=assp
               ystart=refp

          n+=1
          last_refC=refC
          last_assC=assC
          last_assp=assp
          last_refp=refp

     xdelta=last_assp-xstart+len(kmer)
     ydelta=last_refp-ystart+len(kmer)

     if xdelta>=min_size:
          yield( last_assC,last_refC,n,xdelta,ydelta,old_div(float(xdelta),ydelta),xstart,last_assp)



last_refC=False
last_assC=False
n_misjoins=0
for assC,refC,n,xdelta,ydelta,r,x,y in kmer_blocks(args.maxlen):
     
     if last_assC and assC==last_assC:
          if not last_refC==refC:
               n_misjoins+=1
               print("\t".join(map(str,[n_misjoins,assC,ll.get(assC),refC,n,xdelta,ydelta,r,last_refC,last_assC,lasty])))

     last_refC=refC
     last_assC=assC
     lasty=y
#     leng=int(leng)
#     for i in range(1+leng/args.length):
#          print id,i*args.length
