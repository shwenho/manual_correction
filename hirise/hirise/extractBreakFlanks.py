#! /usr/bin/env python3
import argh
import sys
import os
import subprocess
import pysam
from operator import itemgetter
from collections import Counter,defaultdict


def read_breaks(break_fn):
    input_scafs = defaultdict(list) 
    fh = open(break_fn,"r")
    for line in fh:
        if line.startswith('#'): continue
        hr_scaf,in_scaf,in_start,in_end,strand,hr_start,hr_end = line.strip().split()
        input_scafs[in_scaf].append((hr_scaf,int(hr_start),int(hr_end),strand,int(in_start),int(in_end)))
    return input_scafs

def check_fasta_index(fn):
    index_path = fn + ".fai"
    if not os.path.isfile(index_path):
        print("No index file found for fasta file: %s\nCreating index." % (fn),file=sys.stderr)
        subprocess.check_call("samtools faidx %s" % (fn),shell=True)
    return

def get_lengths(fasta_fn):
    lengths_fn = fasta_fn + ".lengths"
    if not os.path.isfile(lengths_fn):
        print("Could not find .lengths file for the fasta file: %s\nMaking .lengths file.",file=sys.stderr)
        subprocess.check_call("fastalength %s > %s" % (fasta_fn,lengths_fn),shell=True)
    fh = open(lengths_fn,"r")
    lengths = {}
    for line in fh:
        length,scaf = line.split()
        length = int(length)
        lengths[scaf] = length
    return lengths
            
def get_region(begin,end,flank,upper,lower=0):
    left = lower if begin - flank < lower else begin - flank
    right = upper if end + flank > upper else end + flank
    return (left,right)

def dedupRegions(regions):
    found = {}
    dedup = []
    for region in regions:
        if region not in found:
            dedup.append(region)
        found[region] = 1
    return dedup


def write_viz_regions(viz_regions,fn):
    hr_id = lambda s: s.split("_")[1]
    fh = open(fn,"w")
    for (in_scaf,in_start,in_stop),hr_regions in viz_regions:
        parts = []
        for scaf,start,stop in hr_regions:
             parts.append("%s:%d-%d" % (hr_id(scaf),start,stop))
        hr_region_str = "-R "+ " -R ".join(parts)
        in_region_str = "-R %s:%d-%d" % (in_scaf,in_start,in_stop)
        print("%s\t%s"%(in_region_str,hr_region_str),file=fh)
    fh.close()
            
        
def write_aln_regions(aln_regions,fasta_fn,out_fn):
    check_fasta_index(fasta_fn)
    fasta = pysam.FastaFile(fasta_fn)
    out = open(out_fn,"w")
    for scaf,start,stop in aln_regions:
        scaf_id = "%s_%d_%d" % (scaf,start,stop)
        #write_fasta(scaf_id,fasta.fetch(scaf.split(".")[0],start,stop).decode('ascii'),out)
        write_fasta(scaf_id,fasta.fetch(scaf.split(".")[0],start,stop),out)
    fasta.close()
    out.close()  

def write_final_aln_regions(viz_regions,fasta_fn):
    check_fasta_index(fasta_fn)
    fasta = pysam.FastaFile(fasta_fn)
    for (in_scaf,in_start,in_stop),hr_regions in viz_regions:
        parts = []
        break_dir = get_dir(in_scaf)
        out_fn=os.path.join(break_dir,"final_break_regions.fasta")
        out = open(out_fn,"w")
        for scaf,start,stop in hr_regions:
            scaf_id = "%s_%d_%d" % (scaf,start,stop)
            write_fasta(scaf_id,fasta.fetch(scaf,start,stop),out)
        out.close()  
    fasta.close()

def write_fasta(header,seq,output,length=100):
    print(">%s" % (header),file=output)
    pos = 0
    seq_len = len(seq)
    while True:
        if pos + length >seq_len:
            print(seq[pos:],file=output)
            break
        print(seq[pos:pos+length],file=output)
        pos += length

def get_dir(query):
    if "." in query:
        query = query.split(".")[0]
    for root,dirnames,filenames in os.walk(os.getcwd()):
        for dirname in dirnames:
            if query in dirname: return dirname
    return "not found"

def scaffold_to_regions(scaf,scaf_breaks,flank_size,max_window,input_lengths,final_lengths):
    #breaks like: [hr_scaf,hr_start,hr_end,strand,in_start,in_end]
    first_break = scaf_breaks[0] 
    last_break = None
    prev_piece = scaf_breaks[0]
    curr_piece = None
    viz_regions = []
    viz_by_scaf = []
    aln_regions = []
    in_len  = input_lengths[scaf]
    #Run through pairs of input scaf regions to process each break
    for i in range(1,len(scaf_breaks)):
        curr_piece = scaf_breaks[i]
        prev_size = prev_piece[5] - prev_piece[4]
        curr_size = curr_piece[5] - curr_piece[4]
        if not viz_regions:
            left,right = get_region(prev_piece[2],prev_piece[2],max_window,final_lengths[prev_piece[0]])
            viz_regions.append((prev_piece[0],left,right))
        #get region of HiRise scaffolds to visualize
        left,right = get_region(curr_piece[1],curr_piece[1],max_window,final_lengths[curr_piece[0]])
        viz_regions.append((curr_piece[0],left,right))
        if curr_size > max_window:
            left,right = get_region(first_break[5],curr_piece[4],max_window,in_len)
            uniq_regions = dedupRegions(viz_regions)
            viz_by_scaf.append(((scaf,left,right),uniq_regions))
            left,right = get_region(first_break[5],curr_piece[4],flank_size,in_len)
            aln_regions.append((scaf,left,right))
            viz_regions = []
            first_break = curr_piece

        prev_piece = curr_piece
    if viz_regions:
        left,right = get_region(first_break[5],curr_piece[4],max_window,in_len)
        uniq_regions = dedupRegions(viz_regions)
        viz_by_scaf.append(((scaf,left,right),uniq_regions))

        left,right = get_region(first_break[5],curr_piece[4],flank_size,in_len)
        aln_regions.append((scaf,left,right))
    return viz_by_scaf,aln_regions


def scaffold_to_aln_regions(scaf,scaf_breaks,flank_size,input_lengths,final_lengths):
    #breaks like: [hr_scaf,hr_start,hr_end,strand,in_start,in_end]
    first_break = scaf_breaks[0] 
    last_break = None
    prev_piece = scaf_breaks[0]
    curr_piece = None
    in_len  = input_lengths[scaf]
    aln_regions = []
    get_id = lambda x: "%s.%d" % (scaf,x)
    prev_size = prev_piece[5] - prev_piece[4]
    left,right = get_region(prev_piece[5],prev_piece[5],flank_size,prev_piece[5])
    aln_regions.append((get_id(0),left,right))
    #Run through pairs of input scaf regions to process each break
    for i in range(1,len(scaf_breaks)):
        curr_piece = scaf_breaks[i]
        prev_size = prev_piece[5] - prev_piece[4]
        curr_size = curr_piece[5] - curr_piece[4]
        #only output left flank if piece is bigger than flank
        if prev_size  >= flank_size * 2:
            left,right = get_region(prev_piece[5],prev_piece[5],flank_size,prev_piece[5])
            aln_regions.append((get_id(i-1),left,right))
        left,right = get_region(curr_piece[4],curr_piece[4],flank_size,in_len,curr_piece[4])
        aln_regions.append((get_id(i),left,right))
    return aln_regions
        
        


def main(breaks_filename:"A .input_breaks.txt file.",
  final_assembly:"Specifies the FASTA file of the final HiRise assembly."=None,
  input_assembly:"Specifies the FASTA file of the input assembly."=None,
  flank_size:"Size of region on either side of a break to extract. Default 200kb."=200000,
  max_window:"For breaks less than 'max_window' apart on the same scaffold, only output one region. Defaults to 1 Mb"=1000000,
  min_length:"Only output for misjoins in scaffolds above this length."=100000,
  fasta_output:"Name of output FASTA file, defaults to 'breakFlanks.fasta'."="breakFlanks.fasta",
  viz_output:"Name of output file contains regions for visualization, one per line."="vizRegions.txt"):
    il = get_lengths(input_assembly)
    fl = get_lengths(final_assembly)
    breaks_by_scaf = read_breaks(breaks_filename)
    tot_aln_regions = []
    visualizations = []
    for scaf in sorted(breaks_by_scaf,key=lambda x: il[x],reverse=True):
        #aln_regions = scaffold_to_aln_regions(scaf,breaks_by_scaf[scaf],flank_size,il,fl)
        #tot_aln_regions.extend(aln_regions)
        #for in_region,hr_regions in scaffold_to_viz_regions(scaf,breaks_by_scaf[scaf],max_window,il,fl):
        #    visualizations.append((in_region,hr_regions))
        viz_regions,aln_regions = scaffold_to_regions(scaf,breaks_by_scaf[scaf],flank_size,max_window,il,fl)
        tot_aln_regions.extend(aln_regions)
        visualizations.extend(viz_regions)
    write_final_aln_regions(visualizations,final_assembly)
    write_aln_regions(tot_aln_regions,input_assembly,fasta_output)
    write_viz_regions(visualizations,viz_output)


if __name__ == "__main__":
    argh.dispatch_command(main)

