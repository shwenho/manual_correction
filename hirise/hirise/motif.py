#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
from builtins import object
#!/usr/bin/env python3

class logo(object):
    def __init__(self,right=False):
        self.counts = {}
        self.right=right
        self.maxi = 0
        self.total={}

    def add_seq(self,s):
        if len(s)>self.maxi:
            self.maxi = len(s)
        
        for i in range(len(s)):
            if self.right:
                ss = s[::-1]
                b = ss[i]
                self.counts[i,b] = self.counts.get((i,b),0)+1
                self.total[b] = self.total.get(b,0)+1
            else:
                b = s[i]
                self.counts[i,b] = self.counts.get((i,b),0)+1
                self.total[b] = self.total.get(b,0)+1

    def print_logo(self):
#        bases = self.total.keys()
#        bases.sort()
        bases = ["A","C","G","T"]
        print("\t".join([" "]+bases))
        for i in range(self.maxi):
            print("\t".join(map(str,[i]+[self.counts.get((i,b),0) for b in bases])))
    

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

#    parser.add_argument('--map')
#    parser.add_argument('--outmap')
    parser.add_argument('-r','--right',default=False,action="store_true")

    nodes={}

    args = parser.parse_args()
    lo = logo(args.right)

    while True:
        l = sys.stdin.readline()
        if not l: break
        if l[0]=="#": continue
        c = l.strip().split()
#        print c
        if len(c)==0:
            continue
        s = c[0]
#        print s
        lo.add_seq(s)
    lo.print_logo()
