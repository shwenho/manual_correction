#!/usr/bin/env python3


import argh
from hirise.hirise_assembly import HiriseAssembly
import sys

def main(hra_file, audit_merge, output="/dev/stdout", debug=False, ref=False):
    hra = load_assembly(hra_file, debug)
    present_contigs = set([contig for contig in hra.contigs_name_iter()])
    with open(output, 'w') as out:
        swap_orientation = {"+": "-", "-":"+"}
        for mer, ref_info, contig_info in parse_auditmers(audit_merge, ref):
            contig_name, contig_pos, contig_orientation = contig_info
            if contig_name not in present_contigs:
                continue
            scaffold_name, scaffold_pos, scaffold_orientation = hra.contig_to_scaffold_coord(contig_name, contig_pos)
            if scaffold_orientation == -1:
                new_orientation = swap_orientation[contig_orientation]
            else:
                new_orientation = contig_orientation
            ref_name, ref_pos, ref_o = ref_info
            if not ref:
                print(mer, ref_name, ref_pos, ref_o, "Sc_" + scaffold_name, scaffold_pos, new_orientation, sep="\t", file=out)

def parse_auditmers(audit_merge, ref):
    #TCATATCCTTCGCCCACTTGTTGATGGGGCTGTTTGTTTTTTTTGTTGTAAATTTGTTTGAGTCCATTGTAGATCCTGGATATTAGCCCTTTGTCAGATGA   4_maternal      135711998       -       ScsamLt_20946   57      +
    with open(audit_merge) as handle:
        for line in handle:
            s = line.split()
            if not ref:
                mer = s[0]
                ref_name = s[1]
                ref_pos = s[2]
                ref_o = s[3]
            
                #combined_contig_name = s[4]
                #contig_name = ''.join(combined_contig_name.split("_")[:-1])
            
                # subtract 1 because starts are 1 indexed
                #broken_contig_start = int(combined_contig_name.split("_")[-1]) -1
                #contig_pos = int(s[5])+broken_contig_start
                contig_name = s[4]
                contig_pos = int(s[5])
                contig_o = s[6]
            
            ref_info = (ref_name, ref_pos, ref_o)
            contig_info = (contig_name, contig_pos, contig_o)
            
            yield mer, ref_info, contig_info

def load_assembly(hra_file, debug):
    if debug:
        print("Load in hra",file=sys.stderr)
    hra = HiriseAssembly()
    hra.load_assembly(hra_file)

    if debug: 
        print("done loading in hra",file=sys.stderr)

    if len(hra.layout_lines)==0: 
        hra.make_trivial_layout_lines(debug=debug)
    return hra

if __name__ == "__main__":
    argh.dispatch_command(main)
