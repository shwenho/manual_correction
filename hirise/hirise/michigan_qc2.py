#!/usr/bin/env python3
#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import networkx as nx
import simulateMichigan as sm
from genomeUtils import build_chromosome_dict
import numpy as np
import math

def r2contig(r,l):
    return "%s.%d" % (r[4],int(old_div(float(r[0]),l)))


if __name__=="__main__":

    import sys
    import argparse
    import re
    parser = argparse.ArgumentParser(description="""
A script for learning the parameters of the Michigan library likelihood model from data.
""")
    parser.add_argument('-s','--simulate',default=False, action='store_true',help="Use simulated rather than real data.")
    parser.add_argument('-b','--binsize',default=100.0, help="Bin size in bp.",type=float)
    parser.add_argument('--Lmax',default=1.0e6, help="Maximum length for calculating g.",type=float)

    parser.add_argument('-c','--chromosomes',required=True,help="A file which lists the chromosomes in the genome and the length of each.")
    parser.add_argument('-d','--debug',default=False,action="store_true",help="Turn on debugging output.")
#parser.add_argument('-n','--nbreaks',default=100,type=int)
    parser.add_argument('-C',"--multiplicity",default=100,type=int,help="What's the mean number of aggregates per spot -- this gets used to calculate the total number of aggregates.")
    parser.add_argument('-m',"--microarraySpots",default=90000,type=int,help="How many spots are on the array?")
    parser.add_argument('-L','--length',default=150000,type=int,help="What's the mean length of the DNA in chromatin aggregates?")
    parser.add_argument('-u','--lengthUnc',default=15000,type=int,help="What's the standard deviation of the length of the DNA in chromatin aggregates?")
    parser.add_argument('-N','--nreads',default=500000000,type=int,help="How many reads (assuming forward reads only) get sampled?")
    parser.add_argument('-f','--noiseFraction',default=0.25,type=float,help="What fraction of the reads are noise, in the sense that they don't come from a 'set' from get ligated independently to a random spot.")
    parser.add_argument('-r','--readLength',default=100 ,type=int,help="ignored for now.")
    parser.add_argument('-H','--head',default=False ,type=int,help="Don't simulate a whole genome's worth of data, just sample reads from the first HEAD bases of chr1.")

    parser.add_argument('-l','--contigSize',default=5000,type=int,help="Chop the chromosomes into 'contigs' of this length")
    parser.add_argument('-M','--minSetAnchor',default=2,type=int,help="The threshhold number of reads to 'anchor' a contig to a set (spot)")
    parser.add_argument('-S','--minSpots',default=2,type=int,help="The threshhold number of shared anchored spots to infer scaffolding links between contigs.")
    parser.add_argument('--hilight',default=False,type=int,help="Hilight the edges between contigs that are HILIGHT steps away along the chromosome.")
    parser.add_argument('--colorCode',default=False,type=str,help="Display matrix in terminal color codes.")

    i=0
    zeros={'k':'000','K':'000','M':'000000',"G":'000000000'}
    for i in range(1,len(sys.argv)):
        arg = sys.argv[i]
#        print arg,"."
        m=re.match("\d+([MGKk])",arg)
        if m:
            arg = re.sub(m.group(1),zeros[m.group(1)],arg)
#            print arg,m.group(0)
            sys.argv[i]=arg
    main_args = parser.parse_args(sys.argv[1:])

#    print main_args

    if True or main_args.debug:
        print(main_args)

    chromLen=build_chromosome_dict(main_args.chromosomes)
    
    G=nx.Graph()
    
    if main_args.simulate:
        data = sm.Michigan_data_generator(main_args,chromLen)
    else:
        print("Reading from a real bam file not yet implemented.")
        exit(0)



    binsize=main_args.binsize
    L_max  =main_args.Lmax
    nbins = 1+int(old_div(L_max, binsize))
    g = np.zeros(nbins)

    print(data.params_blurb())

    buff = []
    qs={}
#    g = 
#(228016, 11124, 0, 42843, 'chr1')

    #def tally(g,dx):

    n=0
    for r in data.generate_reads():
        n+=1
        chrom = r[4]
        x = r[0]
        spot = r[3]
        print("\t".join(map(str,[r, len(list(qs.keys())),sum([ len(qs[i]) for i in list(qs.keys())])])))
        if spot not in qs:
            qs[spot]=[]
        while (len(qs[spot])>0) and ((qs[spot][0][4]!=chrom) or (x-qs[spot][0][0])>L_max):
            qs[spot].pop(0)
        for rr in qs[spot]:
            dx = r[0]-rr[0]
            g[int(old_div(dx,binsize))]+=1

        qs[spot].append(r)

    
    print("n reads: ",n)
    for i in range(nbins):
        print("g",i,i*binsize,g[i],old_div(float(g[i]),n))
#        while buff[0][]
        
