#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import str
#!/usr/bin/env python3
import sys
import hirise.mapper
import pickle as pickle
import argparse

def log(s):
    sys.stderr.write(  "%s\n" %(s) )

parser = argparse.ArgumentParser()
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('--map_file',default=False)
parser.add_argument('--refmap',default=False)

args = parser.parse_args()
if args.debug:
    args.progress=True

if args.progress: log( str(args) )

mapp = pickle.load(open(args.map_file))
rmap = pickle.load(open(args.refmap))

lengths={}
pairs={}
name2pair={}

if args.debug: mapp.debug=True
if args.debug: rmap.debug=True

while True:
    l = sys.stdin.readline()
    if not l: break

#    print l.strip()

    if l[0]=='#': continue
    c = l.strip().split()
    if len(c)==0: 
        print("#XXXX:",l.strip())
        continue
    s1 = c[0]
    s2 = c[1]
    l1 = int(c[2])
    l2 = int(c[3])
    n = int(c[4])
    v = eval(" ".join(c[5:]))

    lengths[s1]=l1
    lengths[s2]=l2

#    print "#", l.strip()

#    if not name_to_pair.has_key(s1):
#        name_to_pair[s1]=mapp.

    # rmap contains a representation of the scaffolds that were fed into 
    # the edge calculator.  

    s1r   = rmap.find_node_by_name(s1)
    s2r   = rmap.find_node_by_name(s2)

#    print s1,s1r,s2,s2r
    
#    if args.debug: print s1r,s1,s1r.root,s1r.leaf
#    s1l  = s1r.leaf
#    s2l  = s2r.leaf

#    r1  = mapp.find_node_by_pair(s1r.s,s1r.x)
#    r2  = mapp.find_node_by_pair(s2r.s,s2r.x)

#    l1 = r1.leaf.mapped_coord
#    l2 = r2.leaf.mapped_coord

    if args.debug: print(s1,s2,s1r,s2r, l)

    for x,y in v:
 
        ori1 = rmap.find_original_coord(s1r,x)
        ori2 = rmap.find_original_coord(s2r,y)

        if args.debug: print("xy",x,y,ori1,ori2)

        mapped1 = mapp.map_coord(ori1[0],ori1[1])
        if args.debug: print("mapped1", mapped1)
        

        mapped2 = mapp.map_coord(ori2[0],ori2[1])
        if args.debug: print("mapped2", mapped2)

        mapped_name1 = mapp.print_name(mapped1[0])
        mapped_name2 = mapp.print_name(mapped2[0])

        lengths[mapped_name1]=mapp.length[mapped1[0]]
        lengths[mapped_name2]=mapp.length[mapped2[0]]
        
#        print "x:",ori1, mapp.map_coord(ori1[0],ori1[1]), ori2, mapp.map_coord(ori2[0],ori2[1]),mapped_name1,mapped_name2,lengths[mapped_name1],lengths[mapped_name2]

        if mapped_name1 < mapped_name2:
            pairs[mapped_name1,mapped_name2] = pairs.get((mapped_name1,mapped_name2),[]) + [ (mapped1[1],mapped2[1]) ]
#        else:
#            pairs[mapped_name2,mapped_name1] = pairs.get((mapped_name2,mapped_name1),[]) + [ (mapped2[1],mapped1[1]) ]

        
        
  #        print ori2, mapp.map_coord(ori2[0],ori2[1])
#        a,b=mapp.map_coord_by_name(s1,x)
#        c,d=mapp.map_coord_by_name(s2,y)
#        print s1,s2,x,y,[a,mapp.print_name(a),b],[c,mapp.print_name(c),d] #,mapp.print_name(a),mapp.print_name(c)
#        if a<=c:
#            pairs[( a,c )] = pairs.get( (a,c),[]) + [ (b,d) ] 
#        else:
#            pairs[( c,a )] = pairs.get( (c,a),[]) + [ (d,b) ] 

            
#for s1 in 

pairids = list(pairs.keys())
pairids.sort()

for p in pairids:
    print(p[0],p[1],lengths[p[0]],lengths[p[1]],len(pairs[p]),pairs[p])


