#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from past.utils import old_div

import sys
import re
"""
			if (/'(.*)'=='([\+-])(.*)' \((\S+) (\S+) (\S+) (\S+)\) (\S+)/) {
				($T,$strand,$Q,$tstart,$qstart,$tstop,$qstop,$misses) = ($1,$2,$3,$4,$5,$6,$7,$8);
				$len = abs($qstop-$qstart);
				$pid = $len ? ($len - $misses + 1) / $len : 0 ; 
				$score = $len - $misses;
				$hits = $len - $misses;
"""

n=0
while True:
    l = sys.stdin.readline()

    if not l: break
    n+=1
    m = re.match("'(.*)'=='([\+-])(.*)' \((\S+) (\S+) (\S+) (\S+)\) (\S+)",l)
    if m:
        T,strand,Q = m.group(1),m.group(2),m.group(3)
        tstart,qstart,tstop,qstop,misses = list(map(int,[m.group(4),m.group(5),m.group(6),m.group(7),m.group(8)]))
        leng = abs(qstop-qstart)                                                                                                                                                                     
        pid = old_div(float(leng - misses + 1), leng)
        score = leng - misses
        hits = leng - misses
        
#        print T,strand,Q,tstart,qstart,tstop,qstop,misses,score,pid,leng
        if pid > 0.98 and leng > 1000:
            print(l.strip(),"#",pid,leng,n)
