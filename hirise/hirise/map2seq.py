#!/usr/bin/env python3
from __future__ import print_function
from builtins import range
#!/usr/bin/env python3
import hirise.seq_getter as seq_getter
#import hirise.mapper
import sys
import pickle


fastaname = sys.argv[1]
mapp = pickle.load(open(sys.argv[2],"rb"))

name = sys.argv[2]
getter = seq_getter.seqs(fastaname)
#s=getter.get(name)

#print name,s

for r in mapp.roots:
#    print r
    last=""
    st=[]
    for n in mapp.nodes_dfs(r):
#        print "#:",n,n.mapped_coord,n.links,n.root,n.leaf
        if n.s == last:
            na = mapp.originals_names[n.s]
            s = getter.get_range(na,lastx,n.x)
            st.append(s)
#            print st
        last = n.s
        lastx = n.x
    g = "N"*1000
    s=g.join(st)
    print(">"+mapp.print_name(r), len(s))
    for i in range(0,len(s),60):
        print(s[i:i+60])
