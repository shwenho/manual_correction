#!/usr/bin/env python3

import argh


def main(audit="/dev/stdin", output="/dev/stdout"):
    with open(output, 'w') as out:
        with open(audit) as handle:
            prev_mer = ""
            is_unique = False
            prev_line = ""
            for line in handle:
                mer = line.split()[0]
                if mer == prev_mer:
                    is_unique = False
                else:
                    if is_unique:
                        print(prev_line, end="", file=out)
                    prev_mer = mer
                    is_unique = True
                    prev_line = line

if __name__ == "__main__":
    argh.dispatch_command(main)
