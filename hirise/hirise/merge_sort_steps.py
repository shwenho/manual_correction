#!/usr/bin/env python3
from __future__ import print_function
from builtins import map
from builtins import range
#!/usr/bin/env python3
import sys
import argparse

if __name__=="__main__":

    parser = argparse.ArgumentParser()


    args = parser.parse_args()
    print(args)

    last1=False
    last2=False
    lasts=0
    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=="#": 
            nd=len(l.strip().split())-2
            print("nd:",nd)
            last1 = (" ",0,[0]*nd)
            last2 = (" ",0,[0]*nd)
            continue
        if l.find("mapped list:")==0: continue
        if l.find("break:")==0: continue
        c=l.strip().split()
        s=c[0]
        x=int(c[1])
        counts=list(map(int,c[2:2+nd]))
        if c[-1]=="1":
            last1 = (s,x,counts)
        if c[-1]=="2":
            last2 = (s,x,counts)
        if last1[0]==last1[0]:
            if last1[1]>last2[1]:
                print("\t".join(map(str,[last1[0],last1[1]]+[ last1[2][i]+last2[2][i] for i in range(nd) ])))
                lasts=last1[0]
            else:
                print("\t".join(map(str,[last2[0],last2[1]]+[ last1[2][i]+last2[2][i] for i in range(nd) ])))
                lasts=last2[0]
        else:
            if last1[0]==lasts:
                print("\t".join(map(str,[last1[0],last1[1]]+[ last1[2][i]+last2[2][i] for i in range(nd) ])))
                lasts=last1[0]

            elif last2[0]==lasts:
                print("\t".join(map(str,[last2[0],last2[1]]+[ last1[2][i]+last2[2][i] for i in range(nd) ])))
                lasts=last2[0]
                


