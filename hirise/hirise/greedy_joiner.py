#!/usr/bin/env python3
from __future__ import print_function
from builtins import str
from builtins import map
#!/usr/bin/env python3
import sys
import networkx as nx
import hirise.chicago_edge_scores as ces
import greedy_chicagoan as gs

def update_end_distance(end_distance,n,g):
    x=0
    q=[n]
    seen={}
    last=False
    while len(q)>0:
        m=q.pop(0)
        if last:
            try:
                x+=g[last][m]['length']
            except Exception as e:
                print("wtf?",last,m,x,n)
                print(math.log(-1.0))
        last=m
        if (not m in end_distance) or end_distance[m]>x: end_distance[m]=x
        
        seen[m]=True
        if len(g.neighbors(m))>2:
            print("topology error:",m,list(g.neighbors(m)))
            print(math.log(-1.0))
        for l in g.neighbors(m):
            if not l in seen:
                q.append(l)

    return x


def is_shaved_tail(G,shave_round,shaved_degree,shave_limit):
    leaves=[]
    r={}
    for n in G.nodes():
        if shave_round.get(n,0)>shave_limit and shaved_degree.get(n,0)==1:
            leaves.append(n)
    for l in leaves:
        q=[l]
        while len(q)>0:
            n=q.pop()
            r[n]=True
            for nn in G.neighbors(n):
                if shave_round.get(nn,0)>shave_limit and shaved_degree.get(nn,0)<=2 and (not nn in q) and (not nn in r ):
                    q.append(nn)
    return r


def distance_to_nearest_branch(G,shave_round,shave_limit,trim_degree):
    branchpoints=[]
    r={}

    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if shave_round.get(n,0)>shave_limit and trim_degree.get(n,0)>2:
            branchpoints.append(n)
#    print leaves

    round=1
#    print "#",leaves
    boundary=list(branchpoints)
    next=[]
    done=[]
    #r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next) :
                    next.append(nn)
#                    r[nn]=round
        for b in next: 
            r[b] = round
#            if len( [nn for nn in G.neighbors(b) if not r.has_key(nn)] )==1: r[b]=round
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if n in r: boundary.append(n)
#        next=[]

#    for n in G.nodes():
#        if not r.has_key(n): r[n]=round
    return r


def distance_to_nearest_leaf(G,shave_round,shave_limit,trim_degree):
    leaves=[]
    r={}
    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if shave_round.get(n,0)<=shave_limit :
            r[n]=0
    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if trim_degree.get(n,0)==1 and shave_round.get(n,0)>shave_limit:
            leaves.append(n)
#    print leaves

    round=1
#    print "#",leaves
    boundary=list(leaves)
    next=[]
    done=[]
#    r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next):
                    next.append(nn)
#                    r[nn]=round
        for b in next: 
            r[b] = round
#            if len( [nn for nn in G.neighbors(b) if not r.has_key(nn)] )==1: r[b]=round
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if n in r: boundary.append(n)
#        next=[]
#    for n in G.nodes():
#        if not r.has_key(n): r[n]=round
    return r
    

def shave_round(G):
    leaves=[]
    for n in G.nodes():
        #print "#",n, G.degree(n),G.degree(n)==1
        if G.degree(n)==1:
            leaves.append(n)
#    print leaves

    round=1
#    print "#",leaves
    boundary=list(leaves)
    next=[]
    done=[]
    r={}
    for b in boundary: r[b]=round
    while len(boundary)>0:
        round +=1
        next=[]
        for n in boundary:
            for nn in G.neighbors(n):
                if (not nn in boundary+done+next):
                    next.append(nn)
#                    r[nn]=round
        nr={}
        for b in next: 
            if len( [nn for nn in G.neighbors(b) if nn not in r] )==1: nr[b]=round
        r.update(nr)
        for b in boundary: done.append(b)
        boundary = []
        for n in next: 
            if n in r: boundary.append(n)
#        next=[]
    for n in G.nodes():
        if n not in r: r[n]=round
    return r

def log(x):
    sys.stderr.write(x+"\n")


LOCAL_BRIDGE=1
CUT_ME=2

edge_tags={}

def add_tag(s1,s2,tag):
    if s1<s2:
        ot = edge_tags.get((s1,s2),set())
        ot.add(tag)
        edge_tags[s1,s2] = ot
    else:
        ot = edge_tags.get((s2,s1),set())
        ot.add(tag)
        edge_tags[s2,s1] = ot


def get_tags(s1,s2):
    if s1<s2:
        ot = edge_tags.get((s1,s2),set())
        return tuple(ot)
    else:
        ot = edge_tags.get((s2,s1),set())
        return tuple(ot)


edge_color_setting="hair"
def edge_tag_to_style(tags,setting=edge_color_setting):
    if setting == "hair":
        style=""
        if "hair" in tags:
            style= "color=red"
        elif "longHair" in tags:
            style= "color=orange"
        elif "H" in tags:
            style= "color=blue"
        elif "Y" in tags:
            style= "color=goldenrod"
        elif "nearY" in tags:
            style= "color=goldenrod4"
        elif "bigH" in tags:
            style= "color=green"
        if "promisc" in tags:
            style += " style=dashed"
        return style
            

def independent_path(G,a,b,k,t):
    q=[a]
    l={}
    l[a]=0
    r=[]
    while len(q)>0:
#        print a,b,G[a][b],q,r
        n=q.pop(0)
        r.append(n)
        for nn in G.neighbors(n):
            if (n==a and nn==b) or (n==b and nn==a): continue
            if G[n][nn]['weight']>-t: continue
            if nn==b: return True
#            print q,[l[i] for i in q]
            l[nn] = min(l.get(nn,10000), l[n]+1)
            if (not nn in q+r) and (l[nn]<=k):
                q.append(nn)
    return False
    
def annotate_edges(t,G,node_list):
    an={}
#    nn= len(list(t.edges()))
#    i=0.0
    for a,b in t.edges():

        if not independent_path(G,a,b,4,2): 
            an[a,b]="local_bridge"
            an[b,a]="local_bridge"
            
    return an


def pairs_overlap(x,y):
    a=min(x[0],x[1])
    b=max(x[0],x[1])
    c=min(y[0],y[1])
    d=max(y[0],y[1])
        
    if a<=c and c<=b: return True
    if a<=d and d<=b: return True
    if c<=a and a<=d: return True
    if c<=b and b<=d: return True
    return False

def qdist(x,y):
    if (not x) or (not y): return (-1)
    if x[0]==y[0]:
        x,y,w,z = list(map(int,[x[2],x[3],y[2],y[3]]))
        ol = pairs_overlap((x,y),(w,z))
        if ol:
            return(-1*min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
        else:
            return(min(
                abs(x-w),
                abs(x-z),
                abs(y-w),
                abs(y-z )))
    else:
        return(1.0e12)

def update_scaffold_coords(coord_sys,n,g,sn):
    x=0
    q=[n]
    seen={}
    cstrand={}
    last=False
    while len(q)>0:
        m=q.pop(0)
        contig=m[:-2]
        if last:
            try:
                x+=g[last][m]['length']
            except Exception as e:
                print("wtf?",last,m,x,n)
                print(math.log(-1.0))
        last=m

        #if (not m in end_distance) or end_distance[m]>x: end_distance[m]=x
        strand=1
        if not contig in seen:
            if m[-1:]=="5":
                strand=1
                cstrand[contig]=1
            elif m[-1:]=="3":
                strand=-1
                cstrand[contig]=-1
            else:
                print("#wtf?")
                raise Exception('not plus or minus?')
        coord_sys[m]=x
        
        if contig in seen:
#            coord_sys[contig]={ "strand":strand, "start":min( coord_sys[contig+".5"], coord_sys[contig+".3"] )  , "stop":max( coord_sys[contig+".5"], coord_sys[contig+".3"] ) }
            coord_sys[contig]=[coord_sys[contig+".5"],cstrand[contig],sn]
#            print "layout:",m,contig,seen.get(contig),coord_sys[contig]

        seen[m]=True
        seen[contig]=True
        if len(g.neighbors(m))>2:
            print("topology error:",m,list(g.neighbors(m)))
            print(math.log(-1.0))
        for l in g.neighbors(m):
            if not l in seen:
                q.append(l)

def test_interc(c1,c2,join_options,graph,ends,linked):
    #os1=oscaffold[c1]
    #os1_mine = (args.fromS <= os1) and (os1 < args.endS)
    #if not os1_mine: return([-100])

    for free_end in ends[scaffold[c2]]:
        for c1_end in [c1+".5",c1+".3"]:
            if c1_end in linked:
                try:
                    gs.test_interc_option( c1_end, linked[c1_end], free_end, join_options, graph )
                except Exception as e:
                    print("c1_end=",c1_end,"--",linked[c1_end],free_end)
                    print(e)
                    raise Exception('too connected 2')

    if len(join_options)>0:
        join_options.sort(reverse=True)
        #            print join_options
        return(join_options[0])
    return([-100.0])

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-t','--threshold',default=0.0 ,  type=float)
    parser.add_argument('-z','--mergeT',default=3.0 ,  type=float)

    parser.add_argument('-H','--head',default=False,type=int)
    parser.add_argument('-D','--savetreedots',default=False,action='store_true')
    parser.add_argument('-d','--debug',default=False,action='store_true')
#    parser.add_argument('-I','--nointerc',default=False,action='store_true')
    parser.add_argument('-p','--progress',default=False,action='store_true')
    parser.add_argument('-M','--maxdegree',default=False,type=int)
    parser.add_argument('-m','--minlength',default=500,type=int)

    parser.add_argument('-S','--silent',default=False,action='store_true')
    parser.add_argument('-K','--cutPromisc',default=False,action='store_true')
    parser.add_argument('-J','--logH',default=False,action='store_true')
    parser.add_argument('-T','--logTags',default=False,action='store_true')
    parser.add_argument('-C','--cheat',default=False,action='store_true')
    parser.add_argument('-B','--blacklist')

    parser.add_argument('-b','--besthits')
    parser.add_argument('-l','--lengths')
    parser.add_argument('-E','--edgefile')
    parser.add_argument('-L','--maxLength',type=float,default=150000.0)
    parser.add_argument('-P','--promisc',type=float,default=0.023)
    parser.add_argument('-o','--dotLabel',default="bad")

    args = parser.parse_args()
    if args.debug:
        args.progress=True

    if args.progress: log( str(args) )
    print("#"+str(args))

    G=nx.Graph()
    SG=nx.Graph()
    OG=nx.Graph()
    linked={}

    # 1 Scaffold29756_1.5 Scaffold91187_1.3 1000.0 200936.0
    f=open(args.edgefile)
    while True:
        l=f.readline()
        if not l: break
        if l[0]=="#":
            c=l.strip().split()
            if c[2][:-2]==c[3][:-2]:
                OG.add_edge(c[2],c[3],length=float(c[4]),contig=True)
            else:
                OG.add_edge(c[2],c[3],length=float(c[4]),contig=False)
                linked[c[2]]=c[3]
                linked[c[3]]=c[2]
    f.close()

    sc=1
    scaffold={}
    slen={}
    for c in nx.connected_components(OG):
        for cc in c:
            scaffold[cc]=sc
            scaffold[cc[:-2]]=sc
        tl=0
        for e1,e2 in nx.dfs_edges(OG,c[0]):
            tl+=OG[e1][e2]['length']
        slen[sc]=tl
        sc+=1

    ends={}
    for n in OG.nodes():
        if OG.degree(n)==1:# and n in oscaffold and oscaffold[n]<args.endS:
#            update_end_distance(end_distance,n,g)
            ends[scaffold[n]] = ends.get(scaffold[n],[])+[n]
#x            print n,scaffold[n],ends[scaffold[n]]

    coord_sys={}
    sn=1
    for sg in nx.connected_components(OG):
        n=sg[0]
        if len(ends[scaffold[n]])==0: 
            print("why no ends?", sn) 
            continue
        update_scaffold_coords(coord_sys,ends[scaffold[n]][0],OG,sn)
        sn+=1

    ll={}
    if args.lengths:
        f = open(args.lengths)
        while True:
            l = f.readline()
            if not l: break
            if l[0]=="#": continue

            c=l.strip().split()
            l = int(c[1])
            ll[c[0]]=int(c[1])
            if l>= args.minlength:
                G.add_node(c[0])
                SG.add_node(c[0])
        f.close()
    if args.progress: print("#Done reading lengths")

    besthit={}
    if args.besthits:
#        besthit={}
        if args.besthits:
            f = open(args.besthits)
            while True:
                l = f.readline()
                if not l: break

                if not l[:5]=="best:": continue
                c=l.strip().split()
                besthit[c[1]]=c[2:]
    #            print c[1],besthit[c[1]]
            f.close()
    if args.progress: print("#Done reading besthits")

    links={}
    join_candidates=[]
    if True:
        f=sys.stdin #open(args.links)
        while True:
            l = f.readline()
            if not l: break
            if not l[0]=="(": continue
            c = eval(l.strip())
            join_candidates.append(c)
  
        f.close()


    join_candidates.sort(reverse=True)
    for j in join_candidates:
        print(j)
        print(j[0],j[1],j[2])
        to_be_cut={}
        for e1,e2 in j[2]:
            print("x",e1,e2,e1 in linked,e2 in linked)
            to_be_cut[e1]=1
            to_be_cut[e2]=1
        already_joined=False
        for e1,e2 in j[1]:
            print("x",e1,e2,e1 in linked,e2 in linked)
            if not e1 in to_be_cut and e1 in linked: already_joined = True
            if not e2 in to_be_cut and e2 in linked: already_joined = True
        for e1,e2 in j[2]:
            if not OG.has_edge(e1,e2):
                already_joined=True
        
        print("#already joined:",already_joined,j[0])
        if (not already_joined) and (j[0]>args.threshold):
            pass

            for x,y in j[2]:
                OG.remove_edge(x,y)
                del linked[x]
                del linked[y]
            for x,y in j[1]:
                linked[x]=y
                linked[y]=x
                OG.add_edge(x,y,length=1000.0,contig=False)
                print("add",x,y)
                #edge_options[x,y] = "[color=brown label=\"{:.2f}\"]".format(weights.get((x,y),0))
                #edge_options[y,x] = "[color=brown label=\"{:.2f}\"]".format(weights.get((x,y),0))
                    
    for s in nx.connected_component_subgraphs(OG):
        l=0
        for a,b in s.edges():
            l+=s[a][b]["length"]
        print("l:",l)

    end_distance={}
    sn=1
    for sg in nx.connected_component_subgraphs(OG):
        ends=[]
        bh_stats={}
        for n in sg.nodes():
            if sg.degree(n)==1:
                ends.append(n)

        if len(ends)==0: 
            print("why no ends?", sn) 
            sn+=1
            continue
        maxx=update_end_distance(end_distance,ends[0],sg)

# ['34329.0', '3', '+', '71834554', '71853152', '1', '1', '18598']

        t=0
        gap_len=0
        for s1,s2 in sg.edges():
            t+=sg[s1][s2]['length']
            if not sg[s1][s2]['contig']: gap_len += sg[s1][s2]['length']
            print("#",sn,s1,s2,sg[s1][s2]['length'],t)
        print(t,n,"slen",gap_len,t-gap_len)

        node_tlen=0
        nodes_list = list(sg.nodes())
        nodes_list.sort(key=lambda x: end_distance[x])
        for n in nodes_list:
            base_name,end_id=n.split('.')
            if end_id=="5": node_tlen+= ll[base_name]
            bh = besthit.get(n[:-2],False)
            x=-1
            chr="-"
            if bh:
                chr=bh[1]
                if bh[2]=="+":
                    if n[-1:]=="5":
                        x=int(bh[3])
                    else:
                        x=int(bh[4])
                if bh[2]=="-":
                    if n[-1:]=="5":
                        x=int(bh[4])
                    else:
                        x=int(bh[3])
            
            print("p:",sn,n,end_distance[n],chr,x,t,ll.get(n[:-2]),bh)
        print(node_tlen,"node_tlen")

        sn+=1

