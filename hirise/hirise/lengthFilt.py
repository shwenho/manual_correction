#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import print_function
import sys
import argparse

parser = argparse.ArgumentParser()

#parser.add_argument('-i','--input')
parser.add_argument('-d','--debug',default=False,action="store_true")
parser.add_argument('-p','--progress',default=False,action="store_true")
parser.add_argument('-L','--length',default=False)
parser.add_argument('-K','--cut',default=1000,type=int)

args = parser.parse_args()
if args.progress: print("#",args)

f=open(args.length)
ll={}
while True:
     l=f.readline()
     if not l: break
     leng,id = l.strip().split()
     leng=int(leng)
     ll[id]=leng
f.close()

on=True
while True:
     l=sys.stdin.readline()
     if not l: break
     if l[0]==">":
          name = l[1:].strip().split()[0]
#          print name,ll.get(name)
          if ll.get(name,0)<args.cut:
               on=False
          else:
               on=True
     if on:
          print(l.strip())
