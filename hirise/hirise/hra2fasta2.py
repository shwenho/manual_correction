#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
from hirise.hirise_assembly import HiriseAssembly
from hirise.p2fa import fastaWriter

tr = str.maketrans("ACTGactg","TGACtgac")


def rc(s):
    r=s[::-1]
    rc = r.translate(tr)
    return(rc)



if __name__=="__main__":
     import sys
     import argparse

     parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('-d','--debug',default=False  ,action="store_true",help="Turn on debugging ouput")
     parser.add_argument('-L','--layout',default=False ,help="A file containing a layout of contigs.")
     parser.add_argument('-i','--infile',default=False ,help="Filename for serialised assembly input file.")
     parser.add_argument('-f','--fasta', default=False ,help="Filename for original contigs fasta.")
     parser.add_argument('-o','--outfile',default=False,help="Filename for output fasta.")
     parser.add_argument("-t", "--table", default=False, help="Filename for output table.")
     args = parser.parse_args()

     if args.infile:
          asf = HiriseAssembly()
          asf.load_assembly(args.infile)

     asf.ocontig_fasta = args.fasta

     gapseq = "N"*100
     
     if args.outfile:

          outfasta=fastaWriter(args.outfile)
          outtable=open(args.table, 'w')
          for scaffold,contig_list in asf.scaffold_lists():
#               print("Sc_"+str(scaffold),contig_list)
               this_sum = 0
               if len(contig_list)>0 :
                    outfasta.next("Sc_"+str(scaffold))
                    seq = []
                    for contig,strand,a,b in contig_list:
                         cseq = asf.get_seq( "_".join(map(str,contig)) )
                         if (not strand=="+") :
                              cseq = rc(cseq)
#                         print(scaffold,contig,strand,a,b,b-a,len(cseq),cseq[:100])
                         seq.append(cseq)
                         c,x,y = asf.contig_to_contig_pos("_".join(map(str,contig)))
                         y = y-1
                         orig_sum = this_sum
                         this_sum += y - x
                         
                         print("Sc_" + str(scaffold), c, x, y, strand, orig_sum, this_sum, sep="\t", file=outtable)
                         this_sum += 100
                    outfasta.write(gapseq.join(seq))

          outfasta.flush()
          outfasta.close()


