#!/usr/bin/env python3
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import range
#!/usr/bin/env python3
import pysam
import sys
import math
import networkx as nx
import hirise.mapper
import pickle as pickle

def log(s):
    print(s)

#['Scaffold1', 'CONTIG1', '+isotig2133903', '1', '105', '16.5636363636364']
#['Scaffold1', 'GAP1', '57', '14']
#['Scaffold1', 'CONTIG2', '+isotig1165818', '163', '348', '33.0517647058824']
#['Scaffold1', 'GAP2', '-22', '1']
#['Scaffold1', 'CONTIG3', '+isotig1090484', '327', '475', '19.6567676767677']
#['Scaffold1', 'GAP3', '31', '11']
#['Scaffold1', 'CONTIG4', '+isotig1523405', '507', '676', '38.6750833333333']
#['Scaffold2', 'CONTIG1', '+isotig2311227', '1', '158', '46.1021296296296']
#['Scaffold2', 'GAP1', '169', '5']
#['Scaffold2', 'CONTIG2', '-isotig2268088', '328', '476', '38.3435353535354']

if __name__=="__main__":

    import sys
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--map',required=True)
    parser.add_argument('--outmap',required=True)
    parser.add_argument('-d','--debug',default=False,action="store_true")

    nodes={}

    args = parser.parse_args()

    rl = sys.getrecursionlimit()
    print(rl)

    rl = sys.setrecursionlimit(50000)

#    if args.progress: log( str(args) )

    mapp = pickle.load( open(args.map)    )

    for r in mapp.roots:
        print("root",r)

    for n in mapp.node:
        m=mapp.node[n]
        print("not",n,mapp.print_name(n),mapp.print_name(m),m.root,mapp.print_name(m.root),m.root in mapp.roots)

    ls=False
    ss=-1
    mapp.obsoleted={}

    scaffold_lines={}
#
#p: 1 Scaffold241044_1.5 0 - -1 29785290.0 1991 False
#p: 1 Scaffold241044_1.3 1991 - -1 29785290.0 1991 False
#p: 1 Scaffold34288_1.5 2991.0 - -1 29785290.0 72848 False
#p: 1 Scaffold34288_1.3 75839.0 - -1 29785290.0 72848 False
#p: 1 Scaffold19727_1.5 76839.0 - -1 29785290.0 17607 False
#

    while True:
        l=sys.stdin.readline()
        if not l: break
        if l[0]=='#': continue
        c = l.strip().split()
        if not c[0]=="p:": continue
        scaffold=int(c[1])
        scaffold_lines[scaffold] = scaffold_lines.get(scaffold,[])+[c]

    

    for scaffold in sorted(scaffold_lines.keys()):
#        if len(scaffold_lines[scaffold])<3: continue
#        print "#",scaffold, len(scaffold_lines[scaffold])
        print("nroots1:",len(list(mapp.roots.keys())),scaffold,len(scaffold_lines[scaffold]))

        contig = scaffold_lines[scaffold][0][2]
        rootcontig=mapp.find_node_by_name(contig)
        if rootcontig in mapp.roots: continue
        del mapp.roots[rootcontig.root]
        mapp.roots[rootcontig]=1
        print(mapp.print_name(rootcontig), rootcontig in mapp.roots)
        mapp.reroot(rootcontig)

    for scaffold in sorted(scaffold_lines.keys()):
#        print "#",scaffold, len(scaffold_lines[scaffold])

        print("nroots2:",len(list(mapp.roots.keys())),scaffold,len(scaffold_lines[scaffold]))

        contig = scaffold_lines[scaffold][0][2]
        rootcontig=mapp.find_node_by_name(contig)
        mapp.set_alias(rootcontig,"scaffold_"+str(scaffold))

        if len(scaffold_lines[scaffold])<3: continue

        if not rootcontig in mapp.roots:
            print("wtf?",scaffold,rootcontig)
            mapp.obsoleted[rootcontig.root]=1
            try:
                del mapp.roots[rootcontig.root]
            except:
                print("error trying to remove", rootcontig.root, "from the roots list",mapp.print_name(rootcontig.root),contig)
#            del mapp.roots[rootcontig.root]
            mapp.roots[rootcontig]=1
            mapp.reroot(rootcontig)

        
        #mapp.dont_update_coords=True

        for i in range(1,len(scaffold_lines[scaffold])-1,2):
            contig1 = scaffold_lines[scaffold][i  ][2]
            contig2 = scaffold_lines[scaffold][i+1][2]
#            print "join",contig1,contig2,end1,end2

            x=mapp.find_node_by_name(contig1)
            y=mapp.find_node_by_name(contig2)

            print("add join",i,x,y)

            mapp.add_join( x , y, 1000 )

        mapp.dont_update_coords=False

#        print mapp.print_name(rootcontig),len(scaffold_lines[scaffold])
        if not rootcontig in mapp.roots:
            print("WTF:  shouldn't need to reroot here")
            
#            mapp.obsoleted[rootcontig.root]=1
            if rootcontig.root in mapp.roots: del mapp.roots[rootcontig.root]
            mapp.roots[rootcontig]=1

        mapp.reroot(rootcontig)

    print("added all the joins")
    mapp.update_roots()
    print("updated roots")

    #    f=open(args.outmap,"wt")
    mapp.write_map_to_file(args.outmap)

    #    pickle.dump(mapp, f)
    #    f.close()

