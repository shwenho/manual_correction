#!/usr/bin/env python3
#!/usr/bin/env python3
from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import sys
import argparse
import random
import gzip
import glob
import re
import os

#sf=1000.0
 
def inregions(regions,x):
    for r in regions:
        if not x[0]==r[0]: continue
        if r[2]==-1: return True
        if (min(x[1:])>=r[1] and min(x[1:])<=r[2]) or (max(x[1:])>=r[1] and max(x[1:])<=r[2]): return True
    return False

def spans_region(regions,x):
    for r in regions:
        if not x[0]==r[0]: continue
        if r[2]==-1: return True
    
        if (min(x[1:])<r[1] and max(x[1:])>r[2]): return True
    return False


def whichregion(regions,x):
    for i in range(len(regions)):
        r=regions[i]
        if not x[0]==r[0]: continue
        if r[2]==-1: return i
        if (min(x[1:])>=r[1] and min(x[1:])<=r[2]) or (max(x[1:])>=r[1] and max(x[1:])<=r[2]): return i
    return False
        
if __name__=="__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-i','--input')
    parser.add_argument('-l','--links')
    parser.add_argument('-o','--outfile',help="Specify the file.  If the filename ends with .pdf, produces pdf using asymptote.")
    parser.add_argument('-I','--internal',action="store_true",help=" ")
    parser.add_argument('-d','--debug',action="store_true",help=" ")
    parser.add_argument('-D','--dots',action="store_true",help=" ")
    parser.add_argument('--noreference',action="store_true")
    parser.add_argument('--singlecontig',action="store_true")
    parser.add_argument('-c','--chr',default="3")
    parser.add_argument('-m','--min',default=1000.0 ,type=float)
    parser.add_argument('-f','--scale',default=10000.0   ,type=float,help=" ")
    parser.add_argument('-r','--rule', default=False, type=float,help=" ") 
    parser.add_argument('-a','--max',default=10.0e6,type=float)
    parser.add_argument('-p','--padding',default=0,type=int)
    parser.add_argument('-R','--region')
    parser.add_argument('-S','--savelinks')
    parser.add_argument('-F','--features')
    parser.add_argument('-T','--ticks')

    parser.add_argument('--segments',default=False)
    parser.add_argument('-b','--bamfile',action="append")
    parser.add_argument('--mask',default=False)
    parser.add_argument('--raw',default=False)
    parser.add_argument('-q','--mapq',default=10,type=int)

    args = parser.parse_args()
    if args.noreference and not args.rule: 
        args.rule=50000.0

    if len(args.bamfile)>0 and args.links:
        print("You should specify either a links file or a list of bam files, but not both.")
        raise Exception 

    if len(args.bamfile)>0:
#        if args.progress: print("#",args)
        from chicago_edge_links2 import SegMapper, bam2chicagolinks, read_mask_ranges
        import pysam
        if args.segments:
            smap=SegMapper(args.segments)
        else:
            smap=False

        if args.mask:
            mask_ranges = read_mask_ranges( open(args.mask) )
        else:
            mask_ranges={}

        bamlist = [ pysam.Samfile(bamfile,"rb") for bamfile in args.bamfile ] 

#    args = parser.parse_args()
    print(args)
    if args.scale: sf=args.scale
    ticks=[]
    if args.ticks:
        f=open(args.ticks)
        while True:
            line=f.readline()
            if not line: break
            c=line.strip().split()
            scaffold=c[0]
            cr=scaffold
            x=float(c[1]) 
            ticks.append((scaffold,x))

    regions = []
    if args.region:
        if args.singlecontig:
            regions = [(args.region,-1,-1)]
        else:
            for region in args.region.split(","):        
                m=re.match("([^:\-]*):(\d+)-(\d+)",region)
                if m:
                    chr,minx,maxx=m.groups()
                    regions.append( (chr,int(minx)-args.padding,int(maxx)+args.padding) )
                    print((chr,int(minx)-args.padding,int(maxx)+args.padding))
    
    scaffold_hues={}
    scaffold_saturations={}
    coords={}
    coord_map={}
    scaffold={}
    contig_strand={}
    my_contigs={}
    bh={}
    sc_max=[{}]*len(regions) #{}
    sc_min=[{}]*len(regions)
    ch_max=[{}]*len(regions)
    ch_min=[{}]*len(regions)
    contig_to_region={}
    def mean(l):
        if len(l)==0: return 0.0
        return old_div(sum(map(float,l)),len(l))

    if args.singlecontig:
        my_contigs=dict( [ (r[0],1) for r in regions])
        for c in my_contigs.keys():
            coord_map[c]=1
            contig_to_region[c]=0
            scaffold[c]=1
            bh[c]=False
            coords[c,"5"]=(1,1,c)
            coords[c,"3"]=(13000,13000,c)

    else:
        f=open(args.input)
        seg_list=[]
        contig_spans={}
        last_contig=False
        last_y=False
        while True:
            l=f.readline()
            if not l: break
            if l[:2]=="p:":
                c=l.strip().split()
                #print c
                sc,contig_end,x,chr,y,contig_length = c[1],c[2],float(c[3]),c[4],float(c[5]),int(c[7])
                #p:      1       Scaffold319768_1.3      0       22      32366537        5841985 2811    ['5336.0', '22', '+', '32363746', '32366537', '1', '2791', '2811']
                if args.noreference:
                    chr=sc
                    y=x
                    v=[1000.0,sc,'+',x,x]
                else:
                    v=eval(" ".join(c[8:]))
                strand="+"
                if v:
                    strand = v[2]


                #print chr,y,i,sc,len(ch_max[i].values()),mean(ch_max[i].values())
                contig,end = contig_end[:-2],contig_end[-1:]
                ((ocontig,base),) = re.findall("(.*)_(\d+)$",contig)
                if end in ["5",5]:
                    base=int(base)
                    seg_list.append((ocontig,base,base+contig_length))


                if last_contig and contig==last_contig:
                    contig_spans[contig] = (chr,last_y,y)
                last_contig=contig
                last_y=y

                if not inregions(regions,(chr,y)): 
                    if (not contig in contig_spans) or (not spans_region(regions,contig_spans[contig])) :
                        continue
                i=whichregion(regions,(chr,y))

                my_contigs[contig]=1
                contig_to_region[contig] = i
                contig_strand[contig]=strand
                if args.noreference:
                    scaffold_hues[contig]=1
                else:
                    scaffold_hues[sc]=1

                #print "\t".join(map(str,[sc,contig_end,contig,end,x,y,chr,strand]))
                coords[contig,end]=(x,y,chr)
                coord_map[contig] = 1
                scaffold[contig]=sc
                bh[contig]=v
                sc_max[i][sc,chr] = max( sc_max[i].get((sc,chr),-1),   x )
                sc_min[i][sc,chr] = min( sc_min[i].get((sc,chr),1e10), x )
                ch_max[i][sc,chr] = max( ch_max[i].get((sc,chr),-1),   y )
                ch_min[i][sc,chr] = min( ch_min[i].get((sc,chr),1e10), y )

#        for x in seg_list:
#            print("xx",x)
        smap=SegMapper(False,seg_list)


    x_offset={}
    y_offset={}
    row_spacing= 0.5e6
    print("#regions",regions)
    for i in range(len(regions)):
#        z=mean( [max([ coords[c,e][1] for c,e in coords.keys() if contig_to_region[c]==i ]), min([ coords[c,e][1] for c,e in coords.keys() if contig_to_region[c]==i ])] ) 
        z=mean( [regions[i][1],regions[i][2]] )
        x_offset[i] = -z + i*row_spacing
        y_offset[i] = -z - i*row_spacing
        print("#",i,x_offset[i],y_offset[i],len(list(ch_max[i].values())))

    print(len(list(coords.keys())))

    other={'3':'5','5':'3'}
    to_clip=[]
    for c,e in list(coords.keys()):
        if not (c,other[e]) in coords:
            to_clip.append(c)
    for k in to_clip:
        del coord_map[k]
    print(len(list(coords.keys())))

    import colorsys
    scaffold_rgb={}
    for s in list(scaffold_hues.keys()):
        #scaffold_hues[s]        = 360.0*random.random()
        #scaffold_saturations[s] = 0.5*random.random() + 0.5
        scaffold_rgb[s] = colorsys.hls_to_rgb( random.random(), 0.5, 0.5*random.random() + 0.5)
        #scaffold_rgb[s] = '%02x%02x%02x' % (255.0*rgb[0], 255.0*rgb[1], 255.0*rgb[2] )

    cm={} # will hold the mean blast-mapped chromosome coordinate of contigs ends for each scaffold/chromosome combo
    cs={} # will hold the chosen strand... 
    cb={} # will hold the mean scaffold coordinate of contig ends for each scaffold/chromosome combo
    cc={} # number of contigs per scaffold/chromosome combo
    #print "1"
    sys.stdout.flush()
    for contig in list(coord_map.keys()):
        sc = scaffold[contig]
        cr=-1
        if bh[contig]:
            cr = bh[contig][1]
#        print(("#sc:",sc,cr))
        for e in ("3","5"):
            cm[sc,cr] = cm.get((sc,cr),0.0) + coords[contig,e][1]
            cb[sc,cr] = cb.get((sc,cr),0.0) + coords[contig,e][0]
            cc[sc,cr] = cc.get((sc,cr),0)   + 1
    sys.stdout.flush()

    for sc,cr in list(cm.keys()):
        cm[sc,cr] /= float(cc[sc,cr])
        cb[sc,cr] /= float(cc[sc,cr])            
    sys.stdout.flush()
    
    for contig in list(coord_map.keys()):
        sc = scaffold[contig]
        cr=-1
        if bh[contig]:
            cr = bh[contig][1]
            #        cr = bh[contig][1]
        for e in ("3","5"):
            x,y,chr = coords[contig,e]
            if (y-cm[sc,cr] > 0 and x-cb[sc,cr] > 0) or ( y-cm[sc,cr]<0 and x-cb[sc,cr]<0) : 
                cs[sc,cr] = cs.get((sc,cr),0) + 1
            else: 
                cs[sc,cr] = cs.get((sc,cr),0) - 1
                
    sys.stdout.flush()

    contigs = list(coord_map.keys())
    contigs.sort(key=lambda x: coord_map[x])
    drawn={}

    print(os.path.splitext(args.outfile))
    if os.path.splitext(args.outfile)[1]==".pdf":
        print("write pdf")
        import pipes
        t = pipes.Template()
        t.append("asy -f pdf -o {}".format(args.outfile),"-.")
        print(t)
        of = t.open("pipefile","w")
    else:
        of = open(args.outfile,"wt")
    
    #    print '//outformat="pdf";'
    of.write("pen feature=red+linewidth(12.0*linewidth());\n")
    of.write("pen thick=linewidth(10.0*linewidth());\n")
    of.write("pen thin=linewidth(0.25*linewidth());\n")
    of.write("var t=rotate(-45.0);\n")

    coords_by_scaffold={}
    for contig in contigs:
        sc = scaffold[contig]
        i = contig_to_region[contig]
        cr=-1

        if bh[contig]:
            cr = bh[contig][1]
        else:
            continue
            #cr = bh[contig][1]
        for e in ("3","5"):
            x,y,chr = coords[contig,e]
            #d = cm[sc,cr] - cb[sc,cr]
            z = cm[sc,cr] + (x - cb[sc,cr])
            if cs[sc,cr]<0:
                z = cm[sc,cr] - (x - cb[sc,cr])

            #print "x",sc,cr,x,cb[sc,cr],sc_min[sc,cr],sc_max[sc,cr],y,cm[sc,cr],ch_min[sc,cr],ch_max[sc,cr],cs[sc,cr]<0,y,z
        d = cm[sc,cr] - cb[sc,cr]

        x5,y5,chr5 = coords[contig,"5"]
        z5 = cm[sc,cr] + (x5 - cb[sc,cr])
        if cs[sc,cr]<0:
            z5 = cm[sc,cr] - (x5 - cb[sc,cr])
            
        x3,y3,chr3 = coords[contig,"3"]
        z3 = cm[sc,cr] + (x3 - cb[sc,cr])
        if cs[sc,cr]<0:
            z3 = cm[sc,cr] - (x3 - cb[sc,cr])

        #if contig_strand[contig]=="-":
        #    q=y3
        #    y3=y5
        #    y5=q

        z3 += x_offset[i]
        z5 += x_offset[i]
        y3 += y_offset[i]
        y5 += y_offset[i]
            
        if True: #inregions(regions,(cr,z3,y3)):  #(cr==args.chr) and (args.min<z3) and (z3<args.max) and (args.min<y3) and (y3<args.max):
            drawn[contig]=1
            #            print "".format
            of.write("//{}\n".format(contig))
            if args.noreference:
                thisRGB = "rgb({},{},{})".format(scaffold_rgb[contig][0],scaffold_rgb[contig][1],scaffold_rgb[contig][2])
            else:
                thisRGB = "rgb({},{},{})".format(scaffold_rgb[sc][0],scaffold_rgb[sc][1],scaffold_rgb[sc][2])

            of.write( "draw( currentpicture, \"\",t*(({},{})--({},{})),NoAlign,{}+thick);\n".format( z3/sf, y3/sf, z5/sf, y5/sf , thisRGB ) )

            coords_by_scaffold[sc] = coords_by_scaffold.get(sc,[]) + [ (z3,y3),(z5,y5) ]

            of.write( "label(rotate(-90)*scale(0.5)*\"{}\",t*({},{}),{},align=SW);\n".format(" ".join(contig.split("_")),0.5*(z3+z5)/sf,0.5*(y3+y5)/sf, thisRGB ))
            #        print "draw(({},{})--({},{}),thick); //{}".format( x3, y3, x5, y5 , cr)
            #if (y-cm[sc,cr] > 0 and x-cb[sc,cr] > 0) or ( y-cm[sc,cr]<0 and x-cb[sc,cr]<0) : 
            #    cs[sc,cr] = cs.get((sc,cr),0) + 1
            #else: 
            #    cs[sc,cr] = cs.get((sc,cr),0) - 1
            
    for sc in list(coords_by_scaffold.keys()):
        xxx=coords_by_scaffold[sc]
        xxx.sort()
        z0 = xxx[0][0]
        z1 = xxx[-1][0]
        xxx.sort(key=lambda x: x[1])
        y0 = xxx[0][1]
        y1 = xxx[-1][1]
#        of.write( "label(rotate(-90)*scale(0.5)*\"{}\",t*({},{}),rgb({},{},{}),align=SW);\n".format(contig,(z3+z5)/2000.0,(y3+y5)/2000.0, scaffold_rgb[sc][0],scaffold_rgb[sc][1],scaffold_rgb[sc][2] ))
#        sc=re.sub("_"," ",sc)
        if args.noreference:
            thisRGB = "rgb({},{},{})".format(scaffold_rgb[contig][0],scaffold_rgb[contig][1],scaffold_rgb[contig][2])
        else:
            thisRGB = "rgb({},{},{})".format(scaffold_rgb[sc][0],scaffold_rgb[sc][1],scaffold_rgb[sc][2])

            of.write( "label(\"{}\",(0,{})+t*({},{}),{},align=SW);\n".format(re.sub("_"," ",sc),int(100.0*random.random()),old_div((z0+z1),2000.0),old_div((y0+y1),2000.0), thisRGB ))
#        of.write( "label(\"{}\",t*({},{}));\n".format(sc,int((z0+z1)/2000.0),int((y0+y1)/2000.0) ))
    
    sys.stdout.flush()
    of.flush()
    
    def map_coord(a,contig,l,coords,bh):
        i=contig_to_region[contig]
        cr=-1
        if bh[contig]:
            cr = bh[contig][1]
        sc=scaffold[contig]

        d = cm[sc,cr] - cb[sc,cr]

        x5,y5,chr5 = coords[contig,"5"]
        z5 = cm[sc,cr] + (x5 - cb[sc,cr])
        if cs[sc,cr]<0:
            z5 = cm[sc,cr] - (x5 - cb[sc,cr])

        x3,y3,chr3 = coords[contig,"3"]
        z3 = cm[sc,cr] + (x3 - cb[sc,cr])
        if cs[sc,cr]<0:
            z3 = cm[sc,cr] - (x3 - cb[sc,cr])

        f=old_div(float(a),l)
        z = z5+f*(z3-z5)
        y = y5+f*(y3-y5)

        z += x_offset[i]
        y += y_offset[i]
            
        return(z,y)

        pass


    minxx=False
    maxxx=False
    minxy=False
    maxxy=False

    def mark_feature(a,b,s1,l1,coords,bh,of):
        global minxx
        global maxxx
        global minxy
        global maxxy

        za,ya = map_coord(a,s1,l1,coords,bh)
        zb,yb = map_coord(b,s1,l1,coords,bh)
#        outf.write("dot(t*({},{}),black);\n".format(za/1000.0,ya/1000.0))
#        outf.write("dot(t*({},{}),black);\n".format(zb/1000.0,yb/1000.0))

        of.write("draw(t*(({},{})--({},{})),feature);\n".format(za/sf,ya/sf,zb/sf,yb/sf))


    def draw_link(a,b,s1,s2,l1,l2,coords,bh,outf,sameScaffold,ticks=[]):
        global minxx
        global maxxx
        global minxy
        global maxxy

        za,ya = map_coord(a,s1,l1,coords,bh)
        zb,yb = map_coord(b,s2,l2,coords,bh)
#        print("#",scaffold[s1],scaffold[s2],za,ya,zb,yb)
        bad=False
        for s,x in ticks:
            x=x+x_offset[0]
#            print("#",x,za,ya,zb,yb)
            if min(za,zb)<x and max(za,zb)>x:
                bad=True

        dotcolor="black"
        if bad: dotcolor="red"

#        outf.write("dot(t*({},{}),black);\n".format(za/1000.0,ya/1000.0))
#        outf.write("dot(t*({},{}),black);\n".format(zb/1000.0,yb/1000.0))
        if not contig_to_region[s1]==contig_to_region[s2]:
            if sameScaffold:
                outf.write("draw(t*(({},{})--({},{})),blue);\n".format(za/sf,ya/sf,zb/sf,yb/sf))
                outf.write("//{} {} blue link\n".format(s1,s2))
            else:
                outf.write("draw(t*(({},{})--({},{})),orange);\n".format(za/sf,ya/sf,zb/sf,yb/sf))

        elif abs(ya-yb) < args.max and args.min < abs(ya-yb):
            if sameScaffold:
                if args.dots:
                    if zb<ya:
                        outf.write("dot(t*(({},{})),{});\n".format(zb/sf,ya/sf,dotcolor))
                else:
                    outf.write("draw(t*(({},{})--({},{})--({},{})),black+thin);\n".format((za/sf),(ya/sf),(zb/sf),ya/sf,(zb/sf),(yb/sf)))
            else:
                if args.dots:
                    if zb<ya:
                        outf.write("dot(t*(({},{})),{});\n".format(zb/sf,ya/sf,dotcolor))
#                    outf.write("draw(t*(({},{})--({},{})--({},{})),black+thin);\n".format((za/sf),(ya/sf),(zb/sf),ya/sf,(zb/sf),(yb/sf)))
                else:
                    outf.write("draw(t*(({},{})--({},{})--({},{})),red+thin);\n".format((za/sf),(ya/sf),(zb/sf),(ya/sf),(zb/sf),(yb/sf)))
        else:
            #outf.write("dot(t*({},{}),red);\n".format(zb/1000.0,ya/1000.0))
            pass

        if not minxx or minxx > za: minxx = za
        if not minxx or minxx > zb: minxx = zb
        if not maxxx or maxxx < za: maxxx = za
        if not maxxx or maxxx < zb: maxxx = zb

        if not minxy or minxy > ya: minxy = ya
        if not minxy or minxy > yb: minxy = yb
        if not maxxy or maxxy < ya: maxxy = ya
        if not maxxy or maxxy < yb: maxxy = yb
        return bad

    link_counts={}
    if args.savelinks:
        slfile=open(args.savelinks,"wt")
    ll={}

    if args.bamfile:
        for s1,s2,l1,l2,nl,v,tids  in bam2chicagolinks(bamlist,my_contigs,smap,mask_ranges,args.mapq,1000,args.internal,tidlist=True):
            if not len(v)==len(tids): raise Exception 
            if args.savelinks and s1 in drawn and s2 in drawn:
                slfile.write("\t".join(map(str,[s1,s2,l1,l2,nl,v]))+"\n")
            if (s1 in drawn) and (s2 in drawn) and (args.internal or (not scaffold[s1] == scaffold[s2])):
                sc1,sc2 = sorted([scaffold[s1],scaffold[s2]])
                #print("#ls",s1,s2,sc1,sc2,nl,v)
                link_counts[sc1,sc2]=link_counts.get((sc1,sc2),0)+nl
                #print [s1,s2,l1,l2,nl] + v, coords[s1,"5"] , coords[s2,"5"], scaffold[s1],scaffold[s2],
                ll[s1]=l1
                ll[s2]=l2
                for pairi in range(len(v)):
                    a,b = v[pairi]
                    tid = tids[pairi]
                    bad=draw_link(a,b,s1,s2,l1,l2,coords,bh,of,scaffold[s1]==scaffold[s2],ticks=ticks)
                    if bad: print("badtid:",tid,sep="\t")


    if args.links:
        for fn in glob.glob(args.links):
            print(fn)
            if fn[-3:]==".gz":
                f = gzip.open(fn)
            else:
                f = open(fn)

            while True:
                l=f.readline()
                if not l: break
                if l[0]=="#": continue
                c=l.strip().split()
                v=eval(" ".join(c[5:]))
                s1,s2,l1,l2,nl = c[0],c[1],int(c[2]),int(c[3]),int(c[4])
                ll[s1]=l1
                ll[s2]=l2
                if args.savelinks and s1 in drawn and s2 in drawn:
                    slfile.write(l)
                if (s1 in drawn) and (s2 in drawn) and (args.internal or (not scaffold[s1] == scaffold[s2])):
                    sc1,sc2 = sorted([scaffold[s1],scaffold[s2]])
                    print("#ls",s1,s2,sc1,sc2,nl,v)
                    link_counts[sc1,sc2]=link_counts.get((sc1,sc2),0)+nl
                    #print [s1,s2,l1,l2,nl] + v, coords[s1,"5"] , coords[s2,"5"], scaffold[s1],scaffold[s2],
                    for a,b in v:
                        draw_link(a,b,s1,s2,l1,l2,coords,bh,of,scaffold[s1]==scaffold[s2])


    if args.features:
        f=open(args.features)
        while True:
            line=f.readline()
            if not line: break
            c=line.strip().split()
            contig=c[0]
            if not contig in my_contigs: continue
            a=int(c[1])
            b=int(c[2])

            mark_feature(a,b,contig,ll[contig],coords,bh,of)
#            mark_feature(contig,a,b,of)

    if args.ticks:
        for scaffold,x in ticks:
            z = x  + x_offset[0]
            of.write("draw(t*(({},{})--({},{})));\n".format((z-50000)/sf,(z+50000)/sf,(z+20000)/sf,(z-20000)/sf))
            
    if args.rule:
        of.write("draw(currentpicture, \"\",t*(({},{})--({},{})),NoAlign,red);\n".format(minxx/sf,(minxx+args.rule)/sf,(maxxy-args.rule)/sf,(maxxy)/sf))
#        of.write("draw(currentpicture, \"\",t*(({},{})--({},{})),NoAlign,red);\n".format(minxx/sf,(minxx+args.rule)/sf,(maxxy-args.rule)/sf,(maxxy)/sf))
        of.write( "label(rotate(0)*scale(2.0)*\"{:,.0f} Kb\",t*({},{})-(10,0),{},align=SW);\n".format(args.rule/1000.0,(minxx)/sf,(minxx+args.rule)/sf , "red" ))
        #draw( currentpicture, "",t*((539.17959,539.17959)--(572.58203,572.58203)),NoAlign,rgb(0.9317775030110922,0.06822249698890781,0.48636235555424634)+thick);
    of.close()
    scaffold_pairs = list(link_counts.keys())
    scaffold_pairs.sort( key=lambda x: link_counts[x], reverse=False )
    for i,j in scaffold_pairs:
        print(i,j,old_div(link_counts[i,j],2))
    if args.savelinks:
        slfile.close()



    
