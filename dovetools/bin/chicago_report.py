#!/usr/bin/env python3


from jinja2 import Environment, PackageLoader
import argh
from dovetools.models import Library
from dovetools.libraries2csv import libraries2csv
import json
import sys
import os

@argh.arg("dove", nargs="+")
def main(dove, output="/dev/stdout", all_chicago=False, results_only = False):
    env = Environment(loader=PackageLoader("chicago_report", "templates"))
    template = env.get_template("chicago_qc.html")
    # create library objects for each library to include
    dove_file = dove[0]
    if all_chicago:
        if len(dove) != 1:
            print("Should have exactly one dove.json when using 'all'", file=sys.stderr)
            sys.exit(2)
        sample_names = sorted(get_sample_names(dove_file))
        libraries = [Library.from_json(dove_file, sample) for sample in sample_names]
        
    else:
        libraries = get_libraries(dove,results_only)
    # get control library
    general = Library.get_general(dove_file)
    example_path = os.path.join(os.path.dirname(__file__), "static", "control.json")
    example = Library.from_json(example_path, "NA12878 control", general)
    # create reports
    rendered_template = template.render(control=example, samples=libraries, Library=Library)
    with open(output, 'w') as output_handle:
        print(rendered_template, file=output_handle)
    libraries2csv(libraries=libraries, filename=output+".txt")

def get_sample_names(dove):
    with open(dove,"r") as dove_handle:
        data = json.load(dove_handle)
    sample_names = [key for key in data["data"]["chicago"].keys() if "output" in data["data"]["chicago"][key]]
    return sample_names

def get_libraries(dove,results_only=False):
    libraries = []
    dove_file = None
    sample = None
    for item in dove:
        if item.endswith(".json"):
            if results_only:
                lib = os.path.basename(os.path.dirname(item))
                libraries.append(Library.from_json(item, 
                                                   lib, 
                                                   results_only = True))
            else:
                dove_file = item
            continue
        else:
            sample = item
        if sample:
            if not dove_file:
                print("Must have a .json file before providing sample {}".format(sample), file=sys.stderr)
                sys.exit(2)
            libraries.append(Library.from_json(dove_file, sample))
    return libraries

if __name__ == "__main__":
    argh.dispatch_command(main)
