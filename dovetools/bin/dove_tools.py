#!/usr/bin/env python3

import dovetools as dt
import argh


if __name__ == "__main__":    
    
    argh.dispatch_commands(
        [dt.fastq_trim.fastq_trim,
         dt.fastq_name_filter.fastq_name_filter,
         dt.sam_add_tags.sam_add_tags,
         dt.sam_subset.sam_subset,
         dt.ios_bins.ios_bins,
         dt.bam_stats.bam_stats,
         dt.qc_report.qc_report,
         dt.sam_single_snps.sam_single_snps])

    
