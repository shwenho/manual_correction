#!/usr/bin/env python3

"""
Author: Jonathan Stites
Company: Dovetail Genomics
Contact: jon@dovetail-genomics.com
Created: 2015-09-10
"""


import json
import argh
import glob
import os
import itertools
import sys
from datetime import datetime

def main(directory: "Directory containing reads",
         description: "A description of the library" = "No description given", 
         junction: "The chicago junction" = "GATCGATC", 
         output: "The output json file. Defaults to reads.json in input directory" = None,
         date: "The date of library creation. Defaults to today" = datetime.now().strftime("%Y-%m-%d"),
         ticket: "The JIRA ticket"=None):
    """
    Script to automatically create json files for fastq reads.
    
    Usage:
    json_reads.py CP510_miseq 
    
    dove-pipe merge -m CP510_miseq/reads.json -s "chicago|CP510_miseq" 
    """

    r1 = get_reads(directory, "1")
    r2 = get_reads(directory, "2")

    if len(r1) != len(r2):
        raise Exception("R1 different number of files R2", r1, r2)

    if bool(set(r1) & set(r2)):
        raise Exception("A file is in both R1 and R2", r1, r2)

    data = get_data(r1, r2, date, description, junction, ticket)
    

    if output is None:
        output = os.path.join(directory, "reads.json")
        
    with open(output, 'w') as output_handle:
        json.dump(data, output_handle, indent=True, sort_keys=True)

def get_reads(directory, read_pair):
    read_filters = ["r" + read_pair, "R" + read_pair]
    fastq_ext = ["fq", "fastq", "fq.gz", "fastq.gz"]
    files = glob.glob(os.path.join(directory, "*"))
    reads = []

    for possible_file in files:
        # Check to make sure it matches a read type and ends in known extension
        for read_filter, ext in itertools.product(read_filters, fastq_ext):
            if read_filter in possible_file and possible_file.endswith(ext):
                abs_file = os.path.abspath(possible_file)
                reads.append(abs_file)
                break
    return reads

def get_data(r1, r2, date, description, junction, ticket):
    data = {}
    data["raw_regex_r1"] = "|".join(r1)
    data["raw_regex_r2"] = "|".join(r2)
    
    data["date"] = date
    data["description"] = description
    data["junction"] = junction
    if ticket:
        data["ticket"] = ticket
    return data


if __name__ == "__main__":
    argh.dispatch_command(main)
