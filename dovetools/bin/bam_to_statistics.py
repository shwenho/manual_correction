#!/usr/bin/env python3

"""
Author:          Jonathan Stites
Date:            03 April 2015
Company:         Dovetail Genomics

Description:
Takes a Chicago bam file and outputs statistics that can be used for
QC. 

Usage:
bam_to_statistics.py -c coverage.txt -i 500 -b human.bam --assembly hg19.json --output human.json "


"""
import sys
import argh
import json
import pysam
from math import log
from collections import defaultdict,Counter
from dovetools.utils import BamTags

MAX_SEP=10000000

def main(coverage_quantiles: "model coverage" = None, 
         bam: "chicago bam file" = None, 
         mapq: "map quality threshold" = 20, 
         fragment_size: "size selection" = None, 
         assembly_statistics: "assembly statistics in json" = None,
         output: "output file" = None): 



    keys = [
        "begin_junctions",
        "raw_junctions",
        "mapped_junctions",
        "mapped_chimeras",
        "duplicates",
        "unmapped",
        "low_mapq",
        "different_scaffold",
        "0_to_2kb_insert",
        "2kb_to_10kb_insert", 
        "10kb_to_100kb_insert", 
        "100kb_to_200kb_insert",
        "200kb_to_inf_insert",
        "duplicates_percent",
        "unmapped_percent",
        "low_mapq_percent",
        "different_scaffold_percent",
        "0_to_2kb_insert_percent",
        "2kb_to_10kb_insert_percent", 
        "10kb_to_100kb_insert_percent", 
        "100kb_to_200kb_insert_percent",
        "200kb_to_inf_insert_percent",
        "total_read_pairs",
        "raw_coverage",
        "model_coverage",
        "raw_coverage_150M",
        "model_hiseq_coverage",
        "raw_junction_percent",
        "begin_junction_percent",
        "estimated_junction_percent",
        "chimeric_percent"
    ]

    
    genome_size = get_genome_size(assembly_statistics)

    statistics = {key: 0 for key in keys}
    statistics["raw"] = {"inserts" : Counter(), 
                         "counts"  : Counter()}
        
    statistics["cumulative"] = {"inserts" : Counter(), 
                                "counts"  : Counter()}
        
    parse_coverage_quantiles(coverage_quantiles, statistics)

    parse_bam_file(bam, statistics, mapq=mapq, genome_size=genome_size)
    update_bam_percents(statistics)
    estimate_uncorrected_hiseq_coverage(statistics)
    estimate_corrected_hiseq_coverage(statistics)

    uncorrected_junction_percent(statistics)
    begin_junction_percent(statistics)

    read_length = get_read_length(bam)
    statistics["read_length"] = read_length
    corrected_junction_percent(statistics, fragment_size, read_length)
    chimeric_percent(statistics)
    raw_to_cumulative(statistics)

    if output:
        with open(output, 'w') as outfile:
            json.dump(statistics, outfile, sort_keys=True, indent=4)


def parse_bam_file(bam, statistics, mapq, genome_size):
    """Takes as input a bam file and returns several statistics"""
    
    # Total number of read pairs (only counts read1)
    total = 0

    # Raw count of non-duplicate read pairs with junctions
    raw_junctions = 0

    # Count of non-duplicate, mapped reads (> mapq) with junctions and same scaffold
    mapped_junctions = 0

    # Count of non-duplicate, mapped reads (> mapq) with junctions and different scaffold
    mapped_chimeras = 0


    t = 0
    with pysam.AlignmentFile(bam, "rb") as bam_file:
        for line in bam_file:

            update_total(line, statistics)
            update_raw_junctions(line, statistics)
            update_begin_junctions(line, statistics)
            update_chimeric(line, statistics, mapq)
            update_read_category(line, statistics, mapq)
            update_uncorrected_coverage(line, statistics, mapq)


    statistics["raw_coverage"] = statistics["raw_coverage"] / genome_size

    # make sure all increments are present
    inserts = statistics["raw"]["inserts"]
    inserts_largest_key = int(max(inserts.keys(), key=int))
    for power in range(3, int(log(inserts_largest_key, 10)) + 1):
        for i in range(1, 10):
            increment = i * (10 ** power)
            if increment > inserts_largest_key:
                break
            # set to 0 if it doesnt exist
            if increment not in inserts:
                inserts[increment] = 0

    counts = statistics["raw"]["counts"]
    counts_largest_key = int(max(counts.keys(), key=int))
    for power in range(3, int(log(counts_largest_key, 10)) + 1):
        for i in range(1, 10):
            increment = i * (10 ** power)
            if increment > counts_largest_key:
                break
            # set to 0 if it doesnt exist
            if increment not in counts:
                counts[increment] = 0


def update_bam_percents(statistics):
    keys = ["duplicates",
            "unmapped",
            "low_mapq",
            "different_scaffold",
            "0_to_2kb_insert",
            "2kb_to_10kb_insert", 
            "10kb_to_100kb_insert", 
            "100kb_to_200kb_insert",
            "200kb_to_inf_insert"]
    for key in keys:
        percent_key = key + "_percent"
        percent_value = statistics[key] / statistics["total_read_pairs"]
        statistics[percent_key] = percent_value

def parse_coverage_quantiles(coverage_quantiles, statistics):
    """Takes as input a coverage quantile file created with 
    coverageQuantiles.py  and returns the parsed data. This is physical 
    coverage, not read coverage."""

    with open(coverage_quantiles) as c:
        for line in c:
            s = line.split()
            cov = float(s[0])
            min_cov = float(s[6])
            max_cov = float(s[8])
    statistics["model_coverage"] = cov
    statistics["min_length_for_coverage"] = min_cov
    statistics["max_length_for_coverage"] = max_cov
    
def begin_junction_percent(statistics):
    begins = statistics["begin_junctions"]
    total = statistics["total_read_pairs"] * 2
    statistics["begin_junction_percent"] = float(begins)/total

def uncorrected_junction_percent(statistics):
    junctions = statistics["raw_junctions"]
    total = statistics["total_read_pairs"]
    statistics["raw_junction_percent"] = junctions/total

def corrected_junction_percent(statistics, fragment_size, read_length):
    junctions = statistics["raw_junctions"]
    total = statistics["total_read_pairs"]
    try:
        factor = int(fragment_size) / (2 * read_length )
    except ValueError:
        statistics["estimated_junction_percent"] = "NA"
        return
    # Don't want to go below 1
    factor = max(factor, 1)
    estimated = min(factor * junctions/total, 1)
    statistics["estimated_junction_percent"] = estimated

def chimeric_percent(statistics):
    mapped_chimeric = statistics["mapped_chimeras"]
    mapped_junctions = statistics["mapped_junctions"]
    try:
        statistics["chimeric_percent"] = mapped_chimeric / (mapped_chimeric + mapped_junctions)
    except ZeroDivisionError:
        statistics["chimeric_percent"] = 0

def estimate_corrected_hiseq_coverage(statistics):
    corrected_coverage = statistics["model_coverage"]
    total = statistics["total_read_pairs"]
    hiseq_coverage = corrected_coverage * 150000000 / total
    statistics["model_coverage_150M"] = hiseq_coverage
    

def estimate_uncorrected_hiseq_coverage(statistics):
    uncorrected_coverage = statistics["raw_coverage"]
    total = statistics["total_read_pairs"]
    hiseq_coverage = uncorrected_coverage * 150000000 / total
    statistics["raw_coverage_150M"] = hiseq_coverage

def update_total(line, statistics):
    """Takes as input a pysam alignment and a integer of the
    total read pairs and return the total incremented by one."""

    if not line.is_read2:
        statistics["total_read_pairs"] += 1

def update_raw_junctions(line, statistics):
    """Takes as input a pysam alignment and a dictionary with raw_junctions,
    and increments the count if the read passes filters and had a junction."""

    if line.is_read2:
        return
    if line.is_duplicate:
        return
    if BamTags.junction(line) == "F":
        return
    statistics["raw_junctions"] += 1

def update_begin_junctions(line, statistics):
    if line.is_duplicate:
        return
    if line.query_sequence.startswith("GATC"):
        statistics["begin_junctions"] += 1

def update_chimeric(line, statistics, mapq):
    """Takes as input a pysam alignment and a dictionary with mapped
    reads with junctions, and mapped reads to different scaffolds with 
    junctions. Returns the integers, incremented if necessary"""

    if line.is_read2:
        return
    if line.is_duplicate:
        return
    if BamTags.junction(line) == "F":
        return
    if line.mapping_quality < mapq or BamTags.mate_mapq(line) < mapq:
        return

    if BamTags.orientation(line) == "D":
        statistics["mapped_chimeras"] += 1
    else:
        statistics["mapped_junctions"] += 1
    

def update_read_category(line, statistics, mapq):
    """Takes as input a pysam alignment and a dictionary and increments
    "duplicates", "unmapped", "mapq", "different_scaffold", "0_to_2kb_insert",
    "2kb_to_10kb_insert", "10kb_to_100kb_insert", "100kb_to_200kb_insert",
    "200kb_to_inf_insert" and increments their counts as necessary."""

    if line.is_read2:
        return
    if line.is_duplicate:
        statistics["duplicates"] += 1
        return
    if line.is_unmapped or line.mate_is_unmapped:
        statistics["unmapped"] += 1
        return
    if line.mapping_quality < mapq or BamTags.mate_mapq(line) < mapq:
        statistics["low_mapq"] += 1
        return
    if line.opt("xt") == "D":
        statistics["different_scaffold"] += 1
        return
        
    insert = abs(line.reference_start - line.next_reference_start)
    update_raw(insert,statistics)
    
    if insert < 2000:
        statistics["0_to_2kb_insert"] += 1
        return
    
    if insert < 10000:
        statistics["2kb_to_10kb_insert"] += 1
        return

    if insert < 100000:
        statistics["10kb_to_100kb_insert"] += 1
        return
        
    if insert < 200000:
        statistics["100kb_to_200kb_insert"] += 1
        return

    if insert >= 200000:
        statistics["200kb_to_inf_insert"] += 1
        return

def raw_to_cumulative(statistics):
    steps = sorted(map(int,statistics["raw"]["inserts"].keys()))
    count_sum = 0
    ins_sum = 0
    for step in steps:
        statistics["cumulative"]["inserts"][step] = ins_sum
        statistics["cumulative"]["counts"][step] = count_sum

        ins_sum += statistics["raw"]["inserts"][step]
        count_sum += statistics["raw"]["counts"][step]
    statistics["cumulative"]["inserts"][steps[-1]] = ins_sum
    statistics["cumulative"]["counts"][steps[-1]] = count_sum

def update_raw(insert,statistics):
    if insert == 0:
        order = 3
    else:
        order = max(int(log(insert,10)),3)
    step = 10 ** order
    relative_bin = insert // step
    abs_bin = min((step * relative_bin),MAX_SEP)
    statistics["raw"]["counts"][abs_bin] += 1
    statistics["raw"]["inserts"][abs_bin] += insert

def update_uncorrected_coverage(line, statistics, mapq, cov_min=2000, cov_max=50000):
    """Takes as input a pysam alignment and a dictionary, and updates the estimated 
    physical coverage for reads 2000kb to 50000kb. This is not corrected for contiguity."""

    if line.is_read2:
        return

    if line.is_duplicate:
        return

    if line.mapping_quality < mapq or BamTags.mate_mapq(line) < mapq:
        return

    if line.reference_id != line.next_reference_id:
        return
        
    insert = abs(line.reference_start - line.next_reference_start)

    if insert < cov_min or cov_max < insert:
        return

    statistics["raw_coverage"] += insert

def get_genome_size(assembly):
    with open(assembly) as f:
        data = json.load(f)
        return data["scaffolds"]["Total length"]

def get_read_length(bam, read_num = 100000):

    longest = 0
    with pysam.AlignmentFile(bam, 'rb') as bam_handle:
        for i, aln in enumerate(bam_handle):
            if i > read_num:
                break
            l = len(aln.query_sequence)
            if l > longest: 
                longest = l
            
    return longest

if __name__ == "__main__":
    argh.dispatch_command(main)
