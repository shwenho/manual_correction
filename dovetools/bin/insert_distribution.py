#!/usr/bin/env python3


import argh
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import pysam
from operator import itemgetter
from collections import Counter
import sys
import os
from matplotlib.ticker import FuncFormatter

default_fontdict = {'fontsize': mpl.rcParams['axes.titlesize'],
                    'fontweight' : "bold"#mpl.rcParams['axes.titleweight']
                    }

def bpFormatter(x,pos):
    if x >= 1e9:
        return "%0.2f Gbp" %(x/1e9)
    if x >= 1e6:
        return "%0.2f Mbp" %(x/1e6)
    if x >= 1e3:
        return "%0.2f kbp" %(x/1e3)
    if x < 0:
        return "%d" % (x)
    else:
        return "%d bp" % (x)

@argh.arg("bam", nargs="+")
@argh.arg("-n", "--name", nargs="+")
@argh.arg("-t", "--text-hist", nargs="+")
@argh.arg("-v", "--verticals", nargs="+")
def main(bam, 
         output=None, 
         show=False, 
         mapq=20, 
         binsize=5000, 
         junction=False, 
         name=None, 
         xlimit=300000, 
         cat_bam=False,
         cdfs_out=None,
         write_counts=True,
         text_hist=None,
         verticals=None, 
         plain=False,
         latex=False, 
         log_log=False,
         markeredgewidth=1):
    if latex:
        mpl.rc('text', usetex=True)
        mpl.rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
    if text_hist:
        if not cat_bam and name and len(text_hist) != len(name):
            raise Exception("WARN: Number of names does not match number of text hist files", text_hist, name)
        counts = read_counts(text_hist)
    else:
        if not cat_bam and name and len(bam) != len(name):
            raise Exception("WARN: Number of names does not match number of bam files", bam, name)
        
        counts = [get_counts(bam_file, mapq, junction, binsize) for bam_file in bam]
        if cat_bam: 
            counts = combine_counts(counts)
    if write_counts: print_counts(counts,cat_bam,bam,junction)
    plot_inserts(counts, binsize, show, output, name, xlimit,plain,cdfs_out, verticals, markeredgewidth,log_log)


def read_counts(text_hist):
    counts_list =[]
    for fn in text_hist:
        fh = open(fn,'r')
        counts = Counter()
        for line in fh:
            num,count = line.split()
            counts[num]=int(count)
        fh.close()
        counts_list.append(counts)
    return counts_list

def print_counts(counts,cat_bam,bams,junctions = False):
    if junctions:
        suffix = ".junctions.txt"
    else:
        suffix = ".txt"
    if cat_bam:
        out = open("combined_counts"+suffix,'w')
        for count in counts:
            for num in sorted(count.keys()):
                print("\t".join(map(str,[num,count[num]])),file=out)
        out.close()
    else:
        for bam,count in zip(bams,counts):
            out = open(os.path.splitext(os.path.basename(bam))[0] + ".counts" + suffix,"w")
            for num in sorted(count.keys()):
                print("\t".join(map(str,[num,count[num]])),file=out)
            out.close()
     


def combine_counts(counts):
    summed_counts = Counter()
    for count in counts:
        summed_counts += count
    return [summed_counts]

def get_counts(bam, mapq, junction, binsize):
    counts = Counter()
    with pysam.AlignmentFile(bam, 'r') as f:
        for aln in f:
            if aln.is_read2:
                continue
            if aln.is_duplicate:
                continue
            if aln.is_unmapped or aln.mate_is_unmapped:
                continue
            if aln.mapq < mapq or aln.opt("MQ") < mapq:
                continue
            if junction and aln.opt("xj") == "F":
                continue
            if aln.reference_id != aln.next_reference_id:
                continue

            insert = abs(aln.reference_start - aln.next_reference_start)
            binned_insert = insert // binsize
            x_coord = binned_insert * binsize
            counts[x_coord] += 1
    return counts

def pdf_to_cdf(pdf):
    cdf = []
    tot = 0
    for prob in pdf:
        tot += prob
        cdf.append(tot)
    return cdf


def plot_inserts(counts, binsize, show, output, name, xlimit,plain,cdfs_out=None,verticals=[], markeredgewidth=1, log_log = False):

    for position, this_count in enumerate(counts):
        if cdfs_out: plt.figure(1)
        xy = this_count.items()
        sorted_xy = sorted(xy,key=itemgetter(0))
        x,y = zip(*sorted_xy)
        total = sum([i for i in y])
        pdf = [i/total for i in y]

        if name:
            plt.plot(x, pdf, "o", label=name[position], markeredgewidth=markeredgewidth)
            plt.legend()
        else:
            if plain:
                plt.plot(x, pdf, "ko")
            else:
                plt.plot(x, pdf, "o")
        if cdfs_out:
            cdf = pdf_to_cdf(pdf)
            plt.figure(2)
            plt.xlim([0,xlimit])
            plt.ylim([0,1.0])
            plt.xlabel("Distance (bp)") 
            plt.ylabel("Probability of an insert in this range or lower")
            plt.title("Chicago library insert cumulative distribution")
            plt.gca().get_xaxis().set_major_formatter(mpl.ticker.FuncFormatter(lambda x,p: format(int(x),',')))
            if name:
                plt.plot(x,cdf,'o',label=name[position])
                plt.legend()
            else:
                plt.plot(x,cdf,'o')
    if cdfs_out: plt.figure(1) 
    plt.xlim([0, xlimit])
    plt.xlabel("Distance (bp)",fontdict=default_fontdict)
    if not plain:
        plt.title("Chicago library insert distributions")
    plt.ylabel("Probability of an insert in this range",fontdict=default_fontdict)
    plt.yscale("log")
    if log_log:
        plt.xscale("symlog")
    ax = plt.gca()
    bp_fmt = FuncFormatter(bpFormatter)
    ax.xaxis.set_major_formatter(bp_fmt)
    labels = ax.get_xticklabels()
    plt.setp(labels, rotation=30)
    plt.tight_layout()
  
    if verticals:
        for num in verticals:
            plt.axvline(x=float(num),ls='--',c='black')

    if show:
        plt.show()
    if output:
        if cdfs_out:
            plt.figure(2)
            plt.savefig(cdfs_out)
            plt.figure(1)
        plt.gcf().set_size_inches(9,7.2) 
        plt.savefig(output)
   
if __name__ == "__main__":
    argh.dispatch_command(main)
