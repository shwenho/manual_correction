#!/usr/bin/env python3

"""
Author:            Jonathan Stites
Date:              03 April 2015
Company:           Dovetail Genomics

Usage:
-j dove.json -l 'chicago_002'   --pdf tumbler_report.pdf --html tumbler_report.html 

Notes:
Does not yet embed images in html.

"""

import json 
import argh
import getpass
import datetime
import matplotlib
matplotlib.rcParams["backend"] = "Agg"
import matplotlib.pyplot as plt
import sys
import uuid
import subprocess
import shutil
import os

@argh.arg("-j", "--jsn", action="append")
@argh.arg("-l", "--library", action="append")
@argh.arg("-s", "--skip", nargs="+")
def main(jsn: "json of statistics"=None, 
         library: "library name in json file"=None, 
         lib_list:"File with names of libraries to generate report for, one per line"=None,
         prefix: "prefix to find library"="data", 
         pdf: "output file for pdf"=None, 
         tmpdir: "temporary directory for images"="/tmp", 
         txt: "file for rst output"=None, 
         html: "file for html output"=None, 
         skip: "steps to skip"=None,
         skip_bad_preseq = True,
         all_chicago = False
        ):

    projects = collect_metadata(jsn, library, prefix,all_chicago,lib_list)

    file_id = os.path.join(tmpdir, "qc_tmp" + str(uuid.uuid4()))
    rst = file_id + ".txt"
    latex = file_id + ".tex"
    temp_pdf = file_id + ".pdf"         
    complexity = file_id + "_complexity.png"
    inserts = file_id + "_all_reads.png"
    inserts_log = file_id + "_all_reads_logx.png"
    inserts_junction = file_id + "_junction_reads.png"
    inserts_junction_log = file_id + "_junction_reads_logx.png"

    with open(rst, 'w') as out:
        write_header(out)
        add_spacing(out)
        print("Dovetail QC Report", file=out)
        print(100*"=", file=out)
        print("Descriptions of libraries in this report.", file=out)
        print(100*"-", file=out)
        add_spacing(out)
        write_descriptions(projects, out)
        add_spacing(out)
        print("Read pair statistics", file=out)
        print(100*"-", file=out)
        explain_bam_stats(out)
        add_spacing(out)

        write_bam_stats(projects, out, raw_bam_categories_keys())
        write_bam_stats(projects, out, percent_bam_categories_keys(), percent=True)
        add_coverage_with_complexity(projects, "raw_coverage", skip_bad_preseq)
        add_coverage_with_complexity(projects, "model_coverage", skip_bad_preseq)

        print("Library metrics", file=out)
        print(100*"-", file=out)
        write_bam_stats(projects, out, misc_stats_keys())
        explain_misc_stats(out)
        add_spacing(out)

        print("Coverage, including complexity", file=out)
        print(100*"-", file=out)
        write_coverage_with_complexity(projects, out, skip_bad_preseq) 
        
        """
        insert_plot(projects, inserts)
        add_image(inserts, out)
        add_spacing(out)

        insert_plot(projects, inserts_log, log=True)
        add_image(inserts_log, out)
        add_spacing(out)

        insert_plot(projects, inserts_junction, junction=True)
        add_image(inserts_junction, out)
        add_spacing(out)

        insert_plot(projects, inserts_junction_log, junction=True, log=True)
        add_image(inserts_junction_log, out)
        add_spacing(out)
        
        if not skip or "preseq" not in skip:
            preseq_plot(projects, complexity)
            add_image(complexity, out)
            add_spacing(out)
        """
        print("FastQC reports", file=out)
        print(100*"-", file=out)
        add_fastqc(projects, out)
        print("Insert distributions", file=out)
        print(100*"-", file=out)
        add_insert_distribution(projects, out)


        write_contig_stats(projects, out, contigs="scaffolds")
        write_contig_stats(projects, out, contigs="contigs")


    if html:
        process = subprocess.Popen(["rst2html.py", rst, html])
        process.wait()
    if txt:
        shutil.copy2(rst, txt)
    if pdf:
        process = subprocess.Popen(["rst2latex.py", rst, latex])
        process.wait()
        process = subprocess.Popen(["pdflatex", "--output-directory", tmpdir, latex])
        process.wait()

        shutil.copy2(temp_pdf, pdf)



def add_coverage_with_complexity(projects, coverage_key, skip_bad_preseq):
    for library in projects:
        chicago_coverage = library["output"]["bam"]["stats"][coverage_key]
        total_reads = library["output"]["bam"]["stats"]["total_read_pairs"]
        total_reads_unique = total_reads - library["output"]["bam"]["stats"]["duplicates"]
        unique_mapped_reads = total_reads - library["output"]["bam"]["stats"]["low_mapq"] - library["output"]["bam"]["stats"]["unmapped"]

        normalized_coverage = chicago_coverage/unique_mapped_reads
        lc_extrap = library["output"]["bam"]["lc_extrap_paired"]

        step = 10000000
        maximum = 400000000

        lib_name = '_'.join([library["project"], library["lib"]])
        lib_table = [[lib_name, "estimated_coverage", "lower_95", "upper_95"], [format_key(total_reads), format_key(chicago_coverage), "NA", "NA"]]
        try:
            coverage_by_complexity = get_coverage_by_complexity(chicago_coverage, unique_mapped_reads, total_reads_unique, lc_extrap, list(range(step, maximum, step)))
        except ComplexityError:
            if skip_bad_preseq:
                coverage_by_complexity = []
                pass
        cov_at_150 = "NA"
        for step in coverage_by_complexity:
            step_num, coverage, upper, low = step
            step_num = int(step_num.replace(",", ""))
            if step_num > 150000000:
                cov_at_150 = coverage
                break
        key = "".join([coverage_key, "_with_complexity_150M"])
        library["output"]["bam"]["stats"][key] = cov_at_150
        

def add_spacing(out):
    print(file=out)
    print(file=out)
        
def explain_misc_stats(out):
    explanation = {
        "Library complexity preseq PE": "Unique molecules at 300M read pairs, using preseq, identifying duplicates by paired-ends",
        "Library complexity ZTP PE": "library complexity using zero-trunacted poisson, identifying duplicates by paired-ends",
        "Raw coverage": "physical coverage of reads between 2 kb and 50 kb",
        "Model coverage": "physical coverage of reads between 2 kb and 50 kb as estimated by the model",
        "Raw coverage 150M": "raw coverage scaled to 150M read pairs. Does not estimate expected increase in duplicates",
        "Model coverage 150M": "model coverage scaled to 150M read pairs",
        "Raw junction percent": "% of read pairs with junctions",
        "Estimate junction percent": "estimated % of read pairs, based on read length and size selection",
        "Chimeric percent": "% of read pairs with junctions that map to different scaffolds",
        "Raw Coverage With Complexity 150M": "The expected coverage of about 150M read pairs, taking duplicates into account using preseq PE",
        "Model Coverage With Complexity 150M": "The expected model coverage of about 150M read pairs, taking duplicates into account using preseq PE" }
    definition_template = "{0}:\n\t{1}"
    for key, value in sorted(explanation.items()):
        print(definition_template.format(key, value), file=out)

def explain_bam_stats(out):
    text = """
    Each filter in the following table was applied sequentially. For example, reads that are both unmapped and duplicates
    will fall in the duplicates category. These are percentages of read pairs, so if 10% of forward reads are unmapped and 10% reverse reads are unmapped, then
    the percent of pairs that cannot be used and are in the unmapped category is approximately 19%."""
    print(text, file=out)

def format_key(key, percent=False):
    key = format_int(key)
    key = format_float(key, percent=percent)
    return str(key)

def format_int(key):
    if str(key).isdigit():
        return "{0:,}".format(int(key))
    return key
    
def format_float(key, percent=False):
    try:
        if percent:
            key = str(round(key*100, 3))+ "%"
        else:
            key = round(key, 3)
        return key
    except TypeError:
        return key

def misc_stats_keys():
    keys = ["chimeric_percent",
            "library_complexity_preseq_PE",
            "library_complexity_ZTP_PE",
            "raw_coverage",
            "model_coverage",
            "raw_coverage_150M",
            "model_coverage_150M",
            "raw_coverage_with_complexity_150M",
            "model_coverage_with_complexity_150M",
            "raw_junction_percent",
            "estimated_junction_percent"]

    return keys

def write_header(out):
    print(":Author:", getpass.getuser(), file=out)
    print(":Date:", datetime.datetime.now().strftime("%Y-%m-%d %H:%M"), file=out)
    print(":Type:", "Chicago QC Report", file=out)
    print(file=out)
    print(file=out)

def write_descriptions(projects, out):
    
    for lib in projects:
        definition_template = "{0} {1}:\n\t{2}"
        print(definition_template.format(lib["project"], lib["lib"], lib.get("description", None)), file=out)
    print(file=out)
    print(file=out)
    

def write_bam_stats(projects, out, keys, percent=False):
    all_bam_values = [["projects", "lib"] + keys]
    for lib in projects:
        try:
            bam_values = [lib["project"], lib["lib"]] + bam_stats(lib, keys, percent=percent)
        except KeyError:
            print(lib, file=sys.stderr)
            raise ComplexityError(lib)
        all_bam_values.append(bam_values)
    print(make_table(all_bam_values, invert=True), file=out)

def collect_metadata(jsn, library, prefix,all_chicago,lib_list):
    projects = []
    if all_chicago:
        if len(jsn) != 1:
            print("ERROR: only use all_chicago option with a single json file specified", file = sys.stderr)
            sys.exit(1)
        j = open(jsn[0],"r")
        load = json.load(j)
        library = [key for key in load["data"]["chicago"].keys() if "output" in load["data"]["chicago"][key]]
        jsn_list = [jsn[0] for i in library]
    elif lib_list:
        if len(jsn) != 1:
            print("ERROR: only use lib_list option with a single json file specified", file = sys.stderr)
            sys.exit(1)
        fh = open(lib_list,"r")
        library = []
        for line in fh:
            library.append(line.strip())
        jsn_list = [jsn[0] for i in library]
    else:
        jsn_list = jsn
    for jsn_file, lib in zip(jsn_list, sorted(library)):
        with open(jsn_file) as j:
            load = json.load(j)
      
        if prefix:
            load = load[prefix]
        info = load["general"]
            
        try:
            info = dict(list(info.items()) + list(dt_get_tree(load, "chicago|" + lib).items()))
        except AttributeError:
            print(jsn_file, lib, "does not contain the correct information")
        projects.append(info)
    return projects

def bam_stats(lib, keys, percent=False):
    values = []
    for key in keys:
        value = lib["output"]["bam"]["stats"][key]
        value = format_key(value, percent=percent)
        values.append(value)
    return values

def raw_bam_categories_keys():
    keys = ["duplicates",
            "unmapped",
            "low_mapq",
            "different_scaffold",
            "0_to_2kb_insert",
            "2kb_to_10kb_insert",
            "10kb_to_100kb_insert",
            "100kb_to_200kb_insert",
            "200kb_to_inf_insert",
            "total_read_pairs"]
    return keys


def add_fastqc(projects, out):
    bigdove1 = "http://192.168.89.252/"
    replace_link = "/projects/"
    
    for project in projects:
        lib = project["lib"]
        proj = project["project"]

        fastqc_r1 = project["output"]["fastqc_r1"]
        fastqc_r2 = project["output"]["fastqc_r2"]

        
        r1_start = fastqc_r1.index(replace_link) + len(replace_link)
        r2_start = fastqc_r2.index(replace_link) + len(replace_link)
        fastqc_r1_local = bigdove1 + fastqc_r1[r1_start:]
        fastqc_r2_local = bigdove1 + fastqc_r2[r2_start:]

        r1_text = "".join([proj, "_", lib, "_read1 fastqc"])
        r2_text = "".join([proj, "_", lib, "_read2 fastqc"])
        print("`", r1_text, "`_ `", r2_text, "`_\n\n", sep="", file=out)
        print(".. _", r1_text, ": " + fastqc_r1_local, "\n\n", sep="", file=out)
        print(".. _", r2_text, ": " + fastqc_r2_local, "\n\n", sep="", file=out)


        add_spacing(out)

def add_insert_distribution(projects, out):
    bigdove1 = "http://192.168.89.252/"
    replace_link = "/projects/"

    for project in projects:
        lib = project["lib"]
        proj = project["project"]
        

        
        inserts = project["output"]["inserts"]
        inserts_junction = project["output"]["inserts_junctions"]

        inserts_start = inserts.index(replace_link) + len(replace_link)
        inserts_junction_start = inserts_junction.index(replace_link) + len(replace_link)

        inserts_local = bigdove1 + inserts[inserts_start:]
        inserts_junction_local = bigdove1 + inserts_junction[inserts_junction_start:]

        print(proj, lib, "all read pair inserts\n", file=out)
        print(project["description"], "\n\n", file=out)
        print(".. image:: ", inserts_local, file=out)
        add_spacing(out)
        
        print(proj, lib, "junction-containing read pair inserts\n", file=out)
        print(project["description"], "\n\n", file=out)
        print(".. image:: ", inserts_junction_local, file=out)
        add_spacing(out)


def percent_bam_categories_keys():
    keys = ["duplicates_percent",
            "unmapped_percent",
            "low_mapq_percent",
            "different_scaffold_percent",
            "0_to_2kb_insert_percent",
            "2kb_to_10kb_insert_percent",
            "10kb_to_100kb_insert_percent",
            "100kb_to_200kb_insert_percent",
            "200kb_to_inf_insert_percent"]
    return keys

def write_contig_stats(projects, out, contigs="contigs"):
    keys = contig_keys()
    all_stats = [[contigs] + keys]
    for lib in projects:
        stats = [lib["assembly_name"]]
        for key in keys:
            val = lib["output"]["bam"]["contig_stats"][contigs][key]
            stats.append(format_key(val))
        all_stats.append(stats)
    print(make_table(all_stats, invert=True), file=out)
    

def contig_keys():
    keys = ["# contigs (>= 0 bp)",
            "# contigs (>= 1000 bp)",
            "Total length (>= 0 bp)",
            "Total length (>= 1000 bp)",
            "# contigs",
            "Largest contig",
            "Total length",  
            "GC (%)",
            "N50",
            "N75",
            "L50",
            "L75",
            "# N's per 100 kbp"]
    return keys
    

def dt_get_tree(jsn, key) :
    """ 
    Get the json subtree corresponding to key in first|second|third 
    hierarchy.
    """
    jj = jsn
    tokens = key.split('|');
    tokens.reverse();
    result = None;
    while tokens :
        pattern = tokens.pop();
        if pattern :
            jj = jsonfind(jj, pattern);
    return jj;

def jsonfind(d, pattern) :
    """ 
    Recurse through the tree looking for keys that
    match the specfied pattern/key. Return None
    if no keys match else return the value of the key
    that matches.
    """
    if isinstance(d, dict):
        for key in d.keys() :
            if key == pattern :
                return(d[key]);
            elif isinstance(d[key], dict) :
                value = jsonfind(d[key], pattern);
                if value != None :
                    return value;
    return None;

def make_table(grid, invert=False):
    if invert:
        grid = list(zip(*grid))
    max_cols = [max(out) + 5 for out in map(list, zip(*[[len(item) for item in row] for row in grid]))]
    rst = table_div(max_cols, 1)

    for i, row in enumerate(grid):
        header_flag = False
        if i == 0 or i == len(grid)-1:
            header_flag = True
        rst += normalize_row(row,max_cols)
        rst += table_div(max_cols, header_flag )
    return rst

def table_div(max_cols, header_flag=1):
    out = ""
    if header_flag == 1:
        style = "="
    else:
        style = "-"

    for max_col in max_cols:
        out += max_col * style + " "

    out += "\n"
    return out


def normalize_row(row, max_cols):
    r = ""
    for i, max_col in enumerate(max_cols):
        r += row[i] + (max_col  - len(row[i]) + 1) * " "

    return r + "\n"

def insert_plot(projects, output, junction=False, xlim=300000, log=False):
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)

    cm = plt.get_cmap("jet")
    num = len(projects) + 1
    cmap = [cm(1.*i/num) for i in range(num)][::-1]

    for i, lib in enumerate(projects):
        if junction:
            insert = lib["output"]["bam"]["junction_inserts"]    

        else:
            insert = lib["output"]["bam"]["all_inserts"]    

        label = lib["project"] + " " + lib["lib"]
        color = cmap[i+1]

        x, y = insert_counts(insert)
        total = sum([y[i] for i in range(len(y)) if x[i] < xlim])

        y = [i/total for i in y]
        ax.plot(x, y, "o", color=color, label=label, markeredgecolor=color)
    
    if junction:
        title = "Junction Read Pairs"
    else:
        title = "All Read Pairs"

    ax.set_title(title)
    ax.set_xlim([1, xlim])
    ax.set_yscale("log")
    if log:
        ax.set_xscale("log")
    plt.xlabel("Read Pair Separation (bp)")
    plt.ylabel("P(separation)")

    ax.legend(loc=9, ncol=2, bbox_to_anchor=(0.5, -0.1))

    with open(output, 'w') as out:
        plt.savefig(out, bbox_inches="tight")

def insert_counts(insert):
    all_x, all_y = [], []
    with open(insert) as f:
        for line in f:
            s = line.split()
            x = float(s[0])
            y = float(s[1])
            all_x.append(x)
            all_y.append(y)
    return all_x, all_y

def preseq_plot(projects, complexity, xlim=200000000):
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    
    cm = plt.get_cmap("jet")
    num = len(projects) + 1
    cmap = [cm(1.*i/num) for i in range(num)][::-1]

    for i, lib in enumerate(projects):
        c_curve = lib["output"]["bam"]["c_curve"]    
        lc_extrap = lib["output"]["bam"]["lc_extrap_paired"]    
        label = lib["project"] + " " + lib["lib"]
        color = cmap[i+1]

        
        x, y = preseq_counts(c_curve)
        ax.plot(x, y, "-", color=color, label=label)

        x, y = preseq_counts(lc_extrap)
        ax.plot(x, y, '--', color=color)

    ax.set_xlim([0, xlim])
    ax.set_ylim([0, xlim])
    locs, labels = plt.xticks()
    plt.xticks(locs, list(map(lambda x: "{0:.0f}".format(x), locs/1000000)))
    locs, labels = plt.yticks()
    plt.yticks(locs, list(map(lambda x: "{0:.0f}".format(x), locs/1000000)))

    plt.xlabel("Read Pairs Sequenced (M)")
    plt.ylabel("Unique Read Pairs (M)")
    plt.title("Library complexity")

    
    plt.plot(range(0, xlim, 1000000), range(0, xlim, 1000000), color=cmap[0])

    ax.legend(loc=9, ncol=2, bbox_to_anchor=(0.5, -0.1))

    with open(complexity, 'w') as out:
        plt.savefig(out, bbox_inches="tight")

def add_image(afile, out):
    print(".. figure::", afile, file=out)

def preseq_counts(count_file):
    x_counts = []
    y_counts = []
    with open(count_file) as f:
        f.readline()
        for line in f:
            s = list(map(float, line.split()))
            x = s[0]
            y = s[1]
            x_counts.append(x)
            y_counts.append(y)

    return x_counts, y_counts


def write_coverage_with_complexity(projects, out, skip_bad_preseq):
    # projects
    #    library
    #         output
    #             bam
    #                stats
    #                        raw_coverage
    #                        total_reads
    #                lc_extrap

    for library in projects:
        chicago_coverage = library["output"]["bam"]["stats"]["raw_coverage"]
        total_reads = library["output"]["bam"]["stats"]["total_read_pairs"]
        total_reads_unique = total_reads - library["output"]["bam"]["stats"]["duplicates"]
        unique_mapped_reads = total_reads_unique - library["output"]["bam"]["stats"]["low_mapq"] - library["output"]["bam"]["stats"]["unmapped"]

        normalized_coverage = chicago_coverage/unique_mapped_reads
        lc_extrap = library["output"]["bam"]["lc_extrap_paired"]

        step = 10000000
        maximum = 400000000

        lib_name = '_'.join([library["project"], library["lib"]])
        lib_table = [[lib_name, "estimated_coverage", "lower_95", "upper_95"], [format_key(total_reads), format_key(chicago_coverage), "NA", "NA"]]
        try:
            coverage_by_complexity = get_coverage_by_complexity(chicago_coverage, unique_mapped_reads, total_reads_unique, lc_extrap, list(range(step, maximum, step)))
            lib_table = lib_table + coverage_by_complexity
            print(make_table(lib_table, invert=False), file=out)
        except ComplexityError:
            if skip_bad_preseq:
                pass

def get_coverage_by_complexity(chicago_coverage, unique_mapped_reads, total_reads_unique, lc_extrap, num_sequenced):
    unique_reads = []
    with open(lc_extrap) as lc_extrap_handle:
        lc_extrap_handle.readline()
        i = 0
        for line in lc_extrap_handle:
            s = line.split()
            s_int = map(int, map(float, s))
            s_int = list(s_int)
            
            if s_int[0] == num_sequenced[0]:
                coverage = [chicago_coverage * i /unique_mapped_reads for i in s_int[1:]]
                this_coverage = [int(round(s_int[0] * total_reads_unique / unique_mapped_reads, -6))] + coverage
                this_coverage = [format_key(i) for i in this_coverage]
                unique_reads.append(this_coverage)
                num_sequenced = num_sequenced[1:]

                if not num_sequenced:
                    return unique_reads
        if len(num_sequenced) != 0:
            raise ComplexityError(lc_extrap)
    
    return unique_reads


class ComplexityError(Exception):
    pass
    

if __name__ == "__main__":
    argh.dispatch_command(main)
