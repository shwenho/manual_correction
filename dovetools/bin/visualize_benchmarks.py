#!/usr/bin/env python

import argh
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import pysam
import sys
import os
import json
import numpy



@argh.arg("benchmark_files", nargs="+")
def main(benchmark_files, output="benchmarks.png"):
    data = read_data(benchmark_files)
    print("hours:", sum(data.values()), file=sys.stderr)
    plot(data, output)

def plot(data, output):
    width = 0.5

    keys = sorted(data.keys(), key=lambda x: data[x], reverse=True)[:20]
    values = [data[i] for i in keys]
    indexes = numpy.arange(len(keys))
    plt.bar(indexes, values, width=width)


    plt.xticks(indexes + width/2, keys, rotation="vertical")
    plt.xlabel("step")
    plt.ylabel("hours")
    plt.tight_layout()
    plt.savefig(output)
    

def read_data(benchmark_files):
    data = {}
    for benchmark_file in benchmark_files:
        with open(benchmark_file) as benchmark_handle:
            this_key = os.path.basename(benchmark_file).rstrip(".json")
            this_data = json.load(benchmark_handle)["wall_clock_times"]["s"][0]

            parent_dir = os.path.dirname(benchmark_file)
            parent_parent_dir = os.path.dirname(parent_dir)


            if os.path.basename(parent_parent_dir) == "benchmarks":

                if this_key[0].isdigit() and this_key[1] == ".":

                    key = "_".join([os.path.basename(os.path.dirname(parent_parent_dir)), os.path.basename(parent_dir), "iter", this_key[0]])
                else:
                    key = "_".join([os.path.basename(os.path.dirname(parent_parent_dir)), os.path.basename(parent_dir)])
            else:
                key = "_".join([os.path.basename(os.path.dirname(parent_dir)), this_key])
                
            data[key] = this_data / 3600
    
    for key in list(data.keys()):
        if "_iter" in key:
            index = key.find("_iter")
            new_key = key[:index] + key[index+7:]
            data.setdefault(new_key, 0)

            data[new_key] += data[key]
            del data[key]

    return data


if __name__ == "__main__": 
    argh.dispatch_command(main)
