#!/usr/bin/env python3


import argh
import pysam
from collections import Counter

def main(bam, output="/dev/stdout", mapq=20, single=False, debug=False):
    histogram = get_duplicate_counts(bam, mapq, single, debug)
    with open(output, 'w') as output_handle:
        maximum = max(histogram.keys())
        maximum2 = max(maximum, 4)
        for i in range(1, maximum2+1):
            if i <=4 and histogram[i] < 1:
                histogram[i] = 1
            print(i, histogram[i], file=output_handle, sep="\t")

def get_duplicate_counts(bam, mapq, single, debug):
    counts = Counter()
    for identifier in get_identifiers(bam, mapq, single):
        counts[identifier] += 1
        if debug:
            print(identifier, counts[identifier])

    histogram = Counter([value for value in counts.values()])
    return histogram


def get_identifiers(bam, mapq, single):

    with pysam.AlignmentFile(bam, 'rb') as bam_handle:
        prev_id = None
        reads_with_same_id = []
        for aln in bam_handle:
            if aln.is_read2:
                continue
            if aln.is_unmapped or aln.mate_is_unmapped:
                    continue
            if aln.mapq < mapq or aln.opt("MQ") < mapq:
                continue
            if single:
                ident = get_identifier_se(aln)
            else:
                ident = get_identifier_pe(aln)
            yield ident


def get_identifier_se(aln):
    pos1 = aln.reference_start
    ref1 = aln.reference_id
    identifier = (ref1, pos1)
    return identifier


def get_identifier_pe(aln):
    pos1, pos2 = aln.reference_start, aln.next_reference_start
    ref1, ref2 = aln.reference_id, aln.next_reference_id
    identifier = (ref1, pos1, ref2, pos2)
    return identifier

if __name__ == "__main__":
    argh.dispatch_command(main)
