# takes best hits from mbChain.py and outputs lines representing semiglobal alignments
import fileinput

if __name__ == "__main__":
    for line in fileinput.input():
        tolerance = 100
        _, _, _, _, sign, t_start, t_end, q_start, q_end, q_len, t_len = line.split()

        t_start = int(t_start)
        t_end = int(t_end)
        t_len = int(t_len)
        q_start = int(q_start)
        q_end = int(q_end)
        q_len = int(q_len)

        if sign == '-':
            aln_len = q_end - q_start + 1
            q_end = q_len - q_start + 1
            q_start = q_end - aln_len

        line = line.strip()
        # q to right of t
        if q_start < tolerance and (t_len - t_end) < tolerance:
            print(line)
            continue
        # q to left of t
        if (q_len - q_end) < tolerance and t_start < tolerance:
            print(line)
            continue
        # q fully in t
        if (q_len - (q_end - q_start + 1)) < tolerance:
            print(line)
            continue
        # t fully in q
        if (t_len - (t_end - t_start + 1)) < tolerance:
            print(line)
            continue
