#!/usr/bin/env python3

"""
Author:          Jonathan Stites
Date:            03 April 2015
Company:         Dovetail Genomics

Description:
Takes a Chicago bam file and outputs statistics that can be used for
QC. 

Usage:
bam_to_statistics.py -c coverage.txt -i 500 -b human.bam --assembly hg19.json --output human.json "


"""
import sys
import argh
import json
import pysam
from dovetools.utils import BamTags

def main(bam: "chicago bam file" = None, 
         mapq: "map quality threshold" = 20, 
         genome_size: "Genome size" = 3000000000,
         mapped_only:"Calculate stats only for mapped reads" = False,
         output: "output file" = "/dev/stdout"): 


    keys = [
        "raw_junctions",
        "mapped_junctions",
        "mapped_chimeras",
        "duplicates",
        "unmapped",
        "low_mapq",
        "different_scaffold",
        "0_to_2kb_insert",
        "2kb_to_10kb_insert", 
        "10kb_to_100kb_insert", 
        "100kb_to_200kb_insert",
        "200kb_to_inf_insert",
        "duplicates_percent",
        "unmapped_percent",
        "low_mapq_percent",
        "different_scaffold_percent",
        "0_to_2kb_insert_percent",
        "2kb_to_10kb_insert_percent", 
        "10kb_to_100kb_insert_percent", 
        "100kb_to_200kb_insert_percent",
        "200kb_to_inf_insert_percent",
        "total_read_pairs",
        "raw_coverage",
        "model_coverage",
        "raw_coverage_150M",
        "model_hiseq_coverage",
        "raw_junction_percent",
        "estimated_junction_percent",
        "chimeric_percent"
    ]

    
    statistics = {key: 0 for key in keys}
        
    parse_bam_file(bam, statistics, mapq=mapq, genome_size=genome_size)
    update_bam_percents(statistics,mapped_only)
    estimate_uncorrected_hiseq_coverage(statistics)

    uncorrected_junction_percent(statistics)

    read_length = get_read_length(bam)
    chimeric_percent(statistics)

    if output:
        with open(output, 'w') as outfile:
            json.dump(statistics, outfile, sort_keys=True, indent=4)


def parse_bam_file(bam, statistics, mapq, genome_size):
    """Takes as input a bam file and returns several statistics"""
    
    # Total number of read pairs (only counts read1)
    total = 0

    # Raw count of non-duplicate read pairs with junctions
    raw_junctions = 0

    # Count of non-duplicate, mapped reads (> mapq) with junctions and same scaffold
    mapped_junctions = 0

    # Count of non-duplicate, mapped reads (> mapq) with junctions and different scaffold
    mapped_chimeras = 0


    t = 0
    with pysam.AlignmentFile(bam, "rb") as bam_file:
        for line in bam_file:

            update_total(line, statistics)
            update_raw_junctions(line, statistics)
            update_chimeric(line, statistics, mapq)
            update_read_category(line, statistics, mapq)
            update_uncorrected_coverage(line, statistics, mapq)


    statistics["raw_coverage"] = statistics["raw_coverage"] / genome_size


def update_bam_percents(statistics,mapped_only=False):
    keys = ["duplicates",
            "unmapped",
            "low_mapq",
            "different_scaffold",
            "0_to_2kb_insert",
            "2kb_to_10kb_insert", 
            "10kb_to_100kb_insert", 
            "100kb_to_200kb_insert",
            "200kb_to_inf_insert"]
    for key in keys:
        percent_key = key + "_percent"
        if mapped_only:
            percent_value = statistics[key] / (statistics["total_read_pairs"] - statistics["unmapped"])
        else:
            percent_value = statistics[key] / statistics["total_read_pairs"]
        statistics[percent_key] = percent_value

def parse_coverage_quantiles(coverage_quantiles, statistics):
    """Takes as input a coverage quantile file created with 
    coverageQuantiles.py  and returns the parsed data. This is physical 
    coverage, not read coverage."""

    with open(coverage_quantiles) as c:
        for line in c:
            s = line.split()
            cov = float(s[0])
            min_cov = float(s[6])
            max_cov = float(s[8])
    statistics["model_coverage"] = cov
    statistics["min_length_for_coverage"] = min_cov
    statistics["max_length_for_coverage"] = max_cov
    
def uncorrected_junction_percent(statistics):
    junctions = statistics["raw_junctions"]
    total = statistics["total_read_pairs"]
    statistics["raw_junction_percent"] = junctions/total

def corrected_junction_percent(statistics, fragment_size, read_length):
    junctions = statistics["raw_junctions"]
    total = statistics["total_read_pairs"]
    try:
        factor = int(fragment_size) / (2 * read_length )
    except ValueError:
        statistics["estimated_junction_percent"] = "NA"
        return
    # Don't want to go below 1
    factor = max(factor, 1)
    estimated = min(factor * junctions/total, 1)
    statistics["estimated_junction_percent"] = estimated

def chimeric_percent(statistics):
    mapped_chimeric = statistics["mapped_chimeras"]
    mapped_junctions = statistics["mapped_junctions"]
    try:
        statistics["chimeric_percent"] = mapped_chimeric / (mapped_chimeric + mapped_junctions)
    except ZeroDivisionError:
        statistics["chimeric_percent"] = 0


def estimate_uncorrected_hiseq_coverage(statistics):
    uncorrected_coverage = statistics["raw_coverage"]
    total = statistics["total_read_pairs"]
    hiseq_coverage = uncorrected_coverage * 150000000 / total
    statistics["raw_coverage_150M"] = hiseq_coverage

def update_total(line, statistics):
    """Takes as input a pysam alignment and a integer of the
    total read pairs and return the total incremented by one."""

    if not line.is_read2:
        statistics["total_read_pairs"] += 1

def update_raw_junctions(line, statistics):
    """Takes as input a pysam alignment and a dictionary with raw_junctions,
    and increments the count if the read passes filters and had a junction."""

    if line.is_read2:
        return
    if line.is_duplicate:
        return
    if BamTags.junction(line) == "F":
        return
    statistics["raw_junctions"] += 1

def update_chimeric(line, statistics, mapq):
    """Takes as input a pysam alignment and a dictionary with mapped
    reads with junctions, and mapped reads to different scaffolds with 
    junctions. Returns the integers, incremented if necessary"""

    if line.is_read2:
        return
    if line.is_duplicate:
        return
    if BamTags.junction(line) == "F":
        return
    if line.mapping_quality < mapq or BamTags.mate_mapq(line) < mapq:
        return

    if BamTags.orientation(line) == "D":
        statistics["mapped_chimeras"] += 1
    else:
        statistics["mapped_junctions"] += 1
    

def update_read_category(line, statistics, mapq):
    """Takes as input a pysam alignment and a dictionary and increments
    "duplicates", "unmapped", "mapq", "different_scaffold", "0_to_2kb_insert",
    "2kb_to_10kb_insert", "10kb_to_100kb_insert", "100kb_to_200kb_insert",
    "200kb_to_inf_insert" and increments their counts as necessary."""

    if line.is_read2:
        return
    if line.is_duplicate:
        statistics["duplicates"] += 1
        return
    if line.is_unmapped or line.mate_is_unmapped:
        statistics["unmapped"] += 1
        return
    if line.mapping_quality < mapq or BamTags.mate_mapq(line) < mapq:
        statistics["low_mapq"] += 1
        return
    if line.opt("xt") == "D":
        statistics["different_scaffold"] += 1
        return
        
    insert = abs(line.reference_start - line.next_reference_start)
    
    if insert < 2000:
        statistics["0_to_2kb_insert"] += 1
        return
    
    if insert < 10000:
        statistics["2kb_to_10kb_insert"] += 1
        return

    if insert < 100000:
        statistics["10kb_to_100kb_insert"] += 1
        return
        
    if insert < 200000:
        statistics["100kb_to_200kb_insert"] += 1
        return

    if insert >= 200000:
        statistics["200kb_to_inf_insert"] += 1
        return


def update_uncorrected_coverage(line, statistics, mapq, cov_min=2000, cov_max=50000):
    """Takes as input a pysam alignment and a dictionary, and updates the estimated 
    physical coverage for reads 2000kb to 50000kb. This is not corrected for contiguity."""

    if line.is_read2:
        return

    if line.is_duplicate:
        return

    if line.mapping_quality < mapq or BamTags.mate_mapq(line) < mapq:
        return

    if line.reference_id != line.next_reference_id:
        return
        
    insert = abs(line.reference_start - line.next_reference_start)

    if insert < cov_min or cov_max < insert:
        return

    statistics["raw_coverage"] += insert

def get_genome_size(assembly):
    with open(assembly) as f:
        data = json.load(f)
        return data["scaffolds"]["Total length"]

def get_read_length(bam, read_num = 100000):

    longest = 0
    with pysam.AlignmentFile(bam, 'rb') as bam_handle:
        for i, aln in enumerate(bam_handle):
            if i > read_num:
                break
            l = len(aln.query_sequence)
            if l > longest: 
                longest = l
            
    return longest

if __name__ == "__main__":
    argh.dispatch_command(main)
