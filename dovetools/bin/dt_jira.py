#!/usr/bin/env python3

import argparse
import sys
import os

import yaml
from jira import JIRA

def parse_args():
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--issue', '--id', action = 'store',
        dest='issue', 
        help=("Specifies the JIRA ID of the issue to query or update."))
    argparser.add_argument('--fields', '-f', action = 'store',nargs="+",
        help=("Specifies fields to return as comma-separated string."))
    argparser.add_argument('--attachment','-a',action='store',
        help=("Specifies a file to add as an attachment."))
    argparser.add_argument('--jql',action='store',
        help=("Specifies a JQL query to search for issues."))
    argparser.add_argument('--config',action='store',type=argparse.FileType('r'),
        default=os.path.expanduser("~/.jira/config.yml"),
        help=("Specifies config yaml file,defaults to ~/.jira/config.yml")) 
    options = argparser.parse_args()
    return options


def get_name2id(jira_handle):
    fields = jira_handle.fields()
    name2id = {x["name"]:x["id"] for x in fields}
    return name2id

def translate_fields(fields,name2id):
    translated = []
    err = []
    for field in fields:
        if field in name2id:
            translated.append(name2id[field])
        elif field.title() in name2id:
            translated.append(name2id[field.title()])
        else:
            err.append(field)
    if err:
        print("Didn't know how to handle the following fields: %s" % ("\t".join(err)),file=sys.stderr)
    return translated

def get_issue_list(jh,jql,fields,name2id):
    translated = translate_fields(fields,name2id)
    found = jh.search_issues(jql,fields=",".join(translated))
    if len(found) == 0:
        print("No issues matching the specified JQL query were found.")
    else:
        print("%d issues" % len(found))
        print("|".join(["JIRA ID"] + fields))
        for issue in found:
            parts = [issue.key] 
            for field in translated:
                parts.append(eval("issue.fields.%s" % (field)))
            print("|".join(parts))

def get_fields(jh,issue,fields,name2id):
    translated = translate_fields(fields,name2id)
    response = jh.issue(id=issue,fields=",".join(translated))
    parts = []
    for iden in translated:
        parts.append(eval("response.fields.%s" % (iden)))
    return "|".join(map(str,parts))

def add_attachment(jh,issue,attachment):
    if not os.path.exists(attachment):
        print("Specified path does not exist: %s" % (attachment),file=sys.stderr)
        sys.exit(1)
    if not os.path.isfile(attachment):
        print("Specified path is not a file: %s" % (attachment),file=sys.stderr) 
        sys.exit(1)
    jh.add_attachment(issue,attachment)


def main(args):
    opt = parse_args()
    conf = yaml.load(opt.config) 
    authed = JIRA("https://dovetail-genomics.atlassian.net",basic_auth=(conf["user"],conf["key"]))
    name2id = get_name2id(authed)
    if opt.fields and opt.issue:
        fields = get_fields(authed,opt.issue,opt.fields,name2id)
        print(fields)
    if opt.attachment:
        add_attachment(authed,opt.issue,opt.attachment)
    if opt.jql:
        get_issue_list(authed,opt.jql,opt.fields,name2id)

if __name__ == "__main__":
    sys.exit(main(sys.argv))
