#!/usr/bin/env python3

import argparse
import os
import sys
import re
import json
import gzip
import bz2
import subprocess
import functools
from glob import glob
from collections import defaultdict,namedtuple

import snakemake

def new_dir(path):
    path = os.path.expanduser(path)
    if os.path.exists(path) and not os.path.isdir(path):
        raise Exception("ERROR: Can't create directory, file with that "
                        "name already exists: %s" % (path) )
    if not os.path.exists(path):
        os.mkdir(path)
    return os.path.abspath(path)
    
def exist(path,directory=False):
    path = os.path.expanduser(path)
    if not os.path.exists(path):
        raise Exception("ERROR: Specified path does not exist: %s" % (path))
    if directory: 
        if not os.path.isdir(path):
            raise Exception("ERROR: Specified path is not a directory: %s" % (path))
    elif not os.path.isfile(path):
        raise Exception("ERROR: Specified path is not a file: %s" % (path))
    return os.path.abspath(path)

exist_file = functools.partial(exist,directory=False)
exist_dir  = functools.partial(exist,directory=True)

def parse_args():
   # Everybody's favorite function...
   parser = argparse.ArgumentParser()
   
   # data input options
   data_inputs = parser.add_mutually_exclusive_group(required=True)
   data_inputs.add_argument('-d','--data-dir',nargs='+',type=exist_dir,
           help="Directory containing many FASTQ file pairs.")
   data_inputs.add_argument('-r','--reads',nargs=2,
           help="Specify two globs, one for read 1s and one for read 2s.") 
   data_inputs.add_argument('-s','--sequencing-dir',nargs='+',type=exist_dir,
           help="Directory corresponding to an Illumina sequencing run.")

   # assembly input options
   parser.add_argument('-f','--fasta',type=exist_file,
           default="/mnt/nas/projects/human/results/hg19/hg19.fa",
           help="FASTA file of assembly to use for QC.")
   parser.add_argument('--snap-index',type=exist_dir,
           default="/mnt/nas/projects/human/results/hg19/snap_index_16",
           help="SNAP index to use for alignments for QC.")

   #output options
   parser.add_argument('-w','--work-dir',default=".",type=new_dir,
           help="Working directory for QC runs.")
   parser.add_argument('-o','--output',default='report.html',
           help = "Name for HTML QC report.")

   #bcl2fastq options
   parser.add_argument('-x','--bcl-options', default = "",
           help = "Additional arguments to pass to bcl2fastq. e.g. "
                  "-x '--barcode-mismatches 0'")
   parser.add_argument('--no-bcl', action = 'store_true',
           help = "Don't run bcl2fastq for a 'sequencing_dir' input.")
   parser.add_argument('---bcl-path', default = "bcl2fastq", 
           help = "Path to bcl2fastq executable.")

   #misc config args
   parser.add_argument('-n','--dryrun',action='store_true',
           help = "Just parse libraries and print IDs and read files "
                  "without actually running QC pipelines.")
   parser.add_argument('--snakefile',
           help = "Path to QC snakefile."
                  "Defaults to ~/dovetail/snake/snakefile.chicago.local "
                  " or ~/shotgun_qc/snake/snakefile.shotgun.qc if "
                  " --shotgun is specified.")
   parser.add_argument('-t','--threads',type=int, default = 16,
           help = "Number of threads to use for QC.")
   parser.add_argument('--shotgun',action='store_true',
           help = "Run shotgun QC instead of chicago QC.")

   args = parser.parse_args()
   return args

class Library:
    def __init__(self,name,read1s,read2s):
        self._name = name
        self._r1 = read1s
        self._r2 = read2s

    @classmethod
    def from_globs(cls,name,glob1,glob2):
        r1 = glob(os.path.abspath(glob1))
        r2 = glob(os.path.abspath(glob2))

        if len(r1) == 0:
            raise Exception("ERROR: No files matching pattern: %s" % (glob1))
        if len(r2) == 0:
            raise Exception("ERROR: No files matching pattern: %s" % (glob2))
        if len(r1) != len(r2):
            raise Exception("ERROR: Mismatch between specified number of read 1 "
                            "and read 2 files specified.")

        return cls(name,r1,r2)

    @classmethod
    def from_dir(cls,data_dir,samples = None):
        r1_keys = ["r1","R1"]
        r2_keys = ["r2","R2","R3"]
        fastq_ext = ["fq", "fastq", "fq.gz", "fastq.gz", "fq.bz2","fastq.bz2"]

        temp_libs = defaultdict(lambda : defaultdict(list))
        for entry in os.listdir(data_dir):
            if os.path.isdir(os.path.join(data_dir,entry)): continue
            if not any([entry.endswith(ext) for ext in fastq_ext]):
                continue
            
            if samples:
                for sample in samples:
                    if entry.startswith(sample + "_S"):
                        lib_id = sample
                        break
                else:
                    continue
            else:
                lib_id = cls.get_lib_name(entry)

            abs_path = os.path.abspath(os.path.join(data_dir,entry))
            this_read_pair = cls.parse_read_pair(abs_path)
            if this_read_pair == "1" or \
               (this_read_pair is None and any([r1 in entry for r1 in r1_keys])):
                temp_libs[lib_id]["r1"].append(abs_path)
            elif this_read_pair == "2" or \
               (this_read_pair is None and any([r1 in entry for r1 in r1_keys])):
                temp_libs[lib_id]["r2"].append(abs_path)

        for name in temp_libs:
            r1 = temp_libs[name]["r1"]
            r2 = temp_libs[name]["r2"]
            if len(r1) != len(r2):
                print("Warning: Did not find same number of read1 and read2 files "
                      "for library: %s" % (name),file=sys.stderr)

            yield cls(name,r1,r2)

    @staticmethod
    def get_lib_name(path):
        base = os.path.basename(path)
        split = base.find("_") if base.find("_") != -1 else base.find(".")
        return base[:split]

    @staticmethod
    def parse_read_pair(entry):
        if entry.endswith(".bam"): return
        if entry.endswith(".gz"):
            open_type = gzip.open
            open_args = {"mode":"rt"}
        elif entry.endswith(".bz2"):
            open_type = bz2.open
            open_args = {"mode":"rt"}
        else:
            open_type = open
            open_args = {}
        with open_type(entry,**open_args) as f:
            try:
                line = f.readline()
                comment_section = line.split()[1]
                read_type = comment_section.split(":")[0]
                if read_type == "1" or read_type == "2":
                    return read_type
                else:
                    return
            except IndexError:
                return

    def __str__(self):
        return "Library id: {0}\nRead 1s: {1}\nRead 2s: {2}\n".format(
                    self._name, ", ".join(self._r1), ", ".join(self._r2))


class QCRunner:
    def __init__(self,
                 libs,
                 shotgun,
                 snakefile,
                 fasta,
                 index):
        self._libs      = libs
        self._shotgun   = shotgun
        self._snakefile = snakefile
        self._fasta     = fasta
        self._index     = index

    def run_qc(self,
               workdir,
               threads = 16,
               dryrun = False):
        if self._shotgun:
            targets = None
            result = workdir
        else:
            targets = ["results.json"]
            result  = os.path.join(workdir,"results.json")
        success = snakemake.snakemake(self._snakefile,
                                      targets        = targets, 
                                      cores          = threads,
                                      workdir        = workdir,
                                      dryrun         = dryrun,
                                      printreason    = True,
                                      printshellcmds = True,
                                      timestamp      = True)
        return result
        

    def qc_libs(self,
                workdir,
                threads,
                dryrun):
        config = {}
        config["contigs"]       = self._fasta
        config["assembly_name"] = os.path.splitext(os.path.basename(self._fasta))[0]
        config["junction"]      = "GATCGATC"
        config["cores"]         = threads 
        config["project"]       = "Development"

        if self._shotgun:
            config["snap_index_shotgun"] = self._index
        else:
            config["snap_index_chicago"] = self._index
            config["junction_check"] = "False"

        results = []

        for lib in self._libs:
            this_dir = os.path.join(workdir,lib._name)
            conf_path = os.path.join(this_dir,"config.json")
            if not os.path.exists(this_dir):
                os.mkdir(this_dir)

            config["raw_regex_r1"] = "|".join(lib._r1)
            config["raw_regex_r2"] = "|".join(lib._r2)
            config["lib"] = lib._name

            json.dump(config,open(conf_path,"w"))
            result_json = self.run_qc(this_dir,threads,dryrun)
            results.append(result_json)
        return results


def generate_report(output,results):
    cmd = "chicago_report.py -o {0} -r {1}".format(output,
                                                   ' '.join(results))
    subprocess.check_call(cmd,shell=True)

                

def parse_sample_sheet(seq_dir):
    ss_path = os.path.join(seq_dir,"SampleSheet.csv")
    if not os.path.exists(ss_path):
        raise Exception("ERROR: No SampleSheet.csv found in the sequencing "
                        "directory: %s" % (seq_dir))
    samples = []
    fh = open(ss_path)

    #skip to beginning of library lines
    for line in fh:
        if line.startswith("Sample_ID,"):
            break

    for line in fh:
        samples.append(line.split(",")[0])
    return samples

def get_libraries(args):
    libs = []
    
    if args.data_dir:
        for ddir in args.data_dir:
            for lib in Library.from_dir(ddir):
                libs.append(lib)

    elif args.reads:
        libs.append(Library.from_globs("lib_001",*args.reads))

    elif args.sequencing_dir:
        for sdir in args.sequencing_dir:
            samples = parse_sample_sheet(sdir)
            if not args.no_bcl:
                run_bcl2fastq(sdir,
                              args.bcl_path,
                              args.bcl_options)

            fastq_dir = os.path.join(sdir,"fastqs")
            for lib in Library.from_dir(fastq_dir,samples):
                libs.append(lib)

    return sorted(libs, key = lambda x: x._name)


def run_bcl2fastq(sdir,bcl_path,options):
    run_dir = os.path.abspath(sdir)
    out_dir = os.path.join(run_dir,"fastqs")
    cmd = "{0} -R {1} -o {2} {3}".format(bcl_path,
                                         run_dir,
                                         out_dir,
                                         options) 
    subprocess.check_call(cmd,shell=True)

def check_snakefile(args):
    if not args.snakefile:
        if args.shotgun:
            args.snakefile = os.path.expanduser("~/shotgun_qc/snake/snakefile.shotgun.qc")
        else:
            args.snakefile = os.path.expanduser("~/dovetail/snake/snakefile.chicago.local")
    exist_file(args.snakefile)

def main(args):
    args = parse_args()
    check_snakefile(args)
    
    libs = get_libraries(args)
    runner = QCRunner(libs,
                      args.shotgun,
                      args.snakefile,
                      args.fasta,
                      args.snap_index)

    results = runner.qc_libs(args.work_dir,
                             args.threads,
                             args.dryrun)

    if args.dryrun:
        for lib in runner._libs:
            print(lib)
    elif not args.shotgun:
        generate_report(args.output,results)


if __name__ == "__main__":
   main(sys.argv)
