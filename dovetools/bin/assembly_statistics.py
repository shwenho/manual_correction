#!/usr/bin/env python3

"""
Author:     Jonathan Stites
Date:       03 April 2015
Company:    Dovetail Genomics


Takes as input a fasta file or gzipped fasta file and outputs statistics
in json format about the contiguity of the assembly contigs and assembly 
scaffolds.

Options:
-a, --assembly       fasta or gzipped-fasta
-o, --output         output file

Usage:
assembly_statistics.py -a hg19.fa -o hg19_stats.json

Notes:
Detects gzip fasta by extension. For contig statistics,
breaks scaffolds at runs of 10 or more consecutive Ns. By default,
statistics are based on contigs/scaffolds with over 500 bases.
"""


import argh
import json
import gzip
from collections import defaultdict
from collections import Counter


def main(assembly=None, output="/dev/stdout", mask_invalid=True, min_size=0,contig_lengths=None):
    """Read fasta file, collect some metrics, output in json format"""

    stats = {}
    gc, at, n, lengths, con_gc, con_at, con_n, con_lengths, num_gaps = read_assembly(assembly, mask_invalid, min_size)
    stats["scaffolds"] = get_stats(lengths, gc, at, n, min_size)
    stats["contigs"] = get_stats(con_lengths, con_gc, con_at, con_n, min_size)
    stats["num_gaps"] = num_gaps
    total_lengths = sum(lengths)
    stats["percent_gaps"] = (round(100*(total_lengths-sum(con_lengths))/float(total_lengths),3))

    with open(output, 'w') as out:
        json.dump(stats, out, indent=4, sort_keys=True)

    if contig_lengths:
        write_contig_lengths(con_lengths,contig_lengths)

def read_assembly(assembly, mask_invalid, min_size):
    """Parse the fasta, collect counts of the bases for scaffolds and contigs,
    and return those metrics"""
    bases = Counter()
    contig_bases = Counter()
    num_gaps = 0    
    lengths = []
    con_lengths = []
    for header, seq in read_fasta(assembly):
        count_bases(seq, bases, min_size)
        lengths.append(len(seq))

        for contig,gap in break_scaffold(seq):
            count_bases(contig, contig_bases, min_size, scaffolds=False)

            con_lengths.append(len(contig))
            if gap:
                num_gaps += 1

    gc, at, n = get_gc(bases, mask_invalid)
    con_gc, con_at, con_n = get_gc(contig_bases, mask_invalid)

    return gc, at, n, lengths, con_gc, con_at, con_n, con_lengths,num_gaps

def get_stats(lengths, gc, at, n, min_size):
    """Take info about the lengths of scaffolds / contigs, and counts of GC, AT,
    and N bases, return metrics"""
    stats = {}
    stats["# contigs (>= 0 bp)"] = len(lengths)
    stats["# contigs (>= 1000 bp)"] = sum(1 for i in lengths if i >= 1000)
    stats["Total length (>= 0 bp)"] = sum(lengths)
    stats["Total length (>= 1000 bp)"] = sum(i for i in lengths if i >= 1000)
    stats["# contigs"] = sum(1 for i in lengths if i >= min_size)
    stats["Largest contig"] = max(lengths)
    stats["Total length"] = sum(i for i in lengths if i >= min_size)
    try:
        stats["GC (%)"] = round(gc*100/(gc+at), 2)
    except ZeroDivisionError:
        stats["GC (%)"] = 0.0
    n50, l50 = nx(lengths, .5, min_size)
    n75, l75 = nx(lengths, .75, min_size)
    n90, l90 = nx(lengths, .9, min_size)
    stats["N50"] = n50
    stats["N75"] = n75
    stats["N90"] = n90
    stats["L50"] = l50
    stats["L75"] = l75
    stats["L90"] = l90
    try:
        stats["# N's per 100 kbp"] = round(n*100000/ sum([i for i in lengths if i >= min_size]), 2)
    except ZeroDivisionError:
        stats["# N's per 100 kbp"] = 0.0
    return stats

def read_fasta(assembly):
    """Parse the fasta, yield header and sequence"""
    if assembly.endswith(".gz"):
        read_mode = "rb"
        open_cmd = gzip.open
    else:
        read_mode = "r"
        open_cmd = open
        
    with open_cmd(assembly, mode=read_mode) as assembly_handle:
        header = None
        for line in assembly_handle:
            if read_mode == "rb":
                line = line.decode("utf-8")
            if line.startswith(">"):
                if header:
                    yield header, ''.join(seq)
                header = line.rstrip()
                seq = []
            else:
                seq.append(line.rstrip())
        yield header, ''.join(seq)


def count_bases(seq, counts, min_size, scaffolds=True):
    """Update counts with this scaffold's bases"""
    if scaffolds and len(seq) < min_size:
        return 

    counts.update(seq)
    return 

def get_gc(counts, mask_invalid=False):
    """Get the percent GC content"""
    g_and_c = set("GgCc")
    a_and_t = set("AaTt")
    n_bases = set("Nn")
    
    gc = 0
    at = 0
    n = 0
    
    for key, value in counts.items():
        if key in g_and_c:
            gc += value
         
        elif key in a_and_t:
            at += value

        elif key in n_bases:
            n += value

        # Deal with weird bases as either error or as an N
        else:
            if mask_invalid:
                n += value
            else:
                raise InvalidBase(key)


    return gc, at, n
            
    
def nx(lengths, fraction, min_size):
    sorted_lengths = sorted(lengths, reverse=True)
    mid = sum([i for i in sorted_lengths if i >= min_size])*fraction
    total = 0
    for i, length in enumerate(sorted_lengths):
        total += length
        if total >= mid:
            return length, i + 1

def break_scaffold(seq):
    """Break up scaffolds into contigs at 10 or more consecutive Ns"""
    i = 0
    cur_contig_start = 0

    while (i < len(seq)) and (seq.find("N", i) != -1):
        start = seq.find("N", i)
        end = start + 1
        while (end != len(seq)) and (seq[end] == "N"):
            end +=1
        
        i = end + 1
        if (end - start) >= 10:
            if cur_contig_start != start:
                yield (seq[cur_contig_start: start],True)
            cur_contig_start = end
    if len(seq[cur_contig_start: ]) != 0:
        yield (seq[cur_contig_start: ],False)


def write_contig_lengths(lengths,fn):
    fh = open(fn,"w")
    for length in lengths:
        print(length,file=fh)

class InvalidBase(Exception):
    pass

if __name__ == "__main__":
    argh.dispatch_command(main)
