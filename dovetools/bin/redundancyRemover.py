#!/usr/bin/env python3 
import sys
import os
import pysam
import argh
import logging
from random import choice
from operator import attrgetter 
from collections import defaultdict
import subprocess


def setup_logging(log_file,log_level):
    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)
    logging.basicConfig(level=numeric_level, filename=log_file,filemode="w") 

def open_fasta(fasta_fn):
    check_fasta_index(fasta_fn)
    fasta = pysam.FastaFile(fasta_fn)
    return fasta

def check_fasta_index(fn):
    index_path = fn + ".fai"
    if not os.path.isfile(index_path):
        print("No index file found for fasta file: %s\nCreating index." % (fn),file=sys.stderr)
        subprocess.check_call("samtools faidx %s" % (fn),shell=True)
    return

def write_fasta(header,seq,output,length=100):
    print(">%s" % (header),file=output)
    pos = 0
    seq_len = len(seq)
    while True:
        if pos + length >= seq_len:
            print(seq[pos:],file=output)
            break
        print(seq[pos:pos+length],file=output)
        pos += length

class SimpleRegion(object):
    def __init__(self,
                 scaf,
                 start,
                 end):
        self.scaf = scaf
        self.start = start
        self.end = end

    def __str__(self):
        return "%s:%d-%d" % (self.scaf,self.start,self.end)

class AlignedRegion:
    def __init__(self,
                 scaf1,
                 start1,
                 end1,
                 scaf2,
                 start2,
                 end2,
                 reverse,
                 score):
        self.scaf1 = scaf1
        self.start1 = start1
        self.end1 = end1
        self.scaf2 = scaf2
        self.start2 = start2
        self.end2 = end2
        self.reverse = reverse
        self.score = score
    
    def __str__(self):
        return "%s:%d-%d,%s:%d-%d,%s,%d" % (self.scaf1,
                                         self.start1,
                                         self.end1,
                                         self.scaf2,
                                         self.start2,
                                         self.end2,
                                         self.reverse,
                                         self.score)

    @classmethod
    def from_hit_line(cls,line):
        #__,scaf1,score,scaf2,strand,start1,end1,start2,end2,__ = line.split()
        __,scaf1,score,scaf2,strand,start2,end2,start1,end1,__,__ = line.split()
        reg =  cls(scaf1,
                   int(start1),
                   int(end1),
                   scaf2,
                   int(start2),
                   int(end2),
                   True if strand == '-' else False,
                   float(score))
        if scaf1 > scaf2:
            reg.swap()
        return reg

    def length(self,scaf=1):
        return eval("self.end%d - self.start%d" % (scaf,scaf))

    def swap(self):
        self.scaf1,self.scaf2 = self.scaf2,self.scaf1
        self.start1,self.start2 = self.start2,self.start1
        self.end1,self.end2 = self.end2,self.end1

    def spans_scaffolds(self,max_frac,lengths):
        len1 = self.length(1)
        len2 = self.length(2)
        frac1 = len1/lengths[self.scaf1]
        frac2 = len2/lengths[self.scaf2]
        span1 = span2 = False
        if frac1 > max_frac:
            span1 = True
        if frac2 > max_frac:
            span2 = True
        return span1,span2


class RedundancyRemover:
    def __init__(self,
                 lengths_fn,
                 tolerance = 100,
                 max_frac = 0.9):
        # tolerance - if region is within x bp of end of scaffold, extend to end
        self.tolerance = tolerance
        # max_frac - if region covers more than max_frac of scaffold, toss scaffold
        self.max_frac = max_frac
        self.lengths = {}
        fh = open(lengths_fn,"r")
        for line in fh:
            length,scaf = line.split()
            self.lengths[scaf] = int(length)
        self.regions = defaultdict(list)


    def read_regions(self, regions_fn):
        fh = open(regions_fn,"r")
        n_read = 0
        for line in fh:
            aln = AlignedRegion.from_hit_line(line)
            self.regions[aln.scaf1].append(aln)
            n_read += 1
        logging.info("Read in %d raw regions." % (n_read))

    def collapse_regions(self):
        collapsed_regions = defaultdict(list)
        for scaf in self.regions:
            regions = sorted(self.regions[scaf],key=attrgetter("start","end"))
            i = 0
            while True:
                if i == len(regions):
                    break
                curr_region = regions[i]
                j = i + 1
                while True:
                    if j == len(regions):
                        break
                    region2 = regions[j]
                    if region2.start > curr_region.end:
                        break
                    new_region = SimpleRegion(scaf,
                                              min(curr_region.start,region2.start),
                                              max(curr_region.end,region2.end))
                    curr_region = new_region
                    j += 1
                collapsed_regions[scaf].append(curr_region)
                i = j
            logging.debug("\t".join(map(str,["COLLAPSED",
                                            list(map(str,regions)),
                                            list(map(str,collapsed_regions[scaf]))])))
                                    #list(map(str,collapsed_regions[scaf]))]))
                                    
        self.regions = collapsed_regions

    def get_remove_region(self,region):
        # if region spans either scaffold, throw one out
        span1,span2 = region.spans_scaffolds(self.max_frac,self.lengths)
        if span1 or span2:
            if span1 and span2:
                #reg_scaf = choice([region.scaf1,region.scaf2])
                reg_scaf = region.scaf1
            elif span1:
                reg_scaf = region.scaf1
            else:
                reg_scaf = region.scaf2
            reg_start = 0
            reg_end = self.lengths[reg_scaf]
        # otherwise truncate the longer of the two scaffolds
        else:
            len1 = self.lengths[region.scaf1] 
            len2 = self.lengths[region.scaf2] 
            if len1 > len2:
                reg_scaf = region.scaf1
                if region.start1 < self.tolerance:
                    reg_start = 0
                else:
                    reg_start = region.start1
                if len1 - region.end1 < self.tolerance:
                    reg_end = len1 
                else:
                    reg_end = region.end1
            else:
                reg_scaf = region.scaf2
                if region.start2 < self.tolerance:
                    reg_start = 0
                else:
                    reg_start = region.start2
                if len2 - region.end2 < self.tolerance:
                    reg_end = len2 
                else:
                    reg_end = region.end2

        remove_reg = SimpleRegion(reg_scaf,reg_start,reg_end)
        logging.debug("\t".join(["TO_REMOVE",
                                 "RAW",
                                 str(region)]))

        logging.debug("\t".join(["TO_REMOVE",
                                 "FINAL",
                                 str(remove_reg)]))
        return remove_reg

    def populate_regions_to_remove(self,regions_file):
        self.read_regions(regions_file)

        truncate_regions = defaultdict(list)
        for scaf in self.regions:
            regions = self.regions[scaf]
            for region in regions:
                reg_to_remove = self.get_remove_region(region)
                truncate_regions[reg_to_remove.scaf].append(reg_to_remove)
        self.regions = truncate_regions
        self.collapse_regions()

    def write_output_assembly(self,in_fasta_fn,out_fasta_fn,removed_fasta_fn):
        in_fh = open_fasta(in_fasta_fn)
        out_fh = open(out_fasta_fn,"w")
        if removed_fasta_fn:
            r_fh = open(removed_fasta_fn,"w")
        n_removed = 0
        for scaf in self.lengths:
            # If scaf is untouched, just output it
            if scaf not in self.regions:
                write_fasta(scaf,in_fh.fetch(scaf),out_fh)
                continue

            regions = self.regions[scaf]
            logging.debug("REGIONS:\t%s" % (list(map(str,regions))))
            # If we want to remove the whole scaffold, just continue
            # assuming these are collapsed regions, first is good as any 
            region = regions[0]
            if region.start == 0 and region.end == self.lengths[scaf]:
                logging.debug("\t".join(["DROPPED:",scaf,str(self.lengths[scaf])]))
                if removed_fasta_fn:
                    write_fasta("%s" % (scaf),
                                in_fh.fetch(scaf),
                                r_fh)
                n_removed += region.end - region.start
                continue
            parts = []
            pos = 0
            for region in regions:
                if region.scaf != scaf:
                    print("BAD!!", file=sys.stderr)
                    sys.exit(1)
                left = in_fh.fetch(scaf,pos,region.start)
                parts.append(left)
                if region.start == 0:
                    pos = region.end
                    logging.debug("\t".join(["TRUNCATED:",str(region)]))
                    n_removed += region.end - region.start
                    if removed_fasta_fn:
                        write_fasta("%s:%d-%d" % (scaf,region.start,region.end),
                                    in_fh.fetch(scaf,region.start,region.end),
                                    r_fh)
                    continue
                elif region.end == self.lengths[scaf]:
                    pos = region.end
                    logging.debug("\t".join(["TRUNCATED:",str(region)]))
                    n_removed += region.end - region.start
                    if removed_fasta_fn:
                        write_fasta("%s:%d-%d" % (scaf,region.start,region.end),
                                    in_fh.fetch(scaf,region.start,region.end),
                                    r_fh)
                    break
                else:
                    logging.debug("\t".join(["INTERNAL:",str(region),self.lengths[scaf]]))
                    removed = "N" * (region.end - region.start)
                    if removed_fasta_fn:
                        write_fasta("%s:%d-%d" % (scaf,region.start,region.end),
                                    in_fh.fetch(scaf,region.start,region.end),
                                    r_fh)
                    parts.append(removed)
                    pos = region.end
            trailing = in_fh.fetch(scaf,pos,self.lengths[scaf])
            parts.append(trailing)
            write_fasta(scaf,"".join(parts),out_fh)
        print("Total removed: %d" % (n_removed),file=sys.stderr)

def main(alignments:"Chained semi-global alignments file from mbChain.py via semiglobal_filter.py.",
         input_fasta:"Fasta file of assembly.",
         scaf_lengths:"File containing 'length scaf_name' lines.",
         output_fasta:"File to write redundancy-removed assembly in FASTA format.",
         tolerance:"Extend aligned regions within tolerance of scaffold end to scaffold end." = 100,
         max_frac:"Extend aligned regions covering > max_frac of scaffold to cover scaffold." = 0.9,
         removed_fasta:"File to write removed regions in FASTA format."= None,
         log_file:"File to write log to." = "log.txt",
         log:"Log level"="INFO"):
    setup_logging(log_file,log)
    regions_by_scaf = defaultdict(list)
    remover = RedundancyRemover(scaf_lengths,
                                tolerance,
                                max_frac)
    remover.populate_regions_to_remove(alignments)
    remover.write_output_assembly(input_fasta,output_fasta,removed_fasta)

if __name__ == "__main__":
    argh.dispatch_command(main)
