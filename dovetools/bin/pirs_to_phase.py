#!/usr/bin/env python3

import argh
from dovetools.utils import FastqHelper, VcfLine
"""
def main(vcf, mypirs, sample="GM24149"):
    for snp_pair in parse_pirs(mypirs):
        print(snp_pair)

def parse_pirs(mypirs):
    started = False
    with open(mypirs) as pir_handle:
        for line in pir_handle:
            if started:
                for pir in get_pirs(line):
                    yield pir

            if line.startswith(sample_name) and len(line.split()) == 2:
                started = True
            
def get_pirs(line):
    s = line.split()
    for i in range(0, len(s)-3, 3):
        print(s[i], s[i+1], s[i+2])
"""
from collections import Counter
import itertools

def main(vcf, mypirs, sample="GM24149", scaff="chr1"):
    vcf_lookup = get_lookup(vcf, scaff)
    good = Counter()
    bad  = Counter()
    for snp_pair in parse_pirs(mypirs, sample):
        in_phase = check_phases(snp_pair, vcf_lookup)
        # -1 means one or both snps were not het
        if in_phase == -1:
            continue
        distance = get_distance(snp_pair)
        bin_pos = get_key(distance)
        if in_phase == 1:
            good[bin_pos] += 1
        elif in_phase == 0:
            bad[bin_pos] += 1

    for key, value in sorted(good.items(), key=lambda x: x[0]):
        percent = round(value*100 / (value + bad[key]), 3)
        new_key = get_key(key, reverse=True)
        print(new_key, percent, value, bad[key], value + bad[key], sep="\t")

def get_key(distance, reverse=False):
    low = 100000
    low_step = 10000
    med = 2000000
    med_step = 200000
    high = 10000000
    high_step = 1000000
    bins = list(itertools.chain(range(0, low, low_step), range(low, med, med_step), range(med, high, high_step)))
    if not reverse:
        for i, this_bin in enumerate(bins):
            if distance < this_bin:
                #print(distance, i-1, this_bin, bins)
                return i-1
        #print(distance, len(bins), this_bin, bins)
        return len(bins)
    else:
        for i, this_bin in enumerate(bins):
            if distance == i:
                return this_bin
        return high

def get_lookup(vcf, scaff):
    lookup = {}
    with open(vcf) as vcf_handle:
        for line in vcf_handle:
            if line.startswith("#"):
                continue
            snp_obj = VcfLine.from_line(line)
            if snp_obj.scaff == scaff:
                lookup[snp_obj.pos] = snp_obj
    return lookup
    
def parse_pirs(mypirs, sample):
    map_started = False
    reads_started = False
    snp_map = {}
    with open(mypirs) as pir_handle:
        for i, line in enumerate(pir_handle):
            if reads_started:
                for pir_pair in get_pirs(line, snp_map):
                    yield pir_pair

            if line.startswith(sample) and len(line.split()) == 2:
                reads_started = True

            if not reads_started and  map_started:
                snp_map[i-1] = int(line.split()[0])
            if line.startswith("MAP"):
                map_started = True
            
def get_pirs(line, snp_map):
    s = line.split()
    for i in range(0, len(s)-3, 3):
        yield ((snp_map[int(s[i])], s[i+1], s[i+2],), (snp_map[int(s[i+3])], s[i+4], s[i+5],))

def check_phases(snp_pair, vcf):
    phase1, phase2 = get_phase(snp_pair[0], vcf), get_phase(snp_pair[1], vcf)
    if phase1 == -1 or phase2 == -1:
        return -1
    if phase1 == phase2:
        return 1
    else:
        return 0
        
def get_phase(snp, vcf):
    pos = snp[0]
    base = snp[1]
    snp_obj = vcf[pos]
    if "0" not in snp_obj.phase or "1" not in snp_obj.phase:
        return -1
    if base == snp_obj.ref:
        phase = int(snp_obj.phase[0])
    elif base == snp_obj.alt:
        phase = int(snp_obj.phase[-1])
    else:
        return False
    return phase

def get_distance(snp_pair):
    return snp_pair[1][0] - snp_pair[0][0] 
>>>>>>> 1b9f35fea9b69fe3891a8408d18325d833a5420d

if __name__ == "__main__":
    argh.dispatch_command(main)
