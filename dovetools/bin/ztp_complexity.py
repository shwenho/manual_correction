#!/usr/bin/env python3


import argh
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

def main(histogram, output="/dev/stdout"):
    unique_molecules, total_reads = parse_hist(histogram)
    complexity = calc_ztp(unique_molecules, total_reads)
    with open(output, 'w') as out:
        print(int(complexity[0]), file=out)

def parse_hist(histogram):
    unique_molecules = 0
    total_reads = 0
    with open(histogram) as f:
        for line in f:
            s = line.split()
            dup_number = int(s[0])
            count = int(s[1])
            unique_molecules += count
            total_reads += dup_number * count
    return unique_molecules, total_reads



def calc_ztp(unique_molecules, total_reads):
    first_guess = 1000000
    func = lambda x: 1- math.exp(-total_reads/x) - unique_molecules/x
    library_complexity = fsolve(func, first_guess)
    return library_complexity

            
if __name__ == "__main__":
    argh.dispatch_command(main)
