#!/usr/bin/env python3

from setuptools import setup
import glob

setup(name='dovetools',
      version='0.2.0',
      description="Dovetail utility tools.",
      author='Jonathan Stites',
      author_email='jon@dovetail-genomics.com',
      license='None',
      packages=['dovetools'],
      install_requires=[
          'pysam>=0.8.1',
          'jira',
          'argh',
          'numpy',
          "pyyaml",
          'matplotlib',
          'snakemake<4.0.0',
          'psutil',
          'jinja2',
          "docutils"
      ],
      zip_safe=False,
      scripts=[f for f in glob.glob("bin/*.py")] + ["bin/dove-pipe"] 
      )

