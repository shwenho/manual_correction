#!/usr/bin/env python3

"""
Author:              Jonathan Stites
Date:                17 June 2014
Company:             Dovetail Genomics

Take a path to bam file, a map quality, pairs of insert sizes,
and an output path, and write Chicago statistics to the output
file.
"""

import argh
import collections
from .utils import BamReader
from .utils import BamTags

class Bam_stats:

    def __init__(self, bam, mapq, inserts, output):
        """Take a path to bam file, map quality as int, 
        list of insert pairs separated by commas, and path 
        to output file, and write Chicago statistics to 
        the output file."""

        self.name_to_count = self.process_bam(bam, mapq, inserts)
        self.output=output

    def process_bam(self, bam, mapq, inserts):
        """Take a bam file, map quality, and insert pairs, and return 
        a ordered dicts of Name: counts."""

        with BamReader.get_handle(bam, "r", bam="b") as bam_handle:

            # Create an ordered dict of Name: function 
            name_to_function = self.add_keys(mapq, inserts)

            # Create an ordered dict of Name: count
            name_to_counts = collections.OrderedDict(
                [(name, 0) for name in name_to_function.keys()])
            
            # Update the counts for every line that is read1, not read2
            for line in bam_handle:
                self.update_counts(line, mapq, name_to_counts, 
                                   name_to_function)

            return name_to_counts

    def add_keys(self, mapq, inserts):
        """Take a map quality and list of inserts pairs separated by 
        commas and return an ordered dict of Name: function."""

        # Create functions to test filters, except insert size test
        unmapped_func = lambda line: line.is_unmapped or line.mate_is_unmapped
        dup_func = lambda line: line.is_duplicate

        mapq_func = lambda line: line.mapq < mapq or BamTags.mate_mapq(line) < mapq
        diff_scaff_func = lambda line: BamTags.orientation(line) == "D"

        # Add keys and functions of filters, except insert size test
        name_to_func = collections.OrderedDict()
        name_to_func["Unmapped"] = unmapped_func
        name_to_func["Duplicates"] =  dup_func
        name_to_func["Mapq < " + str(mapq)] = mapq_func
        name_to_func["Different Scaffold"] = diff_scaff_func

        # Add key and function for insert size test
        for insert_pair in inserts:
            insert_key = self.get_insert_key(insert_pair)
            name_to_func[insert_key] = self.make_insert_func(insert_pair)

        return name_to_func


    def get_insert_key(self, insert):
        """Take an insert min and max separated by a comma and return
        a human-readable key."""
        
        i_min, i_max = map(self.sizeof, insert.split(","))

        # Format special case of only testing if insert is lower than max
        if i_min == "0":
            insert_key = "Insert < " + i_max

        # Format special case of only test if insert is higher than min
        elif i_max.lower() == "inf":
            insert_key = "Insert > " + i_min

        # Format all else
        else:
            insert_key = i_min + " < Insert < " + i_max
        return insert_key

    def sizeof(self, num):
        """Take a string representing a number and return the string
        formatted with base suffix (kb, Mb, etc.)."""
        
        # Do not format "Inf"
        if num.lower() == "inf":
            return num

        # Do not format 0
        if int(num) == 0:
            return num
        
        num = int(num)
        for x in ["", "kb", "Mb", "Gb"]:
            if num < 1000:
                return "%3.0f%s" % (num, x)
            num = num / 1000
        return "%3.0f%s" % (num, x)

    def get_line_insert(self, line):
        """Take a pysam line object and return the insert size."""
        line_insert = abs(self.get_start(line) - BamTags.mate_start(line))
        return line_insert

    def make_insert_func(self, insert_pair):
        """Take a pair of insert min and max, separated by commas,
        and return a function for testing if a line is within the
        inserts."""

        i_min, i_max = map(float, insert_pair.split(","))
        return lambda line: i_min <= self.get_line_insert(line) < i_max

    def update_counts(self, line, mapq, vals, keys):
        """Take a pysam line object, a map quality, and ordered dicts of 
        Name: count and Name: function and return an appropriately 
        incremented Name: count."""

        # Only count pairs by skipping read2
        if line.is_read2:
            return 

        # Test each filter sequentially, increment the first one that is True
        for name, function in keys.items():
            if function(line):
                vals[name] += 1
                return 
        
    def print_stats(self, name_to_counts, output):
        """Take an ordered dict of Name: count and a path to output file
        and write stats to output."""

        with open(output, "w") as output_handle:
            print("Type", "Count", "Percent", sep="\t", file=output_handle)
            for name, count in name_to_counts.items():
                percent = count * 100 / sum(name_to_counts.values())
                rounded_percent = round(percent, 3)
                print(name, count, rounded_percent, sep="\t", 
                      file=output_handle)

    def get_start(self, line):
        """Get the 5 prime end"""
        if line.is_reverse:
            return line.pos + line.rlen
        return line.pos


def bam_stats(
        bam: "Bam file", 
        mapq: "Minimum map quality" = 20,
        inserts: "Inserts. Separate pairs of inserts by commas." = 
        ["500000,Inf", "0,2000", "2000,10000", 
         "10000,100000", "100000,500000"],
        output: "Output file" = "/dev/stdout"):
    """Take a bam file and output Chicago statistics."""

    stats = Bam_stats(bam, mapq, inserts, output)
    stats.print_stats(stats.name_to_count, stats.output)

        
