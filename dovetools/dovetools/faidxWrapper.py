#!/usr/bin/env python3 
import sys
import pysam
import os
import subprocess

class FaidxWrapper:
    def __init__(self,fasta_fn):
        self.check_fasta_index(fasta_fn)
        self.filename = fasta_fn
        self.faidx = pysam.FastaFile(fasta_fn)
        self.lengths = {name:length for name,length in zip(self.faidx.references,
                                                           self.faidx.lengths)}
    def fetch(self,scaffold,start=None,end=None):
        if not start:
            start = 0
        if not end:
            end = self.lengths[scaffold]
        return self.faidx.fetch(scaffold,start,end)

    def get_length(self,scaf):
        return self.lengths[scaf]

    @staticmethod
    def check_fasta_index(fn):
        index_path = fn + ".fai"
        if not os.path.isfile(index_path):
            print("No index file found for fasta file: %s\nCreating index." % (fn),file=sys.stderr)
            subprocess.check_call("samtools faidx %s" % (fn),shell=True)
        return

    @staticmethod
    def write_fasta(header,seq,output,length=100):
        print(">%s" % (header),file=output)
        pos = 0
        seq_len = len(seq)
        while True:
            if pos + length >= seq_len:
                print(seq[pos:],file=output)
                break
            print(seq[pos:pos+length],file=output)
            pos += length
