#!/usr/bin/env python3

import argh
from .utils import FastqHelper
import sys

class FastqNameFilter:
    """This removes fastq lines from a fastq file by read name. Much 
    fastq than grep -v if using many reads to remove. Alternatively it
    can collect only the reads given."""

    def __init__(self, fastq, output, filter_file, verbose=False, get=False):
        """Initialize arguments as attributes for easy access."""
        self.fastq = fastq
        self.output = output
        self.filt = self.get_filter_reads(filter_file)
        self.verbose = verbose
        self.get = get
        self.process_fastq()


    def process_fastq(self):
        """Reads through the fastq file and takes out any fastq entries that match
        the remove file"""

        with open(self.output, "w") as output_handle:
            for lines in FastqHelper.read_lines(self.fastq):
                self.write_if_necessary(lines, output_handle)
                self.print_message_if_necessary(lines)

    def print_message_if_necessary(self, lines):
        """Prints to stderr if verbosity is set."""
        if self.in_filter(lines) and self.verbose:
            self.print_message(lines)
            

    def write_if_necessary(self, lines, output_handle):
        """Conditionally writes the fastq line."""
        if self.in_filter(lines) and self.get:
            output_handle.write(''.join([lines.as_string, "\n"]))
        if not self.in_filter(lines) and not self.get:
            output_handle.write(''.join([lines.as_string, "\n"]))


    def in_filter(self, lines):
        """Checks if the fastq line is in the supplied file."""
        if lines.name.strip("@") in self.filt:
            return True

    def get_message(self):
        """Returns the correct message."""
        if self.get:
            return " was kept."
        return " was filtered."

    def print_message(self, lines):
        """Prints the verbosity message when a line matches one
        from the supplied file."""
        message = self.get_message()
        sys.stderr.write(''.join([lines.name.strip("@"), message, "\n"]))
        sys.stderr.flush()

    def get_filter_reads(self, filter_file):
        """Returns a set of read names from supplied file."""
        my_set = set()
        with open(filter_file, "r") as filt_handle:
            for line in filt_handle:
                my_set.add(line.strip("@").split()[0])
        return my_set

@argh.arg("-o", "--output", required=True)
@argh.arg("-g", "--get")
@argh.arg("-r", "--remove", required=True)
def fastq_name_filter(fastq : "Fastq file", 
                      output : "A output file" = None, 
                      remove : "A file of read names to remove" = None,
                      verbose : "verbosity." = False, 
                      get : "Get, not remove, the reads" = False):
    """This removes fastq lines from a fastq file by read name. Much                                                                                                                                                                     
    fastq than grep -v if using many reads to remove. Alternatively it                                                                                                                                                                   
    can collect only the reads given."""
    FastqNameFilter(fastq, output, remove, verbose=verbose,
                    get=get)

if __name__=="__main__":
    argh.dispatch_command(fastq_name_filter)
