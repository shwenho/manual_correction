#!/usr/bin/env python3

import argh
from .utils import BamReader
from .utils import BamTags

class SamChicagoSubset:

    def __init__(self, my_input: "Sam or bam", 
               output: "Output file" = "-", 
               b: "Input is bam" = False, 
               S: "Output is sam" = False,
               duplicates: "Keep duplicates" = False, 
               min_insert: "Minimum insert" = 0,
               max_insert: "Max insert" = float("inf"), 
               my_type: "Chicago pair type" = None, 
               mapq: "Mapq quality" = 20,
               junction: "Junction status" = None, 
               strand: "Strand mapped to" = None,
               min_scaff_length: "Minimum scaffold size" = 0, 
               inserts: "Only print insert sizes" = False):
        """Subset a bam or sam file by pair type.

        Relies on custom tags for sam files. These are:
        xb:       Bit flag of mate
        xs:       Five prime position of mate
        xm:       Map quality of mate
        xt:       Type, ie innie, outtie, same, different scaff, unmapped
        xj:       T/F for whether the pair was trimmed for junction, for chicago pairs
        xc:       Barcode for Michigan pairs (instead of xj)

        Positional arguments:
        my_input  -- Sam or Bam file

        Keyword Arguments:
        my_output  -- output file
        in_bam  --  input is bam
        out_sam -- output is sam
        duplicates  -- keep duplicate pairs
        min_insert  -- minimum insert
        max_insert  -- maximum insert
        my_type  -- chicago pair type
        mapq  -- map quality
        junction  -- junction type
        strand  -- strand mapped to
        min_scaff_length  -- minimum scaffold size
        inserts  -- only print inserts
        """     
        self.my_input = my_input
        self.my_output = output
        self.in_bam = b
        self.out_sam = S
        self.duplicates = duplicates
        self.min_insert = min_insert
        self.max_insert = max_insert
        self.my_type = my_type
        self.mapq = mapq
        self.junction = junction
        self.strand = strand
        self.min_scaff_length = min_scaff_length
        self.inserts = inserts

    def subset(self):
        """
        Subset a chicago sam/bam file (must have tags added already).
        Only mapped pairs are written.
        """

        # Open the input and output using pysam         
        if self.inserts:
            i = open(self.inserts, 'w')

        with BamReader.get_handle(self.my_input, "r", bam=self.in_bam) as samfile, \
             BamReader.get_handle(self.my_output, "w", bam=not self.out_sam, header=samfile.header) as outfile:
            
            lengths = samfile.lengths
            for line in samfile:
                
                if line.is_unmapped or line.mate_is_unmapped:
                    continue

                if not self.duplicates and line.is_duplicate:
                    continue

                insert = abs(self.get_start(line) - BamTags.mate_start(line))
                line_type = BamTags.orientation(line)

                if self.mapq and (line.mapq < self.mapq or BamTags.mate_mapq(line) < self.mapq):
                    continue
            
                if self.my_type and line_type not in self.my_type:
                    continue

                if self.min_insert and (line_type == "D" or insert < self.min_insert):
                    continue
                    
                if self.max_insert and (line_type == "D" or insert > self.max_insert):
                    continue

                if self.junction and BamTags.junction(line) != self.junction:
                    continue
                
                if self.strand and ((line.is_reverse and self.strand=="+") or (not line.is_reverse and self.strand == "-")):
                    continue

                if self.min_scaff_length and lengths[line.tid] < self.min_scaff_length:
                    continue
                    
                if self.inserts:
                    print(insert, file=i)
                else:
                    outfile.write(line)
                

    def get_start(self, line):
        """Get the 5 prime end"""
        if line.is_reverse:
            return line.pos + line.rlen
        return line.pos

@argh.arg("--my_type", nargs="+")
@argh.arg("-j", "--junction", choices=["T", "F"])
@argh.arg("--strand", choices=["+", "-"])
def sam_subset(my_input: "Sam or bam", 
               output: "Output file" = "/dev/stdout", 
               b: "Input is bam" = False, 
               S: "Output is sam" = False,
               duplicates: "Keep duplicates" = False, 
               min_insert: "Minimum insert" = 0,
               max_insert: "Max insert" = float("inf"), 
               my_type: "Chicago pair type" = None, 
               mapq: "Mapq quality" = 20,
               junction: "Junction status" = None, 
               strand: "Strand mapped to" = None,
               min_scaff_length: "Minimum scaffold size" = 0, 
               inserts: "Only print insert sizes" = None):
    """Take a sam or bam file and subset by pair type.

    Relies on custom tags for sam files. These are:
    xb:       Bit flag of mate
    xs:       Five prime position of mate
    xm:       Map quality of mate
    xt:       Type, ie innie, outtie, same, different scaff, unmapped
    xj:       T/F for whether the pair was trimmed for junction, for chicago pairs
    xc:       Barcode for Michigan pairs (instead of xj)
    """
    
    bam = SamChicagoSubset(my_input, output=output, b=b, S=S, 
                           duplicates=duplicates,
                           min_insert=min_insert, max_insert=max_insert, 
                           my_type=my_type, mapq=mapq,
                           junction=junction, strand=strand,
                           min_scaff_length=min_scaff_length,
                           inserts=inserts)
    bam.subset()

if __name__ == "__main__":
    argh.dispatch_command(sam_subset)
