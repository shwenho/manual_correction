#!/usr/bin/env python3

import argh
from .utils import BamReader
from .utils import BamTags

class IOSbins:

    def __init__(self, my_input: "Sam or Bam files", 
                 output: "Output file" = "/dev/stdout", 
                 binsize: "Size of bins" = 1000, 
                 xmax: "Max insert size" = 500000, 
                 mlen: "Minimum scaffold length" = 0,
                 mapq: "Map quality" = 20, 
                 junction: "Junction type" = None,
                 in_bam: "Input is bam" = False,
                 n: "Max lines to read" = None,
                 no_pseudo: "No pseudocounts" = False,
                 low_map: "Only reads that FAIL mapq filter" = False):
        self.sam = my_input
        self.output = output
        self.binsize = binsize
        self.xmax = xmax
        self.mlen = mlen
        self.mapq = mapq
        self.junction = junction
        self.n = n
        self.in_bam = in_bam
        self.no_pseudo = no_pseudo
        self.low_map = low_map

    def get_counts(self):

        # Create place for counts
        d = self.init_counts()

        for s in self.sam:
            with BamReader.get_handle(s, "r", bam=self.in_bam) as samfile:

                # Don't remember
                lengths = samfile.lengths

                # Number of lines read so far
                line_num = 0

                for line in samfile:
                    line_num += 1

                    if self.n and line_num > self.n:
                        self.print_output(d)
                        return
                    
                    if self.pass_filter(line, lengths):
                    
                        self.update_count(d, line)
                
        self.print_output(d)

    def pass_filter(self, line, lengths):
        if line.is_unmapped or line.mate_is_unmapped:
            return
            
        if line.is_read2:
            return
                
        if line.is_duplicate:
            return

        insert = abs(self.get_start(line) - BamTags.mate_start(line))
        line_type = BamTags.orientation(line)

        
        if ((line.mapq < self.mapq) or (BamTags.mate_mapq(line) < self.mapq)):
            if not self.low_map:
                return
        else:
            if self.low_map:
                return

        if line_type == "D":
            return

        if self.xmax and (line_type == "D" or insert > self.xmax):
            return

        if self.junction and BamTags.junction(line) != self.junction:
            return

        if self.mlen and lengths[line.tid] < self.mlen:
            return

        return True


    def get_pdfs(self, d):
        pdfs = {}
        for pair_type in d.keys():
            pdfs[pair_type] = [0.5] * int((self.xmax/self.binsize))
            total = sum(d["A"])

            if pair_type == "J":
                total = sum(d["J"])
            for size, count in enumerate(d[pair_type]):
                pdfs[pair_type][size] = count/total

        return pdfs

    def get_cdfs(self, pdfs):
        cdfs = {}

        for pair_type in pdfs.keys():
            cdf = 0
            cdfs[pair_type] = [0.5] * int((self.xmax/self.binsize))
            for size, p in enumerate(pdfs[pair_type]):
                cdf += p
                cdfs[pair_type][size] = cdf
        return cdfs

    def print_output(self, d):
        with open(self.output, "w") as output_handle:
            print("size", "innies", "outties", "same", "all", "junction",
                  "innies_p", "outties_p", "same_p", "all_p", "junction_p",
                  "innies_c", "outties_c", "same_c", "all_c", "junction_c",
                  sep="\t", file=output_handle)

            pdfs = self.get_pdfs(d)
            cdfs = self.get_cdfs(pdfs)

            for b in range(0, int((self.xmax/self.binsize))):
                my_items = []
                my_items.append(b*self.binsize)

                for pair_type in "IOSAJ":
                    my_items.append(d[pair_type][b])

                for pair_type in "IOSAJ":
                    my_items.append(pdfs[pair_type][b])

                for pair_type in "IOSAJ":
                    my_items.append(cdfs[pair_type][b])

                str_items = map(str, my_items)
                print("\t".join(str_items), end="\n", file=output_handle)
            

    # Update the count for that pair type, if it's within the correct range
    def update_count(self, d, line):
        insert = abs(self.get_start(line) - BamTags.mate_start(line))
        line_type = BamTags.orientation(line)
        junction = BamTags.junction(line)

        try:
            d[line_type][int(insert/self.binsize)] += 1
            d["A"][int(insert/self.binsize)] += 1
            if junction == "T":
                d["J"][int(insert/self.binsize)] += 1
        except IndexError:
            pass

    # Create dictionary of lists holding the counts of different insert sizes
    def init_counts(self):
        types = ("O", "I", "S", "A", "J")
        d = {}
        i = 0.5
        if self.no_pseudo:
            i = 0
        for pair_type in types:
            d[pair_type] = [i] * int((self.xmax/self.binsize))
        return d

    def get_start(self, line):
        """Get the 5 prime end"""
        if line.is_reverse:
            return line.pos + line.rlen
        return line.pos

    def get_type(self, a1, a2):
        """Decide what type the pair is"""
        if a1.is_unmapped or a2.is_unmapped:
            return "U"
        if a1.tid != a1.rnext:
            return "D"
        if a1.is_reverse == a2.is_reverse:
            return "S"
        if self.is_innie(a1, a2):
            return "I"
        return "O"

    def is_innie(self, a1, a2):
        """Decide if pair is an innie"""
        if not a1.is_reverse:
            return self.get_start(a1) < self.get_start(a2)
        return self.get_start(a2) < self.get_start(a1)


@argh.arg("-n", type=int)
@argh.arg("-b", "--b")
@argh.arg("--junction", choices=["T", "F"])
@argh.arg("my_input", nargs="+")
def ios_bins(my_input: "Sam or Bam file", 
             output: "Output file" = "/dev/stdout", 
             binsize: "Size of bins" = 1000, 
             xmax: "Max insert size" = 500000, 
             mlen: "Minimum scaffold length" = 0,
             mapq: "Map quality" = 20, 
             junction: "Junction type" = None,
             b: "Input is bam" = False,
             n: "Max lines to read" = None,
             no_pseudo: "Turn off pseudocounts" = False,
             low_map: "Only use FAILED mapq reads" = False):
    """Take a sam or bam file and output counts for plotting.

    Take a sam or bam file with my custom tags (see AddTags).
    Output counts for plotting.

    Columns:
    1. size - the size of the bin (one bin per line)
    2. innies - counts of innies
    3. outties - counts of outties
    4. all - counts of all reads
    5. junction - counts of all reads with junctions
    6. innies_p - count of innies / sum(all)
    7. outties_p - count of outties / sum(all)
    8. same_p - count of same / sum(all)
    9. all_p - count of all / sum(all)
    10. junction_p - count of junction / sum(junction)
    11. innies_c - sum(innies_p up to this bin)
    12. outties_c - sum(outties_p up to this bin)
    13. same_c - sum(same_p up to this bin)
    14. all_c - sum(all_p up to this bin)
    15. junction_c - sum(junction_p up to this bin)"""
    ios_bins = IOSbins(my_input, output=output, binsize=binsize, 
                       xmax=xmax, mlen=mlen, in_bam=b,
                       mapq=mapq, junction=junction, n=n, 
                       no_pseudo=no_pseudo, low_map=low_map)
    ios_bins.get_counts()


if __name__ == "__main__":
    argh.dispatch_command(ios_bins)
