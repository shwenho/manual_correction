#!/usr/bin/env python3

"""
Author:        Jonathan Stites
Date:          17 June 2014
Company:       Dovetail Genomics
"""

import argh
import collections
import sys


class QC_report:

    def __init__(self, junctions, duplicates, names, output):
        """Take lists of files for junction stats, duplicate metrics,
        library names, and an output path, and write to output a table
        of important information from those files."""

        lib_stats = self.get_library_stats(junctions, duplicates, names)
        self.print_table(output, lib_stats)

    def get_library_stats(self, junctions, duplicates, names):
        """Take liste of files for junctions stats, duplicate metrics,
        library names, and return an ordered dictionary of statistics."""
        
        if not (len(junctions) == len(duplicates) == len(names)):
            print(junctions, duplicates, names, "not even.")
            sys.exit(4)

        print(junctions, duplicates, names)
        lib_stats = collections.OrderedDict()
        self.library_names(lib_stats, names)
        self.junction_stats(lib_stats, junctions)
        self.duplicate_stats(lib_stats, duplicates)
        return lib_stats

    def library_names(self, lib_stats, names):
        """Take an ordered dictionary of stats and add library names."""
        if names:
            lib_stats["Library"] = names

    def convert_to_percent(self, float_str, round_num):
        """Take a string representing a number and a number of digits 
        for rounding and return the number as a percent with percent 
        symbol, as a string."""

        as_float = float(float_str)
        as_percent = as_float*100
        as_round = round(as_percent, round_num)
        return "".join([str(as_round), "%"])

    def duplicate_stats(self, lib_stats, duplicates):
        """Take a list of duplicate metrics files and return
        pair number, duplicate number, duplicate percent, library size."""
        
        if not duplicates:
            return

        keys = ("Duplicate read pairs in library", 
                "Estimated unique molecules in library")

        for dup_file in duplicates:
            with open(dup_file) as dup_handle:
                for line in dup_handle:
                    
                    # For novosort output
                    if "Proper Pairs" in line:
                        s = line.split()
                        dup_percent = str(int(s[4].replace(",", ""))/int(s[3].replace(",", "")))
                        lib_size = s[6].replace(",", "")

                    elif "Unknown Library" in line:
                        s = line.split()
                        dup_percent = s[8]
                        lib_size = s[9]

                # Format information

                values = (self.convert_to_percent(dup_percent, 3), 
                          self.add_thousands_sep(lib_size))

                # Add information to lib stats. Libraries are ordered
                for num, key in enumerate(keys):
                    lib_stats.setdefault(key, []).append(values[num])
                            
    def junction_stats(self, lib_stats, junctions):
        """Take a list of junction stats files and return 
        junction counts, total counts, percent junctions."""
        
        if not junctions:
            return

        keys = ("Read pairs with junction sequence", 
                "Total read pairs", 
                "Percent read pairs with junction sequence")

        for jun_file in junctions:
            with open(jun_file) as jun_handle:
                for line in jun_handle:
                    if "with junction" in line:
                        s = line.split()
                        reads_with_junction = s[2]
                        total_reads = s[4]
                        junction_percent = s[6]

                        values = (self.add_thousands_sep(reads_with_junction), 
                                  self.add_thousands_sep(total_reads), 
                                  self.convert_to_percent(junction_percent, 3))
                        
                        # Add stats to lib_stats
                        for num, key in enumerate(keys):
                            lib_stats.setdefault(key, []).append(values[num])

    def add_thousands_sep(self, number):
        """Take a string representing a number and return the number
        as a string with commas as a thousands seperator."""
        num = int(number)
        return "{0:,}".format(num)

    def print_table(self, output, lib_stats):
        """Take an output file path and library stats and write a table
        of statistics to output file."""
        with open(output, "w") as output_handle:
            for key, value in lib_stats.items():
                print(key, *value, sep="\t", file=output_handle)


@argh.arg("-j", "--junctions", nargs="+")
@argh.arg("-d", "--duplicates", nargs="+")
@argh.arg("-n", "--names", nargs="+")
def qc_report(
        junctions: "File with junction stats" = None,
        duplicates: "File with metrics stats" = None,
        names: "Names of libraries" = None,
        output: "Output file" = "/dev/stdout"):
    """Take files of junction stats, duplicate metrics, and library
    names, and output a QC report table."""

    QC_report(junctions, duplicates, names, output)


