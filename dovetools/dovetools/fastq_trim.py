#!/usr/bin/env python3

import argh
import itertools
from .utils import FastqHelper
import sys
import re

class FastqTrim:
    """Take fastq files and truncate based on junction sequence.

    This class enables the trimming of paired Chicago fastq files. 
    Chicago data are created such that there are junctions between 
    chimeric reads. It is important to truncate the read at this 
    junction to facilitate mapping. 

    Positional arguments:
    forward  -- Forward fastq file
    reverse  -- Reverse fastq file
    output1  -- File for trimmed forward reads 
    output2  -- File for trimmed reverse reads 
    
    Keyword arguments:
    tag_file -- File for whether each read has junction (default None)
    junction -- Junction sequence (GATCGATC)
    quiet    -- Suppress the junction percent (default False)
    min_len  -- Minimum length for pairs (default 0)
    """    


    def __init__(self, forward, reverse, output1, output2, 
                 tag_file=None, junction="GATCGATC", quiet=False,
                 min_len=0, site_len=None, lengths_hist=None):
        """Take arguments and save as class attributes"""
        if not site_len:
            self.site_len = int(len(junction)/2)
        else:
            self.site_len = int(site_len)
        self.junction = junction
        self.fastq1 = forward
        self.fastq2 = reverse
        self.output1 = output1
        self.output2 = output2
        self.tag_file = tag_file
        self.quiet = quiet
        self.min_len = min_len
        self.lengths_hist = lengths_hist

    def trim_fastq(self):
        """Truncates paired-end fastq at the junction site.

        Takes paired-end fastq files and optionally writes truncated sequence to file. Can write 
        whether reads had junction to tag_file. Can write to stderr the number with junction."""
        n, t = 0, 0

        z = FastqHelper.detect_gzip(self.output1)
        with FastqHelper.detect_file(self.output1, mode="w") as o1, \
             FastqHelper.detect_file(self.output2, mode="w") as o2, \
             open(self.tag_file, 'w') as tag:

            for lines in FastqHelper.read_paired_lines(self.fastq1, self.fastq2):
                t += 1
                has_junct = "F"
                if self.have_junction(lines):
                    n += 1
                    l1_pre = lines[0].sequence
                    l2_pre = lines[1].sequence
                    self.truncate_if_necessary(lines)
                    has_junct = "T"

                if (len(lines[0].sequence) < self.min_len or 
                    len(lines[1].sequence) < self.min_len):
                    continue
                        
                if tag:
                    print("\t".join([lines[0].name.strip('@'), "\t", has_junct]), file=tag)
                if o1 and o2:
                    FastqHelper.write_paired(lines[0], lines[1], o1, o2, z=z)


        if not self.quiet:
            print("with junction:", n, "total:", t, "percent:", n/t, file=sys.stderr)

    def truncate_if_necessary(self, lines):
        """Take pair of fastq line objects and truncates."""
        return  [self.truncate_line(line) for line in lines]

    def have_junction(self, lines):
        """Take pair of fastq line objects, return True if junction."""
        if re.search(self.junction, lines[0].sequence) or re.search(self.junction, lines[1].sequence):
            return True

    def has_junction(self, line):
        """Takes fastq line object, return True if junction."""
        if re.search(self.junction, line.sequence):
            return True

    def truncate_line(self, line):
        """Takes fastq line object and truncates."""
        #        end = line.sequence.find(self.junction)
        end = re.search(self.junction, line.sequence)
        if not end:
            return
        end = end.start()
        end += self.site_len
        line.sequence = line.sequence[:end]
        line.quality = line.quality[:end]
        line.as_string = line.get_string()

def fastq_trim(forward : "Forward fastq file",
               reverse : "Reverse fastq file",
               output1 : "File for trimmed forward reads",
               output2 : "File for trimmed reverse reads",
               tag_file : "File for reads with junction" = None, 
               junction : "Junction sequence" = "GATCGATC", 
               quiet : "Suppress the junction percent" = False,
               min_len : "Minimum pair length" = 0,
               site_len : "Length of site to keep, default half of junction" = None,
               lengths_hist : "File for lengths after junction, does not work anymore" = None):
    """Take fastq files and truncate based on junction sequence.

    This class enables the trimming of paired Chicago fastq files. 
    Chicago data are created such that there are junctions between 
    chimeric reads. It is important to truncate the read at this 
    junction to facilitate mapping. 
    """    
    FastqTrim(forward, reverse, output1=output1, output2=output2,
              tag_file=tag_file, junction=junction, quiet=quiet, 
              min_len=min_len, site_len=site_len, 
              lengths_hist=lengths_hist).trim_fastq()
    

if __name__=="__main__":
    """Run the FastqTrim process if executed from a shell."""
    argh.dispatch_command(fastq_trim)
    
