import pysam
import sys
import bisect
from .utils import BamReader
from .utils import VcfLine
from .utils import BamTags


class SamSingleSnps:
    
    def __init__(self, vcf, my_input, my_output, in_bam, mapq=20):
        self.vcf = open(vcf, "r")
        self.my_input = my_input
        self.my_output = my_output
        self.in_bam = in_bam
        self.mapq = mapq

    def get_single_snps(self):

        with BamReader.get_handle(self.my_input, "r", bam=self.in_bam) as samfile, \
             open(self.my_output, "w") as outfile:

            self.sam_handle = samfile
            
            i_errors = 0
            print("Beginning indexing", file=sys.stderr)
            read_index = pysam.IndexedReads(samfile)
            read_index.build()

            print("Beginning vcf reading", file=sys.stderr)
            het_snps, vcf_lines = self.get_het_snps(samfile)
            print(het_snps.keys(), file=sys.stderr)
            n = 0

            print("Beginnig to process sam file", file=sys.stderr)
            for line in samfile.fetch():
                n += 1
                if n % 100000 == 0:
                    print(n, line.tid, line.pos, file=sys.stderr)
                
                if line.is_read2:
                    continue

                if line.is_unmapped or line.mate_is_unmapped:
                    continue

                if line.is_duplicate:
                    continue

                if line.mapq < self.mapq or BamTags.mate_mapq(line) < self.mapq:
                    continue
                
                if BamTags.orientation(line) == "D":
                    continue

                try:
                    line_1, line_2 = read_index.find(line.qname)
                except ValueError:
                    continue

                snp1_pos = self.is_in(line_1, het_snps)
                snp2_pos = self.is_in(line_2, het_snps)

                snp1_key = line_1.tid, snp1_pos
                snp2_key = line_2.tid, snp2_pos

                try:
                    snp1 = vcf_lines[snp1_key]
                    snp2 = vcf_lines[snp2_key]
                
                except KeyError:
                    continue


                try:
                    self.get_snp_base(line_1, snp1)
                    self.get_snp_base(line_2, snp2)
                except IndexError:
                    i_errors += 1
                    continue
                self.output_line(line_1, snp1, outfile)
                self.output_line(line_2, snp2, outfile)

        print("Index Errors: ", i_errors, file=sys.stderr)

    def output_line(self, line, snp1, my_output):
        print(line.qname, line.flag, self.sam_handle.getrname(line.tid), line.pos, line.mapq,
              line.cigarstring, snp1.scaff, snp1.pos, snp1.ref, snp1.alt, chr(self.get_snp_base(line, snp1)),
              chr(self.get_snp_phred(line, snp1)), snp1.genotype_info, sep="\t", file=my_output)

    def get_snp_base(self, line, snp1):
        return line.query[snp1.pos  - line.pos + line.qstart - 1]

    def get_snp_phred(self, line, snp1):
        return line.qqual[snp1.pos - line.pos + line.qstart - 1]              
            
    def get_het_snps(self, samfile):
        """Create a dictionary of scaffolds (as tid) with value of 
        a sorted list of the positions of het snps"""
        snps = {}
        vcf_lines = {}
        for line in self.vcf:
            l = VcfLine(line)
            scaff = samfile.gettid(l.scaff)
            start = l.pos
            key = scaff, start
            if l.is_het:
                
                snps.setdefault(scaff, [])
                snps.setdefault(scaff, []).append(start)

                vcf_lines[key] = l

        return snps, vcf_lines


    def is_in(self, line, het_snps):
        pos = line.qstart + line.pos
        scaff = line.tid
        try:
            closest = self.find_ge(het_snps[scaff], pos)
            if closest <= line.pos + line.qend:
                return closest
            return 
        except (ValueError, KeyError):
            return 

    def find_ge(self, a, x):
        'Find leftmost item greater than or equal to x'
        i = bisect.bisect_left(a, x)
        if i != len(a):
            return a[i]
        raise ValueError


def sam_single_snps(bam: "A bam file",
                    vcf: "A vcf file",
                    output: "An output file" = "/dev/stdout", 
                    mapq: "A minimum mapq" = 20):
    snps = SamSingleSnps(vcf, bam, output, "b", mapq)
    snps.get_single_snps()
