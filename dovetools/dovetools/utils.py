"""
Author:       Jonathan Stites
Company:      Dovetail Genomics

Utilities that are helpful for many other scripts.

"""

import itertools
import pysam
import gzip
import sys


class FastqHelper:
    """This class contains some useful functions on fastq files."""

    @staticmethod
    def read_lines(my_file):
        """Generator of fastq reads. Serves up all four lines for each read."""
        is_gzipped = FastqHelper.detect_gzip(my_file)
        handle = FastqHelper.detect_file(my_file, 'r')
        while True:
            r = list(itertools.islice(handle, 4))
            if is_gzipped:
                r = [s.decode() for s in r]
            yield FastqLine(r)

    @staticmethod
    def read_paired_lines(f1, f2):
        """Takes two fastq files and returns a generator of fastq reads."""
        z = FastqHelper.detect_gzip(f1)
        h1 = FastqHelper.detect_file(f1, 'r')
        h2 = FastqHelper.detect_file(f2, 'r')

        while True:
            r1 = list(itertools.islice(h1, 4))
            r2 = list(itertools.islice(h2, 4))
            if z:
                r1 = [s.decode() for s in r1]
                r2 = [s.decode() for s in r2]
            yield FastqLine(r1), FastqLine(r2)

    @staticmethod
    def detect_gzip(fastq):
        """Takes a fastq file and returns whether the file is gzipped."""
        if fastq.endswith(".gz"):
            return True
        return False

    @staticmethod
    def write_paired(l1, l2, o1, o2, z=False):
        """Takes two fastq read objects and two file handles and
        writes the fastq reads to the files."""
        r1 = ''.join([l1.as_string, '\n'])
        r2 = ''.join([l2.as_string, '\n'])

        if z:
            o1.write(r1.encode("utf-8"))
            o2.write(r2.encode("utf-8"))
        else:
            o1.write(r1)
            o2.write(r2)

    @staticmethod
    def detect_file(fastq, mode):
        """Takes a fastq file and read/write, and returns a file handle"""
        z = FastqHelper.detect_gzip(fastq)
        if z:
            my_open = gzip.open
            my_mode = mode + "b"
        else:
            my_open = open
            my_mode = mode
        return my_open(fastq, my_mode)

class FastqLine:
    """Makes a class out of the 4 parts of a fastq entry."""

    def __init__(self, lines):
        """Initialize all atributes"""
        self.lines = lines
        self.validate(lines)
        self.name = self.get_name(lines)
        self.comment = self.get_comment(lines)
        self.first_line = self.get_first_line(lines)
        self.sequence = self.get_seq(lines)
        self.quality = self.get_qual(lines)
        self.as_string = self.get_string()

    def validate(self, lines):
        """Make sure the fastq lines are the right number"""
        if len(lines) == 0:
            raise StopIteration
        if len(lines) != 4:
            raise NumberError("Fastq line is not length 4:", lines)

    def get_name(self, lines):
        """Returns the read name (without comment field)"""
        return lines[0].split()[0].rstrip()

    def get_comment(self, lines):
        """Returns comment field"""
        return lines[0].split()[1].rstrip()

    def get_first_line(self, lines):
        """Returns the name plus the comment field"""
        return lines[0].rstrip()

    def get_seq(self, lines):
        """Returns the read sequence"""
        return lines[1].rstrip()

    def get_qual(self, lines):
        """Returns the read quality score"""
        return lines[3].rstrip()

    def get_string(self):
        return '\n'.join(map(str, [self.first_line, self.sequence, "+", self.quality]))

class FastAreader:
    '''
    Class to provide reading of a file containing one or more FASTA
    formatted sequences:
    object instantiation:
    FastAreader(<file name>):

    object attributes:
    fname: the initial file name

    methods:
    readFasta() : returns header and sequence as strings.
    Author: David Bernick
    Date: April 19, 2013
    '''
    def __init__(self, fname):
        '''contructor: saves attribute fname '''
        self.fname = fname

    def readFasta(self):
        '''
        using filename given in init, returns each included FastA record
        as 2 strings - header and sequence.
        whitespace is removed, no adjustment is made to sequence contents.
        The initial '>' is removed from the header.
        '''
        header = ''
        sequence = ''

        with open(self.fname) as fileH:
            # initialize return containers
            header = ''
            sequence = ''

            # skip to first fasta header
            line = fileH.readline()
            while not line.startswith('>'):
                line = fileH.readline()
            header = line[1:].rstrip()

            # header is saved, get the rest of the sequence
            # up until the next header is found
            # then yield the results and wait for the next call.
            # next call will resume at the yield point
            # which is where we have the next header
            for line in fileH:
                if line.startswith('>'):
                    yield header, sequence
                    header = line[1:].rstrip()
                    sequence = ''
                else:
                    sequence += ''.join(line.rstrip().split()).upper()
        # final header and sequence will be seen with an end of file
        # with clause will terminate, so we do the final yield of the data
        yield header, sequence

# presumed object instantiation and example usage
# myReader = FastAreader ('testTiny.fa');
# for head, seq in myReader.readFasta() :
#     print (head,seq)

class BamReader:
    """
    This class makes it easy to open/write pysam files. Specifically, prevents
    duplicating code for deciding the mode type (r, rb, wb, w, wh).
    """

    def get_handle(myfile, mode, bam=False, header=None):
        """Decides how to open a sam/bam file."""

        # should be "r" or "w"
        file_mode = mode

        # Add binary if bam
        if bam:
            file_mode += "b"

        # if it's Sam and there's a header provided, write the header
        elif mode == "w" and header:
            file_mode += "h"

        # Return the file handle, with specified header
        if header:
            return pysam.AlignmentFile(myfile, mode=file_mode, header=header)

        # Return the file handle without headers
        return pysam.AlignmentFile(myfile, mode=file_mode)

class VcfLine:

    def __init__(self, line):
        self.line = line
        self.split_line = line.split()
        self.scaff = self.get_scaff(line)
        self.pos = self.get_pos(line)
        self.ref = self.get_ref(line)
        self.alt = self.get_alt(line)
        self.phase = self.get_phase(line)
        self.genotype_info = self.genotype_info(line)

    def get_scaff(self):
        return self.split_line[0].rstrip()

    def get_pos(self):
        return int(self.split_line[1])

    def get_ref(self):
        return self.split_line[3]

    def get_alt(self):
        return self.split_line[4]

    def get_phase(self):
        return self.split_line[9].split(":")[0]

    def genotype_info(self):
        return self.split_line[9]

    def is_het(self):
        if "1" in self.phase and "0" in self.phase:
            return True
        return False

def get_file_handle(my_file, mode):
    with open(my_file, mode) as f:
        return f



class BamTags:

    @staticmethod
    def mate_mapq(line):
        try:
            return line.opt("xm")
        except KeyError:
            return line.opt("MQ")

    @staticmethod
    def mate_start(line):
        pos = line.mate_pos
        if line.mate_is_reversed:
            pos = pos - line.opt("xs")
        return pos

    @staticmethod
    def orientation(line):
        return line.opt("xt")

    @staticmethod
    def junction(line):
        return line.opt("xj")
