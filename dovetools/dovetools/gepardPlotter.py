#!/usr/bin/env python3 
import sys
import os
import subprocess
import dovetools.fileCheck as check
from tempfile import NamedTemporaryFile
from dovetools.faidxWrapper import FaidxWrapper

class GepardPlotter:
    def __init__(self,
                 fasta1, #First fasta file
                 fasta2, #Second fasta file
                 cmd = "gepardcmd.sh", # Path to gepard shell script
                 matrix = "/opt/gepard-1.30/matrices/edna.mat", # Path to scoring matrix
                 path="."):  #Default base directory for plots
        self.fasta1 = FaidxWrapper(fasta1)
        self.fasta2 = FaidxWrapper(fasta2)
        self.fasta_fn1 = fasta1
        self.fasta_fn2 = fasta2
        if (not check.check_dir(path)) or (not check.check_file(matrix)):
            sys.exit(1)
        self.path = path
        self.matrix = matrix

    def dot_plot(self,
                 scaf1,
                 start1,
                 end1,
                 scaf2,
                 start2,
                 end2,
                 fn = None): #filename for plot
        if not fn:
            fn = "%s_%d_%d_%s_%d_%d_dotplot.png" % (scaf1,
                                                    start1,
                                                    end1,
                                                    scaf2,
                                                    start2,
                                                    end2)
        if not fn.startswith("/"):
            path = os.path.join(self.path,fn)
        else:
            path = fn

        if os.path.exists(path):
            print("ERROR: file already exists: %s" % (path), file=sys.stderr)
            sys.exit(1)

        if not self._verify_region(scaf1,
                                   start1,
                                   end1,
                                   scaf2,
                                   start2,
                                   end2):
            sys.exit(1)

        self._make_dot_plot(scaf1,
                            start1,
                            end1,
                            scaf2,
                            start2,
                            end2,
                            fn)



    def _verify_region(self,
                       scaf1,
                       start1,
                       end1,
                       scaf2,
                       start2,
                       end2):
        if scaf1 not in self.fasta1.lengths:
            print("Scaffold %s not found in fasta file %s." 
                    % (scaf1,self.fasta_fn1), file = sys.stderr)
            return False
        if scaf2 not in self.fasta2.lengths:
            print("Scaffold %s not found in fasta file %s." 
                    % (scaf2,self.fasta_fn2), file = sys.stderr)
            return False
        len1 = self.fasta1.get_length(scaf1)
        len2 = self.fasta2.get_length(scaf2)
        if start1 < 0 or end1 > len1:
            print("Invalid range for scaffold %s\n."
                  "Start: %d\tEnd: %d\t Length: %d" 
                  % (scaf1,start1,end1,len1),file = sys.stderr)
            return False
        if start2 < 0 or end2 > len2:
            print("Invalid range for scaffold %s\n."
                  "Start: %d\tEnd: %d\t Length: %d" 
                  % (scaf2,start2,end2,len2),file = sys.stderr)
            return False
        return True

    def _make_dot_plot(self,
                       scaf1,
                       start1,
                       end1,
                       scaf2,
                       start2,
                       end2,
                       fn):
        seq1_fh = NamedTemporaryFile(buffering=0) 
        seq2_fh = NamedTemporaryFile(buffering=0) 
        FaidxWrapper.write_fasta("%s_%d_%d" % (scaf1,start1,end1),
                                 self.fasta1.fetch(scaf1,start1,end1),
                                 seq1_fh)
        FaidxWrapper.write_fasta("%s_%d_%d" % (scaf2,start2,end2),
                                 self.fasta2.fetch(scaf2,start2,end2),
                                 seq2_fh)
        subprocess.check_call([self.cmd, 
                               "-seq1",seq1_fh.name,
                               "-seq2",seq2_fh.name,
                               "-outfile",fn,
                               "-matrix",self.matrix,
                               "-silent"])

