#!/usr/bin/env python3

import argh
from .utils import BamReader
from .exceptions import QnameError

class SamAddTags:
    """
    Adds custom tags to sam files. These are:
    MC:       CIGAR string for mate
    MQ:       Mapping quality of the mate
    R2:       Sequence of the mate (optional)
    Q2:       PHRED quality of the mate (optional)
    xt:       Type, ie innie, outtie, same, different scaff, unmapped
    xj:       T/F for whether the pair was trimmed for junction, for chicago pairs
    xc:       Barcode for Michigan pairs (instead of xj; optional)
    xs:       Five prime position of mate

    Positional arguments:
    my_input  -- sam or bam file
    my_output -- output file

    Keyword arguments:
    tag_file  -- file of read names stating if junction present (default None)
    in_bam    -- input is bam (default False)
    out_sam   -- output is sam (default False)
    is_barcode -- tag file is barcode instead of junction (default False)
    """


    def __init__(self, my_input, my_output, tag_file=None, in_bam=False, 
                 out_sam=False, is_barcode=False, extra=False, no_validate=False, unpaired=False, skip_unpaired=False):
        self.my_input = my_input
        self.my_output = my_output
        self.in_bam = in_bam
        self.out_sam = out_sam
        self.tag_file = tag_file
        self.is_barcode = is_barcode
        self.extra = extra
        self.no_validate = no_validate
        self.unpaired = unpaired
        self.skip_unpaired = skip_unpaired

    def add_tags(self):
        """Add tags to sam/bam files. 
        Use -S for output as sam, -b for input is bam.
        """

        # Open the input and output using pysam        
        
        with BamReader.get_handle(self.my_input, "r", bam=self.in_bam) as samfile, \
             BamReader.get_handle(self.my_output, "w", bam=not self.out_sam, \
                                  header=samfile.header) as outfile: 
            
            junction =  None
            if self.tag_file:
                junction = self.get_tags(self.tag_file)

            # Get the next pair of reads and their junction/barcode status.
            for a1, a2 in self.get_lines(samfile):

                # Add the tags
                self.append_tags(a1, a2, junction)
                
                # Write to file
                outfile.write(a1)
                if not self.unpaired:
                    outfile.write(a2)

    def get_tags(self, tags):
        junction = {}
        with open(tags, 'r') as handle:
            for line in handle:
                s = line.split()
                name = s[0]
                j = s[1]
                junction[name] = j
        return junction
                    
    
    def append_tags(self, a1, a2, junction):
        # Get I/O/S/ or unmapped     
        if not self.unpaired:
            a_type = self.get_type(a1, a2)


        if self.tag_file:
            try:
                a_junction = junction[a1.qname]

            except KeyError:
                raise QnameError("Tag File does not contain read", a1.qname)

        j_name = "xj"
        if self.is_barcode:
            j_name = "xc"

        if self.unpaired:
            custom_tags =  []
            if self.tag_file:
                custom_tags.append((j_name, a_junction,))
            for tag_name, tag_val in custom_tags:
                a1.setTag(tag_name, tag_val)
            return

        for r1, mate in zip([a1, a2],[a2, a1]):

            custom_tags = [("MQ", mate.mapq), ("xt", a_type),]
            if self.extra:
                custom_tags.append(("R2", mate.seq,))
                custom_tags.append(("Q2", mate.qual,))
                
            if not mate.is_unmapped:
                custom_tags.append(("MC", mate.cigarstring,))
                custom_tags.append(("xs", self.get_start(mate),))

            if self.tag_file:
                custom_tags.append((j_name, a_junction,))
            for tag_name, tag_val in custom_tags:
                r1.setTag(tag_name, tag_val)


    def get_lines(self, samfile):
        """Generator for the sam lines and the tag file lines"""
        while True:
            
            # Get the correct lines
            if self.unpaired:
                a1, a2 = next(samfile), None

            else:
                a1 = next(samfile)
                a2 = next(samfile)
                while a1.is_secondary or a1.is_supplementary:
                    a1 = next(samfile)
                while a2.is_secondary or a2.is_supplementary:
                    a2 = next(samfile)
                # Raise exception if they don't match up
                if a1.qname != a2.qname and not self.no_validate:

                    if self.skip_unpaired:
                        while a1.qname != a2.qname:
                            a1 = a2
                            a2 = next(samfile)
                    else:
                        raise QnameError("Not matched reads", a1.qname, a2.qname)
            
            yield a1, a2 

    def get_start(self, line):
        """Get the 5 prime end"""
        if line.is_reverse:
             return line.pos + line.infer_query_length()
        return line.pos

    def get_type(self, a1, a2):
        """Decide what type the pair is"""
        if a1.is_unmapped or a2.is_unmapped:
            return "U"
        if a1.tid != a1.rnext:
            return "D"
        if a1.is_reverse == a2.is_reverse:
            return "S"
        if self.is_innie(a1, a2):
            return "I"
        return "O"

    def is_innie(self, a1, a2):
        """Decide if pair is an innie"""
        if not a1.is_reverse:
            return self.get_start(a1) < self.get_start(a2)
        return self.get_start(a2) < self.get_start(a1)


@argh.arg("-o", "--output")
def sam_add_tags(my_input: "Input bam/sam", 
                 output: "Output" = "-", 
                 tags: "Tag file" = None, 
                 b: "Input is bam" = False, 
                 S: "Output is sam" = False,
                 is_barcode: "Tag file is barcodes" = False,
                 extra: "Add Mate Seq and Mate quality" = False,
                 no_validate: "Do not check if read pairs match up" = False,
                 unpaired: "Single reads, not paired-end" = False,
                 skip_unpaired=False):

    """Add custom tags to sam file. 

    These are:
    MC:       CIGAR string for mate
    MQ:       Mapping quality of the mate
    R2:       Sequence of the mate (optional, --extra)
    Q2:       PHRED quality of the mate (optional, --extra)
    xt:       Type, ie innie, outtie, same, different scaff, unmapped
    xj:       T/F for whether the pair was trimmed for junction, for chicago pairs
    xc:       Barcode for Michigan pairs (instead of xj; optional)
    xs:       Five prime position of mate

    """

    bam = SamAddTags(my_input, output, tag_file=tags, in_bam=b, out_sam=S,
                     is_barcode=is_barcode, extra=extra, no_validate=no_validate,
                     unpaired=unpaired, skip_unpaired=skip_unpaired)
    bam.add_tags()

if __name__ == "__main__":
    argh.dispatch_command(sam_add_tags)
