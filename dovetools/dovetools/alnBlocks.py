#!/usr/bin/env python3 
import sys
from operator import attrgetter
from collections import defaultdict

def abs_query_pos(aln,pos):
    tot_len = aln.query_length
    offset = 0
    op,length = aln.cigartuples[0]
    if op == 5:
        tot_len += aln.cigartuples[0][1]
        offset = length
    if aln.cigartuples[-1][0] == 5:
        tot_len += aln.cigartuples[-1][1]

    if aln.is_reverse:
        return tot_len - (offset + pos)
    else:
        return offset + pos

def query_aln_positions(aln):
    qstart = abs_query_pos(aln,aln.qstart)
    if aln.is_reverse:
        qend = qstart - aln.query_alignment_length
        qstart,qend = qend,qstart
    else:
        qend = qstart + aln.query_alignment_length
    return qstart,qend

class AlignmentBlock:
    def __init__(self,
                 target,
                 t_start,
                 t_end,
                 qname,
                 q_start,
                 q_end,
                 is_reverse,
                 mapq):
        self.target = target
        self.t_start = t_start
        self.t_end = t_end
        self.qname = qname
        self.q_start = q_start
        self.q_end = q_end
        self.is_reverse = is_reverse
        self.mapq = mapq

    def __str__(self):
        return "\t".join(map(str,[self.qname,
                                  self.q_start,
                                  self.q_end,
                                  self.q_length(),
                                  self.target,
                                  self.t_start,
                                  self.t_end,
                                  self.t_length(),
                                  self.is_reverse,
                                  self.mapq]))

    # Factory method for making new AlignmentBlocks from a BAM entry
    @classmethod
    def from_sam(cls,aln):
        qstart,qend = query_aln_positions(aln)
        return cls(aln.reference_name,
                   aln.reference_start,
                   aln.reference_end,
                   aln.qname,
                   qstart,
                   qend,
                   aln.is_reverse,
                   aln.mapq)

    def copy(self,other):
        self.__init__(**other.__dict__)
    
    def t_length(self):
        return self.t_end - self.t_start

    def q_length(self):
        return self.q_end - self.q_start

    def contains(self,other):
        if ((other.t_start >= self.t_start) and
            (other.t_end <= self.t_end)):
           return True
        else:
            return False

    def extend_block(self,other):

        if (other.target      != self.target or
            other.qname       != self.qname or
            other.is_reverse  != self.is_reverse):
            return False

        if self.contains(other):
            return True
        elif other.contains(self):
            self.copy(other)
            return True

        if other.t_start >= self.t_end:
            t_dist = other.t_start - self.t_end 
        else:
            t_dist = self.t_start - other.t_end
        if other.q_start >= self.q_end:
            q_dist = other.q_start - self.q_end
        else:
            q_dist = self.q_start - other.q_end
        thresh = max((other.t_end - other.t_start) / 5,
                     (self.t_end - self.t_start) / 5)

        if abs(t_dist - q_dist) > thresh:
            return False
        weighted_mapq = int((self.mapq * (self.t_end - self.t_start) 
                           + other.mapq * (other.t_end - other.t_start))
                           / ((self.t_end - self.t_start) + (other.t_end - other.t_start)))
        self.t_end = max(self.t_end,other.t_end)
        self.t_start = min(self.t_start,other.t_start)
        self.q_end = max(self.q_end,other.q_end)
        self.q_start = min(self.q_start,other.q_start)
        self.mapq = weighted_mapq
        return True



class ScaffoldBlocks:
    def __init__(self,scaf_name):
        self.scaf = scaf_name
        self.hits = defaultdict(list)
    
    def update_hits(self,block):
        if block.target != self.scaf:
            print("\t".join(["DIFF_SCAF:",
                             self.target,
                             block.target]),
                             file=sys.stderr)
            sys.exit(1)
        query_blocks = self.hits[block.qname]
        extended = False
        for q_block in query_blocks:
            extended = (extended or q_block.extend_block(block))
        if not extended:
            query_blocks.append(block)

    def get_sorted_blocks(self):
        all_blocks = []
        for query in self.hits:
            all_blocks.extend(self.hits[query])
        return sorted(all_blocks,key=attrgetter("t_start",
                                                "t_end"))
    def block_iter(self):
        for target in self.hits:
            for block in self.hits[target]:
                yield block

    def filter_blocks(self,min_length=10000,min_mapq=20):
        filtered_hits = defaultdict(list)
        for block in self.block_iter():
            if block.t_length() < min_length:
                continue
            if block.mapq < min_mapq:
                continue
            filtered_hits[block.qname].append(block)
        self.hits = filtered_hits

    def collapse_blocks(self):
        collapsed_hits = defaultdict(list)
        for qname in self.hits:
            if len(self.hits[qname]) == 1:
                collapsed_hits[qname].extend(self.hits[qname])
                continue
            for i in range(len(self.hits[qname])):
                block = self.hits[qname][i]
                extended = False
                for other in self.hits[qname][i+1:]:
                    if other.extend_block(block):
                        extended = True
                        break
                if not extended:
                    collapsed_hits[qname].append(block)
        self.hits = collapsed_hits

    def remove_overlaps(self,min_overlap=20000):
        dedup_blocks = defaultdict(list)
        sorted_blocks = self.get_sorted_blocks()
        if len(sorted_blocks) < 2:
            return
        print("BEFORE: ",file=sys.stderr)
        for block in sorted_blocks:
            print("\t".join(map(str,[block.qname,
                                     block.t_start,
                                     block.t_end])),
                                     file=sys.stderr)

        i = 0
        j = 1
        while i < len(sorted_blocks):
            curr_block = sorted_blocks[i]
            while j < len(sorted_blocks):
                block2 = sorted_blocks[j]
                overlap = target_overlap(curr_block,block2)
                if overlap == -1 or overlap > min_overlap:
                    #remove least desirable block (shortest and/or lowest mapq)
                    if curr_block.t_length() < block2.t_length():
                        curr_block = block2
                    j += 1
                else:
                    #keep current block and move on to the next one
                    break
            dedup_blocks[curr_block.qname].append(curr_block)
            i = j
        self.hits = dedup_blocks
        sorted_blocks = self.get_sorted_blocks()
        print("AFTER: ",file=sys.stderr)
        for block in sorted_blocks:
            print("\t".join(map(str,[block.qname,
                                     block.t_start,
                                     block.t_end])),
                                     file=sys.stderr)


    def adjacent_blocks(self,min_length=5000,max_distance=50000):
        sorted_blocks = self.get_sorted_blocks()
        if len(sorted_blocks) == 1:
            return
        i = 0
        j = 1
        while i < len(sorted_blocks) - 1:
            b1 = sorted_blocks[i]
            if b1.t_length() < min_length:
                i += 1
                continue
            j = i + 1
            while j < len(sorted_blocks):
                b2 = sorted_blocks[j]
                if b2.t_length() < min_length:
                    j += 1
                    continue
                if (b2.t_start - b1.t_end) < max_distance:
                    j += 1
                    yield (b1,b2)
                else:
                    break
            i += 1


    def nearby_regions(self,min_length=5000,max_distance=100000):
        sorted_blocks = self.get_sorted_blocks()
        if len(sorted_blocks) == 1:
            return
        i = 0
        j = 0
        while i < len(sorted_blocks) - 1:
            b1 = sorted_blocks[i]
            block_start = b1.t_start
            while True:
                j += 1
                b2 = sorted_blocks[j]
                if ((b2.t_start - b1.t_start) > max_distance or
                     j == (len(sorted_blocks) - 1)):
                    break

            if j > i+1 and any([b.t_length() >= min_length for b in sorted_blocks[i:j]]):
                yield sorted_blocks[i:j] 
            i = j
            j = i 
