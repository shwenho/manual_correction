from dovetools import models

def csv_write(f, a):
    print("\t".join(map(str, a)), file=f)

def libraries2csv(libraries=None, filename=None):
    if not libraries or not filename:
        return
    f = open(filename, "w")
    # library metrics
    print("Libary Metrics", file=f)
    csv_write(f, libraries[0].__library_stats_keys__(csv=True))
    for library in libraries:
        csv_write(f, library.__library_stats_values__())
    # write coverage with complexity tables
    print("Coverages With Complexity", file=f)
    for library in libraries:
        csv_write(f, library.__lc_extrap_seq__())
        csv_write(f, library.__lc_extrap_dup__())
        for cov in library.__lc_extrap_cov__():
            csv_write(f, cov)
    print("Assembly Statistics", file=f)
    # write scaffolds assembly statistics
    print("Scaffolds", file=f)
    csv_write(f, libraries[0].__scaffold_csv_keys__())
    for library in libraries:
        csv_write(f, library.__scaffold_csv_vals__())
    # write contigs assembly statistics
    print("Contigs", file=f)
    csv_write(f, libraries[0].__scaffold_csv_keys__())
    for library in libraries:
        csv_write(f, library.__scaffold_csv_vals__(contig=True))
    f.close()
