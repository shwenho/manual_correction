#!/usr/bin/env python3

import base64
import json
import os
from functools import wraps

def bpFormatter(x):
    if x >= 1e9:
        return "%0.2f Gbp" %(x/1e9)
    if x >= 1e6:
        return "%0.2f Mbp" %(x/1e6)
    if x >= 1e3:
        return "%0.2f kbp" %(x/1e3)
    if x < 0:
        return "%d" % (x)
    else:
        return "%d bp" % (x)

def count_interval_fmt(interval):
    if "start" not in interval:
        raise KeyError
    if "end" in interval:
        return "{} < Insert <= {}".format(bpFormatter(interval["start"]),
                                          bpFormatter(interval["end"]))
    else:
        return "{} < Insert".format(bpFormatter(interval["start"]))

def coverage_interval_fmt(interval):
    if "start" not in interval:
        raise KeyError
    if "end" in interval:
        return "{}-{}".format(bpFormatter(interval["start"]),
                                          bpFormatter(interval["end"]))
    else:
        return "{} <".format(bpFormatter(interval["start"]))

class Library:

    def __init__(self, metrics):
        self.metrics = metrics
        #self.library_stats_keys = None
        self.library_stats_values = None
        self.counts_largest_key = None
        self.inserts_largest_key = None

    @classmethod
    def from_json(cls, json_file, sample, general=None,results_only = False):
        with open(json_file) as json_handle:
            data = json.load(json_handle)
        if results_only:
            metrics = {"output" : data,
                       "project": "Development",
                       "lib"    : sample}
        else:
            metrics = dt_get_tree(data, "data|chicago|" + sample)

        if not general:
            try:
                general = data["data"]["general"]
            except KeyError:
                general = {}
        # default increments if none specified
        if not "count_intervals" in general.keys():
            general["count_intervals"] = [
                                            {"start":0, "end":1000},
                                            {"start":1000, "end":10000},
                                            {"start":10000, "end":100000},
                                            {"start":100000, "end":1000000},
                                            {"start":1000000}
                                         ]
        if not "coverage_intervals" in general.keys():
            general["coverage_intervals"] = [
                                                {"start":1000, "end":100000},
                                                {"start":100000, "end":1000000}
                                            ]
            
        combined = metrics.copy()
        combined.update(general)
        # change project name if sample is control
        if "control" in metrics:
            combined["project"] = "NA12878 control"

        # get largest cumulative counts and inserts
        instance = cls(combined)
        counts_largest_key = max((k for k, v in combined["output"]["bam"]["stats"]["cumulative"]["counts"].items()),
            key=int)
        inserts_largest_key = max((k for k, v in combined["output"]["bam"]["stats"]["cumulative"]["inserts"].items()),
            key=int)
        instance.counts_largest_key = counts_largest_key
        instance.inserts_largest_key = inserts_largest_key
        instance.counts_largest_val = combined["output"]["bam"]["stats"]["cumulative"]["counts"][counts_largest_key]
        instance.inserts_largest_val = combined["output"]["bam"]["stats"]["cumulative"]["inserts"][inserts_largest_key]
        return instance

    @classmethod
    def get_general(cls, json_file):
        with open(json_file) as json_handle:
            data = json.load(json_handle)
        try:
            return data["data"]["general"]
        except KeyError:
            return {}

    def __count_interval_tuple__(self, interval):
        counts =  self.metrics["output"]["bam"]["stats"]["cumulative"]["counts"]
        if "start" not in interval:
            raise KeyError
        # start is either largest val or val from a valid bin
        if int(interval["start"]) > int(self.counts_largest_key):
            start = self.counts_largest_val
        elif str(interval["start"]) in counts:
            start = counts[str(interval["start"])]
        else:
            raise KeyError
        # set end to largest bin unless it is specified
        end = self.counts_largest_val
        if "end" in interval:
            # end is either largest val or val from a valid bin
            if int(interval["end"]) > int(self.counts_largest_key):
                end = self.counts_largest_val
            elif str(interval["end"]) in counts:
                end = counts[str(interval["end"])]
            else:
                raise KeyError
        return (start, end)


    def __coverage_interval_tuple__(self, interval):
        inserts =  self.metrics["output"]["bam"]["stats"]["cumulative"]["inserts"]
        if "start" not in interval:
            raise KeyError
        # start is either largest val or val from a valid bin
        if int(interval["start"]) > int(self.inserts_largest_key):
            start = self.inserts_largest_val
        elif str(interval["start"]) in inserts:
            start = inserts[str(interval["start"])]
        else:
            raise KeyError
        # set end to largest bin unless it is specified
        end = self.inserts_largest_val
        if "end" in interval:
            # end is either largest val or val from a valid bin
            if int(interval["end"]) > int(self.inserts_largest_key):
                end = self.inserts_largest_val
            elif str(interval["end"]) in inserts:
                end = inserts[str(interval["end"])]
            else:
                raise KeyError
        return (start, end)


    def __library_stats_generate_count_interval_keys__(self):
        result = []
        for interval in self.metrics["count_intervals"]:
            result.append(count_interval_fmt(interval))
        return result


    def __library_stats_generate_coverage_interval_keys__(self, csv=False):
        result = []
        for interval in self.metrics["coverage_intervals"]:
            inter_fmt = coverage_interval_fmt(interval)
            result.append("Physical Coverage {}".format(inter_fmt))
            if csv:
                result.append("Physical Coverage {} @ 300M Pairs, 1 Gbp Genome, Complexity Corrected".format(inter_fmt))
            else:
                result.append("Physical Coverage {} @ 300M Pairs, <br>1 Gbp Genome, Complexity Corrected".format(inter_fmt))
        return result

    def __library_stats_generate_count_interval_vals__(self):
        counts =  self.metrics["output"]["bam"]["stats"]["cumulative"]["counts"]
        total_reads = self.__total_reads__()
        result = []
        for interval in self.metrics["count_intervals"]:
            start, end = self.__count_interval_tuple__(interval)
            result.append(self.__percentage__(end - start, total_reads))
        return result

    def __library_stats_generate_coverage_interval_vals__(self):
        inserts =  self.metrics["output"]["bam"]["stats"]["cumulative"]["inserts"]
        total_size = self.__scaffold_total_length__()
        gb = 1000000000
        result = []
        for interval in self.metrics["coverage_intervals"]:
            start, end = self.__coverage_interval_tuple__(interval)
            rc0 = (end - start) / total_size
            rc1 = self.__per_read_raw_coverage__(num_bases=gb, coverage=rc0)
            result.append(self.__coverage__(rc0))
            if "complexity_corrected_override" in self.metrics:
                result.append(self.metrics["complexity_corrected_override"])
            else:
                result.append(self.__coverage__(rc1 * self.__get_reads_by_complexity__(300000000)))
        return result

    def __library_stats_keys__(self, csv=False):
        result = []
        result += ["Project"]
        result += ["Library ID"]
        result += ["Total Read Pairs"]
        result += ["PCR/Optical Duplicates"]
        result += ["Unmapped"]
        result += ["Low Map Quality (Q < 20)"]
        result += ["Different Scaffold"]
        result += self.__library_stats_generate_count_interval_keys__()
        result += ["&nbsp"]
        result += ["Percent of Read Pairs containing a Chicago Junction"]
        result += ["Preseq Library Complexity @ 300M Pairs"]
        result += self.__library_stats_generate_coverage_interval_keys__(csv=csv)
        return result

    def __library_stats_values__(self, pos=None):
        if self.library_stats_values:
            if pos is not None:
                return self.library_stats_values[pos]
            return self.library_stats_values
        total = self.__total_reads__()
        result = []
        result += [self.__project__()]
        result += [self.__library_id__()]
        result += [self.__comma_sep__(self.__total_reads__())]
        result += [self.__percentage__(self.__duplicates__(), total)]
        result += [self.__percentage__(self.__unmapped__(), total)]
        result += [self.__percentage__(self.__low_mapq__(), total)]
        result += [self.__percentage__(self.__different_scaffold__(), total)]
        result +=  self.__library_stats_generate_count_interval_vals__()
        result += ["&nbsp"]
        result += [self.__percentage__(self.__raw_junctions__(), self.__total_reads__())]
        result += [self.__comma_sep__(self.__library_preseq__())]
        result += self.__library_stats_generate_coverage_interval_vals__()
        self.library_stats_values = result
        if pos is not None:
            return self.library_stats_values[pos]
        return self.library_stats_values

    @staticmethod
    def __raw_read_number_keys__():
        sort_order = [
            "Project",
            "Library ID",
            "PCR/Optical Duplicates",
            "Unmapped",
            "Low Map Quality",
            "Different Scaffold",
            "0 < Insert <= 2kbp",
            "2kbp < Insert <= 10kbp",
            "10kbp < Insert <= 100kbp",
            "100kbp < Insert <= 200kbp",
            "200kbp < Insert",
            "Total Read Pairs"
            ]
        return sort_order

    @staticmethod
    def __percentage_read_number_keys__():
        sort_order = [
            "Project",
            "Library ID",
            "PCR/Optical Duplicates",
            "Unmapped",
            "Low Map Quality",
            "Different Scaffold",
            "0 < Insert <= 2kbp",
            "2kbp < Insert <= 10kbp",
            "10kbp < Insert <= 100kbp",
            "100kbp < Insert <= 200kbp",
            "200kbp < Insert"
            ]
        return sort_order

    def __raw_read_number_values__(self, pos=None):
        values = [
            self.__project__(),
            self.__library_id__(),
            self.__comma_sep__(self.__duplicates__()),
            self.__comma_sep__(self.__unmapped__()),
            self.__comma_sep__(self.__low_mapq__()),
            self.__comma_sep__(self.__different_scaffold__()),
            self.__comma_sep__(self.__0_insert_2__()),
            self.__comma_sep__(self.__2_insert_10__()),
            self.__comma_sep__(self.__10_insert_100__()),
            self.__comma_sep__(self.__100_insert_200__()),
            self.__comma_sep__(self.__200_insert__()),
            self.__comma_sep__(self.__total_reads__())
            ]

        if pos is not None:
            return values[pos]
        return values

    def __comma_sep__(self, integer):
        return "{:,}".format(integer)

    def __percentage__(self, number, total):
        return "{}%".format(round(number*100/total, 3))

    def __percentage_read_number_values__(self, pos=None):
        total = self.__total_reads__()
        values = [
            self.__project__(),
            self.__library_id__(),
            self.__percentage__(self.__duplicates__(), total),
            self.__percentage__(self.__unmapped__(), total),
            self.__percentage__(self.__low_mapq__(), total),
            self.__percentage__(self.__different_scaffold__(), total),
            self.__percentage__(self.__0_insert_2__(), total),
            self.__percentage__(self.__2_insert_10__(), total),
            self.__percentage__(self.__10_insert_100__(), total),
            self.__percentage__(self.__100_insert_200__(), total),
            self.__percentage__(self.__200_insert__(), total),
            self.__percentage__(self.__total_reads__(), total)
            ]

        if pos is not None:
            return values[pos]
        return values

    @staticmethod
    def __library_metrics_keys__():
        sort_order = [
            "Project",
            "Library ID",
            "Percent of Chicago Reads that are Chimeric", 
            "Library Complexity (preseq)",
            "Library Complexity (Poisson)",
            "Raw Physical Coverage 2kbp-50kbp",
            "Model Physical Coverage 2kbp-50kbp",
            "Raw Physical Coverage scaled to 150M read pairs",
            "Model Physical Coverage scaled to 150M read pairs",
            "Raw Physical Coverage with Library Complexity scaled to 150M read pairs",
            "Model Physical Coverage with Library Complexity scaled to 150M read pairs",
            "Raw Physical Coverage per 1Gbp with Library Complexity scaled to 150M read pairs",
            "Model Physical Coverage per 1Gbp with Library Complexity scaled to 150M read pairs",
            "Percent of Read Pairs containing a Chicago Junction",
            "Percent of Reads beginning with GATC" 
            ]
        return sort_order

    def __library_metrics_values__(self, pos=None):

        well_mapped = self.__get_good_pairs_percent__() * self.__total_reads__()
        scale = 150000000/self.__total_reads__()
        values = [
            self.__project__(),
            self.__library_id__(),
            self.__percentage__(self.__chimeric__(), self.__combined_junctions__()),
            self.__comma_sep__(self.__library_preseq__()),
            self.__comma_sep__(self.__library_poisson__()),
            self.__coverage__(self.__raw_coverage__()),
            self.__coverage__(self.__model_coverage__()),
            self.__coverage__(self.__per_read_raw_coverage__()*well_mapped*scale),
            self.__coverage__(self.__per_read_model_coverage__()*well_mapped*scale),
            self.__coverage__(self.__raw_coverage_complexity__(150000000)),
            self.__coverage__(self.__model_coverage_complexity__(150000000)),
            self.__coverage__(self.__raw_coverage_complexity__(150000000, num_bases=1000000000)),
            self.__coverage__(self.__model_coverage_complexity__(150000000, num_bases=1000000000)),
            self.__percentage__(self.__raw_junctions__(), self.__total_reads__()),
            self.__percentage__(self.__begin_junctions__(), self.__total_reads__() * 2)
            ]

        if pos is not None:
            return values[pos]
        return values

    def __coverage__(self, coverage):
        rounded = round(coverage, 3)
        return "{} X".format(rounded)

    def __raw_junctions__(self):
        return self.metrics["output"]["bam"]["stats"]["raw_junctions"]        

    def __begin_junctions__(self):
        return self.metrics["output"]["bam"]["stats"]["begin_junctions"]        

    def __raw_coverage__(self):
        return self.metrics["output"]["bam"]["stats"]["raw_coverage"]        

    def __model_coverage__(self):
        return self.metrics["output"]["bam"]["stats"]["model_coverage"]        

    def __raw_coverage_complexity__(self, number, num_bases=None):
        num_reads = self.__get_reads_by_complexity__(number)
        per_read_coverage = self.__per_read_raw_coverage__(num_bases=num_bases)
        return num_reads * per_read_coverage

    def __model_coverage_complexity__(self, number, num_bases=None, estimate="best"):
        # number = reads sequenced
        # bases = size of hypothetical genome

        # number of unique reads
        num_reads = self.__get_reads_by_complexity__(number, estimate=estimate)
        per_read_coverage = self.__per_read_model_coverage__(num_bases=num_bases)
        return num_reads * per_read_coverage
        
    def __get_good_pairs_percent__(self):
        good_num = self.__total_reads__()
        good_num -= self.__duplicates__()
        good_num -= self.__low_mapq__()
        good_num -= self.__unmapped__()
        
        good_denom = self.__total_reads__()
        good_denom -= self.__duplicates__()
        return (good_num / good_denom)

    def __get_duplicate_rate__(self,number):
        num_mapped = number * self.__get_good_pairs_percent__()
        num_unique = self.__get_reads_by_complexity__(number)
        if number == 0: dup_rate = 0
        else: dup_rate = 1.0 - (num_unique/float(num_mapped))
        return dup_rate

    def __get_reads_by_complexity__(self, number, estimate="best"):
        lower_reads = 0
        upper_reads = 0
        lower_distinct = 0
        upper_distinct = 0 
        number = number * self.__get_good_pairs_percent__()
        if "control" in self.metrics:
            lc_extrap = os.path.join(os.path.dirname(__file__), "..", "bin", "static", "lc_extrap_paired.out")
        else:
            lc_extrap = self.metrics["output"]["bam"]["lc_extrap_paired"]
        with open(lc_extrap) as f:
            for line in f:
                if line.startswith("TOTAL_READS"):
                    continue
                try:
                    reads, distinct, lower, upper = list(map(float, line.split()))
                except ValueError:
                    return 0
                    #raise ValueError(lc_extrap)
                if reads <= number:
                    lower_reads = reads
                    if estimate=="best":
                        lower_distinct = distinct
                    elif estimate == "lower":
                        lower_distinct = lower
                    elif estimate == "upper":
                        lower_distinct = upper
                    else:
                        raise ValueError(estimate)
                elif reads >= number:
                    upper_reads = reads
                    upper_distinct = distinct
                    if estimate=="best":
                        upper_distinct = distinct
                    elif estimate == "lower":
                        upper_distinct = lower
                    elif estimate == "upper":
                        upper_distinct = upper
                    else:
                        raise ValueError(estimate)
                    break
        slope = (upper_distinct - lower_distinct) / (upper_reads - lower_reads)
        distinct_at_target = lower_distinct + slope * (number - lower_reads)
        return distinct_at_target

    def __per_read_raw_coverage__(self, num_bases=None, coverage=None):
        if not coverage:
            coverage = self.metrics["output"]["bam"]["stats"]["raw_coverage"]
        reads = self.__total_reads__() - self.__duplicates__() - self.__unmapped__() - self.__low_mapq__()
        if num_bases:
            scale = self.__genome_size__() / num_bases
        else:
            scale = 1
        return (scale * coverage) / reads

    def __genome_size__(self):
        return self.metrics["output"]["bam"]["contig_stats"]["scaffolds"]["Total length"]

    def __per_read_model_coverage__(self, num_bases=None):
        coverage = self.metrics["output"]["bam"]["stats"]["model_coverage"]
        reads = self.__total_reads__() - self.__duplicates__() - self.__unmapped__() - self.__low_mapq__()
        if num_bases:
            scale = self.__genome_size__() / num_bases
        else:
            scale = 1
        return (scale * coverage) / reads

    def __chimeric__(self):
        return self.metrics["output"]["bam"]["stats"]["mapped_chimeras"]

    def __mapped_junctions__(self):
        return self.metrics["output"]["bam"]["stats"]["mapped_junctions"]

    def __combined_junctions__(self):
        return self.__chimeric__() + self.__mapped_junctions__()

    def __library_preseq__(self):
        return int(self.metrics["output"]["bam"]["stats"].get("library_complexity_preseq_PE", -1))

    def __library_poisson__(self):
        return int(self.metrics["output"]["bam"]["stats"].get("library_complexity_ZTP_PE", -1))

    def __project__(self):
        return self.metrics["project"]

    def __library_id__(self):
        return self.metrics["lib"]

    def __duplicates__(self):
        return self.metrics["output"]["bam"]["stats"]["duplicates"]

    def __unmapped__(self):
        return self.metrics["output"]["bam"]["stats"]["unmapped"]

    def __low_mapq__(self):
        return self.metrics["output"]["bam"]["stats"]["low_mapq"]

    def __different_scaffold__(self):
        return self.metrics["output"]["bam"]["stats"]["different_scaffold"]

    def __0_insert_2__(self):
        return self.metrics["output"]["bam"]["stats"]["0_to_2kb_insert"]

    def __2_insert_10__(self):
        return self.metrics["output"]["bam"]["stats"]["2kb_to_10kb_insert"]

    def __10_insert_100__(self):
        return self.metrics["output"]["bam"]["stats"]["10kb_to_100kb_insert"]

    def __100_insert_200__(self):
        return self.metrics["output"]["bam"]["stats"]["100kb_to_200kb_insert"]

    def __200_insert__(self):
        return self.metrics["output"]["bam"]["stats"]["200kb_to_inf_insert"]

    def __total_reads__(self):
        return self.metrics["output"]["bam"]["stats"]["total_read_pairs"]

    def __read_length__(self):
        return "{} bp".format(self.metrics["output"]["bam"]["stats"].get("read_length", "missing"))

    def __lc_extrap_keys__(self):
        coverages = []
        for interval in self.metrics["coverage_intervals"]:
            coverages.append("Estimated Coverage {}".format(coverage_interval_fmt(interval)))
        result  = [self.__project__() + " " + self.__library_id__()]
        result += ["Estimated Duplicate Rate"]
        result += coverages
        return result

    # csv generation
    def __lc_extrap_seq__(self):
        seq  = [self.__project__() + " " + self.__library_id__()]
        for sequence_number in range(0, 600000000 + 1, 50000000):
            seq += [self.__comma_sep__(sequence_number)]
        return seq

    # csv generation
    def __lc_extrap_dup__(self):
        dup  = ["Estimated Duplicate Rate"]
        for sequence_number in range(0, 600000000 + 1, 50000000):
            dup += [self.__percentage__(self.__get_duplicate_rate__(sequence_number),1.0)]
        return dup

    # csv generation; returns each coverage column in a list
    def __lc_extrap_cov__(self):
        all_coverages = []
        for interval in self.metrics["coverage_intervals"]:
            coverages = []
            coverages.append("Estimated Coverage {}".format(coverage_interval_fmt(interval)))
            interval = self.__coverage_interval_tuple__(interval)
            for sequence_number in range(0, 600000000 + 1, 50000000):
                rc0 = (interval[1] - interval[0]) / self.__genome_size__()
                rc1 = self.__per_read_raw_coverage__(coverage=rc0)
                coverages.append(self.__coverage__(rc1 * self.__get_reads_by_complexity__(sequence_number)))
        return all_coverages

    def __lc_extrap_values__(self):
        for sequence_number in range(0, 600000000 + 1, 50000000):
            coverages = []
            for interval in self.metrics["coverage_intervals"]:
                interval = self.__coverage_interval_tuple__(interval)
                rc0 = (interval[1] - interval[0]) / self.__genome_size__()
                rc1 = self.__per_read_raw_coverage__(coverage=rc0)
                coverages.append(self.__coverage__(rc1 * self.__get_reads_by_complexity__(sequence_number)))
            result  = [self.__comma_sep__(sequence_number)]
            result += [self.__percentage__(self.__get_duplicate_rate__(sequence_number),1.0)]
            result += coverages
            yield result

    def __insert_distribution__(self):
        png = self.metrics["output"]["inserts"]
        with open(png, 'rb') as f:
            bytes_png = f.read()
        data = base64.b64encode(bytes_png)
        return "data:image/png;base64,{}".format(data.decode("utf-8"))

    def __junction_insert_distribution__(self):
        png = self.metrics["output"]["inserts_junctions"]
        with open(png, 'rb') as f:
            bytes_png = f.read()
        data = base64.b64encode(bytes_png)
        return "data:image/png;base64,{}".format(data.decode("utf-8"))

    # csv generation
    def __scaffold_csv_keys__(self):
        fields = [
            "Library",
            "assembly ID",
            "# (>= 0 bp)",
            "# (>= 1000 bp)",
            "L50",
            "L75",
            "N50",
            "N75",
            "Largest contig",
            "Total length",
            "Total length (>= 0 bp)",
            "Total length (>= 1000 bp)",
            "# N's per 100 kbp",
            "GC (%)",
            ]
        return fields

    # csv generation
    def __scaffold_csv_vals__(self, contig=False):
        values = [
            self.__project__() + " " + self.__library_id__(),
            self.metrics["output"]["bam"]["contigs"],
            self.__comma_sep__(self.__num_scaffolds_0__(contig=contig)),
            self.__comma_sep__(self.__num_scaffolds_1000__(contig=contig)),
            self.__comma_sep__(self.__scaffold_l50__(contig=contig)),
            self.__comma_sep__(self.__scaffold_l75__(contig=contig)),
            self.__comma_sep__(self.__scaffold_n50__(contig=contig)) + " bp",
            self.__comma_sep__(self.__scaffold_n75__(contig=contig)) + " bp",
            self.__comma_sep__(self.__scaffold_largest__(contig=contig)),
            self.__comma_sep__(self.__scaffold_total_length__(contig=contig)),
            self.__comma_sep__(self.__scaffold_total_length_0__(contig=contig)),
            self.__comma_sep__(self.__scaffold_total_length_1000__(contig=contig)),
            self.__comma_sep__(self.__scaffold_Ns__(contig=contig)),
            self.__percentage__(self.__scaffold_GC__(contig=contig), 100),
            ]
        return values

    @staticmethod
    def __scaffold_keys__():
        sort_order = [
            "assembly ID",
            "# (>= 0 bp)",
            "# (>= 1000 bp)",
            "L50",
            "L75",
            "N50",
            "N75",
            "Largest contig",
            "Total length",
            "Total length (>= 0 bp)",
            "Total length (>= 1000 bp)",
            "# N's per 100 kbp",
            "GC (%)",
            ]
        return sort_order

    def __scaffold_values__(self, pos=None, contig=False):
        values = [
            os.path.basename(self.metrics["output"]["bam"]["contigs"]),
            self.__comma_sep__(self.__num_scaffolds_0__(contig=contig)),
            self.__comma_sep__(self.__num_scaffolds_1000__(contig=contig)),
            self.__comma_sep__(self.__scaffold_l50__(contig=contig)),
            self.__comma_sep__(self.__scaffold_l75__(contig=contig)),
            self.__comma_sep__(self.__scaffold_n50__(contig=contig)) + " bp",
            self.__comma_sep__(self.__scaffold_n75__(contig=contig)) + " bp",
            self.__comma_sep__(self.__scaffold_largest__(contig=contig)),
            self.__comma_sep__(self.__scaffold_total_length__(contig=contig)),
            self.__comma_sep__(self.__scaffold_total_length_0__(contig=contig)),
            self.__comma_sep__(self.__scaffold_total_length_1000__(contig=contig)),
            self.__comma_sep__(self.__scaffold_Ns__(contig=contig)),
            self.__percentage__(self.__scaffold_GC__(contig=contig), 100),
            ]

        if pos is not None:
            return values[pos]
        return values

    def __scaffold_Ns__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return float(self.metrics["output"]["bam"]["contig_stats"][contig_type]["# N's per 100 kbp"])

    def __num_scaffolds__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["# contigs"])

    def __num_scaffolds_0__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["# contigs (>= 0 bp)"])

    def __num_scaffolds_1000__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["# contigs (>= 1000 bp)"])

    def __scaffold_GC__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return float(self.metrics["output"]["bam"]["contig_stats"][contig_type]["GC (%)"])

    def __scaffold_l50__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["L50"])
        
    def __scaffold_l75__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["L75"])

    def __scaffold_largest__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["Largest contig"])

    def __scaffold_n50__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["N50"])

    def __scaffold_n75__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["N75"])

    def __scaffold_total_length__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["Total length"])

    def __scaffold_total_length_0__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["Total length (>= 0 bp)"])

    def __scaffold_total_length_1000__(self, contig=False):
        contig_type = "scaffolds"
        if contig:
            contig_type = "contigs"
        return int(self.metrics["output"]["bam"]["contig_stats"][contig_type]["Total length (>= 1000 bp)"])

    def __fastqc_local__(self, read=1):
        fastqc = self.metrics["output"]["fastqc_r{}".format(read)]
        replace_link = "/projects/"
        try:
            start = fastqc.index(replace_link) + len(replace_link)
        except ValueError:
            start = 0
        return fastqc[start:]

    def __cumulative_counts__(self):
        return self.metrics["output"]["bam"]["stats"]["cumulative"]["counts"]


    def __cumulative_inserts__(self):
        return self.metrics["output"]["bam"]["stats"]["cumulative"]["inserts"]

def dt_get_tree(jsn, key) :
    """ 
    Get the json subtree corresponding to key in first|second|third 
    hierarchy.
    """
    jj = jsn
    tokens = key.split('|');
    tokens.reverse();
    result = None;
    while tokens :
        pattern = tokens.pop();
        if pattern :
            jj = jsonfind(jj, pattern);
    return jj;

def jsonfind(d, pattern) :
    """ 
    Recurse through the tree looking for keys that
    match the specfied pattern/key. Return None
    if no keys match else return the value of the key
    that matches.
    """
    if isinstance(d, dict):
        for key in d.keys() :
            if key == pattern :
                return(d[key]);
            elif isinstance(d[key], dict) :
                value = jsonfind(d[key], pattern);
                if value != None :
                    return value;
    return None;

