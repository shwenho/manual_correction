#!/usr/bin/env python3 
import sys
import os

def check_dir(path,silent=False):
    if not os.path.isdir(path):
        if not silent:
            print("ERROR: Specified path does not exist or "
                  "is not a directory:\n%s" % (path), file=sys.stderr)
        return False
    return True


def check_file(path,silent=False):
    if not os.path.isfile(path):
        if not silent:
            print("ERROR: Specified path does not exist or "
                  "is not a file:\n%s" % (path), file=sys.stderr)
        return False
    return True

def check_exists(path,silent=False):
    if not os.path.exists(path):
        if not silent:
            print("ERROR: Specified path does not exist:\n%s" % (path), file=sys.stderr)
        return False
    return True
