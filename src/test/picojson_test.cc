#include <string>
#include <vector>
#include <iostream>
#include "picojson.h"

#include "gtest/gtest.h"

const std::string model_json = "{\"model_type\": \"chicago\", \"N\": 338657.5978329083, \"beta\": [0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05], \"Nn\": 7138.1194359941255, \"fn\": \"338657.5978329083*(7.02589624611152e-12+(1.0-0.02107768873833456)*(0.0010213324568692176*exp(-(x)/500.0) + 0.000211434184175803*exp(-(x)/900.8241153272055) + 2.4126299880631892e-05*exp(-(x)/1622.9681735100849) + 3.2339077161572e-05*exp(-(x)/2924.0177382128654) + 2.5567888732372052e-05*exp(-(x)/5268.0513844533225) + 3.2092501138910775e-06*exp(-(x)/9491.175455796849) + 0.0*exp(-(x)/17099.759466766965) + 0.0*exp(-(x)/30807.751387916716) + 0.0*exp(-(x)/55504.730778481135) + 0.0*exp(-(x)/99999.99999999997)))\", \"alpha\": [0.5106662284346087, 0.19046501191009718, 0.039156216850825716, 0.0945600352578711, 0.1346929516341211, 0.030459555912476236, 0.0, 0.0, 0.0, 0.0], \"pn\": 0.02107768873833456, \"G\": 32155272.0}";

TEST(PicoJsonTest, ArrayTypeTest) {
  picojson::value v;
  std::string err = picojson::parse(v, model_json);
  ASSERT_TRUE(err.empty());
  picojson::value &N = v.get("N");
  ASSERT_NEAR(N.get<double>(), 338657.5978329083, 0.001);
  std::vector<double> beta_vec = v.get_vector<double>("beta");
  ASSERT_EQ(10, beta_vec.size());
  ASSERT_NEAR(beta_vec[0], .002, 1e-6);
  ASSERT_NEAR(beta_vec[8], 1.8016482306544117e-05, 1e-6);
  ASSERT_NEAR(beta_vec[9], 1.0000000000000003e-05, 1e-6);

  if (v.contains("beta")) {
      fprintf(stdout, "model json string contains 'beta'\n");
  } else {
      fprintf(stdout, "model json string does not contain 'beta'\n");
  }

  if (v.contains("model_type")) {
      fprintf(stdout, "model json string contains 'model_type'\n");
      std::string model_type = v.get("model_type").get<std::string>();
      if (model_type=="chicago") {
	fprintf(stdout, "Chicago model\n");
      }
  } else {
      fprintf(stdout, "model json string does not contain 'model_type'\n");
  }

  if (v.contains("fn")) {
    fprintf(stdout, "model json string contains 'fn'\n");
    std::string fn = v.get("fn").get<std::string>();
    std::cout << fn << "\n";
    //    fprintf(stdout,fn);

  }

}
