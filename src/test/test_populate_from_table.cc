#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "link_range_iterator.h"
#include "hirise_threads.h"
#include "oo_requests.h"
#include <fenv.h>



inline bool file_exists (const std::string& name) {
  ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }   
}

int main(int argc, char * argv[]) {
  
  bool layout=false;
  bool dump_reads=false;
  bool restore_dump=false;
  bool read_bed=false;
  bool useDepthPenalty=false;
  int pad=0;
  int minsep=0;
  int mapq = 0;
  int n_threads=16;
  std::string hrafile;
  std::vector<std::string> model_files;
  std::string reads_dump_out;
  std::string output_file;
  std::string reads_dump_in ;
  std::string bedfile ;
  std::vector<std::string> bamfiles;
  std::string shotfile = "";
  std::string oo_req_file = "";

  
  CustomPairLM model;
  ChicagoLinks links;
  links.PopulateFromTable( argv[1] , false, &model, 0 );
  links.PrintModelInfo();

  exit(0);

}
