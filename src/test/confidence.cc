#include <vector>
#include <string> 
#include <iostream>
#include <fstream>
#include <math.h>
#include "generic_setup.h"
#include "chicago_links.h"
#include "scaffold_join_score.h"


int main(int argc, char** argv) {
  ChicagoLinks links;
  PathSet scaffolds;

  try {  
    auto chrono_start = std::chrono::high_resolution_clock::now();

    int mapq;
    int nthreads;
    std::string hra;
    std::string read_dump;
    std::vector<std::string> model_files;
    std::vector<std::string> bam_files;
    std::string mask;
    std::string output;

    // set all those variables
    parse_command_line(argc, argv, mapq, hra, nthreads, read_dump, model_files, bam_files, mask, output);

    // initialize scaffolds, reads, sort by broken contig coordinates, and initialize indexes
    set_links_and_scaffolds(links, scaffolds, mapq, hra, read_dump, model_files, bam_files, mask, chrono_start);

    // write to file the orientation confidence of all the contigs in scaffolds
    orientation_confidence(scaffolds, links, output);
  }
  catch (const std::exception &ex) {
    { std::cerr << "error: " << ex.what() << "\n"; }
    return 1;
  }
  return 0;
}
