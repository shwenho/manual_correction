#include <unordered_map>
#include <boost/algorithm/searching/knuth_morris_pratt.hpp>
#include "model_parser.h"
#include "simulate_chicago_reads.h"


template <>
void ReadSimulatorThread<HRRealChicagoReadSimulatorTask>::gen_reads(
        const HRRealChicagoReadSimulatorTask& t) {
    const RestrictionSites& sites = m_sim->get_sites();
    for (int i = t.chunk_start; i < t.chunk_end; ++i) {
        for (int j = i+1; j < m_sim->get_last_valid_site(i); ++j) {
            int64_t x = sites[j] - sites[i];
            assert(x <= MAX_RANGE);

            double n0 = m_sim->get_pnorm() * m_sim->get_likelihood(x);
            std::poisson_distribution<int64_t> distribution(n0);
            int64_t n = distribution(m_generator);

            for (int64_t nn = 0; nn < n;) {
                // switch to normal sample
                int frag_length = m_frag_distribution(m_generator);
                if (frag_length < MIN_FRAG_LEN || frag_length > MAX_FRAG_LEN)
                    continue;

                int frag_1 = int( drandt() * frag_length );
                int frag_2 = int( frag_length ) - frag_1 ;

                int s1 = 2 * int(drandt()*2) - 1;
                int s2 = 2 * int(drandt()*2) - 1;

                int64_t a = sites[i] + s1*frag_1 ;
                int64_t b = sites[j] + s2*frag_2 ;

                // handle inference between MboI sites within fragment size...
                if (a > sites[j] || b < sites[i])
                    continue;

                // handle inference between MboI sites within fragment size...
                if (a < 0 || b < 0)
                    continue;

                std::string construct_seq =
                    construct_part( sites[i], s1, frag_1 ) +
                    rev_comp(construct_part( sites[j], s2, frag_2 ));
                gen_end_sequence_pair(construct_seq);

                // Increment counter
                ++nn;
            }
        }
    }
}

template <>
void ReadSimulatorThread<HRIntraRegionNoiseSimulatorTask>::gen_reads(
        const HRIntraRegionNoiseSimulatorTask& t) {
    const RestrictionSites& sites = m_sim->get_sites();
    for (int64_t nn = t.chunk_start; nn < t.chunk_end;) {
        int i = int(drandt() * sites.size());
        int j = int(drandt() * sites.size());
        if ( i == j )
            continue;

        // switch to normal sample
        int frag_length = m_frag_distribution(m_generator);
        if (frag_length < MIN_FRAG_LEN || frag_length > MAX_FRAG_LEN)
            continue;

        int frag_1 = int( drandt() * frag_length );
        int frag_2 = int( frag_length ) - frag_1 ;

        int s1 = 2 * int(drandt()*2) - 1;
        int s2 = 2 * int(drandt()*2) - 1;

        std::string construct_seq =
            construct_part( sites[i], s1, frag_1 ) +
            rev_comp(construct_part( sites[j], s2, frag_2 ));
        gen_end_sequence_pair(construct_seq);

        // Increment counter
        ++nn;
    }
}

template <>
void ReadSimulatorThread<HRShortInnieSimulatorTask>::gen_reads(
        const HRShortInnieSimulatorTask& t) {
    const std::string& seq = m_sim->get_seq();

    for (int64_t nn = t.chunk_start; nn < t.chunk_end;) {
        // switch to normal sample
        int frag_length = m_frag_distribution(m_generator);
        if (frag_length < MIN_FRAG_LEN || frag_length > MAX_FRAG_LEN)
            continue;

        int64_t a = int64_t(drandt() * (seq.length()-frag_length));
        int64_t b = a + frag_length - 1;
        if (!m_sim->region_in_ref(a, b))
            continue;

        std::string construct_seq = seq.substr(a, frag_length);
        gen_end_sequence_pair(construct_seq);

        // Increment counter
        ++nn;
    }
}

template <typename T>
void ReadSimulatorThread<T>::gen_end_sequence_pair(
        std::string construct_seq) {
    // Get read length from ChicagoReadSimulator task
    const int& read_length = m_sim->get_read_length();

    std::string r1, r2;
    if ( construct_seq.length() >= read_length ) {
        r1 = construct_seq.substr(0, read_length);
        r2 = rev_comp(
                construct_seq.substr(construct_seq.length()-read_length,
                read_length));
    } else {
        r1 = construct_seq;
        r2 = rev_comp(construct_seq);
    }

    // Coin toss
    if ( drandt() < 0.5 )
        r1.swap(r2);

    std::ostringstream buf;
    buf << "@read" << ":" << m_id << ":" << m_generated << "/1\n"
        << r1 << "\n"
        << "+\n"
        << std::string(r1.length(), 'M') << "\n"
        << "@read" << ":" << m_id << ":" << m_generated << "/2\n"
        << r2 << "\n"
        << "+\n"
        << std::string(r2.length(), 'M') << "\n";

    // Write interleaved read pair to stdout
    std::cout << buf.str();

    // Increment generated reads count
    ++m_generated;
}

template <typename T>
std::string ReadSimulatorThread<T>::construct_part(int64_t pos,
                                                   int s,
                                                   int frag_len) {
    const std::string& seq = m_sim->get_seq();
    const std::string& motif = m_sim->get_motif();
    return s == 1 ?
        rev_comp(seq.substr(pos, frag_len)) :
        seq.substr(pos-frag_len, frag_len+motif.length());
}

template <typename T>
std::string ReadSimulatorThread<T>::rev_comp(std::string s) {
    std::reverse(s.begin(), s.end());
    for (std::string::iterator it = s.begin(); it != s.end(); ++it) {
        switch (*it) {
            case 'A': *it = 'T'; break;
            case 'T': *it = 'A'; break;
            case 'C': *it = 'G'; break;
            case 'G': *it = 'C'; break;
        }
    }

    return s;
}

void Reference::read(std::ifstream& stream) {
    std::string name, line, padding(INTRA_CONTIG_PAD_LEN, 'N');
    std::ostringstream buf;

    // Seek out first record in the file.
    bool header = false;
    while (std::getline(stream, line))
        if (header = line.compare(0, 1, ">") == 0)
            break;

    if (!header)
        throw std::runtime_error("No FASTA records found in reference.");

    int64_t start_pos = m_cursor;
    do {
        // Extract the name of the record (up to first whitespace)
        name = line.substr(1, line.find_first_of(" \t\r\n\v\b\f")-1);
        FASTAIndex idx(name);
        idx.start = m_cursor;
        while (true) {
            std::getline(stream, line);
            if (line.compare(0, 1, ">") == 0 || stream.eof()) {
                idx.end = start_pos + buf.tellp();
                // Keep track of pre-padded length
                m_length += idx.end - idx.start;
                // Pad between chromosomes
                m_cursor = idx.end + INTRA_CONTIG_PAD_LEN;
                buf << padding;
                break;
            }
            buf << std::uppercase << line;
        }
        m_records.push_back(idx);
    } while (line.compare(0, 1, ">") == 0);

    // Store the concatenated reference
    m_seq += buf.str();
    m_haplotypes++;
}

bool ChicagoReadSimulator::region_in_ref(int64_t start, int64_t end) const {
    std::vector<FASTAIndex>::const_iterator itr;
    for (itr = m_ref->m_records.begin();
         itr != m_ref->m_records.end();
         ++itr) {
        if (itr->contains(start) && itr->contains(end))
            return true;
    }
    return false;
}

void ChicagoReadSimulator::find_sites(int min_site_sep/*=MIN_SITE_SEP*/) {
    const std::string& seq = get_seq();

    // Identify all restriction sites in reference sequence
    RestrictionSites all_sites;
    for (std::string::const_iterator it = seq.begin();; ++it) {
        it = boost::algorithm::knuth_morris_pratt_search(it, seq.end(),
                                                         m_motif.begin(),
                                                         m_motif.end());
        if (it == seq.end())
            break;
        all_sites.push_back(std::distance(seq.begin(), it));
    }
    std::cerr << "Total restriction sites found: " << all_sites.size() << "\n";

    // Cluster restriction sites that the minimum distance between clusters
    // is enforced.
    RestrictionSiteClusters clusters;

    // Add first cluster
    clusters.push_back(RestrictionSiteCluster(all_sites[0]));

    // Cluster remaining sites
    for (RestrictionSites::const_iterator it = all_sites.begin()+1;
         it != all_sites.end(); ++it) {
        RestrictionSiteCluster& cluster = clusters.back();
        if (cluster.is_valid_member(*it, min_site_sep))
            cluster.add(*it);
        else
            clusters.push_back(RestrictionSiteCluster(*it));
    }

    // Populate container of effective restriction sites
    for (RestrictionSiteClusters::const_iterator it = clusters.begin();
         it != clusters.end(); ++it)
        m_sites.push_back(it->centroid());
    std::cerr << "Effective restriction sites: " << m_sites.size() << "\n";

    // Identify all possible valid forward site pairs (upper triangle)
    int i = 0, j = 0;
    for (; i < m_sites.size(); ++i)
        for (; j <= m_sites.size(); ++j)
            if (j == m_sites.size() || m_sites[j] > m_sites[i]+MAX_RANGE) {
                m_site_pairs += j-i-1;
                m_max_fw_sites[i] = j;
                break;
            }
    std::cerr << "Possible site pairs: " << m_site_pairs << "\n";

    // Identify all possible reverse site pairs (bottom triangle)
    i = m_sites.size()-1, j = m_sites.size()-1;
    for (; i > -1; --i)
        for (; j >= -1; --j)
            if (j == -1 || m_sites[i] > m_sites[j]+MAX_RANGE) {
                m_max_re_sites[i] = j+1;
                break;
            }
}

void ChicagoReadSimulator::precompute_likelihoods() {
    for (int i = 0; i <= MAX_RANGE; ++i) {
        double& likelihood = m_likelihoods[i];
        m_model->NonNoiseInsertDist(i, &likelihood);
    }
}

void ChicagoReadSimulator::calculate_normalization() {
    double h1, h2;
    m_model->H0(MIN_SEP, &h1);
    m_model->H0(MAX_SEP, &h2);
    std::cerr << "h1: " << h1 << "\n"
              << "h2: " << h2 << "\n";

    // If target depth is not specified, derive N from datamodel
    bool auto_calc_target_read_count = m_target_depth == 0.0;
    double N = auto_calc_target_read_count ?
        m_model->N() :
        m_target_depth * m_model->genome_size() / ( h2-h1 );

    // Update target depth for logging purposes.
    if (auto_calc_target_read_count)
        m_target_depth = N * ( h2-h1 ) / m_model->genome_size();

    // Generate reads evenly from all haplotypes
    N /= m_ref->m_haplotypes;

    // Adjust reads to generate based on rate of reads that will not be usable.
    N /= (1 - m_p_unusable);
    std::cerr << "Target depth: " << m_target_depth << "\n"
              << "Genome size: " << m_model->genome_size() << "\n"
              << "N: " << N << "\n";

    // Sample site pairs
    int64_t n_samples = SITE_PAIR_SAMPLING_RATE * m_site_pairs;
    if (n_samples < 10) {n_samples = 10;}
    const double sampling_rate = double(n_samples) / m_site_pairs;
    std::cerr << "Randomly selecting " << n_samples
              << " samples to calculate prob scaling factor...\n";

    int i, j, fw, re;
    int64_t dist;
    double total_sample_prob = 0.0;
    for (int itr = 0; itr < n_samples; ++itr) {
        // Pick a random site pair to sample from
        i = int(drandt() * m_sites.size());
        fw = m_max_fw_sites[i];
        re = m_max_re_sites[i];
        j = drandt()*(fw - re) + re;
        if (i == j)
            continue;
        dist = abs(m_sites[j] - m_sites[i]);
        assert(dist > 0 && dist <= MAX_RANGE);
        total_sample_prob += m_likelihoods[dist];
    }

    // Number of real Chicago reads to generate
    double Nc = N * (1 - m_model->noise_probability());

    // Likelihood to reads scaling factor
    m_pnorm = (sampling_rate * Nc) / total_sample_prob;
    std::cerr << "Prob scaling factor: " << m_pnorm << "\n";
}

void ChicagoReadSimulator::gen_read_pairs() {
    int64_t generated = 0;

    // Generate real chicago reads
    queue_tasks<HRRealChicagoReadSimulatorTask>(m_sites.size(), generated);

    int64_t chicago_reads = generated;
    std::cerr << "Real Chicago read pairs: " << chicago_reads << "\n";

    // Intra-region noise
    double pn = m_model->noise_probability();
    double Nn = double(generated) * (pn / (1-pn));
    std::poisson_distribution<int64_t> noise_reads_distribution(Nn);
    int64_t n_intra_region_noise = noise_reads_distribution(m_generator);

    // Generate intra-region noise reads
    queue_tasks<HRIntraRegionNoiseSimulatorTask>(n_intra_region_noise,
                                                 generated);

    int64_t noise = generated - chicago_reads;
    std::cerr << "Intra-region noise read pairs: " << noise << "\n";

    // Short innies
    double expected_n_short_innies =
        (m_p_innie / (1-m_p_innie)) * double(generated);
    std::poisson_distribution<int64_t> short_innie_distribution(
        expected_n_short_innies);
    int64_t n_short_innies = short_innie_distribution(m_generator);

    // Generate short innies
    queue_tasks<HRShortInnieSimulatorTask>(n_short_innies, generated);

    int64_t innies = generated - chicago_reads - noise;
    std::cerr << "Short innies read pairs: " << innies << "\n";
    std::cerr << "Total read pairs generated: " << generated << "\n";
}

template<typename T>
void ChicagoReadSimulator::do_threaded(HRTaskQueue<T>& q, int64_t& count) {
    std::vector<ReadSimulatorThread<T>* > threads;
    for (int i = 0; i < m_threads; ++i) {
        ReadSimulatorThread<T>* t = new ReadSimulatorThread<T>(q, i,
                                                               m_generator(),
                                                               this);
        if (t->start())
            throw std::runtime_error("Error starting thread.");
        threads.push_back(t);
    }

    typename std::vector<ReadSimulatorThread<T>* >::iterator it;
    for (it = threads.begin(); it != threads.end(); ++it)
        if ((*it)->join())
            throw std::runtime_error("Error joining thread.");

    // Collect data from the threads and clean up
    for (it = threads.begin(); it != threads.end(); ++it) {
        count += (*it)->get_generated();
        delete *it;
    }
    threads.clear();
}

template<typename T>
void ChicagoReadSimulator::queue_tasks(int64_t tasks, int64_t& count) {
    // Break tasks into 3 * threads
    int chunk_size = (tasks + (m_threads*3) - 1) / (m_threads*3);

    HRTaskQueue<T> queue;
    int64_t i, chunk_start, chunk_end;
    for (i = 0; i < tasks;) {
        chunk_start = i;
        chunk_end = i+chunk_size > tasks ? tasks : i+chunk_size;
        queue.enqueue(T(chunk_start, chunk_end));
        i = chunk_end;
    }
    queue.close();

    return do_threaded(queue, count);
}

int dump_usage() {
    // TODO(jeev): Update this message.
    std::cerr << "A program to generate simulated Chicago reads in fastq format\n" <<
    "\n" <<
    "example usage:\n" <<
    "simulate_chicago_reads -m datamodel.out -f GRCh38_chr1.fa \n" <<
    "\n" <<
    "Where 'datamodel.out' contains a json formatted chicago dataset model.  The insert site distribution model, genome size are and noise rate are all used.\n" <<
    "\n" <<
    "So far (TODO) sequencing errors are not modeled at all.\n" <<
    "So far (TODO) the short innies have fragment size 375 +- 50.\n" <<
    "\n" <<
      //    "-f filename for forward read fastq output\n" <<
      //    "-r filename for reverse read fastq output\n" <<
    "-m data model file.\n" <<
    "-t Number of threads.\n" <<
    "-M junction sequence.\n" <<
    "-a start coordinate of the region to sample\n" <<
    "-b end coordinate of the region to sample\n" <<
    "-l read length (default 100)\n" <<
    "-d specifies the target chicago sampling depth for 1-50kb pairs.\n" <<
    "-f fasta file containing the reference sequence you wish to simulate sampling.  Only the first entry is used.\n" <<
    "-i Fraction of short innies.\n" <<
    "-u Fraction of unusable reads.\n" <<
      //    "-p Read name prefix." <<
    "\n" <<
    "\n" <<
    "\n";

    return 0;
}

int main(int argc, char * argv[]) {
    if ( argc == 1 )
        return dump_usage();

    std::ifstream model_stream;

    // Initialize reference
    Reference* ref = new Reference();

    // Set a random seed
    std::random_device rd;
    int64_t seed = rd();

    // Default values
    int threads = 1;
    std::string motif = "GATC";
    int min_site_sep = MIN_SITE_SEP;
    int read_length = 100;
    // If target depth is not specified, N will be derived from the datamodel
    double target_depth = 0.0;
    double p_innie = 0.20;
    double p_unusable = 0.0;

    int c;
    while ((c = getopt(argc, argv, "s:t:M:S:l:d:i:u:m:f:H:h?")) != -1) {
        switch (c) {
            case 's': seed = atoi(optarg);          break;
            case 't': threads = atoi(optarg);       break;
            case 'M': motif = optarg;               break;
            case 'S': min_site_sep = atoi(optarg);  break;
            case 'l': read_length = atoi(optarg);   break;
            case 'd': target_depth = atof(optarg);  break;
            case 'i': p_innie = atof(optarg);       break;
            case 'u': p_unusable = atof(optarg);    break;
            case 'm': model_stream.open(optarg);    break;
            case 'f':
            {
                std::ifstream fasta_stream(optarg);
                // Sanity check FASTA file
                if (fasta_stream.fail()) {
                    std::cerr << "Failed to read FASTA file." << "\n";
                    return 1;
                }
                try {
                    ref->read(fasta_stream);
                }
                catch (std::exception &e) {
                    std::cerr << e.what() << "\n";
                    return 1;
                }
                fasta_stream.close();
                break;
            }
            case 'h': return dump_usage();          break;
            case '?': return dump_usage();          break;
        }
    }

    // Sanity check model file
    if (model_stream.fail()) {
        std::cerr << "Failed to read model file." << "\n";
        return 1;
    }

    std::string model_json;
    std::getline(model_stream, model_json);
    LikelihoodModelParser model_parser;
    PairedReadLikelihoodModel* model = model_parser.ParseModelJson(
        model_json.c_str(), false);
    model_stream.close();

    // Rescale model for input genome size
    std::cerr << "Sequence length: " << ref->get_unpadded_length() << "\n"
              << "Haplotypes: " << ref->get_haplotype_count() << "\n";
    model->RescaleGenomesize( ref->get_unpadded_length() );

    // Run read simulator
    ChicagoReadSimulator* sim = new ChicagoReadSimulator(model,
                                                         ref,
                                                         motif,
                                                         read_length,
                                                         target_depth,
                                                         p_innie,
                                                         p_unusable,
                                                         threads,
                                                         seed);
    sim->find_sites(min_site_sep);
    sim->precompute_likelihoods();
    sim->calculate_normalization();

    try {
        sim->gen_read_pairs();
    }
    catch (std::exception &e) {
        std::cerr << e.what() << "\n";
        return 1;
    }

    return 0;
}
