#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include <fenv.h>
#include <Eigen/Dense>
#include "chicago_read_pairs.h"
#include "chicago_bam_filters.h"


int main(int argc, char* argv[]) {

  std::string bamfilename ;
  std::string region = ".";
  int min_qual = 10;
  int max_separation = 10000000;
  double genome_size = 3.0e9;
  int c;
  while ((c = getopt(argc, argv, "s:m:b:M:R:o:K:D:z:q:k:G:")) != -1) {
    switch (c) {
    case 'b': bamfilename = std::string(optarg);  break;
    case 'q': min_qual = atoi(optarg);            break;
    case 'R': region = std::string(optarg);       break;
    case 'G': genome_size = atof(optarg);       break;
    case 'M': max_separation = atoi(optarg);       break;
    }
  }


  SVReadPairFilter filter(min_qual);
  filter.keep_noise=true;
  ChicagoReadPairs data;
  data.SetHistogramOnly();
  data.G = genome_size;
  data.max_separation = max_separation;

  try {
    data.fill_from_bam(filter, bamfilename,region);
    //    binner.load_genome_map(mapfile);
  }
  catch (std::exception &e) {
    std::cerr << e.what() << "\n";
    return 1;
  }

  data.dump_histogram();
  exit(0);

}
