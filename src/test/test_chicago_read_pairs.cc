#include "chicago_read_pairs.h"
#include "chicago_bam_filters.h"
#include "chicago_sv_binner.h"

int main() {
    ChicagoReadPairs data;
    SVReadPairFilter filter(10);
    SVReadPairBinner binner(300);

    // Load relevant data from files
    try {
        data.fill_from_bam(
            filter, "/docker/sandbox/common/real.snap.md.sorted.bam");
        binner.load_genome_map("/opt/GRCh38/GRCh38.fa.map");
    }
    catch (std::exception &e) {
        std::cerr << e.what() << "\n";
        return 1;
    }

    // Bin reads
    const Reads& reads = data.get_reads();
    uint32_t xbin, ybin;
    for (Reads::const_iterator it = reads.begin(); it != reads.end(); ++it)
        binner.bin(*it, xbin, ybin);

    return 0;
}
