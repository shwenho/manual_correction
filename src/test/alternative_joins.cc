#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "link_range_iterator.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "hirise_threads.h"
//#include "oo_requests.h"
#include "hirise_setup.h"
#include "contig_index.h"
#include <fenv.h>

#define MIN_LEN 1000
typedef std::unordered_map <std::pair<int,int>, std::pair<int,int>, dtg::hash_pair >::iterator  scaffold_index_it_t ;

int main(int argc, char * argv[]) {
  int gapsize=100;
  ChicagoLinks links; 
  PathSet scaffolds;
  int nthreads;

  HiriseOpts opts = setup(argc,argv,links,scaffolds);

  auto links_index_contigs   =  IndexContigs( links ) ;
  auto links_index_scaffolds =  IndexScaffolds( links , scaffolds) ;
  links.InitContigIndex( links_index_contigs, links_index_scaffolds  );

  links.PrintModelInfo();
  auto n50= scaffolds.N50() ;  std::cout << "L/N50: " <<  n50.first << "\t" << n50.second << "\n";

  HRThreadPool<AltJoinsWorkerThread,HRScaffoldIntercScoreTask,ScaffoldMergeMove> pool(opts.nthreads,"dummy");
  pool.init();

  //OO_Vec *oov = NULL;
  int n_queued=0;
  //  int count=0;

  for (scaffold_index_it_t it = links_index_scaffolds.begin(); it != links_index_scaffolds.end(); it++) {
    int s1 = it->first.first;
    int s2 = it->first.second;
    if (s1!=s2) {
      pool.enqueue( HRScaffoldIntercScoreTask(links,scaffolds,s1,s2,gapsize) );      
    }
  }
  
  pool.close_queue();
  pool.join_all();

  ofstream outstream;
  outstream.open(opts.outfile);

  outstream << "graph G {\n" ;
  
  outstream << "    node[shape=box, style=rounded, fontname=sans, fontsize=2, penwidth=2];\n";
  outstream << "    edge[penwidth=2, color=grey]; \n";
  
  scaffolds.write_graphvis_node_info(outstream) ;
  scaffolds.write_graphvis_scaffold_edges(outstream) ;

  auto result_it = pool.result_iterator();
  while (!result_it.done()) {
    if ( result_it.current_.score < opts.minscore ) {break;}
    outstream << result_it.current_.join1.id1 << " -- " << result_it.current_.join1.id2 << " [label=\""<< result_it.current_.score <<"\"]\n";
    ++result_it;
  }

  outstream << "}\n" ;

  outstream.close();

  std::cout << "done\n";
  return 0;
} 


