#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "hirise_threads.h"

class TestThread : public HRThread {
public:
  void *run() {
    fprintf(stdout,"thread id: %lu\n",id());
  }
};

struct HRScaffoldSupportTask {
  int scaffold_id;
  int mini;
  int maxi;

  HRScaffoldSupportTask(int s, int i, int j): scaffold_id(s), mini(i), maxi(j) {}
};

class WorkerThread :  public HRThread {
  HRTaskQueue<HRScaffoldSupportTask*>& queue_;
  int id_;

public:
  WorkerThread(HRTaskQueue<HRScaffoldSupportTask*>& queue, int i) : queue_(queue), id_(i) {}

  void* run() {
    int j,k;
    for (int i=0;;i++) {
      HRScaffoldSupportTask* task= (HRScaffoldSupportTask*) queue_.dequeue();
      if (task==NULL) break;
      double x=0.0;
      fprintf(stdout,"worker thread %d %lx got job: %d %d-%d\n",id_,id(),task->scaffold_id, task->mini, task->maxi);
      fflush(stdout);
      for(j=0;j<20000;j++) {
	for(k=0;k<20000;k++) {
	  x+=1.0;
	}
      }
      fprintf(stdout,"worker thread %d %lx done (%g)\n",id_,id(),x);
      fflush(stdout);
      if ((queue_.size()==0)&&(queue_.isClosed())) { break; }
    }
    return NULL;
  }
};

int main(int argc, char * argv[]) {
  int i;
  int n_threads = std::stoi(argv[1]) ;
  int n_tasks   = std::stoi(argv[2]) ;
  std::vector<WorkerThread *> threads  ;

  HRTaskQueue<HRScaffoldSupportTask*> queue;

  for(i=0;i<n_threads;i++) {
    threads.push_back( new WorkerThread(queue,i) );
    threads[i]->start();
    fprintf(stdout,"thread started: %d\t%lx\n",i,threads[i]->id()); fflush(stdout);
  }

  for(i=0;i<n_tasks;i++) {
    queue.enqueue( new HRScaffoldSupportTask(i,i+1,i+2) );
  }
  queue.close();

  for(i=0;i<n_threads;i++) {
    fprintf(stdout,"thread to join: %d\n",i); fflush(stdout);
    threads[i]->join();
  }

  return(0);

}
