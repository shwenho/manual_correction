#include <string>
#include <vector>
#include <iostream>
#include <float.h>
#include <math.h>
#include "dtg_fft.h"
#include <chrono>
#include <time.h>
#include <complex>
#include <cmath>

//const double PI  =3.141592653589793238463;

int main(int argc, char * argv[]) {

  int n=1<<std::stoi(argv[1]);
  int i;

  std::vector<double> x;
  std::vector<std::complex<double>> X;
  std::vector<std::complex<double>> Z;
  std::vector<std::complex<double>> W;

  x.resize(n);
  X.resize(n);
  Z.resize(n);
  W.resize(n);

  std::fill(X.begin(),X.end(),std::complex<double>(0,0));
  std::fill(Z.begin(),Z.end(),std::complex<double>(0,0));

  for(i=0;i<n;i++) {
    x[i] =sin((double(i)*300.0/double(n)+0.25)*2.0*PI);
    x[i]+=sin((double(i)*0.3/double(n)+0.25)*2.0*PI);
    W[i] = exp( std::complex<double>(0.0,-2.0*PI* double(i)/n)); 
  }

  auto chrono_start = std::chrono::high_resolution_clock::now();


  dtg_fft(W,x,X,Z,n,0,0,1);

  auto elapsed = std::chrono::high_resolution_clock::now() - chrono_start;
  long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  fprintf(stdout, "#N= %d ; Took %g seconds for fft\n",n, double(microseconds)/1.0e6);  
  
  for(i=0;i<n;i++) {
    fprintf(stdout,"%d\t%g\t%g\t%g\t%g\n",i,x[i],std::real(X[i]),std::imag(X[i]),sqrt(std::norm(X[i])) );
  }



  exit(0);
}
