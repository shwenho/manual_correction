#include <iostream>
#include <stdlib.h>
#include "zmasker.h"

int main() {
    srand(time(NULL));
    std::vector<double> values;
    for (int itr = 0; itr < 1000; ++itr)
        values.push_back((rand()%100000)/1000);
    std::cerr << "No. of values: " << values.size() << std::endl;

    ZMasker<double> maskr(2.7, 0.0, 1000, 50, 100.0);
    maskr.mask(values);

    for (std::vector<double>::const_iterator it = values.begin();
         it != values.end(); ++it)
        std::cout << *it << std::endl;
    return 0;
}
