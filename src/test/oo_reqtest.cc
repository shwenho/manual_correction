#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "oo_requests.h"

inline bool file_exists (const std::string& name) {
  std::ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }   
}

int main(int argc, char * argv[]) {
  
  std::string oo_req_file;

  int ii=1;
  while (ii<argc) {
    if  ( strcmp(argv[ii],"-r")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing o&o requests file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"o&o_requests: %s\n",argv[ii]);
      oo_req_file = argv[ii];
    } else {
      fprintf(stdout,"unrecognized option: %s\n",argv[ii]);
      exit(0);
    }
    ii++;
  }

  OO_Requests oo_reqs;
  if (!oo_req_file.empty()) {
    Load_OO_Reqs(oo_reqs, oo_req_file, true);
  }
  exit(0);
}
