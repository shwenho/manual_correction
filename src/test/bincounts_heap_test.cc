#include <string>
#include <vector>
#include <iostream>
#include <float.h>
#include "bincounts_heap.h"

int main(int argc, char * argv[]) {

  int i;
  int n;
  n=13;
  BinCountsHeap h(n);

  for(i=0;i<n;i++) {
    (void) h.IncrementBin(i, double(i)/10.0);
  }

  (void) h.IncrementBin(2,3.0);
  (void) h.IncrementBin(8,13.0);
  (void) h.IncrementBin(5,6.0);

  h.Examine();

  fprintf(stdout,"top 1 sum: %g\n",h.SumTopN(1) );
  fprintf(stdout,"top 2 sum: %g\n",h.SumTopN(2) );
  fprintf(stdout,"top 3 sum: %g\n",h.SumTopN(3) );
  fprintf(stdout,"top 4 sum: %g\n",h.SumTopN(4) );
  fprintf(stdout,"top 5 sum: %g\n",h.SumTopN(5) );
  fprintf(stdout,"top 6 sum: %g\n",h.SumTopN(6) );
  h.Examine();

  exit(0);
}
