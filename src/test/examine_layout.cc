#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "link_range_iterator.h"
#include "hirise_threads.h"


const std::string model_json = "{\"N\": 338657.5978329083, \"beta\": [0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05], \"Nn\": 7138.1194359941255, \"fn\": \"338657.5978329083*(7.02589624611152e-12+(1.0-0.02107768873833456)*(0.0010213324568692176*exp(-(x)/500.0) + 0.000211434184175803*exp(-(x)/900.8241153272055) + 2.4126299880631892e-05*exp(-(x)/1622.9681735100849) + 3.2339077161572e-05*exp(-(x)/2924.0177382128654) + 2.5567888732372052e-05*exp(-(x)/5268.0513844533225) + 3.2092501138910775e-06*exp(-(x)/9491.175455796849) + 0.0*exp(-(x)/17099.759466766965) + 0.0*exp(-(x)/30807.751387916716) + 0.0*exp(-(x)/55504.730778481135) + 0.0*exp(-(x)/99999.99999999997)))\", \"alpha\": [0.5106662284346087, 0.19046501191009718, 0.039156216850825716, 0.0945600352578711, 0.1346929516341211, 0.030459555912476236, 0.0, 0.0, 0.0, 0.0], \"pn\": 0.02107768873833456, \"G\": 32155272.0}";


//const std::string model_json = "{\"N\": 169328.5, \"beta\": [0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05], \"Nn\": 7138.1194359941255, \"fn\": \"338657.5978329083*(7.02589624611152e-12+(1.0-0.02107768873833456)*(0.0010213324568692176*exp(-(x)/500.0) + 0.000211434184175803*exp(-(x)/900.8241153272055) + 2.4126299880631892e-05*exp(-(x)/1622.9681735100849) + 3.2339077161572e-05*exp(-(x)/2924.0177382128654) + 2.5567888732372052e-05*exp(-(x)/5268.0513844533225) + 3.2092501138910775e-06*exp(-(x)/9491.175455796849) + 0.0*exp(-(x)/17099.759466766965) + 0.0*exp(-(x)/30807.751387916716) + 0.0*exp(-(x)/55504.730778481135) + 0.0*exp(-(x)/99999.99999999997)))\", \"alpha\": [0.5106662284346087, 0.19046501191009718, 0.039156216850825716, 0.0945600352578711, 0.1346929516341211, 0.030459555912476236, 0.0, 0.0, 0.0, 0.0], \"pn\": 0.02107768873833456, \"G\": 32155272.0}";


//void inline MapCheck(Base b, Base b2, Base b3, const PathSet& scaffolds) {
void inline MapCheck(Base b, const PathSet& scaffolds) {
  Base b2;
  Base b3;
  scaffolds.ScaffoldCoord(b,&b2);
  scaffolds.ContigCoord(b2,&b3);
  assert(b==b3);  
  fprintf(stdout,"(%d,%d) -> (%d,%d)\n",b.target_id,b.x,b2.target_id,b2.x);
  fprintf(stdout,"\t-> (%d,%d)\n",b3.target_id,b3.x);
}

char rbuffer [256];

struct Range {
  std::string seq;
  int start;
  int stop;

  Range(char* s) {
    char* pch;
    std::string st;
    pch = std::strtok (s,":");
    seq = std::string(pch);    
    pch = std::strtok (NULL,"-");
    start=std::stoi(pch);
    pch = std::strtok (NULL,"- \n,");
    stop=std::stoi(pch);
    //sscanf("%s:%d-%d",s,rbuffer,&start,&stop);
    //    st = rbuffer;
    //seq = st; 
    fprintf(stdout,"#%s,%d,%d\n",seq.c_str(),start,stop);
  }

  char* c_str() {
    sprintf(rbuffer,"%s:%d-%d\n",seq.c_str(),start,stop);
    return rbuffer;
  }

};

int main(int argc, char * argv[]) {
  
  bool layout=false;
  bool dump_reads=false;
  bool restore_dump=false;
  int pad=0;
  int minsep=0;
  int mapq = 0;
  std::string hrafile;
  std::string reads_dump_out;
  std::string reads_dump_in ;
  std::vector<std::string> bamfiles;
  std::vector<Range> ranges;
  //-p 100000 -m 500 -q 5 
  int ii=0;
  while (ii<argc) {
    if ( strcmp(argv[ii],"-p")==0 ) {
      ii++;
      pad = std::stoi(argv[ii]);
      //      fprintf(stdout,"R: %d %d\n",ii,std::stoi(argv[ii]));
    } else if  ( strcmp(argv[ii],"-m")==0 ) {
      ii++;
      minsep = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-q")==0 ) {
      ii++;
      mapq = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-i")==0 ) {
      ii++;
      hrafile = argv[ii];
      layout=true;
    } else if  ( strcmp(argv[ii],"-X")==0 ) {
      ii++;
      reads_dump_in = argv[ii];
      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-o")==0 ) {
      ii++;
      reads_dump_out = argv[ii];
      dump_reads=true;
      //      fprintf(stdout,"N: %d %d\n",ii,std::stoi(argv[ii]));
    } else if  ( strcmp(argv[ii],"-b")==0 ) {
      ii++;
      fprintf(stdout,"bamfile: %s\n",argv[ii]);
      bamfiles.push_back(std::string(argv[ii]));
    } else if  ( strcmp(argv[ii],"-R")==0 ) {
      ii++;
      fprintf(stdout,"range: %s\n",argv[ii]);
      ranges.push_back(Range(argv[ii]));
      //      bamfiles.push_back(std::string(argv[ii]));
    } 
    ii++;
  }

  fprintf(stdout,"pad:\t%d\n",pad);
  fprintf(stdout,"mapq:\t%d\n",mapq);
  fprintf(stdout,"minsep:\t%d\n",minsep);
  fprintf(stdout,"hra:\t%s\n",hrafile.c_str());
  for (ii=0;ii<bamfiles.size();ii++) {   fprintf(stdout,"bam:\t%s\n",bamfiles[ii].c_str());  }
  for (ii=0;ii<ranges.size();ii++)   {   fprintf(stdout,"range:\t%s\n",ranges[ii].c_str());  }

  LikelihoodModelParser model_parser;
  PairedReadLikelihoodModel *model = model_parser.ParseModelJson(model_json.c_str());

  //  if (argc<3) {
  //  fprintf(stdout, "Expected at least 3 args: bed file, hra file, and one or more bamfiles.  Found %d\n", argc-1);
  //  exit(0);
  //}

  ChicagoLinks links;
  PathSet scaffolds;

  auto chrono_start = std::chrono::high_resolution_clock::now();
  links.min_mapq() = mapq;

  if (restore_dump) { 
    links.PopulateFromBam( bamfiles[0].c_str(), false, model, 0 ,reads_dump_in);
  } else {
    for (int i=0;i<bamfiles.size();i++) {
      fprintf(stdout,"opening bam file %s\n",bamfiles[i].c_str());
      links.PopulateFromBam( bamfiles[i].c_str(), false, model, 0 );
    }
  }
  auto elapsed = std::chrono::high_resolution_clock::now() - chrono_start;
  long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  fprintf(stdout, "Took %lld microseconds for bam loading\n", microseconds);

  if (layout) {
    scaffolds.ParseFromHRA(links.GetHeader(),hrafile);
    const SegmentSet masking = scaffolds.MaskedSegments() ;
    links.SetMaskedFlags( masking );
    links.SwitchToScaffoldCoordinates( scaffolds );
  } else {
    //links.SortByContigs();
  }

  if (dump_reads) {
    links.write_reads(reads_dump_out);
  }

  //scaffolds.IndexMaskedScaffoldSegments();

  exit(0);

}
