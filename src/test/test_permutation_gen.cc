#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>

#include "hr_perm_utils.h"

#define MIN_DOUBLE -9.9e99;

//dummy
double window_pair_score( int p, int state, int backlink_pos, int ancestor ) {
  return 0.0;
}


int main(int argc, char * argv[]) {

  int w=std::stoi(argv[1]);

  LocalOODPLinks lool(w);
  DPmatrix matrix(lool,10);


  for(int p = -w+1; p< matrix.npositions;p++) {
    std::cout << "position: " << p << " / "<< matrix.npositions <<"\n";
    for(uint state=0; state<lool.nstates;state++) {
      if (matrix.is_forbidden(p,state)) continue;

      std::cout << "state: " << state << "\t" << lool.states[state] << "\n";

      double best_score = MIN_DOUBLE;

      for( Backlink_iterator x=lool.ancestors(state); !(x.done()); ++x) {
	int backlink_pos = p - x->delta;
	if ((backlink_pos>=-w+2) &&   !matrix.is_forbidden(backlink_pos,x->ancestor) ) {
	  std::cout << "\tbl pos:\t" << backlink_pos << "\t" << *x << "\t" << x->ancestor << "\t" <<  x->delta << "\t" << x->s  <<  "\t" << lool.states[x->ancestor] << "\n";

	  double score = window_pair_score( p, state, backlink_pos, x->ancestor );
	  if ( score > best_score ) {best_score = score;}

	}
      } 
      matrix.set_score(p,state,best_score);

      }
      
    }
  }
  
}

