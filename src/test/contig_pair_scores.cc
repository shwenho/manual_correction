#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"


const std::string model_json = "{\"N\": 338657.5978329083, \"beta\": [0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05], \"Nn\": 7138.1194359941255, \"fn\": \"338657.5978329083*(7.02589624611152e-12+(1.0-0.02107768873833456)*(0.0010213324568692176*exp(-(x)/500.0) + 0.000211434184175803*exp(-(x)/900.8241153272055) + 2.4126299880631892e-05*exp(-(x)/1622.9681735100849) + 3.2339077161572e-05*exp(-(x)/2924.0177382128654) + 2.5567888732372052e-05*exp(-(x)/5268.0513844533225) + 3.2092501138910775e-06*exp(-(x)/9491.175455796849) + 0.0*exp(-(x)/17099.759466766965) + 0.0*exp(-(x)/30807.751387916716) + 0.0*exp(-(x)/55504.730778481135) + 0.0*exp(-(x)/99999.99999999997)))\", \"alpha\": [0.5106662284346087, 0.19046501191009718, 0.039156216850825716, 0.0945600352578711, 0.1346929516341211, 0.030459555912476236, 0.0, 0.0, 0.0, 0.0], \"pn\": 0.02107768873833456, \"G\": 32155272.0}";


//const std::string model_json = "{\"N\": 169328.5, \"beta\": [0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05], \"Nn\": 7138.1194359941255, \"fn\": \"338657.5978329083*(7.02589624611152e-12+(1.0-0.02107768873833456)*(0.0010213324568692176*exp(-(x)/500.0) + 0.000211434184175803*exp(-(x)/900.8241153272055) + 2.4126299880631892e-05*exp(-(x)/1622.9681735100849) + 3.2339077161572e-05*exp(-(x)/2924.0177382128654) + 2.5567888732372052e-05*exp(-(x)/5268.0513844533225) + 3.2092501138910775e-06*exp(-(x)/9491.175455796849) + 0.0*exp(-(x)/17099.759466766965) + 0.0*exp(-(x)/30807.751387916716) + 0.0*exp(-(x)/55504.730778481135) + 0.0*exp(-(x)/99999.99999999997)))\", \"alpha\": [0.5106662284346087, 0.19046501191009718, 0.039156216850825716, 0.0945600352578711, 0.1346929516341211, 0.030459555912476236, 0.0, 0.0, 0.0, 0.0], \"pn\": 0.02107768873833456, \"G\": 32155272.0}";

int main(int argc, char * argv[]) {

  LikelihoodModelParser model_parser;
  PairedReadLikelihoodModel *model = model_parser.ParseModelJson(model_json.c_str());

  if (argc<3) {
    fprintf(stdout, "Expected at least 2 args: bed file and one or more bamfiles.  Found %d\n", argc-1);
    exit(0);
  }

  ChicagoLinks links;

  auto chrono_start = std::chrono::high_resolution_clock::now();
  links.min_mapq() = 50;

  for (int i=2;i<argc;i++) {
    links.PopulateFromBam( argv[i], false, model, 0 );
  }
  links.SortByContigs();

  links.ReadMaskedRanges(argv[1]);

  auto elapsed = std::chrono::high_resolution_clock::now() - chrono_start;
  long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  fprintf(stdout, "Took %lld microseconds for bam loading\n", microseconds);

  links.AllContigPairScores();

  exit(0);

}

