#include <vector>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <ios>
#include <assert.h>
#include "cnpy.h"
#include "sparse_kernel_scores.h"
#include "chicago_bam_filters.h"
#include "chicago_wSI_lm.h"
#include "peak_caller.h"

/*
void init_fake_reads(std::vector<readpair>& reads) {
  int i;
  readpair read;
  for (i=0;i<NREADS;i++) {
    read.xbin = int( (float(i)/float(NREADS))*MAXBIN );
    read.ybin = 2;
    reads.push_back( read );
  }
}

float ff(int x) {
  return( exp(-float(x)/10000.0)/10000.0 );
}

variant_spec string_to_variant( std::string kernel_spec ) {

  std::vector<std::string> tokens;
  std::stringstream ss(kernel_spec);
  std::string token;
  //  variant_spec v=  variant_spec(30000,INVERSION,HET);

  while(std::getline(ss,token,',')) {
    tokens.push_back(token);
    std::cout << "t:\t" << token << "\n";
  }

  kernel_t k;
  if (tokens[0]=="INVERSION") {  k=INVERSION;} else \
  if (tokens[0]=="DELETION" ) {  k=DELETION; } else \
  if (tokens[0]=="DUPT"     ) {  k=DUPT;     } else \
  if (tokens[0]=="DUPR"     ) {  k=DUPR;     } else \
  if (tokens[0]=="DUPL"     ) {  k=DUPL;     } else {
    assert(1==0);
  }

  zygosity_t z;
  if (tokens[1]=="HOMO") {  z=HOMO;} else
  if (tokens[1]=="HET" ) {  z=HET; }

  int D = stoi(tokens[2]);

  return variant_spec(D,k,z);
}*
*/

void SVKernelScorer::prepare_data(int min_qual,
                                  std::string region,
                                  std::vector<std::string> bamfiles,
                                  std::string masked_bed,
                                  bool do_model_fit) {

  // Read reads from bamfile
  ChicagoReadPairs data;
  SVReadPairFilter filter(min_qual,0);

  filter.keep_noise=true;
  data.max_separation = 10000000;

  for (std::vector<std::string>::const_iterator it=bamfiles.begin(); it!=bamfiles.end(); it++ ) {
    INFO("Reading from BAM file: '%s'...\n", it->c_str());
    data.fill_from_bam(filter, *it, region);
  }

  const Reads& reads = data.get_reads();
  INFO("Done reading %lu reads.\n\n", reads.size());

  // Load genome map into binner
  m_binner.load_genome_map(data.get_refs());

  m_min_isize = filter.min_isize;
  // std::cout << "min isize:" << m_min_isize << "\n";
  // Bin reads
  INFO("Assigning reads to bins...\n\n");
  int64_t xbin, ybin;
  for (Reads::const_iterator it = reads.begin(); it != reads.end(); ++it) {
    m_binner.bin(*it, xbin, ybin);
    if (ybin<HH) {
#if DUMP_BINNING
      std::cout << "bin:" << it->pos_1 << "\t" << it->pos_2 << "\t" << xbin <<"\t" << ybin << "\n";
#endif
      m_binned_reads.push_back( BinnedRead(xbin,ybin) );
    }
  }

  // Sort binned reads
  std::sort(m_binned_reads.begin(), m_binned_reads.end());

  // Load masked regions from BED
  std::ifstream masked(masked_bed);

  int64_t scaffold_id;
  if (masked.is_open() && masked.good()) {
    std::string line, scaffold;
    int64_t start, end;
    std::map<int64_t, int64_t> masked_set;
    while (std::getline(masked, line)) {
      std::istringstream buf(line);
      buf >> scaffold >> start >> end;
      m_binner.bin(scaffold, start, end, masked_set,scaffold_id);
      data.AddMaskedSegment(Tripple(scaffold_id,start,end));
      //      std::cout << "masked segment\t"<< scaffold_id <<"\t" << start << "\t" << end <<"\n";
    }
    std::copy(masked_set.begin(),
              masked_set.end(),
              std::back_inserter(m_masked_bins));
  } else {
    std::cout << "masked segment is open\t"<< masked.is_open() << "\n"; // scaffold_id <<"\t" << start << "\t" << end <<"\n";
    std::cout << "masked segment is good\t"<< masked.good() << "\n"; // scaffold_id <<"\t" << start << "\t" << end <<"\n";

  }
  data.SortSegments();
  //  std::sort(data.m_segments.begin(),data.m_segments.end());

#if SKIP_MASKING_CORRECTION
#else
  data.apply_masking_correction();
#endif

  if (do_model_fit) {
    INFO("Optimizing likelihood model parameters...\n\n");
    m_model = new ChicagoSILM;
    m_model->fit(data);
  }

}

/*
void SVKernelScorer::correct_model_for_masking() {

  int64_t last_id=-1;
  int64_t last_end=-1;
  double expected_n_good_both_masked=0.0;
  double total_segment_length  =0.0;
  double p_masked=0.0;
  double pn= m_model->noise_probability() ;  // 0.5;   // noise rate
  double G=m_model->genome_size() ; // 20.0e6; // genome size
  double N=m_model->N() ;  //10000.0; //total n reads
  double n_bar;

  // Check for overlapping segments;
  std::vector<Tripple>::const_iterator tr=m_segments.begin();
  std::vector<Tripple>::const_iterator cc=tr;
  for( std::vector<Tripple>::const_iterator it=m_segments.begin();it<m_segments.end();++it) {
    assert(it->id >=0);
    int l1 = it->end-it->start;

    p_masked += (2.0*l1/G)*(1.0-pn);
    m_model->n_bar(l1,&n_bar);
    //    std::cout << "masked:\t" << it->id << "\t" << it->start << "\t" << it->end << "\t" << n_bar << "\t" << (2.0*l1/G)*(1.0-pn) <<"\t"<<( n_bar - 0.5*(N*l1/G)*(l1/G)*pn )<< "\n";
    p_masked -= ( n_bar - 0.5*(N*l1/G)*(l1/G)*pn )/N; // avoid double counting; // m_model->T_good(l1)

    if (last_id>-1) {
      if ((last_id==it->id)&&(last_end >= it->start)) {assert(1==0);}
    }

    //TODO:  is there a variable for max separation cutoff??
    while ( (tr->id!=it->id) || ((it->start-tr->end)>500000) ) {++tr;}

    for(cc=tr;cc!=it;cc++) {
      int l2  = cc->end   - cc->start;
      int gap = it->start - cc->end;
      m_model->n_bar(l1,l2,gap,&n_bar);
      expected_n_good_both_masked += n_bar - (N*l1/G)*(l2/G)*pn ;  // m_model->n_bar_good(l1,l2,gap);
    }

    total_segment_length += it->end - it->start;
    last_id = it->id;
    last_end = it->end;
  }

  p_masked -= (expected_n_good_both_masked / N) ; // (because we've already double counted these)
  p_masked += pn*( 1.0 - pow( (G-total_segment_length)/G ,2.0) ); // p noise and masked;

  std::cout << "Genome size:\t" << G <<"\n";
  std::cout << "N reads:\t"     << N <<"\n";
  std::cout << "total masked length:\t"     << total_segment_length <<"\n";
  std::cout << "probability that a read is masked:\t" << p_masked <<"\n";


  m_model->set_N( N/(1.0-p_masked) );

}
*/

void SVKernelScorer::cache_window_data() {
  // Verify that we have reads
  DT_ASSERT(!m_binned_reads.empty(), "No usable reads found.");

  int x, i=0, j=0, k=0, l=0;
  m_begin = m_binned_reads.front().xbin + WW;
  m_end = m_binned_reads.back().xbin - WW+2;
  for (x = m_begin; x < m_end; ++x) {
    // Iterate forward to first read inside window
    while (x-m_binned_reads[i].xbin > WW) { i++; }
    // Iterate forward to first read outside of window
    while (j<m_binned_reads.size() && m_binned_reads[j].xbin-x < WW) { j++; }

    if (!m_masked_bins.empty()) {
      // Iterate forward to first masked bin inside window
      while (k<m_masked_bins.size() && x-m_masked_bins[k].first > WW) { k++; }
      // Iterate forward to last masked bin inside window
      while (l<m_masked_bins.size() && m_masked_bins[l].first-x < WW) { l++; }
    }
    m_windows.push_back(Window(i, j, k, l));
#if DUMP_READS_PER_WINDOW
    std::cout << "reads_in_window:\t" << j-i << "\n";
    std::cout << "masked_bins_in_window:\t" << l-k << "\t" << k <<"\t" << l << "\n";
#endif
  }
#if DUMP_READS_PER_WINDOW
  std::cout.flush();
#endif

}

void SVKernelScorer::build_variant_list(int Dmin,
                                        int Dmax,
                                        int stepsize,
                                        std::string types,
                                        std::string zygosity) {
  std::string type_str, zygosity_str;
  std::vector<std::string> tt, zz;
  std::vector<std::string>::const_iterator t, z;

  // Extract types and zygosity from comma-separated strings
  std::istringstream buf(types);
  while (std::getline(buf, type_str, ','))
    tt.push_back(type_str);
  buf.str(zygosity);
  buf.clear();
  while (std::getline(buf, zygosity_str, ','))
    zz.push_back(zygosity_str);

  for (int D = Dmin; D <= Dmax; D += stepsize)
    for (t = tt.begin(); t != tt.end(); ++t)
      for (z = zz.begin(); z != zz.end(); ++z)
        m_variants.push_back( variant_spec(D, *t, *z) );
}

void SVKernelScorer::dump_kernels(std::string fp) const {
  if (fp.empty())
    return;

  INFO("Caching SV detection kernels to '%s'...\n\n", fp.c_str());

  // Write out variant descriptions
  std::ofstream cache(fp, std::ios::out | std::ofstream::binary);
  for (std::vector<variant_spec>::const_iterator it = m_variants.begin();
       it != m_variants.end(); ++it)
    cache << it->id() << "\n";

  // Spacer between variants and kernel calculations
  cache << std::endl;

  // Write out kernel elements
  cache.write(reinterpret_cast<const char *>(m_deltas.data()),
              m_deltas.size()*sizeof(float));
  cache.write(reinterpret_cast<const char *>(m_areaTij.data()),
              m_areaTij.size()*sizeof(float));
  cache.write(reinterpret_cast<const char *>(m_areaVs.data()),
              m_areaVs.size()*sizeof(float));
  cache.write(reinterpret_cast<const char *>(m_offsets.data()),
              m_offsets.size()*sizeof(float));
}

bool SVKernelScorer::load_kernels_from_cache(std::string fp) {
  // Ensure cache files are present
  std::ifstream cache(fp, std::ios::in | std::ifstream::binary);
  if (cache.fail())
    return false;

  INFO("Loading SV detection kernels from cache...\n\n");

  // Load descriptions
  std::string id_str;
  while (std::getline(cache, id_str) && !id_str.empty())
    m_variants.push_back(variant_spec(id_str));

  // Initialize kernel elements for loading from files
  init_kernel_elements();

  // Load kernel elements
  cache.read(reinterpret_cast<char *>(m_deltas.data()),
             m_deltas.size()*sizeof(float));
  cache.read(reinterpret_cast<char *>(m_areaTij.data()),
             m_areaTij.size()*sizeof(float));
  cache.read(reinterpret_cast<char *>(m_areaVs.data()),
             m_areaVs.size()*sizeof(float));
  cache.read(reinterpret_cast<char *>(m_offsets.data()),
             m_offsets.size()*sizeof(float));
  return true;
}

//typedef float (*t_int2float)(int);
//typedef float (*t_int2floatp)(int,float*);
//PairedReadLikelihoodModel&

//template <typename int2floatF>
void SVKernelScorer::calc_kernel_elements() {
  INFO("Calculating SV detection kernels...\n\n");

  // Initialize containers for kernel calculations
  init_kernel_elements();

  std::vector<int> dv;
  int x,y,k,d0,xb,yb,i;
  float Sij, Tij;
  float sum_of_f_of_dv;
  float f_of_d0;
  float log_f_of_d0;
  int center;
  int height;
  int region;
  PairedReadLikelihoodModel& f = *m_model;
  double n_over_g = f.N()/f.genome_size();
  double n_bar_0 = 0.0;
  double n_bar_1 = 0.0;
  double n_bar   = 0.0;

  for (xb=WW;xb>=-2*WW;xb--) {
    x=xb*m_binsize;
    for (yb=xb+1;yb<2*WW;yb++) {
      center = (xb+yb)/2;
      height = (yb-xb);
      if (center < -WW ) {continue;}
      if (center >= WW ) {continue;}
      if (height >= HH)  {continue;}
      y=yb*m_binsize;
      d0= y>x ? y-x : 0.25*m_binsize;
      assert(y>x);
      m_model->n_bar(m_binsize,m_binsize,d0-m_binsize,&n_bar_0);
      if (d0<m_min_isize) {continue;}
      f_of_d0 = (f(d0));

      log_f_of_d0 = log(f_of_d0);

      for (k=0;k<m_variants.size();k++) {
        const variant_spec& v = m_variants[k];

        region = get_region(v,x+m_binsize/2,y+m_binsize/2);
        if (region==-1) {
          m_deltas[ KERNEL_INDEX(center,height,k) ] = 0.0;
	  m_areaTij[KERNEL_INDEX(center,height,k) ]=0.0;
	} else {

	  n_bar_1=0.0;
          get_variant_separations( v, dv, x+m_binsize/2, y+m_binsize/2, region );

	  for(int i=0;i<dv.size();i++) { 
	    assert((dv[i]-m_binsize)>=0);
	    m_model->n_bar(m_binsize,m_binsize,dv[i]-m_binsize,&n_bar);
	    if (abs(n_bar)>100) { std::cout << "Xnb:\t" << xb << "\t" << yb << "\t" << n_bar << "\t" << k << "\t" << center << "\t" << height << "\t" << i << "\t" << dv[i] << "\t" << KERNEL_INDEX(center,height,k) << "\n"; }
	    n_bar_1+=n_bar;
	  }

	  //	  if (abs(n_bar_1)>400) { std::cout << "X:\t" << dx << "\t" << dy << "\t" << n_bar_1 << "\t" << "\n"; }

          sum_of_f_of_dv= PROB_EPSILON ; // so log(\epsilon) isn't -\infty, just a big penalty.
          for(i=0;i<dv.size();i++) { sum_of_f_of_dv += f(dv[i]); }
          switch(v.zygosity){
          case HOMO :
            Sij = log( sum_of_f_of_dv ) - log_f_of_d0;
	    //            Tij = -( sum_of_f_of_dv - f_of_d0) ;  // - ( n_bar_1 - n_bar_2)/A 
	    m_areaTij[ KERNEL_INDEX(center,height,k) ] =  -(n_bar_1-n_bar_0) ;
            break;
          case HET  :
            Sij = log( 0.5*f_of_d0 + 0.5* sum_of_f_of_dv ) - log_f_of_d0;
	    //            Tij = -0.5*( sum_of_f_of_dv - f_of_d0 )  ;  // -( 0.5*n_bar_1 + 0.5*n_bar_2 - n_bar_2)/A
	    m_areaTij[ KERNEL_INDEX(center,height,k) ] =  -0.5*(n_bar_1-n_bar_0) ;
            break;
          }
          m_deltas[  KERNEL_INDEX(center,height,k) ] = Sij;
	  //Tij = m_areaTij[ KERNEL_INDEX(center,height,k) ] / (m_binsize*m_binsize*n_over_g ) ;
	  //          m_areaTij[ KERNEL_INDEX(center,height,k) ] = xb<yb ? Tij*m_binsize*m_binsize*n_over_g : 0.5 * Tij*m_binsize*m_binsize*n_over_g ;

          if ( xb > -WW) { m_areaVs[  AREAVS_INDEX(xb,k) ] += m_areaTij[ KERNEL_INDEX(center,height,k) ]  ;  }
          if ( yb <  WW) { m_areaVs[  AREAVS_INDEX(yb,k) ] += m_areaTij[ KERNEL_INDEX(center,height,k) ]  ;  }

          m_offsets[k] += m_areaTij[ KERNEL_INDEX(center,height,k) ] ;  //  xb<yb ? Tij*m_binsize*m_binsize*n_over_g : 0.5 * Tij*m_binsize*m_binsize*n_over_g ;
	  //          m_offsets[k] += Tij*BINSIZE  *BINSIZE  *n_over_g;
#if DUMP_KERNELS
	  //	  std::cout << "k:\t" << k << "\t" << center <<"\t" <<height << "\t" << Sij << "\t" << (xb<yb ? Tij*m_binsize*m_binsize*n_over_g : 0.5 * Tij*m_binsize*m_binsize*n_over_g) <<"\t" << sum_of_f_of_dv << "\t" << f_of_d0 << "\t" << region << "\t" << dv.size() << "\t" << -(n_bar_1-n_bar_0)  <<"\t"<< v.id() << "\n";
	  std::cout << "k:\t" << k << "\t" << center <<"\t" <<height << "\t" << Sij << "\t" << m_areaTij[ KERNEL_INDEX(center,height,k) ] <<"\t" << sum_of_f_of_dv << "\t" << f_of_d0 << "\t" << region << "\t" << dv.size() <<"\t"<< v.id() << "\t";
	  for(int i=0;i<dv.size();i++) { 
	    std::cout << dv[i] <<"\t";
	  }	  
	  std::cout <<"\n";
	  
#endif
        }
      }
    }
  }

#if DUMP_OFFSETS
  for (k=0;k<m_variants.size();k++) {
     const variant_spec& v = m_variants[k];
     //m_offsets[k] += Tij*BINSIZE*BINSIZE*n_over_g;
     std::cout << "T:\t" << k << "\t" << m_offsets[k] << "\t" << v.id() << "\n";
     for (xb=-WW;xb<WW;xb++) {
       std::cout << "V:\t" << k << "\t" << xb << "\t" << m_areaVs[  AREAVS_INDEX(xb,k) ] << "\t" << v.id() << "\n";
     }
  }
#endif

  /*
  for(center=-WW;center<WW ;center++) {
    for (height=0;height<HH;height++) {
      for (k=0;k<NKERNELS;k++) {
  kernels[ KERNEL_INDEX(center,height,k) ] = kernels[ KERNEL_INDEX(center,height,k) ] - kernels[ KERNEL_INDEX(center+1,height,k) ]  ;
      }
    }
  }
  center = WW;
  for (height=0;height<HH;height++) {
    for (k=0;k<NKERNELS;k++) {
      kernels[ KERNEL_INDEX(center,height,k) ] = 0.0;
    }
  }
  */
}

/*
void calc_kernel_offsets(float* kernel_offset ,const std::vector<variant_spec>& variants, PairedReadLikelihoodModel& f) {
  int x,y,k,d0,xb,yb,i;
  int nv = variants.size();
  variant_spec v;
  std::vector<int> dv;
  float Tij;
  float sum_of_f_of_dv;
  float f_of_d0;
  float log_f_of_d0;
  int center;
  int height;

  for (k=0;k<nv;k++) {
    kernel_offset[k]=0.0;
  }


  for(xb=WW;xb>=-2*WW;xb--) {
    x=xb*BINSIZE;
    for (yb=xb;yb<2*WW;yb++) {
      center = (xb+yb)/2;
      height = (yb-xb);
      if (center < -WW ) {continue;}
      if (center >= WW ) {continue;}
      if (height >= HH) {continue;}
      y=yb*BINSIZE;
      d0=y-x;
      f_of_d0 = (f(d0));
      //      log_f_of_d0 = log(f_of_d0);
      for (k=0;k<nv;k++) {
  //  if (region==-1) { kernels[ KERNEL_INDEX(center,height,k) ] = 0.0; } else {
  v = variants[k];
  int region = get_region(v,dv,x,y);
  if (region==-1) {continue;}
  get_variant_separations( v, dv, x,y, region );
  sum_of_f_of_dv= PROB_EPSILON ; // so log(\epsilon) isn't -\infty, just a big penalty.
  for(i=0;i<dv.size();i++) { sum_of_f_of_dv += f(dv[i]); }
  switch(v.zygosity){
  case HOMO :
    Tij = -( sum_of_f_of_dv - f_of_d0) ;
    break;
  case HET  :
    Tij = -0.5*( sum_of_f_of_dv - f_of_d0 )  ;
    break;
  }
  kernel_offset[ k ] += Tij*BINSIZE*BINSIZE;
  //std::cout << "k:\t" << k << "\t" << center <<"\t" <<height << "\t" << Sij <<"\t" << sum_of_f_of_dv << "\t" << f_of_d0 << "\t" << dv.size() << "\n";
      }
    }
  }
}
*/

void SVKernelScorer::calc_kernel_stats() {
  INFO("Calculating SV detection kernels statistics...\n\n");

  // Initialize containers for kernel statistics
  init_kernel_stats();

  int k,x,y;
  float delta;
  double n_bar;

  for(k=0;k<m_variants.size();k++) {
    double total_n_expected=0.0;
    auto ids = m_variants[k].id() ;
    m_means[k]=m_offsets[k];
    for(x=-WW;x<WW;x++) {

      /* TODO: what about this?
      y=0;
      m_model->n_bar(m_binsize,m_binsize,m_binsize*(y-1),&n_bar);
      delta = m_deltas[ KERNEL_INDEX(x,y,k) ];
      m_means[k] += delta * n_bar;
      m_var[k] += delta * sqrt(n_bar);
      */

      for(y=1;y<HH;y++) {
        m_model->n_bar(m_binsize,m_binsize,m_binsize*(y-1),&n_bar);
	total_n_expected += n_bar;
        // std::cout << "kmean_delta:\t" << x <<"\t" <<y<<"\t"<<n_bar<<"\t" << kernels[ KERNEL_INDEX(x,y,k) ] <<"\t" << kernels[ KERNEL_INDEX(x,y,k) ] * n_bar<< "\n";
        delta = m_deltas[ KERNEL_INDEX(x,y,k) ];
        m_means[k] += delta * n_bar;
        m_var[k] += delta * sqrt(n_bar);
#if DUMP_BINNING
        std::cout << "kmeanD:\t" << x << "\t" << y << "\t" << n_bar << "\t" << m_deltas[ KERNEL_INDEX(x,y,k) ] << "\t" << m_deltas[ KERNEL_INDEX(x,y,k) ] * n_bar<< "\n";
#endif
      }
    }
    std::cout << "kernel_mean_sd:\t"<<k<<"\t"<<m_means[k]<<"\t"<<m_var[k] <<"\t" << total_n_expected <<"\t" << ids <<"\n";
  }
}

/*
//void calc_kernel_means(float* kernels, float* kernel_offset ,const std::vector<variant_spec>& variants, std::vector<float>& kernel_means, PairedReadLikelihoodModel& model) {
void calc_kernel_var(float* kernels, float* kernel_offset ,const std::vector<variant_spec>& variants, std::vector<float>& kernel_var, PairedReadLikelihoodModel& model) {
  int nv = variants.size();
  kernel_var.resize(nv);
  double n_bar ;

  for(int k=0;k<nv;k++) {
    kernel_var[k]=0.0;
  }

  for(int x=-WW;x<WW;x++) {
    for(int y=1;y<HH;y++) {
      model.n_bar(BINSIZE,BINSIZE,BINSIZE*(y-1),&n_bar);
      for(int k=0;k<nv;k++) {
  kernel_var[k] += kernels[ KERNEL_INDEX(x,y,k) ] * sqrt(n_bar)  ;
      }
    }
  }
}
*/

int SVKernelScorer::get_region(const variant_spec& variant,
                               int x,
                               int y) const {
  int D = variant.D;
  zone_t zonex = CENTER;
  zone_t zoney = CENTER;

  assert(x<=y);

  if      (x<(-D/2)) { zonex = LEFT; }
  else if (x> (D/2)) { zonex = RIGHT; }

  if      (y<(-D/2)) { zoney = LEFT; }
  else if (y> (D/2)) { zoney = RIGHT; }

  if ( (zonex==LEFT  ) && (zoney==CENTER) ) { return(0); }
  if ( (zonex==CENTER) && (zoney==CENTER) ) { return(3); }
  if ( (zonex==CENTER) && (zoney==RIGHT ) ) { return(1); }
  if ( (zonex==LEFT  ) && (zoney==RIGHT ) ) { return(2); }
  return(-1);
}

void SVKernelScorer::get_variant_separations(const variant_spec& variant,
                                             std::vector<int>& dv,
                                             int x,
                                             int y,
                                             int region) const {
  dv.clear();
  int D = variant.D;
  //  std::cout << "region:" << "\t" << region << "\n";

  switch (variant.type) {
  case INVERSION:
    switch(region) {
    case 0:                    dv.push_back(-y-x);          break;
    case 1:                    dv.push_back(y+x);           break;
    case 2:                    dv.push_back(y-x);           break;
    case 3:                    dv.push_back(y-x);           break;
    }
    break;

  case DELETION:
    switch(region) {
    case 0:                             break;
    case 1:                             break;
    case 2:                    dv.push_back(y-x-D);           break;
    case 3:                             break;
    }
    break;

  case DUPT:
    switch(region) {
    case 0:                    dv.push_back(y-x); dv.push_back(y-x+D);           break;
    case 1:                    dv.push_back(y-x); dv.push_back(y-x+D);           break;
    case 2:                    dv.push_back(y-x+D);                              break;
    case 3:                    dv.push_back(y-x);
                               dv.push_back(y-x);
                               dv.push_back(y-x+D);
                               dv.push_back(D-y+x);                              break;
    }
    break;

  case DUPL:
    switch(region) {
    case 0:                    dv.push_back(-y-x); dv.push_back(y-x+D);           break;
    case 1:                    dv.push_back(y-x); dv.push_back(y+x+D);           break;
    case 2:                    dv.push_back(y-x+D);                              break;
    case 3:                    dv.push_back(y-x);
                               dv.push_back(y-x);
                               dv.push_back(y+x+D);
                               dv.push_back(D+y+x);                              break;
    }
    break;

  case DUPR:
    switch(region) {
    case 0:                    dv.push_back(y-x); dv.push_back(-y-x+D);           break;
    case 1:                    dv.push_back(y-x+D); dv.push_back(y+x);           break;
    case 2:                    dv.push_back(y-x+D);                              break;
    case 3:                    dv.push_back(y-x);
                               dv.push_back(y-x);
                               dv.push_back(D-y-x);
                               dv.push_back(D-y-x);                              break;
    }
    break;

  default:
    assert(1==0);
  }

}

// double SVKernelScorer::pairs_per_window() const {
//   double result=0.0;
//   double n_bar ;

//   for(int y=1;y<HH;y++) {
//     m_model->n_bar(m_binsize,m_binsize,m_binsize*(y-1),&n_bar);
//     result += n_bar;
//   }

//   result *= W2;
//   return result;
// }

void SVKernelScorer::find_variants(float score_threshold,
                                   double zmask_threshold,
                                   int zmask_window,
                                   int zmask_step) {
  INFO("Scanning for structural variants using %i thread(s)...\n\n",
       m_threads);

  // Initialize scores container
  init_scores();

  HRTaskQueue<HRKernelScorerTask> queue;

  int tpt = m_variants.size() / m_threads;
  int tptr = m_variants.size() % m_threads;
  int i, chunk, start=0;
  for (i = 0; i < m_threads; ++i) {
    chunk = i < tptr ? tpt+1: tpt;
    queue.enqueue(HRKernelScorerTask(start, start+chunk));
    start += chunk;
  }
  queue.close();

  std::vector<KernelScorerThread*> threads;
  for (i = 0; i < m_threads; ++i) {
   KernelScorerThread* t = new KernelScorerThread(this,
                                                  queue,
                                                  score_threshold,
                                                  zmask_threshold,
                                                  zmask_window,
                                                  zmask_step);
    if (t->start())
      throw std::runtime_error("Error starting thread.");
    threads.push_back(t);
  }

  std::vector<KernelScorerThread*>::iterator it;
  for (it = threads.begin(); it != threads.end(); ++it)
    if ((*it)->join())
      throw std::runtime_error("Error joining thread.");

  // Clean up
  for (it = threads.begin(); it != threads.end(); ++it)
    delete *it;

  threads.clear();
}

void SVKernelScorer::prune_variants() {
  if (m_calls.empty())
    return;

  INFO("Pruning candidate list...\n\n");

  // Sort variants by score
  std::sort(m_calls.begin(),
            m_calls.end(),
            [](const VariantCall& lhs, const VariantCall& rhs) {
              return lhs.score >= rhs.score;
            });


  // Remove variant calls that are too close to higher-scoring calls.
  std::vector<VariantCall>::iterator i, j;
  for (i=m_calls.begin(); i!=m_calls.end(); ++i) {
    const int& iwidth = m_variants[i->variant].D/m_binsize/2;
    for (j=i+1; j!=m_calls.end();) {
      const int& jwidth = m_variants[j->variant].D/m_binsize/2;
      if ((j->bin + jwidth) < (i->bin - iwidth) ||
          (j->bin - jwidth) > (i->bin + iwidth) ) {
        ++j;
        continue;
      }
      j = m_calls.erase(j);
    }
  }

  // Sort variants by bin
  std::sort(m_calls.begin(),
            m_calls.end(),
            [](const VariantCall& lhs, const VariantCall& rhs) {
              return lhs.bin < rhs.bin;
            });
}

void SVKernelScorer::dump_variants(std::string fp) const {
  INFO("Writing %lu putative structural variant(s) to '%s'...\n\n",
       m_calls.size(),
       fp.c_str());

  std::ofstream stream(fp);
  std::vector<VariantCall>::const_iterator i;
  for (i=m_calls.begin(); i!=m_calls.end(); ++i) {
    // Determine genome position of variant
    std::string chrom;
    int64_t pos;
    m_binner.debin(i->bin+m_begin, chrom, pos);

    stream << m_binsize << "\t"
           << i->bin << "\t"
           << chrom << "\t"
           << pos << "\t"
           << m_variants[i->variant].id() << "\t"
           << i->score << "\n";
  }
}

void SVKernelScorer::dump_scores(std::string fp) const {
  INFO("Writing data archive to '%s'...\n\n", fp.c_str());

  const unsigned int nkernels = m_variants.size();
  const unsigned int nbins = m_end - m_begin;

  std::vector<int> kernels;
  std::vector<variant_spec>::const_iterator it;
  for (it=m_variants.begin(); it!=m_variants.end(); ++it)
    it->serialize_to_vec(kernels);

  const unsigned int val_shape[] = {1};
  cnpy::npz_save(fp, "offset", &m_begin, val_shape, 1, "w");
  cnpy::npz_save(fp, "binsize", &m_binsize, val_shape, 1, "a");
  const unsigned int k_shape[] = {nkernels, 3};
  cnpy::npz_save(fp, "kernels", kernels.data(), k_shape, 2, "a");
  const unsigned int s_shape[] = {nkernels, nbins};
  cnpy::npz_save(fp, "scores", m_scores.data(), s_shape, 2, "a");
}

void KernelScorerThread::handle_task(const HRKernelScorerTask& task) {
  const int& begin = me->m_begin;
  const int& end = me->m_end;
  const int nbins = end-begin;
  int k, x, xb, yb, r, s, center, height;

  for (k = task.start; k < task.end; ++k) {
    auto ids = me->m_variants[k].id() ;
    std::vector<float> k_scores(nbins);

    for (x = begin; x < end; ++x) {
    // while (((x-x0)<MAXBIN) && x < binned_reads.back().xbin + WW) {
      // fastforward(binned_reads,i,j,x);
      // std::cout << "Bin:" << x << "\t"
      //           << N << "\t"
      //           << i << "\t"
      //           << j << "\t"
      //           << j-i << "...\n";

      // local_n_reads = j-i;
      // for (k=0;k<nv;k++) {
      //   //TODO:  figure out the appriate value of N0 and switch to this:
      //   // scores_table[SCORE_INDEX(x-x0,k) ] = (local_n_reads / expected_pairs_per_window)*score_offset[k];
      //   //scores_mat[k][x-x0] = score_offset[k];
      //   scores_mat[k][x-x0] = score_offset[k];
      // }
      k_scores[x-begin] = me->m_offsets[k];

      const Window& w = me->m_windows[x-begin];
      for (r = w.read_start; r < w.read_end; ++r) {
        xb = me->m_binned_reads[r].xbin - x;
        yb = me->m_binned_reads[r].ybin;
        // N++;
        //score[k] += kernel_deltas[xb*(NKERNELS*HH) + yb*NKERNELS + k];

        // assert( SCORE_INDEX(x-x0,k) >= 0 );
        // assert( SCORE_INDEX(x-x0,k) < NKERNELS*MAXBIN );
        // assert( KERNEL_INDEX(xb,yb,k) >= 0 );
        // assert( KERNEL_INDEX(xb,yb,k)< NKERNELS*W2*HH );
        //    scores_table[SCORE_INDEX(x-x0,k) ] += kernel_deltas[ KERNEL_INDEX(xb,yb,k) ] + scores_table[SCORE_INDEX(x-1-x0,k) ]  ;
        k_scores[x-begin] += me->m_deltas[ KERNEL_INDEX(xb,yb,k) ];
      }

      for (r = w.mask_start; r < w.mask_end; ++r) {
        // assert(xb>=-WW);
        // assert(xb<  WW);
        // assert(AREAVS_INDEX(xb,k)>=0);
        // assert(AREAVS_INDEX(xb,k)<W2*me->m_variants.size());
        xb = me->m_masked_bins[r].first - x;
        k_scores[x-begin] -= me->m_areaVs[ AREAVS_INDEX(xb,k) ];
        for (s = r+1; s < w.mask_end; ++s) {
          yb = me->m_masked_bins[s].first - x;
          center = (xb+yb)/2;
          height = yb-xb;
          if (height>=HH) { continue;}
          // assert( KERNEL_INDEX(center,height,k) <  me->m_variants.size()*W2*HH);
          // assert( KERNEL_INDEX(center,height,k) >=  0);
          k_scores[x-begin] += me->m_areaTij[ KERNEL_INDEX(center,height,k) ];
        }
      }
    }

#if DUMP_SCORE
    std::cout << "score:\nscore:\nscore:\n";
    for (x = begin; x < end; ++x) {
      std::cout << "score:\t" << x << "\t" << k_scores[x-begin] << "\t" << k << "\t" << ids << "\n";
    }
#endif

#ifdef RAW
    std::copy(k_scores.begin(),
              k_scores.end(),
              me->m_scores.begin() + k*nbins);
#endif

    // Score normalization
    m_normalizer.normalize(k_scores);

    // Copy Z-scores to 1D array
    const std::vector<double>& zscores = m_normalizer.get_zscores();

#if DUMP_SCORE
    std::cout << "zscore:\nzscore:\nzscore:\n";
    for (std::vector<double>::const_iterator z = zscores.begin(); z < zscores.end(); ++z) {
      std::cout << "zscore:\t" << *z << "\n";
    }
#endif

#ifndef RAW
    std::copy(zscores.begin(),
              zscores.end(),
              me->m_scores.begin() + k*nbins);
#endif

    // Call peaks in normalized scores
    std::vector<int> peaks;
    detect_peaks(k_scores,
                 peaks,
                 m_threshold,
                 me->m_variants[k].D/me->m_binsize);

    std::vector<int>::const_iterator it;
    for (it=peaks.begin(); it!=peaks.end(); ++it) {
      VariantCall call(*it, k, k_scores[*it]);
      me->log_call(call);
    }
  }
}

int usage() {
  std::string str;

  std::cerr << "Welcome to SAVVY.\n\n"
            << "Usage: savvy [<options>] BAM1 [BAM2 ...]" << "\n\n"
            << "Options:" << "\n";
  for (int i = 0; i < MAX_OPT; ++i) {
    const option& opt = OPTS[i];
    str = opt.name;
    for (auto &c: str) c = std::toupper(c);
    if (opt.val != 0)
      std::cerr << "-" << static_cast<char>(opt.val) << " " << str << ", ";
    std::cerr << "--" << opt.name << " " << str << "\n"
              << "\t" << OPT_HELP[i] << "\n";
  }
  return 0;
}

int main(int argc, char * argv[]) {
  // LLR threshold
  float score_threshold = 20.0f;
  // Datamodel
  std::ifstream model_stream;
  // Extracting and validating read pairs
  std::vector<std::string> bamfiles;
  std::string region = ".";
  // Prefix for variants and archive dump
  std::string outfilename="savvy";
  // Parallelization
  int threads = 1;
  // MAPQ threshold
  int read_pair_min_qual = 10;
  // Bin size
  int binsize = 300;
  // Kernels of interest
  int variant_min_size = 1000;
  int variant_max_size = 100000;
  int variant_step_size = 1000;
  std::string variant_types = "INV,DEL,DUPT,DUPL,DUPR";
  std::string variant_zygosity = "HET,HOMO";
  std::string kernel_cache;
  // Zmasking (Normalization)
  double zmask_threshold = 4;
  int zmask_window = 10000;
  int zmask_step = 100;
  // Low mappability region masking
  std::string masked_bed;

  //Parse options
  int c, longopt;
  while (true) {
    longopt = 0;
    c = getopt_long(argc, argv, "t:m:r:o:w:h?", OPTS, &longopt);
    if (c == -1) break;

    switch (c) {
      case 't': score_threshold = std::stod(optarg);            break;
      case 'm': model_stream.open(optarg);                      break;
      case 'r': region = std::string(optarg);                   break;
      case 'o': outfilename = std::string(optarg);              break;
      case 'w': threads = atoi(optarg);                         break;
      case 'h':
      case '?': return usage();
      case 0:
      {
        switch (longopt) {
          case OPT_MIN_QUAL:
            read_pair_min_qual = atoi(optarg);
            break;
          case OPT_BINSIZE:
            binsize = std::stoi(optarg);
            break;
          case OPT_VARIANT_MIN_SIZE:
            variant_min_size = std::stoi(optarg);
            break;
          case OPT_VARIANT_MAX_SIZE:
            variant_max_size = std::stoi(optarg);
            break;
          case OPT_VARIANT_STEP_SIZE:
            variant_step_size = std::stoi(optarg);
            break;
          case OPT_VARIANT_TYPES:
            variant_types = std::string(optarg);
            break;
          case OPT_VARIANT_ZYGOSITY:
            variant_zygosity = std::string(optarg);
            break;
          case OPT_KERNEL_CACHE:
            kernel_cache = std::string(optarg);
            break;
          case OPT_ZMASK_THRESHOLD:
            zmask_threshold = std::stod(optarg);
            break;
          case OPT_ZMASK_WINDOW:
            zmask_window = std::stoi(optarg);
            break;
          case OPT_ZMASK_STEP:
            zmask_step = std::stoi(optarg);
            break;
          case OPT_MASKED_BED:
            masked_bed = std::string(optarg);
            break;
        }
      }
    }
  }

  for (int i = optind; i != argc; ++i)
      bamfiles.push_back(argv[i]);

  // Verify that at least one BAM file has been specified
  if (bamfiles.empty()) {
    INFO("No BAM files were specified.\n");
    return usage();
  }

  try {
    // Initialize scoring engine
    SVKernelScorer* scorer = new SVKernelScorer(binsize, threads);

    // Load kernels from cache, if possible.
    bool using_cached_kernels =
      !kernel_cache.empty() && scorer->load_kernels_from_cache(kernel_cache);

    // Parse model, if necessary.
    bool use_existing_model = model_stream.is_open() && model_stream.good();
    if (!using_cached_kernels && use_existing_model) {
      // Parse model
      std::string model_json;
      std::getline(model_stream, model_json);
      scorer->load_model_from_json(model_json);
    }

    // Read in and bin reads and/or fit model, if necessary
    scorer->prepare_data(read_pair_min_qual,
                         region,
                         bamfiles,
                         masked_bed,
                         !using_cached_kernels && !use_existing_model);

    //    scorer->correct_model_for_masking();

    if (!using_cached_kernels) {
      // Build list of variants for which to determine scores
      scorer->build_variant_list(variant_min_size,
                                 variant_max_size,
                                 variant_step_size,
                                 variant_types,
                                 variant_zygosity);
      // Calculate kernel deltas and offsets
      scorer->calc_kernel_elements();
      // Write kernel descriptions to file
      scorer->dump_kernels(kernel_cache);
    }

    // Threaded score calculation and normalization
    scorer->cache_window_data();
    scorer->find_variants(score_threshold,
                          zmask_threshold,
                          zmask_window,
                          zmask_step);
    scorer->prune_variants();

    if (!outfilename.empty()) {
      // Write scores out to file
      scorer->dump_scores(outfilename+".svz");

      // Write out variants to file
      scorer->dump_variants(outfilename+".svy");
    }
  }
  catch (std::exception& e) {
    ERROR(e.what());
  }

  // int nv = variants.size();
  // int x, k;
  // float* kernel_deltas ;
  // float* scores_table ;
  // kernel_deltas = (float*) malloc( sizeof(float)*NKERNELS*W2*HH );
  // std::cout << "allocated " << sizeof(float)*NKERNELS*W2*HH / 1024 / 1024 / 1024  << " Gb for the kernels\n";

  // for(x=-WW;x<WW;x++)
  //   for(int y=0;y<HH;y++)
  //     kernel_deltas[ KERNEL_INDEX(x,y,0) ] = -15.0;

  // float score_offset[NKERNELS]={};
  // calc_kernel_elements(kernel_deltas,variants, *model );
  // calc_kernel_offsets( score_offset ,variants, *model );

  // double n_over_g = model->N()/model->genome_size();
  // std::cout << "N/G:" << n_over_g << "\n";
  // for (k=0;k<nv;k++)
  //   score_offset[k]*=n_over_g ;

  // std::vector<float> kernel_var;
  // std::vector<float> kernel_means;
  // const double expected_pairs_per_window =  pairs_per_window(*model)  ;
  // calc_kernel_means(kernel_deltas, score_offset , variants, kernel_means,*model) ;
  // calc_kernel_var(  kernel_deltas, score_offset , variants, kernel_var  ,*model) ;

  // std::cout << "done populating the kernel elements\n";

  // if (debug) {
    // for(int k=0;k<nv;k++)
    //   std::cout << "offset:\t" << score_offset[k] << "\n";

    // for(int k=0;k<nv;k++) {
    //   std::cout << "kmean:\t" << kernel_means[k] << "\n";
    // }

    // for(int k=0;k<nv;k++) {
    //   std::cout << "kvar:\t" << kernel_var[k] << "\n";
    // }
  // }

  // std::cout << "total n reads: " << binned_reads.size() << "\n";
  // std::sort(binned_reads.begin(),binned_reads.end());
  // std::cout << "Done sorting the reads\n";
  // std::cout << "Bin range:\t" << binned_reads.front().xbin << "-" << binned_reads.back().xbin << "\n";

  // uint64_t N=0;
  // int i=0,j=0;
  // int local_n_reads;

  //main loop:
  // const int x0 = binned_reads.front().xbin - WW+1;
  // const int xX = std::min<int>(binned_reads.back().xbin+WW, x0+MAXBIN);

  // x=x0;
  // for (k=0;k<nv;k++) {
  //   scores_mat[k][x-x0] = score_offset[k] ;
  // }
  // x++;

  // Cache read boundaries for every window
  // std::vector<std::pair<int, int> > bounds;
  // for (x = x0; x < xX; ++x) {
  //   // Iterate forward to first read inside window
  //   while ( x - binned_reads[i].xbin > WW ) { i++; }
  //   // Iterate forward to first read outside of window
  //   while ( j<binned_reads.size() && binned_reads[j].xbin - x < WW ) { j++; }
  //   bounds.push_back(std::make_pair(i, j));
  // }

  // Initialize array of scores to be written to a .npy file.
  // scores_table = (float*) malloc( sizeof(float)*NKERNELS*MAXBIN );
  // std::cout << "allocated " << sizeof(float)*NKERNELS*MAXBIN / 1024 / 1024 / 1024  << " Gb for the scores table\n";



  //    float z=0.0;  for(k=0;k<NKERNELS;k++) {    z+=score[k];  }

  //  char filename[] = "test.bin";
  // std::ofstream outfile(outfilename , std::ios::out | std::ios::binary);
  // outfile.write( (const char *) scores_table ,  sizeof(float)*(MAXBIN+1)*NKERNELS );
  // outfile.close();

  //    std::cout << "z: " << z << " \n";
  // std::cout << "total N float adds: " << N << " \n";

  // std::cout << "debug: " << debug << " \n";


  // if (debug) {
    //      for(k=0;k<variants.size();k++) {
    //	std::cout << "offset:\t" << score_offset[k] << "\n"; // << binned_reads[x].ybin <<"\n";
    // }

    /*
    for(x=0;x<binned_reads.size();x++) {
std::cout << "read:\t" << binned_reads[x].xbin << "\t" << binned_reads[x].ybin <<"\n";
}*/

    // for(x=-WW;x<WW;x++)
    //   for(int y=0;y<HH;y++)
    //     std::cout << "k:\t" << x <<"\t" << y <<"\t" << kernel_deltas[ KERNEL_INDEX(x,y,0) ]  << "\n" ;

    // for(x=x0;x<binned_reads.back().xbin + WW;x++) {
    //   std::cout << "score:\t" << x*BINSIZE ;
    //   for(k=0;k<variants.size();k++)
    //     std::cout << "\t" << scores_table[SCORE_INDEX(x-x0,k)] << "\n";
    // }
  // }
  INFO("Done.\n");
  return 0;
}

