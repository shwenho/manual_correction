#include <string>
#include <vector>
#include <iostream>

#include "picojson.h"
#include "chicago_read.h"
#include "segment_set.h"
#include "paired_read_likelihood_model.h"
#include "gtest/gtest.h"


TEST(ContigPairTest, ArrayTypeTest) {
  
  Read r1;
  //Read r2;

  int gap=300;
  int l1=2300;
  int l2=5700;
  int x=7;
  int y=32;
  int sep;

  ContigPair cp(l1,l2,gap);
  
  cp.target_1 = 1;
  cp.target_2 = 2;
  
  r1.target_1 = 1;
  r1.target_2 = 2;

  r1.pos_1 = x;
  r1.pos_2 = y;
  
  sep = cp.separation(r1);

  if (sep!=l1+gap+y-x) { // todo: why -1 ?
    cp.dump(r1);
    fprintf(stdout,"sep=%d %d\n",sep,l1+gap+y-x );
    assert( cp.separation(r1)==l1+gap+y-x );
  }

  cp.set_strand_2(1);
  sep = cp.separation(r1);

  if (sep!=l1+l2+gap-x-y) { // todo: why -1 ?
    cp.dump(r1);
    fprintf(stdout,"sep=%d \t %d (%d,%d)\n",sep,l1+l2+gap-x-y,cp.strand_1,cp.strand_2);
    assert( cp.separation(r1)==l1+gap );
  }

  cp.set_strand_1(1);
  sep = cp.separation(r1);

  if (sep!=x+l2+gap-y) { // todo: why -1 ?
    cp.dump(r1);
    fprintf(stdout,"sep=%d %d\n",sep,x+l2+gap-y);
    assert( cp.separation(r1)==x+l2+gap-y );
  }

  cp.set_strand_2(0);
  sep = cp.separation(r1);

  if (sep!=gap+x+y) { // todo: why -y ?
    cp.dump(r1);
    fprintf(stdout,"sep=%d %d\n",sep,gap+x+y);
    assert( cp.separation(r1)==gap+x+y);
  }

  //
  //
  //
  //


  //  cp.target_1 = 1;
  //cp.target_2 = 2;
  
  r1.target_1 = 2;
  r1.target_2 = 1;

  cp.set_strand_2(0);
  cp.set_strand_1(0);


  //r1.pos_1 = x;
  //r1.pos_2 = y;
  
  sep = cp.separation(r1);

  if (sep!=l1+gap+x-y) { // todo: why -1 ?
    cp.dump(r1);
    fprintf(stdout,"sep=%d %d\n",sep,l1+gap+x-y );
    assert( cp.separation(r1)==l1+gap+x-y );
  }

  cp.set_strand_2(1);
  sep = cp.separation(r1);

  if (sep!=l1+l2+gap-x-y) { // todo: why -1 ?
    cp.dump(r1);
    fprintf(stdout,"sep=%d \t %d (%d,%d)\n",sep,l1+l2+gap-x-y,cp.strand_1,cp.strand_2);
    assert( cp.separation(r1)==l1+gap );
  }

  cp.set_strand_1(1);
  sep = cp.separation(r1);

  if (sep!=y+l2+gap-x) { // todo: why -1 ?
    cp.dump(r1);
    fprintf(stdout,"sep=%d %d\n",sep,x+l2+gap-y);
    assert( cp.separation(r1)==x+l2+gap-y );
  }

  cp.set_strand_2(0);
  sep = cp.separation(r1);

  if (sep!=gap+x+y) { // todo: why -y ?
    cp.dump(r1);
    fprintf(stdout,"sep=%d %d\n",sep,gap+x+y);
    assert( cp.separation(r1)==gap+x+y);
  }



 }
