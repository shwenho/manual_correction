#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "link_range_iterator.h"
#include "hirise_threads.h"
#include "oo_requests.h"
#include <fenv.h>

//const std::string model_json = "{\"N\": 338657.5978329083, \"beta\": [0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05], \"Nn\": 7138.1194359941255, \"fn\": \"338657.5978329083*(7.02589624611152e-12+(1.0-0.02107768873833456)*(0.0010213324568692176*exp(-(x)/500.0) + 0.000211434184175803*exp(-(x)/900.8241153272055) + 2.4126299880631892e-05*exp(-(x)/1622.9681735100849) + 3.2339077161572e-05*exp(-(x)/2924.0177382128654) + 2.5567888732372052e-05*exp(-(x)/5268.0513844533225) + 3.2092501138910775e-06*exp(-(x)/9491.175455796849) + 0.0*exp(-(x)/17099.759466766965) + 0.0*exp(-(x)/30807.751387916716) + 0.0*exp(-(x)/55504.730778481135) + 0.0*exp(-(x)/99999.99999999997)))\", \"alpha\": [0.5106662284346087, 0.19046501191009718, 0.039156216850825716, 0.0945600352578711, 0.1346929516341211, 0.030459555912476236, 0.0, 0.0, 0.0, 0.0], \"pn\": 0.02107768873833456, \"G\": 32155272.0}";


//const std::string model_json = "{\"N\": 169328.5, \"beta\": [0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05], \"Nn\": 7138.1194359941255, \"fn\": \"338657.5978329083*(7.02589624611152e-12+(1.0-0.02107768873833456)*(0.0010213324568692176*exp(-(x)/500.0) + 0.000211434184175803*exp(-(x)/900.8241153272055) + 2.4126299880631892e-05*exp(-(x)/1622.9681735100849) + 3.2339077161572e-05*exp(-(x)/2924.0177382128654) + 2.5567888732372052e-05*exp(-(x)/5268.0513844533225) + 3.2092501138910775e-06*exp(-(x)/9491.175455796849) + 0.0*exp(-(x)/17099.759466766965) + 0.0*exp(-(x)/30807.751387916716) + 0.0*exp(-(x)/55504.730778481135) + 0.0*exp(-(x)/99999.99999999997)))\", \"alpha\": [0.5106662284346087, 0.19046501191009718, 0.039156216850825716, 0.0945600352578711, 0.1346929516341211, 0.030459555912476236, 0.0, 0.0, 0.0, 0.0], \"pn\": 0.02107768873833456, \"G\": 32155272.0}";


//void inline MapCheck(Base b, Base b2, Base b3, const PathSet& scaffolds) {
void inline MapCheck(Base b, const PathSet& scaffolds) {
  Base b2;
  Base b3;
  bool flip;
  scaffolds.ScaffoldCoord(b,&b2,&flip);
  scaffolds.ContigCoord(b2,&b3,&flip);
  assert(b==b3);  
  fprintf(stdout,"(%d,%d) -> (%d,%d)\n",b.target_id,b.x,b2.target_id,b2.x);
  fprintf(stdout,"\t-> (%d,%d)\n",b3.target_id,b3.x);
}

inline bool looks_like_bamfile(std::string const &filename) {
  if ( filename.length() >= 5 ) {
    return (0== filename.compare( filename.length() - 4, 4, ".bam" ) );
  } 
  return false;
}

inline bool file_exists (const std::string& name) {
  ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }   
}

int main(int argc, char * argv[]) {
  
  bool use_broken_contig=false;
  bool layout=false;
  bool dump_reads=false;
  bool restore_dump=false;
  bool read_bed=false;
  bool useDepthPenalty=false;
  bool noJoin=false;
  int pad=0;
  int minsep=0;
  int mapq = 0;
  int n_threads=16;
  std::string hrafile;
  std::vector<std::string> model_files;
  std::string reads_dump_out;
  std::string output_file;
  std::string reads_dump_in ;
  std::string bedfile ;
  std::vector<std::string> bamfiles;
  std::string shotfile = "";
  std::string oo_req_file = "";

  //  std::vector<std::string> ranges;
  //-p 100000 -m 500 -q 5 
  int ii=1;
  while (ii<argc) {
    if ( strcmp(argv[ii],"-q")==0 ) {
      ii++;
      mapq = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-i")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing HRA file: %s\n",argv[ii]); exit(0); }
      hrafile = argv[ii];
      layout=true;
    } else if  ( strcmp(argv[ii],"-j")==0 ) {
      ii++;
      n_threads = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-X")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing reads dump input file: %s\n",argv[ii]); exit(0); }
      reads_dump_in = argv[ii];
      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-M")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing model file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"model_file: %s\n",argv[ii]);
      model_files.push_back(std::string(argv[ii]));
      //      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-o")==0 ) {
      ii++;
      output_file = argv[ii];
      //      fprintf(stdout,"N: %d %d\n",ii,std::stoi(argv[ii]));
    } else if ( strcmp(argv[ii],"-O")==0 ) {
      ii++;
      reads_dump_out = argv[ii];
      dump_reads=true;
    } else if  ( strcmp(argv[ii],"-b")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing BAM file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"bamfile: %s\n",argv[ii]);
      bamfiles.push_back(std::string(argv[ii]));
    } else if  ( strcmp(argv[ii],"-m")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing masking BED file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"masking_bedfile: %s\n",argv[ii]);
      bedfile =argv[ii];
      read_bed=true;
    } else if  ( strcmp(argv[ii],"-s")==0 ) {
      ii++;
      if (!file_exists(argv[ii]) && strcmp(argv[ii], "fake") != 0) { fprintf(stdout,"Missing shotgun BAM file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"shotgun_bamfile: %s\n",argv[ii]);
      shotfile = argv[ii];
    } else if (strcmp(argv[ii], "-d") == 0) {
      useDepthPenalty = false;
    } else if (strcmp(argv[ii], "-D") == 0) {
      useDepthPenalty = true;
    } else if (strcmp(argv[ii], "-B") == 0) {
      use_broken_contig = true;
    } else if (strcmp(argv[ii], "-n") == 0) {
      noJoin = true;
    } else if  ( strcmp(argv[ii],"-r")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing o&o requests file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"o&o_requests: %s\n",argv[ii]);
      oo_req_file =argv[ii];
    } else {
      fprintf(stdout,"unrecognized option: %s\n",argv[ii]);
      exit(0);
    }
    ii++;
  }

  if (argc<3) {
    fprintf(stdout,"usage: %s [-j <n threads>=16] -m <mask bedfile> -b <bamfile> [-b <bamfile> ... ] [-s <shotgunbam>] -M <modelfile> [-M <modelfile> ...] [-X <dump restore file>] -o <dump file> -i <hra file> -q <mapq>\n",argv[0]);
    // and -r for oo requests file
    exit(0);
  }

  //
  // Check agreement in # chicago files and # models (if only one model, combine chicago bamfiles)
  //
  int model_increment = 0;
  if (bamfiles.size() > 1 && model_files.size() > 1) {
    if (bamfiles.size() != model_files.size()) {
      fprintf(stdout, "Multiple bamfiles and model files cannot disagree in counts: %lu and %lu\n", bamfiles.size(), model_files.size());
      exit(0);
    }
    model_increment = 1;
  }

  std::vector<PairedReadLikelihoodModel *> models;
  for (int i = 0; i < model_files.size(); i++) {
    std::string model_json;
    std::ifstream model_stream ;
    model_stream.open(model_files[i]);
    //  model_file >> model_json;
    std::getline(model_stream, model_json);
    LikelihoodModelParser model_parser;
    PairedReadLikelihoodModel *model = model_parser.ParseModelJson(model_json.c_str());
    models.push_back(model);
  }
  // feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);

  /*
  if (argc<3) {
    fprintf(stdout, "Expected at least 3 args: bed file, hra file, and one or more bamfiles.  Found %d\n", argc-1);
    exit(0);
    }*/

  ChicagoLinks links;
  PathSet scaffolds;
  OO_Requests oo_reqs;

  auto chrono_start = std::chrono::high_resolution_clock::now();
  links.min_mapq() = mapq;

  if (restore_dump) { 
    links.PopulateFromBam( bamfiles[0].c_str(), false, models[0], 0 ,reads_dump_in);
    links.SetModelsVector(models);
  }
  else {
    // If only one model, use for all chicago bamfiles (model_increment == 0)
    // otherwise chicago & models match up one by one (model_increment == 1)
    for (int i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      std::cout << "Load data from:" <<  bamfiles[i] << "\twith model:\t" << model_files[model_i] << "as bamfile? "<< looks_like_bamfile(bamfiles[i]) << "\n";
    }
    for (int i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      if (looks_like_bamfile(bamfiles[i])) {
	fprintf(stdout,"looks like bamfile: %s\n",bamfiles[i].c_str());
	links.PopulateFromBam(   bamfiles[i] , false, models[model_i], model_i );
      } else {
	fprintf(stdout,"looks like tablefile: %s\n",bamfiles[i].c_str());
	links.PopulateFromTable( bamfiles[i] , false, models[model_i], model_i );	
      }	
    }
    //    links.SortByContigs();
  }

  scaffolds.ParseFromHRA(links.GetHeader(), hrafile );
  if (read_bed) {
    scaffolds.ReadMaskedSegments(bedfile,links.GetHeader());
  }
  const SegmentSet masking = scaffolds.MaskedSegments() ;
  links.SetMaskedFlags( masking );
  
  // "fake" ugliness affects this file only. Can call utility method in a cleaner way later, if needed.
  if (!shotfile.empty()) {
    if (shotfile == "fake") {
      scaffolds.FakeContigSegShotgun();
    }
    else {
      scaffolds.CountContigSegShotgun(shotfile);
    }
    fprintf(stdout, "Switching to MetaHirise\n");
    for (int i = 0; i < models.size(); i++) {
      //ChicagoExpModel* cmodel = dynamic_cast<ChicagoExpModel*>(models[i]);
      models[i]->SetMetagenomic(scaffolds.ShotgunCounted(), useDepthPenalty);
    }
  }

  if (!oo_req_file.empty()) {
    Load_OO_Reqs(oo_reqs, oo_req_file, true);
  }
    
  if (dump_reads) {
    std::cout << "Dumping reads\n";
    links.write_reads(reads_dump_out);
  }
  if (noJoin) {
    std::cout << "Not doing any joins.\n";
    exit(0);
  }

  if (use_broken_contig) {
    fprintf(stderr,"Switching to broken contig coordinates...\n");
    links.SwitchToBrokenContigCoordinates( scaffolds );
  } else {
    fprintf(stderr,"Switching to scaffold coordinates...\n");
    links.SwitchToScaffoldCoordinates( scaffolds );
  }
  fprintf(stderr,"Indexing masked segments...\n");
  scaffolds.IndexMaskedScaffoldSegments();


  //  int n_threads=std::stoi(argv[1]);
  std::vector<JoinScoreWorkerThread *> threads  ;
  HRTaskQueue<HRScaffoldJoinScoreTask> queue;
  //  JoinScoreWorkerThread
  int i;
  int bsize = 512;
  unsigned long long min_links = 1;

  if (! oo_req_file.empty()) {
    min_links = 1;
  }

  char* buffer=0;
  buffer = new char[bsize];
  for(i=0;i<n_threads;i++) {
    sprintf(buffer,(output_file + "_%d.txt").c_str(),i);
    threads.push_back( new JoinScoreWorkerThread(queue,i, std::string(buffer)) );
    threads[i]->start();
    fprintf(stdout,"thread started: %d\t%lx\n",i,threads[i]->id()); fflush(stdout);
  }

  InterScaffoldLinksIterator sc_it(links);

  //  int count=0;
  while(!sc_it.done()) {
    //fprintf("scit: %d %d %d %d\n",(*sc_it)->id1,sc_it->id2,sc_it->mini,sc_id->maxi);
    //fprintf(stdout,"scit: %d %d-%d\n",(*sc_it).id1,(*sc_it).mini,(*sc_it).maxi); //,sc_it->id2,sc_it->mini,sc_id->maxi);
    //links.ScaffoldSupport((*sc_it).mini,(*sc_it).maxi,scaffolds,150000);

    OO_Vec *oov = NULL;

    if (! oo_req_file.empty()) {
      // If we have requests for an id pair, 
      // get the o&o requests vector and flag it as processed
      // by setting the nlinks value of first element.
      SegPair idpair((*sc_it).id1, (*sc_it).id2);
      if (oo_reqs.count(idpair)) {
	oov = &(oo_reqs[std::make_pair((*sc_it).id1, (*sc_it).id2)]);
	oov->front().nlinks = sc_it.nlinks();
      }
      else {
	++sc_it;
	continue;
      }
    }
    // Always enqueue if based on a request, else only if enough links
    if ((! oo_req_file.empty()) || sc_it.nlinks()>=min_links) {      

      //      queue.enqueue( HRScaffoldJoinScoreTask(links,scaffolds,(*sc_it).id1,(*sc_it).id2,(*sc_it).mini,(*sc_it).maxi) , n_threads*1000*2 );       Not working: deadlocks
      queue.enqueue( HRScaffoldJoinScoreTask(links,scaffolds,(*sc_it).id1,(*sc_it).id2,(*sc_it).mini,(*sc_it).maxi,oov) );      
    }
    ++sc_it;
  };

  for (OO_Requests::iterator ooit = oo_reqs.begin(); ooit != oo_reqs.end(); ++ooit) {
    // Check to see if first request of this pair has a nonzero number of links (which means they've been processed)
    OO_Vec *oov = &(ooit->second);
    if (oov->size() && !((oov->front()).nlinks)) {
      //fprintf(stdout,"queuing pair without links: %d %d\n", ooit->first.first, ooit->first.second);
      //fflush(stdout);
      queue.enqueue( HRScaffoldJoinScoreTask(links, scaffolds, ooit->first.first, ooit->first.second, 0, 0, oov));
    }
  }

  queue.close();

  for(i=0;i<n_threads;i++) {
    fprintf(stdout,"thread to join: %d\n",i); fflush(stdout);
    threads[i]->join();
  }

  //links.InspectAllScaffolds(         scaffolds );
  //  links.SwitchToContigCoordinates(scaffolds);

  links.PrintModelInfo();

  //std::cout << scaffolds;

  auto elapsed = std::chrono::high_resolution_clock::now() - chrono_start;
  long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  fprintf(stdout, "Took %lld microseconds for bam, bed and hra loading\n", microseconds);

  exit(0);

}
