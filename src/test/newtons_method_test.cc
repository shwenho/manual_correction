#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include <fenv.h>
#include <Eigen/Dense>

typedef Eigen::ArrayXd abtype;
typedef  std::map<int,double> histogramT;
typedef  std::map<int,double>::iterator hist_it;

double f0(double x, abtype alpha, abtype beta, double pn, double G) {
  double L;
  Eigen::ArrayXd a_scratch ;
  a_scratch = -x * beta;
  a_scratch = a_scratch.exp();
  a_scratch *= alpha*beta;
  L = a_scratch.sum();
  return L;
}

double f(double x, abtype alpha, abtype beta, double pn, double G) {
  double L;
  L = f0(x,alpha,beta,pn,G);
  return (pn/G)  + (1.0-pn)*L;
}

double partial_lnf_partial_alpha_i(double x, abtype alpha, abtype beta, double pn, double G, int i) {
  double L;  
  //  Eigen::ArrayXd a_scratch ;
  L = -x* beta(i) ;
  L = exp(L)      ;
  L *= beta(i)    ;
  L *= (1.0-pn)   ;
  L /= f( x,alpha,beta,pn,G );
  return L;
}

double partial_lnf_partial_pn(double x, abtype alpha, abtype beta, double pn, double G) {
  double L;  
  //  Eigen::ArrayXd a_scratch ;
  L = 1.0/G;
  L -= f0(x,alpha,beta,pn,G);
  L /= f( x,alpha,beta,pn,G );
  return L;
}

double partial2_lnf_partial_pn2(double x, abtype alpha, abtype beta, double pn, double G) {
  double L;  
  L = partial_lnf_partial_pn(x,alpha,beta,pn,G);
  L = -L*L;
  return L;
}

double partial2_lnf_partial_alpha_i_partial_alpha_j(double x, abtype alpha, abtype beta, double pn, double G, int i, int j) {
  double L;  
  //  Eigen::ArrayXd a_scratch ;
  L = -beta[i]*exp(-beta[i]*x) ;
  L *= beta[j]*exp(-beta[j]*x) ;
  L *= (1.0-pn);
  L *= (1.0-pn);
  double ff = f( x,alpha,beta,pn,G );
  L /= ff*ff;
  return L;
}

double partial2_lnf_partial_alpha_i_partial_pn(double x, abtype alpha, abtype beta, double pn, double G, int i) {
  double L1;  
  double L2;  
  double ff;
  //  Eigen::ArrayXd a_scratch ;
  ff =  f(x,alpha,beta,pn,G);

  L1 = -beta[i]*exp(-beta[i]*x) ;
  L1 /= ff;

  L2 = -1.0 / (ff*ff);
  L2 *= ((1.0/G) - f0(x,alpha,beta,pn,G) );
  L2 *= (1.0-pn);
  L2 *= beta[i]*exp(-beta[i]*x);

  return L1+L2;
}



Eigen::VectorXd gradFx( double x, abtype alpha, abtype beta, double pn, double G, Eigen::VectorXd & gF , double count) {


  //  Eigen::VectorXd gF(alpha.size()+1);

  int i;
  for(i=0;i<alpha.size() ;i++) {    
    gF[i] += count*partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,i);  
  }
  i=alpha.size();

  gF[i] += count*partial_lnf_partial_pn(x,alpha,beta,pn,G);

  return gF;

}

Eigen::VectorXd gradF( histogramT histogram, abtype alpha, abtype beta, double pn, double G, double lambda) {
  int n = alpha.size()+2;
  Eigen::VectorXd gF(n);

  //  gF=0.0;
  for(hist_it i=histogram.begin();i!=histogram.end();i++) {
    if (i->second > 0){
      gradFx(i->first,alpha,beta,pn,G,gF,i->second);
    }
  };
  
  for (int i=0;i<alpha.size();i++) {
    gF(i) += lambda;
  }

  gF(n-1) = +( alpha.sum() - 1.0 );

  return gF;
}


Eigen::MatrixXd hessianFx( double x, abtype alpha, abtype beta, double pn, double G, Eigen::MatrixXd & hF , double count) {

  //  std::cout << "hessian size:" << alpha.size() <<"\n";
  //Eigen::MatrixXd hF((alpha.size()+1),(alpha.size()+1));
  
  int i,j;
  for(i=0;i<alpha.size() ;i++) {    
    for(j=0;j<alpha.size() ;j++) {    
              //partial2_lnf_partial_alpha_i_partial_alpha_j
      hF(i,j) += count* partial2_lnf_partial_alpha_i_partial_alpha_j(x,alpha,beta,pn,G,i,j);  
    }
  }

  i=alpha.size();
  double xtmp;
  for(j=0;j<alpha.size() ;j++) {    
    xtmp = count*partial2_lnf_partial_alpha_i_partial_pn(x,alpha,beta,pn,G,j);
    hF(i,j) += xtmp;  
    hF(j,i) += xtmp;
  }
  
  hF(i,i) += count*partial2_lnf_partial_pn2(x,alpha,beta,pn,G);

  return hF;

}


Eigen::MatrixXd hessianF( histogramT histogram, abtype alpha, abtype beta, double pn, double G, double lambda) {
  int n = alpha.size()+2;
  Eigen::MatrixXd h(n,n);

  for(hist_it i=histogram.begin();i!=histogram.end();i++) {
    if (i->second > 0){
      hessianFx(i->first,alpha,beta,pn,G,h,i->second);
    }
  };
  
  int i=n-1;
  for(int j=0;j<alpha.size() ;j++) {    
    h(i,j) = 1.0;  
    h(j,i) = 1.0;
  }

  /*
  h(n-1,n-1) = -1.0e-9;
  h(n-2,n-1) = -1.0e-9;
  h(n-1,n-2) = -1.0e-9;
  */
  
  return h;
}

double loglikelihood(histogramT histogram, abtype alpha, abtype beta, double pn, double G) {
  double L=0.0;
  double x;
  for(hist_it i=histogram.begin();i!=histogram.end();i++) {
    if (i->second > 0){
      x = log( f(i->first,alpha,beta,pn,G) );
      std::cout << "llsum:\t" << i->first << "\t" << i->second << "\n" << x << "\t" << L << "\n";
      L+=i->second * x ;
    }
  };
  
  return L;
  

}

int main(int argc, char* argv[]) {

  int n;
  n=10;
 
  histogramT histogram;
  double lambda=1.0e-7;
  double epsilon = 0.001 ;

  int k,l,m;
  std::ifstream fh(argv[1]);
  while(! fh.eof()) {
    fh >> k;
    fh >> l;
    fh >> m;
    //    std::cout << k <<"\t" <<l <<"\n";

    if (k>1000) {
      histogram[k]=double(l)*1.0e-9;
    }
  }

  histogram[10000000]=double( 10000000 )*1.0e-9;

  fh.close();

  //  histogram[1000]=0.2;
  //histogram[1500]=0.1;

  double x=500.0;
  abtype alpha(n);
  abtype beta(n);
  double pn = 0.4;
  double G = 2.9e9;


  const std::vector<double> betaV = { 0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05};  
  const std::vector<double> alphaV = {0.2596398458617465, 0.2184877929541886, 0.19276735652678798, 0.06612921087467549, 0.13835425638673426, 0.09578541569209319, 0.028836121703774004, 0.0, 0.0, 0.0};
  for(int i=0;i<n;i++) {
    alpha(i) = alphaV[i];
    //	alpha(i) = 1.0;
	beta(i) = betaV[i];
  }

  alpha = (1.0/alpha.sum())*alpha ;


  std::cout << "alpha:\n";
  std::cout << alpha << "\n\n";
  //auto delF = gradF(histogram,alpha,beta,pn,G,lambda);
  //  auto hF1 = hessianF(histogram,alpha,beta,pn,G,lambda);
  //auto ll = loglikelihood(histogram,alpha,beta,pn,G);

  for (x=1000;x<2000000;x+=200) {
    std::cout <<"debug:\t";
    std::cout << x << "\t";
    std::cout << f0(x,alpha,beta,pn,G) << "\t";
    std::cout << f(x,alpha,beta,pn,G) << "\t";
    std::cout << partial_lnf_partial_pn(x,alpha,beta,pn,G) << "\t";
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,0) << "\t";
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,1) << "\t";
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,2) << "\t"; 
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,3) << "\t";
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,4) << "\t";
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,5) << "\t";
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,6) << "\t";
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,7) << "\t";
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,8) << "\t";
    std::cout << partial_lnf_partial_alpha_i(x,alpha,beta,pn,G,9) << "\t";
    std::cout <<"\n";
  }

  for(int kk=0;kk<10;kk++) {


    //std::cout << "lnL:\t";
    //std::cout << ll << "\n\n";

    //std::cout << "delF:\n";
    //std::cout << delF << "\n\n";

  /* 
  for(int i=0;i<n;i++) {
    alpha(i) -= 0.01 * delF(i) ;
    if (alpha(i)<0.0) {alpha(i)=0.0;}
  }
  pn -= 0.1*delF(n);
  */

  //  alpha = (1.0/alpha.sum())*alpha ;

  

  if (pn>1.0) {pn=0.95;}
  if (pn<0.0) {pn=0.05;}

  auto delF = gradF(histogram,alpha,beta,pn,G,lambda);
  auto hF1 = hessianF(histogram,alpha,beta,pn,G,lambda);


  //  hF1*=-1.0;
  std::cout << "hF1:\n";
  std::cout << hF1 << "\n\n";

  std::cout << "hF1 inverse:\n";
  std::cout << hF1.inverse() << "\n\n";
 
  std::cout << "hF1 inverse * hF1:\n";
  std::cout << hF1.inverse()*hF1 << "\n\n";
 
  //  std::cout << delF << "\n";
  std::cout << "alpha:\n";
  std::cout << alpha << "\n\n";

  std::cout << "pn:\t";
  std::cout <<  pn << "\n\n";

  std::cout << "lambda:\t";
  std::cout <<  lambda << "\n\n";

  std::cout << "hF1 inverse * delF:\n";
  std::cout << hF1.inverse() * delF << "\n\n";

  Eigen::MatrixXd step = epsilon* (hF1.inverse() * delF) ;
  std::cout << "step:\n";
  std::cout << step << "\n\n";

  std::cout << "alpha:\n";
  std::cout << alpha << "\n\n";

  auto ll = loglikelihood(histogram,alpha,beta,pn,G);

  std::cout << "params:\t" << kk << "\t" << ll << "\t";
  for(int i=0;i<n;i++) {
    std::cout << alpha(i) << "\t";
  }
    std::cout << pn << "\t";
    std::cout << lambda << "\n";

  for(int i=0;i<n;i++) {
    std::cout << i << "\t";
    std::cout << alpha(i) << "\t";
    std::cout << (step(i)) << "\n";
    alpha(i) += (step(i)) ;
    if (alpha(i)<0.0) {alpha(i)=0.0;}
  }
  alpha   = alpha / alpha.sum()  ;
  pn     += step(n)    ;
  lambda += step(n+1)  ;

  std::cout << "alpha:\n";
  std::cout << alpha << "\n\n";

  std::cout << "pn:\t";
  std::cout <<  pn << "\n\n";

  std::cout << "lambda:\t";
  std::cout <<  lambda << "\n\n";

  }
}


/*
  std::cout << "lnL:\t";
  std::cout << ll << "\n\n";

  std::cout << "delF:\n";
  std::cout << delF << "\n\n";
 
  std::cout << "alpha:\n";
  std::cout << alpha << "\n\n";

  std::cout << "pn:\t";
  std::cout <<  pn << "\n\n";

  std::cout << "lambda:\t";
  std::cout <<  lambda << "\n\n";
  }
}
*/
