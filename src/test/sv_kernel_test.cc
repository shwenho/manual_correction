#include <vector>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <ios>
#include <assert.h>
#include "config.h"
#include "kernel_calc.h"


struct readpair {
/*
  xbin and ybin should have the same relationship to the x and y genome coordinates 
  that center and height have respectively to x and y in the following code:

  for(xb=WW;xb>=-2*WW;xb--) {
    x=xb*BINSIZE;
    for (yb=xb;yb<2*WW;yb++) {
      center = (xb+yb)/2;
      height = (yb-xb);
      if (center < -WW ) {continue;}
      if (center >= WW ) {continue;}
      if (height >= HH) {continue;}
      y=yb*BINSIZE;      
*/
  int xbin;
  int ybin;
  //  strand
};

bool operator< (const readpair& lhs, const readpair& rhs) {
  if (lhs.xbin != rhs.xbin) {
    return (lhs.xbin < rhs.xbin);
  } else {
    return ( lhs.ybin < rhs.ybin ); 
  }
};

void fastforward(const std::vector<readpair>& reads, int* i, int* j, int x) {
  while ( (*i<reads.size()) && x - reads[*i].xbin > WW ) {(*i)++;}
  while ( (*j<reads.size()) && reads[*j].xbin - x < WW ) {(*j)++;}
} 

void init_fake_reads(std::vector<readpair>& reads) {
  int i;
  readpair read;
  for (i=0;i<NREADS;i++) {
    read.xbin = int( (float(i)/float(NREADS))*MAXBIN );
    read.ybin = 2;
    reads.push_back( read );
  }
}

float ff(int x) {
  return( exp(-float(x)/10000.0)/10000.0 );
}

int main(int argc, char * argv[]) {
  
  std::vector<variant_spec> variants;
  // TODO:  populate this a better way.
  variants.push_back( variant_spec(30000,INVERSION,HET) );
  variants.push_back( variant_spec(30000,INVERSION,HOMO) );
  variants.push_back( variant_spec(30000,DELETION,HET) );
  variants.push_back( variant_spec(30000,DELETION,HOMO) );

  float* kernel_deltas ;
  float* scores_table ;
  kernel_deltas = (float*) malloc( sizeof(float)*W2*HH*NKERNELS );
  std::cout << "allocated " << sizeof(float)*W2*HH*NKERNELS / 1024 / 1024 / 1024  << " Gb for the kernels\n";

  calc_kernel_elements(kernel_deltas,variants, &ff);
  std::cout << "done populating the kernel elements\n";

  scores_table  = (float*) malloc( sizeof(float)*(MAXBIN+1)*NKERNELS );
  std::cout << "allocated " << sizeof(float)*float(MAXBIN)*NKERNELS / 1024 / 1024 / 1024  << " Gb for the scores table\n";

  float score[NKERNELS]={};

  std::vector<readpair> reads;
  //  load_read_data(bamfile,&reads)
  //  load_kernel_data(kernelfile,&kernel_deltas)

  //Just for timing / testing:
  init_fake_reads(reads);
  std::cout << "total n fake reads: " << reads.size() << "\n";
  std::sort(reads.begin(),reads.end());
  std::cout << "Done sorting the reads\n";

  uint64_t N=0;
  int i,j=0;
  int k,xb,yb,r,t;
  int x=WW;

  i=0;
  j=0;

  //main loop:
  N=0;
  while (x < MAXBIN) {
    if (x%1000==0) {
      std::cout << "Bin:" << x << "\t" << N  << "\t" << i << "\t" << j  << "\t" << j-i << "...\n";
    }
    fastforward(reads,&i,&j,x);
    for (r=i;r<j;r++) {
      xb = reads[r].xbin - x ;
      yb = reads[r].ybin;
      for (k=0;k<NKERNELS;k++) {
	N+=1;
	//score[k] += kernel_deltas[xb*(NKERNELS*HH) + yb*NKERNELS + k]; 
	assert( SCORE_INDEX(x,k) >=0 );
	assert( SCORE_INDEX(x,k) < (MAXBIN+1)*NKERNELS  );
	assert(  KERNEL_INDEX(xb,yb,k)>=0 );
	assert(  KERNEL_INDEX(xb,yb,k)<W2*HH*NKERNELS );
	scores_table[SCORE_INDEX(x,k) ] += kernel_deltas[ KERNEL_INDEX(xb,yb,k) ]; 
      }
    }
    x++;
  }

  float z=0.0;  for(k=0;k<NKERNELS;k++) {    z+=score[k];  }

  //  char filename[] = "test.bin";
  std::ofstream outfile("test.bin" , std::ios::out | std::ios::binary);
  outfile.write( (const char *) scores_table ,  sizeof(float)*(MAXBIN+1)*NKERNELS );
  outfile.close();

  std::cout << "z: " << z << " \n";  
  std::cout << "total N float adds: " << N << " \n";  

  
}

