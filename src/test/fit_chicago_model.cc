#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include <fenv.h>
#include <Eigen/Dense>
#include "chicago_read_pairs.h"
#include "chicago_bam_filters.h"
#include "chicago_lm.h"
#include "chicago_wSI_lm.h"
//#include "c.h"

#define DEFAULT_MAX_SEP 10000000
#define DEFAULT_G       3.0e9
#define DEFAULT_MIN_Q   10

void dump_usage() {
  std::cout << "fit a chicago model.\n\
options:\n\
 -I           (create a model with short innies)\n\
 -b <bamfilename>         \n\
 -q <min_qual> (default="<< DEFAULT_MIN_Q <<")\n\
 -R <region> (default to all)\n\
 -M <max_separation> (default="<< DEFAULT_MAX_SEP <<")\n\
";
}
// -G genome_size (default="<< DEFAULT_G <<")\n\

int main(int argc, char* argv[]) {

  std::string bamfilename ="" ;
  std::string region = ".";
  int min_qual = DEFAULT_MIN_Q;
  int max_separation = DEFAULT_MAX_SEP;
  double genome_size = DEFAULT_G;
  bool dump_help=false;
  bool model_short_innies = false;
  int c;
  while ((c = getopt(argc, argv, "s:m:b:M:R:o:K:D:z:q:k:G:h?I")) != -1) {
    switch (c) {
    case 'h': dump_usage(); exit(0);  break;
    case '?': dump_usage(); exit(0);  break;
    case 'I': model_short_innies=true;break;
    case 'b': bamfilename = std::string(optarg);  break;
    case 'q': min_qual = atoi(optarg);            break;
    case 'R': region = std::string(optarg);       break;
      //    case 'G': genome_size = atof(optarg);       break;
    case 'M': max_separation = atoi(optarg);       break;
    }
  }

  if (bamfilename=="") {
    dump_usage(); exit(0);
  }

  SVReadPairFilter filter(min_qual);
  filter.keep_noise=true;
  ChicagoReadPairs data;
  data.SetHistogramOnly();
  data.max_separation = max_separation;

  try {
    data.fill_from_bam(filter, bamfilename,region);
    //    binner.load_genome_map(mapfile);
  }
  catch (std::exception &e) {
    std::cerr << e.what() << "\n";
    return 1;
  }

  //  PairedReadLikelihoodModel* model;
  if (model_short_innies) {
    ChicagoSILM model;
    model.fit(data);
    std::cout << model;  // write as json

  } else {
    ChicagoLM model;
    model.fit(data);
    std::cout << model;  // write as json


  }
  //  model.set_genome_size(genome_size);
  //  fit_chicago_model(data,model);

  //  std::cout << *model;  // write as json

  exit(0);

}
