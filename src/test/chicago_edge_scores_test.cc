#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_edge_scores.h"
#include "chicago_links.h"
#include "chicago_scaffolds.h"
#include "gtest/gtest.h"

const std::string model_json = "{\"N\": 338657.5978329083, \"beta\": [0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05], \"Nn\": 7138.1194359941255, \"fn\": \"338657.5978329083*(7.02589624611152e-12+(1.0-0.02107768873833456)*(0.0010213324568692176*exp(-(x)/500.0) + 0.000211434184175803*exp(-(x)/900.8241153272055) + 2.4126299880631892e-05*exp(-(x)/1622.9681735100849) + 3.2339077161572e-05*exp(-(x)/2924.0177382128654) + 2.5567888732372052e-05*exp(-(x)/5268.0513844533225) + 3.2092501138910775e-06*exp(-(x)/9491.175455796849) + 0.0*exp(-(x)/17099.759466766965) + 0.0*exp(-(x)/30807.751387916716) + 0.0*exp(-(x)/55504.730778481135) + 0.0*exp(-(x)/99999.99999999997)))\", \"alpha\": [0.5106662284346087, 0.19046501191009718, 0.039156216850825716, 0.0945600352578711, 0.1346929516341211, 0.030459555912476236, 0.0, 0.0, 0.0, 0.0], \"pn\": 0.02107768873833456, \"G\": 32155272.0}";

TEST(chicago_edge_scores_test, model_function_insert_test) {
  ChicagoExpModel model;
  model.SetModel(model_json.c_str());
  double likelihood;
  model.ModelLikelihood(10000, &likelihood);
  fprintf(stdout, "Got: %f for 10k\n", likelihood);
  FILE *out = fopen("test.out","w");

  int start = 0, end = 200000;
  //  time_t time_start = time(NULL);
  auto chrono_start = std::chrono::high_resolution_clock::now();
  start = 0;
  double F_result;
  double H_result;
  double sum = 0;
  while(start != end) {
    model.ModelLikelihood(start, &likelihood);
    model.F(start, &F_result);
    model.H(start, &H_result);
    // make the compiler actually calculate
    sum += likelihood + F_result + H_result;
    //fprintf(out, "%d %.10g %.10g %.10g\n",start, likelihood, F_result, H_result);
    start++;
  }
  // start = 0;
  // while(start != end) {
  //   model.ModelLikelihood(start, &likelihood);
  //   //fprintf(out, "%d %.10g\n",start, likelihood);
  //   start++;
  // }
  // start = 0;
  // while(start != end) {
  //   model.ModelLikelihood(start, &likelihood);
  //   //fprintf(out, "%d %.10g\n",start, likelihood);
  //   start++;
  // }
  // start = 0;
  // while(start != end) {
  //   model.ModelLikelihood(start, &likelihood);
  //   //fprintf(out, "%d %.10g\n",start, likelihood);
  //   start++;
  // }
  // start = 0;
  // while(start != end) {
  //   model.ModelLikelihood(start, &likelihood);
  //   //fprintf(out, "%d %.10g\n",start, likelihood);
  //   start++;
  // }
  //  time_t time_end = time(NULL);
  auto elapsed = std::chrono::high_resolution_clock::now() - chrono_start;
  long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  fprintf(stdout, "Took %lld microseconds for %g\n", microseconds, sum);
  fclose(out);
}


TEST(chicago_links_test, populate_from_bam_test) {
  auto chrono_start = std::chrono::high_resolution_clock::now();
  ChicagoLinks links;
  links.min_mapq() = 50;
  links.PopulateFromBam("/mnt/scratch1/projects/chuck/rewrite/dovetail/src/test/chr22.chicago_017.snap.md.sorted.bam");
  //  links.PopulateFromBam("/mnt/scratch1/projects/chuck/chm1_pacbio/mbo1_1/aln/chm1_pacbio_001007805.chicago_001.snap.samtools.md.sorted.bam");
  auto elapsed = std::chrono::high_resolution_clock::now() - chrono_start;
  long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  fprintf(stdout, "Took %lld microseconds for bam loading\n", microseconds);
  int32_t range_start, range_end;
  const Read *found = NULL;
  links.FindReads(1, 0, 1000, &range_start, &range_end);
  links.GetRead(range_start, &found);
  fprintf(stdout, "Pos %d %d\n", found->target_1, found->pos_1);
  links.GetRead(range_end, &found);
  fprintf(stdout, "Pos %d %d\n", found->target_1, found->pos_1);

  //links.dumpBitInterleavedCoords();

  //links.FindReadsForSegmentPair( 1,1,10000, 2,1,10000 ); 
  links.FindReadsForContigPair("Scaffold1","Scaffold45");

  links.FindReadsForContigPair("Scaffold2027","Scaffold1243");

  links.FindReadsForContigPair(    1 ,  80 );
  links.FindReadsForContigPair( 1445 , 974 );
  //std::cout << "\n";
  //Scaffold2027    Scaffold1243


}

TEST(chicago_scaffolds_test, parse_hra_test) {
  ChicagoLinks links;
  links.min_mapq() = 50;
  links.PopulateFromBam("chr22.chicago_017.snap.md.sorted.bam");
  PathSet path_set;
  path_set.ParseFromHRA(links, "hirise_iter_0.hra");
  fprintf(stdout, "Got %d scaffolds\n", path_set.NumScaffolds());
}

