#!/usr/bin/perl

open(GOLD,"gold_insert_scores.txt") or die "Can't open gold\n";
open(TEST,"test.out") or die "Can't open test\n";
$sum = 0.0;
$abs_sum = 0.0;
$count = 0;
while($g = <GOLD>) {
    $t = <TEST>;
    chomp($g);
    chomp($t);
    @wg = split / /, $g;
    @wt = split / /, $t;
    $sum = $sum + ($wg[1] - $wt[1]);
    $abs_sum = $abs_sum + abs($wg[1] - $wt[1]);
    $count++;
}
printf STDOUT "Mean difference, abs diff are %.10f and %.10f for %d seen\n", (1.0 * $sum)/$count, (1.0 * $abs_sum)/$count, $count
