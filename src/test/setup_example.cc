#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "link_range_iterator.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "hirise_threads.h"
//#include "oo_requests.h"
#include "hirise_setup.h"
#include "contig_index.h"
#include <fenv.h>

int main(int argc, char * argv[]) {

  ChicagoLinks links; 
  PathSet scaffolds;
  setup(argc,argv,links,scaffolds);

  auto links_index_contigs   =  IndexContigs( links ) ;
  auto links_index_scaffolds =  IndexScaffolds( links , scaffolds) ;
  links.InitContigIndex( links_index_contigs, links_index_scaffolds  );

  links.PrintModelInfo();
  auto n50= scaffolds.N50() ;  std::cout << "L/N50: " <<  n50.first << "\t" << n50.second << "\n";

  std::cout << "done\n";
  return 0;
} 


