#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "link_range_iterator.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "hirise_threads.h"
//#include "oo_requests.h"
#include "contig_index.h"
#include <fenv.h>


//void inline MapCheck(Base b, Base b2, Base b3, const PathSet& scaffolds) {
void inline MapCheck(Base b, const PathSet& scaffolds) {
  Base b2;
  Base b3;
  bool flip;
  scaffolds.ScaffoldCoord(b,&b2,&flip);
  scaffolds.ContigCoord(b2,&b3,&flip);
  assert(b==b3);  
  fprintf(stdout,"(%d,%d) -> (%d,%d)\n",b.target_id,b.x,b2.target_id,b2.x);
  fprintf(stdout,"\t-> (%d,%d)\n",b3.target_id,b3.x);
}

inline bool looks_like_bamfile(std::string const &filename) {
  if ( filename.length() >= 5 ) {
    return (0== filename.compare( filename.length() - 4, 4, ".bam" ) );
  } 
  return false;
}

inline bool file_exists (const std::string& name) {
  ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }   
}

bool move_invalidated(int round, std::map<int,int>& scaffold_mod_round, const ScaffoldMergeMove move, PathSet& scaffolds ) {
  int c1,c2,c3,c4;
  int s1,s2,r,d;
  bool s1_touched, s2_touched;

  c1 = move.join1.id1;
  c2 = move.join1.id2;
  s1 = scaffolds.ContigScaffold(c1);
  s2 = scaffolds.ContigScaffold(c2);

  if ((round==0) || ( (scaffold_mod_round.find(s1) != scaffold_mod_round.end()) && ( scaffold_mod_round[s1]>=round ) )) { s1_touched=true;} else {s1_touched=false;}
  if ((round==0) || ( (scaffold_mod_round.find(s2) != scaffold_mod_round.end()) && ( scaffold_mod_round[s2]>=round ) )) { s2_touched=true;} else {s2_touched=false;}

  if (s1_touched || s2_touched) {return true;} 

  return false;
}


void log_progress_info( std::ostream& out,const std::string& message,const std::chrono::time_point<std::chrono::high_resolution_clock>& chrono_start) {
  std::chrono::seconds s = std::chrono::duration_cast<std::chrono::seconds>(  std::chrono::high_resolution_clock::now() - chrono_start   );
  out << message ;
  out << "[ " <<   
    std::chrono::duration_cast<std::chrono::hours>(s).count() << " hours, "
      << std::chrono::duration_cast<std::chrono::minutes>(s % std::chrono::hours(1)).count() << " minutes, "
      << std::chrono::duration_cast<std::chrono::seconds>(s % std::chrono::minutes(1)).count() << " seconds]"
      << std::endl;
}

int main(int argc, char * argv[]) {
  
  bool use_broken_contig=false;
  bool layout=false;
  bool dump_reads=false;
  bool restore_dump=false;
  bool read_bed=false;
  bool useDepthPenalty=false;
  int pad=0;
  int minsep=0;
  int mapq = 0;
  uint n_threads=16;
  int max_rounds = 10;
  int minscore = 20;
  int maxscore = 100;
  int gapsize = 1000;
  int hard_min_len = 1000;
  int soft_min_len = 1000;
  int min_links = 1;
  int stepsize = 10;
  std::string hrafile;
  std::vector<std::string> model_files;
  std::string reads_dump_out;
  std::string output_file;
  std::string reads_dump_in ;
  std::string bedfile ;
  std::vector<std::string> bamfiles;
  std::string shotfile = "";
  std::string oo_req_file = "";

  //  std::vector<std::string> ranges;
  //-p 100000 -m 500 -q 5 
  int ii=1;
  while (ii<argc) {
    if ( strcmp(argv[ii],"-R")==0 ) {
      ii++;
      max_rounds = std::stoi(argv[ii]);
    } else if ( strcmp(argv[ii],"-q")==0 ) {
      ii++;
      mapq = std::stoi(argv[ii]);
    } else if ( strcmp(argv[ii],"-l")==0 ) {
      ii++;
      min_links = std::stoi(argv[ii]);
    } else if ( strcmp(argv[ii],"-g")==0 ) {
      ii++;
      gapsize = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-i")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing HRA file: %s\n",argv[ii]); exit(0); }
      hrafile = argv[ii];
      layout=true;
    } else if  ( strcmp(argv[ii],"-j")==0 ) {
      ii++;
      n_threads = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-X")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing reads dump input file: %s\n",argv[ii]); exit(0); }
      reads_dump_in = argv[ii];
      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-M")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing model file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"model_file: %s\n",argv[ii]);
      model_files.push_back(std::string(argv[ii]));
      //      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-o")==0 ) {
      ii++;
      output_file = argv[ii];
      //      fprintf(stdout,"N: %d %d\n",ii,std::stoi(argv[ii]));
    } else if ( strcmp(argv[ii],"-O")==0 ) {
      ii++;
      reads_dump_out = argv[ii];
      dump_reads=true;
    } else if  ( strcmp(argv[ii],"-b")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing BAM file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"bamfile: %s\n",argv[ii]);
      bamfiles.push_back(std::string(argv[ii]));
    } else if  ( strcmp(argv[ii],"-m")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing masking BED file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"masking_bedfile: %s\n",argv[ii]);
      bedfile =argv[ii];
      read_bed=true;
    } else if  ( strcmp(argv[ii],"-s")==0 ) {
      ii++;
      if (!file_exists(argv[ii]) && strcmp(argv[ii], "fake") != 0) { fprintf(stdout,"Missing shotgun BAM file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"shotgun_bamfile: %s\n",argv[ii]);
      shotfile = argv[ii];
    } else if (strcmp(argv[ii], "-d") == 0) {
      useDepthPenalty = false;
    } else if (strcmp(argv[ii], "-D") == 0) {
      useDepthPenalty = true;
    } else if (strcmp(argv[ii], "--minscore") == 0) {
      ii++;
      minscore = std::stoi(argv[ii]);
    } else if (strcmp(argv[ii], "--hardminlen") == 0) {
      ii++;
      hard_min_len = std::stoi(argv[ii]);
    } else if (strcmp(argv[ii], "--softminlen") == 0) {
      ii++;
      soft_min_len = std::stoi(argv[ii]);
    } else if (strcmp(argv[ii], "--maxscore") == 0) {
      ii++;
      maxscore = std::stoi(argv[ii]);
    } else if (strcmp(argv[ii], "--stepsize") == 0) {
      ii++;
      stepsize = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-r")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing o&o requests file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"o&o_requests: %s\n",argv[ii]);
      oo_req_file =argv[ii];
    } else {
      fprintf(stdout,"unrecognized option: %s\n",argv[ii]);
      exit(0);
    }
    ii++;
  }
  use_broken_contig = true;

  if (argc<3) {
    fprintf(stdout,"usage: %s [-j <n threads>=16] -m <mask bedfile> -b <bamfile> [-b <bamfile> ... ] [-s <shotgunbam>] -M <modelfile> [-M <modelfile> ...] [-X <dump restore file>] -o <dump file> -i <hra file> -q <mapq>\n",argv[0]);
    // and -r for oo requests file
    exit(0);
  }

  //
  // Check agreement in # chicago files and # models (if only one model, combine chicago bamfiles)
  //
  int model_increment = 0;
  if (bamfiles.size() > 1 && model_files.size() > 1) {
    if (bamfiles.size() != model_files.size()) {
      fprintf(stdout, "Multiple bamfiles and model files cannot disagree in counts: %lu and %lu\n", bamfiles.size(), model_files.size());
      exit(0);
    }
    model_increment = 1;
  }
  std::vector<PairedReadLikelihoodModel *> models;
  for (uint i = 0; i < model_files.size(); i++) {
    std::string model_json;
    std::ifstream model_stream ;
    model_stream.open(model_files[i]);
    //  model_file >> model_json;
    std::getline(model_stream, model_json);
    LikelihoodModelParser model_parser;
    PairedReadLikelihoodModel *model = model_parser.ParseModelJson(model_json.c_str());
    models.push_back(model);
  }
  // feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);

  /*
  if (argc<3) {
    fprintf(stdout, "Expected at least 3 args: bed file, hra file, and one or more bamfiles.  Found %d\n", argc-1);
    exit(0);
    }*/

  ChicagoLinks links;

  auto chrono_start = std::chrono::high_resolution_clock::now();
  links.min_mapq() = mapq;
  std::cout << restore_dump << std::endl;
  if (restore_dump) { 
    std::cout << "restoring reads " << std::endl;
    links.PopulateFromBam( bamfiles[0].c_str(), false, models[0], 0 ,reads_dump_in);
    links.SetModelsVector(models);
  }
  else {
    // If only one model, use for all chicago bamfiles (model_increment == 0)
    // otherwise chicago & models match up one by one (model_increment == 1)
    for (uint i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      std::cout << "Load data from:" <<  bamfiles[i] << "\twith model:\t" << model_files[model_i] << "as bamfile? "<< looks_like_bamfile(bamfiles[i]) << "\n";
    }
    for (uint i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      if (looks_like_bamfile(bamfiles[i])) {
	fprintf(stdout,"looks like bamfile: %s\n",bamfiles[i].c_str());
	links.PopulateFromBam(   bamfiles[i] , false, models[model_i], model_i );
      } else {
	fprintf(stdout,"looks like tablefile: %s\n",bamfiles[i].c_str());
	links.PopulateFromTable( bamfiles[i] , false, models[model_i], model_i );	
      }	
    }
    //    links.SortByContigs();
    std::cout << "\n";
  }


  log_progress_info(std::cout, "Time: Done loading read data", chrono_start);

  links.summary(std::cout);

  if (dump_reads) {
    std::cout << "Dumping reads\n";
    links.write_reads(reads_dump_out);
    exit(0);
  }

  PathSet scaffolds(links.GetHeader());
  //  OO_Requests oo_reqs;

  scaffolds.ParseFromHRA(links.GetHeader(), hrafile );

  log_progress_info(std::cout, "Time: Done loading scaffolding", chrono_start);

  //std::cout << "##\n";
  //std::cout << scaffolds;


  if (read_bed) {
    scaffolds.ReadMaskedSegments(bedfile,links.GetHeader());
    log_progress_info(std::cout, "Time: Done loading masked segments", chrono_start);
  }

  const SegmentSet masking = scaffolds.MaskedSegments() ;
  links.SetMaskedFlags( masking );
  log_progress_info(std::cout, "Time: Done setting masking flags", chrono_start);
  
  // "fake" ugliness affects this file only. Can call utility method in a cleaner way later, if needed.
  if (!shotfile.empty()) {
    if (shotfile == "fake") {
      scaffolds.FakeContigSegShotgun();
    }
    else {
      scaffolds.CountContigSegShotgun(shotfile);
    }
    fprintf(stdout, "Switching to MetaHirise\n");
    for (uint i = 0; i < models.size(); i++) {
      //ChicagoExpModel* cmodel = dynamic_cast<ChicagoExpModel*>(models[i]);
      models[i]->SetMetagenomic(scaffolds.ShotgunCounted(), useDepthPenalty);
    }
  }

  //  if (!oo_req_file.empty()) {
  //  Load_OO_Reqs(oo_reqs, oo_req_file, true);
  //}
    

  //  if (use_broken_contig) {
    fprintf(stderr,"Switching to broken contig coordinates...\n");
    links.SwitchToBrokenContigCoordinates( scaffolds );
    fprintf(stderr,"Switching mask to broken contig coordinates...\n");
    //scaffolds.SwitchMaskToBrokenContigCoordinates();
  log_progress_info(std::cout, "Time: Done switching to broken scaffold coordinates", chrono_start);
    //} else {
    // fprintf(stderr,"Switching to scaffold coordinates...\n");
    //links.SwitchToScaffoldCoordinates( scaffolds );
    //}
  log_progress_info(std::cout, "Time: Done indexing the masked segemnts in broken contig coordinates", chrono_start);

  fprintf(stderr,"Indexing links table...\n");
  fprintf(stdout,"Indexing links table...\n");

  auto links_index_contigs   =  IndexContigs( links ) ;
  log_progress_info(std::cout, "Time: Done building index of links table by contig pair", chrono_start);
  auto links_index_scaffolds =  IndexScaffolds( links , scaffolds) ;
  log_progress_info(std::cout, "Time: Done building index of links table by scaffold pair", chrono_start);

  links.InitContigIndex( links_index_contigs, links_index_scaffolds  );
  log_progress_info(std::cout, "Time: Done indexing the links table by contigs and scaffolds", chrono_start);
  fprintf(stderr,"Indexing masked segments...\n");
  fprintf(stdout,"Indexing masked segments...\n");
  scaffolds.IndexMaskedScaffoldSegments(true);



  //Start of code that could be put in a loop to interate joining for a fixed number of cycles, or until no joins are being made.

  int start_score=maxscore;
  int step_size=stepsize;
  int score = start_score;

  std::vector<ScaffoldMergeMove> local_moves;
  //  std::string layout_filename_base = "layout_";
  std::map<int,int> scaffold_mod_round;
  int global_round = -1;
  while (score >= minscore) {
    std::cout << "#score " << score << " start " << start_score << " end " << minscore <<  " hard cutoff " << hard_min_len << " soft cutoff " << soft_min_len << std::endl;
    for (int round =0; round<max_rounds; round++) {
      global_round++;

      std::cout << "#round " << round << " global_round " << global_round << "\n"; // end " << minscore << endl;
  //  int n_threads=std::stoi(argv[1]);
  std::vector<IntercScoreWorkerThread *> threads  ;
  HRTaskQueue<HRScaffoldIntercScoreTask> queue;
  //  JoinScoreWorkerThread
  uint i;
  int bsize = 512;


  char* buffer=0;
  buffer = new char[bsize];
  for(i=0;i<n_threads;i++) {
    sprintf(buffer,(output_file + "_%d_%d.txt").c_str(),i, global_round);
    threads.push_back( new IntercScoreWorkerThread(queue,i, std::string(buffer)) );
    threads[i]->start();
    fprintf(stdout,"thread started: %d\t%lx\n",i,threads[i]->id()); fflush(stdout);
  }

  log_progress_info(std::cout, "Time: Started worker threads", chrono_start);

  typedef std::unordered_map <std::pair<int,int>, std::pair<unsigned long long,unsigned long long>, dtg::hash_pair >::iterator  scaffold_index_it_t ;
  bool s1_touched;
  bool s2_touched;
  int n_queued=0;
  for (scaffold_index_it_t it = links_index_scaffolds.begin(); it != links_index_scaffolds.end(); it++) {
    int s1 = it->first.first;
    int s2 = it->first.second;
    unsigned long long link_mini = 0;
    unsigned long long link_maxi = 0;
    unsigned long long scaffold_num_links = 0;
    if (s1!=s2) {

      if ((global_round==0) || ( (scaffold_mod_round.find(s1) != scaffold_mod_round.end()) && ( scaffold_mod_round[s1]>=global_round-1 ) )) { s1_touched=true;} else {s1_touched=false;}
      if ((global_round==0) || ( (scaffold_mod_round.find(s2) != scaffold_mod_round.end()) && ( scaffold_mod_round[s2]>=global_round-1 ) )) { s2_touched=true;} else {s2_touched=false;}

      if (s1_touched || s2_touched) {
	auto length1 = scaffolds.GetScaffold(s1).SeqLength();
	auto length2 = scaffolds.GetScaffold(s2).SeqLength();
	if ( (length1 > hard_min_len)&&( length2 >hard_min_len ) ) {
	  if ( (length1 > soft_min_len) || ( length2 > soft_min_len ) ) {
	    links.GetScaffoldPairRowRange(s1, s2, &link_mini, &link_maxi);
	    scaffold_num_links = link_maxi - link_mini;
	    if(scaffold_num_links >= min_links) {
	      queue.enqueue( HRScaffoldIntercScoreTask(links,scaffolds,s1,s2,gapsize) );      
	      n_queued+=1;
	    }
	  }
	}
      }
    }
  }

  queue.close();

  std::sort(local_moves.begin(),local_moves.end());

  std::cout << "Queued " << n_queued << " scaffold pairs.  Score: " << score<< "\n"; 
  log_progress_info(std::cout, "Time: Queued scaffold pairs with at least " + std::to_string(min_links) + " link", chrono_start);


  for(i=0;i<n_threads;i++) {
    //fprintf(stdout,"thread to join: %d\n",i); fflush(stdout);
    threads[i]->join();
    
    //std::cout << "thread had " << threads[i]->moves_.size() << " moves.\n"; 
  }
  log_progress_info(std::cout, "Time: Worker threads joined", chrono_start);

  //merge sort of moves.
  bool done=false;
  bool success=false;
  int nmoves=0;

  std::vector<int> move_ptr;
  move_ptr.resize(n_threads+1);
  for(i=0;i<n_threads;i++) {
    move_ptr[i]=threads[i]->moves_.size()-1 ; 
    if (threads[i]->moves_.size()==0) {
      std::cout  << "#thread move buffer size: "<< i << "\t" << threads[i]->moves_.size() << "\n"; // << threads[i]->moves_[0].score << "\t" << threads[i]->moves_.back().score  << "\n"; 
    } else {
      std::cout  << "#thread move buffer size: "<< i << "\t" << threads[i]->moves_.size() << "\t" << threads[i]->moves_[0].score << "\t" << threads[i]->moves_.back().score  << "\n"; 
    }
  }
  move_ptr[n_threads] = local_moves.size()-1;
  if (local_moves.size()>0){
    std::cout  << "#local move buffer size: " << local_moves.size() << "\tscores rangeL"<< local_moves[0].score << " - "<< local_moves.back().score <<"\n"; 
  } else {
    std::cout  << "#local move buffer size: 0\n";// << local_moves.size() << "\tscores rangeL"<< local_moves[0].score "\n"; 
  }
  int n_tried_and_failed=0;
  ScaffoldMergeMove move;
  while (!done) {
    done=true;
    int besti=-1;
    double bestscore=-1.0e6;

    //check the move at the top of the list for each thread:
    for(i=0;i<n_threads;i++) {
      if (move_ptr[i]>0) {
	done=false;
	if ((besti==-1)||( threads[i]->moves_[move_ptr[i]].score > bestscore )) {
	  besti=i;
	  move    = threads[i]->moves_[move_ptr[i]];
	  bestscore=move.score;
	  //std::cout << "so far: besti:" <<besti << "\tmove_ptr[i]:" << move_ptr[i] << "\t" << move.score  << "\n";
	}
      }
    }
    //and check the local pile of moves:
    i=n_threads;
    if (move_ptr[i]>0) {
      done=false;
      if ((besti==-1)||( local_moves[move_ptr[i]].score > bestscore )) {
	besti=i;
	move    = local_moves[move_ptr[i]];
	bestscore=move.score;
      }
    }
    //    std::cout << "test move from thread " << besti << "\t" << bestscore << "\t" << move_ptr[besti] << "\n";

    //std::cout << "besti:" <<besti << "\tmove_ptr[n_threads]:" << move_ptr[n_threads] << "\t" << move.score  << "\n";
  
    if (besti>=0) {

      //std::cout << "testing move with score " << move.score << "\n";

      move_ptr[besti]-=1;
      if (move.score>score) {
	success = scaffolds.AcceptMove(move,scaffold_mod_round,global_round);
	if (success) {
	  nmoves +=1;
	} else {
	  n_tried_and_failed ++;
	}
      } else { // the rest of them have score < 20
	done=true;
      }
    }
  }

  // std::cout << "Round " << round << " tried and failed:" << n_tried_and_failed << "\n";
  for(i=0;i<local_moves.size();i++) {
    move = local_moves[i];
    if (move_invalidated(global_round,scaffold_mod_round,move,scaffolds)) { local_moves[i] = local_moves.back(); local_moves.resize( local_moves.size()-1 ); } // throw out invalidated moves;
  }
  std::cout << "Round " << round << " local move buffer size: " << local_moves.size() << "\n";
  for(i=0;i<n_threads;i++) {
    //copy re-usable moves from thread pool to local pool.
    std::cout << "Test moves for moving to local: " << i << "\t" << threads[i]->moves_.size() << "\n";
    for(uint j=0;j<threads[i]->moves_.size();j++) {
      move = threads[i]->moves_[j];
      //      std::cout << "Test moves for moving to local: " << i << "\t" << threads[i]->moves_.size() << "\n";
      if (!move_invalidated(global_round+1,scaffold_mod_round,move,scaffolds)) { 
	local_moves.push_back(move);
      }	
    }
    threads[i]->moves_.resize(0);
  }
  std::cout  << "#score "<<score << " Round " << round << " local move buffer size: " << local_moves.size() << "\n";
  std::cout << "#score "<<score<<" round "<< round << ": Accepted " << nmoves <<"\t"; //elapsed: " << microseconds << " microseconds\n";
  log_progress_info(std::cout, "Time: All moves processed", chrono_start);

  //  auto elapsed = std::chrono::high_resolution_clock::now() - chrono_start;
  //long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  //  fprintf(stdout, "Took %lld microseconds for bam, bed and hra loading\n", microseconds);
  //  std::cout.flush();


  std::ofstream layout_out( output_file + "_layout_" + std::to_string(global_round) + ".out", std::ios::out );
  layout_out << scaffolds;
  layout_out.close();

  auto n50= scaffolds.N50() ;  
  std::cout << "L/N50: " <<  n50.first << "\t" << n50.second << "\n";

  if (nmoves==0) {break;}

  //update the indices
  log_progress_info(std::cout, "Time: About to sort by scaffolds", chrono_start);
  links.SortByScaffolds(scaffolds);
  log_progress_info(std::cout, "Time: Done sorting by scaffolds", chrono_start);
  update_contig_index( links, links_index_contigs ) ;
  log_progress_info(std::cout, "Time: Done re-building index of links table by contig pair", chrono_start);
  links_index_scaffolds =  IndexScaffolds( links , scaffolds) ;
  log_progress_info(std::cout, "Time: Done re-building index of links table by scaffold pair", chrono_start);
  links.InitContigIndex( links_index_contigs, links_index_scaffolds );
    }
    score -= step_size;

  }
  log_progress_info(std::cout, "Time: Done indexing links by contigs", chrono_start);
  links.PrintModelInfo();

  std::cout << "##\n";
  std::cout << scaffolds;
  //  auto elapsed = std::chrono::high_resolution_clock::now() - chrono_start;
  // long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  //fprintf(stdout, "Took %lld microseconds\n", microseconds);

  exit(0);

}
