#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include <unistd.h>
#include "model_parser.h"
#include "paired_read_likelihood_model.h"

int main(int argc, char * argv[]) {

  std::string model_json;
  std::ifstream model_stream ;
  int c;
  int seed= int(time(NULL)) ;
  int num=100;
  while ((c = getopt(argc, argv, "M:s:N:")) >= 0) {
    switch (c) {
    case 's':  seed = atoi(optarg); break;
    case 'N':  num = atoi(optarg); break;
    case 'M':  model_stream.open(optarg); break;
    }
  }

  srand48(seed);
  drand48();

  //  std::cout << seed << "\n";

  std::getline(model_stream, model_json);
  LikelihoodModelParser model_parser;
  PairedReadLikelihoodModel *model = model_parser.ParseModelJson(model_json.c_str(),false);

  int i;
  for (i=0;i<num;i++) {
    std::cout << model->sample_distribution() << "\n";
  }
}
