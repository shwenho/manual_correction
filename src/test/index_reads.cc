#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "link_range_iterator.h"
#include "hirise_threads.h"
#include "contig_index.h"
#include <fenv.h>

//void inline MapCheck(Base b, Base b2, Base b3, const PathSet& scaffolds) {
void inline MapCheck(Base b, const PathSet& scaffolds) {
  Base b2;
  Base b3;
  bool flip;
  scaffolds.ScaffoldCoord(b,&b2,&flip);
  scaffolds.ContigCoord(b2,&b3,&flip);
  assert(b==b3);  
  fprintf(stdout,"(%d,%d) -> (%d,%d)\n",b.target_id,b.x,b2.target_id,b2.x);
  fprintf(stdout,"\t-> (%d,%d)\n",b3.target_id,b3.x);
}

inline bool looks_like_bamfile(std::string const &filename) {
  if ( filename.length() >= 5 ) {
    return (0== filename.compare( filename.length() - 4, 4, ".bam" ) );
  } 
  return false;
}

inline bool file_exists (const std::string& name) {
  ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }   
}

int main(int argc, char * argv[]) {
  
  bool layout=false;
  bool dump_reads=false;
  bool restore_dump=false;
  bool read_bed=false;
  int pad=0;
  int minsep=0;
  int mapq = 0;
  int n_threads=16;
  std::string hrafile;
  std::vector<std::string> model_files;
  std::string reads_dump_out;
  std::string output_file;
  std::string reads_dump_in ;
  std::string bedfile ;
  std::vector<std::string> bamfiles;

  int ii=1;
  while (ii<argc) {
    if ( strcmp(argv[ii],"-q")==0 ) {
      ii++;
      mapq = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-i")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing HRA file: %s\n",argv[ii]); exit(0); }
      hrafile = argv[ii];
      layout=true;
    } else if  ( strcmp(argv[ii],"-j")==0 ) {
      ii++;
      n_threads = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-X")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing reads dump input file: %s\n",argv[ii]); exit(0); }
      reads_dump_in = argv[ii];
      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-M")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing model file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"model_file: %s\n",argv[ii]);
      model_files.push_back(std::string(argv[ii]));
      //      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-o")==0 ) {
      ii++;
      output_file = argv[ii];
    } else if ( strcmp(argv[ii],"-O")==0 ) {
      ii++;
      reads_dump_out = argv[ii];
      dump_reads=true;
    } else if  ( strcmp(argv[ii],"-b")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing BAM file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"bamfile: %s\n",argv[ii]);
      bamfiles.push_back(std::string(argv[ii]));
    } else if  ( strcmp(argv[ii],"-m")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing masking BED file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"masking_bedfile: %s\n",argv[ii]);
      bedfile =argv[ii];
      read_bed=true;
    } else {
      fprintf(stdout,"unrecognized option: %s\n",argv[ii]);
      exit(0);
    }
    ii++;
  }

  if (argc<3) {
    fprintf(stdout,"usage: %s [-j <n threads>=16] -m <mask bedfile> -b <bamfile> [-b <bamfile> ... ] -M <modelfile> [-M <modelfile> ...] [-X <dump restore file>] -o <dump file> -i <hra file> -q <mapq>\n",argv[0]);
    // and -r for oo requests file
    exit(0);
  }

  //
  // Check agreement in # chicago files and # models (if only one model, combine chicago bamfiles)
  //
  int model_increment = 0;
  if (bamfiles.size() > 1 && model_files.size() > 1) {
    if (bamfiles.size() != model_files.size()) {
      fprintf(stdout, "Multiple bamfiles and model files cannot disagree in counts: %lu and %lu\n", bamfiles.size(), model_files.size());
      exit(0);
    }
    model_increment = 1;
  }

  std::vector<PairedReadLikelihoodModel *> models;
  for (int i = 0; i < model_files.size(); i++) {
    std::string model_json;
    std::ifstream model_stream ;
    model_stream.open(model_files[i]);
    //  model_file >> model_json;
    std::getline(model_stream, model_json);
    LikelihoodModelParser model_parser;
    PairedReadLikelihoodModel *model = model_parser.ParseModelJson(model_json.c_str());
    models.push_back(model);
  }

  ChicagoLinks links;
  PathSet scaffolds;

  auto chrono_start = std::chrono::high_resolution_clock::now();
  links.min_mapq() = mapq;

  if (restore_dump) { 
    links.PopulateFromBam( bamfiles[0].c_str(), false, models[0], 0 ,reads_dump_in);
    links.SetModelsVector(models);
  }
  else {
    // If only one model, use for all chicago bamfiles (model_increment == 0)
    // otherwise chicago & models match up one by one (model_increment == 1)
    for (int i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      std::cout << "Load data from:" <<  bamfiles[i] << "\twith model:\t" << model_files[model_i] << "as bamfile? "<< looks_like_bamfile(bamfiles[i]) << "\n";
    }
    for (int i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      if (looks_like_bamfile(bamfiles[i])) {
	fprintf(stdout,"looks like bamfile: %s\n",bamfiles[i].c_str());
	links.PopulateFromBam(   bamfiles[i] , false, models[model_i], model_i );
      } else {
	fprintf(stdout,"looks like tablefile: %s\n",bamfiles[i].c_str());
	links.PopulateFromTable( bamfiles[i] , false, models[model_i], model_i );	
      }	
    }
    links.SortByContigs();
  }

  scaffolds.ParseFromHRA(links.GetHeader(), hrafile );
  if (read_bed) {
    scaffolds.ReadMaskedSegments(bedfile,links.GetHeader());
  }
  const SegmentSet masking = scaffolds.MaskedSegments() ;
  links.SetMaskedFlags( masking );
  
  if (dump_reads) {
    std::cout << "Dumping reads\n";
    links.write_reads(reads_dump_out);
  }

  fprintf(stderr,"Switching to scaffold coordinates...\n");
  links.SwitchToScaffoldCoordinates( scaffolds );
  fprintf(stderr,"Indexing masked segments...\n");
  scaffolds.IndexMaskedScaffoldSegments(links.GetHeader());

  int i;
  int bsize = 512;
  int min_links = 1;
  InterScaffoldLinksIterator sc_it(links);
  std::map <std::pair<int,int>, std::pair<int,int> > ContigIndex = IndexContigs(sc_it);
  /*  InterScaffoldLinksIterator sc_it(links);
  std::map <std::pair<int,int>, std::pair<int,int> > ContigIndex;
  std::pair<int,int> key;
  std::pair<int,int> value;
  while(!sc_it.done()) {
    key = std::make_pair((*sc_it).id1,(*sc_it).id2);
    value = std::make_pair((*sc_it).mini, (*sc_it).maxi);
    ContigIndex.insert(std::make_pair(key, value));

    ++sc_it;
    }*/
  
  
  for(auto it = ContigIndex.cbegin(); it != ContigIndex.cend(); ++it)
    {
      std::cout << "INDEX " << it->first.first << "\n";
    }
  links.PrintModelInfo();

  auto elapsed = std::chrono::high_resolution_clock::now() - chrono_start;
  long long seconds = std::chrono::duration_cast<std::chrono::seconds>(elapsed).count();
  fprintf(stdout, "Took %lld seconds for bam, bed and hra loading\n", seconds);

  exit(0);

}
