#include <iostream>
#include <sstream>
#include "peak_caller.h"

int main() {
    std::vector<int> values = {
    0, 6, 25, 20, 25, 8, 15, 6, 0, 6, 0, -5, -15, -3, 4, 10, 8, 13, 8, 10, 3,
    1, 20, 7, 3, 0 };

    std::vector<int> peaks;
    detect_peaks<int>(values, peaks, 7, 2);

    std::stringstream buf;
    buf << "Peak indices are: ";
    std::vector<int>::const_iterator it;
    for (it=peaks.begin(); it!=peaks.end(); ++it)
        buf << *it << " ";
    std::cerr << buf.str() << std::endl;
}
