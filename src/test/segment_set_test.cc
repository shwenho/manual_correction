#include <string>
#include <vector>
#include <iostream>

#include "picojson.h"
#include "segment_set.h"
#include "gtest/gtest.h"


TEST(SegmentSetTest, ArrayTypeTest) {
  
  SegmentSet masked_regions;

 
  masked_regions.AddSegment( MaskedSegment(0,100,200)  );
  masked_regions.AddSegment( MaskedSegment(0,150,250) );
  masked_regions.AddSegment( MaskedSegment(1,1510000,1511000) );
  masked_regions.ComputeCoverage();

  //  masked_regions.dumpCoverage();

  assert(  masked_regions.isCovered( Base(0,175)) );
  assert(  masked_regions.isCovered( Base(0,100)) );
  assert( !masked_regions.isCovered( Base(0,10) ) );
  assert( !masked_regions.isCovered( Base(0,250) ) );
  assert( !masked_regions.isCovered( Base(0,251) ) );
  assert( !masked_regions.isCovered( Base(1,251) ) );
  assert(  masked_regions.isCovered( Base(1,1510000) ) );
  assert( !masked_regions.isCovered( Base(1,2511000) ) );
  assert( !masked_regions.isCovered( Base(1,1511000) ) );

 }
