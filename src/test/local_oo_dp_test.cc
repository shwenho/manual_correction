#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include "hr_perm_utils.h"
#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "link_range_iterator.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "hirise_threads.h"
//#include "oo_requests.h"
#include "contig_index.h"
#include <fenv.h>
#include "pair_hash.h"

//#define MIN_DOUBLE -9.9e99;
#define MIN_DOUBLE -200;


void log_progress_info( std::ostream& out,const std::string& message,const std::chrono::time_point<std::chrono::high_resolution_clock>& chrono_start) {
  std::chrono::seconds s = std::chrono::duration_cast<std::chrono::seconds>(  std::chrono::high_resolution_clock::now() - chrono_start   );
  out << message ;
  out << "[ " <<   
    std::chrono::duration_cast<std::chrono::hours>(s).count() << " hours, "
      << std::chrono::duration_cast<std::chrono::minutes>(s % std::chrono::hours(1)).count() << " minutes, "
      << std::chrono::duration_cast<std::chrono::seconds>(s % std::chrono::minutes(1)).count() << " seconds]"
      << std::endl;
}

//void inline MapCheck(Base b, Base b2, Base b3, const PathSet& scaffolds) {
void inline MapCheck(Base b, const PathSet& scaffolds) {
  Base b2;
  Base b3;
  bool flip;
  scaffolds.ScaffoldCoord(b,&b2,&flip);
  scaffolds.ContigCoord(b2,&b3,&flip);
  assert(b==b3);  
  fprintf(stdout,"(%d,%d) -> (%d,%d)\n",b.target_id,b.x,b2.target_id,b2.x);
  fprintf(stdout,"\t-> (%d,%d)\n",b3.target_id,b3.x);
}

inline bool looks_like_bamfile(std::string const &filename) {
  if ( filename.length() >= 5 ) {
    return (0== filename.compare( filename.length() - 4, 4, ".bam" ) );
  } 
  return false;
}

inline bool file_exists (const std::string& name) {
  ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }   
}


//dummp
double metacontig_pair_score(int i, int j, DPmatrix matrix, int orientation_i, int orientation_j, int gapsize, const PathSet& scaffolds,
			  std::unordered_map<ContigPair, double, dtg::hash_cp>& llr_lookup) {
  //std::cout << "about to check " << i << " and " << j << "\nin " << matrix.original_scaffold.GetPathConst().size() << " gapsize " << gapsize << "\n";
  double llr=0;
  int size = matrix.original_scaffold.GetPathConst().size();
  const TargetRange contig_i = matrix.original_scaffold.GetPathConst()[i];
  const TargetRange contig_j = matrix.original_scaffold.GetPathConst()[j];
  //  std::cout << contig_i << "\nand contig " << contig_j << "\nstatei "  << i << " " << j <<  " oi " << orientation_i << " oj " << orientation_j << "\n";

  ContigPair cp(contig_i.id,contig_j.id,contig_i.len(),contig_j.len(),gapsize);
  if (orientation_i==1) { cp.set_strand_1(1);}
  if (orientation_j==1) { cp.set_strand_2(1);}
  cp.normalize();
  
  int count = llr_lookup.count(cp);
  
  if(count <= 0) {
    if(contig_j.len() < 0) {
      std::cerr << "wtf? " << contig_j.len() << "\n";
    }
    unsigned long long mini, maxi;
    matrix.read_links.GetContigPairRowRange(cp,&mini,&maxi);
    
    matrix.read_links.ContigPairLLRCorrected(&cp,scaffolds,mini,maxi,&llr,200000);
    //std::cout  << "llr: " << llr << "\n";
  }
  else {
    llr = llr_lookup[cp];
    //std::cout << "lookup cp worked for " << cp << " score: " << llr << "\n";
  }
  llr_lookup[cp] = llr;
  return llr;
}

double window_pair_score( int p, int state, int backlink_pos, int ancestor, DPmatrix matrix, const PathSet& scaffolds,
			  std::unordered_map<ContigPair, double, dtg::hash_cp>& llr_lookup)  {
  int w = matrix.w;
//
//       backlink_pos     p   backlink_pos+w         p+w 
//             |          |         |                 |  
//             |          |         |         +-------+
//             V          V         V         |
//                     +----+----+----+----+  V
//                     |    |    |  e |  f |
//                     +----+----+----+----+
//           +----+----+----+----+
//           |  a | b  | c  |  d |
//           +----+----+----+----+
//

//  return 0.0;
  
  double score = 0.0;

  // contig pairs within the new window, not already accoutned for.   (e,f) in the figure;
  for (int i=backlink_pos+w;i<p+w;i++) {
    int gapsize = 1000;
    for (int j=i+1;j<p+w;j++) {
      if ((i-p)>=w) {throw std::runtime_error("wtf1?\n");}
      if ((j-p)>=w) {throw std::runtime_error("wtf2?\n");}
      // matrix.contig_lookup just returns the index of the contig ... we'll need to know more: strand and coordinate.
      int size = matrix.original_scaffold.GetPathConst().size();
      if(i < 0 || j < 0 || i >= size || j >= size) {
	return score;
      }
      else{
	score += metacontig_pair_score( matrix.contig_lookup(p,state,j),  matrix.contig_lookup(p,state,i), matrix  ,  matrix.orientation_lookup(p, state, j), matrix.orientation_lookup(p, state, i), gapsize, scaffolds, llr_lookup) ;
	gapsize += matrix.original_scaffold.GetPathConst()[j].len() + gapsize;
      }
    }
  }

  // contig pairs between the first and second windows  a,e; a,f; b,e; b,f; c,e; c,f; d,e; d,f; in the figure;
  for (int i=backlink_pos;i<backlink_pos+w;i++) {
    int gapsize = 1000;
    for (int j=backlink_pos+w;j<p+w;j++) {
      int size = matrix.original_scaffold.GetPathConst().size();
      if(i < 0 || j < 0 || i >= size || j >= size) {
	return score;
      }
      else{

	score += metacontig_pair_score(  matrix.contig_lookup(backlink_pos,ancestor,i),  matrix.contig_lookup(p,state,j), matrix,matrix.orientation_lookup(p, state, j), matrix.orientation_lookup(backlink_pos, state, i),gapsize, scaffolds, llr_lookup  ) ;
	gapsize += matrix.original_scaffold.GetPathConst()[j].len() + gapsize;
      }
    }
  }
  //std::cout << "score: " << score << "\n";
  return score;

}


int main(int argc, char * argv[]) {

  int w=3;

  bool use_broken_contig=false;
  bool layout=false;
  bool dump_reads=false;
  bool restore_dump=false;
  bool read_bed=false;
  bool useDepthPenalty=false;
  int pad=0;
  int minsep=0;
  int mapq = 0;
  uint n_threads=16;
  int max_rounds = 10;
  int minscore = 20;
  int maxscore = 100;
  int gapsize = 1000;
  int hard_min_len = 1000;
  int soft_min_len = 1000;
  int min_links = 1;
  int stepsize = 10;
  std::string hrafile;
  std::vector<std::string> model_files;
  std::string reads_dump_out;
  std::string output_file;
  std::string reads_dump_in ;
  std::string bedfile ;
  std::vector<std::string> bamfiles;
  std::string shotfile = "";
  std::string oo_req_file = "";

  //  std::vector<std::string> ranges;
  //-p 100000 -m 500 -q 5 
  int ii=1;
  while (ii<argc) {
    if ( strcmp(argv[ii],"-R")==0 ) {
      ii++;
      max_rounds = std::stoi(argv[ii]);
    } else if ( strcmp(argv[ii],"-q")==0 ) {
      ii++;
      mapq = std::stoi(argv[ii]);
    } else if ( strcmp(argv[ii],"-l")==0 ) {
      ii++;
      min_links = std::stoi(argv[ii]);
    } else if ( strcmp(argv[ii],"-g")==0 ) {
      ii++;
      gapsize = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-i")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing HRA file: %s\n",argv[ii]); exit(0); }
      hrafile = argv[ii];
      layout=true;
    } else if  ( strcmp(argv[ii],"-j")==0 ) {
      ii++;
      n_threads = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-X")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing reads dump input file: %s\n",argv[ii]); exit(0); }
      reads_dump_in = argv[ii];
      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-M")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing model file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"model_file: %s\n",argv[ii]);
      model_files.push_back(std::string(argv[ii]));
      //      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-o")==0 ) {
      ii++;
      output_file = argv[ii];
      //      fprintf(stdout,"N: %d %d\n",ii,std::stoi(argv[ii]));
    } else if ( strcmp(argv[ii],"-O")==0 ) {
      ii++;
      reads_dump_out = argv[ii];
      dump_reads=true;
    } else if  ( strcmp(argv[ii],"-b")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing BAM file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"bamfile: %s\n",argv[ii]);
      bamfiles.push_back(std::string(argv[ii]));
    } else if  ( strcmp(argv[ii],"-m")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing masking BED file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"masking_bedfile: %s\n",argv[ii]);
      bedfile =argv[ii];
      read_bed=true;
    } else if  ( strcmp(argv[ii],"-s")==0 ) {
      ii++;
      if (!file_exists(argv[ii]) && strcmp(argv[ii], "fake") != 0) { fprintf(stdout,"Missing shotgun BAM file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"shotgun_bamfile: %s\n",argv[ii]);
      shotfile = argv[ii];
    } else if (strcmp(argv[ii], "-d") == 0) {
      useDepthPenalty = false;
    } else if (strcmp(argv[ii], "-D") == 0) {
      useDepthPenalty = true;
    } else if (strcmp(argv[ii], "--minscore") == 0) {
      ii++;
      minscore = std::stoi(argv[ii]);
    } else if (strcmp(argv[ii], "--hardminlen") == 0) {
      ii++;
      hard_min_len = std::stoi(argv[ii]);
    } else if (strcmp(argv[ii], "--softminlen") == 0) {
      ii++;
      soft_min_len = std::stoi(argv[ii]);
    } else if (strcmp(argv[ii], "--maxscore") == 0) {
      ii++;
      maxscore = std::stoi(argv[ii]);
    } else if (strcmp(argv[ii], "--stepsize") == 0) {
      ii++;
      stepsize = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-r")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing o&o requests file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"o&o_requests: %s\n",argv[ii]);
      oo_req_file =argv[ii];
    } else {
      fprintf(stdout,"unrecognized option: %s\n",argv[ii]);
      exit(0);
    }
    ii++;
  }
  use_broken_contig = true;

  if (argc<3) {
    fprintf(stdout,"usage: %s [-j <n threads>=16] -m <mask bedfile> -b <bamfile> [-b <bamfile> ... ] [-s <shotgunbam>] -M <modelfile> [-M <modelfile> ...] [-X <dump restore file>] -o <dump file> -i <hra file> -q <mapq>\n",argv[0]);
    // and -r for oo requests file
    exit(0);
  }

  //
  // Check agreement in # chicago files and # models (if only one model, combine chicago bamfiles)
  //
  int model_increment = 0;
  if (bamfiles.size() > 1 && model_files.size() > 1) {
    if (bamfiles.size() != model_files.size()) {
      fprintf(stdout, "Multiple bamfiles and model files cannot disagree in counts: %lu and %lu\n", bamfiles.size(), model_files.size());
      exit(0);
    }
    model_increment = 1;
  }
  std::vector<PairedReadLikelihoodModel *> models;
  for (uint i = 0; i < model_files.size(); i++) {
    std::string model_json;
    std::ifstream model_stream ;
    model_stream.open(model_files[i]);
    //  model_file >> model_json;
    std::getline(model_stream, model_json);
    LikelihoodModelParser model_parser;
    PairedReadLikelihoodModel *model = model_parser.ParseModelJson(model_json.c_str());
    models.push_back(model);
  }
  // feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);

  /*
  if (argc<3) {
    fprintf(stdout, "Expected at least 3 args: bed file, hra file, and one or more bamfiles.  Found %d\n", argc-1);
    exit(0);
    }*/

  ChicagoLinks links;

  auto chrono_start = std::chrono::high_resolution_clock::now();
  links.min_mapq() = mapq;
  std::cout << restore_dump << std::endl;
  if (restore_dump) { 
    std::cout << "restoring reads " << std::endl;
    links.PopulateFromBam( bamfiles[0].c_str(), false, models[0], 0 ,reads_dump_in);
    links.SetModelsVector(models);
  }
  else {
    // If only one model, use for all chicago bamfiles (model_increment == 0)
    // otherwise chicago & models match up one by one (model_increment == 1)
    for (uint i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      std::cout << "Load data from:" <<  bamfiles[i] << "\twith model:\t" << model_files[model_i] << "as bamfile? "<< looks_like_bamfile(bamfiles[i]) << "\n";
    }
    for (uint i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      if (looks_like_bamfile(bamfiles[i])) {
	fprintf(stdout,"looks like bamfile: %s\n",bamfiles[i].c_str());
	links.PopulateFromBam(   bamfiles[i] , false, models[model_i], model_i );
      } else {
	fprintf(stdout,"looks like tablefile: %s\n",bamfiles[i].c_str());
	links.PopulateFromTable( bamfiles[i] , false, models[model_i], model_i );	
      }	
    }
    //    links.SortByContigs();
    std::cout << "\n";
  }


  log_progress_info(std::cout, "Time: Done loading read data", chrono_start);

  links.summary(std::cout);

  if (dump_reads) {
    std::cout << "Dumping reads\n";
    links.write_reads(reads_dump_out);
    exit(0);
  }

  PathSet scaffolds(links.GetHeader());
  //  OO_Requests oo_reqs;

  scaffolds.ParseFromHRA(links.GetHeader(), hrafile );

  log_progress_info(std::cout, "Time: Done loading scaffolding", chrono_start);

  //std::cout << "##\n";
  //std::cout << scaffolds;


  if (read_bed) {
    scaffolds.ReadMaskedSegments(bedfile,links.GetHeader());
    log_progress_info(std::cout, "Time: Done loading masked segments", chrono_start);
  }

  const SegmentSet masking = scaffolds.MaskedSegments() ;
  links.SetMaskedFlags( masking );
  log_progress_info(std::cout, "Time: Done setting masking flags", chrono_start);
  
  // "fake" ugliness affects this file only. Can call utility method in a cleaner way later, if needed.
  if (!shotfile.empty()) {
    if (shotfile == "fake") {
      scaffolds.FakeContigSegShotgun();
    }
    else {
      scaffolds.CountContigSegShotgun(shotfile);
    }
    fprintf(stdout, "Switching to MetaHirise\n");
    for (uint i = 0; i < models.size(); i++) {
      //ChicagoExpModel* cmodel = dynamic_cast<ChicagoExpModel*>(models[i]);
      models[i]->SetMetagenomic(scaffolds.ShotgunCounted(), useDepthPenalty);
    }
  }

  //  if (!oo_req_file.empty()) {
  //  Load_OO_Reqs(oo_reqs, oo_req_file, true);
  //}
    

  //  if (use_broken_contig) {
    fprintf(stderr,"Switching to broken contig coordinates...\n");
    links.SwitchToBrokenContigCoordinates( scaffolds );
    fprintf(stderr,"Switching mask to broken contig coordinates...\n");
    //scaffolds.SwitchMaskToBrokenContigCoordinates();
  log_progress_info(std::cout, "Time: Done switching to broken scaffold coordinates", chrono_start);
    //} else {
    // fprintf(stderr,"Switching to scaffold coordinates...\n");
    //links.SwitchToScaffoldCoordinates( scaffolds );
    //}
  log_progress_info(std::cout, "Time: Done indexing the masked segemnts in broken contig coordinates", chrono_start);

  fprintf(stderr,"Indexing links table...\n");
  fprintf(stdout,"Indexing links table...\n");

  auto links_index_contigs   =  IndexContigs( links ) ;
  log_progress_info(std::cout, "Time: Done building index of links table by contig pair", chrono_start);
  auto links_index_scaffolds =  IndexScaffolds( links , scaffolds) ;
  log_progress_info(std::cout, "Time: Done building index of links table by scaffold pair", chrono_start);

  links.InitContigIndex( links_index_contigs, links_index_scaffolds  );
  log_progress_info(std::cout, "Time: Done indexing the links table by contigs and scaffolds", chrono_start);
  fprintf(stderr,"Indexing masked segments...\n");
  fprintf(stdout,"Indexing masked segments...\n");
  scaffolds.IndexMaskedScaffoldSegments(true);



  // LOCAL OO PART

  LocalOODPLinks lool(w);
  for(auto const &iter : scaffolds) {
    auto const scaffold = iter.second;
    DPmatrix matrix(lool,scaffold.size()-1, links, scaffold);

  std::unordered_map <ContigPair, double, dtg::hash_cp > llr_lookup;
  for(int p = -w+1; p< matrix.npositions;p++) {  /// iterate through the contigs of the scaffold.
    std::cout << "position: " << p << " / "<< matrix.npositions <<"\n";

    double bestest_score = MIN_DOUBLE;
    for(uint state=0; state<lool.nstates;state++) { // iterate through all the possible states for this position.
      if (matrix.is_forbidden(p,state)) continue;
      
      std::cout << "state: " << state << "\t" << lool.states[state] << " ";
      
      double best_score = MIN_DOUBLE;
      Backlink best_backlink;
      for( Backlink_iterator x=lool.ancestors(state); !(x.done()); ++x) {
	int backlink_pos = p - x->delta;
	if ((backlink_pos>=-w+2) &&   !matrix.is_forbidden(backlink_pos,x->ancestor) ) {
	  double score = matrix.get_score(backlink_pos,x->ancestor) + window_pair_score( p, state, backlink_pos, x->ancestor, matrix, scaffolds, llr_lookup);
	  if ( score > best_score ) {
	    best_score = score;
	    best_backlink = *x;
	  }
	}
      }
      matrix.set_score(p,state,best_score);
      std::cout  << best_score << "\n";
      matrix.set_score_path(p,state,best_backlink);
    }
    //      std::cout << "at p: " << p << " state " << best_state << " has best score " << bestest_score << "\n";    
  }
  for(int p = matrix.npositions;p>=-w+1;p--) {  /// iterate through the contigs of the scaffold.
    std::cout << "position: " << p << " / "<< matrix.npositions <<"\n";    
    double best_pos_score = -100;
    uint best_pos_state;
    Backlink best_pos_backlink;
    for(uint state=0; state<lool.nstates;state++) { // iterate through all the possible states for this position.
      if (matrix.is_forbidden(p,state)) continue;
      if (matrix.get_score(p, state) > best_pos_score) {
	best_pos_state = state;
	best_pos_backlink = matrix.get_score_path(p, state);
	best_pos_score = matrix.get_score(p, state);
      }
    }
    std::cout << "score: " << best_pos_score << " state: " << best_pos_state << " backlink " << best_pos_backlink << "\n";
  }
  }
}
