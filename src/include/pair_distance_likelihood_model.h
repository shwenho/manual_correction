#ifndef CHICAGO_EDGE_SCORES_H
#define CHICAGO_EDGE_SCORES_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
#include <Eigen/Dense>
#include <picojson.h>
#include <pthread.h>
#include "error_msg.h"
#include "chicago_read.h"
#include "paired_read_likelihood_model.h"

class PairDistanceLM : public PairedReadLikelihoodModel {
  friend class ChicagoLM ;
  friend class ChicagoSILM ;
  friend class GaussianLM ;
  friend class CustomPairLM ;
public:

  void SetModel(const char *model_json, const bool fill_cache = true) {} ;
  int sample_distribution() {return(0);} ;
  void NonNoiseInsertDist(int insert_size, double *likelihood) {} ;
  virtual void F0(int insert_size, double *result) =0 ;
  virtual void H0(int insert_size, double *result) =0 ;
  virtual void F_good(int insert_size, double *result) =0  ;
  virtual void H_good(int insert_size, double *result) =0 ;
  virtual void DescriptionString(char* sbuff) = 0;
  virtual void Init() = 0 ;

  pthread_mutex_t mutex_;
  pthread_mutex_t mutex_T_;
  pthread_mutex_t mutex_Tplus_;
  pthread_mutex_t mutex_F_;
  pthread_mutex_t mutex_A_;
  pthread_mutex_t mutex_P_;



  //PairDistanceLM() { }

  //  PairDistanceLM(const char *model_json) {
  //  Init();
  //  SetModel(model_json);
  //}



  // f in python
  inline void InsertSizeDist(int insert_size, double *likelihood) {
    //    ExpLikelihood(insert_size, likelihood);
    NonNoiseInsertDist(insert_size, likelihood);
    *likelihood *= one_minus_prob_noise_;
    *likelihood += prob_noise_div_genome_size_;
  }

  // f in python
  inline float InsertSizeDistF(int insert_size) {
    //    ExpLikelihood(insert_size, likelihood);
    double l;
    NonNoiseInsertDist(insert_size, &l);
    l *= one_minus_prob_noise_;
    l += prob_noise_div_genome_size_;
    return float(l);
  }

  // lnF in python
  inline void LogModelFunction(int d, double *log_likelihood) {

    //assert(d-cache_min_>=0);
    if (cache_min_ <= d && d < cache_max_) {
      if (Fcache_fill_[d-cache_min_]>0) {
	*log_likelihood = Fcache_[d-cache_min_];
      } else {
	pthread_mutex_lock(   &mutex_F_ );
	//	double ll;
	InsertSizeDist( d , &Fcache_[d-cache_min_] );
	Fcache_[d-cache_min_] = log(Fcache_[d-cache_min_]);
	Fcache_fill_[d-cache_min_]=1;
	*log_likelihood = Fcache_[d-cache_min_];
	pthread_mutex_lock(   &mutex_F_ );

      }
    } else {
	InsertSizeDist( d , log_likelihood );
	*log_likelihood = log(*log_likelihood);
    }

  }

  // n_bar0 in python prototype
  inline void MeanNumLinksNull(double length1_x_length2, double *likelihood) {
    *likelihood = length1_x_length2 * mean_num_links_null_coeff_;
  }

  double genome_size() {
    return double(genome_size_);
  }

  double noise_probability() {
    return double(prob_noise_);
  }

  const int32_t N() const { return num_links_; }

  inline void F(int insert_size, double *result) {
    F0(insert_size, result);
    double zero_result = 0.0d;
    F0(0, &zero_result);
    *result -= zero_result;
  }

  inline void H(int insert_size, double *result) {
    H0(insert_size, result);
    double zero_result = 0.0d;
    H0(0, &zero_result);
    *result -= zero_result;
  }

  inline void Fplus(int insert_size, double *result) {
    F_good(insert_size, result);
    //    *result *= -1.0;
  }

  inline void Hplus(int insert_size, double *result) {
    H_good(insert_size, result);
    //    *result *= -1.0;
  }


  inline void Tplus(int d, double *result) {
    
    double f_0=0.0;
    double h_0=0.0;
    //    int int_d = int(d);
    //    if (d<cache_min_) {
    //  fprintf(stdout,"T\td<0\t%g\t%d\n",d,int_d);  fflush(stdout);
    // }
    if(d < 0) {
      std::cerr << d << " should not be less than 0 " << "\n";
    }
    assert(d>=0);
    assert(d-cache_min_>=0);

    if (d>=cache_max_) { d=cache_max_-1; }

    //    if ( (TPluscache_fill_[d-cache_min_]>0) &&  abs(TPluscache_[d-cache_min_])>100.0) { std::cout << "Tplus large cache value found:\t" <<  d << "\t" << TPluscache_[d-cache_min_] <<"\t" << f_0 <<"\t" <<h_0 << "\n"; };


    if (cache_min_ <= d && d < cache_max_) {
      if (TPluscache_fill_[d-cache_min_]>0) {
	*result = TPluscache_[d-cache_min_];
      } else {
	pthread_mutex_lock(   &mutex_Tplus_ );
	Fplus(d, &f_0);
	Hplus(d, &h_0);
	TPluscache_[d-cache_min_] = h_0 - d*f_0  ;
	TPluscache_fill_[d-cache_min_]=1;
	*result = TPluscache_[d-cache_min_];
	//	if ( abs(*result)>100 ) { std::cout << "Tplus large value computed:\t" <<  d << "\t" << TPluscache_[d-cache_min_] <<"\t" << f_0 <<"\t" <<h_0 << "\n"; };
	pthread_mutex_unlock(   &mutex_Tplus_ );

      }
      //      fprintf(stdout,"TplusC\t%d\t%g\n",d,*result);  fflush(stdout);

    } else {
      *result = 0.0;
      return ;

    }
  }



  inline void T(double d, double *result) {

    double f_0;
    double h_0;
    int int_d = int(d);
    if (d<cache_min_) {
      fprintf(stdout,"T\td<0\t%g\t%d\n",d,int_d);  fflush(stdout);
    }
    assert(d-cache_min_>=0);
    if (cache_min_ <= int_d && int_d < cache_max_) {
      if (Tcache_fill_[d-cache_min_]>0) {
	*result = Tcache_[d-cache_min_];
      } else {
	pthread_mutex_lock(   &mutex_T_ );
	F(d, &f_0);
	H(d, &h_0);
	Tcache_[d-cache_min_] = d * f_0 - h_0;
	Tcache_fill_[d-cache_min_]=1;
	*result = Tcache_[d-cache_min_];
	pthread_mutex_unlock(   &mutex_T_ );

      }
      fprintf(stdout,"Tc\t%d\t%g\n",int_d,*result);  fflush(stdout);

    } else {
      F(d, &f_0);
      H(d, &h_0);
      *result = d * f_0 - h_0;
      fprintf(stdout,"Tm\t%g\t%g\t%g\t%g\n",d,*result,f_0,h_0);  fflush(stdout);

    }
  }

  inline void p(double insert_size_1, double insert_size_2, int gap, double *result) {
    double f_sum, f_gap, f_1_gap, f_2_gap;
    T(insert_size_1 + insert_size_2 + gap, &f_sum);
    T(gap, &f_gap);
    T(insert_size_1 + gap, &f_1_gap);
    T(insert_size_2 + gap, &f_2_gap);
    fprintf(stdout,"calc p: %g %g %g %g %g %g\n",insert_size_1,insert_size_2,f_sum,f_gap,f_1_gap,f_2_gap);
    *result = f_sum + f_gap - f_1_gap - f_2_gap;
    *result /= genome_size_;
  }

  void MeanNumLinks(int insert1, int insert2, int gap, double *result) {
    p(insert1, insert2, gap, result);
    *result *= num_links_;
  }

  void RescaleGenomesize(double GG, bool fill_cache=false) {
    //std::cerr << "old genome size: " << genome_size_ << "\n";

    //XXX
    double gss = GG / genome_size_;
    double new_num_links = gss * num_links_ * (1 + prob_noise_ * (gss - 1));
    double rho_n = 2.0 * num_links_ * prob_noise_ / genome_size_ / genome_size_;
    double new_nnoise_links = rho_n * 0.5 * GG * GG;
    double new_pnoise = new_nnoise_links / new_num_links;

    num_links_ = new_num_links;
    genome_size_ = GG;
    prob_noise_ = new_pnoise ;

    prob_noise_div_genome_size_ = prob_noise_ / genome_size_;
    one_minus_prob_noise_ = 1.0d - prob_noise_;
    mean_num_links_null_coeff_ = num_links_ * prob_noise_ / (genome_size_ * genome_size_);

    logG_ = log(genome_size_);
    logN_ = log(num_links_);
    lnFinf_ = log(prob_noise_) - logG_;
    //        self.lnFinf = math.log(self.pn)-math.log(self.G)
    if (fill_cache) {
      fill_caches();
    }
    //std::cerr << "new genome size: " << genome_size_ << "\n";
  }

  void set_N(double N) {    num_links_ = N;  };

  void n_bar(int l1,double *l) {
    *l=0.0;
    double f;
    double h;
    double f_0;
    double h_0;
    F_good(l1, &f);
    F_good(0 , &f_0);
    H_good(l1, &h);
    H_good(0 , &h_0);

#if 0
    std::cout << "debug1\t" << l1 <<"\t" << f <<"\t" << f_0 <<"\t" << h <<"\t" << h_0 <<"\n";
    std::cout << "debug2\t" <<  ((l1 * (f-f_0))) <<"\n";
    std::cout << "debug2\t" <<  (( (h-h_0) )) <<"\n";
    std::cout << "debug2\t" <<  ((l1 * (f-f_0) - (h-h_0) )) <<"\n";
    std::cout << "debug2\t" <<  num_links_*((l1 * (f-f_0) - (h-h_0) )/genome_size_) <<"\n";
    std::cout << "debug3\t" <<  num_links_*( 0.5*(l1/genome_size_)*l1*prob_noise_div_genome_size_) <<"\n";
#endif
    *l = num_links_*(-(l1 * (f-f_0) - (h-h_0) )/genome_size_  + 0.5*(l1/genome_size_)*l1*prob_noise_div_genome_size_ );
    //    *l = num_links_*( (t1-t2-t3+t4) + prob_noise_div_genome_size_ * l1*l2) / genome_size_ ;

  }

  void n_bar(int l1,int l2,int g,double *l) {
    //    double p_link = 0.0;
    //p(l1,l2,g,&p_link);
    double t1=0.0;
    double t2=0.0;
    double t3=0.0;
    double t4=0.0;

    Tplus(g,&t1);  Tplus(g+l1, & t2) ; Tplus(g+l2, &t3) ; Tplus(g+l1+l2 , &t4) ;
    *l = num_links_*( (t1-t2-t3+t4) + prob_noise_div_genome_size_ * l1*l2) / genome_size_ ;
#if 0
    if (abs(*l)>400) { 
      std::cout << "large n_bar found:\t" << *l <<"\t" << l1 <<"\t" << l2 <<"\t" << g <<"\t" << t1 <<"\t" << t2 <<"\t" << t3 <<"\t" << t4 << "\n";
    }
#endif
  }

  void LLRAreaContribution( ContigPair *cp, int n , double *result ) {

    double t1=0.0;
    double t2=0.0;
    double t3=0.0;
    double t4=0.0;
    int s1,s2;
    double normf=one_minus_prob_noise_N_over_G_;
    double original_result = *result;
    if (is_metagenomic_) {
      //std::cout << "metagenomic? " << is_metagenomic_ << "\n";
      s1 = cp->nshotgun_1; // + 0.1;
      s2 = cp->nshotgun_2; // + 0.1;
      // N -> N ((s1+s2)/(l1+l2)) (G/S_T)
      normf *= ((double(s1+s2))/(cp->length_1*cp->length_2))*genome_div_total_shotgun_ ;

      *result += mean_num_links_null_coeff_metag_ * s1*s2;  // N p_n s1s2/S_T^2
      *result -= mean_num_links_null_coeff_ * genome_div_total_shotgun_ * ((cp->length_1*cp->length_2)/(cp->length_1+cp->length_2))*(s1+s2) ; //  N p_n (G/S_T) (s1+s2)/(l1+l2) l1l2/G^2
      //*result += num_links_ * prob_noise_ * ( (s1*s2)/ ) ;  // N p_n ( s1s2/S_T^2  - ((s1+s2)/(s_t))*((l1l2)/(l1+l2))/G )
      //XXX
    }

    Tplus(cp->gap,&t1);  
    Tplus(cp->gap + cp->length_1, & t2) ; 
    Tplus(cp->gap + cp->length_2, &t3) ; 
    Tplus(cp->gap + cp->length_1+ cp->length_2 , &t4) ;
    //    std::cout << "LLRAC:\t" << is_metagenomic_<<"\t"<< normf <<"\t" << t1 << "\t" << t2<< "\t" << t3<< "\t" << t4 << "\t"<< *cp<<"\n";

    *result -= normf * ( t1 - t2 - t3 + t4  );
    if( *result > 0 && original_result < 0) {
      std::cout << "#debug: weird result? " << cp->target_1 << " " << cp->target_2 << " "<< *result << " and n is " << n << " " << t1 << " " << t2 << " " << t3 << " " << t4 << " " << normf << "\n";
    }


  }

  void LLRReadPairContribution(ContigPair *cp,Read read,double *result) {
    //pthread_mutex_lock(   &mutex_P_ );
    //    *result += logN_;
    //    *result -= logG_;
    *result -= lnFinf_;
    double d = 0.0;
    InsertSizeDist( cp->separation(read), &d );
    //fprintf(stdout,"llr:\t%d\t%d\t%d\t%g\t%g\t%g\t%g\n",read.pos_1,read.pos_2,cp->separation(read),*result,log(d),lnFinf_ ,log(d)-lnFinf_ );
    *result += log(d);
    //pthread_mutex_unlock(   &mutex_P_ );

  }


  void SetMetagenomic(int64_t n_shotgun, bool useDepthPenalty=false) {
    std::cout << "Flagging as metagenomic\n";
    is_metagenomic_ = true;
    use_depth_penalty_ = useDepthPenalty;
    genome_div_total_shotgun_ = genome_size_ * 1.0 / n_shotgun;
    mean_num_links_null_coeff_metag_ = num_links_ * prob_noise_ / (n_shotgun * n_shotgun);
  }

  void UnSetMetagenomic() {
    // Should be initial state, now that Init() has been fixed to initialize metagenomic fields.
    std::cout << "Unflagging as metagenomic\n";
    is_metagenomic_ = false;
  }

  inline bool NeedDepthDiscrep(ContigPair *cp) {
    return (is_metagenomic_ && use_depth_penalty_);
  }

  void LLRDepthDiscrep(ContigPair *cp, double *result) {
    if (!NeedDepthDiscrep(cp)) {
      return;
    }
    double l1 = cp->length_1;
    double l2 = cp->length_2;
    double s1 = 0.0, s2 = 0.0;
    // Add a pseudo-count to avoid FP exceptions on log & gamma functions,
    // with no more skew than necessary.
    if (is_metagenomic_) {
      //std::cout << "metagenomic? " << is_metagenomic_ << "\n";
      s1 = cp->nshotgun_1 + 0.1;
      s2 = cp->nshotgun_2 + 0.1;
    }
    //fprintf(stdout,"nbar0 %g\t", n_bar0);
    //fprintf(stdout,"final result0 %g\n", *result);
    //fflush(stdout);

    // For the Metagenomics case...
    // ALSO add in depth discrepancy penalty (to reduce odds of linking between organisms)
    // ln(
    //    Binom(s1 | s1 + s2, l1 / (l1 + l2))
    //    -------------------------------------------
    //    Poisson(s1|lambda=s1)*Poisson(s2|lambda=s2)
    //   )

    // Binom("k" | "n", "p") = (n choose k) * p^k * (1-p)^(n-k)
    // for "k" == s1, "n" == (s1+s2), "p" == l1/(l1+l2)
    // -> (s1+s2 choose s1) * (l1/(l1+l2))^s1 * (1 - l1/(l1+l2))^(s1+s2-s1)
    // =  ((s1+s2)! / (s1! * (s1+s2-s1)!)) * (l1(l1+l2))^s1 * (l2/(l1+l2))^s2
    // =  ((s1+s2)! / (s1! * s2!)) * (l1^s1 * l2^s2) / ((l1+l2)^(s1+s2))
    // Poissons
    // (s1^s1 * e^(-s1) / s1!) * (s2^s2 * e^(-s2) / s2!)
    // -> (s1^s1 * s2^s2) / (e^(s1+s2) * s1! * s2!)
    //
    // Binom/Poissons ->
    // [((s1+s2)! / (s1! * s2!)) * (l1^s1 * l2^s2) / ((l1+l2)^(s1+s2))]
    //     /
    // [(s1^s1 * s2^s2) / (e^(s1+s2) * s1! * s2!)]
    //
    // Cancel the s1!*s2! and collecting numerator & denominator terms ->
    // [(s1+s2)! * (l1^s1 * l2^s2) * e^(s1+s2)]
    //    /
    // [(l1+l2)^(s1+s2) * s1^s1 * s2^s2]
    //
    // Applying natural log ->
    // ln[(s1+s2)! * (l1^s1 * l2^s2) * e^(s1+s2)] - ln[(l1+l2)^(s1+s2) * s1^s1 * s2^s2]
    // ->
    // lngamma(s1+s2) + s1*ln(l1) + s2*ln(l2) + s1 + s2 - (s1+s2)ln(l1+l2) - s1*ln(s1) - s2*ln(s2)
    double log_l1 = log(l1);
    double log_l2 = log(l2);
    // Positive values (of the above formula) correspond to stronger associations,
    // so subtract it from *result below,
    // so that the LLR score will be correspondingly increased when return value is subtracted.
    double depth_discrep = (lgamma(s1+s2)
			    + s1*log_l1
			    + s2*log_l2
			    + s1 + s2
			    - (s1+s2)*(log_l1 + log_l2)
			    - s1*log(s1)
			    - s2*log(s2)
			    );
#if score_debug_level > 1
    if (fabs(depth_discrep) > fabs(*result)) {
      std::cerr << "Area_3: " << l1 << "," << l2 << "," << s1 << "," << s2 << "\t" << *result << "\t" << depth_discrep << "\t" << *cp << "\n";
    }
#endif
    *result -= depth_discrep;
  }


 private:


  void fill_caches() {
    int i;
    double f_0;
    double h_0;
    fprintf(stderr,"start cache fill, %d %d\n",cache_min_,cache_max_); fflush(stderr);
    for (i=0;i<cache_max_;i++) {
      TPluscache_fill_[i-cache_min_]=1;
      F_good(i, &f_0);
      H_good(i, &h_0);
      TPluscache_[i-cache_min_] = h_0 - i*f_0;
      //      fprintf(stdout,"Tplus cache\t%d\t%g\t%g\t%g\t%g\n",i,one_minus_prob_noise_N_over_G_ * TPluscache_[i-cache_min_],TPluscache_[i-cache_min_],h_0,f_0);
    }
    for (i=0;i<cache_max_;i++) {
      Fcache_fill_[i-cache_min_]=1;
      InsertSizeDist( i , &Fcache_[i-cache_min_] );
      Fcache_[i-cache_min_] = log(Fcache_[i-cache_min_]);
    }
    for (i=cache_min_;i<cache_max_;i++) {
      Tcache_fill_[i-cache_min_]=1;
      F(i, &f_0);
      H(i, &h_0);
      Tcache_[i-cache_min_] = i * f_0 - h_0;
    }
    fprintf(stderr,"done cache fill\n"); fflush(stderr);
  }


  // scalars
  bool is_metagenomic_ ;
  bool use_depth_penalty_ ;
  double genome_div_total_shotgun_;
  double mean_num_links_null_coeff_metag_ ;

  int32_t num_links_;       ///< N in python code
  int64_t genome_size_;     ///< size of genome in base pairs
  double  prob_noise_;      ///< probability of noise links
  double logG_;
  double logN_;
  double lnFinf_;
  // Cached precaculated values
  double prob_noise_div_genome_size_;
  double one_minus_prob_noise_;
  double mean_num_links_null_coeff_;
  double one_minus_prob_noise_N_over_G_ ;
  int cache_max_;
  int cache_min_;
  std::vector<double> Tcache_;
  std::vector<int> Tcache_fill_;

  std::vector<double> TPluscache_;
  std::vector<int> TPluscache_fill_;

  std::vector<double> Fcache_;
  std::vector<int> Fcache_fill_;
  //  std::vector<double> Tcache_;


};


#endif // CHICAGO_EDGE_SCORES_H
