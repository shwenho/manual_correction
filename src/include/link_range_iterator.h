#ifndef LINK_RANGE_ITER_H
#define LINK_RANGE_ITER_H

#include <pthread.h>
#include <map>
#include <vector>
#include <list>
#include "common.h"
#include "error_msg.h"
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <fstream>
#include "chicago_links.h"
#include "chicago_read.h"


struct LinkRange {
  int id1;
  int id2;
  unsigned long long mini;
  unsigned long long maxi;

  LinkRange() : id1(0), id2(0), mini(0), maxi(0) {}

};


class IntraScaffoldLinksIterator : public std::iterator<std::input_iterator_tag, LinkRange> {

  LinkRange v_;
  const ChicagoLinks& links_;
  bool done_;

public:

  bool done() {
    return(done_);
  }

  IntraScaffoldLinksIterator( const ChicagoLinks& links ) : links_(links), done_(false) {
    
    unsigned long long i,j;
    unsigned long long nlinks = links_.reads_.size();

    //Read read;

    i=0;     while (i<nlinks &&  links_.reads_[i].target_1==-1) {i++;}
    
             while (i<nlinks && links_.reads_[i].target_1!=links_.reads_[i].target_2 ) { i++; }
    j=i;     while (j<nlinks && links_.reads_[j].target_1==links_.reads_[j].target_2 && links_.reads_[j].target_1==links_.reads_[i].target_1 ) { j++;}

    v_.mini = i;
    v_.maxi = j;
    v_.id1 = links_.reads_[i].target_1;
    v_.id2 = v_.id1;
  } 

  IntraScaffoldLinksIterator& operator++() {
    unsigned long long i,j;
    //    int nlinks;
    unsigned long long nlinks = links_.reads_.size();
    i=v_.maxi;
    while (i<nlinks && links_.reads_[i].target_1!=links_.reads_[i].target_2 ) { i++; }

    if (i==nlinks) {
      v_.mini = 0;
      v_.maxi = 0;
      done_=true;
      return *this;
    }
    j=i;
    while (j<nlinks && links_.reads_[j].target_1==links_.reads_[j].target_2 && links_.reads_[j].target_1==links_.reads_[i].target_1 ) { j++;}
    v_.mini = i;
    v_.maxi = j;
    v_.id1 = links_.reads_[i].target_1;
    v_.id2 = v_.id1;

    return *this;
  }
  LinkRange& operator*() {return v_;}

private:

};

class InterScaffoldLinksIterator : public std::iterator<std::input_iterator_tag, LinkRange> {
  unsigned long long limi_;
  LinkRange v_;
  const ChicagoLinks& links_;
  bool done_;
public:
  
  bool done() {
    return(done_);
  }
  
  inline unsigned long long nlinks() const {
    return(v_.maxi-v_.mini);
  }
  
  InterScaffoldLinksIterator( const ChicagoLinks& links ) : links_(links), done_(false) {
    
    unsigned long long i,j;
    unsigned long long nlinks = links_.reads_.size();
    limi_=nlinks;
    //Read read;

    i=0;     while (i<limi_ &&  (links_.reads_[i].target_1==-1||links_.reads_[i].target_2==-1)) {i++;}
    
             while (i<limi_ && links_.reads_[i].target_1==links_.reads_[i].target_2 ) { i++; }
    j=i;     while (j<limi_ && links_.reads_[j].target_1==links_.reads_[i].target_1 && links_.reads_[j].target_2==links_.reads_[i].target_2 ) { j++;}

    v_.mini = i;
    v_.maxi = j;
    v_.id1 = links_.reads_[i].target_1;
    v_.id2 = links_.reads_[i].target_2;

  } 

  InterScaffoldLinksIterator( const ChicagoLinks& links, unsigned long long mini, unsigned long long maxi ) : links_(links), done_(false) {
    
    unsigned long long i,j;
    unsigned long long nlinks = links_.reads_.size();
    limi_=maxi;
    //Read read;

    i=mini;  while (i<limi_ &&  (links_.reads_[i].target_1==-1||links_.reads_[i].target_2==-1)) {i++;}
    
             while (i<limi_ && links_.reads_[i].target_1==links_.reads_[i].target_2 ) { i++; }
    j=i;     while (j<limi_ && links_.reads_[j].target_1==links_.reads_[i].target_1 && links_.reads_[j].target_2==links_.reads_[i].target_2 ) { j++;}

    v_.mini = i;
    v_.maxi = j;
    v_.id1 = links_.reads_[i].target_1;
    v_.id2 = links_.reads_[i].target_2;

  } 

  InterScaffoldLinksIterator& operator++() {
    unsigned long long i,j;
    //    int nlinks;
    //int nlinks = links_.reads_.size();
    i=v_.maxi;
    //    while (i<nlinks &&  (links_.reads_[i].target_1==-1||links_.reads_[i].target_2==-1)) {i++;}
    while (i<limi_ && ( links_.reads_[i].target_1==links_.reads_[i].target_2 || links_.reads_[i].target_1==-1||links_.reads_[i].target_2==-1)) { i++; }

    if (i==limi_) {
      v_.mini = 0;
      v_.maxi = 0;
      done_=true;
      return *this;
    }
    j=i;
    ///      while (j<limi_ && links_.reads_[j].target_1==links_.reads_[j].target_2 && links_.reads_[j].target_1==links_.reads_[i].target_1 ) { j++;}
    j=i;     while (j<limi_ && links_.reads_[j].target_1==links_.reads_[i].target_1 && links_.reads_[j].target_2==links_.reads_[i].target_2 ) { j++;}
    v_.mini = i;
    v_.maxi = j;
    v_.id1 = links_.reads_[i].target_1;
    v_.id2 = links_.reads_[i].target_2;

    return *this;
  }
  LinkRange& operator*() {return v_;}

private:

};


//for when sorting on broken contig coords:
class ScaffoldPairLinksIterator : public std::iterator<std::input_iterator_tag, LinkRange> {

  LinkRange v_;
  const ChicagoLinks& links_;
  const PathSet& scaffolds_;
  bool done_;
public:
  
  bool done() {
    return(done_);
  }
  
  inline unsigned long long nlinks() const {
    return(v_.maxi-v_.mini);
  }

  std::pair<int,int> sc_pair(int i) {
    int s1 = scaffolds_.ContigScaffold(links_.reads_[i].target_1);
    int s2 = scaffolds_.ContigScaffold(links_.reads_[i].target_2);
    if (s1>s2) {std::swap(s1,s2);}
    return std::make_pair(s1,s2);
  }
  
  ScaffoldPairLinksIterator( const ChicagoLinks& links, const PathSet& scaffolds ) : links_(links), scaffolds_(scaffolds), done_(false) {
    
    unsigned long long i,j;
    unsigned long long nlinks = links_.reads_.size();

    //Read read;

    i=0;     while (i<nlinks &&  (links_.reads_[i].target_1==-1||links_.reads_[i].target_2==-1)) {i++;}
    
//             while (i<nlinks && links_.reads_[i].target_1==links_.reads_[i].target_2 ) { i++; }
    j=i;     while (j<nlinks && (sc_pair(i)==sc_pair(j))) {j++;} 

    v_.mini = i;
    v_.maxi = j;
    v_.id1 = scaffolds_.ContigScaffold(links_.reads_[i].target_1); //links_.reads_[i].target_1;
    v_.id2 = scaffolds_.ContigScaffold(links_.reads_[i].target_2);// links_.reads_[i].target_2;
    if (v_.id1>v_.id2) {std::swap(v_.id1,v_.id2);}
  } 

  ScaffoldPairLinksIterator& operator++() {
    unsigned long long i,j;
    //    int nlinks;
    unsigned long long nlinks = links_.reads_.size();
    i=v_.maxi;
    //    while (i<nlinks &&  (links_.reads_[i].target_1==-1||links_.reads_[i].target_2==-1)) {i++;}
    //while (i<nlinks && ( links_.reads_[i].target_1==links_.reads_[i].target_2 || links_.reads_[i].target_1==-1||links_.reads_[i].target_2==-1)) { i++; }

    if (i==nlinks) {
      v_.mini = 0;
      v_.maxi = 0;
      done_=true;
      return *this;
    }
    //j=i;
    ///      while (j<nlinks && links_.reads_[j].target_1==links_.reads_[j].target_2 && links_.reads_[j].target_1==links_.reads_[i].target_1 ) { j++;}
    //j=i;     while (j<nlinks && links_.reads_[j].target_1==links_.reads_[i].target_1 && links_.reads_[j].target_2==links_.reads_[i].target_2 ) { j++;}
    j=i;     while (j<nlinks && (sc_pair(i)==sc_pair(j))) {j++;} 
    v_.mini = i;
    v_.maxi = j;
    //    v_.id1 = links_.reads_[i].target_1;
    // v_.id2 = links_.reads_[i].target_2;
    v_.id1 = scaffolds_.ContigScaffold(links_.reads_[i].target_1); //links_.reads_[i].target_1;
    v_.id2 = scaffolds_.ContigScaffold(links_.reads_[i].target_2);// links_.reads_[i].target_2;
    if (v_.id1>v_.id2) {std::swap(v_.id1,v_.id2);}

    return *this;
  }

  LinkRange& operator*() {return v_;}

private:

};


#endif
