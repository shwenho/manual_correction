#ifndef hirise_setup_h_
#define hirise_setup_h_



void log_progress_info( std::ostream& out,const std::string& message,const std::chrono::time_point<std::chrono::high_resolution_clock>& chrono_start) {
  out << message ;
  out << "[ " << std::chrono::duration_cast<std::chrono::minutes>(  std::chrono::high_resolution_clock::now() - chrono_start   ).count() << " minutes in ]";
  out << "\n" ;
  out.flush();
}


inline bool looks_like_bamfile(std::string const &filename) {
  if ( filename.length() >= 5 ) {
    return (0== filename.compare( filename.length() - 4, 4, ".bam" ) );
  } 
  return false;
}

inline bool file_exists (const std::string& name) {
  ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }   
}

struct HiriseOpts {
  int nthreads;
  std::string outfile;
  double minscore;
  HiriseOpts() : nthreads(1), minscore(20.0) {};
};

HiriseOpts setup(int argc, char * argv[],   ChicagoLinks& links, PathSet& scaffolds ) {
  HiriseOpts opts;
  bool use_broken_contig=false;
  bool layout=false;
  bool dump_reads=false;
  bool restore_dump=false;
  bool read_bed=false;
  bool useDepthPenalty=false;
  int pad=0;
  int minsep=0;
  int mapq = 0;
  int n_threads=16;
  int max_rounds = 10;
  int minscore = 20;
  int maxscore = 100;
  int gapsize = 1000;
  std::string hrafile;
  std::vector<std::string> model_files;
  std::string reads_dump_out;
  std::string output_file;
  std::string reads_dump_in ;
  std::string bedfile ;
  std::vector<std::string> bamfiles;
  std::string shotfile = "";
  std::string oo_req_file = "";

  //  std::vector<std::string> ranges;
  //-p 100000 -m 500 -q 5 
  int ii=1;
  while (ii<argc) {
    if ( strcmp(argv[ii],"-R")==0 ) {
      ii++;
      max_rounds = std::stoi(argv[ii]);
    } else if ( strcmp(argv[ii],"-q")==0 ) {
      ii++;
      mapq = std::stoi(argv[ii]);
    } else if ( strcmp(argv[ii],"-g")==0 ) {
      ii++;
      gapsize = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-i")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing HRA file: %s\n",argv[ii]); exit(0); }
      hrafile = argv[ii];
      layout=true;
    } else if  ( strcmp(argv[ii],"-j")==0 ) {
      ii++;
      n_threads = std::stoi(argv[ii]);
      opts.nthreads = n_threads;

    } else if  ( strcmp(argv[ii],"-X")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing reads dump input file: %s\n",argv[ii]); exit(0); }
      reads_dump_in = argv[ii];
      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-M")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing model file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"model_file: %s\n",argv[ii]);
      model_files.push_back(std::string(argv[ii]));
      //      restore_dump=true;
    } else if  ( strcmp(argv[ii],"-o")==0 ) {
      ii++;
      output_file = argv[ii];
      opts.outfile = output_file;
      //      fprintf(stdout,"N: %d %d\n",ii,std::stoi(argv[ii]));
    } else if ( strcmp(argv[ii],"-O")==0 ) {
      ii++;
      reads_dump_out = argv[ii];
      dump_reads=true;
    } else if  ( strcmp(argv[ii],"-b")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing BAM file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"bamfile: %s\n",argv[ii]);
      bamfiles.push_back(std::string(argv[ii]));
    } else if  ( strcmp(argv[ii],"-m")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing masking BED file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"masking_bedfile: %s\n",argv[ii]);
      bedfile =argv[ii];
      read_bed=true;
    } else if  ( strcmp(argv[ii],"-s")==0 ) {
      ii++;
      if (!file_exists(argv[ii]) && strcmp(argv[ii], "fake") != 0) { fprintf(stdout,"Missing shotgun BAM file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"shotgun_bamfile: %s\n",argv[ii]);
      shotfile = argv[ii];
    } else if (strcmp(argv[ii], "-d") == 0) {
      useDepthPenalty = false;
    } else if (strcmp(argv[ii], "-D") == 0) {
      useDepthPenalty = true;
    } else if (strcmp(argv[ii], "--minscore") == 0) {
      ii++;
      minscore = std::stoi(argv[ii]);
      opts.minscore = minscore;
    } else if (strcmp(argv[ii], "--maxscore") == 0) {
      ii++;
      maxscore = std::stoi(argv[ii]);
    } else if  ( strcmp(argv[ii],"-r")==0 ) {
      ii++;
      if (!file_exists(argv[ii])) { fprintf(stdout,"Missing o&o requests file: %s\n",argv[ii]); exit(0); }
      fprintf(stdout,"o&o_requests: %s\n",argv[ii]);
      oo_req_file =argv[ii];
    } else {
      fprintf(stdout,"unrecognized option: %s\n",argv[ii]);
      exit(0);
    }
    ii++;
  }
  use_broken_contig = true;

  if (argc<3) {
    fprintf(stdout,"usage: %s [-j <n threads>=16] -m <mask bedfile> -b <bamfile> [-b <bamfile> ... ] [-s <shotgunbam>] -M <modelfile> [-M <modelfile> ...] [-X <dump restore file>] -o <dump file> -i <hra file> -q <mapq>\n",argv[0]);
    // and -r for oo requests file
    exit(0);
  }

  //
  // Check agreement in # chicago files and # models (if only one model, combine chicago bamfiles)
  //
  int model_increment = 0;
  if (bamfiles.size() > 1 && model_files.size() > 1) {
    if (bamfiles.size() != model_files.size()) {
      fprintf(stdout, "Multiple bamfiles and model files cannot disagree in counts: %lu and %lu\n", bamfiles.size(), model_files.size());
      exit(0);
    }
    model_increment = 1;
  }
  std::vector<PairedReadLikelihoodModel *> models;
  for (int i = 0; i < model_files.size(); i++) {
    std::string model_json;
    std::ifstream model_stream ;
    model_stream.open(model_files[i]);
    //  model_file >> model_json;
    std::getline(model_stream, model_json);
    LikelihoodModelParser model_parser;
    PairedReadLikelihoodModel *model = model_parser.ParseModelJson(model_json.c_str());
    models.push_back(model);
  }
  // feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);

  /*
  if (argc<3) {
    fprintf(stdout, "Expected at least 3 args: bed file, hra file, and one or more bamfiles.  Found %d\n", argc-1);
    exit(0);
    }*/

  //  ChicagoLinks links;

  auto chrono_start = std::chrono::high_resolution_clock::now();
  links.min_mapq() = mapq;
  std::cout << restore_dump << std::endl;
  if (restore_dump) { 
    std::cout << "restoring reads " << std::endl;
    links.PopulateFromBam( bamfiles[0].c_str(), false, models[0], 0 ,reads_dump_in);
    links.SetModelsVector(models);
  }
  else {
    // If only one model, use for all chicago bamfiles (model_increment == 0)
    // otherwise chicago & models match up one by one (model_increment == 1)
    for (int i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      std::cout << "Load data from:" <<  bamfiles[i] << "\twith model:\t" << model_files[model_i] << "as bamfile? "<< looks_like_bamfile(bamfiles[i]) << "\n";
    }
    for (int i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      if (looks_like_bamfile(bamfiles[i])) {
	fprintf(stdout,"looks like bamfile: %s\n",bamfiles[i].c_str());
	links.PopulateFromBam(   bamfiles[i] , false, models[model_i], model_i );
      } else {
	fprintf(stdout,"looks like tablefile: %s\n",bamfiles[i].c_str());
	links.PopulateFromTable( bamfiles[i] , false, models[model_i], model_i );	
      }	
    }
    //    links.SortByContigs();
    std::cout << "\n";
  }

  log_progress_info(std::cout, "Done loading read data", chrono_start);

  if (dump_reads) {
    std::cout << "Dumping reads\n";
    links.write_reads(reads_dump_out);
    exit(0);
  }
  
  scaffolds.SetHeader(links.GetHeader());
  //  PathSet scaffolds(links.GetHeader());
  //  OO_Requests oo_reqs;

  scaffolds.ParseFromHRA(links.GetHeader(), hrafile );

  log_progress_info(std::cout, "Done loading scaffolding", chrono_start);

  //std::cout << "##\n";
  //std::cout << scaffolds;


  if (read_bed) {
    scaffolds.ReadMaskedSegments(bedfile,links.GetHeader());
    log_progress_info(std::cout, "Done loading masked segments", chrono_start);
  }

  const SegmentSet masking = scaffolds.MaskedSegments() ;
  links.SetMaskedFlags( masking );
  log_progress_info(std::cout, "Done setting masking flags", chrono_start);
  
  // "fake" ugliness affects this file only. Can call utility method in a cleaner way later, if needed.
  if (!shotfile.empty()) {
    if (shotfile == "fake") {
      scaffolds.FakeContigSegShotgun();
    }
    else {
      scaffolds.CountContigSegShotgun(shotfile);
    }
    fprintf(stdout, "Switching to MetaHirise\n");
    for (int i = 0; i < models.size(); i++) {
      //ChicagoExpModel* cmodel = dynamic_cast<ChicagoExpModel*>(models[i]);
      models[i]->SetMetagenomic(scaffolds.ShotgunCounted(), useDepthPenalty);
    }
  }

  //  if (!oo_req_file.empty()) {
  //  Load_OO_Reqs(oo_reqs, oo_req_file, true);
  //}
    



  //  if (use_broken_contig) {
    fprintf(stderr,"Switching to broken contig coordinates...\n");
    links.SwitchToBrokenContigCoordinates( scaffolds );
  log_progress_info(std::cout, "Done switching to broken scaffold coordinates", chrono_start);
    //} else {
    // fprintf(stderr,"Switching to scaffold coordinates...\n");
    //links.SwitchToScaffoldCoordinates( scaffolds );
    //}
  fprintf(stderr,"Indexing masked segments...\n");
  fprintf(stdout,"Indexing masked segments...\n");
  scaffolds.IndexMaskedScaffoldSegments();
  log_progress_info(std::cout, "Done indexing the masked segemnts in broken contig coordinates", chrono_start);

  return opts;

}
#endif
