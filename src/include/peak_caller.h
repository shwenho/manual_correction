#ifndef PEAK_CALLER_H
#define PEAK_CALLER_H

#include <algorithm>
#include <cmath>
#include <vector>

enum PeakType {
    NONE,
    SIMPLE  = (1 << 1),
    RISING  = (1 << 2),
    FALLING = (1 << 3),
    BOTH    = RISING | FALLING
};

template <typename T>
struct PeakValueComparator {
    const std::vector<T>& values;

    PeakValueComparator(const std::vector<T>& _values) : values(_values) {}
    bool operator()(const int& lhs, const int& rhs) {
        return values[lhs] >= values[rhs];
    }
};

template <typename T>
void detect_peaks(const std::vector<T>& x,
                  std::vector<int>& peaks,
                  T mph=-INFINITY,
                  uint32_t mpd=1,
                  PeakType ptype=RISING,
                  T threshold=0,
                  bool kpsh=false) {
    if (x.size() < 3)
        return;

    int i;
    // Diffs between elements in vector
    std::vector<T> dx;
    for (i=1; i<x.size(); ++i)
        dx.push_back(x[i] - x[i-1]);

    // Identify peaks
    PeakType this_ptype;
    for (i=0; i<dx.size()-1; ++i) {
        // Enforce minimum peak height
        if (x[i+1] < mph)
            continue;

        const T& before = dx[i];
        const T& after = dx[i+1];

        // Identify peak type
        if (ptype == SIMPLE) {
            if (before <= threshold || after >= -threshold)
                continue;
        }
        else {
            this_ptype = NONE;
            if (before > threshold && after <= 0)
                this_ptype = RISING;
            else if (before >= 0 && after < -threshold)
                this_ptype = FALLING;

            if ((ptype & this_ptype) == 0)
                continue;
        }

        // Add peak
        peaks.push_back(i+1);
    }

    if (!peaks.empty() && mpd > 1) {
        // Sort peaks by peak height
        std::sort(peaks.begin(), peaks.end(), PeakValueComparator<T>(x));
        // Remove peaks closer than minimum peak distance
        std::vector<int>::iterator j, k;
        for (j=peaks.begin(); j!=peaks.end(); ++j) {
            for (k=j+1; k!=peaks.end();) {
                if ((*k) < (*j)-mpd ||
                    (*k) > (*j)+mpd ||
                    (kpsh && x[*j] == x[*k])) {
                    ++k;
                    continue;
                }
                k = peaks.erase(k);
            }
        }
    }

    std::sort(peaks.begin(), peaks.end());
}

#endif
