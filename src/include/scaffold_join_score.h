#ifndef SCAFFOLD_JOIN_SCORE_H
#define SCAFFOLD_JOIN_SCORE_H

#include <map>
#include <string>
#include <vector>
#include "htslib/hts.h"
#include "htslib/sam.h"
#include "common.h"
#include "error_msg.h"
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "chicago_read.h"
#include "pair_distance_likelihood_model.h"
#include "paired_read_likelihood_model.h"
#include "segment_set.h"
#include "bam_header_info.h"
#include "chicago_scaffolds.h"
#include "bincounts_heap.h"
#include "chicago_links.h"
#include "link_range_iterator.h"




#define WRITE_LINKS true 

//         0          1              2           3              4
//    ==========  ============  ===========  =============  ============  
//  g=0        g=1           g=2          g=3            g=4            g=n+1     

#define MAX_STEPS 1
bool interc_gap_relevant(const Path& sc1,int g,const std::map<int,int>& read_hits) {
  int i;
  std::map<int,int>::const_iterator it;
  for (i=max(0,g-1-MAX_STEPS);i<min(sc1.len(),g+1+MAX_STEPS);i++) {
    it=read_hits.find(sc1.IdAtPosition(i));
    if (!(it==read_hits.end())) {return true;}
  }
  return false;
}

double gap_support_llr(const Path& scaffold, int g, const ChicagoLinks& links, const PathSet& scaffolds, int& num_links_used, ofstream& logstream) {
  int n = scaffold.ncontigs();
  int i,j;
  unsigned long long mini,maxi;
  //  int g;
  int gapsize;
  double dllr;
  double llr=0.0;

  int gapx = scaffold.GetContig(g).scaffold_start;
  num_links_used = 0;
  for (i=0;i<g;i++) {
    const TargetRange    contigi = scaffold.GetContig(i);
    if (gapx-contigi.scaffold_end > 200000) {continue; };
    for (j=g;j<n;j++) {
      const TargetRange   contigj = scaffold.GetContig(j);
      //      if (contigi.scaffold_start - gapx > 200000) {break; };
      gapsize = contigj.scaffold_start - contigi.scaffold_end;
      if (gapsize>200000) {break;}
      //      std::cout << "(" << i <<"," << j << ")\t" << gapsize << "\t" << contigi <<","<<contigj <<" \n";// <<  get_group(group,i      
      ContigPair cp(contigi.id,contigj.id,contigi.len(),contigj.len(),gapsize);
      if (contigi.orientation=='-') { cp.set_strand_1(1);}
      if (contigj.orientation=='-') { cp.set_strand_2(1);}
      cp.normalize();
      links.GetContigPairRowRange(cp,&mini,&maxi);

      //links.ContigPairLLR(&cp,mini,maxi,&dllr,200000);
      links.ContigPairLLRCorrected(&cp,scaffolds,mini,maxi,&dllr,200000);
      // 0 links seems... problematic... testing.
      if((maxi-mini) < 1 && (dllr > 0)) {
	logstream << "#debug: weird has " << maxi-mini << " but result " << dllr << "\n";
      }

      llr += dllr;
      num_links_used += maxi - mini;
      #if(DEBUG_ENABLED) 
	logstream << "#debug: ollr\t" <<  contigi.id << "\t" << contigj.id << "\t" << contigi.orientation <<  "\t" << contigj.orientation
		  << "\t" << contigi.len() << "\t" << contigj.len() 
		  << "\t"  << gapsize << "\t" << maxi - mini << "\t" << dllr << "\t" << llr << "\n";
#endif
    }
    
  }
  return(llr);
}

void write_edge(std::ostream& out, const TargetRange& c1,const TargetRange& c2, const int gap, const ChicagoLinks &links) {
  //  out << "edge: " << c1 << "," << c2<<"\t" << gap << "\n";
  
  //  out << "edge: "; c1.WriteHiRiseName(out,links.GetHeader()); out << "," ;  c2.WriteHiRiseName(out,links.GetHeader()); out <<"\t" << gap << "\n";
  out << "('"; c1.WriteHiRiseNameRight(out,links.GetHeader()); out << "', '" ;  c2.WriteHiRiseNameLeft(out,links.GetHeader()); out <<"')";
}


ScaffoldMergeMove log_join_option(std::ostream& out, double score,const Path& s1, const Path& s2 , const Path& scaffold , const ChicagoLinks &links ) {
  ScaffoldMergeMove move;
  int n1 = s1.ncontigs();

  const TargetRange contigi = scaffold.GetContig(n1-1);
  const TargetRange contigj = scaffold.GetContig(n1);

  //  std::cout << "add a join:\n";
  move.AddJoin( Join(contigi.id,contigj.id,contigi.orientation,contigj.orientation) );

#if WRITE_LINKS  
  if (score > 0.0) {
    out << "link score " ; contigi.WriteHiRiseNameRight(out,links.GetHeader()) ; out << " " ; contigj.WriteHiRiseNameLeft(out,links.GetHeader()) ; out << " " << score << "\n";
  }
#endif
  return(move);
}


ScaffoldMergeMove log_interc_option(std::ostream& out, double score,const Path& s1, const Path& s2, int g, const Path& scaffold, const std::vector<int>& group, const ChicagoLinks &links ) {
  ScaffoldMergeMove move;

#if WRITE_LINKS  
  if (score>0.0) {out << "interc: (" << score << ", ("; }
#endif
  //  the joins to make
  int n = scaffold.ncontigs();
  int i,j;

  //std::cout << "add an interc:\n";
  for (i=0;i<n-1;i++) {
    const TargetRange    contigi = scaffold.GetContig(i);
    //    for (j=i+1;j<n;j++) {
    j=i+1;

      const TargetRange   contigj = scaffold.GetContig(j);
      if (group[i]!=group[j]) {
	//	if (j==i+1) {	  

#if WRITE_LINKS  
      if (score>0.0) {	  write_edge( out, contigi, contigj,0, links  );  out << ", "; }
#endif
      move.AddJoin( Join(contigi.id,contigj.id,contigi.orientation,contigj.orientation) );
	  //	} 
      }
      //    }
  }
#if WRITE_LINKS  
  if (score>0.0) {  out << "), ("; }
#endif
  //  the joins to remove

  const TargetRange contigi = s1.GetContig(g-1);
  const TargetRange contigj = s1.GetContig(g);
#if WRITE_LINKS  
  if (score>0.0) { write_edge( out, contigi, contigj,0, links  );  out << ", "; }
#endif
  move.SetBreak( Join(contigi.id,contigj.id,contigi.orientation,contigj.orientation) );

#if WRITE_LINKS  
  if (score>0.0) {  out << "))\n";   }
#endif
  return(move);
}


double interct_score(const Path scaffold, const std::vector<int>& group, const ChicagoLinks &links, const PathSet& scaffolds, int& num_links_used, ofstream& logstream) {
  int n = scaffold.ncontigs();
  int i,j;
  unsigned long long mini,maxi;
  int gapsize;
  double dllr;
  double llr=0.0;
  //ContigPair cp;
  //contigi  = scaffold.GetContig(0);
  //const TargetRange contigj  = scaffold.GetContig(0);

  int gapx,gapy;// = scaffold.GetContig(g).scaffold_start;
  num_links_used = 0;
  //gapx to gapy is the range of scaffold coordinates encompasing the inserted scaffold.
  gapx = scaffold.GetContig(0  ).scaffold_end   ;
  gapy = scaffold.GetContig(n-1).scaffold_start ;
  for (i=0;i<n;i++) {
    if (group[i]==0) {gapx = scaffold.GetContig(i).scaffold_end; }
    if (group[i]==1) {gapy = scaffold.GetContig(i).scaffold_start; break;}
  }
  /*
  if ((gapx==-1) || (gapy==-1)) {
    std::cerr << "gapx: "<< gapx <<"\n";
    std::cerr << "gapy: "<< gapy <<"\n";
    std::cerr << "Error identifying gap boundaries in a scaffold with "<< n <<" contigs:\n";
    for (int i=0;i<n;i++) {
      std::cerr << "\t" << i <<"\t" << group[i]  << ":\n";
    }
    assert(gapx!=-1);
    assert(gapy!=-1);
  }
  */
  for (i=0;i<n;i++) {
    //    if (group[i]>0) {break;}
    const TargetRange    contigi = scaffold.GetContig(i);
    //    if ( gapx-contigi.scaffold_end > 200000 ) {continue;}
    for (j=i+1;j<n;j++) {
      const TargetRange   contigj = scaffold.GetContig(j);
      //  if ( contigj.scaffold_end-gapy > 200000 ) {break;}

   //std::cout << "scaffold: " << n <<"\t"<< i <<"," << j << "\n"; // <<  get_group(group,i) << "\t" <<  get_group(group,j) << "\t" << different_groups(group,i,j) <<"\n";
   if (group[i]!=group[j]) {
	gapsize = contigj.scaffold_start - contigi.scaffold_end;
	if (gapsize>200000) {continue;}
	//if (j==i+1) { std::cout << "##### ";	}; //  write_edge( std::cout, contigi, contigj, gapsize, links  ); std::cout << "\n";} 
	//if (j==i+1) {	  write_edge( std::cout, contigi, contigj, gapsize, links  ); std::cout << "\n";} 
	//write_edge( std::cout, contigi, contigj, gapsize, links  ); std::cout << "\n";
	//std::cout << "(" << i <<"," << j << ";" << group[i] << "," << group[j] << ")\t" << gapsize << "\t" << contigi <<","<<contigj <<" \n";// <<  get_group(group,i) << "\t" <<  get_group(group,j) << "\t" << different_groups(group,i,j) <<"\n";
	ContigPair cp(contigi.id,contigj.id,contigi.len(),contigj.len(),gapsize);
	
	if (contigi.orientation=='-') { cp.set_strand_1(1);}
	if (contigj.orientation=='-') { cp.set_strand_2(1);}
	cp.normalize();
	

	links.GetContigPairRowRange(cp,&mini,&maxi);
        //links.ContigPairLLR(&cp,mini,maxi,&dllr,200000);
        links.ContigPairLLRCorrected(&cp,scaffolds,mini,maxi,&dllr,200000);
	llr += dllr;
	num_links_used += maxi - mini;

	#if(DEBUG_ENABLED) 
	  logstream << "#debug: llr\t" <<  contigi.id << "\t" << contigj.id << "\t" << contigi.orientation  << "\t" << contigj.orientation 
		    << "\t" << contigi.len() << "\t" << contigj.len() 
		    << "\t" << gapsize << "\t" << maxi - mini << "\t" << dllr << "\t" << llr << " " << group.size() << " " << i << " " << j;
	  for(auto group_item=group.begin(); group_item != group.end(); group_item++) {
	    logstream << " " << *group_item << " ";
	  }
	  logstream << "\n";
#endif
	//std::cout << "score:" << cp << "\t" << mini << "-" << maxi << "\t" << dllr<<"\n";
      }
    }
  }
  //std::cout << "\n";
  return(llr);
}

#define MAX_INTERC_SIZE 20000
void scaffold_interc_scores(int s1, int s2,const PathSet& scaffolds, const ChicagoLinks &links, ofstream& logstream, std::vector<ScaffoldMergeMove>& move_pool,int gapsize=1000 ) {
  //
  //  For a pair of scaffolds, compute all the end-to-end joins and intecalation scores that can be made with them.
  //
  Path merged_scaffold;
  std::vector<int> group;
  std::vector<ScaffoldMergeMove> moves;
  ScaffoldMergeMove move;
  int n1,n2;
  Path sc1 = scaffolds.GetScaffold(s1);
  Path sc2 = scaffolds.GetScaffold(s2);
  double llr;
  n1 = sc1.ncontigs();
  n2 = sc2.ncontigs();
  int l1 = sc1.SeqLength();
  int l2 = sc2.SeqLength();
  double ollr;
  int g;
  unsigned long long sp_rowi,sp_rowj;
  links.GetScaffoldPairRowRange(s1,s2,&sp_rowi,&sp_rowj);
  //  links.CheckScaffoldPairRowRange(s1,s2,sp_rowi,sp_rowj,scaffolds);
  int nL=0;
  int nR=0;

  std::map<int,int> read_hits;
  //  read_hits.clear();
  links.RowRangeHitMap(sp_rowi,sp_rowj, read_hits,logstream,scaffolds) ;
  
  //end-to-end joins:
  g=0;
  int num_links_used = 0;
  scaffolds.MakeIntercPath(sc1,sc2, merged_scaffold, group, g,false,gapsize) ;  llr= interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream) ;
  move=  log_join_option(logstream, llr, sc2,sc1,merged_scaffold,links); move.score = llr; move.num_links_used = num_links_used; moves.push_back(move);
#if(DEBUG_ENABLED)
  if( llr > 0) {
    //logstream << "#: " << llr <<"\t"  << s1 << "," << s2 << "\t"  << g << false << "\n";
  }
  #endif
  scaffolds.MakeIntercPath(sc1,sc2, merged_scaffold, group, g,true,gapsize) ;  llr= interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream);
  move=  log_join_option(logstream, llr, sc2,sc1,merged_scaffold,links); move.score = llr; move.num_links_used = num_links_used; moves.push_back(move);
#if(DEBUG_ENABLED)
  if(llr > 0) {
    //logstream << "end2end: " << llr <<"\t"  << s1 << "," << s2 << "\t"  << g << true << "\n";
  }
  #endif
  g=n1;
  scaffolds.MakeIntercPath(sc1,sc2, merged_scaffold, group, g,false,gapsize) ;   llr=interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream);
  move=log_join_option(logstream, llr, sc1,sc2,merged_scaffold,links); move.score = llr; move.num_links_used = num_links_used; moves.push_back(move);
#if(DEBUG_ENABLED)
  if( llr > 0) {
    //  logstream << "end2end: " << llr <<"\t"  << s2 << "," << s1 << "\t"  << g << false << "\n";
  }
  #endif
  scaffolds.MakeIntercPath(sc1,sc2, merged_scaffold, group, g,true,gapsize) ;    llr=interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream);
  move=log_join_option(logstream, llr, sc1,sc2,merged_scaffold,links); move.score=llr; move.num_links_used = num_links_used; moves.push_back(move);
  #if(DEBUG_ENABLED)
    if( llr > 0) {
    //  logstream << "end2end: " << llr <<"\t"  << s2 << "," << s1 << "\t"  << g << true << "\n";
  }
  #endif
  /*
  logstream << "contigs sc1:\t";
  for (g=0;g<n1;g++) {
    logstream << sc1.IdAtPosition(g) << ",";
  }
  logstream << "\n";

  logstream << "gaps to use 1:\t";
  for (g=1;g<n1;g++) {
    logstream << interc_gap_relevant(sc1,g,read_hits) << ",";
    
  }
  logstream << "\n";

  logstream << "contigs sc2:\t";
  for (g=0;g<n2;g++) {
    logstream << sc2.IdAtPosition(g) << ",";
  }
  logstream << "\n";

  logstream << "gaps to use 2:\t";
  for (g=1;g<n2;g++) {
    logstream << interc_gap_relevant(sc2,g,read_hits) << ",";
    
  }
  logstream << "\n";

  logstream << "link contigs::\t";
  for (std::map<int,int>::iterator it=read_hits.begin();it!=read_hits.end();it++) {
    logstream << it->first << ",";
  }
  logstream << "\n";    
  
  */
  // intercs of s2 into s1:  [skip if s2 too long?]
  int num_links_orig = 0;
  if (l2<MAX_INTERC_SIZE) {
    for (g=1;g<n1;g++) {

      if (! interc_gap_relevant(sc1,g,read_hits) ) {continue;}
    
    nL++;
    scaffolds.MakeIntercPath(sc1,sc2, merged_scaffold, group, g,false) ;  llr=interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream); ollr= gap_support_llr(sc1,g,links,scaffolds, num_links_orig, logstream);
#if(DEBUG_ENABLED)
    if( llr-ollr > 0) {
      logstream << "interc: " << llr - ollr<<"\t" << ollr << "\t" << s1 << "," << s2 << "\t"  << g << "\n";
    }
    #endif
//  std::cout << "interc: " << llr - ollr<<"\t"  << s1 << "," << s2 << "\t" << merged_scaffold << "\n";
    move= log_interc_option(logstream, llr-ollr, sc1,sc2,g,merged_scaffold,group,links); move.score=llr-ollr; move.score_orig = ollr;
    move.num_links_used = num_links_used-num_links_orig; move.num_links_orig = num_links_orig; moves.push_back(move);

    scaffolds.MakeIntercPath(sc1,sc2, merged_scaffold, group, g,true) ;   llr=interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream); //ollr= gap_support_llr(sc1,g,links,scaffolds);
#if(DEBUG_ENABLED)
    if( llr-ollr > 0) {
      logstream << "interc: " << llr - ollr<<"\t" << ollr << "\t" << s1 << "," << s2 << "\t"  << g << "\n";
    }
    #endif
//  std::cout << "interc: " << llr - ollr<<"\t"  << s1 << "," << s2 << "\t" << merged_scaffold << "\n";
     move= log_interc_option(logstream, llr-ollr, sc1,sc2,g,merged_scaffold,group,links); move.score=llr-ollr; move.score_orig = ollr;
     move.num_links_used = num_links_used-num_links_orig; move.num_links_orig = num_links_orig; moves.push_back(move);
  }
   }

  // intercs of s1 into s2:  [skip if s1 too long?]
   if(l1<MAX_INTERC_SIZE) {
  for (g=1;g<n2;g++) {

    if (! interc_gap_relevant(sc2,g,read_hits) ) {continue;}

    nR++;
    scaffolds.MakeIntercPath(sc2,sc1, merged_scaffold, group, g,false) ;  llr=interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream) ; ollr= gap_support_llr(sc2,g,links,scaffolds, num_links_orig, logstream);
#if(DEBUG_ENABLED)
    if( llr-ollr > 0) {
      logstream << "interc: " << llr - ollr<<"\t" << ollr << "\t" << s1 << "," << s2 << "\t"  << g << "\n";
    }
    #endif
    //  std::cout << "interc: " << llr - ollr<<"\t"  << s2 << "," << s1 << "\t" << merged_scaffold << "\n";
    move=log_interc_option(logstream, llr-ollr, sc2,sc1,g,merged_scaffold,group,links); move.score=llr-ollr; move.score_orig = ollr;
    move.num_links_used = num_links_used-num_links_orig; move.num_links_orig = num_links_orig; moves.push_back(move);

    scaffolds.MakeIntercPath(sc2,sc1, merged_scaffold, group, g,true) ;   llr=interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream) ;// ollr=gap_support_llr(sc2,g,links,scaffolds);
    #if(DEBUG_ENABLED)
      if( llr-ollr > 0) {
      logstream << "interc: " << llr - ollr<<"\t" << ollr << "\t" << s1 << "," << s2 << "\t"  << g << "\n";
    }
    #endif
//  std::cout << "interc: " << llr - ollr<<"\t" << s2 << "," << s1 << "\t" << merged_scaffold << "\n";
    move=log_interc_option(logstream, llr-ollr, sc2,sc1,g,merged_scaffold,group,links); move.score=llr-ollr; move.score_orig = ollr;
    move.num_links_used = num_links_used-num_links_orig; move.num_links_orig = num_links_orig; moves.push_back(move);
  }
   }
  //  std::cout << "moves: " << moves.size() << "\n";
  std::sort(moves.begin(),moves.end());

  int i;
  for(i=0;i<moves.size();i++) {
    if (moves[i].score>0.0) {
      move_pool.push_back( moves[i] );
    }
  }
  return;
  }

void orientation_confidence(const PathSet& scaffolds, const ChicagoLinks& links, std::string output) {
  /* For every contig in a scaffold, write the confidence we have in its orientation.
  */
  ofstream output_stream(output);

  // iterate over every scaffold in assembly
  for(const auto& it_obj : scaffolds) {

    // represented as a map of scaffold_id: Path scaffold
    // so scaffold is accessed as second
    auto scaffold = it_obj.second;

    // skip scaffolds of only one contig
    if(scaffold.size() == 1) {
      continue;
    }
    
    // iterate over every contig in the scaffold
    // we want the index so we use this style loop
    // for every contig, test what the llr is after inserting
    // it in every orientation
    for(std::size_t i = 0; i != scaffold.size(); i++) {
      auto contig = scaffold[i];

      int gapsize = 1000;
      double llr;
      double opposite_llr;
      int num_links_used;
      ofstream logstream;
      
      // group will represent three segments: left of intercalating,
      // right of intercalating, and the intercalating contig
      // there is no "left" (?) when it's actually an end-to-end join
      std::vector<int> group;

      // Make a scaffold containing just the contig we want to test
      Path interc_scaffold;
      interc_scaffold.Append(contig);


      // scaffold post-intercalation
      Path merged_scaffold;

      // scaffold after taking out the intercalating contigs
      Path new_scaffold;

      // Add back in all contigs, except the contig we're testing
      for(std::size_t j = 0; j != scaffold.size(); j++) {
	auto new_contig = scaffold[j];
	if(i != j) {
	  new_scaffold.Append(new_contig);
	}
      }
    
      // Basically remake the original scaffold, but set "group" in the correct way
      // scaffold coords get reset here.
      scaffolds.MakeIntercPath(new_scaffold, interc_scaffold, merged_scaffold, group, i,false,gapsize) ;  

      // use "group" to test the llr in the area around the intercalating contig
      llr= interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream) ;
      
      // Make a new scaffold, with the contig in the opposite orientation
      scaffolds.MakeIntercPath(new_scaffold, interc_scaffold, merged_scaffold, group, i,true,gapsize) ;  

      // get the llr for the opposite orientation
      opposite_llr= interct_score(merged_scaffold,group,links,scaffolds, num_links_used, logstream) ;

      // per Nik
      double delta = llr - opposite_llr;
      double confidence = 1/(1 + exp(-delta));

      // write to file (is just contig length, but should be some reversible id
      output_stream << contig.scaffold_id << "\t" << TargetRange::header.GetTargetName(contig.target_id) << "\t" << 
	contig.target_start << "\t" << contig.target_end << "\t" << 
	contig.orientation << "\t" << 
	contig.scaffold_start << "\t" << contig.scaffold_end << "\t" << 
	llr << "\t" << opposite_llr << "\t" << confidence << "\n";	
    }
  }
}

class Joiner {
private:
  ofstream & stream_;
  char* sbuff_;
  int sbuff_len_;

public:
  Joiner( std::ofstream & logstream  ): stream_(logstream) { 
    sbuff_len_ = 1024;
    sbuff_ = new char[sbuff_len_];
    
  };

  void AltJoins(int scaffold_1,int scaffold_2,const ChicagoLinks& links,const PathSet& scaffolds,ofstream& logstream, std::vector<ScaffoldMergeMove>& move_pool, int gapsize=1000) {
    unsigned long long sp_rowi,sp_rowj;
    int i,j,k,c1,c2,l1,l2;
    int32_t scaffold_1_shot, scaffold_2_shot = 1;  //TODO: metagenomics comapt?
    double llr;
    links.GetScaffoldPairRowRange(scaffold_1,scaffold_2,&sp_rowi,&sp_rowj);

    InterScaffoldLinksIterator sc_it(links,sp_rowi,sp_rowj);

    while(!sc_it.done()) {
      c1 = (*sc_it).id1;
      c2 = (*sc_it).id2;
      l1 = scaffolds.BrokenContigLen(c1);
      l2 = scaffolds.BrokenContigLen(c2);

      ContigPair cp(c1, c2, l1, l2, scaffold_1_shot, scaffold_2_shot, gapsize);
      llr=0.0; 

      //      std::cout << "cp:" << scaffold_1 <<"\t"<< scaffold_2 <<"\t"<< c1 << "\t"<< c2 <<"\t"<<sc_it.nlinks() <<"\n";

      cp.set_strand_2(0);  links.ContigPairLLR(&cp,(*sc_it).mini ,(*sc_it).maxi,&llr,200000); if (llr>0.0) { move_pool.push_back( ScaffoldMergeMove(cp,llr)); }
      cp.set_strand_1(1);  links.ContigPairLLR(&cp,(*sc_it).mini ,(*sc_it).maxi,&llr,200000); if (llr>0.0) { move_pool.push_back( ScaffoldMergeMove(cp,llr)); }
      cp.set_strand_2(1);  links.ContigPairLLR(&cp,(*sc_it).mini ,(*sc_it).maxi,&llr,200000); if (llr>0.0) { move_pool.push_back( ScaffoldMergeMove(cp,llr)); }
      cp.set_strand_1(0);  links.ContigPairLLR(&cp,(*sc_it).mini ,(*sc_it).maxi,&llr,200000); if (llr>0.0) { move_pool.push_back( ScaffoldMergeMove(cp,llr)); }
      
      ++sc_it;
    }

  }


  void IntercScore(int scaffold_1,int scaffold_2,const ChicagoLinks& links,const PathSet& scaffolds,ofstream& logstream, std::vector<ScaffoldMergeMove>& move_pool, int gapsize=1000) {
    
    scaffold_interc_scores(scaffold_1, scaffold_2, scaffolds,links, logstream,move_pool, gapsize) ;
    return;

  }

  void JoinScore(unsigned long long i,unsigned long long j, const ChicagoLinks&  links,const PathSet&  scaffolds, ofstream & logstream, int gap=1000, int scaffold_1=-1, int scaffold_2=-1) {
    //    int ii;

    int scaffold_1_len, scaffold_2_len;
    int32_t scaffold_1_shot, scaffold_2_shot;
    Read read = links.reads_[i];

    if (scaffold_1 < 0) {
      scaffold_1 = read.target_1;
      scaffold_2 = read.target_2;
    } else {
      if (i<j) {
	assert(read.target_1 == scaffold_1);
	assert(read.target_2 == scaffold_2);
      }
    };

    if (links.isScaffoldSorted()) {
      scaffold_1_len = scaffolds.ScaffoldLen(scaffold_1);
      scaffold_2_len = scaffolds.ScaffoldLen(scaffold_2);
      scaffold_1_shot = scaffolds.ShotgunCnt(scaffold_1);
      scaffold_2_shot = scaffolds.ShotgunCnt(scaffold_2);
    } else if (links.isBrokenContigSorted()) {
      scaffold_1_len = scaffolds.BrokenContigLen(scaffold_1);
      scaffold_2_len = scaffolds.BrokenContigLen(scaffold_2);
      scaffold_1_shot = 1; //scaffolds.ShotgunCnt(scaffold_1);  //TODO:  fix this if you want metagenomic mode to work right in broken contig mode....
      scaffold_2_shot = 1; //scaffolds.ShotgunCnt(scaffold_2);      
    } else {
      assert(1==0);
    }

    int mi,mj;

    ContigPair cp(scaffold_1, scaffold_2, scaffold_1_len, scaffold_2_len, 
		  scaffold_1_shot, scaffold_2_shot, gap);

    //    cp.set( read.target_1, read.target_2, scaffold_1_len, scaffold_2_len, gap );
    double llr1,llr2,llr3,llr4;
    double cllr1,cllr2,cllr3,cllr4;

    //  void ContigPairLLRCorrected( ContigPair *cp, const PathSet& scaffolds, int mini, int maxi, double *result ) const {

    if (scaffold_1<int(scaffolds.scaffold_mask_segments_index_.size())) {
      mi = scaffolds.scaffold_mask_segments_index_[scaffold_1];
      assert(mi>=0);
      assert(mi<=int(scaffolds.scaffold_mask_segments_.size()));
    } else {
      mi = scaffolds.scaffold_mask_segments_.size();
    }

    if (scaffold_2<int(scaffolds.scaffold_mask_segments_index_.size())) {
      mj = scaffolds.scaffold_mask_segments_index_[scaffold_2];
      assert(mj>=0);
      assert(mj<=int(scaffolds.scaffold_mask_segments_.size()));
    } else {
      mj = scaffolds.scaffold_mask_segments_.size();
    }

    /*
    mi = scaffolds.scaffold_mask_segments_index_[scaffold_1];
    mj = scaffolds.scaffold_mask_segments_index_[scaffold_2];

    if (mi<0 || mi>scaffolds.scaffold_mask_segments_.size()) {  // TODO :: fix this work-around for unintialized values in the index
      mi =scaffolds.scaffold_mask_segments_.size();
    }
    if (mj<0 || mi>scaffolds.scaffold_mask_segments_.size()) {  // TODO :: fix this work-around for unintialized values in the index
      mj =scaffolds.scaffold_mask_segments_.size();
    }
    */

    cllr1=0.0;                      links.ContigPairLLRCorrected(&cp,scaffolds,i,j,&cllr1,200000);
    cllr2=0.0; cp.set_strand_1(1);  links.ContigPairLLRCorrected(&cp,scaffolds,i,j,&cllr2,200000);
    cllr3=0.0; cp.set_strand_2(1);  links.ContigPairLLRCorrected(&cp,scaffolds,i,j,&cllr3,200000);
    cllr4=0.0; cp.set_strand_1(0);  links.ContigPairLLRCorrected(&cp,scaffolds,i,j,&cllr4,200000);

    llr1=0.0; cp.set_strand_2(0);  links.ContigPairLLR(&cp,i,j,&llr1,200000);
    llr2=0.0; cp.set_strand_1(1);  links.ContigPairLLR(&cp,i,j,&llr2,200000);
    llr3=0.0; cp.set_strand_2(1);  links.ContigPairLLR(&cp,i,j,&llr3,200000);
    llr4=0.0; cp.set_strand_1(0);  links.ContigPairLLR(&cp,i,j,&llr4,200000);

    unsigned long long good_scores = 0;
    for (unsigned long long k=i; k<j; k++) {
      read = links.reads_[k];
      if (read.Ok()) {
	good_scores += 1;
      }
    }

    //    sprintf(sbuff_,"%d\t%d\t%d\t%d\t%d\t%d\t>> <> << ><\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%d\t",scaffold_1,scaffold_2,scaffold_1_len,scaffold_2_len,good_scores,gap,llr1,llr2,llr3,llr4,cllr1,cllr2,cllr3,cllr4,j-i);
    sprintf(sbuff_,"%d\t%d\t%d\t%d\t%llu\t%d\t>> <> << ><\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t",scaffold_1,scaffold_2,scaffold_1_len,scaffold_2_len,good_scores,gap,llr1,llr2,llr3,llr4,cllr1,cllr2,cllr3,cllr4);
    stream_ << sbuff_;

    /*
    for (; mi<scaffolds.scaffold_mask_segments_.size() && scaffolds.scaffold_mask_segments_[mi].target_id==scaffold_1 ;mi++) {
      stream_ << scaffolds.scaffold_mask_segments_[mi];
    }

    stream_ << "\t";

    for (; mj<scaffolds.scaffold_mask_segments_.size() && scaffolds.scaffold_mask_segments_[mj].target_id==scaffold_2 ;mj++) {
      stream_ << scaffolds.scaffold_mask_segments_[mj];
    }
    */
    stream_ << "\n";
    
  }
  
  };

#endif
