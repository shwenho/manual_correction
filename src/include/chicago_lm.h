#ifndef CHICAGO_LM_H
#define CHICAGO_LM_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/NonLinearOptimization>
#include <picojson.h>
#include <pthread.h>
#include "error_msg.h"
#include "chicago_read.h"
#include "pair_distance_likelihood_model.h"
#include "histogram_provider.h"
#include "serialization_utils.h"

class ChicagoLM : public PairDistanceLM  {
  friend class SVKernelScorer;

public:

  ChicagoLM() { Init(); }
  ChicagoLM(const char *model_json, int64_t n_shotgun = 0) {
    Init();
    SetModel(model_json);
    if (n_shotgun) {
      SetMetagenomic(n_shotgun);
    }
  }

  //
  // Model specific stuff  (override in implementations)
  //


  void fit(const HistogramProvider& data);

  void SetModel(const char *model_json, const bool fill_cache = true) {
      picojson::value v;
      std::string err = picojson::parse(v, model_json);
      if(err != "") {
	std::cerr << "model file " << model_json << " has pico json err: " << err;
	throw std::runtime_error(err);
      }
      std::vector<double> beta_vec =  v.get_vector<double>("beta");
      std::vector<double> alpha_vec = v.get_vector<double>("alpha");
      num_links_ = v.get("N").get<double>();;
      genome_size_ = v.get("G").get<double>();
      prob_noise_ = v.get("pn").get<double>();
      DT_ASSERT(beta_vec.size() == alpha_vec.size(), "alpha and beta must match in size.");
      int size = beta_vec.size();
      alpha_.resize(size);
      beta_.resize(size);
      a_scratch_.resize(size);
      std::copy(beta_vec.begin(), beta_vec.end(), &(beta_(0)));
      std::copy(alpha_vec.begin(), alpha_vec.end(), &(alpha_(0)));
      neg_beta_ = -1.0f * beta_;
      neg_alpha_ = -1.0f * alpha_;
      alpha_beta_ = alpha_ * beta_; // element wise, not dot product
      neg_alpha_div_beta_ = -1.0 * alpha_ / beta_;
      prob_noise_div_genome_size_ = prob_noise_ / genome_size_;
      one_minus_prob_noise_ = 1.0d - prob_noise_;
      mean_num_links_null_coeff_ = num_links_ * prob_noise_ / (genome_size_ * genome_size_);
      one_minus_prob_noise_N_over_G_ = one_minus_prob_noise_ * num_links_ / genome_size_ ;
      //one_minus_prob_noise_N_over_G_ = one_minus_prob_noise_ * num_links_ / genome_size_ ;
      is_metagenomic_=false;
      logG_ = log(genome_size_);
      logN_ = log(num_links_);
      lnFinf_ = log(prob_noise_) - logG_;
    //        self.lnFinf = math.log(self.pn)-math.log(self.G)
      if (fill_cache) {
	fill_caches();
      }
  }

  int sample_distribution() {

    double x;
    double short_innie_prob = 0.2;

    if (false) {
    x = drand48();
    if (x<prob_noise_) {
      return(-1);
    }

    x = drand48();
    if (x<short_innie_prob) {
      return(-2);
    }
    }

    int i=0;
    double rs=alpha_[i];
    x = drand48();
    while (x>rs) {
      i++;
      rs+=alpha_[i];
    }

    x = - log(1.0-drand48())  / beta_[i];

    return(int(x));
  }

  //  inline int32_t NumExponential() { return beta_.rows(); }

  /* void ModelFunctionInserts(const Eigen::VectorXd &inserts, double *likelihood) { */
  /*   // outer product to create matrix with row for each insert */
  /*   // and column for each coefficient */
  /*   m_scratch_ = inserts.transpose() * neg_beta_; */
  /*   m_scratch_.exp(); */
  /*   m_scratch_ = m_scratch_ * alpha_beta_; */
  /*   double result = m_scratch_.sum(); */
  /*   *likelihood = result; */
  /* } */

  // Currently slower with small values than just regular math version below
  void NonNoiseInsertDist(int insert_size, double *likelihood) {
    Eigen::ArrayXd a_scratch ;
    //a_scratch.resize();
    //    pthread_mutex_lock(   &mutex_ );
    a_scratch = insert_size * neg_beta_;
    a_scratch = a_scratch .exp();
    a_scratch *= alpha_beta_;
    *likelihood = a_scratch.sum();
    //pthread_mutex_unlock(   &mutex_ );
  }

  /* // Current fastest way of calculating mixture of exponential probability currently */
  /* // P_{raw}(x)= \sum \alpha_i \beta_i e^{-x \beta_i}  */
  /* // f0 in python */
  /* void ExpLikelihood(int insert_size, double *likelihood) { */
  /*   double *alpha_beta_start = alpha_beta_start_; */
  /*   double *neg_beta_start = neg_beta_start_; */
  /*   double *neg_beta_end = neg_beta_end_; */
  /*   *likelihood = 0.0d; */
  /*   while(neg_beta_start != neg_beta_end) { */
  /*     *likelihood += *alpha_beta_start++ * exp(*neg_beta_start++ * insert_size); */
  /*   } */
  /* } */


  inline void F_good(int insert_size, double *result) {
    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;

    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= neg_alpha_;
    *result = - a_scratch.sum();

    //fprintf(stdout,"F_good\t%g\n",a_scratch.sum());
    //    *result *= one_minus_prob_noise_;
    //*result += prob_noise_div_genome_size_ * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  inline void H_good(int insert_size, double *result) {
    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;
    // 0.5*d*d*self.pn/self.G +
    // (1.0-self.pn) * sum([ -(old_div(alpha[i],beta[i]))*( math.exp(-beta[i]*d) * (beta[i] * d + 1.0)) for i in range(self.nexps) ])
    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= (beta_ * insert_size) + 1;
    a_scratch *= neg_alpha_div_beta_;
    *result = - a_scratch.sum();
    //    fprintf(stdout,"H_good\t%g\n",a_scratch.sum());
    //*result *= one_minus_prob_noise_;
    //*result += prob_noise_div_genome_size_ * 0.5 * insert_size * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  inline void F0(int insert_size, double *result) {
    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;

    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= neg_alpha_;
    *result = a_scratch.sum();
    *result *= one_minus_prob_noise_;
    *result += prob_noise_div_genome_size_ * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  inline void H0(int insert_size, double *result) {
    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;
    // 0.5*d*d*self.pn/self.G +
    // (1.0-self.pn) * sum([ -(old_div(alpha[i],beta[i]))*( math.exp(-beta[i]*d) * (beta[i] * d + 1.0)) for i in range(self.nexps) ])
    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= (beta_ * insert_size) + 1;
    a_scratch *= neg_alpha_div_beta_;
    *result = a_scratch.sum();
    *result *= one_minus_prob_noise_;
    *result += prob_noise_div_genome_size_ * 0.5 * insert_size * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  /// Initialize with default values
  void Init() {
    num_links_ = 0;
    genome_size_ = 0.0d;
    prob_noise_ = 0.0d;
    prob_noise_div_genome_size_ = 0.0d;
    one_minus_prob_noise_ = 0.0d;
    mean_num_links_null_coeff_ = 0.0d;
    beta_.resize(0);
    alpha_.resize(0);
    alpha_beta_.resize(0);
    neg_beta_.resize(0);
    cache_max_ = 400000;
    cache_min_ =-400000;

    // std::cerr << "set cache min and max\n";

    Tcache_.resize(cache_max_-cache_min_);
    Tcache_fill_.resize(cache_max_-cache_min_);
    std::fill(Tcache_fill_.begin(),Tcache_fill_.end(),0);

    TPluscache_.resize(cache_max_-cache_min_);
    TPluscache_fill_.resize(cache_max_-cache_min_);
    std::fill(TPluscache_fill_.begin(),TPluscache_fill_.end(),0);

    Fcache_.resize(cache_max_-cache_min_);
    Fcache_fill_.resize(cache_max_-cache_min_);
    std::fill(Fcache_fill_.begin(),Fcache_fill_.end(),0);

    pthread_mutex_init( &mutex_, NULL);
    pthread_mutex_init( &mutex_T_, NULL);
    pthread_mutex_init( &mutex_Tplus_, NULL);
    pthread_mutex_init( &mutex_F_, NULL);
    pthread_mutex_init( &mutex_A_, NULL);
    pthread_mutex_init( &mutex_P_, NULL);


  }

  void DescriptionString(char* sbuff) {
    sprintf(sbuff,"Chicago Model, N=%d; pn=%g",num_links_,prob_noise_);
  };



  void write_gnuplot_string(std::ostream& fh) const {
    int n = beta_.size();
    fh << num_links_ << "*( (" << prob_noise_ << "/" << genome_size_ << ") + (1-"<< prob_noise_ <<")*(" ;
    int i=0;

    fh << " (" <<     beta_(i)*alpha_(i) << "*exp(-x*"<<beta_(i)<<") "   << ") ";
    for(i=1;i<n-3;i++) {
      fh << "+(" <<         beta_(i)*alpha_(i) << "*exp(-x*"<<beta_(i)<<") "     << ") ";
    }
    fh << "))";
  }

 void print_chicago_json(std::ostream& fh) const {

   double pn = prob_noise_;
   double N  = num_links_;
   double Nn = N*pn;
   double G = genome_size_;

   fh << "{";
   fh << "\"N\": " << N;
   fh << ", " << "\"pn\": " << pn;
   fh << ", " << "\"Nn\": " << Nn;
   fh << ", " << "\"G\": " << G;
   fh << ", " << "\"model_type\": \"chicago\" ";
   fh << ", \"beta\": " ; vec2json(fh,beta_);

   fh << ", " << "\"alpha\": "; vec2json(fh,alpha_);

   fh << ", " << "\"fn\": \"" ; write_gnuplot_string(fh) ; fh << '\"';
   fh << "}\n";
 }

 void set_genome_size(double G) {genome_size_=G;};
 // void set_beta(Eigen::VectorXd beta) {   beta_ = beta; }
 // void set_alpha(Eigen::VectorXd beta) {  alpha_ = alpha; }

private:

 void update_precalculated_values() {

   int size = beta_.size();
   //      alpha_.resize(size);
   //   beta_.resize(size);
   a_scratch_.resize(size);
   //   m_scratch_.resize(size);
   neg_beta_ = -1.0f * beta_;
   neg_alpha_ = -1.0f * alpha_;
   alpha_beta_ = alpha_ * beta_; // element wise, not dot product
   neg_alpha_div_beta_ = -1.0 * alpha_ / beta_;
   prob_noise_div_genome_size_ = prob_noise_ / genome_size_;
   one_minus_prob_noise_ = 1.0d - prob_noise_;
   mean_num_links_null_coeff_ = num_links_ * prob_noise_ / (genome_size_ * genome_size_);
   one_minus_prob_noise_N_over_G_ = one_minus_prob_noise_ * num_links_ / genome_size_ ;

   //one_minus_prob_noise_N_over_G_ = one_minus_prob_noise_ * num_links_ / genome_size_ ;
   is_metagenomic_=false;
   logG_ = log(genome_size_);
   logN_ = log(num_links_);
   lnFinf_ = log(prob_noise_) - logG_;

 }

  // Matrices and vectors of data
  Eigen::ArrayXd  alpha_;       ///< alpha coefficients for exponential model;
  Eigen::ArrayXd  beta_;        ///< beta coefficients for exponential model;
  Eigen::ArrayXd  neg_beta_;    ///< cached negative coefficients for exponential model;
  Eigen::ArrayXd  neg_alpha_;    ///< cached negative coefficients for exponential model;
  Eigen::ArrayXd  neg_alpha_div_beta_;    ///< cached negative coefficients for exponential model;
  Eigen::ArrayXd  alpha_beta_;  ///< cached alpha*beta coefficients for exponential model;
  Eigen::ArrayXd  a_scratch_;   ///< vector for intermediate results
  Eigen::MatrixXd m_scratch_;   ///< Matrix for storing intermediate results;

};

std::ostream& operator<<(std::ostream& out,const ChicagoLM& model) {

  model.print_chicago_json(out);
  //  print_chicago_json(out,model);
  return out;

}

struct ChicagoFunctor {
  typedef double Scalar;

  enum {
    InputsAtCompileTime = Eigen::Dynamic,
    ValuesAtCompileTime = Eigen::Dynamic
  };

  typedef Eigen::Matrix<Scalar,InputsAtCompileTime,1> InputType;
  typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,1> ValueType;
  typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,InputsAtCompileTime> JacobianType;

  int m_inputs, m_values;

  ChicagoFunctor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
  ChicagoFunctor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

  int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const {

    int n = x.size();
    double chisq=0.0;
    Eigen::VectorXd alpha(n-2);

    for(int j =0;j<n-2;j++) {
      alpha(j) = (tanh(x(j))+1.0)/2.0;
    }

    //    std::cout << "size: " << n << "\n";
    //    double pn = (tanh(x(n-2))+1.0)/2.0;
    //double pn = x(n-2);
    //double N  = x(n-1);
    //    double lambda  = x(n-1);
    double B = x(n-1);
    for(int i = 1; i < this->Points.size(); ++i) {
      double d = this->Points[i].x;
      if (d<1000) {fvec(i)=0.0; continue;}
      double f = 0.0;

      for(int j =0;j<n-2;j++) {
  f += alpha(j)*this->beta(j) * exp(-this->beta(j)*d)  ;
      }
      //      f *= N*(1.0-pn)   ;
      f *= B ;
      f+= this->pn_N_over_G ;
      //      f*=N;
      fvec(i) = (this->Points[i].y - f ) / this->Points[i].dy;
      chisq+= fvec(i)*fvec(i);
    }

    double sum_alpha=0.0;
    for(int j =0;j<n-2;j++) {
      sum_alpha += alpha(j);
    }
    sum_alpha -= 1.0;


    sum_alpha *= 100.0;
    //    std::cout << "sum alpha: " << sum_alpha << "\n";
    fvec(0) = (sum_alpha);
    //    fvec(1) = 100.0 *( (pn_N_over_G * G / N) - pn )  ;

    chisq += sum_alpha * sum_alpha;
    //    chisq += fvec(1)*fvec(1);
    //    std::cout << "dp\t" << B << "\t" << chisq << "\n";// << j << "\t" << alpha(i) << "\t" << this->beta(j) << "\n";
    return 0;
  }

  PointVector Points;
  Eigen::VectorXd beta;
  //  Eigen::VectorXd alpha;
  double G;
  double pn_N_over_G;
  //  double N_tmp;

  int inputs() const { return 2; }
  int values() const { return this->Points.size(); } // The number of observations

};


struct FunctorNumericalDiff : Eigen::NumericalDiff<ChicagoFunctor> {};


void ChicagoLM::fit( const HistogramProvider& data ) {
  genome_size_ = data.genome_size();
  double G = genome_size_;
  double pn=0.5;
  //std::cout << "hi\n";
  double N=0.0;
  for( PointVector_const_it it=data.histogram.begin();it!=data.histogram.end();it++) {
    //std::cout << "data:\t" << it->x << "\t" << it->y <<"\t" << it->dy  <<"\n";
    if (it->x>1000) {N += it->y;}
    //    N += it->y;
  }

  int n=10;
  Eigen::VectorXd beta(n) ;
  Eigen::VectorXd alpha(n) ;
  Eigen::VectorXd x(n+2);
  //  beta << 0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000e-05 ;


  const std::vector<double> betaV = { 0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05};
  const std::vector<double> alphaV = {0.2596398458617465, 0.2184877929541886, 0.19276735652678798, 0.06612921087467549, 0.13835425638673426, 0.09578541569209319, 0.028836121703774004, 0.0, 0.0, 0.0};

  for(int i=0;i<n;i++) {
    //    alpha(i) = alphaV[i];
      alpha(i) = 0.5;
    beta(i) = betaV[i];
  }
  alpha = (1.0/alpha.sum())*alpha ;

  FunctorNumericalDiff functor;
  functor.Points = data.histogram;
  functor.beta = beta;
  //  functor.beta = alpha;
  functor.G = G;
  functor.pn_N_over_G = data.histogram.back().y;
  //  std::cout << "starting values: " << x << std::endl;

  while ( functor.pn_N_over_G * G / N > 1.0 ) { N *= 1.5; }
  //  std::cout << "Noise level: " << functor.pn_N_over_G  << "\t" << data.histogram.back().x <<"\t" <<data.histogram.back().y << std::endl;

  //  functor.N = N ;

  for(int i=0;i<n;i++) {
    x(i) = atanh( (alpha(i)+1.0e-9)*2.0 - 1.0) ;
    //std::cout << "atanh: " << alpha(i) << "\t" << x(i) << "\n";
    //  alpha(i) = 1.0;
    //beta(i) = betaV[i];
  }
  x(n) = atanh( (pn )*2.0 - 1.0);
  //  x(n) = 0.1;
  x(n+1) = N;
  //  x(n+2) = 1.0;

  //  std::cout << "starting values: " << x << std::endl;
  Eigen::LevenbergMarquardt<FunctorNumericalDiff> lm(functor);
  Eigen::LevenbergMarquardtSpace::Status status = lm.minimize(x);
  //std::cout << "status: " << status << std::endl;
  // std::cout << "ending values: " << x << std::endl;

  //  std::cout << "constraint penalty: " << 100.0 *( (functor.pn_N_over_G * G / N) - pn ) << std::endl;


  for(int i=0;i<n;i++) {    alpha(i) = (tanh(x(i))+1.0)/2.0;  }
  pn = (tanh(x(n))+1.0)/2.0 ;
  //pn = x(n);
  double B = x(n+1);
  double A = functor.pn_N_over_G;
  double z = A / B;

  //  std::cout << "A:\t" <<A<<"\n";
  //std::cout << "B:\t" <<B<<"\n";
  //std::cout << "G:\t" <<G<<"\n";
  //std::cout << "z:\t" <<z<<"\n";
  //std::cout << "G*z:\t" <<G*z<<"\n";

  //  if      (G*z < 0.01 ) { std::cout << "Apparently very low noise rate. Adjusting genome size \n"; G = 0.01 / z; }
  //else if (G*z > 100.0) { std::cout << "Genome size estimate too high?. Adjusting genome size \n"; G = 100.0 / z;}

  pn = G*z / (1.0 + G*z);
  N =  B / (1.0-pn) ;

  prob_noise_ = pn;
  genome_size_ = G;
  num_links_ = N;
  beta_ = beta;
  alpha_= alpha;

  //  print_chicago_json(std::cout);
  //  std::cout << "\n";
  //std::cout.flush();

  update_precalculated_values();
}

#endif // CHICAGO_LM_H
