#ifndef CHICAGO_LINKS_H
#define CHICAGO_LINKS_H

#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include "htslib/hts.h"
#include "htslib/sam.h"
#include "common.h"
#include "error_msg.h"
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <ios>
#include "chicago_read.h"
#include "pair_distance_likelihood_model.h"
#include "paired_read_likelihood_model.h"
#include "segment_set.h"
#include "bam_header_info.h"
#include "chicago_scaffolds.h"
#include "chicago_lm.h"
//#include "bincounts_heap.h"
//#include "scaffold_breaker.h"
//#include "link_range_iterator.h"
#include "../include/pair_hash.h"
#include "timsort.hpp"

// bitflags for status in read object
enum SortState {
  UNSORTED         = 1 << 0,
  QUAD             = 1 << 1,
  CONTIGS          = 1 << 2,
  SCAFFOLDS        = 1 << 3,
  BROKEN_CONTIGS   = 1 << 4,
};



ChicagoLM defaultModel;

const std::string empty_string = std::string();

uint64_t qk(uint64_t x, uint64_t y) {
  uint64_t result=0;
  uint8_t i;
  for(i=0;i<32;i++) {
    result |= ( (x&1) << 2*i) | ( (y&1)<<((2*i)+1) );
    x=x>>1;
    y=y>>1;
  }
  return result;  
}

uint64_t qk(Read r, std::vector<int64_t> *tstarts) {
    uint64_t result=0;
    uint64_t x,y;
    x = (*tstarts)[r.target_1] +  r.pos_1;
    y = (*tstarts)[r.target_2] +  r.pos_2;

    uint8_t i;
    for(i=0;i<32;i++) {
      //printf("x:\t%d\t%d\t%d\t%d\t%d\t%d\t%d.\n",i,x,y,*result,(x&1) << 2*i,(y&1)<<((2*i)+1),*result) ;
      result |= ( (x&1) << 2*i) | ( (y&1)<<((2*i)+1) );
      x=x>>1;
      y=y>>1;
    }
    //    std::cout << "z:\t" << (*tstarts)[r.target_1] +  r.pos_1 << "\t" <<  (*tstarts)[r.target_2] +  r.pos_2 << "\t" <<  result << '\n' ;// << endl;
    
    return result;
}



void kq(uint64_t b,uint64_t *x, uint64_t *y) {
  //    uint64_t result=0;
    *x=0;
    *y=0;

    uint8_t i;
    for(i=0;i<32;i++) {
      //printf("x:\t%d\t%d\t%d\t%d\t%d\t%d\t%d.\n",i,x,y,*result,(x&1) << 2*i,(y&1)<<((2*i)+1),*result) ;
      *x |= ((b>>2*i)&1)<<i ;
      *y |= ((b>>(2*i+1))&1)<<i ;
    }
    //    std::cout << "z:\t" << (*tstarts)[r.target_1] +  r.pos_1 << "\t" <<  (*tstarts)[r.target_2] +  r.pos_2 << "\t" <<  result << '\n' ;// << endl;
}

class BinIndex {
  std::vector<Read>              *readsp_;   
  std::vector<int64_t>           *target_startsp_;
  int                            binbits_;
  uint64_t                       binmask_;
  uint64_t                       binincr_;
  std::map<std::pair<int32_t,int32_t>, std::pair<int32_t,int32_t> > index_;

 public:
  void Setup(int b,std::vector<Read> * reads,std::vector<int64_t> * target_starts) {
    binbits_=b;
    binmask_  = 1;
    binincr_ = ((binmask_<<binbits_));
    binmask_ = ~((binmask_<<binbits_)-1);
    readsp_ = reads;
    target_startsp_= target_starts;
  }


  /*
    (a,b) ----+
      |       |
      |       |
      +-----(c,d)
   */
  void IntervalPairToListOfRanges(uint64_t a, uint64_t b, uint64_t c, uint64_t d, std::vector<std::pair<int32_t,int32_t>> *range_list) {
    //std::vector<std::pair<int32_t,int32_t>> result;
    
     uint64_t z;
     uint64_t x;
     uint64_t y;
     if (b<a) {
       z=a; a=b; b=z; 
       z=c; c=d; d=z; 
     } //TODO:  may beed to switch a<->b and c<->d if b<a ... 

     if (c<a) { z=a; a=c; c=z; }
     if (d<b) { z=b; b=d; d=z; }

     std::cout << "(a,b) (c,d):" << a << "\t" <<  b << "\t" <<  c << "\t" <<  d << "\n"; 

     uint64_t binAB = qk(a,b) & binmask_;
     uint64_t binCD = qk(c,d) & binmask_;

     std::map< std::pair<int32_t,int32_t>, std::pair<int32_t,int32_t>>::const_iterator s;

     range_list->clear()  ;  // @todo: switch to a thread-safe method of doing this.

     if (binCD<binAB) { x=binAB; binAB=binCD; binCD=x; }

     std::cout << "bin range:" << binAB << "\t" <<binCD << "\n"; // binincr_ << "\t" << x << "\t" << y << "\t" << s->second.first << "\t" << s->second.second <<"\n"; // << "\t" << x << "\t" << y << "\t" << index_.find( std::make_pair(x ,y) ) << "\n"

     while (binAB<=binCD) {

      kq(binAB,&x,&y);
      //      std::cout << "bin:" << binAB << "\t" << x << "\t" << y << "\t" << index_.find( std::make_pair(x ,y) ) << "\n";      
      //      std::cout << "bin:" << x << "\t" << y <<"\n"; // << "\t" << x << "\t" << y << "\t" << index_.find( std::make_pair(x ,y) ) << "\n";

      std::cout << "(a,b) (c,d):" << x << "\t" <<  y << "\n"; // 
      s = index_.find( std::make_pair(x ,y) ) ;
      if (s!=index_.end()) {
	std::cout << "bin:" << binAB << "\t" << binincr_ << "\t" << x << "\t" << y << "\t" << s->second.first << "\t" << s->second.second <<"\n"; // << "\t" << x << "\t" << y << "\t" << index_.find( std::make_pair(x ,y) ) << "\n"
	//      std::cout << "bin:" << s->second.first << "\t" << s->second.second <<"\n"; // << "\t" << x << "\t" << y << "\t" << index_.find( std::make_pair(x ,y) ) << "\n";
	range_list->push_back( std::make_pair(s->second.first,s->second.second ) );
      }
      binAB+=binincr_;  // #TODO:  fix this ;  if the rectangle is split between bins, this can lead to checking more bins than necessary.
    }
     //     return result;
  }

  void ReIndex() {
    uint nr = readsp_->size();
    //binmask  = 1;
    //binmask = ~((binmask<<binbits_)-1);
    //    uint64_t binmask =  ~((1<<binbits_) - 1)  ;
    uint64_t current_bin=0;
    uint64_t last_bin=0;
    uint64_t x,y; //,x2,y2;
    int first_i=0;
    Read r;
    uint i=0;
    r = (*readsp_)[i];
    first_i = i;
    last_bin = qk( r, target_startsp_ ) & binmask_ ;    
    kq(last_bin,&x,&y);

    std::cout << binbits_ << "\n";
    std::cout << (1<<binbits_) << "\n";
    std::cout << ((1<<binbits_)-1) << "\n";
    std::cout << ~((1<<binbits_)-1) << "\n";
    std::cout << binmask_ << "\n";
    for(i=1;i<nr;i++) {
      r = (*readsp_)[i];
      //std::cout << "j:\t" << i << r.pos_1 << '\n' ;// << endl;
      current_bin = qk( r, target_startsp_ ) & binmask_ ;
      if (current_bin<last_bin) {
	// somethign went wrong -- expecting sorted;
	std::cout << "WTF? expected sorted read buffer.\n" ;
      } else if ( current_bin>last_bin ) {
	//	std::cout << "binrange\t" << last_bin << "\t" << x <<"\t" << y << "\t"<< first_i << "\t" << i << "\t" << i-first_i <<'\n' ;

	index_.insert(std::make_pair(
				     std::make_pair(x      ,y),
				     std::make_pair(first_i,i)
				     ));

	//index_[ std::pair( x,y ) ] = std::pair( first_i,i )
	//std::cout << "binrange" << last_bin << "\t" << x <<"\t" << y << "\t"<< first_i << "\t" << i << '\n' ;
	first_i = i;
      }
      last_bin = current_bin ;
      kq(last_bin,&x,&y);
    }
    //    std::cout << "binrange\t" << last_bin << "\t" << x <<"\t" << y << "\t"<< first_i << "\t" << i << "\t" << i-first_i <<'\n' ;
    //    index_.resize(index_.size()+1);
    index_.insert(std::make_pair(
				     std::make_pair(x      ,y),
				     std::make_pair(first_i,i)
				     ));

    
  }


  // private:
  //DT_DISABLE_COPY(BinIndex);

};

//void BinIndex::Setup


struct RSDelta {
  int     x ;
  double dy ;
  int     z ;
  int   row;
  int   col;

RSDelta(int x, double dy, int z) : x(x), dy(dy), z(z), row(0), col(0) {}
RSDelta(int x, double dy, int z, int row, int col) : x(x), dy(dy), z(z), row(row), col(col) {}

  bool operator < (const RSDelta& r) const {
    return(x<r.x);
  }

};

struct QuadSorter {
  std::vector<int64_t> *tstarts;
  bool operator() (Read i, Read j) { return ( qk(i,tstarts) < qk(j,tstarts) ); }
} ;

std::vector<int> globaldummyx;


struct ScaffoldSorter {
  const std::vector<int>& contig_scaffold ;
  ScaffoldSorter() : contig_scaffold( globaldummyx ) {};
  ScaffoldSorter( const std::vector<int>& cs ) : contig_scaffold( cs ) {};

  void check_range(int i) {
    if ( i < -1)                      { 
      std::cout << "bad i =" <<i<<"\n";
      throw std::runtime_error("bad i, <-1"); }
    if ( i >= int(contig_scaffold.size())) { 
      std::cout << "bad i =" <<i<<"\n";
      throw std::runtime_error("bad i, >=size"); }
  }


  bool operator() (Read i, Read j) { 
    int s1,s2,s3,s4;

    check_range(i.target_1);
    check_range(i.target_2);
    check_range(j.target_1);
    check_range(j.target_2);

    /*
    if ((i.target_1 <-1) || (i.target_1 > contig_scaffold.size())) { std::cout << "target id out of range:\t" << i.target_1 << "\t" << contig_scaffold.size() << "\n"; throw std::runtime_error("bad target id 1"); }
    if ((i.target_2 <-1) || (i.target_2 > contig_scaffold.size())) { std::cout << "target id out of range:\t" << i.target_2 << "\t" << contig_scaffold.size() << "\n"; throw std::runtime_error("bad target id 2"); }
    if ((j.target_1 <-1) || (j.target_1 > contig_scaffold.size())) { std::cout << "target id out of range:\t" << j.target_1 << "\t" << contig_scaffold.size() << "\n"; throw std::runtime_error("bad target id 3"); }
    if ((j.target_2 <-1) || (j.target_2 > contig_scaffold.size())) { std::cout << "target id out of range:\t" << j.target_2 << "\t" << contig_scaffold.size() << "\n"; throw std::runtime_error("bad target id 4"); }
    */

    if (i.target_1==-1) { s1=-1;} else { s1 = contig_scaffold[i.target_1]; }
    if (i.target_2==-1) { s2=-1;} else { s2 = contig_scaffold[i.target_2]; }
    if (s1>s2) {std::swap(s1,s2);}

    if (j.target_1==-1) { s3=-1;} else { s3 = contig_scaffold[j.target_1]; }
    if (j.target_2==-1) { s4=-1;} else { s4 = contig_scaffold[j.target_2]; }
    if (s3>s4) {std::swap(s3,s4);}

    if (s1!=s3) {
      return ( s1<s3 );
    } else if ( s2 != s4 ) {
      return ( s2 < s4 ); 
    } else {
      if (i.target_1!=j.target_1) {
	return ( i.target_1 < j.target_1 );
      } else if ( i.target_2 != j.target_2 ) {
	return ( i.target_2 < j.target_2 ); 
      } else {
	return ( i.pos_1 < j.pos_1 );
      }
    }
  }
} ;


struct PairSorter {
  //  std::vector<int64_t> *tstarts;
  bool operator() (Read i, Read j) { 
    if (i.target_1!=j.target_1) {
      return ( i.target_1 < j.target_1 );
    } else if ( i.target_2 != j.target_2 ) {
      return ( i.target_2 < j.target_2 ); 
    } else {
      return ( i.pos_1 < j.pos_1 );
    }
  }
} ;

class ChicagoLinks {



public:

  bool isScaffoldSorted()  const   { return (sorting_==SCAFFOLDS);        }
  bool isBrokenContigSorted() const { return (sorting_==BROKEN_CONTIGS);  }
  bool isContigSorted() const {       return (sorting_==CONTIGS);         }

  size_t size() {return reads_.size();}
  /// Map from an id to a range in data vector
  typedef std::map<int32_t, std::pair<int32_t,int32_t> > IndexRangeMap;

  ChicagoLinks() : min_mapq_(0) {}

  int8_t &min_mapq() { return min_mapq_; }

  void SwitchToContigCoordinates(const PathSet& pathset) {
    Read *r;
    uint32_t i;
    Base b1,b2;
    bool flipped1;
    bool flipped2;
    for(i=0;i<reads_.size();i++){
      r = &reads_[i];
      pathset.ContigCoord(Base(r->target_1,r->pos_1),&b1,&flipped1);
      pathset.ContigCoord(Base(r->target_2,r->pos_2),&b2,&flipped2);
      //fprintf(stdout,"re-coordmap:\t(%d,%d)\t(%d,%d)\t-> (%d,%d)\t(%d,%d) \n",r->target_1,r->pos_1,r->target_2,r->pos_2,b1.target_id,b1.x,b2.target_id,b2.x);
      r->target_1 = b1.target_id ;
      r->target_2 = b2.target_id ;
      r->pos_1    = b1.x         ;
      r->pos_2    = b2.x         ;

      if (flipped1) { r->flip_read1(); };
      if (flipped2) { r->flip_read2(); };

      if ((r->target_1 > r->target_2) || ((r->target_1==r->target_2) && (r->pos_1 > r->pos_2)) ) { r->swap();}
    }
    SortByContigs();
    sorting_=CONTIGS;

    //    masked_segments_.SwitchToContigCoordinates(pathset);
  }

  void summary( std::ostream& of ) {
    Read* r;
    std::map<int,int> lc;
    for (uint i=0;i<reads_.size();i++) {
      r=&reads_[i];
      lc[r->libid]++;
    }


    of << "Readparirs by library:\n";
    for( std::map<int,int>::const_iterator i=lc.begin();i!=lc.end();i++) {
      of << "libid " << i->first << "\t" <<i->second<<"\t" << *(lib_models_[i->first]) <<"\n";
    }

  }

  void SwitchToBrokenContigCoordinates(const PathSet& pathset) {
    Read *r;
    uint32_t i;
    Base b1,b2;
    bool flipped1;
    bool flipped2;
    for(i=0;i<reads_.size();i++){
      r = &reads_[i];
      pathset.ScaffoldCoord(Base(r->target_1,r->pos_1),&b1,&flipped1,true);
      pathset.ScaffoldCoord(Base(r->target_2,r->pos_2),&b2,&flipped2,true);
      //fprintf(stdout,"coordmap:\t(%d,%d)\t(%d,%d)\t-> (%d,%d)\t(%d,%d) \n",r->target_1,r->pos_1,r->target_2,r->pos_2,b1.target_id,b1.x,b2.target_id,b2.x);
      if(b1.x < 0 && b1.x != -1) {
	//std::cerr << "r1: " << r->pos_1 << " new: " << b1.x << " target: " << b1.target_id << "\n";
	assert(0);
      }
      if(b2.x < 0 && b2.x != -1) {
	//std::cerr << "r2: " << r->pos_2 << " new: " << b2.x << " target: " << b2.target_id <<"\n";
	assert(0);
      }

      r->target_1 = b1.target_id ;
      r->target_2 = b2.target_id ;
      r->pos_1    = b1.x         ;
      r->pos_2    = b2.x         ;
      
      if (flipped1) { r->flip_read1(); };
      if (flipped2) { r->flip_read2(); };

      if ((r->target_1 > r->target_2) || ((r->target_1==r->target_2) && (r->pos_1 > r->pos_2)) ) { r->swap();}
    }


    fprintf(stderr,"sort to new coords:\n");
    SortByScaffolds(pathset);
    sorting_=BROKEN_CONTIGS;
    //XXX

    //    masked_segments_.SwitchToScaffoldCoordinates(pathset);

  }


  void SwitchToScaffoldCoordinates(const PathSet& pathset) {
    Read *r;
    uint32_t i;
    Base b1,b2;
    bool flipped1;
    bool flipped2;
    for(i=0;i<reads_.size();i++){
      r = &reads_[i];
      pathset.ScaffoldCoord(Base(r->target_1,r->pos_1),&b1,&flipped1);
      pathset.ScaffoldCoord(Base(r->target_2,r->pos_2),&b2,&flipped2);
      //fprintf(stdout,"coordmap:\t(%d,%d)\t(%d,%d)\t-> (%d,%d)\t(%d,%d) \n",r->target_1,r->pos_1,r->target_2,r->pos_2,b1.target_id,b1.x,b2.target_id,b2.x);
      r->target_1 = b1.target_id ;
      r->target_2 = b2.target_id ;
      r->pos_1    = b1.x         ;
      r->pos_2    = b2.x         ;

      if (flipped1) { r->flip_read1(); };
      if (flipped2) { r->flip_read2(); };

      if ((r->target_1 > r->target_2) || ((r->target_1==r->target_2) && (r->pos_1 > r->pos_2)) ) { r->swap();}
    }
    fprintf(stderr,"sort to new coords:\n");
    SortByContigs();
    sorting_=SCAFFOLDS;
    //XXX

    //    masked_segments_.SwitchToScaffoldCoordinates(pathset);

  }
  //  bool compare_function( Read i, Read j) {return ( QuadKey(i) < QuadKey(j) ) ;}
  
  void dumpBitInterleavedCoords() {

    int i;
    Read readi ;
    int nr=reads_.size();
    for(i=0;i<nr;i++) {

      //INFO("Opening file %s.\n", bam_file.c_str());
      readi = reads_[i];
      
      int x,y;
      header_.GetTargetStart(readi.target_1,&x); x += readi.pos_1 ;
      header_.GetTargetStart(readi.target_2,&y); y += readi.pos_2 ;

      //int x = target_starts_[readi.target_1] +  readi.pos_1;
      //int y = target_starts_[readi.target_2] +  readi.pos_2;
      //uint64_t r=0;
      //QuadKey(readi,&r);
      //      printf("i:\t%d\t%d\t%d\t%d\t%d\t%d.\n",i,readi.target_1,readi.target_2,x,y,QuadKey(readi));
      std::cout << "i:\t" << x << "\t" << y << "\t" <<  QuadKey(readi) << "\t" << qk(readi,&header_.target_starts_ )<< '\n' ;// << endl;
    }
  }

  uint64_t QuadKey(Read r) {
    uint64_t result=0;
    int32_t x1,y1;
    uint64_t x,y;
    header_.GetTargetStart(r.target_1,&x1); x = x1 + r.pos_1 ;
    header_.GetTargetStart(r.target_2,&y1); y = y1 + r.pos_2 ;

    //    x = target_starts_[r.target_1] +  r.pos_1;
    //y = target_starts_[r.target_2] +  r.pos_2;
    //x = r.pos_1;
    //y = r.pos_2;

    uint8_t i;
    for(i=0;i<32;i++) {
      //printf("x:\t%d\t%d\t%d\t%d\t%d\t%d\t%d.\n",i,x,y,*result,(x&1) << 2*i,(y&1)<<((2*i)+1),*result) ;
      //result |= ( ((x>>i)&1) << 2*i) | ( ((y>>i)&1)<<((2*i)+1) );
      result |= ( (x&1) << 2*i) | ( (y&1)<<((2*i)+1) );
      x=x>>1;
      y=y>>1;
    }
    return result;
  }

  void SetMaskedFlags( const SegmentSet& masked_segments ) {

    //std::sort(masked_segments_.begin(),masked_segments_.end());
    
    for(std::vector<Read>::iterator it = reads_.begin(); it != reads_.end(); ++it) {
      //      fprintf(stdout,"test pair pair: (%d,%d),(%d,%d)\n",it->target_1,it->pos_1,it->target_2,it->pos_2);
      if (masked_segments.isCovered(it)) {
	//	fprintf(stdout,"masked read pair: (%d,%d),(%d,%d)\n",it->target_1,it->pos_1,it->target_2,it->pos_2);
	
	it->flags |= MASKED   ; 
	it->flags &= ~LINK_OK ;

      } else {
	//	fprintf(stdout,"OK\n");
      }
    }
  }

  void PrintModelInfo(  ) {
    uint i;
    fprintf(stdout,"set models:\n");
    for (i=0;i<lib_models_.size();i++) {
      std::cout << *(lib_models_[i]) << "\n";      
    }
  }

  void SetModelsVector(  std::vector<PairedReadLikelihoodModel *> models ) {
    uint32_t i;
    lib_models_ = models;

    libraries_.resize(models.size());
    for(i=0;i<models.size();i++) {
      libraries_[i]=i;
    }

    PrintModelInfo();
  }

  void UpdateLibinfo(PairedReadLikelihoodModel * model, const int libid) {
    /// Lib info stuff
    
    // Grow the vector of model objects, if it doesn't already go up to libid
    if (uint(libid+1) > lib_models_.size()) {
      lib_models_.resize(libid+1);
    }

    // If this libid isn't already in the list of libraries, add it.
    if ((std::find(libraries_.begin(),libraries_.end(),libid)==libraries_.end())) { 
      libraries_.push_back(libid); 
      //std::cout << "libraries:" <<"\n";
      for (uint i=0;i<libraries_.size();i++) {
	INFO("Library %d.\n", libraries_[i]);
	//	std::cout << "\t" << libraries_[i] <<"\n";
      }
    }
    //INFO("Resized models\n");
    lib_models_[libid] = model ;
    //INFO("Stored model\n");
        
  }

  void PopulateFromTable( const std::string & file_name, const bool quadSort = true, PairedReadLikelihoodModel * model=&defaultModel , const int libid=0, const std::string & restore_file = empty_string  ) {
    int x,y;

    if (! header_.initialized_) {
      ErrorMsg::Error("When loading from a table, you have to put it after a bamfile in the list.\n");      
    }

    UpdateLibinfo(model,libid);
    int res,pos_1,pos_2,separation;

    char target_name_1[NAME_BUFFER_SIZE];
    char target_name_2[NAME_BUFFER_SIZE];
    char st1,st2;
    int32_t tid1,tid2;
    
    std::ifstream in(file_name.c_str());
    if (!in.good()) {
      ErrorMsg::Error("Couldn't open file %s\n", file_name.c_str());
    }
    std::cout << "Parsing read table " << file_name.c_str() << "\n";

    std::string line;
    while (in.good()) {
      std::getline(in, line);
      /*
	Scaffold514325  95      -       Scaffold388971  108     -       271
	Scaffold120435  384     -       Scaffold364841  990     +       181
	Scaffold359978  62      +       Scaffold158791  2306    +       827
	Scaffold251687  224     +       Scaffold436320  54      -       129
      */
      res = sscanf(line.c_str(), "%s\t%d\t%c\t%s\t%d\t%c\t%d", target_name_1, &pos_1, &st1,
		                                               target_name_2, &pos_2, &st2,
		                                                &separation                    );
      
      if (!in.good()) { break; }//ErrorMsg::Error("Not in good: %s\n", line.c_str()); }
      if (res != 7) { ErrorMsg::Error("Expecting 6 arguments to match but got %d on line '%s'\n", res, line.c_str()); }

      header_.GetTargetId(target_name_1,&tid1);
      header_.GetTargetId(target_name_2,&tid2);

      if (tid1==-1 || tid2==-1) {continue;}

      reads_.resize(reads_.size()+1);
      struct Read &read = reads_.back();
      read.target_1     = tid1 ;
      read.pos_1        = pos_1 ;
      read.sam_flag_1   = 0 ; //b->core.flag;
      read.size_1       = separation ; // b->core.l_qseq;
      read.mapq_1       = 0 ; //b->core.qual;
      
      read.target_2     = tid2 ; 
      read.pos_2        = pos_2 ;
      read.size_2       = 100 ; // b->core.l_qseq;
      
      if (st1=='-') { read.sam_flag_1 |= 16; }
      if (st2=='-') { read.sam_flag_1 |= 32; }
      
      read.libid = libid;
      read.flags |= LINK_OK ; 

      header_.GetTargetStart(read.target_1,&x); x += read.pos_1 ;
      header_.GetTargetStart(read.target_2,&y); y += read.pos_2 ;
      if (x>y) { read.swap(); }      

      //      std::cout << tid1 <<"\t" << tid2 << "\t" << target_name_1 << ","  << pos_1 << ","  << st1 << "\t"  << target_name_2 << ","  << pos_2 << ","  << st2 << "\t"  << separation << "\t" << read.sam_flag_1  << "\n" ;


    }
    
    std::cout << "nread pairs:\t" << reads_.size() <<"\n";

  }
   
 

  void PopulateFromBam(const std::string & bam_file, const bool quadSort = true, PairedReadLikelihoodModel * model=&defaultModel , const int libid=0, const std::string & restore_file = empty_string ) {

    
    UpdateLibinfo(model,libid);

    /// Reserve space for reads
    //    if (num_reads_guess>0) {  reads_.reserve( reads_.size() + num_reads_guess );  }
    fprintf(stdout,"#reads_ size,capacity,max_size: %lu %lu %lu\n\n",reads_.size(), reads_.capacity(), reads_.max_size());
    fflush(stdout);

    /// BAM
    bam1_t *b = bam_init1();
  
    INFO("Opening file %s.\n", bam_file.c_str());
    htsFile *bf = hts_open(bam_file.c_str(), "rb");
    assert(bf);
    
    // Parse out the target names from the header
    bam_hdr_t *header = sam_hdr_read(bf);
    header_.Update( header );

    if (restore_file.compare(empty_string)!=0) {
      fprintf(stdout,"restore from %s\n",restore_file.c_str());
      read_reads(restore_file);
      fflush(stdout);
    } else {

    // Parse out the individual reads
    int total_count = 0, unmapped_count = 0, read2_count = 0;
    int bad_mapq = 0, duplicate = 0;
    int32_t curr_id = 0;
    uint32_t curr_id_start = 0;
    uint32_t curr_id_end = 0;
    int x,y;


    while(sam_read1(bf, header, b) > 0) {
      total_count++;
        if (b->core.flag & BAM_FUNMAP || b->core.flag & BAM_FMUNMAP){
            unmapped_count++;
            continue;
        }
        // Ignore duplicates
        if (b->core.flag & BAM_FDUP){
            ++duplicate;
            continue;
        }
        // Enforce minimum MAPQ constraints only for both reads
        uint8_t *tag = bam_aux_get(b, "MQ");
        if(tag == NULL)
            throw std::runtime_error("No MQ tag in shotgun BAM file. Please realign with DT SNAP.");
        if (b->core.qual < min_mapq_ || bam_aux2i(tag) < min_mapq_){
            ++bad_mapq;
            continue;
        }
        if (b->core.flag & BAM_FREAD2) {
	read2_count++;
            continue;
        }

      // check to see if we're at end of target id (target) and store range if so
      if (curr_id != b->core.tid) {
	// only insert if we saw some mapping reads
	if (curr_id_start != curr_id_end) {
	  target_map_.insert(std::make_pair(curr_id, std::make_pair(curr_id_start, curr_id_end)));
	  curr_id = b->core.tid;
	  curr_id_start = curr_id_end = reads_.size();
	}
      }

      curr_id_end++;       // Add this read to range for target id

      //      if (reads_.size()%100000==0) {fprintf(stdout,"\033[F\033[J#stored %f million reads                           \n",float(reads_.size())/1000000.0);}
      //if (reads_.size()%100000==0) {fprintf(stdout,"#stored %f million reads                           \n\033[1A33[2K",float(reads_.size())/1000000.0);}
      if (reads_.size()%100000==0) {fprintf(stdout,"#stored %f million reads                           \r",float(reads_.size())/1000000.0); fflush(stdout);}

      reads_.resize(reads_.size()+1);
      struct Read &read = reads_.back();
      read.target_1     = b->core.tid;
      read.pos_1        = b->core.pos;
      read.sam_flag_1   = b->core.flag;
      read.size_1       = b->core.l_qseq;
      read.mapq_1       = b->core.qual;
      
      read.target_2     = b->core.mtid;
      read.pos_2        = b->core.mpos;
      
      read.libid = libid;

      tag = bam_aux_get(b, "xs");
      if (tag != NULL) { 
	read.size_2       = bam_aux2i(tag); 
      }
      tag = bam_aux_get(b, "MQ");
      assert(tag);
      read.mapq_2       = bam_aux2i(tag);

      header_.GetTargetStart(read.target_1,&x); x += read.pos_1 ;
      header_.GetTargetStart(read.target_2,&y); y += read.pos_2 ;
      if (x>y) { read.swap(); }

      // tags
      tag = bam_aux_get(b, "NM");
      assert(tag);
      read.edit_dist_1 = bam_aux2i(tag);
      
      tag = bam_aux_get(b, "xj"); 
      //assert(tag);

      bool has_junction = false;
      if (tag != NULL) {
	char *xj_tag = bam_aux2Z(tag);
	if(xj_tag != NULL) {
	  has_junction = (*xj_tag) == 'T';
	}
      }
      
      tag = bam_aux_get(b, "xt");
      //assert(tag);
      if (tag != NULL) {
	char *xt_tag = bam_aux2Z(tag);
	if(xt_tag != NULL) {
	  read.type = *xt_tag;
	}
      }

      // set flags for processing later
      if (has_junction) { 
	read.flags |= JUNCTION_FOUND ; 
      }
      if (read.sam_flag_1 & BAM_FDUP) {
	read.flags |= DUPLICATE ; 
	duplicate++;
      }

      if (read.mapq_1 >= min_mapq_ && read.mapq_2 >= min_mapq_) {
	if ((read.sam_flag_1 & BAM_FDUP) == 0) {
	  read.flags |= LINK_OK ; 
	}
      }
      else {
	bad_mapq++;
      }
    }

    //Insert our last pair
    target_map_.insert(std::make_pair(curr_id, std::make_pair(curr_id_start, curr_id_end)));
    INFO("Got %d reads %d unmapped %d read2 %d bad mapq %d duplicate\n", 
	 total_count, unmapped_count, read2_count, bad_mapq, duplicate);
    bam_destroy1(b);
    bam_hdr_destroy(header);
    hts_close(bf);
    }

    if (quadSort) {
      //
    } else {
      //
    }
    sorting_=UNSORTED;
  }

  void SortByQuad() {
      QuadSorter qsr;
      qsr.tstarts = &header_.target_starts_;
      std::sort(reads_.begin(),reads_.end(),qsr);

      bin_index_.Setup(36,&reads_,&header_.target_starts_);
      bin_index_.ReIndex();
      sorting_ = QUAD;
  }

  void SortByContigs() {
      PairSorter psr;
      gfx::timsort(reads_.begin(),reads_.end(),psr);
      sorting_ = CONTIGS;
  }

  void SortByScaffolds(const PathSet& scaffolds) {
    //    boost::timer t;

    //std::sort(reads_.begin(),reads_.end());
    //    SortByQuad();
    ScaffoldSorter psr( scaffolds.ContigScaffoldMap() );
    //    psr.contig_scaffold=scaffolds.ContigScaffoldMap();
    gfx::timsort(reads_.begin(),reads_.end(),psr);
    sorting_ = CONTIGS;

    //    std::cerr << "sort took: " << t.elapsed() << std::endl;
  }

  //#define tmp_score_debug_level 2
  void ContigPairLLRCorrected( ContigPair *cp, const PathSet& scaffolds, unsigned long long mini, unsigned long long maxi, double *result, int max_sep=10000000 ) const {

    ContigPairLLR ( cp, mini, maxi, result, max_sep);  // the is the contribution from the reads
    //
    // bailing out here if the score is horrible already
    //
    if (*result < -200.0) {return ; }

    double uncorrected = *result;
    double dr,dl,libid;

    //
    // Apply depth discrepancy penalty: do nothing unless metagenomics flaggged for at least one model
    //
    PairedReadLikelihoodModel* needed_cmodel = NULL;
    bool do_depth_discrep_penalty=false;

    // for debugging (putting into stringstream in loop very slow)
    std::ostringstream os;
    std::vector<float> all_dls_a;
    std::vector<float> all_dls_b;
    std::vector<float> all_dls_c;
    std::vector<float> all_pos_a;
    std::vector<float> all_pos_b;
    std::vector<float> all_pos_c;

    // length of masked segments
    int a_size=0;
    int b_size=0;
    int c_size=0;
    for (uint n=0; n<libraries_.size(); n++) {
      libid = libraries_[n];

      if (lib_models_[libid]->NeedDepthDiscrep(cp)) {
	needed_cmodel = lib_models_[libid];
	break;
      }
    }

    if (do_depth_discrep_penalty) {
      dl=0.0;
      needed_cmodel->LLRDepthDiscrep(cp, &dl);
      *result -= dl;
#if tmp_score_debug_level > 1
      if (dl > 0) {
	//std::cerr << "0:\t<< " << *cp << "\t/\t" << mi << "," << mj << "\t" << uncorrected << " -> " << *result << "\tlibid: " << libid << "\tdl: " << dl << "\n";

      }
#endif
    }

    int i,j;
    int mi,mj;
    int target_1 = cp->target_1;
    int target_2 = cp->target_2;
    ContigPair gap_info(0,0,0);
    int gap;
    int n;
    //now correct for the masked segments in the two scaffolds:
    if (target_1<int(scaffolds.scaffold_mask_segments_index_.size())) {
      mi = scaffolds.scaffold_mask_segments_index_[target_1];
      assert(mi>=0);
      assert(mi<=int(scaffolds.scaffold_mask_segments_.size()));
    } else {
      mi = scaffolds.scaffold_mask_segments_.size();
    }
    
    if (target_2<int(scaffolds.scaffold_mask_segments_index_.size())) {
      mj = scaffolds.scaffold_mask_segments_index_[target_2];
      assert(mj>=0);
      assert(mj<=int(scaffolds.scaffold_mask_segments_.size()));
    } else {
      mj = scaffolds.scaffold_mask_segments_.size();
    }    
  
#if tmp_score_debug_level>1
    //os << *cp << "\t/\t" << mi << "," << mj << "\t" << *result << "\n";

#endif

    //todo:  skip if the "gap" ever gets too big

    // adjust score for each masked segment on scaffold 1:
    gap_info.length_2 = cp->length_2; //scaffolds.ScaffoldLen(target_2);
    for(i=0;i<int(scaffolds.scaffold_mask_segments_.size()); i++){
      //      std::cout << "mask i: " << i << " id: " << target_1 << " and also " << scaffolds.scaffold_mask_segments_[i].target_id << " start: " << scaffolds.scaffold_mask_segments_[i].start << " end: " << scaffolds.scaffold_mask_segments_[i].end << " size: " << scaffolds.scaffold_mask_segments_.size() << "\n";
    }
    for (i=mi;(i<int(scaffolds.scaffold_mask_segments_.size())) && (target_1==scaffolds.scaffold_mask_segments_[i].target_id);i++) {
      //std::cout << "mask i: " << i << " id: " << target_1 << " and also " << scaffolds.scaffold_mask_segments_[i].target_id << " start: " << scaffolds.scaffold_mask_segments_[i].start << " end: " << scaffolds.scaffold_mask_segments_[i].end << " size: " << scaffolds.scaffold_mask_segments_.size() << "\n";
      gap_info.gap      = cp->separation( scaffolds.scaffold_mask_segments_[i]) ;
      if (gap_info.gap > max_sep) {continue;}
	int tmp_x1;
	int tmp_x2;
	//assert(scaffolds.scaffold_mask_segments_[i].start < scaffolds.scaffold_mask_segments_[i].end < cp->length_1);
      gap_info.length_1 = scaffolds.scaffold_mask_segments_[i].length();
      all_pos_a.push_back(scaffolds.scaffold_mask_segments_[i].start);   
      all_pos_a.push_back(scaffolds.scaffold_mask_segments_[i].end);

	  cp->target_1_coordmap(scaffolds.scaffold_mask_segments_[i].start, &tmp_x1);
	  cp->target_1_coordmap(scaffolds.scaffold_mask_segments_[i].end, &tmp_x2);

	  all_pos_a.push_back(tmp_x2 < cp->length_1);
      all_pos_a.push_back(cp->length_1); 
     all_pos_a.push_back(gap_info.gap);
      all_pos_a.push_back(cp->length_1 + cp->gap);
      a_size += gap_info.length_1;

      for (n=0;n<int(libraries_.size());n++) {
	libid = libraries_[n];
	dl=0.0;
	lib_models_[libid]->LLRAreaContribution(&gap_info,n,&dl);
	*result -= dl;
#if tmp_score_debug_level > 1
	all_dls_a.push_back(dl);
	if (dl > 0) {
	  //os << "A:\t<< " << gap_info << "\t/\t" << mi << "," << mj << "\t" << uncorrected << " -> " << *result << "\tlibid: " << libid << "\tdl: " << dl << "\n";

	}
#endif
      }
#if tmp_score_debug_level>1
      //os << "\t<< " << gap_info << "\t/\t" << mi << "," << mj << "\t" << *result << "\n";
#endif
    }

    // adjust score for each masked segment on scaffold 2:
    gap_info.length_1 = cp->length_1;//;scaffolds.ScaffoldLen(target_1);
    for (i=mj;(i<int(scaffolds.scaffold_mask_segments_.size())) && (target_2==scaffolds.scaffold_mask_segments_[i].target_id);i++) {
      gap_info.gap      = cp->separation( scaffolds.scaffold_mask_segments_[i]) ;
      if (gap_info.gap > max_sep) {continue;}
      gap_info.length_2 = scaffolds.scaffold_mask_segments_[i].length();

	all_pos_b.push_back(scaffolds.scaffold_mask_segments_[i].start);
	all_pos_b.push_back(scaffolds.scaffold_mask_segments_[i].end);
	all_pos_b.push_back(gap_info.length_2);
	all_pos_b.push_back(gap_info.gap);
	all_pos_b.push_back(cp->length_2 + cp->gap); 
	b_size += gap_info.length_2;
      for (n=0;n<int(libraries_.size());n++) {
	libid = libraries_[n];
	dl=0.0;
	lib_models_[libid]->LLRAreaContribution(&gap_info,n,&dl);
	*result -= dl;
#if tmp_score_debug_level > 1

	all_dls_b.push_back(dl);
	if (dl > 0) {
	  //os << "B:\t<< " << gap_info << "\t/\t" << mi << "," << mj << "\t" << uncorrected << " -> " << *result << "\tlibid: " << libid << "\tdl: " << dl << "\n";

	}
#endif
      }
#if tmp_score_debug_level>1
      //os << "\t>> " << gap_info << "\t/\t" << mi << "," << mj << "\t" << *result << "\n";
#endif
    }

    // add back in to avoid double-discount of mask-mask overlap regions in the plane:
    for (i=mi;(i<int(scaffolds.scaffold_mask_segments_.size())) && (target_1==scaffolds.scaffold_mask_segments_[i].target_id);i++) {
      if (cp->separation( scaffolds.scaffold_mask_segments_[i]) > max_sep) {continue;}
      for (j=mj;(j<int(scaffolds.scaffold_mask_segments_.size())) && (target_2==scaffolds.scaffold_mask_segments_[j].target_id);j++) {
	gap_info.gap      = cp->separation( scaffolds.scaffold_mask_segments_[i], scaffolds.scaffold_mask_segments_[j] );
	if (gap_info.gap > max_sep) {continue;}
	gap_info.length_1 = scaffolds.scaffold_mask_segments_[i].length() ;
	gap_info.length_2 = scaffolds.scaffold_mask_segments_[j].length() ;
	  all_pos_c.push_back(scaffolds.scaffold_mask_segments_[i].start);
	  all_pos_c.push_back(scaffolds.scaffold_mask_segments_[i].end);

	  
	  all_pos_c.push_back(gap_info.length_1);
	  all_pos_c.push_back(scaffolds.scaffold_mask_segments_[j].start);
	  all_pos_c.push_back(scaffolds.scaffold_mask_segments_[j].end);
	  all_pos_c.push_back(gap_info.length_2);
	  all_pos_c.push_back(gap_info.gap);
	  c_size += gap_info.length_1 + gap_info.length_2;

	for (n=0;n<int(libraries_.size());n++) {
	  libid = libraries_[n];
	  dl=0.0;
	  lib_models_[libid]->LLRAreaContribution(&gap_info,n,&dl);
	  *result += dl;
#if tmp_score_debug_level > 1
	  all_dls_c.push_back(dl);
	  //os << "C:\t<< " << gap_info << "\t/\t" << mi << "," << mj << "\t" << uncorrected << " -> " << *result << "\tlibid: " << libid << "\tdl: " << dl << "\n";

#endif
#if tmp_score_debug_level>1 
	  //os << "\t## lib_" << n <<" " << gap_info << "\t/\t" << mi << "," << mj << "\t" << dl << "\t"<< *result << "\n";
#endif
	}
      }
    }

    #if tmp_score_debug_level >1
    if((mini==maxi) && (*result > 0)) {
      os << "A:";
      double a = 0;
      for(auto i=all_dls_a.begin(); i != all_dls_a.end(); i++) {
	a += *i;
	os << "dllr " << *i << " sum " << a << " ";

      }
      os << "\nTotal A: " << a << "\nPos A:";
      for(auto i=all_pos_a.begin(); i != all_pos_a.end(); i++) {
	os << *i << " ";
      }
      os << "\nB:";
      double b = 0;
      for(auto i=all_dls_b.begin(); i != all_dls_b.end(); i++) {
	b += *i;
	os << "dllr " << *i << " sum " << b << " ";

      }
      os << "\nTotal B: " << b << "\nPos B:";
      for(auto i=all_pos_b.begin(); i != all_pos_b.end(); i++) {
	os << *i << " ";
      }
      os << "\nC:";
      double c = 0;
      for(auto i=all_dls_c.begin(); i != all_dls_c.end(); i++) {
	c += *i;
	os << "dllr " << *i << " sum " << c << " ";
      }
      os << "\nTotal C: " << c << "\nPos C:";
      for(auto i=all_pos_b.begin(); i != all_pos_b.end(); i++) {
	os << *i << " ";
      }
      os << "\n";
      std::cout << os.str();
      std::cout << "A size: " << a_size << " B size: " << b_size << " C size: " << c_size << "\n";
      std::cout << "A length: " << cp->length_1 << " B length: " << cp-> length_2 << "\n";
      std::cout << "weird result3: "<< cp->target_1 << " " << cp-> target_2 << " " << mini << " " << maxi << " " << uncorrected << " " << cp->gap << '\n';
      std::cout << "weird result4: "<< cp->target_1 << " " << cp-> target_2 << " " << mini << " " << maxi << " " << *result << " " << cp->gap << '\n';
    }
    #endif
  }

  void RowRangeHitMap(int i,int j, std::map<int,int>& map,  std::ofstream& logstream,const PathSet& scaffolds) const {
    int k;
    int id;
    for (k=i;k<j;k++) {
      //      logstream << "row range map:\t" << k <<"\t"<< reads_[k].target_1 << "\t" << reads_[k].target_2 << "\t" << scaffolds.ContigScaffold(reads_[k].target_1) << "\t" << scaffolds.ContigScaffold(reads_[k].target_2) << "\n";
      id = reads_[k].target_1;
      map[id]=1;
      id = reads_[k].target_2;
      map[id]=1;
    }
  }

  void ContigPairLLR ( ContigPair *cp, unsigned long long mini, unsigned long long maxi, double *result, int max_sep=10000000) const {

    //PairedReadLikelihoodModel model;
    double dl;
    *result=0.0;
    Read read;
    if (mini>maxi) { throw std::runtime_error("invalid row range");  }
    uint n = maxi - mini;
    uint8_t libid;

    if (mini==maxi) {
      //std::cout << "weird result0: "<< cp->target_1 << " " << cp-> target_2 << " " << mini << " " << maxi << " " << *result << '\n';
    }
    //#define score_debug_level 2
#if score_debug_level>1 
    std::cout << "CP:\t" << *cp << "\t" << *result << "\t" << mini << "\t" << maxi << "\n" ;
#endif

    for (n=0;n<libraries_.size();n++) {
      libid = libraries_[n];
      lib_models_[libid]->LLRAreaContribution(cp,n,result);
#if score_debug_level>1 
      std::cout << "Library:" << n << "\t" << int(libid) << "\t" << *result << "\n";
#endif
    }
    if (mini==maxi) {
      //std::cout << "weird result1: "<< cp->target_1 << " " << cp-> target_2 << " " << mini << " " << maxi << " " << *result << '\n';
    }
      
    for (int k=mini; k<maxi; k++) {
      read = reads_[k];
      if ( cp->separation(read) > max_sep) {
#if score_debug_level>1 
	std::cout << "Read:\t" << int(read.libid) << "\t" << cp->separation(read) << "\t HIGH SEP\n" ;
#endif
	continue;
      }
      if (read.Ok()) {
	lib_models_[read.libid]->LLRReadPairContribution(cp,read,result);
#if score_debug_level>1 
	std::cout << "Read:\t" << int(read.libid) << "\t" << cp->separation(read) << "\t" << *result << "\n";
#endif
      } else {
#if score_debug_level>1 
	std::cout << "Read:\t" << int(read.libid) << "\t" << cp->separation(read) << "\t NOT OK\n" ;
#endif
      }
      
    }
#if score_debug_level>1 
    std::cout << "###: LLR:" << "\t" << *result << "\n";
#endif
    if (mini==maxi) {
      //std::cout << "weird result2: "<< cp->target_1 << " " << cp-> target_2 << " " << mini << " " << maxi << " " << *result << '\n';
    }
    
  }

  void InspectAllScaffolds( const PathSet& scaffolds ) {
    assert(sorting_==SCAFFOLDS);
    
    unsigned long long nlinks = reads_.size();
    int gap=100;
    double llr1,llr2,llr3,llr4=0.0;
    double dl=0.0;
    Read read;
    unsigned long long j=0;
    unsigned long long i=0;
    unsigned long long k=0;
    while (i<nlinks) {
      while (j<nlinks && (reads_[j].target_1 == reads_[i].target_1) && (reads_[j].target_2==reads_[i].target_2)) {j++;}

	read = reads_[i];
	if (read.target_1 == read.target_2) {

	  fprintf(stdout,"scaffold %d; range %llu-%llu; %llu links\n",read.target_1,i,j,j-i); 
	  //	  ScaffoldSupport(i,j,this,scaffolds);
	  
	}
	i=j;
    }
  }

  const char* GetTargetName(const int id) const {
    return header_.GetTargetName(id);
    //    assert(id>0 && id<target_names_.size());
    //    return target_names_[id].c_str();
  }


  /**

     When the links are sorted by (contig1,contig2), compute linkage scores for all pairs of contis connected by at least n_min links.

   */

  
  void AllContigPairScores(unsigned long long nmin=1) {
    unsigned long long nlinks = reads_.size();
    int gap=100;
    double llr1,llr2,llr3,llr4=0.0;
    double dl=0.0;
    Read read;
    unsigned long long j=0;
    unsigned long long i=0;
    unsigned long long k=0;
    while (i<nlinks) {
      while (j<nlinks && (reads_[j].target_1 == reads_[i].target_1) && (reads_[j].target_2==reads_[i].target_2)) {j++;}

      if ((j-i)>nmin) {
	read = reads_[i];
	if (read.target_1 != read.target_2) {
	ContigPair *cp = new ContigPair(read.target_1,read.target_2,header_.target_sizes_[read.target_1],header_.target_sizes_[read.target_2],gap);

	llr1=0.0; ContigPairLLR(cp,i,j,&llr1);
	llr2=0.0; cp->strand_1 = 1; ContigPairLLR(cp,i,j,&llr2);
	llr3=0.0; cp->strand_2 = 1; ContigPairLLR(cp,i,j,&llr3);
	llr4=0.0; cp->strand_1 = 0;  ContigPairLLR(cp,i,j,&llr4);
      
	//	llr=0.0;
	//for (k=i;k<j;k++) {  
	//  read = reads_[k];
	//  lib_models_[read.libid].F( fabs(read.pos_2 - read.pos_1), &dl );
	//  llr += dl;
	//}
	unsigned long long n=0;
	for (k=i;k<j;k++) {
	  if (reads_[k].Ok()) {
	    n+=1;
	  }
	}

	std::cout << header_.target_names_[read.target_1] <<"\t" << header_.target_names_[read.target_2] << "\t" << header_.target_sizes_[read.target_1] << "\t" << header_.target_sizes_[read.target_2] <<"\t" << n <<"\t" ;
	std::cout << llr1 << "\t" << llr2 << "\t" << llr3 << "\t" << llr4 << "\t" << "\t[ ";
	for (k=i;k<j;k++) {
	  if (reads_[k].Ok()) {
	    std::cout << "(" << reads_[k].pos_1 << "," << reads_[k].pos_2 << "), " ;
	  }
	}
	std::cout << "]";
	//	std::cout << "(" << reads_[k].pos_1 << "," << reads_[k].pos_2 << ") ]" ;
	std::cout << "\n";
      }      
      }
      i=j;
    }
  };


  /**

     Use a bin index to find the read pairs that relate a pair contig segments

   */

  void FindReadsForSegmentPair(int32_t target1_id, int32_t target1_start, int32_t target1_end,
			       int32_t target2_id, int32_t target2_start, int32_t target2_end) {

    int x1,x2,y1,y2;
    header_.GetTargetStart(target1_id,&x1); x1 += target1_start;
    header_.GetTargetStart(target1_id,&x2); x2 += target1_end  ;
    header_.GetTargetStart(target2_id,&y1); y1 += target2_start;
    header_.GetTargetStart(target2_id,&y2); y2 += target2_end  ;
    //int x1 = target_starts_[target1_id] +  target1_start  ;
    //int x2 = target_starts_[target1_id] +  target1_end    ;
    //int y1 = target_starts_[target2_id] +  target2_start  ;
    //int y2 = target_starts_[target2_id] +  target2_end    ;
    
    bin_index_.IntervalPairToListOfRanges(x1,y1,x2,y2,&range_list);
 
    std::pair<int32_t,int32_t> range;
    for(uint i=0;i<range_list.size();i++) {
      range = range_list[i];
      std::cout << "bin range:" << range.first << "\t" << range.second << "\n";
      for(int j=range.first; j<range.second; j++) {
	struct Read &read = reads_[j];
	if ((read.target_1 == target1_id) && (target1_start <= read.pos_1) && (read.pos_1 < target1_end) && 
	    (read.target_2 == target2_id) && (target2_start <= read.pos_2) && (read.pos_2 < target2_end)) {
	  std::cout << "read in bin:"  << read.target_1 <<"\t" << read.target_2 << "\t" << read.pos_1 << "\t" << read.pos_2 << "\t<<\n" ;
	  //std::cout << "<<<<<<<<<<";
	}
	else if ((read.target_2 == target1_id) && (target1_start <= read.pos_2) && (read.pos_2 < target1_end) && 
		 (read.target_1 == target2_id) && (target2_start <= read.pos_1) && (read.pos_1 < target2_end)) {
	  std::cout << "read in bin:"  << read.target_1 <<"\t" << read.target_2 << "\t" << read.pos_1 << "\t" << read.pos_2 << "\t>>\n" ; 
	  //std::cout << ">>>>>>>>>>";
	}
      }
    }
  }

  /**

     Use a bin index to find the read pairs that relate a pair contig segments

   */

  void FindReadsForContigPair(std::string contig1,std::string contig2) {
    int32_t target_id1,target_id2,l1,l2;
    header_.GetTargetId(contig1, &target_id1);
    header_.GetTargetId(contig2, &target_id2);
    l1=header_.target_sizes_[target_id1];
    l2=header_.target_sizes_[target_id2];
    std::cout <<"contig pair IDs:\t"<< target_id1 <<"\t"<< target_id2 <<"\t" << contig1 << "\t" << contig2 << "\t" << l1 <<"\t" <<l2 << "\n"; 
    FindReadsForSegmentPair( target_id1, 0, l1, target_id2, 0, l2 ); 
  }

  void FindReadsForContigPair(int target_id1, int target_id2) {
    int32_t l1,l2;
    l1=header_.target_sizes_[target_id1];
    l2=header_.target_sizes_[target_id2];
    std::cout <<"contig pair IDs:\t"<<target_id1 << "\t" << target_id2 <<"\t" ;
    std::cout << header_.target_names_[target_id1] << "\t" << header_.target_names_[target_id2] << "\t" << l1 <<"\t" <<l2 << "\n"; 
    FindReadsForSegmentPair( target_id1, 0, l1, target_id2, 0, l2 );    
  }

  /**
   * Find the reads that have the forward read mapping to a region
   * specified by target id, coordinate start in target and coordinate
   * end in target. Search is coordinates are usual c style closed open so
   * all reads such that
   * target_start <= read_pos < target_end 
   * or [target_start, target_end)
   * indexes returned are the start and stop into the reads data structure
   * or -1 if no reads are found.
   * @todo - should we filter based on ok?
   * @todo - should we just be passing around the iterators to avoid the addition 
   * and subtraction for indexes?
   */
  void FindReads(int32_t target_id, int32_t target_start, int32_t target_end,
		 int32_t *read_index_start, int32_t *read_index_end) const {
    assert(target_end >= target_start && target_start >= 0 && target_id >= 0);
    IndexRangeMap::const_iterator s = target_map_.find(target_id);
    int32_t search_start = s->second.first;
    int32_t search_end = s->second.second;
    Read search_read;
    search_read.target_1 = target_id;
    search_read.pos_1 = target_start;
    std::vector<Read>::const_iterator reads_begin = reads_.begin();
    std::vector<Read>::const_iterator lower = std::lower_bound(reads_begin + search_start, 
							       reads_begin + search_end,
							       search_read);
    search_read.pos_1 = target_end;
    std::vector<Read>::const_iterator upper = std::upper_bound(reads_begin + search_start, 
							       reads_begin + search_end,
							       search_read);
    // Set the index for lower bound
    if (lower == reads_.end()) {
      *read_index_start = -1;
    }
    else {
      *read_index_start = lower - reads_begin;
    }
    // Set the index for upper bound
    if (upper == reads_.end()) {
      *read_index_end = -1;
    }
    else {
      *read_index_end = upper - reads_begin;
    }
  }

  inline const BamHeaderInfo& GetHeader() const { return(header_); }

  /* Get the id (index) of target given the name. -1 if not found 
  inline void GetTargetId(const std::string &name, int32_t *id) const { 
    std::map<std::string, int32_t>::const_iterator i = target_idx_.find(name);
    i != target_idx_.end() ?  *id = i->second : *id = -1; 
  }

  // Get the size for a given id. 
  inline void GetTargetSize(int32_t target_id, int32_t *size) const { *size = target_sizes_[target_id]; }

  // Get the read with given index, memory owned by data structure so don't call free on it 
  inline void GetRead(int32_t index, const Read **read) const { (*read) = &reads_[index]; }
  */

  //  void dump_reads_in(std::string filename) const  {
  //   ofstream outfile(filename.c_str() , ios::in | ios::binary);
  //  outfile.write( (const char *)&reads_.front() ,reads_.size() * sizeof(Read) );
  //  outfile.close();
  //}
  void InitContigIndex(const std::unordered_map <std::pair<int,int>, std::pair<unsigned long long,unsigned long long>, dtg::hash_pair >& index ) {
    //InterScaffoldLinksIterator sc_it(*this);
    contig_index_ = index; 
    contigs_indexed_=true;
  }
  void InitContigIndex(const std::unordered_map <std::pair<int,int>, std::pair<unsigned long long,unsigned long long>, dtg::hash_pair >& index , const std::unordered_map <std::pair<int,int>, std::pair<unsigned long long,unsigned long long>, dtg::hash_pair >& scaffold_index ) {
    //InterScaffoldLinksIterator sc_it(*this);
    contig_index_ = index; 
    scaffold_index_ = scaffold_index; 
    contigs_indexed_=true;
  }

  void GetContigPairRowRange (const ContigPair& cp, unsigned long long* mini, unsigned long long* maxi) const {
    if (contigs_indexed_) {
      std::pair<int,int> q = std::make_pair(cp.target_1,cp.target_2);
      std::unordered_map <std::pair<int,int>, std::pair<unsigned long long,unsigned long long>, dtg::hash_pair >::const_iterator i = contig_index_.find( q );
      if (i!=contig_index_.end()) {
	//      std::pair<int,int> p = contig_index_[q];
	*mini =  i->second.first  ;
	*maxi =  i->second.second ;
	//	*maxi = i->second.second ;
	return;
      } 
    }
    *mini = 0; 
    *maxi = 0;
    return;
  }

  void GetScaffoldPairRowRange (int s1, int s2, unsigned long long* mini, unsigned long long* maxi) const {
    if (contigs_indexed_) { 
      //      int s1 = contig_scaffold_map_[cp.target_1];
      //int s2 = contig_scaffold_map_[cp.target_2];
      //      if (s1>s2) {std::swap(s1,s2);}
      assert(s1<=s2);
      std::pair<int,int> q = std::make_pair( s1,s2);
      std::unordered_map <std::pair<int,int>, std::pair<unsigned long long,unsigned long long>, dtg::hash_pair >::const_iterator i = scaffold_index_.find( q );
      if (i!=scaffold_index_.end()) {

	assert(i->first.first == s1);
	assert(i->first.second == s2);
	//      std::pair<int,int> p = contig_index_[q];
	*mini =  i->second.first  ;
	*maxi =  i->second.second ;
	//	*maxi = i->second.second ;
	return;
      } 
    }
    *mini = 0; 
    *maxi = 0;
    return;
  }

  void CheckScaffoldPairRowRange (int ss1, int ss2, unsigned long long mini, unsigned long long maxi,const PathSet& scaffolds) const {
    for(unsigned long long i=mini;i<maxi;i++) {
      int c1 = reads_[i].target_1;
      int c2 = reads_[i].target_2;
      int s1 = scaffolds.ContigScaffold(c1);
      int s2 = scaffolds.ContigScaffold(c2);
      if (s1>s2) {std::swap(s1,s2);}
      assert(s1==ss1);
      assert(s2==ss2);
    }
  }
 
  void write_reads(std::string filename) const  {
    std::ofstream outfile(filename.c_str() , std::ios::out | std::ios::binary);
    
    unsigned long nreads = reads_.size();
    fprintf(stdout,"#write to %s, %lu records\n",filename.c_str(),nreads);
    outfile.write( (const char *)&nreads ,sizeof(nreads) );
    outfile.write( (const char *)&reads_.front() ,nreads * sizeof(Read) );
    outfile.close();
  }

  void read_reads(std::string filename) {
    unsigned long nreads = reads_.size();
    //    int npairs;
    std::ifstream infile(filename, std::ios::binary);

    infile.read((char*)&nreads, sizeof(nreads));
    reads_.resize(nreads);
    fprintf(stdout,"#Read from %s, %lu records\n",filename.c_str(),nreads);
    infile.read((char*)&reads_.front(), nreads * sizeof(Read));

    infile.close();

    //TODO update target_map_

  }

  /*
  void read_reads(std::string filename, int npairs) const  {
    std::ifstream infile(filename, ios::binary);
    
    reads_.resize(npairs);
    infile.read((char*)&reads_.front(), npairs * sizeof(Read));

    outfile.close();
  }
  */


private:
  DT_DISABLE_COPY(ChicagoLinks);
  int8_t             min_mapq_;
  IndexRangeMap      target_map_;    ///< Map of target index to sorted range of reads that map
  BamHeaderInfo      header_;

  //std::map<std::string, int32_t> target_idx_;    ///< Map names to index/numeric id of targets
  // @todo - should the target names and sizes live somewhere else as there
  // are of general interest but convenient for now since reading bams...
  //std::vector<std::string>       target_names_;  ///< Names of target ids in order
  //std::vector<int32_t>           target_sizes_;  ///< Sizes of target ids in order

  std::vector<Read>              reads_;         ///< Vector of reads sorted by target id, read start
  BinIndex                       bin_index_;
  SortState                      sorting_;
  std::vector<std::pair<int32_t,int32_t>> range_list ;;
  std::vector<PairedReadLikelihoodModel *>  lib_models_;
  std::vector<uint8_t> libraries_;
  //  SegmentSet masked_segments_;


  bool contigs_indexed_;
  std::unordered_map <std::pair<int,int>, std::pair<unsigned long long,unsigned long long>, dtg::hash_pair > contig_index_;
  std::unordered_map <std::pair<int,int>, std::pair<unsigned long long,unsigned long long>, dtg::hash_pair > scaffold_index_;

  friend class IntraScaffoldLinksIterator;
  friend class InterScaffoldLinksIterator;
  friend class ScaffoldPairLinksIterator;
  friend class Breaker;
  friend class Joiner;

};

#endif // CHICAGO_LINKS_H
