#ifndef MODEL_PARSER_H
#define MODEL_PARSER_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
#include <Eigen/Dense>
#include <picojson.h>
#include "error_msg.h"
#include "chicago_read.h"
#include "paired_read_likelihood_model.h"
#include "pair_distance_likelihood_model.h"
#include "chicago_lm.h"
#include "chicago_wSI_lm.h"
#include "stranded_gaussian_model.h"
#include "custom_pair_model.h"

class LikelihoodModelParser {
 public:
  PairedReadLikelihoodModel* ParseModelJson(const char *model_json, const bool fill_caches=true) {
    picojson::value v;
    std::string err = picojson::parse(v, model_json);

    if (v.contains("model_type")) {
      std::string model_type = v.get("model_type").get<std::string>() ;
      
      if (model_type == "chicago") {
	ChicagoLM *result = new ChicagoLM ;
	
	result->SetModel(model_json,fill_caches);
	// fprintf(stderr, "Made a chicago model\n");
	return result;
      } else if (model_type == "chicagoSI") {
	ChicagoSILM *result = new ChicagoSILM ; //GaussianLM ;

	result->SetModel(model_json,fill_caches);
	// fprintf(stderr, "Made a shotgun model\n");
	return result;

      } else if (model_type == "shotgun") {
	StrandedGaussianLM *result = new StrandedGaussianLM ; //GaussianLM ;
	
	result->SetModel(model_json,fill_caches);
	// fprintf(stderr, "Made a shotgun model\n");
	return result;

      } else if (model_type == "custom") {
	CustomPairLM *result = new CustomPairLM; //GaussianLM ;

	result->SetModel(model_json,fill_caches);
	// fprintf(stderr, "Made a custom pair model\n");
	return result;

      } else {
	throw( std::runtime_error("model parsing error: unrecognized model type.") );
      }
    } 
    // for backward compatibility, when the model json doesn't explicitly specify a type, assume it's a chicago library.
    ChicagoLM *result = new ChicagoLM ;
    //ChicagoExpModel result;
    result->SetModel(model_json,fill_caches);
    // fprintf(stderr, "Made a chicago model\n");
    return result;

  }

    //    std::vector<double> beta_vec =  v.get_vector<double>("beta");
    //    std::vector<double> alpha_vec = v.get_vector<double>("alpha");
    //    num_links_ = v.get("N").get<double>();;
	// Should never reach here
};

#endif
