#ifndef CHICAGO_READ_PAIRS_H
#define CHICAGO_READ_PAIRS_H

#include <stdexcept>
#include "htslib/sam.h"
#include "chicago_read.h"
#include "histogram_provider.h"

typedef std::vector<std::pair<std::string, int64_t> > References;
typedef std::vector<Read> Reads;
typedef  std::map<int,double> histogramT;
typedef  std::map<int,double>::iterator hist_it;

struct Tripple {
  int64_t id;
  int64_t start;
  int64_t end;
  
  Tripple(int64_t a, int64_t b, int64_t c) : id(a), start(b), end(c) {};
};
bool operator< (const Tripple& lhs, const Tripple& rhs) {
  if (lhs.id != rhs.id) {
    return (lhs.id < rhs.id);
  } else {
    return ( lhs.start < rhs.start ); 
  }
};

/*
sc = 1000.0
if len(sys.argv)>1:
    sc=float(sys.argv[1])

ys=0.0
xs=0.0
nb=0.0
lastx=False
while True:
    l = sys.stdin.readline()
    if not l: break
    c = list(map(float,l.strip().split()))
    if not lastx:
        lastx=c[0]-1
    ys += c[1]
    xs += c[0]*c[1]
    nb += c[0]-lastx
    if ys > sc:
        print(old_div(xs,ys), old_div(ys,nb), nb)
        ys = 0.0
        xs = 0.0
        nb = 0.0
    lastx=c[0]

 */
void smooth_histogram ( histogramT raw , int maxx, double G, histogramT & smoothed, histogramT & errors) {
  //  histogramT smoothed;
  //  histogramT errors;

  //  return smoothed;
}

class ChicagoReadPairs : public HistogramProvider {
  //  friend class SVKernelScorer ;
//class ChicagoReadPairs {
private:
    References m_refs;
    Reads m_reads;
    std::vector<Tripple> m_segments;  // vector of masked segments for computing histogram counts corrections.

    bool histogram_only;
    histogramT smoothed;
    histogramT errors;
    std::string m_region;
    double m_genome_size;
    void _reset() {
        m_refs.clear();
        m_reads.clear();
    }

public:

  histogramT separation_histogram;
  //  double G;
  int max_separation = 1000000;

  //  ChicagoReadPairs() : histogram_only(false), max_separation(1000000), G(25.0e6) {}
  ChicagoReadPairs() : histogram_only(false), max_separation(1000000) {}

  void AddMaskedSegment(Tripple t) { m_segments.push_back(t); }
  void SortSegments() {std::sort(m_segments.begin(),m_segments.end()); }

  void SetHistogramOnly() { histogram_only = true; };

  //	smooth_histogram(separation_histogram,max_separation,G,smoothed,errors);
  void make_smoothed_histogram() {
    int maxx = max_separation;
    histogramT& raw = separation_histogram;
    histogram.resize(0);
 

    double min_number_counts_for_mean = 1000.0;
    int lastx = raw.begin()->first - 1;
    int x;
    double ys,xs,nb;
    ys=0.0;
    xs=0.0;
    nb=0.0;
    for (hist_it it = raw.begin(); it!=raw.end() ; it++) {
      if (it->first>=maxx) {break;}
      x = it->first;
      ys+=it->second;
      xs+=it->second*it->first;
      nb+=x-lastx;
      lastx=x;
      if (ys>min_number_counts_for_mean) {
	int xx = int(xs/ys);
	//	smoothed[xx]=ys/nb;
	//errors[  xx]=sqrt(ys)/nb;

	histogram.push_back(DataPoint(xx,ys/nb,sqrt(ys)/nb));


	ys=0.0;
	xs=0.0;
	nb=0.0;
      }

    }

    //  assert(raw.back().first==maxx);

    //smoothed[maxx] = 2.0*raw[maxx]/G;
    //errors[  maxx] = 2.0*sqrt(raw[maxx])/G;
    // std::cout <<  "smoothed histgoram noise level:" << raw[maxx] << "\t" << m_genome_size  <<"\n";
    if (raw[maxx]>0){
      histogram.push_back( DataPoint(maxx, 2.0*raw[maxx]/m_genome_size , 2.0*sqrt(raw[maxx])/ m_genome_size ));
    }

  }

  void apply_masking_correction() {
    if (m_segments.size()>0) {
      double L =0.0; //(total_masked_length)
      double m =0.0; // mean masked length    
      double G = genome_size();
      //      apply_mask_correction=true;
      for( std::vector<Tripple>::const_iterator it=m_segments.begin();it<m_segments.end();++it) {
	L += it->end - it->start;
      }
      m = L / m_segments.size();

      for (std::vector<DataPoint>::iterator it = histogram.begin(); it!=histogram.end() ; it++) {
	//DataPoint p = *it;
	std::cout << "hct:\t" << it->x << "\t" << it->y << "\t" << it->dy << "\t";
	it->y /= ( 1.0 - L/G + (1.0 - exp(- it->x / m))*(-L/G + pow((L/G),2.0)) );
	std::cout << it->y << "\t" << L <<"\t"<<G<<"\t"<<(1.0 - exp(- it->x / m)) << "\n";
      }

    } else {
      std::cout << "No Masked segments\n"; 
    }
  }

  void dump_histogram() {

    for (hist_it it = separation_histogram.begin(); it!=separation_histogram.end() ; it++) {
      std::cout << "h:\t" << it->first << "\t" << it->second<< "\n";
    }

    for (std::vector<DataPoint>::iterator it = histogram.begin(); it!=histogram.end() ; it++) {
      std::cout << "s:\t" << it->x << "\t" << it->y << "\t" << it->dy << "\n";
    }
  }

    // Getters
    const Reads& get_reads() const { return m_reads; }
    double genome_size() const { return m_genome_size; }
    const References& get_refs() const { return m_refs; }

    // Parsers
    template<typename FILTER>
    void fill_from_bam(const FILTER& filter,
                       std::string bam,
                       std::string region=".") {
        // Reset containers
        //_reset();

        bool appending = false;
	if (m_refs.size()>0) {appending=true;}

        // Open BAM
        samFile *in = sam_open(bam.c_str(), "r");
        if(in == NULL)
            throw std::runtime_error("Unable to open BAM/SAM file.");

        // Get the header
        bam_hdr_t *header = sam_hdr_read(in);
        if(header == NULL) {
            sam_close(in);
            throw std::runtime_error("Unable to open BAM header.");
        }

	double total_length = 0;
	if (appending) {
    // Make sure BAM headers are equivalent
    bool matches = m_refs.size() == header->n_targets;
    if (matches) {
      for (int i = 0; i < header->n_targets; ++i) {
        if (m_refs[i].first != header->target_name[i] ||
            m_refs[i].second != header->target_len[i]) {
          matches = false;
          break;
        }
      }
    }
    if (!matches)
      throw std::runtime_error("BAM files have differing headers.");
	} else {
	  // Extract lengths of the reference sequences
	  for (int i = 0; i < header->n_targets; ++i) {
            m_refs.push_back(
			     std::make_pair(header->target_name[i], header->target_len[i]));
            total_length += header->target_len[i];
	  }
	}

        // Load the index
        hts_idx_t *idx = sam_index_load(in, bam.c_str());
        if(idx == NULL) {
            bam_hdr_destroy(header);
            sam_close(in);
            throw std::runtime_error("Unable to open BAM/SAM index.");
        }


        // Move the iterator to region of interest
        m_region=region;
        hts_itr_t *iter = sam_itr_querys(idx, header, region.c_str());
	//	std::cout << "genome size:\t" << m_genome_size << "\n";
        if(iter == NULL) {
            hts_idx_destroy(idx);
            bam_hdr_destroy(header);
            sam_close(in);
            throw std::runtime_error("Unable to iterate to region.");
        }

        if (!appending) {
          if (region==".") { m_genome_size = total_length; } else { m_genome_size = (iter->end - iter->beg) ; }
        }

        // Extract alignment records
        bam1_t *aln = bam_init1();
	//	filter.keep_noise=true;

        while(sam_itr_next(in, iter, aln) >= 0) {
            if (!filter(aln))
                continue;

            Read read;
            read.target_1     = aln->core.tid;
            read.pos_1        = aln->core.pos;
            read.sam_flag_1   = aln->core.flag;
            read.size_1       = aln->core.l_qseq;
            read.mapq_1       = aln->core.qual;
            read.target_2     = aln->core.mtid;
            read.pos_2        = aln->core.mpos;

            uint8_t *tag;
            if (tag = bam_aux_get(aln, "xs"))
                read.size_2 = bam_aux2i(tag);
            if (tag = bam_aux_get(aln, "MQ"))
                read.mapq_2 = bam_aux2i(tag);
            if (tag = bam_aux_get(aln, "NM"))
                read.edit_dist_1 = bam_aux2i(tag);
            if (tag = bam_aux_get(aln, "xt"))
                read.type = *bam_aux2Z(tag);
            if (tag = bam_aux_get(aln, "xj")) {
	      if (*(bam_aux2Z(tag)) == 'T') {
		read.flags |= JUNCTION_FOUND;
	      }
	    }
            if (read.sam_flag_1 & BAM_FDUP)
                read.flags |= DUPLICATE;

	    if (!histogram_only) {
	      if ((aln->core.tid == aln->core.mtid)) { // keep the noise if only for the histogram.
		m_reads.push_back(read);
	      }
	    }

	    int sep;
	    if (read.target_1 == read.target_2 ) {
	      sep = abs( read.pos_1 - read.pos_2 );
	      if (sep>max_separation) {sep=max_separation;}
	    } else {
	      sep = max_separation;
	      //	      std::cout << "max_sep hit\n";
	    }
	    separation_histogram[sep]++;

        }

        hts_itr_destroy(iter);
        hts_idx_destroy(idx);
        bam_destroy1(aln);
        bam_hdr_destroy(header);
        sam_close(in);

	//	smooth_histogram(separation_histogram,max_separation,G,smoothed,errors);
	make_smoothed_histogram();

    }
};


#endif
