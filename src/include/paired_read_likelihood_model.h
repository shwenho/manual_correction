
#ifndef PAIRED_READ_LIKELIHOOD_MODEL_H
#define PAIRED_READ_LIKELIHOOD_MODEL_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
#include <Eigen/Dense>
#include <picojson.h>
#include "error_msg.h"
#include "chicago_read.h"
#include "segment_set.h"
#include "histogram_provider.h"

enum cpKind {
  GAPcp = 0,
  INTERC_LR = 1,  // intercalate target_2 into target_1
  INTERC_RL = 2   // intercalate target_1 into target_2
};

struct ContigPair {
  int target_1    ;
  int target_2    ;
  double length_1    ;
  double length_2    ;
  int start_1     ;
  int end_1       ;
  int start_2     ;
  int end_2       ;
  int nshotgun_1  ;
  int nshotgun_2  ;
  uint8_t strand_1 ;
  uint8_t strand_2 ;
  int gap         ;
  std::vector<std::pair<int,int>> masked_segments_1 ;
  std::vector<std::pair<int,int>> masked_segments_2 ;
  cpKind K     ;
  int location ;
  int x0;
  int y0;
  int sx;
  int sy;


ContigPair(int l1, int l2, int g) :                   target_1(0), target_2(0), length_1(l1), length_2(l2), start_1(0),end_1(l1), start_2(0),end_2(l2),  nshotgun_1(0), nshotgun_2(0), strand_1(0), strand_2(0),gap(g) , K(GAPcp) { update(); }
ContigPair(int t1, int t2, int l1, int l2, int g) : target_1(t1), target_2(t2), length_1(l1), length_2(l2), start_1(0),end_1(l1), start_2(0),end_2(l2) , nshotgun_1(0), nshotgun_2(0), strand_1(0), strand_2(0),gap(g), K(GAPcp) { update();}
ContigPair(int t1, int t2, int l1, int l2, int s1, int s2, int g) : target_1(t1), target_2(t2), length_1(l1), length_2(l2), strand_1(0), strand_2(0), gap(g), start_1(0), start_2(0),end_1(l1),end_2(l2), nshotgun_1(s1), nshotgun_2(s2), K(GAPcp) { update();}

  void flip() {
    std::swap(target_1,target_2);
    std::swap(length_1,length_2);
    std::swap(start_1,start_2);
    std::swap(end_1,end_2);
    std::swap(nshotgun_1,nshotgun_2);
    std::swap(strand_1,strand_2);
    strand_1 = strand_1 ^ 1;
    strand_2 = strand_2 ^ 1;
    update();
  }

  void normalize() {
    if (target_1>target_2) {
      flip();
    }
  }

  void set_strand_1(int x) {
    if (! x==strand_1) {
      assert(x==1||x==0);
      strand_1 = x;
      update();
    }
  }

  void set_strand_2(int x) {
    if (! x==strand_2) {
      assert(x==1||x==0);
      strand_2 = x;
      update();
    }
  }

  void update() {
    switch(K) {
    case GAPcp:
      location = length_1;
      if      (strand_1==0) { x0=0        ; sx= 1; }
      else if (strand_1==1) { x0=length_1 ; sx=-1; }
      else                  { assert(1==0); };
      if      (strand_2==0) { y0=length_1 + gap            ; sy= 1; }
      else if (strand_2==1) { y0=length_1 + gap + length_2 ; sy=-1; }
      else { assert(1==0); }

      break;                                                            //  1
    case INTERC_LR:                                                       //  ------------------------->
      if      (strand_1==0) { x0=0        ; sx= 1; }                    //               ---> 2
    //      else if (strand_1==1) { x0=length_1 ; sx=-1; }
      else { assert(1==0); }
      if      (strand_2==0) { y0=location + gap            ; sy= 1; }
      else if (strand_2==1) { y0=location + gap + length_2 ; sy=-1; }
      else { assert(1==0); }

      break;
    case INTERC_RL:                                                      //  -------------------------> 2
      if      (strand_1==0) { x0=location + gap            ; sx= 1 ; }   //            ---> 1
      else if (strand_1==1) { x0=location + gap + length_1 ; sx=-1; }
      else { assert(1==0); }
      if      (strand_2==0) { y0=0                         ; sy= 1; }
      //      else if (strand_2==1) { y0=length_1 + gap + length_2 ; sx=-1; }
      else { assert(1==0); }

      break;
    }
  }

  void dump() {
    fprintf(stdout,"Contig pair: contigs(%d,%d) strands(%d,%d) lengths(%g,%g) gap(%d) x0sx(%d,%d) y0sy(%d,%d)\n",target_1,target_2,strand_1,strand_2,length_1,length_2,gap,x0,sx,y0,sy);
    //    fprintf(stdout,"Contig pair: contigs(%d,%d) strands(%d,%d) lengths(%d,%d) gap(%d)\n",target_1,target_2,strand_1,strand_2,length_1,length_2,gap);
  }

  void dump(Read r) {
    fprintf(stdout,"Contig pair: contigs(%d,%d) strands(%d,%d) lengths(%g,%g) gap(%d) x0sx(%d,%d) y0sy(%d,%d)\n",target_1,target_2,strand_1,strand_2,length_1,length_2,gap,x0,sx,y0,sy);
    int xx;
    if (r.target_1==target_1) {
      target_1_coordmap(r.pos_1,&xx);
      fprintf(stdout,"target_1: %d->%d\n",r.pos_1,xx);
      target_2_coordmap(r.pos_2,&xx);
      fprintf(stdout,"target_2: %d->%d\n",r.pos_2,xx);
    } else if  (r.target_1==target_2) {
      target_1_coordmap(r.pos_2,&xx);
      fprintf(stdout,"target_1: %d->%d\n",r.pos_2,xx);
      target_2_coordmap(r.pos_1,&xx);
      fprintf(stdout,"target_2: %d->%d\n",r.pos_1,xx);
    }
  }

  inline void target_1_coordmap (int x, int* xx) {

    switch(K) {
    case GAPcp:
      *xx = ( x0 + x*sx );
      break;                                                            //  1
    case INTERC_LR:                                                       //  ------------------------->
      assert(strand_1==0);
      if (x >= location) {
	*xx = x + location + 2*gap;
      } else {
	*xx = x ;
      }
      break;
    case INTERC_RL:                                                      //  -------------------------> 2
      *xx = ( x0 + x*sx );
      break;
    }
  }

  inline void target_2_coordmap (int y, int* yy) {

    switch(K) {
    case GAPcp:
      *yy = ( y0 + y*sy );
      break;                                                            //  1
    case INTERC_LR:                                                       //  ------------------------->
      *yy = ( y0 + y*sy );
      break;
    case INTERC_RL:                                                      //  -------------------------> 2
      assert(strand_2==0);
      if (y >= location) {
	*yy = y + location + 2*gap;
      } else {
	*yy = y ;
      }
      break;
    }
  }

  int pair_strand_type(Read read) {
    
    //         first   second
    //   0 :    -->      <--    Innie
    //   1 :    <--      -->    Outie
    //   2 :    -->      -->    Fwd
    //   3 :    <--      <--    Rev

    if (read.target_1 == read.target_2) {

      if (!read.read1_flipped() &&  read.read2_flipped()) { return(0); } // Innie
      if ( read.read1_flipped() && !read.read2_flipped()) { return(1); } // Outie
      if (!read.read1_flipped() && !read.read2_flipped()) { return(2); } // Fwd
      if ( read.read1_flipped() &&  read.read2_flipped()) { return(3); } // Rev

    }

    if ( target_1 == read.target_1 && target_2 == read.target_2 ) {

      bool f1 = strand_1==0 ? read.read1_flipped() : !read.read1_flipped() ;
      bool f2 = strand_2==0 ? read.read2_flipped() : !read.read2_flipped() ;

      if (!f1 &&  f2) { return(0); } // Innie
      if ( f1 && !f2) { return(1); } // Outie
      if (!f1 && !f2) { return(2); } // Fwd
      if ( f1 &&  f2) { return(3); } // Rev

    } else if ( target_1 == read.target_2 && target_2 == read.target_1) {

      bool f1 = strand_1==0 ? read.read2_flipped() : !read.read2_flipped() ;
      bool f2 = strand_2==0 ? read.read1_flipped() : !read.read1_flipped() ;

      if (!f1 &&  f2) { return(0); } // Innie
      if ( f1 && !f2) { return(1); } // Outie
      if (!f1 && !f2) { return(2); } // Fwd
      if ( f1 &&  f2) { return(3); } // Rev

    } else {
      fprintf(stderr,"mismatch: %d %d ; %d %d\n",read.target_1,read.target_2,target_1,target_2);
      assert(0);

    }
    throw( std::runtime_error("strand error.") );
    return(-1);
  }

  int separation(Read read) {
    //int result = gap;
    //    int x,y,z,o1,o2;

    int xx,yy=0;

    if (read.target_1 == read.target_2) {
      return abs(read.pos_1-read.pos_2);
    }

    if ( target_1 == read.target_1 && target_2 == read.target_2 ) {

      target_1_coordmap( read.pos_1, &xx );
      target_2_coordmap( read.pos_2, &yy );

    } else if ( target_1 == read.target_2 && target_2 == read.target_1) {

      target_1_coordmap( read.pos_2, &xx );
      target_2_coordmap( read.pos_1, &yy );

    } else {
      fprintf(stderr,"mismatch: %d %d ; %d %d\n",read.target_1,read.target_2,target_1,target_2);
      throw( std::runtime_error("contig pair / read mismatch error.") );
    }
    return( abs(yy-xx) ) ;

  };

#define min4(A,B,C,D) std::min(std::min(A,B),std::min(C,D))

  /*
def fragment_size(l1,l2,o1,o2,coords,g=0):
    """o1 and o2 indicate the orientations of the two contigs being used  0 means 5' to 3'; 1 means flipped"""
    x,y = coords[0],coords[1]
    if (o1,o2) == (0,0):     #         -------1----->    ----2------>
        return (y+l1-x+g)
    if (o1,o2) == (0,1):     #         -------1----->   <---2-------
        return (l2-y+l1-x+g)
    if (o1,o2) == (1,0):     #        <-------1-----     ----2------>
        return (x+y+g)
    if (o1,o2) == (1,1):     #        <-------1-----    <----2------
        return (l2-y+x+g)
   */

  int separation( const MaskedSegment& s1,  const MaskedSegment& s2) {
    int xx1,xx2,yy1,yy2;

    if (s1.target_id == s2.target_id) {
      return ( min4(abs(s1.start-s2.start),abs(s1.start-s2.end),abs(s1.end-s2.start),abs(s1.end-s2.end)  ) ) ;
    }

    if ( target_1 == s1.target_id && target_2 == s2.target_id ) {
      target_1_coordmap( s1.start, &xx1 );
      target_1_coordmap( s1.end  , &xx2 );
      target_2_coordmap( s2.start, &yy1 );
      target_2_coordmap( s2.end  , &yy2 );
    } else     if ( target_1 == s2.target_id && target_2 == s1.target_id ) {
      target_1_coordmap( s2.start, &xx1 );
      target_1_coordmap( s2.end  , &xx2 );
      target_2_coordmap( s1.start, &yy1 );
      target_2_coordmap( s1.end  , &yy2 );
    } else {
      fprintf(stderr,"mismatch: %d %d ; %d %d\n",s1.target_id,s2.target_id,target_1,target_2);
      assert(0);
    }
    return( min4( abs(yy1-xx1), abs(yy1-xx2), abs(yy2-xx1), abs(yy2-xx2) ) );
  }

  int separation(  const MaskedSegment& s1 ) {  // intended to give the gapsize to use when computing masking corrections between a contig and a masked segment in the other contig.
    int xx1,xx2,yy1,yy2;

    assert( K==GAPcp || ( K==INTERC_LR && s1.target_id == target_1 ) || (  K==INTERC_RL && s1.target_id == target_2  ) ); // not valid for masked segments on an INTERCALATED contig
    switch(K) {
    case GAPcp:
      if ( target_1 == s1.target_id ) {
	target_1_coordmap( s1.start, &xx1 );
	target_1_coordmap( s1.end  , &xx2 );
	yy1 = length_1 + gap;
	if((xx1 < 0) || (xx1 > length_1) || (xx2 < 0) || (xx2 > length_1)){
	  std::cerr << "id1: " << target_1 << " id2: " << target_2 << " start: " << s1.start << " end: " << s1.end << " xx1: " << xx1 <<  " xx2: " << xx2 << " length1: " << length_1 <<  " length2: " << length_2 << " yy1: " << yy1 << " x0: " << x0 << " sx: " << sx << "\n";
	}
	assert((0 <= xx1) && (xx1 <= length_1));
	assert((0 <= xx2) && (xx2 <= length_1));
	assert(length_1 < yy1);
	return(std::min(abs(yy1-xx1),abs(yy1-xx2)));
      } else     if ( target_2 == s1.target_id ) {
	xx1 = length_1;
	target_2_coordmap( s1.start, &yy1 );
	target_2_coordmap( s1.end  , &yy2 );
	if((yy1 < 0) || (yy1 > length_2+length_1+gap) || (yy2 < 0) || (yy2 > length_2+length_1+gap)){
	  std::cerr << "id1: " << target_1 << " id2: " << target_2 << " start: " << s1.start << " end: " << s1.end << " yy1: " << yy1 << " yy2: " << yy2 << " length1: " << length_1 << " length2: " << length_2 <<  " xx1: " << xx1 << " y0: " << y0 << " sy: " << sy << "\n";
	}

	//assert((0 <= xx1) && (xx1 <= yy1));
	assert(yy1 <= length_2+gap+length_1);
	assert(yy1 >= 0);
	assert(yy2 <= length_2+gap+length_1);
	assert(yy2 >= 0);

	//assert((xx1 <= yy2) && (yy2 <= length_2+length_1+gap));
	return(std::min(abs(yy1-xx1),abs(yy2-xx1)));
      } else {
	//	fprintf(stderr,"mismatch: %d %d ; %d %d\n",s1.target_id,s2.target_id,target_1,target_2);
	assert(0);
      }
      break;
    case INTERC_LR:  // 2 intercalates into 1
      assert(0);
      assert(target_1 == s1.target_id);
      target_1_coordmap( s1.start, &xx1 );
      target_1_coordmap( s1.end  , &xx2 );

      target_2_coordmap( 0, &yy1 );
      target_2_coordmap( length_2-1 , &yy2 );
      return( min4( abs(yy1-xx1), abs(yy1-xx2), abs(yy2-xx1), abs(yy2-xx2) ) );

      break;
    case INTERC_RL:  // 1 intercalates into 2
      assert(0);
      assert(target_1 == s1.target_id);
      target_1_coordmap( 0, &xx1 );
      target_1_coordmap( length_1-1  , &xx2 );

      target_2_coordmap( s1.start, &yy1 );
      target_2_coordmap( s1.end  , &yy2 );
      return( min4( abs(yy1-xx1), abs(yy1-xx2), abs(yy2-xx1), abs(yy2-xx2) ) );

      break;
    }
    throw( std::runtime_error("unrecognized case in separation.") );
    return(0);
  }

  int separationL( const MaskedSegment& s1 ) {  // masked segment on the intercalated scaffold, paired with the part LEFT of the intercalation location
    int xx1,xx2,yy1,yy2;

    assert(!( K==GAPcp || ( K==INTERC_LR && s1.target_id == target_1 ) || (  K==INTERC_RL && s1.target_id == target_2  )) ); // only valid for masked segments on an INTERCALATED contig

    switch(K) {
    case GAPcp:
      assert(1==0);
      break;
    case INTERC_LR:  // 2 intercalates into 1
      target_2_coordmap( s1.start, &xx1 );
      target_2_coordmap( s1.end  , &xx2 );
      return( std::min(abs(xx1-location),abs(xx2-location)) );
      break;
    case INTERC_RL:  // 1 intercalates into 2
      target_1_coordmap( s1.start, &xx1 );
      target_1_coordmap( s1.end  , &xx2 );
      return( std::min(abs(xx1-location),abs(xx2-location)) );
      break;
    }
    return 0;

  }

  int separationR( const MaskedSegment& s1 ) {  // masked segment on the intercalated scaffold, paired with the part RIGHT of the intercalation location
    int xx1,xx2,yy1,yy2;

    assert(!( K==GAPcp || ( K==INTERC_LR && s1.target_id == target_1 ) || (  K==INTERC_RL && s1.target_id == target_2  )) ); // only valid for masked segments on an INTERCALATED contig
    yy1 = location + 2*gap;

    switch(K) {
    case GAPcp:
      assert(1==0);
      break;
    case INTERC_LR:  // 2 intercalates into 1
      target_2_coordmap( s1.start, &xx1 );
      target_2_coordmap( s1.end  , &xx2 );
      return( std::min(abs(xx1-yy1),abs(xx2-yy1)) );
      break;
    case INTERC_RL:  // 1 intercalates into 2
      target_1_coordmap( s1.start, &xx1 );
      target_1_coordmap( s1.end  , &xx2 );
      return( std::min(abs(xx1-yy1),abs(xx2-yy1)) );
      break;
    }
    return 0;
  }

  bool operator==(const ContigPair &other) const
  { return (target_1 == other.target_1
            && target_2 == other.target_2
            && strand_1 == other.strand_1
	    && strand_2 == other.strand_2
	    && gap      == other.gap);
  }
};

  std::ostream& operator<<(std::ostream& out, const ContigPair& cp) {
    out << cp.target_1 << "\t" << cp.target_2 << "\t" << cp.length_1 << "\t" << cp.length_2 << "\t" << int(cp.strand_1) << "\t"<< int(cp.strand_2) << "\t" << cp.gap  ;
    return out ;
    }

class PairedReadLikelihoodModel {
 public:

  virtual void SetModel(const char *model_json) {} ;
  virtual void LLRAreaContribution( ContigPair *cp , int n, double *result ) =0;
  virtual void LLRReadPairContribution( ContigPair *cp, Read r, double *result ) {};

  virtual void InsertSizeDist(     int l, double *result ) = 0 ;
  virtual float InsertSizeDistF(    int l ) = 0 ;
  virtual void NonNoiseInsertDist( int l, double *result ) = 0 ;

  virtual void H0( int l, double *result ) =0;

  // Required by read simulator
  virtual double genome_size() =0;

  virtual double noise_probability() =0;
  virtual const int32_t N() const =0;
  virtual void set_N(double N) =0;

  virtual void RescaleGenomesize(double GG, bool fill_cache=false) =0;
  virtual void fit(const HistogramProvider& data)=0;

  virtual void n_bar(int l1,int l2,int g,double *l) =0;
  virtual void n_bar(int l1,double *l) =0;

  virtual void SetMetagenomic(int64_t n_shotgun, bool useDepthPenalty=false) =0;
  virtual void UnSetMetagenomic() =0;
  virtual bool NeedDepthDiscrep(ContigPair *cp) =0;
  virtual void LLRDepthDiscrep(ContigPair *cp, double *result) =0;

  virtual void DescriptionString(char* sbuff) = 0;

  float operator() (int x) {return InsertSizeDistF(x); };

  // virtual int sample_distribution() =0;
};


std::ostream& operator<<(std::ostream& out, PairedReadLikelihoodModel& model) {
  // int bsize=1024;
  char *sbuff;
  sbuff=new char[1024];
  model.DescriptionString(sbuff);
  //  sprintf(sbuff,"%s",model.DescriptionString() );
  out.write( sbuff , strlen(sbuff) );
  return out;
}


#endif
