
#ifndef COMMON_H
#define COMMON_H

// Macro for disabling copy constructors we don't want...
#define DT_DISABLE_COPY(Class) \
  Class(const Class &); \
  Class &operator=(const Class &);

#define NAME_BUFFER_SIZE 1024

#endif // COMMON_H
