#ifndef MAPPED_READ_PAIR_H
#define MAPPED_READ_PAIR_H

#include <map>
#include <string>
#include <vector>
#include "htslib/hts.h"
#include "htslib/sam.h"
#include "common.h"
#include "error_msg.h"
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include <iostream>

// bitflags for status in read object
enum LinkFlags {
  LINK_OK        = 1 << 0, // high level bit to set after filters
  JUNCTION_FOUND = 1 << 1, // did we find a junction in this read based on bam tags
  DUPLICATE      = 1 << 2, // is this read marked as duplicate
  MASKED         = 1 << 3, // in a region masked out for some reason?
};

/// Paired read data structure
struct Read {
  // Primary Read
  int32_t target_1;    ///< numeric identifier of scaffold
  int32_t pos_1;       ///< position of read in scaffold
  int16_t sam_flag_1;  ///< flag vector from sam_1;
  int16_t size_1;      ///< size of read in bp
  int8_t  mapq_1;      ///< mapq score 
  int8_t  edit_dist_1; ///< edit distance
  // Mate
  int32_t target_2;    ///< numeric identifier of scaffold
  int32_t pos_2;       ///< position of read in scaffold
  int16_t size_2;      ///< size of read in bp
  int8_t  mapq_2;      ///< mapq score phred33

  int8_t  libid;      ///< mapq score phred33

  char    type;        ///< junction type
  int8_t  flags;       ///< bitmap of flags of interest

  Read() : target_1(0), pos_1(0), sam_flag_1(0), size_1(0), mapq_1(0), edit_dist_1(0),
    target_2(0), pos_2(0), size_2(0), mapq_2(0), libid(0), type(0), flags(0) {};

  inline bool Ok() const          { return (LINK_OK & flags)        != 0; }
  inline bool HasJunction() const { return (JUNCTION_FOUND & flags) != 0; }
  inline bool IsDuplicat()const   { return (DUPLICATE & flags)      != 0; }
  inline bool IsMasked() const    { return (MASKED & flags )        != 0; }

  /// compare based on target name and then on position within target.
  /// @todo - drop target name comparison?
  inline bool operator< (const Read& rhs) const {
    if (target_1 < rhs.target_1) 
      return true;
    if (target_1 > rhs.target_1) 
      return false;
    return pos_1 < rhs.pos_1;
  }

  inline void swap() {
    int32_t temp = target_1;
    target_1 = target_2;
    target_2 = temp;

    temp = pos_1;
    pos_1 = pos_2;
    pos_2 = temp;

  }

};


#endif
