#ifndef serialization_utils_
#define serialization_utils_


void vec2json( std::ostream& fh, Eigen::VectorXd vec ) {
  int n = vec.size();
  if (n==0) {fh << "[]"; return;}
  if (n==0) {fh << "[]"; return;}
  fh << "[";
  fh << vec(0);
  for (int i=1;i<n;i++) {
    fh << ", " << vec(i);
  }
  fh << "] ";  
}

void vec2json( std::ostream& fh, Eigen::ArrayXd vec ) {
  int n = vec.size();
  if (n==0) {fh << "[]"; return;}
  if (n==0) {fh << "[]"; return;}
  fh << "[";
  fh << vec(0);
  for (int i=1;i<n;i++) {
    fh << ", " << vec(i);
  }
  fh << "] ";  
}


#endif
