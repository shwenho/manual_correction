#ifndef SCAFFOLD_BREAKER_H
#define SCAFFOLD_BREAKER_H

#include <map>
#include <string>
#include <vector>
#include "htslib/hts.h"
#include "htslib/sam.h"
#include "common.h"
#include "error_msg.h"
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "chicago_read.h"
#include "pair_distance_likelihood_model.h"
#include "paired_read_likelihood_model.h"
#include "segment_set.h"
#include "bam_header_info.h"
#include "chicago_scaffolds.h"
#include "bincounts_heap.h"
#include "chicago_links.h"

//#define SKIP_BIN_HEAPS

enum peakState {
  OUT   = 0 ,
  IN_1  = 1 ,
  IN_2  = 2
};

class PeakLogger {
public:
  //  PeakLogger() {}
 PeakLogger(std::ofstream & logstream, bool peaks, double t1, double t2, double end_thresh, std::string tag): stream_(logstream), peaks_(peaks), t1_(t1), t2_(t2), end_thresh_(end_thresh), state_(OUT), range_start_(0), tag_(tag), leader_(true),last_end_thresh_x_(0) {
    if (!peaks) {
      sign_=-1.0;
      t1_ = std::min(-t1,-t2);
      t2_ = std::max(-t1,-t2);
    } else {
      sign_=1.0;
      t1_ = std::min(t1,t2);
      t2_ = std::max(t1,t2);
    }
    sbuff_len_ = 1024;
    sbuff_ = new char[sbuff_len_];
  }

  void reset() {
    state_=OUT;
    leader_=true;
  }

  void finalize(int scaffold, int x) {

    sprintf(sbuff_,"leader2\t%d\t%d\t%s\n",
	    scaffold,last_end_thresh_x_,tag_.c_str());
    stream_ << sbuff_ ;
    
    switch (state_) {
    case OUT:
      break;
    case IN_1:
      break;
    case IN_2:

      state_=OUT;
      //	sprintf(sbuff_,"low_support:\t%d\t%d\t%g\t%d\t%s\n",range_start_,last_x_,sign_*max_height_,peak_pos_,tag_.c_str()); stream_ << sbuff_ ;
      //	sprintf(sbuff_,"%d\t%d\t%d\t%g\t%d\t%s\n",scaffold,range_start_,last_x_,peak_pos_,sign_*max_height_,peak_pos_,tag_.c_str()); stream_ << sbuff_ ;
      sprintf(sbuff_,"%d\t%d\t%d\t%d\t%g\t%d\t%g\t%s\n",
	      scaffold,
	      range_start_,
	      x,
	      peak_pos_,
	      sign_*max_height_,
	      x+1,
	      sign_*max_other_height_,
	      tag_.c_str()); stream_ << sbuff_ ;
      break;
    }
  }

  void handle(int scaffold, int scaffold_length, int x, double y0, double z0) {
    double y = sign_*y0;
    double z = sign_*z0;

    if ( y0 >= end_thresh_ ) { 
      last_end_thresh_x_ = x;
      if (leader_) {
	leader_ = false;  
	sprintf(sbuff_,"leader1\t%d\t%d\t%s\n",
		scaffold,x,tag_.c_str());
	stream_ << sbuff_ ;
      }
    }
    //    sprintf(sbuff_,"#handle:\t(%d,%g)\t%d\t%g\t%g\t%g\t%s\n",x,y0,state_,t1_,t2_,y,tag_.c_str()); stream_ << sbuff_ ;
    switch (state_) {
    case OUT:
      if (y>t1_) { 
	state_=IN_1; 
	if (y>t2_) { 
	  state_=IN_2; 
	} 
	max_height_=y;
	peak_pos_=x;
	range_start_=x;  
	max_other_height_=z;
      }
      break;
    case IN_1:
      if (y>max_height_) { 
	max_height_=y;
	peak_pos_ = x;
      }
      if (z>max_other_height_) { max_other_height_=z; }

      if (y<t1_) { state_=OUT;  }
      if (y>t2_) { state_=IN_2; } 

      break;
    case IN_2:
      if (y>max_height_) { 
	peak_pos_ = x;
	max_height_=y;
      }
      if (y<t1_) { 
	state_=OUT;
	//	sprintf(sbuff_,"low_support:\t%d\t%d\t%g\t%d\t%s\n",range_start_,last_x_,sign_*max_height_,peak_pos_,tag_.c_str()); stream_ << sbuff_ ;
	//	sprintf(sbuff_,"%d\t%d\t%d\t%g\t%d\t%s\n",scaffold,range_start_,last_x_,peak_pos_,sign_*max_height_,peak_pos_,tag_.c_str()); stream_ << sbuff_ ;
	sprintf(sbuff_,"%d\t%d\t%d\t%d\t%g\t%d\t%g\t%s\n",
		scaffold,
		range_start_,
		last_x_,
		peak_pos_,
		sign_*max_height_,
		scaffold_length,
		sign_*max_other_height_,
		tag_.c_str()); stream_ << sbuff_ ;
      } else {
	if (z>max_other_height_) { max_other_height_=z; }
      }
      break;
    }
    last_x_ = x;
  }
  /*
4302 625 1331 625 26.834807654045882 2900 rawLLR
4302 1535 1539 1535 26.20178359795429 2900 rawLLR
11188 206 308 206 29.25252426755568 1020 rawLLR
4963 247 470 247 27.619287662861687 4600 rawLLR
1545 403 913 403 22.08661988321728 25359 rawLLR
1545 3000 6000 4500 13.776266920892486 25359 False clippedLLR
12695 491 501 491 26.533616577458137 1766 rawLLR
1137 1326 1384 1326 29.024604509484384 2710 rawLLR
   */  

private:
  ofstream & stream_;
  double t1_;         // report the domain over which the peak exceeds this threshold.
  double t2_;         // log peaks the breach this threshold
  double end_thresh_; // track and the locations of the first and last times the likelihood exceeds this threshold, to avoid breaking just because you're not far enough into the scaffold to 
                      // have  a good support score.
  double sign_;
  double max_height_;
  double max_other_height_;
  bool peaks_;
  bool leader_;
  peakState state_;
  int range_start_;
  int last_x_;
  int last_end_thresh_x_;
  int peak_pos_;
  char* sbuff_;
  int sbuff_len_;
  std::string tag_;
};


class Breaker {
private:
  PeakLogger rawPeaks_ ;
  PeakLogger clipPeaks_;
public:
  //  Breaker() {};
  //TODO -- peak fitering thresholds are hardcoded here.
 Breaker( std::ofstream & logstream  ): rawPeaks_( PeakLogger(logstream,false,20.0,30.0,20.0,"rawLLR") ), clipPeaks_( PeakLogger(logstream,false,20.0,30.0,20.0,"clippedLLR") ) { };
  
  void ScaffoldSupport( unsigned long long mini, unsigned long long maxi, const ChicagoLinks& links, const PathSet& scaffolds , ofstream & logstream, int max_sep=200000,int min_sep=500) {
    unsigned long long i,j,n;
    int target_id,libid =0;
    uint mi,mj,mk;
    int gap,nn;
    Read read;
    read = links.reads_[mini];
    target_id = read.target_1;
    std::vector<RSDelta> sort_buffer;
    char* logbuff = 0;
    logbuff = new char[1024];
    double raw_score, clipped_score;

    rawPeaks_.reset();
    clipPeaks_.reset();

    int scaffold_length = scaffolds.ScaffoldLen(target_id);
    int binsize=1500;

    //for keeping track of which rows and columns are the candidate outliers.
    BinCountsHeap row_totals( 1+scaffold_length / binsize);
    BinCountsHeap col_totals( 1+scaffold_length / binsize);

    if (target_id<int(scaffolds.scaffold_mask_segments_index_.size())) {
      mi = scaffolds.scaffold_mask_segments_index_[target_id];
      assert(mi>=0);
      assert(mi<=int(scaffolds.scaffold_mask_segments_.size()));
    } else {
      mi = scaffolds.scaffold_mask_segments_.size();
    }
    mj=mi;
    mk=mi;


    sort_buffer.clear();
    sort_buffer.reserve(2*(maxi-mini));
    fprintf(stdout,"## %d\n",target_id);
    fflush(stdout);
    
    double ll,rs,dl;
    ContigPair cp (read.target_1,read.target_1,0,0,0);
    ContigPair cpM(read.target_1,read.target_1,0,0,0);
    //    int scaffold_length =0;

    for(i=mini;i<maxi;i++) {
      read = links.reads_[i];
      assert( (read.target_1 == target_id) && (read.target_2==target_id) );
      // Build a buffer of read-pair ends with associated score bump.
      ll=0.0;
      //if (cp.separation(read)<=max_sep && ((read.flags&LINK_OK)!=0)) {  //TODO:  why does this cause a seg fault?
      if ( (int(read.flags)&int(LINK_OK))!=0 ) {
	if ((cp.separation(read)<=max_sep) && (cp.separation(read)>=min_sep)) {
	  links.lib_models_[read.libid]->LLRReadPairContribution(&cp,read,&ll);
	  if (read.pos_1 < read.pos_2) {
	    sort_buffer.push_back( RSDelta(read.pos_1, ll, 0, read.pos_1/binsize, read.pos_2/binsize  ));
	    sort_buffer.push_back( RSDelta(read.pos_2,-ll, 0, read.pos_1/binsize, read.pos_2/binsize  ));
	  } else {
	    sort_buffer.push_back( RSDelta(read.pos_2, ll, 0, read.pos_2/binsize, read.pos_1/binsize  ));
	    sort_buffer.push_back( RSDelta(read.pos_1,-ll, 0, read.pos_2/binsize, read.pos_1/binsize  ));
	  }
	  //	  fprintf(stdout,"pair_support:%d\t%d\t%d\t%g\t%d\n",read.pos_1,read.pos_2,cp.separation(read),ll,read.flags);
	}
      }
    }

    fprintf(stdout,"sb length=%d\n",int(sort_buffer.size()));
    fprintf(stdout,"masked segments %d/%d\n",mi,int(scaffolds.scaffold_mask_segments_.size()));
    fflush(stdout);
    // loop over pairs of in-range masked segments
    for (i=mi;(i<scaffolds.scaffold_mask_segments_.size()) && (target_id==scaffolds.scaffold_mask_segments_[i].target_id);i++) {
      for (j=mi;j<i;j++) {
	gap = scaffolds.scaffold_mask_segments_[i].start - scaffolds.scaffold_mask_segments_[j].end;
	if (gap<max_sep) {
	  if (gap<0) {
	    //fprintf(stdout,"overlapping masked segments! (WTF?) %d,%d %d:%d-%d %d:%d-%d\n",
	    //    j,i,target_id,scaffolds.scaffold_mask_segments_[j].start, scaffolds.scaffold_mask_segments_[j].end,
	    //    target_id,scaffolds.scaffold_mask_segments_[i].start, scaffolds.scaffold_mask_segments_[i].end
	    //	    );
	    continue;
	  }
	  ll=0.0;
	  for (n=0;n<links.libraries_.size();n++) {
	    libid = links.libraries_[n];
	    cp.gap = gap;
	    cp.length_1=scaffolds.scaffold_mask_segments_[j].end - scaffolds.scaffold_mask_segments_[j].start;
	    cp.length_2=scaffolds.scaffold_mask_segments_[i].end - scaffolds.scaffold_mask_segments_[i].start;
	    links.lib_models_[libid]->LLRAreaContribution(&cp,n,&ll);
	    //fprintf(stdout,"seg pair correction:%d\t%d\t%g\t",i,j,ll); 
	    //fprintf(stdout,"%d %d:%d-%d\t",j,scaffolds.scaffold_mask_segments_[j].target_id,scaffolds.scaffold_mask_segments_[j].start,scaffolds.scaffold_mask_segments_[j].end);
	    //fprintf(stdout,"%d %d:%d-%d\n",i,scaffolds.scaffold_mask_segments_[i].target_id,scaffolds.scaffold_mask_segments_[i].start,scaffolds.scaffold_mask_segments_[i].end);

	    fflush(stdout);
	    assert(scaffolds.scaffold_mask_segments_[j].end>=0);
	    assert(scaffolds.scaffold_mask_segments_[i].start>=0);
	    sort_buffer.push_back( RSDelta( scaffolds.scaffold_mask_segments_[j].end   , ll, 1 ));
	    sort_buffer.push_back( RSDelta( scaffolds.scaffold_mask_segments_[i].start ,-ll, 1 ));

	  }
	}
      }
    }
    fprintf(stdout,"sb length=%d\n",int(sort_buffer.size()));
    fflush(stdout);

    std::sort(sort_buffer.begin(),sort_buffer.end());

    rs=0.0;
    cp.gap=0;
    double mask_correction;
    double mask_correction_delta;
    int    max1_row_i=0, max1_col_i=0, max2_row_i=0, max2_col_i=0; 
    double max1_row_v=0.0, max1_col_v=0.0, max2_row_v=0.0, max2_col_v=0.0; 
    double clip_correction=0.0;
    int row, col;
    double dy;
    for(i=0;i<sort_buffer.size();i++) {
      //fprintf(stdout,"support step: %d %d %g\n",i,sort_buffer[i].x,sort_buffer[i].dy);

      dy=sort_buffer[i].dy;
      rs+=dy;
      
      if (sort_buffer[i].z>0) continue;
      row = sort_buffer[i].row;
      col = sort_buffer[i].col;
#ifndef SKIP_BIN_HEAPS      
      row_totals.IncrementBin(row,dy);
      col_totals.IncrementBin(col,dy);
#endif
      dl=0.0;  
      mask_correction=0.0;
      cp.length_1 = std::min( sort_buffer[i].x                  , max_sep);
      cp.length_2 = std::min( scaffold_length-sort_buffer[i].x  , max_sep);

      while ( (scaffolds.scaffold_mask_segments_[mk].target_id == target_id) && (scaffolds.scaffold_mask_segments_[mk].start-sort_buffer[i].x)  < max_sep )      {mk++;}
      while ( (scaffolds.scaffold_mask_segments_[mj].target_id == target_id) && (scaffolds.scaffold_mask_segments_[mj].start-sort_buffer[i].x)  < 0       )      {mj++;}
      while ( (scaffolds.scaffold_mask_segments_[mi].target_id == target_id) && (sort_buffer[i].x-scaffolds.scaffold_mask_segments_[mi].end  ) > max_sep )      {mi++;}

      for (n=0;n<links.libraries_.size();n++) {
	libid = links.libraries_[n];
	links.lib_models_[libid]->LLRAreaContribution(&cp,n,&dl);
	//std::cout << "Library:" << n << "\t" << int(libid) << "\t" << *result << "\n";
	// Compute the masking corrections:
	nn=0;
	for (j=mi;j<mj;j++) {
	  cpM.gap      = std::max(sort_buffer[i].x-scaffolds.scaffold_mask_segments_[j].end,0) ; //todo:  fix this max, that happens when we're IN a masked region
	  cpM.length_1 = std::min(max_sep, scaffolds.scaffold_mask_segments_[j].end - scaffolds.scaffold_mask_segments_[j].start );
	  cpM.length_2 = cp.length_2 ; 
	  mask_correction_delta = 0.0;
	  links.lib_models_[libid]->LLRAreaContribution(&cpM,n,&mask_correction_delta);	  
	  //fprintf(stdout,"mask correctionL: %d %d %d/%d %d - %d %d %g\n",target_id,sort_buffer[i].x,j,mj,scaffolds.scaffold_mask_segments_[j].start,scaffolds.scaffold_mask_segments_[j].end,cpM.gap,mask_correction_delta);
	  mask_correction+=mask_correction_delta;
	  nn++;
	}
	for (j=mj;j<mk;j++) {
	  cpM.gap      = std::max(scaffolds.scaffold_mask_segments_[j].start -sort_buffer[i].x,0); //todo:  fix this max, that happens when we're IN a masked region
	  cpM.length_2 = std::min(max_sep, scaffolds.scaffold_mask_segments_[j].end - scaffolds.scaffold_mask_segments_[j].start );
	  cpM.length_1 = cp.length_1 ; 
	  mask_correction_delta = 0.0;
	  links.lib_models_[libid]->LLRAreaContribution(&cpM,n,&mask_correction_delta);	  
	  //fprintf(stdout,"mask correctionM: %d %d %d/%d %d - %d %d %g\n",target_id,sort_buffer[i].x,j,mj,scaffolds.scaffold_mask_segments_[j].start,scaffolds.scaffold_mask_segments_[j].end,cpM.gap,mask_correction_delta);

	  mask_correction+=mask_correction_delta;
	  nn++;
	}
      }

#ifndef SKIP_BIN_HEAPS
      clip_correction = std::max( row_totals.SumTopN(2) , col_totals.SumTopN(2) );
#endif
      //    support:        1       0       157     10.3786 -1.90539e+08    -inf    -1.90539e+08    10.3786 26190   157     10.3786 0       ::      0-0-14  14

      raw_score     = rs + dl - mask_correction                   ;
      clipped_score = rs + dl - mask_correction - clip_correction ;

      rawPeaks_.handle (target_id,scaffold_length,sort_buffer[i].x,raw_score    ,clipped_score);
      clipPeaks_.handle(target_id,scaffold_length,sort_buffer[i].x,clipped_score,raw_score    );

      //sprintf(logbuff,"support:\t%d\t%d\t%d\t%g\t%g\t%g\t%g\t%g\t%d\t%d\t%g\t%d\t::\t%d-%d-%d\t%d\n",
      //	      target_id,i,sort_buffer[i].x,
      //	      rs,dl,-mask_correction,dl,clip_correction,
      //	      scaffold_length,sort_buffer[i].x,dy,sort_buffer[i].z,mi,mj,mk,nn);

      //#define LOG_SUPPORT
#ifdef LOG_SUPPORT
      sprintf(logbuff,"support:\t%d\t%d\t%g\t%g\t%g\t%g\t%g\n", //\t%g\t%g\t%g\t%d\t%d\t%g\t%d\t::\t%d-%d-%d\t%d\n",
      	      target_id,sort_buffer[i].x,raw_score,clipped_score,clip_correction,row_totals.SumTopN(2) , col_totals.SumTopN(2));      
      //      fflush(stdout);
      logstream << logbuff ;
#endif
    }

    rawPeaks_.finalize(  target_id , scaffold_length-1 );
    clipPeaks_.finalize( target_id , scaffold_length-1 );

    fprintf(stdout,"done with scaffold %d\n",target_id);
    fflush(stdout);

    //For clipped likelihood...  look at the slope of the running sum and add some new mask segments??    but that might not really do the trick for segments with lots of links, but not short-range links.
    // maybe look at the slope of the running sum while excluding pairs closer than some threshold.
    
  }
  
};
#endif
