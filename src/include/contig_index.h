#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "link_range_iterator.h"
#include "hirise_threads.h"
#include <fenv.h>
#include <unordered_map>
#include "pair_hash.h"


std::unordered_map <std::pair<int,int>, std::pair<unsigned long long, unsigned long long>, dtg::hash_pair > IndexContigs(const ChicagoLinks& links) {
  InterScaffoldLinksIterator sc_it(links);
  std::unordered_map <std::pair<int,int>, std::pair<unsigned long long, unsigned long long>, dtg::hash_pair > index;
  index.reserve(1000000);
  std::pair<int,int> key;
  std::pair<unsigned long long, unsigned long long> value;
  while(!sc_it.done()) {
    key = std::make_pair((*sc_it).id1,(*sc_it).id2);
    value = std::make_pair((*sc_it).mini, (*sc_it).maxi);
    index[key] =  value;
    
    ++sc_it;
  }
  std::cout << "done indexing contigs." << "\tload factor:" << index.load_factor() << "\tsize:" << index.size() << "\tbucket count:" << index.bucket_count()  << "\tmax load factor:" << index.max_load_factor() << "\n"; ; std::cout.flush(); 
  return index;
}


void update_contig_index(const ChicagoLinks& links, std::unordered_map <std::pair<int,int>, std::pair<unsigned long long, unsigned long long>, dtg::hash_pair >& index) {
  InterScaffoldLinksIterator sc_it(links);
  std::pair<int,int> key;
  std::pair<unsigned long long, unsigned long long> value;
  int n = 0;
  while(!sc_it.done()) {
    key = std::make_pair((*sc_it).id1,(*sc_it).id2);
    value = std::make_pair((*sc_it).mini, (*sc_it).maxi);
    index[key] =  value;
    ++sc_it;
    n += 1;
  }
  std::cout << "done re-indexing contigs." << "\tload factor:" << index.load_factor() << "\tsize:" << index.size() << "\tbucket count:" << index.bucket_count()  << "\tmax load factor:" << index.max_load_factor() << "number of insertions " << n << "\n"; 
  std::cout.flush(); 

}

//ScaffoldPairLinksIterator
//for when the links table is indexing broken contigs, but sorted to group by pairs of scaffolds first:
std::unordered_map <std::pair<int,int>, std::pair<unsigned long long, unsigned long long>, dtg::hash_pair > IndexScaffolds(const ChicagoLinks& links, const PathSet& scaffolds) {
  ScaffoldPairLinksIterator sc_it(links,scaffolds);
  std::unordered_map <std::pair<int,int>, std::pair<unsigned long long, unsigned long long>, dtg::hash_pair > index;
  index.reserve(1000000);
  std::pair<int,int> key;
  std::pair<unsigned long long, unsigned long long> value;
  while(!sc_it.done()) {
    key = std::make_pair((*sc_it).id1,(*sc_it).id2);
    value = std::make_pair((*sc_it).mini, (*sc_it).maxi);
    /*
    if ( (index.size()%1000)==0 ) {
      std::cout << "insert:\t" << (*sc_it).id1 <<","<<(*sc_it).id2 << "\t" <<  (*sc_it).mini << "-" << (*sc_it).maxi << "\t" << index.load_factor() << "\t" << index.size() << "\t" << index.bucket_count()  << "\t" << index.max_load_factor()  << "\t" << hp(key)<< "\n";
    }
    */
    index.insert(std::make_pair(key, value));
    
    ++sc_it;
  }
  std::cout << "done indexing scaffolds." << "\tload factor:" << index.load_factor() << "\tsize:" << index.size() << "\tbucket count:" << index.bucket_count()  << "\tmax load factor:" << index.max_load_factor() << "\n"; ; std::cout.flush(); 

  return index;
}

