#ifndef LLR_REQUESTS_H
#define LLR_REQUESTS_H

#include <stdint.h>
// #include <assert.h>
// #include "error_msg.h"
// #include "chicago_read.h"
// #include "segment_set.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include "error_msg.h"

typedef std::pair<int, int> SegPair;

class OoInfo {
 public:
  // results
  double  llr_score;
  double  cllr_score;
  unsigned long long nlinks;
  // O&O specifics for request
  int gap;
  short strand_1;
  short strand_2;
 OoInfo(int gapl, short s1, short s2):
  gap(gapl), strand_1(s1), strand_2(s2), nlinks(0) {
    llr_score  = -std::numeric_limits<double>::infinity();
    cllr_score = -std::numeric_limits<double>::infinity();
  }
 OoInfo(int gapl):
  gap(gapl), strand_1(-1), strand_2(-1), nlinks(0) {
    llr_score  = -std::numeric_limits<double>::infinity();
    cllr_score = -std::numeric_limits<double>::infinity();
  }

  void putScore(double score, double cscore, unsigned long long links) {
    llr_score  = score;
    cllr_score = cscore;
    nlinks     = links;
  }
  bool operator < (const OoInfo& r) {
    return ((gap < r.gap)
	    || (strand_1 < r.strand_1)
	    || (strand_2 < r.strand_2));
  }
};

typedef std::vector<OoInfo> OO_Vec;

typedef std::map<SegPair, OO_Vec> OO_Requests;

void Load_OO_Reqs(OO_Requests &L, const std::string &file_name, bool debug = 0)
{
  std::ifstream in(file_name.c_str());
  if (!in.good()) {
    ErrorMsg::Error("Couldn't open file %s\n", file_name.c_str());
  }
  std::string line;
  bool formatok = true;
  while (in.good()) {
    std::getline(in, line);
    if (! line.length()) continue;
    std::istringstream iss(line);
    int i1, i2, gap;
    short s1, s2;

    // Let's take advantage of C++ formatting input...
    if (! ((iss >> i1) &&
	   (iss >> i2) &&
	   (iss >> gap)))
      {
	// Fail if don't get three mandatory values
	formatok = false;
	break;
      }
    else if ((iss >> s1) &&
             (iss >> s2))
      {
        // Fail if got strand values but they are invalid
        if (s1 > 1 || s2 > 1 || s1 < 0 || s2 < 0) {
          formatok = false;
          break;
        }
      }
    else {
      // no (valid) strand info, will do all combinations
      s1 = s2 = -1;
    }

    if (i1 > i2) {
	std::swap(i1, i2);

	// if s1 > -1
	if (s1 > -1) {
	  std::swap(s1, s2);
	  s1 = 1 - s1;
	  s2 = 1 - s2;
	}
    }

    // Insert an entry (or four entries if no strand info)
    OO_Vec *v = &(L[std::make_pair(i1, i2)]);
    if (s1 >= 0) { 
      OoInfo info = { gap, s1, s2 };
      v->push_back(info);
    }
    else { // insert four entries (not necessary at the moment, though)
      OoInfo info = { gap, s1, s2 };
      v->push_back(info);       
      /*
      for (short i = 0; i <= 1; i++) { 
	for (short j = 0; j <= 1; j++) { 
	  OoInfo info = { gap, i, j }; 
	  v->push_back(info); 
	}
      }
      */
    }
  }
  if (formatok && ! debug) return;
  /*
  // Debugging test:
  std::cerr << "OO Requests Hash contains:\n";
  for (OO_Requests::iterator it=L.begin(); it!=L.end(); ++it) {
    std::cout << "\t" << it->first.first << "," << it->first.second << std::endl;
    for (OO_Vec::iterator jt = it->second.begin(); jt != it->second.end(); ++jt) {
      std::cerr << "\t\t"
		<< jt->gap << ","
		<< jt->strand_1 << ","
		<< jt->strand_2 << ";"
		<< jt->llr_score << ","
		<< jt->cllr_score << ","
		<< jt->nlinks << std::endl;
    }
    }*/

  if (!formatok) {
    ErrorMsg::Error("Bad formatting! File %s, line: '%s'\n", 
		    file_name.c_str(), line.c_str()); 
  }
}
#endif
