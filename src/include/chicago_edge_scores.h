#ifndef CHICAGO_EDGE_SCORES_H
#define CHICAGO_EDGE_SCORES_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
#include <Eigen/Dense>
#include <picojson.h>
#include <pthread.h>
#include "error_msg.h"
#include "chicago_read.h"
#include "paired_read_likelihood_model.h"
#include <cmath>

class ChicagoExpModel : public PairedReadLikelihoodModel {

public:
  ChicagoExpModel() { Init(); }

  ChicagoExpModel(const char *model_json, int64_t n_shotgun = 0) {
    Init();
    SetModel(model_json);
    if (n_shotgun) {
      SetMetagenomic(n_shotgun);
    }
  }

  void SetModel(const char *model_json, const bool fill_cache = true) {
      picojson::value v;
      std::string err = picojson::parse(v, model_json);
      std::vector<double> beta_vec =  v.get_vector<double>("beta");
      std::vector<double> alpha_vec = v.get_vector<double>("alpha");
      num_links_   = v.get("N").get<double>();
      genome_size_ = v.get("G").get<double>();
      prob_noise_ = v.get("pn").get<double>();
      DT_ASSERT(beta_vec.size() == alpha_vec.size(), "alpha and beta must match in size.");
      int size = beta_vec.size();
      alpha_.resize(size);
      beta_.resize(size);
      a_scratch_.resize(size);
      std::copy(beta_vec.begin(), beta_vec.end(), &(beta_(0)));
      std::copy(alpha_vec.begin(), alpha_vec.end(), &(alpha_(0)));
      neg_beta_ = -1.0f * beta_;
      neg_alpha_ = -1.0f * alpha_;
      alpha_beta_ = alpha_ * beta_; // element wise, not dot product
      neg_alpha_div_beta_ = -1.0 * alpha_ / beta_;

      logG_ = log(genome_size_);
      logN_ = log(num_links_);
      prob_noise_div_genome_size_ = prob_noise_ / genome_size_;
      mean_num_links_null_coeff_ = num_links_ * prob_noise_ / (genome_size_ * genome_size_);
      lnFinf_ = log(prob_noise_) - logG_;

      one_minus_prob_noise_ = 1.0d - prob_noise_;

      //        self.lnFinf = math.log(self.pn)-math.log(self.G)
      // std::cout << "neg_beta_  " << neg_beta_ << std::endl;
      // std::cout << "neg_alpha_ " << neg_alpha_ << std::endl;
      if (fill_cache) {
	fill_caches();
      }
  }

  inline int32_t NumExponential() { return beta_.rows(); }

  /* void ModelFunctionInserts(const Eigen::VectorXd &inserts, double *likelihood) { */
  /*   // outer product to create matrix with row for each insert */
  /*   // and column for each coefficient */
  /*   m_scratch_ = inserts.transpose() * neg_beta_; */
  /*   m_scratch_.exp(); */
  /*   m_scratch_ = m_scratch_ * alpha_beta_; */
  /*   double result = m_scratch_.sum(); */
  /*   *likelihood = result; */
  /* } */

  // Currently slower with small values than just regular math version below
  void ModelFunctionInsertVec(int insert_size, double *likelihood) {
    Eigen::ArrayXd a_scratch ;
    //a_scratch.resize();
    //    pthread_mutex_lock(   &mutex_ );
    a_scratch = insert_size * neg_beta_;
    a_scratch = a_scratch .exp();
    a_scratch *= alpha_beta_;
    *likelihood = a_scratch.sum();
    //pthread_mutex_unlock(   &mutex_ );
  }

  /* // Current fastest way of calculating mixture of exponential probability currently */
  /* // P_{raw}(x)= \sum \alpha_i \beta_i e^{-x \beta_i}  */
  /* // f0 in python */
  /* void ExpLikelihood(int insert_size, double *likelihood) { */
  /*   double *alpha_beta_start = alpha_beta_start_; */
  /*   double *neg_beta_start = neg_beta_start_; */
  /*   double *neg_beta_end = neg_beta_end_; */
  /*   *likelihood = 0.0d; */
  /*   while(neg_beta_start != neg_beta_end) { */
  /*     *likelihood += *alpha_beta_start++ * exp(*neg_beta_start++ * insert_size); */
  /*   } */
  /* } */

  // f in python
  inline void ModelLikelihood(int insert_size, double *likelihood) {
    //    ExpLikelihood(insert_size, likelihood);
    ModelFunctionInsertVec(insert_size, likelihood);
    *likelihood *= one_minus_prob_noise_;
    *likelihood += prob_noise_div_genome_size_;
  }

  // lnF in python
  inline void LogModelFunction(int d, double *log_likelihood) {

    //assert(d-cache_min_>=0);
    if (cache_min_ <= d && d < cache_max_) {
      if (Fcache_fill_[d-cache_min_]>0) {
	*log_likelihood = Fcache_[d-cache_min_];
      } else {
	pthread_mutex_lock(   &mutex_F_ );
	//	double ll;
	ModelLikelihood( d , &Fcache_[d-cache_min_] );
	Fcache_[d-cache_min_] = log(Fcache_[d-cache_min_]);
	Fcache_fill_[d-cache_min_]=1;
	*log_likelihood = Fcache_[d-cache_min_];
	pthread_mutex_lock(   &mutex_F_ );
	
      }
    } else {
	ModelLikelihood( d , log_likelihood );
	*log_likelihood = log(*log_likelihood);
    }

  }
  
  // n_bar0 in python prototype
  inline void MeanNumLinksNull(double length1_x_length2, double coeff, double *likelihood) {
    *likelihood = length1_x_length2 * coeff;
  }

  // double genome_size() {
  // return double(genome_size_);
  // }
  // double noise_probability() {
  // return double(prob_noise_);
  // }
  
  inline void F0(int insert_size, double *result) {
    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;

    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= neg_alpha_;
    *result = a_scratch.sum();
    *result *= one_minus_prob_noise_;
    *result += prob_noise_div_genome_size_ * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  inline void F(int insert_size, double *result) {
    F0(insert_size, result);
    double zero_result = 0.0d;
    F0(0, &zero_result);
    *result -= zero_result;
  }

  inline void H0(int insert_size, double *result) {
    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;
    // 0.5*d*d*self.pn/self.G + 
    // (1.0-self.pn) * sum([ -(old_div(alpha[i],beta[i]))*( math.exp(-beta[i]*d) * (beta[i] * d + 1.0)) for i in range(self.nexps) ])
    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= (beta_ * insert_size) + 1;
    a_scratch *= neg_alpha_div_beta_;
    *result = a_scratch.sum();
    *result *= one_minus_prob_noise_;
    *result += prob_noise_div_genome_size_ * 0.5 * insert_size * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  inline void H(int insert_size, double *result) {
    H0(insert_size, result);
    double zero_result = 0.0d;
    H0(0, &zero_result);
    *result -= zero_result;
  }  

  inline void T(int d, double *result) {

    double f_0;
    double h_0;
    if (d<cache_min_) {
      fprintf(stdout,"T\td<0\t%d\n",d);  fflush(stdout);
    }
    assert(d-cache_min_>=0);
    if (cache_min_ <= d && d < cache_max_) {
      if (Tcache_fill_[d-cache_min_]>0) {
	*result = Tcache_[d-cache_min_];
      } else {
	pthread_mutex_lock(   &mutex_T_ );
	F(d, &f_0);
	H(d, &h_0);
	Tcache_[d-cache_min_] = d * f_0 - h_0;
	Tcache_fill_[d-cache_min_]=1;
	*result = Tcache_[d-cache_min_];
	pthread_mutex_unlock(   &mutex_T_ );

      }
      // fprintf(stdout,"Tc\t%d\t%g\n",d,*result);  fflush(stdout);

    } else {
      F(d, &f_0);
      H(d, &h_0);
      *result = d * f_0 - h_0;
      // fprintf(stdout,"Tm\t%d\t%g\n",d,*result);  fflush(stdout);

    }
  }

  inline void p(int insert_size_1, int insert_size_2, int gap, double *result) {
    double f_sum, f_gap, f_1_gap, f_2_gap;
    T(insert_size_1 + insert_size_2 + gap, &f_sum);
    T(gap, &f_gap);
    T(insert_size_1 + gap, &f_1_gap);
    T(insert_size_2 + gap, &f_2_gap);
    *result = f_sum + f_gap - f_1_gap - f_2_gap;
    *result /= genome_size_;
  }

  void MeanNumLinks(int insert1, int insert2, int gap, double *result) {
    p(insert1, insert2, gap, result);
    *result *= num_links_;
  }

  void n_bar(int l1,int l2,int g,double *l) {
    double p_link = 0.0;
    p(l1,l2,g,&p_link);
    *l = p_link * num_links_;
  }

  void LLRAreaContribution( ContigPair *cp, int xxxxx , double *result ) {
    double l1 = cp->length_1;
    double l2 = cp->length_2;
    double s1 = 0.0, s2 = 0.0;
    // No pseudo-count necessary with depth_discrep code moved out?
    if (is_metagenomic_) {
      //std::cout << "metagenomic? " << is_metagenomic_ << "\n";
      s1 = cp->nshotgun_1; // + 0.1;
      s2 = cp->nshotgun_2; // + 0.1;
    }

    //pthread_mutex_lock(   &mutex_A_ );

      /*
    def lnL(self,l1,l2,o1,o2,g,links):
        n=len(links)
        n_bar = self.n_bar(l1,l2,g)
        try:
            r= n*self.logN - n_bar - n*self.logG + sum( [ math.log(self.omega(l1,l2,fragment_size(l1,l2,o1,o2,links[i],0))) + self.lnF( fragment_size(l1,l2,o1,o2,links[i],g) ) for i in range(n) ] )
           |====================================|
   */
    // return( - self.n_bar(l1,l2,g) + self.n_bar0(l1*l2) - n * self.lnFinf + sum( [ ( self.lnF( fragment_size(l1,l2,o1,o2,links[i],g) ) ) for i in range(n) ] )  )
    //        self.lnFinf = math.log(self.pn)-math.log(self.G)

    double p_link = 0.0,n_bar0=0.0;
    // p is a function of g (gap length) and that's OK
    p(l1, l2,cp->gap,&p_link);
    //fprintf(stdout,"p_link:\t%d\t%d\t%d\t%d\t%g\t%g\t%g\n",cp->length_1,cp->length_2,cp->gap,num_links_,logN_,logG_,p_link*num_links_ );
    //*result +=  double(n)  * logN_ ;
    //*result += -double(n)  * logG_;

    // For Metagenomics, need to multiply by correction
    // ((shot1 + shot2)/(len1 + len2)) (G/Shot_T)
    double meta_correx = is_metagenomic_?
      (genome_div_total_shotgun_ * (s1 + s2))/(l1 + l2)
      :
      1.0;
    double delta = -num_links_ * p_link * meta_correx;
    *result += delta;
#if score_debug_level > 1    
    if (delta > 0) {
      std::cerr << "Area_1: " << l1 << "," << l2 << "," << s1 << "," << s2 << "\t" << *result << "\t" << delta << "\t" << *cp << "\n";
    }
#endif

    //    fprintf(stdout,"nbar %g\t", -num_links_ * p_link * meta_correx);
    //fflush(stdout);

    // set noise_term
    if (is_metagenomic_) {
      MeanNumLinksNull(s1*s2, mean_num_links_null_coeff_metag_, &n_bar0);
    } else {
      MeanNumLinksNull(l1*l2, mean_num_links_null_coeff_, &n_bar0);
    }

    *result += n_bar0;

#if score_debug_level > 1
    if (*result > 0) {
      std::cerr << "Area_2: " << l1 << "," << l2 << "," << s1 << "," << s2 << "\t" << *result << "\t" << n_bar0 << "\t" << *cp << "\n";
    }
#endif
    //pthread_mutex_unlock(   &mutex_A_ );
  } 

  void LLRReadPairContribution(ContigPair *cp,Read read,double *result) {
    //pthread_mutex_lock(   &mutex_P_ );
    //    *result += logN_;
    //    *result -= logG_;
    *result -= lnFinf_;
    double d = 0.0;
    ModelLikelihood( cp->separation(read), &d );
    //fprintf(stdout,"llr:\t%d\t%d\t%d\t%g\t%g\t%g\t%g\n",read.pos_1,read.pos_2,cp->separation(read),*result,log(d),lnFinf_ ,log(d)-lnFinf_ );
    *result += log(d);
    //pthread_mutex_unlock(   &mutex_P_ );

  }

private:
  void fill_caches() {
    int i;
    double f_0;
    double h_0;
    fprintf(stderr,"start cache fill\n"); fflush(stderr);
    for (i=0;i<cache_max_;i++) {
      Fcache_fill_[i-cache_min_]=1;
      ModelLikelihood( i , &Fcache_[i-cache_min_] );
      Fcache_[i-cache_min_] = log(Fcache_[i-cache_min_]);
    }
    for (i=cache_min_;i<cache_max_;i++) {
      Tcache_fill_[i-cache_min_]=1;
      F(i, &f_0);
      H(i, &h_0);
      Tcache_[i-cache_min_] = i * f_0 - h_0;
    }
    fprintf(stderr,"done cache fill\n"); fflush(stderr);
  }

  /// Initialize with default values
  void Init() {
    num_links_ = 0;
    genome_size_ = 0.0d;
    prob_noise_ = 0.0d;
    prob_noise_div_genome_size_ = 0.0d;
    one_minus_prob_noise_ = 0.0d;
    mean_num_links_null_coeff_ = 0.0d;
    beta_.resize(0);
    alpha_.resize(0);
    alpha_beta_.resize(0);
    neg_beta_.resize(0);
    cache_max_ = 400000;
    cache_min_ =-400000;
    is_metagenomic_ = false;
    use_depth_penalty_ = false;
    genome_div_total_shotgun_ = 0.0;
    mean_num_links_null_coeff_metag_ = 0.0;

    Tcache_.resize(cache_max_-cache_min_);
    Tcache_fill_.resize(cache_max_-cache_min_);
    std::fill(Tcache_fill_.begin(),Tcache_fill_.end(),0);

    Fcache_.resize(cache_max_-cache_min_);
    Fcache_fill_.resize(cache_max_-cache_min_);
    std::fill(Fcache_fill_.begin(),Fcache_fill_.end(),0);

    pthread_mutex_init( &mutex_, NULL);
    pthread_mutex_init( &mutex_T_, NULL);
    pthread_mutex_init( &mutex_F_, NULL);
    pthread_mutex_init( &mutex_A_, NULL);
    pthread_mutex_init( &mutex_P_, NULL);
  }

  // scalars
  int32_t num_links_;       ///< N in python code
  int64_t genome_size_;     ///< size of genome in base pairs
  double  prob_noise_;      ///< probability of noise links
  double logG_;
  double logN_;
  double lnFinf_;
  // Cached precalculated values
  double prob_noise_div_genome_size_;
  double one_minus_prob_noise_;
  double mean_num_links_null_coeff_;
  // Extra scalars & precalcs for metagenomics
  bool is_metagenomic_;
  bool use_depth_penalty_;
  double genome_div_total_shotgun_;
  double mean_num_links_null_coeff_metag_;
  
  // Matrices and vectors of data
  Eigen::ArrayXd  alpha_;       ///< alpha coefficients for exponential model;
  Eigen::ArrayXd  beta_;        ///< beta coefficients for exponential model;
  Eigen::ArrayXd  neg_beta_;    ///< cached negative coefficients for exponential model;
  Eigen::ArrayXd  neg_alpha_;    ///< cached negative coefficients for exponential model;
  Eigen::ArrayXd  neg_alpha_div_beta_;    ///< cached negative coefficients for exponential model;
  Eigen::ArrayXd  alpha_beta_;  ///< cached alpha*beta coefficients for exponential model;
  Eigen::ArrayXd  a_scratch_;   ///< vector for intermediate results
  Eigen::MatrixXd m_scratch_;   ///< Matrix for storing intermediate results;
  
  std::vector<double> Tcache_;
  std::vector<int> Tcache_fill_;

  std::vector<double> Fcache_;
  std::vector<int> Fcache_fill_;
  //  std::vector<double> Tcache_;
  
  pthread_mutex_t mutex_;
  pthread_mutex_t mutex_T_;
  pthread_mutex_t mutex_F_;
  pthread_mutex_t mutex_A_;
  pthread_mutex_t mutex_P_;
};

#endif // CHICAGO_EDGE_SCORES_H
