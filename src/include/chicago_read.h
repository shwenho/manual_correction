#ifndef CHICAGO_READ_H
#define CHICAGO_READ_H

#include <map>
#include <string>
#include <vector>
#include "htslib/hts.h"
#include "htslib/sam.h"
#include "common.h"
#include "error_msg.h"
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include <iostream>

// bitflags for status in read object
enum LinkFlags {
  DEFAULT        = 0 ,
  LINK_OK        = 1 << 0, // high level bit to set after filters
  JUNCTION_FOUND = 1 << 1, // did we find a junction in this read based on bam tags
  DUPLICATE      = 1 << 2, // is this read marked as duplicate
  MASKED         = 1 << 3, // in a region masked out for some reason?
};

void swap_sam_flags(int16_t *flags) {

  int16_t new_flags = 0;

  new_flags = *flags & ( 1 | 2 | 256 | 512 | 1024 | 2048 ) ;

  //  new_flags |= 1 & *flags ; // read paired 
  //  new_flags |= 2 & *flags ; // read mapped in proper pair

  new_flags |= (4 & *flags) << 1 ; // read unmapped  
  new_flags |= (8 & *flags) >> 1 ; // mate unmapped  

  new_flags |= (16 & *flags) << 1 ; // read reverse strand
  new_flags |= (32 & *flags) >> 1 ; // mate reverse strand 

  new_flags |= (64  & *flags) << 1 ; // first in pair
  new_flags |= (128 & *flags) >> 1 ; // second in pair

  //  new_flags |= 256  & *flags ; // not primary alignments
  //new_flags |= 512  & *flags ; // read fails platform/vendor quality checks
  //new_flags |= 1024 & *flags ; // read is PCR or optical duplicate
  //new_flags |= 2048 & *flags ; // rsupplementary alignment
  
  *flags = new_flags ;
  
}

/// Paired read data structure
struct Read {
  // Primary Read
  int32_t target_1;    ///< numeric identifier of scaffold
  int32_t pos_1;       ///< position of read in scaffold
  int16_t sam_flag_1;  ///< flag vector from sam_1;
  int16_t size_1;      ///< size of read in bp
  int8_t  mapq_1;      ///< mapq score 
  int8_t  edit_dist_1; ///< edit distance
  // Mate
  int32_t target_2;    ///< numeric identifier of scaffold
  int32_t pos_2;       ///< position of read in scaffold
  int16_t size_2;      ///< size of read in bp
  int8_t  mapq_2;      ///< mapq score phred33

  int8_t  libid;      ///< mapq score phred33

  char    type;        ///< junction type
  int     flags;       ///< bitmap of flags of interest

  Read() : target_1(0), pos_1(0), sam_flag_1(0), size_1(0), mapq_1(0), edit_dist_1(0),
    target_2(0), pos_2(0), size_2(0), mapq_2(0), libid(0), type(0), flags(DEFAULT) {};

  inline bool Ok() const          { return (LINK_OK & flags)        != 0; }
  inline bool HasJunction() const { return (JUNCTION_FOUND & flags) != 0; }
  inline bool IsDuplicat()const   { return (DUPLICATE & flags)      != 0; }
  inline bool IsMasked() const    { return (MASKED & flags )        != 0; }
  inline void SetMasked()         { flags |= MASKED ; }

  /// compare based on target name and then on position within target.
  /// @todo - drop target name comparison?
  inline bool operator< (const Read& rhs) const {
    if (target_1 < rhs.target_1) 
      return true;
    if (target_1 > rhs.target_1) 
      return false;
    return pos_1 < rhs.pos_1;
  }

  inline void flip_read1() { sam_flag_1 = sam_flag_1 ^ 16 ; }
  inline void flip_read2() { sam_flag_1 = sam_flag_1 ^ 32 ; }

  inline bool read1_flipped() { return( (sam_flag_1 & 16) != 0 ) ; }
  inline bool read2_flipped() { return( (sam_flag_1 & 32) != 0 ) ; }

  inline void swap() {
    int32_t temp = target_1;
    target_1 = target_2;
    target_2 = temp;

    temp = pos_1;
    pos_1 = pos_2;
    pos_2 = temp;
    swap_sam_flags( &sam_flag_1 ) ;
  }

};

std::ostream& operator<<(std::ostream& out, const Read& r) {
  out << r.target_1 << ":" <<r.target_2 ;
  return out;
}


struct Base {
  int32_t target_id; 
  int32_t x        ; 

  //Base(int target, int x): target_id(target), x(x) {} ;
Base(int32_t target, int32_t x): target_id(target), x(x) {} ;
Base(): target_id(-1), x(-1) {} ;

  bool operator < (const Base& r) const {
    if (target_id != r.target_id) {
      return (target_id < r.target_id);
    } else {
      return ( x < r.x ); 
    }
  }

  bool operator ==(const Base& r) const {
    return((target_id == r.target_id) && (x == r.x ));
  }
};


#endif
