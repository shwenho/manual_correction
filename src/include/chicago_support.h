#ifndef CHICAGO_SUPPORT_H
#define CHICAGO_SUPPORT_H

class ChicagoSupport {

 public:
  void PairsToSupport(const ChicagoLinks &links, const Path &path) {
    int scaffold = 98;
    int scaffold_length = 8607;
    int pairs[43][2] = 
      {778, 494}, {1275, 1558}, {1710, 7104}, {2080, 2379}, {2190, 1925}, {2333, 1290}, {2370, 2076}, {2403, 2121}, {2473, 2192}, {2801, 3061}, {3681, 3393}, {3952, 4300}, {4790, 5078}, {5080, 5356}, {5086, 4820}, {5104, 4820}, {5245, 5549}, {5319, 5606}, {5412, 2212}, {5451, 5738}, {5900, 6205}, {6075, 6351}, {6159, 5862}, {6345, 6650}, {6370, 6660}, {6442, 6718}, {6500, 6794}, {6709, 7009}, {6728, 6455}, {6810, 7101}, {6813, 6522}, {6892, 7158}, {6979, 5624}, {7147, 7437}, {7437, 7723}, {7518, 7782}, {7544, 7804}, {7548, 7847}, {7549, 7825}, {7762, 7492}, {8135, 8430}, {8222, 8490}, {8426, 8116};
    int masked_segments[18][2] = {309, 319}, {509, 537}, {757, 767}, {891, 901}, {1048, 1233}, {1838, 1849}, {2325, 2348}, {2692, 2736}, {2953, 2963}, {3209, 3219}, {3284, 3294}, {3388, 3398}, {3811, 3821}, {4480, 4500}, {5515, 5525}, {7193, 7235}, {7461, 7551}, {8038, 8088};
    std::vector<const Read *> pairs;
    int num_tile_cols = 0, num_tile_rows = 0;
    // fill in pairs for all the scaffold, do we need contig ids in the read? 
    // while filling in pairs determine the maximum number of tiles.
    Matrix2d tiles(num_tile_rows, num_tile_cols);
    tiles.setZero();

    // calculate ll of each read pair insert in scaffold coordinates and add to proper tile
    
    // for each masked segment calculate the likelihood adjustment

    // calculate the fine grained support
    
  }

 private:

  
}

#endif // CHICAGO_SUPPORT_H
