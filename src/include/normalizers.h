#ifndef NORMALIZERS_H
#define NORMALIZERS_H

#include <vector>
#include <cmath>

template <typename T>
class ZMasker {
    typedef std::vector<T> Values;
private:
    // Config parameters
    int _window; // Window size
    int _step; // Window shift
    double _threshold; // Z-score threshold
    T _masked; // Masked value

    // Container to hold Z-scores
    std::vector<double> _zmap;

    void _prepare(Values& orig) {
        int i, j, step = _step;
        double mean, std, z;
        for (i = 0; step != 0; i += step){
            mean = 0.0, std = 0.0;
            // Calculate mean of window
            for (j = i; j < i+_window; ++j)
                mean += orig[j];
            mean /= _window;
            // Calculate standard deviation of window
            for (j = i; j < i+_window; ++j)
                std += pow(orig[j]-mean, 2);
            std = sqrt(std / (_window-1));
            // Update Z-scores if necessary
            for (j = i; j < i+_window; ++j) {
                z = (orig[j]-mean) / std;
                if (std::isnan(_zmap[j]) || z < _zmap[j])
                    _zmap[j] = z;
            }
            // Increment position to next window
            step = std::min<int>(orig.size()-_window-i, _step);
        }
    }

public:
    ZMasker(double threshold,
            T masked,
            int window=10000,
            int step=100) :
        _threshold(threshold), _masked(masked), _window(window), _step(step) {}

    void normalize(Values& orig) {
        if (_window == 0)
            return;
        // Reset Z-scores container
        _zmap.clear();
        _zmap.resize(orig.size(), NAN);
        // Calculate minimum Z-scores (across all spanning windows)
        // for each value in original vector
        _prepare(orig);
        // Apply mask on original vector of values
        for (int i = 0; i < orig.size(); ++i)
            if (orig[i] < _masked ||
                std::isnan(_zmap[i]) ||
                _zmap[i] < _threshold)
                orig[i] = _masked;
    }

    // Getters
    const std::vector<double>& get_zscores() { return _zmap; }
};


#endif
