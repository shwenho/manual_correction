#ifndef HISTOGRAM_PROVIDER_
#define HISTOGRAM_PROVIDER_


// mean to be a base class for classes that provide a vector of datapoints.

struct DataPoint {

  int x;
  double y;
  double dy;

  DataPoint() : x(-1), y(0), dy(0) {}
  DataPoint(int xx, double yy, double ddyy ) : x(xx), y(yy), dy(ddyy) {}

};

typedef std::vector<DataPoint> PointVector;
typedef std::vector<DataPoint>::iterator PointVector_it;
typedef std::vector<DataPoint>::const_iterator PointVector_const_it;

class HistogramProvider {
public:
  PointVector histogram ;
  virtual double genome_size() const = 0;
};

#endif
