#ifndef ERROR_MSG_H
#define ERROR_MSG_H

#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#define ERROR(m) ErrorMsg::Error(m)
#define DT_ASSERT(c,m) if(!( c )) { ERROR(m); }
#define INFO(...) fprintf (stderr, __VA_ARGS__)

/// Basic place to centralize errors.
class ErrorMsg {
 public:
  // Exit with an informative error message
  static void Error(const std::string &msg) {
    fprintf(stdout, "%s\n", msg.c_str());
    exit(1);
  }

  /// Exit with an informative error message
  static void Error(const char *format, ...) {
    va_list args;
    va_start(args, format);
    vfprintf(stdout, format, args);
    va_end(args);
    exit(1);
  }
};

#endif // ERROR_MSG_H;
