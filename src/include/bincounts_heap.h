#ifndef BINCOUNTS_HEAP
#define BINCOUNTS_HEAP

#include <vector>
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include <cstdio>
using namespace std;

// Class for tracking most-contributing rows and columns for clipped likelihood 
// by maintaining a heap.
// To speed updates, keeps indexes of where each value lives in the heap
// (so that we can do a log-time re-heapify (upwards or downwards) 
// instead of linear-time rebuild.

class BinCountsHeap {
 public:
  BinCountsHeap(int N) {
    values_.resize(N, 0.0);
    index_.resize(N);
    back_idx.resize(N);
    int i;
    for (i=0; i<N; i++) {
      index_[i]   = &values_[i];
      back_idx[i] = i;
    }
    make_heap();
  }

  // Done: faster, by re-heapifying upwards or downwards after changing one
  // value. Required maintaining a back-index so that we could tell where
  // the value currently lives in the heap.
  // Return true if bin was an element in the heap, false if out of range
  bool IncrementBin(int bin, double dv) {
    if (bin >= values_.size()) {
      return false;
    }
    values_[bin]+=dv;
    heapify_updown(back_idx[bin]);
    return true;
  }

  void Examine() {
    uint i;
    for (i=0; i<values_.size(); i++) {
      fprintf(stdout,"bin_counts: %d\t%g\n",i,values_[i]);
    }
    fprintf(stdout,"\n");
    for (i=0;i<values_.size();i++) {
      fprintf(stdout,"heap_before: %d\t%g\t%d\n", i, *(index_[i]), back_idx[i]);
    }
    fprintf(stdout,"\n");
    pop_heap(index_.size());
    pop_heap(index_.size()-1);
    for (i=0;i<values_.size();i++) {
      fprintf(stdout,"heap_after: %d\t%g\t%d\n", i, *(index_[i]), back_idx[i]);
    }
    fprintf(stdout,"\n");
    push_heap(index_.size()-2);
    push_heap(index_.size()-1);
    for (i=0;i<values_.size();i++) {
      fprintf(stdout,"heap_restored: %d\t%g\t%d\n", i, *(index_[i]), back_idx[i]);
    }
  }

  // Optimized: because n (# to sum) << s (size of heap),
  // best to pop the values, then push them back on, not remake the whole heap.
  // Silently lower n to s, if was greater.
  double SumTopN(int n) {
    int i;
    double t = 0.0;

    // Save the size of index_ on entry, for readability.
    int s = index_.size();
    if (n > s) {
      n = s;
    }
    if (n == 2) {
      int big;
      if (s > 2) {
	big = idx_a_lt_b(1, 2)? 2 : 1;
      }
      else {
	big = 1;
      }
      return (*(index_[0]) + *(index_[big]));
    }
    // Pop the top n value-indexes into last n locations of index_
    // summing into r as we go.
    for (i=0; i<n; i++) {
      pop_heap(s-i);
      t += *(index_[(s-1)-i]);
    }
    // Replace the n value-indexes in last-popped-first-pushed order
    for (i=n; i>0; i--) {
      push_heap(s-i);
    }
    return(t);
  }

private:
  inline bool ptr_a_lt_b(const double* a, const double* b)
  {
    return (*a < *b);
  }
  inline bool idx_a_lt_b(const int a, const int b)
  {
    return ptr_a_lt_b(index_[a], index_[b]);
  }
  // Heap functions following _Algorithms_, CLR 1990
  // adjusted to 0-based indexing.
  inline int parent(const int i) {
    return ((i + 1)/2) - 1;
  }
  inline int left(const int i) {
    return 2*(i+1) - 1;
  }
  inline int right(const int i) {
    return 2*(i+1);

  }

  inline void exchange(int x, int y) {
    // swap the pointers in index_
    // *and* update the back_idx ints saying
    // where the pointer to each values_ element is
    //
    // before:
    // index_[x] -> values_[xloc]
    // back_idx[xloc] = x
    // index_[y] -> values_[yloc]
    // back_idx[yloc] = y
    //
    // after:
    // index_[x] -> values_[yloc]
    // back_idx[yloc] = x
    // index_[y] -> values_[xloc]
    // back_idx[xloc] = y
    
    // 1. store values temporarily
    double* tmp_index = index_[x];
    int xloc = index_[x] - &(values_[0]);
    int yloc = index_[y] - &(values_[0]);
    // 2. clobber x values with y's
    index_[x] = index_[y];
    back_idx[yloc] = x;
    // 3. clobber y values with x's
    index_[y] = tmp_index;
    back_idx[xloc] = y;
  }
  void heapify_down(int j, int fence) {
    // Move j index_ downwards until pointed-to
    // value is not less than that of either kid.
    int big;
    int c1 = left(j);
    int c2 = right(j);
    if (c1 < fence && idx_a_lt_b(j, c1)) {
      big = c1;
    }
    else {
      big = j;
    }
    if (c2 < fence && idx_a_lt_b(big, c2)) {
      big = c2;
    }
    if (big != j) {
      exchange(j, big);
      // previous j index_ now moved to location
      // of its former child index_, big;
      // percolate downwards
      heapify_down(big, fence);
    }
  }
  inline void heapify_up(int j) {
    while (j > 0 && idx_a_lt_b(parent(j), j)) {
      exchange(j, parent(j));
      j = parent(j);
    }
  }
  // Pop largest value into last element and reduce fence value
  // (caller-enforced) so it's just outside.
  // heapify_down the remaining contents, since the root is
  // unlikely to still be largest!
  // Don't check for underflow: internal use only.
  inline void pop_heap(int fence) {
    exchange(0, fence-1);
    heapify_down(0, fence-1);
  }
  // Caller puts to-be-pushed value just outside the fence;
  // we heapify_up to move it inside with valid heap properties
  inline void push_heap(int fence) {
    // heapify_up doesn't need a fence,
    // but the fence value is one past the end of heap, being added,
    // so the one to look at and potentially move up
    heapify_up(fence);
  }
  // apply heapify_updown only where heap occupies full vectors,
  // so fence value isn't needed
  inline void heapify_updown(int j) {
    // Could apply checks to see which of heapify_up or heapify_down
    // would actually do something, but they will safely check for themselves
    // in constant time.
    heapify_up(j);
    heapify_down(j, index_.size());
  }
  void make_heap() {
    for (int i = parent(index_.size()-1); i >= 0; i--) {
      heapify_down(i, index_.size());
    }
  }
  std::vector<double> values_; 
  std::vector<double*> index_;
  std::vector<int> back_idx;
};

#endif
