#ifndef CHICAGO_SCAFFOLDS_H
#define CHICAGO_SCAFFOLDS_H

#include "stdio.h"
#include <map>
#include <vector>
#include <iostream>
#include <string>
#include <string.h>
#include <fstream>
#include "bam_header_info.h"
//#include "chicago_edge_scores.h"
#include "segment_set.h"
#include "paired_read_likelihood_model.h"

//class to represent a particular "join", or linkage across a gap between a pair of contig ends.
struct Join {
  uint32_t id1;
  uint32_t id2;
  int8_t o1;
  int8_t o2;
  Join(): id1(0), id2(0), o1(0),o2(0) {};
  Join(uint32_t id1, uint32_t id2, int8_t o1, int8_t o2): id1(id1), id2(id2), o1(o1),o2(o2) {};
  Join(const ContigPair& cp) : id1(cp.target_1), id2(cp.target_2), o1( (cp.strand_1==0)? '+' :'-'  ), o2( (cp.strand_2==0)? '+' :'-') {};
  bool NotNull() const { return((o1=='+') || (o1=='-'));};
  bool Empty() const { return(!((o1=='+') || (o1=='-')));};
};

std::ostream& operator<<(std::ostream& out, const Join& j) {
  out << "link: " << j.id1 << "," << j.id2 << " " << char(j.o1) << char(j.o2)  ;
  return out;
}

struct ScaffoldMergeMove {
  int8_t type; //0 for end-to-end; 1 for interc.
  double score;
  double score_orig=0;
  int num_links_used;
  int num_links_orig=0;
  Join join1;
  Join join2;
  Join breakage;

  ScaffoldMergeMove() : join1( Join() ), join2( Join()), breakage(Join()), type(0), score(0.0) {};
  ScaffoldMergeMove(const ContigPair& cp, double s) : join1( Join(cp) ), join2( Join()), breakage(Join()), type(0), score(s) {};
  void SetInterc() { type=1; }
  bool IsInterc() { return(type==1); }
  void SetJoin() { type=0; }
  bool IsJoin() { return(type==0); }
  void AddJoin( Join j ) {
    //std::cout << "adding a join:\n";
   //std::cout << "join1=" << join1.id1 << "\t" << join1.id2 << "\t" << char(join1.o1) << "\t" << char(join1.o2) << "\n";
    if (join1.Empty()) {
      join1 = j;
    } else if (join2.Empty()) {
      join2 = j;
    } else {
      ErrorMsg::Error("Tried to add three joins to the same move;\n");      
      assert(1==0);
    }
  }
  void SetBreak( Join j ) {
    type=1;
    breakage=j;
  }
};


std::ostream& operator<<(std::ostream& out, const ScaffoldMergeMove& move) {
  if (move.type==0) {
    out << "join:   "<< move.score << " " << move.join1 ;
  } else {
    out << "interc: "<< move.score << " " << move.join1 << " " << move.join2 << " / break " << move.breakage ;
  }
  return out;
}

bool operator< (const ScaffoldMergeMove& lhs, const ScaffoldMergeMove& rhs) {
    return ( lhs.score < rhs.score ); 
};

/// Node in the path for higher scaffolds.
struct TargetRange {
  static BamHeaderInfo header;
  uint32_t id;             ///< unique id of this "broken contig"
  int32_t scaffold_id;    ///< id (same id as chicago links data structure), -1 for gap
  int32_t target_id;      ///< id (same id as chicago links data structure), -1 for gap
  int32_t target_start;   ///< starting position in the target for range.
  int32_t target_end;     ///< ending position in the target for range
  int32_t scaffold_start; ///< start of range in parent scaffold 
  int32_t scaffold_end;   ///< end of range in parent
  int32_t n_shotgun;      ///< number of shotgun reads aligned in this span
  // Put smallest members last, for smaller overall struct footprint
  int8_t  orientation;    ///< + for forward strand, - for reverse, 
  TargetRange() : id(0), target_id(-1), target_start(-1), target_end(-1), 
		  scaffold_start(-1), scaffold_end(-1), n_shotgun(0), orientation('+') {} ;
  TargetRange(int n) : id(n), target_id(-1), target_start(-1), target_end(-1), 
	          scaffold_start(-1), scaffold_end(-1), n_shotgun(0), orientation('+') {};
  void Init() {
    target_id = target_start = target_end = scaffold_start = scaffold_end = -1;
    n_shotgun = 0;
    orientation = 'x';
  }

  inline int len() const {
    return(target_end - target_start);
  }
  
  void WriteHiRiseName(std::ostream& out,  const BamHeaderInfo&  header ) const {
      out << header.GetTargetName(target_id) << "_" << target_start+1 ;
  }

  void WriteHiRiseNameLeft(std::ostream& out,  const BamHeaderInfo&  header ) const {
      out << header.GetTargetName(target_id) << "_" << target_start+1 ;
      if (orientation == '+') {
	out << ".5";
      } else {
	out << ".3";
      }
  }

  void WriteHiRiseNameRight(std::ostream& out,  const BamHeaderInfo&  header ) const {
      out << header.GetTargetName(target_id) << "_" << target_start+1 ;
      if (orientation == '+') {
	out << ".3";
      } else {
	out << ".5";
      }
  }

  void Flip() {
    if (orientation == char('-')) {
      orientation = char('+');
    } else if (orientation == char('+')) {
      orientation = char('-');
    } else {
      ErrorMsg::Error("Error flipping strand (%c) %d;\n",char(orientation),int(orientation));      
    }
    //    std::cout << orientation << "\n";
  }

  /// compare based on target name and then on position within target.
  /// @todo - drop target name comparison?
  inline bool operator< (const TargetRange& rhs) const {
    if (target_id < rhs.target_id) 
      return true;
    if (target_id > rhs.target_id) 
      return false;
    return target_start < rhs.target_start;
  }
};

BamHeaderInfo TargetRange::header = BamHeaderInfo();

#if 0
std::ostream& operator<<(std::ostream& out, const TargetRange& seg) {
  // int bsize=1024;
  char* sbuff;
  sbuff=new char[1024];
  sprintf(sbuff,"C%d(%d/%d:%d-%d/%c %d-%d)|",seg.id,seg.scaffold_id,seg.target_id,seg.target_start,seg.target_end,char(seg.orientation),seg.scaffold_start,seg.scaffold_end);
  out.write( sbuff , strlen(sbuff) );
  return out;
}
#else
std::ostream& operator<<(std::ostream& out, const TargetRange& seg) {
  // int bsize=1024;
  //  char* sbuff;
  //sbuff=new char[1024];
  //sprintf(sbuff,"C%d(%d/%d:%d-%d/%c %d-%d)|",seg.id,seg.scaffold_id,seg.target_id,seg.target_start,seg.target_end,char(seg.orientation),seg.scaffold_start,seg.scaffold_end);
  //out.write( sbuff , strlen(sbuff) );
  if (seg.orientation=='+') {
    out <<"P " << seg.scaffold_id << " " << TargetRange::header.GetTargetName(seg.target_id) <<" "<< seg.target_start+1 << " 5 " << seg.scaffold_start << " " << seg.len() << "\n";
    out <<"P " << seg.scaffold_id << " " << TargetRange::header.GetTargetName(seg.target_id) <<" "<< seg.target_start+1 << " 3 " << seg.scaffold_end << " " << seg.len() << "\n";

  } else {
    out <<"P " << seg.scaffold_id << " " << TargetRange::header.GetTargetName(seg.target_id) <<" "<< seg.target_start+1 << " 3 " << seg.scaffold_start << " " << seg.len() << "\n";
    out <<"P " << seg.scaffold_id << " " << TargetRange::header.GetTargetName(seg.target_id) <<" "<< seg.target_start+1 << " 5 " << seg.scaffold_end << " " << seg.len() << "\n";

  }
  return out;
}

#endif

/**
 * Connected ranges to make a path of a target through assembly.
 * A scaffold is a path through the defined input space.
 */
class Path {
public:
  std::vector<TargetRange> &GetPath() { return path_; }
  std::vector<TargetRange> const &GetPathConst() const { return path_; }
  std::vector<TargetRange>::const_iterator GetPathIter() const { return path_.begin(); }
  std::vector<TargetRange>::const_iterator GetPathEnd()  const { return path_.end()  ; }

  std::vector<TargetRange>::const_iterator begin() { return path_.begin(); }
  std::vector<TargetRange>::const_iterator end()  { return path_.end()  ; }
  TargetRange& operator[](uint i) {return path_[i];}
  const TargetRange& operator[](uint i) const {return path_[i];}
  size_t size() const { return path_.size();}

  void Append(const TargetRange &span) { path_.push_back(span); };
  int ncontigs() const { return(path_.size()); }
  // concat
  // break
  static const int kGapId = -1;

  void Reset() {
    path_.resize(0);
  }

  uint32_t HeadId() {
    return(path_[0].id);
  }

  uint32_t TailId() {
    return(path_.back().id);
  }

  //Is the 5' end of this scaffold involved in this join?
  bool HeadMatches(const Join join) const {
    if ((join.id1==path_[0].id) && (join.o1!=path_[0].orientation)) {
      return true;
    }
    if ((join.id2==path_[0].id) && (join.o2==path_[0].orientation)) {
      return true;
    }
    return false;
  }

  //Is the 3' end of this scaffold involved in this join?
  bool TailMatches(const Join join) const {
    if ((join.id1==path_.back().id) && (join.o1==path_.back().orientation)) {
      return true;
    }
    if ((join.id2==path_.back().id) && (join.o2!=path_.back().orientation)) {
      return true;
    }
    return false;
  }

  bool IncludesJoin(const Join join) const {
    for(int i=0;i<int(path_.size())-1;i++) {
      if ( (join.id1==path_[i].id) && (join.id2==path_[i+1].id) && (join.o1==path_[i].orientation) && (join.o2==path_[i+1].orientation) ) {return true;}
      if ( (join.id2==path_[i].id) && (join.id1==path_[i+1].id) && (join.o2!=path_[i].orientation) && (join.o1!=path_[i+1].orientation) ) {return true;}
    }
    return false;
  }

  int SeqLength() const {
    return(path_.back().scaffold_end);
  }

  const TargetRange& GetContig(int i) const {
    return path_[i];
  }

  int len() const {
    return path_.size();
  }

  void FlipLastSeg() {
    
    TargetRange &b = path_.back();
    //    std::cout << "flip this seg:" << b << "\n";
    b.Flip();
    //std::cout << "post flip:" << b << "\n";
  }

  void ResetScaffoldCoords(int gapsize) {
    int x=0;
    std::vector<TargetRange>::iterator seg = path_.begin() ; 
    std::vector<TargetRange>::iterator end = path_.end()   ;
    
    for (;seg!=end;seg++) {
      seg->scaffold_start = x;
      x += seg->len();
      seg->scaffold_end   = x;
      x += gapsize;
      
    }
  }

  void ResetScaffoldCoords(int scaffold, int gapsize) {
    int x=0;
    std::vector<TargetRange>::iterator seg = path_.begin() ; 
    std::vector<TargetRange>::iterator end = path_.end()   ;
    
    for (;seg!=end;seg++) {
      seg->scaffold_id = scaffold;
      seg->scaffold_start = x;
      x += seg->len();
      seg->scaffold_end   = x;
      x += gapsize;
      
    }
  }

  int IdAtPosition(int g) const {
    assert(g>=0);
    assert(g<int(path_.size()));
    return path_[g].id;
  }

private:
  int32_t id_;
  // @todo - should keep this sorted by scaffold coordinates
  // this could be a linked list for faster insert, but would stil have to update 
  // all coordinates
  std::vector<TargetRange> path_;

};

std::ostream& operator<<(std::ostream& out, Path& path) {
  //  out << "path: [";
  int i;
  std::vector<TargetRange>::const_iterator     seg =path.GetPathIter();
  std::vector<TargetRange>::const_iterator     rend=path.GetPathEnd();
  for (;seg!=rend;seg++) {
    out << *seg ;// <<", ";
  }
  //out << "]";
  return out;
}

class PathSet {

public:

  PathSet() : contig_counter_(0) {};
  PathSet(BamHeaderInfo header) : contig_counter_(0) { 
    TargetRange::header = header; 
  };

  void SetHeader( BamHeaderInfo header  ) {
    TargetRange::header = header;
  }

  int32_t ShotgunCounted() {
    return shotgun_counted_;
  }

  std::map<int32_t,Path>::const_iterator GetScaffoldIter() { return scaffolds_.begin(); }
  std::map<int32_t,Path>::const_iterator GetScaffoldEnd()  { return scaffolds_.end()  ; }

  std::map<int32_t,Path>::const_iterator begin() { return scaffolds_.begin(); }
  std::map<int32_t,Path>::const_iterator end()  { return scaffolds_.end()  ; }
  const std::map<int32_t,Path>::const_iterator begin() const { return scaffolds_.begin(); }
  const std::map<int32_t,Path>::const_iterator end()  const { return scaffolds_.end()  ; }
  size_t size()  { return scaffolds_.size()  ; }

  const Path& GetScaffold(int s) const {
    std::map<int32_t, Path>::const_iterator path_it = scaffolds_.find(s);
    if (path_it==scaffolds_.end()) {
      ErrorMsg::Error("Couldn't find scaffold %d\n",s);
    } 
    return( path_it->second);
  }

  const Path& FindScaffold(int scaffold_id) const {
    std::map<int32_t, Path>::const_iterator path_it = scaffolds_.find(scaffold_id);
    if (path_it == scaffolds_.end()) {
      //      return(Path());
      std::cout << "Failed to find scaffold "<< scaffold_id<<"\n";
      assert(1==0); // scaffold not found;
    }
    return( path_it->second );
  }

  int ScaffoldHeadId(int scaffold_id) const {
    Path p = FindScaffold(scaffold_id);
    return(p.HeadId());
  }

  bool AcceptMove( const ScaffoldMergeMove move, std::map<int,int>& scaffold_mod_round, int round ) {

    // for a move expressed in terms of broken contig ends, figure out which scaffolds 
    // contain these ends at the moment, figure out the orientations and how to merge them into a new scaffold.

    //Todo:  add more checks to make sure this is a still-valid move.
    
    uint32_t c1,c2,c3,c4;
    int s1,s2,r,d;
    Path p1,p2;
    int g=0;
    bool flipp2=false;
    bool validation_fails=false;
    std::ostringstream outstream;

    if (move.type==0) {
      c1 = move.join1.id1;
      c2 = move.join1.id2;
      s1 = ContigScaffold(c1);
      s2 = ContigScaffold(c2);
      p1 = FindScaffold(s1);
      p2 = FindScaffold(s2);
      //      assert(s1!=s2);
      r=s1; d=s2;
      if (p1.HeadMatches(move.join1)) { 
	g=0;
	if (p2.HeadMatches(move.join1)) {

	  outstream << "join end to end scaffolds? <"<<s1<<"-  -"<<s2<<">\t"<< move.score <<"\n";
	  flipp2=true;
	} else if (p2.TailMatches(move.join1)) {
	  outstream << "join end to end scaffolds? <"<<s1<<"-  <"<<s2<<"-\t"<< move.score<<"\n";

	} else {
	  //	  assert(1==0);
	  validation_fails=true;
	}
      } else if (p1.TailMatches(move.join1)) {
	g=p1.len();
	if (p2.HeadMatches(move.join1)) {

	  outstream << "join end to end scaffolds? -"<<s1<<">  -"<<s2<<">\t"<< move.score<<"\n";	  
	} else if (p2.TailMatches(move.join1)) {
	  outstream << "join end to end scaffolds? -"<<s1<<">  <"<<s2<<"-\t"<< move.score<<"\n";	  

	  flipp2=true;
	} else {
	  //	  assert(1==0);
	  validation_fails=true;
	}
      } else {
	//	assert(1==0);
	validation_fails=true;
      }
      //      std::cout << "join end to end scaffolds -"<<s1<<">  <"<<s2<<"\t"<< move.score<<"-\n";	  
      
    } else if (move.type==1) {
      s1 = ContigScaffold(move.breakage.id1);
      s2 = ContigScaffold(move.breakage.id2);
      assert(s1==s2);
      r=s1;

      //      c1 = move.join1.id1;
      //c2 = move.join1.id2;

      //c3 = move.join2.id1;
      //c4 = move.join2.id2;

      // ----------------*    *--------*   *---------
      //                 c1  c2       c3   c4

      s1 = ContigScaffold(move.join1.id1);
      s2 = ContigScaffold(move.join1.id2);
      if (s1==r) {
	d=s2; 
	c1=move.join1.id1; 
	c2=move.join1.id2; 
      } else if (s2==r) {
	d=s1; 
	c1=move.join1.id2; 
	c2=move.join1.id1; 
      } else {assert(1==0);};


      p1 = FindScaffold(r);
      p2 = FindScaffold(d);
      if (!( ((p2.HeadMatches(move.join1))&&(p2.TailMatches(move.join2))) ||
	     ((p2.HeadMatches(move.join2))&&(p2.TailMatches(move.join1))) )) {
	validation_fails=true;
	return false;
      }
      if (!p1.IncludesJoin(move.breakage)) {validation_fails=true; return false;}

      outstream << "intercalating join: "<<r<<" <- "<<d<<"\n";      

      s1 = ContigScaffold(move.join2.id1);
      s2 = ContigScaffold(move.join2.id2);
      if (s1==r) {                //  -----DDD--------*c3    c4*-----------RRR----
	c4=move.join2.id1;   
	c3=move.join2.id2; 
      } else if (s2==r) {
	c4=move.join2.id2;   
	c3=move.join2.id1; 
      } else {assert(1==0);};

      bool flip_s1=false;
      bool flip_s2=false;
      for(g=0;g<p1.len();g++) {
	TargetRange t = p1.GetContig(g);
	if ( (t.id == c1) || (t.id == c4) ) {
	  g++;
	  if (t.id==c4) {flip_s1=true;} // c4 comes first. 
	  break;
	}
      }

      //check whether the 'donor' is flipped:
      if (c2==p2.HeadId()) { flip_s2 = false;  } else if (c2==p2.TailId()) {flip_s2 = true; } else { assert(1==0); }
     
      //std::cout << " "<<s1<<"+"<<s2<<" after " <<g <<" contigs";
      if (flip_s1 ^ flip_s2) { 
	//std::cout << ", flip s2"; 
	flipp2=true;
      }
      //std::cout << ".\n";

    }
    if (r==d) {return false;}

    if (validation_fails) {
      std::cout << "Validation checks failed\n";
      return false;
    }
    //    outstream << "accepted contigs sc1:\t";
    /*    for (int g_pos=0;g_pos<g;g_pos++) {
      outstream << p1.IdAtPosition(g_pos) << ",";
    }
    outstream << "\n";

    outstream << "accepted contigs sc2:\t";
    for (int g_pos=g;g_pos<;g_pos++) {
      outstream << p1.IdAtPosition(g_pos) << ",";
    }
    outstream << "\n";
    */
    std::cout << outstream.str();

    //Construct the merged scaffold
    Path newpath;
    std::vector<int> dummy;
#if(DEBUG_ENABLED) 
    std::cout << "#debug: starting new accepted join " << "\n";

#endif
    MakeIntercPath(p1,p2,newpath,dummy,g,flipp2,1000, true);
    //std::cout << newpath <<"\n";

    //Replace the path r in the paths_ map.
    scaffolds_.erase(r);
    scaffolds_.insert(std::make_pair(r,newpath));

    //Update ScaffoldContig for all the contigs in d (donor).
    std::vector<TargetRange>::const_iterator     Iseg =p2.GetPathIter();
    std::vector<TargetRange>::const_iterator     Irend=p2.GetPathEnd();
    for (; Iseg!=Irend; Iseg++) {
      contig_scaffold_map_[Iseg->id]=r;
#if(DEBUG_ENABLED) 
	std::cout << "#debug: relabel " << Iseg->id << " from " << d << " to " << r<< "\n";
#endif
    }
#if(DEBUG_ENABLED) 
    // totally would mess up gapsize changing, but this doesn't seem to be implemented yet anyway
      std::cout << "#debug: score: " << move.score << "\n";
      std::cout << "#debug: score_orig: " << move.score_orig << "\n";
      std::cout << "#debug: num_links: " << move.num_links_used << "\n";
      std::cout << "#debug: num_links_orig: " << move.num_links_orig << "\n";
      std::cout << "#debug: scaffold_id: " << r << "\n";

#endif   
    scaffold_mod_round.erase(r);
    scaffold_mod_round.erase(d);
    scaffold_mod_round[r]=round;
    scaffold_mod_round[d]=round;

    //Erase the inserted scaffold
    scaffolds_.erase(d);
    
    return true;
      
  }
  
  void MakeIntercPath(const Path &receiver, const Path &inserted, Path &newpath, std::vector<int> &group, int gapno,bool flip=false,int gapsize=1000, bool accepted=false) const {
    int i;
    TargetRange r;
    int n1 = receiver.ncontigs() ;
    int n2 = inserted.ncontigs() ; 
    int ncontigs = n1+n2; 
    newpath.Reset();
    group.resize(ncontigs);
    int rid = 0;

    std::vector<TargetRange>::const_iterator     seg =receiver.GetPathIter();
    std::vector<TargetRange>::const_iterator     rend=receiver.GetPathEnd();

    rid=seg->scaffold_id;

    int len_s0 = 0;
    int len_s1 = 0;
    int len_s2 = 0;
#if(DEBUG_ENABLED)
    if(accepted) {
      std::cout << "#debug: ";
    }
    #endif
    for (i=0; i<gapno; i++ , seg++) {
      group[i]=0 ;
      newpath.Append(*seg);      
#if(DEBUG_ENABLED)
      if( accepted) {
	std::cout << "seg0:"  << (*seg).target_id << ":" << (*seg).orientation  << ":"
		  << (*seg).target_start << ":" << (*seg).target_end << ":" << (*seg).id 
		  << ":" << (*seg).scaffold_start << ":" << (*seg).scaffold_end << "," ;
	len_s0 += (*seg).len();
      }
      #endif
    }
    if (!flip) {
      std::vector<TargetRange>::const_iterator     Iseg =inserted.GetPathIter();
      std::vector<TargetRange>::const_iterator     Irend=inserted.GetPathEnd();
      for (; Iseg!=Irend; Iseg++) {
	group[i]=2 ;
      //	group.insert ( std::pair<int,int>(i,2) );
	i++;
	newpath.Append(*Iseg);     
#if(DEBUG_ENABLED)
	if( accepted) { 
	  std::cout << "seg1:" << (*Iseg).target_id << ":" << (*Iseg).orientation   << ":"
		    << (*Iseg).target_start << ":" << (*Iseg).target_end << ":" << (*Iseg).id 
		    << ":" << (*Iseg).scaffold_start << ":" << (*Iseg).scaffold_end << "," ;
	  len_s1 += (*Iseg).len();
	}
	#endif
      }
    } else {
      std::vector<TargetRange>::const_iterator     Iseg =inserted.GetPathEnd();
      std::vector<TargetRange>::const_iterator     Irend=inserted.GetPathIter();
      Iseg--;
      for (; Iseg>=Irend; Iseg--) {
	group[i]=2 ;
	//	group.insert ( std::pair<int,int>(i,2) );
	i++;
	newpath.Append(*Iseg);      
	//std::cout << "seg2-: " << *Iseg << "\n";
	newpath.FlipLastSeg();
#if(DEBUG_ENABLED)
	if( accepted) {
	  std::cout << "seg1:" << (*Iseg).target_id << ":" << (*Iseg).orientation   << ":"
		    << (*Iseg).target_start << ":" << (*Iseg).target_end <<  ":" << (*Iseg).id 
		    << ":" << (*Iseg).scaffold_start << ":" << (*Iseg).scaffold_end << "," ;
	  len_s1 += (*Iseg).len();
	}
       #endif
      }

    }

    for (; seg!=rend; seg++) {
      group[i]=1 ;
      //group.insert ( std::pair<int,int>(i,1) );
      i++;
      newpath.Append(*seg);      
#if(DEBUG_ENABLED)
      if( accepted) { 
	std::cout << "seg2:" << (*seg).target_id <<":"<< (*seg).orientation   << ":"
		  << (*seg).target_start << ":" << (*seg).target_end <<  ":" << (*seg).id 
		  << ":" << (*seg).scaffold_start << ":" << (*seg).scaffold_end << "," ;
	len_s2 += (*seg).len();
      }
      #endif
      //std::cout << "seg1: " << *seg << "\n";
      //std::cout << newpath << "\n";
    }
    newpath.ResetScaffoldCoords(rid,gapsize);
#if(DEBUG_ENABLED)
    if( accepted) { 
      std::cout << "\n";
      std::cout << "#debug: numcontigs: " << n1 <<  "\t" << n2 <<  "\t" << ncontigs << "\n";
      std::cout << "#debug: seq length: " << len_s0 << "\t" << len_s1  << "\t" << len_s2 << "\t" << newpath.SeqLength() << "\n";
      std::cout << "#debug: flip: " << flip << "\n";
    }
#endif
  }


  void ParseFromHRA(const BamHeaderInfo &header, const std::string &file_name) {
    std::ifstream in(file_name.c_str());
    if (!in.good()) { 
      ErrorMsg::Error("Couldn't open file %s\n", file_name.c_str());
    }
    std::cout << "Parsing HRA file " << file_name.c_str() << "\n";
    //max_target_id_=-1;
    max_scaffold_=0;
    scaffold_length_.clear();
    scaffold_nshot_.clear();
    shotgun_counted_ = 0;

    max_target_id_=header.target_names_.size();
    std::string line;
    while (in.good()) {
      std::getline(in, line);
      if (line.length() > 3) {
	if (line[0] == 'P') {
	  // make a piece of path for scaffold
	  HandlePLine(header, in, line);
	}
	else if (line[0] == 'D') {
	  // insert a masked region
	  HandleDLine(header, line);
	}
	else if (line[0] == 'L') {
	  // make sure target length is correct
	  HandleLLine(header, line);
	}
	else {
	  continue;
	}
      }
    }
    in.close();
    //IndexMaskedRegions();

    std::sort(contig_segments_.begin(),contig_segments_.end());
    IndexContigSegments();
    masked_segments_.ComputeCoverage();
    std::cout << "Done with HRA file " << file_name.c_str() << "\n";
  }

  std::pair<int,int> N50() {
    auto i= GetScaffoldIter();   // { return scaffolds_.begin(); }
    auto end= GetScaffoldEnd();  //  { return scaffolds_.end()  ; }
    std::vector<int> lengths;
    double l = 0;
    double totalL = 0;
    for(;i!=end;i++) {
      l = i->second.SeqLength();
      totalL += l;
      lengths.push_back( l );
    }
    //    std::cout << "total length: " << totalL << "\n";
    //std::cout << "n scaffolds: " << lengths.size() << "\n";

    std::sort(lengths.rbegin(),lengths.rend());   //reverse sort
    std::sort(lengths.begin(),lengths.end());
    uint32_t ii=0;
    double s=0;
    ii=0;
    while ((ii<lengths.size() ) && (s<totalL*0.5)) {
      ii++;
      s+=lengths[ii];
    }
    //std::cout << "ii: " << ii << "\n";
    //std::cout << "s: " << s << "\n";
    //std::cout << "s/t: " << (s/totalL) << "\n";

    return std::make_pair(int(lengths[ii]),ii);
  }

  // *** UpdateScaffoldsReads ***
  // Update (shotgun) read counts for all scaffolds.
  void UpdateScaffoldReads() {
    for (std::map<int32_t, Path>::iterator it = scaffolds_.begin(); it != scaffolds_.end(); ++it) {
      // we want the Path (it->second) for each mapping pair
      scaffold_nshot_[it->first] = 0;
      UpdateOneScaffoldsReads(it->second);
    }
  }
  
  // *** UpdateOneScaffoldsReads(int32_t scafid) ***
  // Copy (shotgun) counts from contig_segment_ TargetRange's into a Scaffold's Path's TargetRange's
  // 
  // For each TargetRange in the Path
  //     find the corresponding TargetRange in contig_segments_ through binary search
  //     copy the n_shotgun count
  //
  // void UpdateOneScaffoldsReads(int32_t scafid) {
  void UpdateOneScaffoldsReads(Path &scaf_path) {
    for (std::vector<TargetRange>::iterator it = scaf_path.GetPath().begin();
	 it != scaf_path.GetPath().end();
	 ++it) { // iterating through target ranges of the Path
      Base tstart = Base(it->target_id, it->target_start);
      int cseg_id = ContigSegBsearch(tstart);
      assert (!(contig_segments_[cseg_id] < *it || *it < contig_segments_[cseg_id]));
      it->n_shotgun = contig_segments_[cseg_id].n_shotgun;
      scaffold_nshot_[contig_segments_[cseg_id].scaffold_id] += it->n_shotgun;
    }
  }

  void FakeContigSegShotgun() {
    uint32_t segment_vec_index = 0;

    shotgun_counted_ = 0;
    for (;
	 segment_vec_index < contig_segments_.size();
	 segment_vec_index++)
      {
	int32_t nshot = (contig_segments_[segment_vec_index].target_end 
			 - contig_segments_[segment_vec_index].target_start);
	contig_segments_[segment_vec_index].n_shotgun = nshot;
	scaffold_nshot_[contig_segments_[segment_vec_index].scaffold_id] += nshot;
	shotgun_counted_ += nshot;
      }
  }

  // *** CountContigSegShotgun ***
  // Scan whole BAM file to count reads in each TargetRange.
  //      modeled on: chicago_links.h:PopulateFromBam
  // Here, make use that reads in BAM file are sorted by Target & offset.
  // What about read overlapping multiple TargetRange's?
  //      Shouldn't be an issue for now, but...
  //	  Just in case, take range overlapping midpoint of read mapping.
  // Default libid=0, means ignore library IDs, but may pay attention in future.
  //      For now ignore always! Which means don't even include as an arg.
  //
  void CountContigSegShotgun(const std::string & bam_file, int8_t min_mapq = 20) {
    // , const int libid=0) {
    // Would be nice to be able to count by library, but not for now:
    // * need to have access to a libraries_ data structure as in ChicagoLinks class
    // * how would we count multiple libraries? more abstract to count

    // Unlike PopulateFromBam, don't need to store the reads, just count them
    // So no "Reserve space for reads"

    /// BAM
    bam1_t *b = bam_init1();
  
    INFO("Opening file %s.\n", bam_file.c_str());
    htsFile *bf = hts_open(bam_file.c_str(), "rb");
    assert(bf);
    
    // Parse out the target names from the header
    bam_hdr_t *header = sam_hdr_read(bf);
    // header_.Update( header );

    // Parse out the individual reads
    int32_t sam_count = 0, unmapped_count = 0, secondary_count = 0;
    int32_t bad_mapq = 0, duplicate = 0, counted = 0;
    uint32_t segment_vec_index = 0;
    int32_t missing_target = 0;

    while(sam_read1(bf, header, b) > 0) {
      sam_count++;
      // skip unmapped as uninteresting
      if (b->core.flag & BAM_FUNMAP) {
        unmapped_count++;
        continue;
      }
      if (b->core.flag & BAM_FSECONDARY) {
        secondary_count++;
        continue;
      }

      // Omit code to ignore reverse-end reads
      // Omit code to store ranges when end of target is reached

      // Adapt this code to identify the target & pos/offset
      // Need to identify middle of read? Stick with start of read, for now.
      // read.target_1     = b->core.tid;
      // read.pos_1        = b->core.pos;
      // read.sam_flag_1   = b->core.flag;
      // read.size_1       = b->core.l_qseq;
      // read.mapq_1       = b->core.qual;
      
      // Omit data for mate
      // Omit code for tags (ignore)
      // NM is #mismatches and MD is CIGAR string
      // What are xj & xt? Relate to Chicago junctions & read types.
      if (b->core.flag & BAM_FDUP) {
        duplicate++;
	continue;
      }
      if (b->core.qual < min_mapq) {
        bad_mapq++;
	continue;
      }

      // Count this in the appropriate TargetRange/contig_segments_[] n_shotgun
      // * Find the right TargetRange in contig_segments_ list
      for (; // Should be proceeding through BAM and contig_segments_ in tandem
	   segment_vec_index < contig_segments_.size()
	     && (contig_segments_[segment_vec_index].target_id < b->core.tid
		 || (contig_segments_[segment_vec_index].target_id == b->core.tid
		     && contig_segments_[segment_vec_index].target_end < b->core.pos));
	   segment_vec_index++) {
	// empty
      }
      // Shouldn't be here without the read being in the contig_sements_[segment_vec_index]
      // * Add to the count there
      if (!(contig_segments_[segment_vec_index].target_id == b->core.tid
	    && contig_segments_[segment_vec_index].target_start <= b->core.pos
	    && contig_segments_[segment_vec_index].target_end   >= b->core.pos)) {
	if (! missing_target) {
	  fprintf(stdout,
		  "\nMissing target for read (first example only):\n%s%d\n%s%lu\n%s%d\n%s%d\n",
		  "\tsegment_vec_index:       ", segment_vec_index,
		  "\tcontig_segments_.size(): ", contig_segments_.size(),
		  "\tb->core.tid:             ", b->core.tid,
		  "\tb->core.pos:             ", b->core.pos);
	  fflush(stdout);
	}
	missing_target++;
	continue;
      }
      contig_segments_[segment_vec_index].n_shotgun++;
      counted++;
      if (!(counted & 0x7FFFF)) { // print roughly every half-million reads, without cost of division
	fprintf(stdout,"#counted %0.2f million mapped reads        \r",float(counted)/1e6);
	fflush(stdout);
      }
    }
    INFO("Saw %d reads %d unmapped %d 2ndary %d bad_mapq %d duplicate %d counted %d missing_target\n", 
         sam_count, unmapped_count, secondary_count, bad_mapq, duplicate, counted, missing_target);
    shotgun_counted_ = counted;
    bam_destroy1(b);
    bam_hdr_destroy(header);
    hts_close(bf);
  }


  void ContigCoord(const Base scaffold_coord, Base* result, bool* flipped) const {
    int i = 0;
    *flipped=false;
    //    Path path = scaffolds_.find(scaffold_coord.target_id);
    std::map<int32_t, Path>::const_iterator path_it = scaffolds_.find(scaffold_coord.target_id);
    if (path_it == scaffolds_.end()) {
      result->target_id = scaffold_coord.target_id ;
      result->x         = scaffold_coord.x         ;      
    }
    Path path = path_it->second;

    //    std::vector<TargetRange>            ranges=path.GetPath();
    std::vector<TargetRange>::const_iterator     seg =path.GetPathIter();
    std::vector<TargetRange>::const_iterator     rend=path.GetPathEnd();
    //TODO:  speed up with a binary search for the right segment. XXX 
    for (;seg!=rend;seg++) {
      //      fprintf(stderr,"it: %d\t(%d:%d-%d;%c)\t[%d-%d]\t%d\n",seg->scaffold_id,seg->target_id,seg->target_start,seg->target_end,seg->orientation,seg->scaffold_start,seg->scaffold_end,scaffold_coord.x);
      if ( seg->scaffold_start <= scaffold_coord.x && scaffold_coord.x <seg->scaffold_end ) {break;}
    }
    if ( seg->orientation=='+' ) {
      result->target_id = seg->id;
      result->x         = seg->target_start + scaffold_coord.x-seg->scaffold_start;
      //      return(Base(seg->target_id,seg->target_start + scaffold_coord.x-seg->scaffold_start));
    } else {
      result->target_id = seg->id;
      result->x         = -scaffold_coord.x + seg->scaffold_end +contig_segments_[i].target_start;
      *flipped=true;
      //      return(Base(seg->target_id,-scaffold_coord.x + seg->scaffold_end +contig_segments_[i].target_start));
      //      return(Base(seg->target_id,scaffold_coord.x-));
    }
  }

  const SegmentSet & MaskedSegments() const {
    return(masked_segments_);
  }

  void ReadMaskedSegments(const std::string & bed_file, const BamHeaderInfo &header ) {
    masked_segments_.ReadBed(bed_file,header);    
  }

  // void update() {
  // }

  
  // *** ContigSegBsearch(const Base contig_coord) ***
  // Assumed about contig_segments_:
  // * and contig_segments_index_, as built by IndexContigSegments()
  // * No empty entries in contig_segments_ -- no -1 values in contig_segments_[0..(size-1)]
  // * Segments for a contig c are in
  //         contig_segments_[contig_segments_index[c]..contig_segments_index[c+1]]
  // 
  int ContigSegBsearch(const Base contig_coord) const {
    uint32_t left = contig_segments_index_[contig_coord.target_id];
    if (left < 0
	|| left >= contig_segments_.size()
	|| contig_segments_[left].target_id != contig_coord.target_id) {
      return -1;
    } 
    uint32_t right = (uint(contig_coord.target_id)+1 >= contig_segments_index_.size())
      ? contig_segments_.size()
      : contig_segments_index_[contig_coord.target_id+1];

    assert(left < right);
    // A bit of paranoia here, to be sure [left, right) region in contig_segments_
    // has segments all from the desired contig/target_id
    while (left < right-1
	   && contig_segments_[right-1].target_id != contig_coord.target_id) {
      right--;
    }
    // Now the binary search...
    while (left < right-1) {
      int mid = left+right / 2;
      if (contig_segments_[mid].target_start > contig_coord.target_id) {
	right = mid;
      }
      else {
	left = mid;
      }
    }
    // Now left == right - 1, and right is always the fencepost, never a candidate, so left is the correct index.
    // Double-check that it contains the desired coordinate!
    if (!( (contig_segments_[left].target_start <= contig_coord.x) && (contig_segments_[left].target_end > contig_coord.x) )) {
      return -1;
    }
    else {
      return left;
    }
  }
 
  // ScaffoldCoordBsearch(const Base contig_coord, Base* result) const {
  void ScaffoldCoordBsearch(const Base contig_coord, Base* result) const {
    // i === segment index
    int i = ContigSegBsearch(contig_coord);
    if (i < 0) {
      result->target_id = -1;
      result->x         = -1;
      return; // return (Base(-1,-1));
    }
    if ( contig_segments_[i].orientation == '+' ) {
      result->target_id = contig_segments_[i].scaffold_id ;
      result->x         = contig_segments_[i].scaffold_start + contig_coord.x-contig_segments_[i].target_start ;
    }
    else {
      result->target_id = contig_segments_[i].scaffold_id ;
      result->x         = contig_segments_[i].scaffold_end - (contig_coord.x-contig_segments_[i].target_start) ;
    }
    if (result->x<0) {
      fprintf(stdout, "why mapped coordinate < 0?\n"); 
      fprintf(stdout, "\tin:       (%d,%d)\n", contig_coord.target_id, contig_coord.x); 
      fprintf(stdout, "\tsegment:  %d:%d-%d %d:%d-%d %c\n",
	      contig_segments_[i].target_id,
	      contig_segments_[i].target_start,
	      contig_segments_[i].target_end,
	      contig_segments_[i].scaffold_id,
	      contig_segments_[i].scaffold_start,
	      contig_segments_[i].scaffold_end,
	      contig_segments_[i].orientation
	      ); 
      fprintf(stdout,"\tout:      (%d,%d)\n",result->target_id,result->x); 
      fflush(stdout);
    }
    assert(result->x >= 0);
    // return value already set
  }
  
  // ScaffoldCoord(const Base contig_coord, Base* result) const {
  void ScaffoldCoord(const Base contig_coord, Base* result, bool* flipped, bool broken_contig_coord=false) const {
    uint i=0;
    *flipped=false;
    // int j = 0;
    i = contig_segments_index_[contig_coord.target_id];

    if (i<0) {
      result->target_id = -1;
      result->x         = -1;
      return; //(Base(-1,-1));
    } 
    if (i>=contig_segments_.size()) {
      result->target_id = -1;
      result->x         = -1;
      return; //(Base(-1,-1));
      //return(Base(-1,-1));
    } 

    //fprintf(stdout, "%d:%d-%d %d:%d-%d %c\n",contig_segments_[i].target_id,contig_segments_[i].target_start,contig_segments_[i].target_end,
    //         contig_segments_[i].scaffold_id,contig_segments_[i].scaffold_start,contig_segments_[i].scaffold_end, contig_segments_[i].orientation);
    // j=i+1;
	//	fprintf(stdout, "%d:%d-%d %d:%d-%d %c\n",contig_segments_[j].target_id,contig_segments_[j].target_start,contig_segments_[j].target_end,
	//    contig_segments_[j].scaffold_id,contig_segments_[j].scaffold_start,contig_segments_[j].scaffold_end, contig_segments_[j].orientation );
    //find the right segment

    if (!(contig_segments_[i].target_id == contig_coord.target_id)) {
      result->target_id = -1;
      result->x         = -1;
      return; //(Base(-1,-1));
      //      return(Base(-1,-1));
    }
    //    fprintf(stdout,"map (%d,%d) %d (%d,%d)\n",contig_coord.target_id,contig_coord.x,i,contig_segments_[i].target_id,contig_segments_[i].target_start); fflush(stdout);
    while (
	   (i<contig_segments_.size()-1) &&
	   (contig_segments_[i+1].target_id == contig_coord.target_id) &&
	   (contig_segments_[i+1].target_start < contig_coord.x)
	   //	   contig_segments_[i].target_id==contig_coord.target_id &&
	   )     
      {

	i++; 
	//	fprintf(stdout, "%d:%d-%d %d:%d-%d %c\n",contig_segments_[i].target_id,contig_segments_[i].target_start,contig_segments_[i].target_end,
	//    contig_segments_[i].scaffold_id,contig_segments_[i].scaffold_start,contig_segments_[i].scaffold_end, contig_segments_[i].orientation);
	// j=i+1;
	//fprintf(stdout, "%d:%d-%d %d:%d-%d %c\n",contig_segments_[j].target_id,contig_segments_[j].target_start,contig_segments_[j].target_end,
	//    contig_segments_[j].scaffold_id,contig_segments_[j].scaffold_start,contig_segments_[j].scaffold_end, contig_segments_[j].orientation );

      }

    assert(contig_segments_[i].target_id == contig_coord.target_id);

    if (!( (contig_segments_[i].target_start <= contig_coord.x) && (contig_segments_[i].target_end > contig_coord.x) )) {
      result->target_id = -1;
      result->x         = -1;
      return; //(Base(-1,-1));
      //      return(Base(-1,-1));
    } 

    if (broken_contig_coord) {  
      result->target_id = contig_segments_[i].id;
      result->x         =  contig_coord.x-contig_segments_[i].target_start ;
      *flipped=false;
    } else {
    
      if ( contig_segments_[i].orientation=='+' ) {

	result->target_id = contig_segments_[i].scaffold_id ;
	result->x         = contig_segments_[i].scaffold_start + contig_coord.x-contig_segments_[i].target_start ; //seg->target_start + scaffold_coord.x-seg->scaffold_start;
	//      return(Base( contig_segments_[i].scaffold_id,contig_segments_[i].scaffold_start + contig_coord.x-contig_segments_[i].target_start ));	

      } else {
	result->target_id = contig_segments_[i].scaffold_id ;
	result->x         = contig_segments_[i].scaffold_end - (contig_coord.x-contig_segments_[i].target_start) ;
	*flipped=true;
	//      return(Base( contig_segments_[i].scaffold_id,contig_segments_[i].scaffold_end - (contig_coord.x-contig_segments_[i].target_start) ));
      }
    }
    if (result->x<0) {
      fprintf(stdout,"why mapped coordinate < 0?\n"); 
      fprintf(stdout,"\tin:       (%d,%d)\n",contig_coord.target_id,contig_coord.x); 
      fprintf(stdout,"\tsegment:  %d:%d-%d %d:%d-%d %c\n",contig_segments_[i].target_id,contig_segments_[i].target_start,contig_segments_[i].target_end,
	      contig_segments_[i].scaffold_id,contig_segments_[i].scaffold_start,contig_segments_[i].scaffold_end, contig_segments_[i].orientation
	      ); 
      fprintf(stdout,"\tout:      (%d,%d)\n",result->target_id,result->x); 
      
      fflush(stdout);
    }
    assert(result->x>=0);
  }

  inline int32_t NumScaffolds() { return scaffolds_.size(); }


  /*
  void SwitchToContigCoordinates(const PathSet& pathset) {
    MaskedSegment *seg;
    int i;
    Base b1,b2;
    for(i=0;i<segments_.size();i++){
      seg = &segments_[i];
      pathset.ContigCoord(Base(seg->target_id,seg->begin),&b1);
      pathset.ContigCoord(Base(seg->target_id,seg->end  ),&b2);
      //fprintf(stdout,"re-coordmap:\t(%d,%d)\t(%d,%d)\t-> (%d,%d)\t(%d,%d) \n",r->target_1,r->pos_1,r->target_2,r->pos_2,b1.target_id,b1.x,b2.target_id,b2.x);
      assert (b1.target_id==b2.target_id) ;
      if (b1.target_id != -1) {
	seg->target_id = b1.target_id ;
	seg->begin    = b1.x         ;
	seg->end      = b2.x         ;
      }
    }
    //SortByContigs();
  }

  */

  void IndexMaskedScaffoldSegments(bool broken_contig_coords=false) {
    //  std::vector<int> scaffold_mask_segments_index_;          ///< Starting positions in scaffold_mask_segments_ for each contig.
    //std::vector<MaskedSegment> scaffold_mask_segments_;


    CoverageStep s;
    //    CoverageStep s;
    int state=0;  // outside a masked segment
    int target_id,i;
    Base start,start_s;
    Base end,end_s;
    scaffold_mask_segments_.clear();    
    bool flipped;

    fprintf(stdout,"IndexMaskedScaffoldSegments:\n");
    fflush(stdout);

    for (i=0;i<int(masked_segments_.coverage_steps_.size());i++) {
      s = masked_segments_.coverage_steps_[i];
      if (state==0 && s.y>0) {
	state=1;
	start = Base(s.target_id,s.x);
      } else if (state==1 && s.y==0) {
	state=0;
	end = Base(s.target_id,s.x);

	ScaffoldCoord(start,&start_s,&flipped, broken_contig_coords);
	ScaffoldCoord(end  ,&end_s,&flipped, broken_contig_coords);

	if (start_s.target_id>=0 && start_s.target_id == end_s.target_id) {
	  if (start_s.x < end_s.x) {
	    scaffold_mask_segments_.push_back( MaskedSegment(start_s.target_id, start_s.x, end_s.x)  );
	    //fprintf(stdout,"mask seg: (%d,%d)-(%d,%d)\t-> %d : %d - %d\n",start.target_id,start.x,end.target_id,end.x,start_s.target_id, start_s.x, end_s.x);
	  } else {
	    scaffold_mask_segments_.push_back( MaskedSegment(start_s.target_id, end_s.x, start_s.x)  );
	    //fprintf(stdout,"mask seg: (%d,%d)-(%d,%d)\t-> %d : %d - %d\n",start.target_id,start.x,end.target_id,end.x,start_s.target_id, end_s.x, start_s.x);
	  }
	}

      }
    }


      //      fprintf(stdout,"coverage curve: %d\t%d\t%d\t%g\n",i,masked_segments_.coverage_steps_[i].target_id,masked_segments_.coverage_steps_[i].x,masked_segments_.coverage_steps_[i].y );
  
    fprintf(stdout,"Add gaps:\n");    fflush(stdout);

    std::map<int32_t, Path>::const_iterator path_it ;
    Path path ;
    std::vector<TargetRange>::const_iterator     seg ;
    std::vector<TargetRange>::const_iterator     segL;
    std::vector<TargetRange>::const_iterator     rend;

    // Don't add gaps...?
    if (!broken_contig_coords) {
    for (target_id=0;target_id<max_scaffold_;target_id++) {

      path_it = scaffolds_.find(target_id);
      if (path_it!=scaffolds_.end()) {
      path = path_it->second;
      seg =path.GetPathIter();
      segL=path.GetPathIter();
      rend=path.GetPathEnd();

      //TODO:  speed up with a binary search for the right segment. XXX 
      seg++;
      for (;seg!=rend;seg++) {
	//fprintf(stdout,"it: %d\t(%d:%d-%d;%c)\t[%d-%d]\n",seg->scaffold_id,seg->target_id,seg->target_start,seg->target_end,seg->orientation,seg->scaffold_start,seg->scaffold_end);
	//fprintf(stdout,"gap: scaffold %d %d-%d\n",seg->scaffold_id,segL->scaffold_end,seg->scaffold_start);
	fflush(stdout);

	//	if ( seg->scaffold_start <= scaffold_coord.x && scaffold_coord.x <seg->scaffold_end ) {break;}
	scaffold_mask_segments_.push_back( MaskedSegment(seg->scaffold_id, segL->scaffold_end,seg->scaffold_start)  );
	segL++;
      }
    }
    }    
    }
    std::sort(scaffold_mask_segments_.begin(),scaffold_mask_segments_.end());

    int max_scaffold = scaffold_mask_segments_[scaffold_mask_segments_.size()-1].target_id;
    scaffold_mask_segments_index_.resize(max_scaffold+1);
    std::fill(scaffold_mask_segments_index_.begin(), scaffold_mask_segments_index_.end(), 0);

    int lasts=-1;
    for (int i=0;i<int(scaffold_mask_segments_.size());i++) {
      if (scaffold_mask_segments_[i].target_id > lasts) {
	lasts=scaffold_mask_segments_[i].target_id;
	scaffold_mask_segments_index_[lasts] = i;
      }
    }
    /*
    for (int i=0;i<int(scaffold_mask_segments_index_.size());i++) {
      fprintf(stdout,"scaffold segments: %d %d\n",i,scaffold_mask_segments_index_[i]);
    }
    */
    //std::cerr <<"finished index\n";
  }
  void SwitchMaskToBrokenContigCoordinates() {
    Base b1,b2;
    bool flipped1;
    bool flipped2;
    for(auto seg=masked_segments_.begin();seg<masked_segments_.end();seg++){

      ScaffoldCoord(Base(seg->target_id,seg->start),&b1,&flipped1,true);
      // end is one past the length of the contig
      ScaffoldCoord(Base(seg->target_id,seg->end-1 ),&b2,&flipped2,true);

      //std::cerr << "id1: " << b1.target_id << "\nid2: " << b2.target_id << " orig_id: " << seg->target_id << " start: " << seg->start << " end: " << seg->end << "\n";
      if(b1.target_id!=b2.target_id) {
	//std::cerr << "id1: " << b1.target_id << "\nid2: " << b2.target_id << " orig_id: " << seg->target_id << " start: " << seg->start << " end: " << seg->end << "\n";
      }
      //assert (b1.target_id==b2.target_id) ;
      //      std::cerr << "start: " << seg->start << " new: " << b1.x << " end: " << b2.x << " id " << b1.target_id << "\n";
      if (b1.target_id != -1) {
	//std::cerr << "start: " << seg->start << " new: " << b1.x << " end: " << b2.x << " id " << b2.target_id << "\n";
	seg->target_id = b1.target_id ;
	seg->start     = b1.x         ;
	seg->end       = b2.x         ;

      }
    }
    masked_segments_.ComputeCoverage();
  }

  /*
  void SwitchToScaffoldCoordinates(const PathSet& pathset) {
    MaskedSegment *seg;
    //    Read *r;
    int i;
    Base b1,b2;
    for(i=0;i<reads_.size();i++){
      seg = &segments_[i];
      //      r = &reads_[i];
      pathset.ScaffoldCoord(Base(seg->target_id,seg->begin),&b1);
      pathset.ScaffoldCoord(Base(seg->target_id,seg->end  ),&b2);
      //      pathset.ScaffoldCoord(Base(r->target_1,r->pos_1),&b1);
      //pathset.ScaffoldCoord(Base(r->target_2,r->pos_2),&b2);
      //fprintf(stdout,"coordmap:\t(%d,%d)\t(%d,%d)\t-> (%d,%d)\t(%d,%d) \n",r->target_1,r->pos_1,r->target_2,r->pos_2,b1.target_id,b1.x,b2.target_id,b2.x);
      if (b1.target_id==b2.target_id) {	
	seg->target_id = b1.target_id ;
	seg->begin    = b1.x         ;
	seg->end      = b2.x         ;
      } else {
	//#todo -- a break happened in this masked segment; need to add multiple?  
      }
    }
    SortByContigs();
    //XXX
  }
  */

  std::vector<int> scaffold_mask_segments_index_;          ///< Starting positions in scaffold_mask_segments_ for each contig.
  std::vector<MaskedSegment> scaffold_mask_segments_;

  const std::vector<int>& ContigScaffoldMap() const {
    return contig_scaffold_map_;
  }

  inline int ContigScaffold(int span_id) const {
    return( contig_scaffold_map_[span_id] );
  }

  inline int ScaffoldLen(int scaffold_id) const {
    assert(scaffold_id < int(scaffold_length_.size()));
    return( scaffold_length_[scaffold_id] );
  } 
  inline int32_t ShotgunCnt(int scaffold_id) const {
    assert(scaffold_id < int(scaffold_length_.size()));
    return( scaffold_nshot_[scaffold_id] );
  } 

  //TODO
  inline int BrokenContigLen(int scaffold_id) const {
    //    if (i>=contig_segments_.size()) {
    assert(scaffold_id < int(target_ranges_.size()));
    assert(scaffold_id == target_ranges_[scaffold_id].id);
    return( target_ranges_[scaffold_id].len()  );
  } 
  //  inline int32_t ShotgunCnt(int scaffold_id) const {
  //  assert(scaffold_id < scaffold_length_.size());
  //  return( scaffold_nshot_[scaffold_id] );
  //} 
  
  void write_graphvis_scaffold_edges(std::ofstream& fh) {
    
    int i;

    std::map<int32_t,Path>::const_iterator     scaffold =GetScaffoldIter();
    std::map<int32_t,Path>::const_iterator     send     =GetScaffoldEnd();
    
    Path path = scaffold->second ;

    for (;scaffold!=send;scaffold++) {
      path = scaffold->second;

      TargetRange r;

      std::vector<TargetRange>::const_iterator     seg =path.GetPathIter();
      std::vector<TargetRange>::const_iterator     rend=path.GetPathEnd();
      for (;seg!=rend;seg++) {
	if (r.target_id!=-1) {
	  fh << r.id << " -- " << seg->id << " [color=\"red\"]\n";
	}
	//	out << *seg ;// <<", ";
	r = *seg;
      }
    }    
    //  out << "\n";
    //    return out;

  }

  void write_graphvis_node_info(std::ofstream& fh) {
    uint32_t i;
    TargetRange seg;
    for(i=0;i<target_ranges_.size();i++) {
      seg = target_ranges_[i];
      int len = (target_ranges_[i].target_end - target_ranges_[i].target_start);
      if ( len>=1000 ) {
	fh << seg.id << " [label=\"" << TargetRange::header.GetTargetName(seg.target_id)<< "_" <<seg.target_start+1   <<"\\n"<< int(len/1000) <<" kb\" shape=box]\n";
      }
      //use of deleted function ‘std::basic_ofstream<  if (seg.orientation=='+') {
      //    out <<"P " << seg.scaffold_id << " " << TargetRange::header.GetTargetName(seg.target_id) <<" "<< seg.target_start+1 << " 5 " << seg.scaffold_start << " " << seg.len() << "\n";

    }
  }
  
 private:

  void IndexContigSegments() {
    uint32_t i=0;
    int last_target=-1;
    int target_id;

    contig_segments_index_.resize(max_target_id_+1);
    std::fill(contig_segments_index_.begin(),contig_segments_index_.end(),-1) ;

    for(i=0;i<contig_segments_.size();i++) {
      target_id = contig_segments_[i].target_id;
      if (target_id > last_target) {
	contig_segments_index_[target_id]=i;
	last_target = target_id;
      }
    }
  }

  void HandleDLine(const BamHeaderInfo &header, std::string &line) {
    const int32_t offset = 2;
    int32_t target_start = 0, target_end = 0;
    char target_name[NAME_BUFFER_SIZE];
    int res = sscanf(line.c_str() + offset, "%s %d %d", target_name, &target_start, &target_end);
    if (res != 3) { ErrorMsg::Error("Expecting 3 arguments to match on D line but got %d for '%s'\n", res, line.c_str()); }
    if (target_end<=target_start) {return;}
    MaskedSegment &span = masked_segments_.NewSeg();

    //masked_regions_.resize(masked_regions_.size() + 1);
    //TargetRange &span = masked_regions_.back();
    
    header.GetTargetId(target_name, &span.target_id);
    span.start = target_start;
    span.end = target_end;
  }

  void HandleLLine(const BamHeaderInfo &header, std::string &line) {
    const int32_t offset = 2;
    int32_t target_size = 0;
    char target_name[NAME_BUFFER_SIZE];
    int res = sscanf(line.c_str() + offset, "%s %d", target_name, &target_size);
    if (res != 2) { ErrorMsg::Error("Expecting 2 arguments to match on L line but got %d for '%s'\n", res, line.c_str()); }
    int32_t target_id = -1;
    int32_t bam_target_size = 0;
    header.GetTargetId(target_name, &target_id);
    header.GetTargetSize(target_id, &bam_target_size);
    if (bam_target_size != target_size) { ErrorMsg::Error("Sizes don't match for %s hra file says %d and bam says %d\n",
							  target_name, target_size, bam_target_size); }
  }

  void HandlePLine(const BamHeaderInfo &header, std::ifstream &in, std::string &line) {
    TargetRange span(contig_counter_);
    contig_counter_++;
    const int32_t offset = 2;
    int32_t scaffold_id_1 = 0, start_pos_1 = 0, scaffold_orientation_end_1 = 0, scaffold_pos_1 = 0, target_length_1 = 0;
    char target_name_1[NAME_BUFFER_SIZE];
    int32_t scaffold_id_2 = 0, start_pos_2 = 0, scaffold_orientation_end_2 = 0, scaffold_pos_2 = 0, target_length_2 = 0;
    char target_name_2[NAME_BUFFER_SIZE];    
    int res = sscanf(line.c_str() + offset, "%d %s %d %d %d %d", &scaffold_id_1, target_name_1,
		     &start_pos_1, &scaffold_orientation_end_1, &scaffold_pos_1, &target_length_1);
    if (!in.good() || res != 6) { ErrorMsg::Error("Expecting 6 arguments to match but got %d on line '%s'\n", res, line.c_str()); }

    if (max_scaffold_ < scaffold_id_1) {max_scaffold_=scaffold_id_1;}
    int orientation_1 = scaffold_orientation_end_1;
    
    // These lines should always come in pairs
    std::getline(in, line);
    assert(line.size() > 3 && line[0] == 'P');
    res = sscanf(line.c_str() + offset, "%d %s %d %d %d %d", &scaffold_id_2, target_name_2,
		 &start_pos_2, &scaffold_orientation_end_2, &scaffold_pos_2, &target_length_2);
    if (res != 6) { ErrorMsg::Error("Expecting 6 arguments to match but got %d on line '%s'\n", res, line.c_str()); }
    assert(target_length_1==target_length_2);
    if (max_scaffold_ < scaffold_id_2) {max_scaffold_=scaffold_id_2;}

    assert(scaffold_id_1==scaffold_id_2);

    // Update the target coordinates and scaffold coordinates, note that target coordinates are 0 based even though
    // scaffolds are 1 based.
    span.target_start = start_pos_1 - 1; // hra file scaffold starts are 1 based but other coordinates are 0 based
    span.target_end = start_pos_1 - 1 + target_length_1;
    span.scaffold_start = scaffold_pos_1;
    span.scaffold_end = scaffold_pos_1 + target_length_1;
    span.scaffold_id = scaffold_id_1;

    assert(target_length_1==span.scaffold_end - span.scaffold_start);
    assert(target_length_1==span.target_end - span.target_start);

    if (int(scaffold_length_.size()) < scaffold_id_1+1) { scaffold_length_.resize(scaffold_id_1+1,0); }
    if (int(scaffold_nshot_.size())  < scaffold_id_1+1) { scaffold_nshot_.resize(scaffold_id_1+1,0); }
    if ( scaffold_pos_1 > scaffold_length_[scaffold_id_1] ) { scaffold_length_[scaffold_id_1]=scaffold_pos_1; }
    if ( scaffold_pos_2 > scaffold_length_[scaffold_id_1] ) { scaffold_length_[scaffold_id_1]=scaffold_pos_2; }

    // Figure out our orientation from paired lines.
    int orientation_2 = scaffold_orientation_end_2;
    if (orientation_1 == 5 && orientation_2 == 3) { 
      span.orientation = '+'; 
    }
    else if (orientation_1 == 3 && orientation_2 == 5) { 
      span.orientation = '-'; 
    }
    else { ErrorMsg::Error("Expected 5/3 or 3/5 got %d/%d\n", orientation_1, orientation_2); }
    
    //assert(strcmp(target_name_1, target_name_2) == 0);
    assert(scaffold_id_1 == scaffold_id_2);
    header.GetTargetId(target_name_1, &span.target_id);
    if (span.target_id > max_target_id_) {max_target_id_ = span.target_id;}
    DT_ASSERT(span.target_id >= 0, "Couldn't find target id for span\n");	  

    // keep track of which scaffolds a target maps into
    target_scaffold_map_.insert(std::make_pair(span.target_id, scaffold_id_1));

    // keep track of which scaffold each TargetRange maps into
    if (span.id >= contig_scaffold_map_.size()) {
      contig_scaffold_map_.resize(span.id+1);
    }
    contig_scaffold_map_[span.id]=scaffold_id_1 ;

    // Append to current scaffold or start a new one.
    std::map<int32_t, Path>::iterator path = scaffolds_.find(scaffold_id_1);
    if (path == scaffolds_.end()) {
      scaffolds_.insert(std::make_pair(scaffold_id_1,Path()));
      path = scaffolds_.find(scaffold_id_1);
    }
    //std::cout << span << "\n";
    path->second.Append(span);
    contig_segments_.push_back(span);
    target_ranges_.push_back(span);
  }
  
  /*
  void IndexMaskedRegions() {
    // Just to check for sanity sake
    std::sort(masked_regions_.begin(), masked_regions_.end());
    std::vector<TargetRange>::const_iterator current = masked_regions_.begin(), end = masked_regions_.end();
    int32_t curr_pos = 0;
    int32_t curr_id = 0;
    int32_t curr_id_start = 0;
    int32_t curr_id_end = 0;
    while(current != end) {
      if (curr_id != current->target_id) {
	masked_map_.insert(std::make_pair(curr_id, std::make_pair(curr_id_start, curr_id_end)));
	curr_id = current->target_id;
	curr_id_start = curr_id_end = curr_pos;
      }
      curr_id_end++;
      curr_pos++;
      current++;
    }
    // Add in the last one
    masked_map_.insert(std::make_pair(curr_id, std::make_pair(curr_id_start, curr_id_end)));

  }    
  */  

  uint32_t contig_counter_;
  int max_target_id_;
  int max_scaffold_;
  int64_t shotgun_counted_;                    ///< Overall sum of n_shotgun counts (now 64-bit because may fake as scaffold size)
  std::map<int32_t, Path> scaffolds_;          ///< Individual scaffolds
  std::multimap<int,int> target_scaffold_map_; ///< Maping from target_ids to scaffold_ids
  std::vector<int> contig_scaffold_map_ ;
  std::vector<TargetRange> contig_segments_;   ///< Broken ranges sorted by contig.
  std::vector<TargetRange> target_ranges_;     ///< Broken ranges sorted by id.
  std::vector<int> contig_segments_index_;     ///< Starting positions in contig_segments_ for each contig.
  std::vector<int> scaffold_length_;           ///< Scaffold lengths... by scaffold_id
  std::vector<int> scaffold_nshot_;            ///< Scaffold #shotgun reads... by scaffold_id
  //std::vector<TargetRange> masked_regions_;  ///< Sorted Range of masked regions
  //ChicagoLinks::IndexRangeMap masked_map_;   ///< Map of target index to sorted range of masked regions
  SegmentSet masked_segments_;
};

std::ostream& operator<<(std::ostream& out, PathSet& pathset) {
  // int bsize=1024;
  //out << "Scaffolds: \n";
  int i;
  std::map<int32_t,Path>::const_iterator     scaffold =pathset.GetScaffoldIter();
  std::map<int32_t,Path>::const_iterator     send     =pathset.GetScaffoldEnd();

  Path p = scaffold->second ;
  //out << p.ncontigs()  << "\n";
  //  out << p  << "\n";
  //out << "xx: first scaffold n contigs: " << scaffold->second.ncontigs() << "\n";
  //  out << scaffold->second << "\n";
  for (;scaffold!=send;scaffold++) {
    p = scaffold->second;
    //    out << "scaffold: \t" << scaffold->first << "\t" << p << "\n" ;
    out  << p ;
  }    
  //  out << "\n";
  return out;
}

#endif // CHICAGO_SCAFFOLDS_H
