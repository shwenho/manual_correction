#ifndef hr_perm_utils_h
#define hr_perm_utils_h

#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include "chicago_links.h"
#include "chicago_scaffolds.h"

uint factorial(uint n) {
  uint r=1;
  for (uint i=1;i<=n;i++) {
    r*=i;
  }
  return r;
}

std::string uint2binary(uint i, int w) {
  std::string s;
  for (uint f=0;f<w;f++) {
    if (((i>>f)&1)==1) {s+="1"; } else {s+="0";}
  }
  return s;
}

//mapping from intergers i, 0<=1<n! to the n! permuations of n things.
//the 'lowest order' bits (i%n) indicate which element to take first...
//then update i to (i-(i%n))/n, and n to n-1...  and repeat.
void set_perm(uint n , uint i, std::vector<uint>& p) {
  std::vector<char> flags;
  flags.resize(n);
  std::fill(flags.begin(),flags.end(),0);

  //  uint n = w;

  //  std::cout << i << " th perm of "<< w<<" things:\t" << i << ":\t";
  uint m=0;
  while (n>1) {
    uint j= i%n ; // "use the jth remaining element"
    uint k=0;
    while (flags[k]!=0) {k++;}
    for (uint l=0;l<j;l++) {
      k++;
      while (flags[k]!=0) {k++;}
    }
    flags[k]=1;
    p[m]=k;
    m++;
    i-=(i%n);
    i/=n;
    n-=1;
  }

  uint j= 0 ; // "use the only remaining element
  uint k=0;

  while (flags[k]!=0) {k++;}
  for (uint l=0;l<j;l++) {
    k++;
    while (flags[k]!=0) {k++;}
  }
  flags[k]=1;
  p[m]=k;

}

//provide a simple interaface for iterating over all the 
class Permutation {
public:

  uint n;
  uint w;  // lets you constrain so that no element gets moved more than w-1 places away from it's original loc.
  uint i;
  uint np;

  std::vector<uint> p;
  Permutation(uint n)                 : n(n), w(n), i(0) {
    p.resize(n);
    np=factorial(n);
    set_perm(n,i,p);
  };

  Permutation(uint n, uint w)         : n(n), w(w), i(0) {
    p.resize(n);
    np=factorial(n);
    set_perm(n,i,p);
  };

  Permutation(uint n, uint w, uint i) : n(n), w(w), i(i) {
    p.resize(n);
    np=factorial(n);
    set_perm(n,i,p);
    
    if (! check_constraint() ) {
      throw std::runtime_error("Incompatible constratint and specified permutation.");
    }
  };

  bool check_constraint() {
    if (w>=n) {return true;}
    for(uint i=0;i<n;i++) {
      if (abs(p[i]-i)>=w) {return false;}
    }
    return true;
  }

  uint operator[](uint i) const { return p[i]; }
  uint end() {return factorial(n);};

  void operator++() { 
    i++;
    set_perm(n,i,p);
    while(i<np && !check_constraint()) {
      i++;
      set_perm(n,i,p);
    } 
  }

};

typedef uint FlipBits;
typedef std::vector<uint> PermutationVector;
class LocalOOState {
 public:
  static uint counter;
  PermutationVector p;
  FlipBits    f;
  uint id;
  LocalOOState(PermutationVector pi, FlipBits fi) : p(pi), f(fi), id(counter) { counter++; };
};

uint LocalOOState::counter=0;

struct Backlink {
  uint s;
  uint ancestor;
  int delta;

  Backlink(uint st,uint a,int d) : s(st), ancestor(a), delta(d) {}
Backlink() : s(0), ancestor(0), delta(0) {}
};


std::ostream& operator<<(std::ostream& out, const Backlink& bl) {
  out << bl.s <<","<<bl.ancestor<<";"<<bl.delta;
  return out;
}
 
bool flip_bits_compatible( uint f1, uint f2, int delta , uint w) {
  //not tested yet
  //  return true;
  if (delta>=w) return true; 
  f2=f2>>delta;
  uint mask = 1<<(w-delta);
  mask-=1;
  //  mask=mask>>delta;
  //  std::cout << "flip test:\t" << delta << "\t" << uint2binary(mask,w-delta) << "\t" << uint2binary(f1,w) << "\t" << uint2binary(f2,w)  << "\t" << uint2binary(f2&mask,w) << "\t" << ( (f1&mask) == f2 ) << "\n";
  if ( (f1&mask) == f2 ) {return true;} else { return false;}
}

bool permutations_compatible( std::vector<uint> p1, std::vector<uint> p2, int delta) {
  uint w = p1.size();
  if (delta>=w) return true; 
  /*
  std::cout << "\n\n" << delta << "\n";
  for (uint i = 0; i<w; i++) {
    std::cout << p2[i] ;
  }  
  std::cout << "\n";

  for (uint i = 0; i<abs(delta); i++) {
    std::cout << "-";
  }
  //  std::cout << "\n";

  for (uint i = 0; i<w; i++) {
    std::cout << p1[i] ;
  }  
  std::cout << "\n";
*/

  if (delta<0) {throw std::runtime_error("delta<0");}
  if (delta>=w) {throw std::runtime_error("delta>=w");}

  uint j=0;
  for (uint i = delta; i<w; i++) {

    //    std::cout << i <<"\t"<<j<<"\t"<<int(p2[i])+delta<<"\t"<<p1[j]<<"\t"<<"\n";
    if (!( int(p2[i])-delta ==p1[j] )) {
      
      return false;
    }

    j++;
  }
  return true;
}

void traverse(std::vector< std::vector<int> >& touched, const std::vector<std::vector< Backlink>>& bl, uint sid, int w, int delta) {
  if (delta>w) {return;}
  touched[delta][sid] = 1;
  //  std::cout << "set flag: "<< delta<<"\t"<<sid<<"\n";

  for(uint bli=0;bli<bl[sid].size();bli++) {
    int d    = bl[sid][bli].delta;
    uint anc = bl[sid][bli].ancestor;
    traverse(touched,bl,anc,w,delta+d);
  }

}

void prune_backlink_graph(   std::vector<std::vector< Backlink>>& bl, const std::vector< LocalOOState >& s , int w) {
  
  std::vector< std::vector<int> > touched;  // keep track of which states have already been reached.
  touched.resize(w+1);
  for (uint i=0;i<=w;i++) {
    touched[i].resize(s.size());
  }

  for (uint sid=0;sid<s.size();sid++) {
    //    std::cout << "\n\nclear flags\n";
    for (uint i=0;i<=w;i++) {
      std::fill(touched[i].begin(),touched[i].end(),0);  // clear the touched trackers.
    }
    
    std::vector<int> stack;
    std::vector<int> prune_list;
    int lvl=0;
    std::vector< Backlink> newbl;
    int last_delta=0;

    for(uint bli=0;bli<bl[sid].size();bli++) {
      int delta = bl[sid][bli].delta;
      if (delta<last_delta) {throw std::runtime_error("blacklink pruning assumes raw backlinks in increasing order of delta\n");}
      last_delta=delta;
      uint anc = bl[sid][bli].ancestor;
      if (touched[delta][anc]>0) {
	//prune this one;
	//	std::cout << "prune!\t" << delta << "\t" << anc << "\t" << touched[delta][anc] <<"\n";
	prune_list.push_back(bli);
      } else {
	//	std::cout << "backlink: " << sid<< "\t"<<bli <<"\t"<< bl[sid][bli] <<"\n";// << "\t" << bl[sid][bli] << "\n";
	newbl.push_back(bl[sid][bli]);
      }

      traverse(touched,bl,anc,w,delta); // flag all the states reachable traversing along this tree.      

    }
    //    std::cout << "backlink:  \n"; // << sid << "\t" << bl[sid][bli] << "\n";
    bl[sid]=newbl;

    /*
    for(uint bli=0;bli<bl[sid].size();bli++) {
      std::cout << "backlink: " << sid<< "\t"<<bli <<"\t"<< bl[sid][bli] <<"\n";// << "\t" << bl[sid][bli] << "\n";
    }     
    */
  }
};

struct Backlink_iterator {
  std::vector< Backlink>& blv;
  uint i;

  Backlink_iterator(std::vector< Backlink>& backlinks) : blv(backlinks), i(0) {}

  void operator++() { i++; }
  Backlink* operator->() { return &(blv[i]); }
  Backlink operator*() { return blv[i]; }
  bool done() { return i>=blv.size(); }
  
};

struct LocalOODPLinks {
  uint nstates;
  uint w;
  uint w2;
  std::vector< LocalOOState > states;
  std::vector<std::vector< Backlink>> bl;

  LocalOODPLinks(uint window, uint max_shuffle=-1) : w(window), w2(max_shuffle) {
    if (w2==-1) {w2=w+1;}
    Permutation pr(w,w2);
    for (;pr.i < pr.end();++pr) {
      for (uint f=0;f<((1<<w)-1);f++) {
	states.push_back( LocalOOState( pr.p ,f) );
      }
    }    
    nstates=states.size();
    bl.resize(nstates); 
    
    
    for (int delta = 1; delta <= w ; delta++) {
      for (uint i=0;i<states.size();i++) {
	for (uint j=0;j<states.size();j++) {	
	  if ( permutations_compatible( states[i].p ,states[j].p,delta) && flip_bits_compatible(states[i].f,states[j].f,delta,w)  ) {
	    bl[i].push_back(Backlink(states[i].id,states[j].id,delta)); 
	  } 
	}
      }
    }
    
    prune_backlink_graph(bl,states, w);
  }

  Backlink_iterator ancestors(uint i) {
    return Backlink_iterator( bl[i] );
  }

};

std::ostream& operator<<(std::ostream& out, const Permutation& p) {
  out << p[0];
  for (uint i=1;i<p.n;i++) {
    out << " " << p[i]  ;
  }
  return out;
}


std::ostream& operator<<(std::ostream& out, const PermutationVector& p) {
  out << p[0];
  for (uint i=1;i<p.size();i++) {
    out << " " << p[i]  ;
  }
  return out;
}

std::ostream& operator<<(std::ostream& out, const LocalOOState& s) {
  out << s.p << " : " << uint2binary(s.f,s.p.size()) << " id:"<<s.id ;
  return out;
}

struct DPmatrix {
  const LocalOODPLinks& links;
  int nstates;
  int npositions;
  int w;
  std::vector<double> score;
  std::vector< Backlink > score_path;
  std::vector<bool>   forbidden;
  const ChicagoLinks& read_links;
  const Path& original_scaffold;
  
DPmatrix( const LocalOODPLinks& linksv , const ChicagoLinks& read_links_, const Path& original_scaffold_ ) : read_links(read_links_), original_scaffold(original_scaffold_), links(linksv), nstates(linksv.nstates), w(linksv.w) { setup(); };
DPmatrix( const LocalOODPLinks& linksv, int n , const ChicagoLinks& read_links_, const Path& original_scaffold_ ) : read_links(read_links_), original_scaffold(original_scaffold_), links(linksv), nstates(linksv.nstates), w(linksv.w), npositions(n) { setup(); };

  
  void setup() {
    score.resize( nstates*(npositions +2*w-2) );
    score_path.resize( nstates*(npositions +2*w-2) );
    forbidden.resize( nstates*(npositions +2*w-2) );
    
    flag_boundary_crossers();

  }

  void score_delta(int p, int statei, int statej, float llr) {
    // for every contig i in state i
    for(int i=p; i<p+w;i++) {
      int gapsize=1000;
      // for every contig j in range (i.e. between i-w and p)
      // accumulate gapsize along the way
      const TargetRange contig_i = original_scaffold.GetPathConst()[contig_lookup(p, statei, i)];
      for(int j=p-1; j>=i-w; j--) {
	const TargetRange contig_j = original_scaffold.GetPathConst()[contig_lookup(p, statej, j)];
	std::cout << contig_i << " and contig " << contig_j << " p " << p << " statei "  << statei << " " << statej << "\n";
      }
    }
    // for every contig pair in range between two states
    // look up contig ids based on position in scaffold
    // look up lengths 
    // look up gapsize
    // look up orientat.ions
    // create contig pair
    // get llr
    // return total llr
    //ContigPair cp(c1, c2, l1, l2, scaffold_1_shot, scaffold_2_shot, gapsize);

    

  }

  int contig_lookup(int p,int state,int j) {

    if (state>links.states.size()) {throw std::runtime_error("wtf?  state out of range\n");}
    const LocalOOState& s = links.states[state];

    if ((j-p)>=w) {throw std::runtime_error("wtf? index out of window \n");}

    //    std::cout << "p:\t" << p << "\tst:\t" << state << "\t" << j << "\t" << s.p << "\tconitg: " << p+s.p[j-p] << "/" << ((s.f>>(j-p))&1) << "\n" ;
    //    std::cout << "p:\t" << p << "\tst:\t" << state << "\t" << j << "\t" << s << "\tconitg: " << p+int(s.p[j-p]) << " / " << ((s.f>>(j-p))&1) << "\n" ;

    return p+int(s.p[j-p]);
  }

  int orientation_lookup(int p, int state, int j) {

    if (state>links.states.size()) {throw std::runtime_error("wtf?  state out of range\n");}
    const LocalOOState& s = links.states[state];

    if ((j-p)>=w) {throw std::runtime_error("wtf? index out of window \n");}

    //    std::cout << "p:\t" << p << "\tst:\t" << state << "\t" << j << "\t" << s.p << "\tconitg: " << p+s.p[j-p] << "/" << ((s.f>>(j-p))&1) << "\n" ;
    //    std::cout << "p:\t" << p << "\tst:\t" << state << "\t" << j << "\t" << s << "\tconitg: " << p+int(s.p[j-p]) << " / " << ((s.f>>(j-p))&1) << "\n" ;

    int o = uint2binary(s.f,s.p.size())[int(s.p[j-p])];
    return o - '0';

  }

#define MATRIX_I(P,S) (((P)+w-1)*nstates)+S 
  
  double get_score(int p,int s) {return score[MATRIX_I(p,s)];}
  Backlink get_score_path(int p,int s) {return score_path[MATRIX_I(p,s)];}
  bool is_forbidden(int p,int s) {return forbidden[MATRIX_I(p,s)];}

  void set_score(int p,int s,double sc) { score[MATRIX_I(p,s)]=sc;    }
  void set_score_path(int p,int s,Backlink prev_path) { score_path[MATRIX_I(p,s)]=prev_path;    }
  void set_forbidden(int p,int s)      {     forbidden[MATRIX_I(p,s)]=true; }

#define BC_DEBUG false
  void flag_boundary_crossers() {
    for(int p = -w+1; p<= -1 ;p++) {
      for(uint state=0; state<nstates;state++) {
#if BC_DEBUG
	std::cout << "fbc:" << p << "\t" << state << "\t" ;
	for (int i=0;i<w;i++) { std::cout << int(links.states[state].p[i])+p << " "; }
#endif	  
	for (int i=p;i<0;i++) {
	  if ((int(links.states[state].p[i-p])+p)>=0){
#if BC_DEBUG
	    std::cout << "\tXXX";
#endif	  
	    set_forbidden(p,state);
	    break;
	  }
	}
#if BC_DEBUG
	std::cout << "\n";
#endif	  
      }
    } 

    for(int p = npositions-w+1; p< npositions ;p++) {
      for(uint state=0; state<nstates;state++) {
#if BC_DEBUG
	std::cout << "fbc:" << p << "\t" << state << "\t" ;
	for (int i=0;i<w;i++) { std::cout << int(links.states[state].p[i])+p << " "; }
#endif	  
	for (int i=p;i<npositions;i++) {
	  //	  std::cout << int(links.states[state].p[i-p])+p << " ";
	  if ((int(links.states[state].p[i-p])+p)>=npositions){
#if BC_DEBUG
	    std::cout << "\tXXX";
#endif
	    set_forbidden(p,state);
	    break;
	  }
	}
#if BC_DEBUG
	std::cout << "\n";
#endif
      }
    } 
  }

};


#endif




