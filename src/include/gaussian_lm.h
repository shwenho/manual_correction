#ifndef GAUSSIAN_LM_H
#define GAUSSIAN_LM_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
#include <Eigen/Dense>
#include <picojson.h>
#include <pthread.h>
#include "error_msg.h"
#include "chicago_read.h"
#include "paired_read_likelihood_model.h"
#include "pair_distance_likelihood_model.h"
#include "constants.h"

class GaussianLM : public PairDistanceLM  {

public:

  GaussianLM() { Init(); }


  //
  // Model specific stuff  (override in implementations)
  //

  void fit(const HistogramProvider& data) {assert(1==0);};

  void SetModel(const char *model_json, const bool fill_cache = true) {
      picojson::value v;
      std::string err = picojson::parse(v, model_json);

      num_links_ = v.get("N").get<double>();;
      genome_size_ = v.get("G").get<double>();
      prob_noise_ = v.get("pn").get<double>();

      insert_mean_ = v.get("mean").get<double>();
      insert_sigma_ = v.get("sdev").get<double>();

      prob_noise_div_genome_size_ = prob_noise_ / genome_size_;
      one_minus_prob_noise_ = 1.0d - prob_noise_;
      mean_num_links_null_coeff_ = num_links_ * prob_noise_ / (genome_size_ * genome_size_);
      one_minus_prob_noise_N_over_G_ = one_minus_prob_noise_ * num_links_ / genome_size_ ;

      logG_ = log(genome_size_);
      logN_ = log(num_links_);
      lnFinf_ = log(prob_noise_) - logG_;

      norm_factor_ = one_over_sqrt_two_pi / insert_sigma_ ;
      sigma_over_root_2pi_ = insert_sigma_ / root_two_pi ;
      two_sigma_squared_ = 2.0 * insert_sigma_ * insert_sigma_ ;
      sigma_root2_ = sqrt(2.0)*insert_sigma_ ;
    //        self.lnFinf = math.log(self.pn)-math.log(self.G)
      if (fill_cache) {
	fill_caches();
      }
  }

  int sample_distribution() {

    return(0);
  }


  void NonNoiseInsertDist(int insert_size, double *likelihood) {
    *likelihood =  norm_factor_ * exp( -pow( insert_size - insert_mean_ , 2.0) / two_sigma_squared_ );
  }


  inline void F_good(int insert_size, double *result) {
    *result = 1.0 - 0.5*std::erf((insert_size - insert_mean_)/sigma_root2_);
  }

  inline void H_good(int insert_size, double *result) {
    *result = 0.5*insert_mean_;

    *result -=  0.5*insert_mean_*std::erf((insert_size - insert_mean_)/sigma_root2_);
    *result -= -sigma_over_root_2pi_ * exp( - pow( insert_size - insert_mean_ , 2.0) / two_sigma_squared_ );

  }

  inline void F0(int insert_size, double *result) {

    *result = 0.5*std::erf((insert_size - insert_mean_)/sigma_root2_);

    *result *= one_minus_prob_noise_;
    *result += prob_noise_div_genome_size_ * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  inline void H0(int insert_size, double *result) {
    // integral (x exp(-(x-m)^2/(2 s^2)))/(sqrt(2 pi) s) dx = -1/2 m erf((m-x)/(sqrt(2) s))-(s e^(-(m-x)^2/(2 s^2)))/sqrt(2 pi)+constant

    *result  =  0.5*insert_mean_*std::erf((insert_size - insert_mean_)/sigma_root2_);
    *result += -sigma_over_root_2pi_ * exp( - pow( insert_size - insert_mean_ , 2.0) / two_sigma_squared_ );

    *result *= one_minus_prob_noise_;
    *result += prob_noise_div_genome_size_ * 0.5 * insert_size * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  /// Initialize with default values
  void Init() {
    num_links_ = 0;
    genome_size_ = 0.0d;
    prob_noise_ = 0.0d;
    prob_noise_div_genome_size_ = 0.0d;
    one_minus_prob_noise_ = 0.0d;
    mean_num_links_null_coeff_ = 0.0d;

    insert_mean_ = 0.0d;
    insert_sigma_ = 0.0d;

    prob_noise_div_genome_size_ = 0.0d;
    one_minus_prob_noise_ = 1.0d ;
    mean_num_links_null_coeff_ = 0.0d;

    cache_max_ = 400000;
    cache_min_ =-400000;

    // std::cerr << "set cache min and max\n";

    Tcache_.resize(cache_max_-cache_min_);
    Tcache_fill_.resize(cache_max_-cache_min_);
    std::fill(Tcache_fill_.begin(),Tcache_fill_.end(),0);

    TPluscache_.resize(cache_max_-cache_min_);
    TPluscache_fill_.resize(cache_max_-cache_min_);
    std::fill(TPluscache_fill_.begin(),TPluscache_fill_.end(),0.0);

    Fcache_.resize(cache_max_-cache_min_);
    Fcache_fill_.resize(cache_max_-cache_min_);
    std::fill(Fcache_fill_.begin(),Fcache_fill_.end(),0);

    pthread_mutex_init( &mutex_, NULL);
    pthread_mutex_init( &mutex_T_, NULL);
    pthread_mutex_init( &mutex_F_, NULL);
    pthread_mutex_init( &mutex_A_, NULL);
    pthread_mutex_init( &mutex_P_, NULL);

  }

  void DescriptionString(char* sbuff) {
    sprintf(sbuff,"Gaussian Model, N=%d; pn=%g",num_links_,prob_noise_);
  };

private:
  double insert_mean_ ;
  double insert_sigma_;
  double norm_factor_;
  double sigma_over_root_2pi_ ;
  double two_sigma_squared_ ;
  double sigma_root2_;
};


#endif // GaussianLM
