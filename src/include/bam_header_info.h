#ifndef BAM_HEADER_INFO_H
#define BAM_HEADER_INFO_H


#include <map>
#include <string>
#include <vector>
#include "htslib/hts.h"
#include "htslib/sam.h"
#include "common.h"
#include "error_msg.h"
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include <iostream>
#include <fstream>

class BamHeaderInfo {
 public:

   BamHeaderInfo() : initialized_(false) {}

  
  
  /** Get the id (index) of target given the name. -1 if not found */
  inline void GetTargetId(const std::string &name, int32_t *id) const { 
    std::map<std::string, int32_t>::const_iterator i = target_idx_.find(name);
    i != target_idx_.end() ?  *id = i->second : *id = -1; 
  }

  /** Get the size for a given id. */
  inline void GetTargetSize(int32_t target_id, int32_t *size) const { *size = target_sizes_[target_id]; }
  inline void GetTargetStart(int32_t target_id, int32_t *start) const { *start = target_starts_[target_id]; }

  const char* GetTargetName(const int id) const {
    assert(id>=0);
    assert(id<int(target_names_.size()));
    return target_names_[id].c_str();
  }

  void Update(bam_hdr_t *header) {
    initialized_=true;
    if (target_names_.size()==0) {
      int target_ix = 0;
      int total_length_so_far=0;
      int inter_contig_buffer_bases = 50000;

      target_names_.reserve(header->n_targets);
      target_sizes_.reserve(header->n_targets);
      target_starts_.reserve(header->n_targets);
      while(target_ix < header->n_targets) {
	target_names_.push_back(header->target_name[target_ix]);
	target_sizes_.push_back(header->target_len[target_ix]);
	target_starts_.push_back(total_length_so_far);
	target_idx_.insert(std::make_pair(target_names_.back(), target_ix));
	total_length_so_far+=header->target_len[target_ix] + inter_contig_buffer_bases;
	
	target_ix++;
      } 
    } else {
      // check for mismatch between target_ids when loading additional bam files.
      int target_ix = 0;
      while(target_ix < header->n_targets) {
	assert (  target_names_[target_ix].compare(header->target_name[target_ix])==0) ;
	target_ix++;
      } 
    }
  }

  
  std::map<std::string, int32_t> target_idx_;    ///< Map names to index/numeric id of targets
  std::vector<std::string>       target_names_;  ///< Names of target ids in order
  std::vector<int32_t>           target_sizes_;  ///< Sizes of target ids in order
  std::vector<int64_t>           target_starts_;  ///< Starts of target ids in order
  bool initialized_;
  
 private:
};

#endif
