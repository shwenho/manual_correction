#ifndef SPARSE_KERNEL_SCORES_H
#define SPARSE_KERNEL_SCORES_H

#include <vector>
#include <cmath>
#include <mutex>
#include <assert.h>
#include <getopt.h>
#include "error_msg.h"
#include "hirise_threads.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "sparse_kernel_scores_defs.h"
#include "chicago_sv_binner.h"
#include "normalizers.h"

enum kernel_t {
  INVERSION,
  DELETION,
  DUPT,
  DUPL,
  DUPR,
  MAX_KERNEL_TYPE
};

enum zygosity_t {
  HET,
  HOMO,
  MAX_KERNEL_ZYGOSITY
};

enum zone_t {
  LEFT,
  CENTER,
  RIGHT
};


struct NamedEnum {
  std::string name;
  std::string abbr;

  NamedEnum(std::string _name, std::string _abbr) : name(_name), abbr(_abbr) {}
};

const NamedEnum KERNEL_TYPE[MAX_KERNEL_TYPE] = {
  NamedEnum("INVERSION", "INV"),
  NamedEnum("DELETION", "DEL"),
  NamedEnum("TANDEM_DUPLICATION", "DUPT"),
  NamedEnum("L_DUPLICATION", "DUPL"),
  NamedEnum("R_DUPLICATION", "DUPR")
};

const NamedEnum KERNEL_ZYGOSITY[MAX_KERNEL_ZYGOSITY] = {
  NamedEnum("HETEROZYGOUS", "HET"),
  NamedEnum("HOMOZYGOUS", "HOMO")
};

struct variant_spec {
  int D;
  kernel_t type;
  zygosity_t zygosity;

  variant_spec() {}
  variant_spec(int d, kernel_t t, zygosity_t z): D(d), type(t), zygosity(z) {}
  variant_spec(int d, std::string type_str, std::string zygosity_str) : D(d) {
    _set_type_from_string(type_str);
    _set_zygosity_from_string(zygosity_str);
  }
  variant_spec(std::string id_str) {
    std::string type_str, zygosity_str;
    std::stringstream buf(id_str);
    buf >> zygosity_str >> type_str >> D;
    _set_type_from_string(type_str);
    _set_zygosity_from_string(zygosity_str);
  }

  const std::string id() const {
    std::ostringstream buf;
    buf << KERNEL_ZYGOSITY[zygosity].abbr << " "
        << KERNEL_TYPE[type].abbr << " "
        << D;
    return buf.str();
  }

  void serialize_to_vec(std::vector<int>& vec) const {
    vec.push_back(zygosity);
    vec.push_back(type);
    vec.push_back(D);
  }

  void _set_type_from_string(std::string type_str) {
    int i = 0;
    for (; i < MAX_KERNEL_TYPE; ++i) {
      const NamedEnum& desc = KERNEL_TYPE[i];
      if (type_str == desc.name || type_str == desc.abbr) {
        type = kernel_t(i);
        break;
      }
    }
    if (i == MAX_KERNEL_TYPE)
      throw std::runtime_error("Invalid kernel type.");
  }

  void _set_zygosity_from_string(std::string zygosity_str) {
    int i = 0;
    for (; i < MAX_KERNEL_ZYGOSITY; ++i) {
      const NamedEnum& desc = KERNEL_ZYGOSITY[i];
      if (zygosity_str == desc.name || zygosity_str == desc.abbr) {
        zygosity = zygosity_t(i);
        break;
      }
    }
    if (i == MAX_KERNEL_ZYGOSITY)
      throw std::runtime_error("Invalid kernel zygosity.");
  }
};

struct BinnedRead {
/*
  xbin and ybin should have the same relationship to the x and y genome coordinates
  that center and height have respectively to x and y in the following code:

  for(xb=WW;xb>=-2*WW;xb--) {
    x=xb*BINSIZE;
    for (yb=xb;yb<2*WW;yb++) {
      center = (xb+yb)/2;
      height = (yb-xb);
      if (center < -WW ) {continue;}
      if (center >= WW ) {continue;}
      if (height >= HH) {continue;}
      y=yb*BINSIZE;
*/
  int xbin;
  int ybin;
  //  strand

  BinnedRead(int x, int y): xbin(x), ybin(y) {};
  BinnedRead(): xbin(0), ybin(0) {};

  bool operator<(const BinnedRead& rhs) const {
    return xbin != rhs.xbin ? xbin < rhs.xbin : ybin < rhs.ybin;
  };
};

enum OptionDefs {
  OPT_THRESHOLD,
  OPT_MODEL,
  OPT_REGION,
  OPT_OUTFILE,
  OPT_WORKERS,
  OPT_MIN_QUAL,
  OPT_BINSIZE,
  OPT_VARIANT_MIN_SIZE,
  OPT_VARIANT_MAX_SIZE,
  OPT_VARIANT_STEP_SIZE,
  OPT_VARIANT_TYPES,
  OPT_VARIANT_ZYGOSITY,
  OPT_KERNEL_CACHE,
  OPT_ZMASK_THRESHOLD,
  OPT_ZMASK_WINDOW,
  OPT_ZMASK_STEP,
  OPT_MASKED_BED,
  MAX_OPT
};

const std::string OPT_HELP[MAX_OPT] = {
  "Log-likelihood ratio threshold for calling structural variants. (default: 20)",
  "Path to json-formatted datamodel.",
  "Samtools-friendly region string (CHR:START-END) describing the region within which to search for structural variants.",
  "Prefix of path to dump list of structural variants and data archive to.",
  "Number of worker threads to use. (default: 1)",
  "Mapping quality threshold to filter reads to be used. (default: 10)",
  "Size of bins tiling the reference genome. (default: 300)",
  "Minimum size of structural variants to search for. (default: 1000)",
  "Maximum size of structural variants to search for. (default: 100000)",
  "Increment between minimum and maximum structural variant sizes. (default: 1000)",
  "Types of structural variants to search for. (default: 'INV,DEL,DUPT,DUPL,DUPR')",
  "Zygosity of structural variants to search for. (default: 'HET,HOMO')",
  "Prefix of path to cache kernel calculations to.",
  "Z-score threshold for denoising. (default: 4)",
  "Size of window for denoising. (default: 10000)",
  "Number of bins by which to shift window during denoising. (default: 100)",
  "List of low read mappability regions to mask, in BED format."
};

const option OPTS[] = {
  {"threshold",         required_argument,  NULL, 't' },
  {"model",             required_argument,  NULL, 'm' },
  {"region",            required_argument,  NULL, 'r' },
  {"outfile",           required_argument,  NULL, 'o' },
  {"workers",           required_argument,  NULL, 'w' },
  {"min_qual",          required_argument,  NULL, 0   },
  {"bin_size",          required_argument,  NULL, 0   },
  {"variant_min_size",  required_argument,  NULL, 0   },
  {"variant_max_size",  required_argument,  NULL, 0   },
  {"variant_step_size", required_argument,  NULL, 0   },
  {"variant_types",     required_argument,  NULL, 0   },
  {"variant_zygosity",  required_argument,  NULL, 0   },
  {"kernel_cache",       required_argument,  NULL, 0   },
  {"zmask_threshold",   required_argument,  NULL, 0   },
  {"zmask_window",      required_argument,  NULL, 0   },
  {"zmask_step",        required_argument,  NULL, 0   },
  {"masked_bed",        required_argument,  NULL, 0   },
  {NULL,                0,                  NULL, 0   }
};

struct VariantCall {
  int bin;
  int variant;
  float score;

  VariantCall(int _bin, int _variant, float _score) :
    bin(_bin), variant(_variant), score(_score) {}
};

struct Window {
  int64_t read_start;
  int64_t read_end;
  int64_t mask_start;
  int64_t mask_end;

  Window(int64_t _read_start,
         int64_t _read_end,
         int64_t _mask_start,
         int64_t _mask_end) :
    read_start(_read_start), read_end(_read_end),
    mask_start(_mask_start), mask_end(_mask_end) {}
};

class SVKernelScorer {
  friend class KernelScorerThread;
private:
  // Datamodel
  PairedReadLikelihoodModel* m_model;

  // Binner
  int m_binsize;
  SVReadPairBinner m_binner;

  //min read pair separation
  int m_min_isize;

  std::vector<BinnedRead> m_binned_reads;
  std::vector<std::pair<int64_t, int64_t> > m_masked_bins;

  // Variants
  std::vector<variant_spec> m_variants;

  // Kernel calculations
  std::vector<float> m_deltas;
  std::vector<float> m_areaTij;
  std::vector<float> m_areaVs;
  std::vector<float> m_offsets;
  std::vector<float> m_means;
  std::vector<float> m_var;
  std::vector<float> m_scores;

  // Best calls
  std::vector<VariantCall> m_calls;

  // First window
  int m_begin;

  // Last window
  int m_end;

  // Cache window data
  std::vector<Window> m_windows;

  // Number of threads to use
  int m_threads;

  // Mutex for logging calls
  std::mutex m_lock;

  int get_region(const variant_spec& variant, int x, int y) const;
  void get_variant_separations(const variant_spec& variant,
                               std::vector<int>& dv,
                               int x,
                               int y,
                               int region) const;

public:
  SVKernelScorer(int _binsize, int _threads) :
    m_binsize(_binsize), m_binner(_binsize), m_threads(_threads) {}

  void load_model_from_json(const std::string& json) {
    INFO("Parsing user-specified datamodel...\n\n");
    LikelihoodModelParser model_parser;
    m_model = model_parser.ParseModelJson(json.c_str(), false);
  }

  void init_kernel_elements() {
    m_deltas.resize(m_variants.size()*W2*HH);
    m_areaTij.resize(m_variants.size()*W2*HH);
    m_areaVs.resize(m_variants.size()*W2);
    m_offsets.resize(m_variants.size());
  };
  void init_kernel_stats() {
    m_means.resize(m_variants.size());
    m_var.resize(m_variants.size());
  }
  void init_scores() {
    m_scores.resize(m_variants.size() * (m_end-m_begin));
  }

  void prepare_data(int min_qual,
                    std::string region,
                    std::vector<std::string> bamfile,
                    std::string masked_bed,
                    bool do_model_fit=false);

  void cache_window_data();

  void correct_model_for_masking() ;

  void build_variant_list(int Dmin,
                          int Dmax,
                          int stepsize,
                          std::string types,
                          std::string zygosity);

  void calc_kernel_elements();
  void calc_kernel_stats();
  // double pairs_per_window() const;
  void find_variants(float score_threshold,
                     double zmask_threshold,
                     int zmask_window,
                     int zmask_step);
  void prune_variants();
  void log_call(VariantCall& call) {
    m_lock.lock();
    m_calls.push_back(call);
    m_lock.unlock();
  }

  bool load_kernels_from_cache(std::string fp);
  void dump_kernels(std::string fp) const;
  void dump_scores(std::string fp) const;
  void dump_variants(std::string fp) const;
};

// Parallelization
struct HRKernelScorerTask {
    int start;
    int end;

    HRKernelScorerTask(int _start, int _end) : start(_start), end(_end) {}
    HRKernelScorerTask() : start(-1), end(-1) {}

    bool is_valid() const { return start > -1 && end > start; }
};

class KernelScorerThread : public HRThread {
private:
    // Pointer to the SVKernelScorer
    SVKernelScorer* me;

    // Task queue
    HRTaskQueue<HRKernelScorerTask>& m_queue;

    // Minimum score required to call a variant
    float m_threshold;

    // Normalization and denoising
    ZMasker<float> m_normalizer;

public:
    KernelScorerThread(SVKernelScorer* _me,
                       HRTaskQueue<HRKernelScorerTask>& _queue,
                       float score_threshold,
                       double zmask_threshold,
                       int zmask_window,
                       int zmask_step) :
      me(_me),
      m_queue(_queue),
      m_threshold(score_threshold),
      m_normalizer(zmask_threshold,
                   score_threshold,
                   zmask_window,
                   zmask_step) {}

    void* run() {
      while (!m_queue.Done()) {
        const HRKernelScorerTask& task = m_queue.dequeue();
        if (task.is_valid())
          handle_task(task);
      }

      return NULL;
    }

    void handle_task(const HRKernelScorerTask& task);
};


#endif
