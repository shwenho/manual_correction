#ifndef CHICAGO_BAM_FILTERS_H
#define CHICAGO_BAM_FILTERS_H

#include <iostream>
#include "htslib/sam.h"

struct DefaultFilter {
    DefaultFilter() {}

    bool operator()(bam1_t *aln) const {
        return true;
    }
};

struct SVReadPairFilter {
    int min_mapq;
    int min_isize;
    bool keep_noise;

SVReadPairFilter(int _min_mapq, int _min_isize=1000) :
    min_mapq(_min_mapq), min_isize(_min_isize), keep_noise(true) {}

    bool operator()(bam1_t *aln) const {
        // Unmapped
        if (aln->core.flag & BAM_FUNMAP)
            return false;
        // Unmapped mate
        if (aln->core.flag & BAM_FMUNMAP)
            return false;
        // Only use forward reads  (only use read1)
        if (aln->core.flag & BAM_FREAD2)
            return false;
        // Ignore duplicates
        if (aln->core.flag & BAM_FDUP)
            return false;
        // Ignore interchromosomal reads
        if ((aln->core.tid != aln->core.mtid) && (!keep_noise))
            return false;
        // Enforce minimum insert size constraints
        if ((abs(aln->core.isize) < min_isize) && (aln->core.tid == aln->core.mtid))
            return false;
        // Enforce minimum MAPQ constraints
        uint8_t *tag = bam_aux_get(aln, "MQ");
        if (aln->core.qual < min_mapq ||
            tag == NULL ||
            bam_aux2i(tag) < min_mapq)
            return false;

        return true;
    }
};


#endif
