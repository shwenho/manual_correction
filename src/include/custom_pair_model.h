#ifndef CUSTOM_PAIR_MODEL_H
#define CUSTOM_PAIR_MODEL_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
#include <Eigen/Dense>
#include <picojson.h>
#include "error_msg.h"
#include "chicago_read.h"
#include "segment_set.h"
#include "paired_read_likelihood_model.h"
#include "constants.h"

/*
A likelihood model for fake read pairs.
 */

class CustomPairLM : public PairedReadLikelihoodModel {

 public:

  CustomPairLM() { Init(); }

  CustomPairLM(const char *model_json) {
    Init();
    //    gmodel_->SetModel(model_json);
  }

  void SetModel(const char *model_json) {                          };//      gmodel_->SetModel(model_json);  } ;
  void SetModel(const char *model_json, const bool fillCache ) {   };// gmodel_->SetModel(model_json, fillCache);  } ;

  void LLRAreaContribution( ContigPair *cp , int n, double *result ) {
    *result=0.0;
    //    gmodel_->LLRAreaContribution(cp,n,result);
  }

  void LLRReadPairContribution( ContigPair *cp, Read r, double *result ) {
    double mean=0.0;
    double sdev=0.0;
    double x=0.0;
    double z=3.0;
    double llr;
    if ( has_expected_orientation(cp,r) ) {
      mean = double(r.size_1);
      sdev = double(r.size_2);
      x=double( cp->separation(r) );
      
      //      llr = ( pow(mean + z*sdev, 2.0) - pow(x-mean,2.0) )/(2.0*sdev*sdev) ;
      llr = 300.0 -pow((x-mean),2.0)/(2.0*sdev*sdev) - log(sdev*root_two_pi ) ;
      if (llr<0.0) {*result=0.0;}
      else { *result=llr; }
      std::cout << "expected orientation:\t" << *cp <<"\t" << r << "\t" << mean << "\t" << sdev << "\t" << x << "\t" << llr << "\n";

    } else {
      std::cout << "unexpected orientation:\t" << *cp <<"\t" << r << "\n";
    }
  };

  void fit(const HistogramProvider& data) {assert(1==0);};
  void InsertSizeDist(     int l, double *result ) { *result=0.0; } ; // gmodel_->InsertSizeDist(l,result)    ; } ;
  float InsertSizeDistF(   int l                 ) { return( 0.0)    ; } ;
  void NonNoiseInsertDist( int l, double *result ) { *result=0.0; } ; //gmodel_->NonNoiseInsertDist(l,result); } ;

  void H0( int l, double *result ) {                             *result=0.0; } ;//   gmodel_->H0(l,result); } ;
  double genome_size() {                                         return(0.0); } ;// gmodel_->genome_size(); };
  double noise_probability() {                                   return(0.0); } ;//gmodel_->noise_probability(); } ;
  const int32_t N() const {                                      return(0.0); } ;// gmodel_->N(); } ;

  void RescaleGenomesize(double GG, bool fill_cache=false) { } ;// gmodel_->RescaleGenomesize(GG,fill_cache); }  ;

  void n_bar(int l1,int l2,int g,double *l) { *l=0.0; }; // gmodel_->n_bar(l1,l2,g,l);};
  void n_bar(int l1,double *l) { *l=0.0; }; // gmodel_->n_bar(l1,l2,g,l);};
  void set_N(double N) {     };

  int sample_distribution() { return(0);}; //gmodel_->sample_distribution(); };

  void SetMetagenomic(int64_t n_shotgun, bool useDepthPenalty=false) {} ; // gmodel_->SetMetagenomic(n_shotgun,useDepthPenalty); } ;
  void UnSetMetagenomic() {                                        };//    gmodel_->UnSetMetagenomic(); };
  bool NeedDepthDiscrep(ContigPair *cp) { return(false); }; //gmodel_->NeedDepthDiscrep(cp); };
  void LLRDepthDiscrep(ContigPair *cp, double *result) { *result=0.0; } ; //gmodel_->LLRDepthDiscrep(cp,result); };

  void DescriptionString(char* sbuff) {
    sprintf(sbuff,"Custom pair model.");
    //    gmodel_->DescriptionString(sbuff + 25);
  };

 private:
  
  bool has_expected_orientation(ContigPair *cp, Read r) {

    //   0 :    -->      <--    Innie
    //   1 :    <--      -->    Outie
    //   2 :    -->      -->    Fwd
    //   3 :    <--      <--    Rev

    //    std::cout << "debug stranded model: pair type: " << int(cp->pair_strand_type(r)) << "\tcp:\t" << int(cp->strand_1) << "\t" << int(cp->strand_2) << "\tread:\t" << int(r.read1_flipped()) << "\t" << int(r.read2_flipped()) << "\n"; 

    if (cp->pair_strand_type(r) == 0) { return(true); }  // this models a library where the pairs are supposed to be innies.
 
    return(false);
  }

  void Init() {
    //    lnFinf_ = 0.0;

  };
  //  GaussianLM *gmodel_ ;

};

#endif
