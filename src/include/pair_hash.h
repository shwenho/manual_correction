#ifndef PAIR_HASH_H
#define PAIR_HASH_H


namespace dtg {
  struct hash_pair {
  public:
    std::size_t operator()(const std::pair<int,int> &x) const { 
      const std::size_t fnv_prime = 1099511628211u;
      const std::size_t fnv_offset_basis = 14695981039346656037u;
      std::size_t hash = fnv_offset_basis;
      hash ^= (x.first);
      hash *= fnv_prime;
      hash ^= (x.second);
      hash *= fnv_prime;
      return hash;
    }

  };

  struct hash_cp {
  public:
    std::size_t operator()(const ContigPair &cp) const {
      const std::size_t fnv_prime = 1099511628211u;
      const std::size_t fnv_offset_basis = 14695981039346656037u;
      std::size_t hash = fnv_offset_basis;
      hash ^= (cp.target_1);
      hash *= fnv_prime;
      hash ^= (cp.target_2);
      hash *= fnv_prime;
      hash ^= (cp.strand_1);
      hash *= fnv_prime;
      hash ^= (cp.strand_2);
      hash *= fnv_prime;
      hash ^= (cp.gap);
      hash *= fnv_prime;
      return hash;
    }
  };
}

#endif


