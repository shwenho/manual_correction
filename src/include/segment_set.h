#ifndef SEGMENT_SET_H
#define SEGMENT_SET_H

#define BINARY_SEARCH_CHECK false

#include <map>
#include <string>
#include <sstream>
#include <vector>
#include "common.h"
#include "error_msg.h"
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include "chicago_read.h"
#include "bam_header_info.h"
#include <string.h>
//#include "chicago_scaffolds.h"

enum MaskedRegionTypes {
  DEEP        = 1 << 0,
  PROMISCUOUS = 1 << 1,
  GAP         = 1 << 2,
  DESERT      = 1 << 3,
  HIRISE_GAP  = 1 << 4,
};

struct CoverageStep {
  int32_t target_id; 
  int32_t x        ; 
  double  y        ; 
  //bool CoverageStep::operator< (const CoverageStep& r);
  //  bool CoverageStep::operator< (const Base& r);

CoverageStep(int target, int x, double y): target_id(target), x(x), y(y) {};
CoverageStep(): target_id(0), x(0), y(0) {};

};

struct MaskedSegment {
  // Primary Read
  int32_t target_id; 
  int32_t start    ; 
  int32_t end      ; 
  int8_t  flags    ;

MaskedSegment(int target, int start, int end):  target_id(target), start(start), end(end), flags(0) {} ;
MaskedSegment():  target_id(0), start(0), end(0) {} ;

  inline int length() const { return( abs(end-start) ); };
  
  bool operator < (const MaskedSegment& r) const {
    if (target_id != r.target_id) {
      return (target_id < r.target_id);
    } else {
      return (start < r.start);
    }
  }

};


bool operator< (const CoverageStep& lhs, const CoverageStep& rhs) {
    if (lhs.target_id != rhs.target_id) {
      return (lhs.target_id < rhs.target_id);
    } else {
      return ( lhs.x < rhs.x ); 
    }
};

bool operator< (const CoverageStep& lhs, const Base& rhs) {
    if (lhs.target_id != rhs.target_id) {
      return (lhs.target_id < rhs.target_id);
    } else {
      return ( lhs.x < rhs.x ); 
    }
};

bool operator<= (const CoverageStep& lhs, const Base& rhs) {
    if (lhs.target_id != rhs.target_id) {
      return (lhs.target_id <= rhs.target_id);
    } else {
      return ( lhs.x <= rhs.x ); 
    }
};

bool operator< (const Base& lhs, const CoverageStep& rhs) {
    if (lhs.target_id != rhs.target_id) {
      return (lhs.target_id < rhs.target_id);
    } else {
      return ( lhs.x < rhs.x ); 
    }
};

bool operator<= (const Base& lhs, const CoverageStep& rhs) {
    if (lhs.target_id != rhs.target_id) {
      return (lhs.target_id < rhs.target_id);
    } else {
      return ( lhs.x <= rhs.x ); 
    }
};

bool operator> (const Base& lhs, const CoverageStep& rhs) {
    if (lhs.target_id != rhs.target_id) {
      return (lhs.target_id > rhs.target_id);
    } else {
      return ( lhs.x > rhs.x ); 
    }
};

std::ostream& operator<<(std::ostream& out, const MaskedSegment& seg) {
  // int bsize=1024;
  char* sbuff;
  sbuff=new char[1024];
  sprintf(sbuff,"(%d:%d-%d)",seg.target_id,seg.start,seg.end);
  out.write( sbuff , strlen(sbuff) );
  //reinterpret_cast<const char*>(&obj.field1),
  //         sizeof(obj.field1)); // or something better defined
  // ...
  return out;
}

//bool CoverageStep::operator< (const Base& r) const {
//    if (target_id != r.target_id) {
//      return (target_id < r.target_id);
//    } else {
//      return ( x < r.x ); 
//    }
//  }

  //  CoverageStep(int32_t,)
//};

class SegmentSet {
 public:
  using segment_vec = std::vector<MaskedSegment>;
  using iterator = segment_vec::iterator;
  using const_iterator = segment_vec::const_iterator;

  iterator begin() { return segments_.begin(); }
  iterator end() { return segments_.end(); }
  const_iterator begin() const { return segments_.begin(); }
  const_iterator end() const { return segments_.end(); }
  const_iterator cbegin() const { return segments_.cbegin(); }
  const_iterator cend() const { return segments_.cend(); }


  MaskedSegment& NewSeg() {
    segments_.resize(segments_.size()+1);
    return(segments_.back());
  }

  void AddSegment(MaskedSegment segment) {
    segments_.push_back(segment);
  }

  SegmentSet(void) {
    coverage_valid_ = false;
  }

  void ReadBed(const std::string & bed_file ,const BamHeaderInfo &header ) {
    std::string line,f1,f2,f3,f4;
    int x,y;
    //MaskedSegment seg;
    fprintf(stderr,"open mask file: %s\n",bed_file.c_str());
    std::ifstream myfile (bed_file,std::ifstream::in);
    if (myfile.is_open())
      {
	segments_.clear();
	while ( std::getline (myfile,line) )
	  {

	    std::stringstream ls(line);
	    if (
		std::getline(ls,f1,'\t') &&
		std::getline(ls,f2,'\t') &&
		std::getline(ls,f3,'\t') &&
		std::getline(ls,f4) 
		) {
	      	
	      std::map<std::string, int32_t>::const_iterator i = header.target_idx_.find(f1);

	      if (i != header.target_idx_.end()) {
		//	      masked_segments_.resize(masked_segments_.size()+1);
		x=stoi(f2);
		y=stoi(f3);
		if (y<=x) {continue;}
		

	      MaskedSegment &seg = NewSeg();

	      // i != target_idx_.end() ? seg.target_id = i->second : seg.target_id = -1;

	      seg.start     = x   ;
	      seg.end       = y   ;
	      seg.target_id = i->second ;
	      if      (f4=="deep")         { seg.flags = DEEP;  }
	      else if (f4=="promiscuous")  { seg.flags = PROMISCUOUS; }
	      else if (f4=="gap")          { seg.flags = GAP; }
	      else if (f4=="readDesert")   { seg.flags = DESERT; }

	      //fprintf(stdout,"seg: %s\t%d\t%d\t%d\t%d\n",f1.c_str(),seg.target_id,seg.start,seg.end,seg.flags);
	      } else {
		//		fprintf(stdout,"Missing scaffold id %s:  %s\t%s\t%s\t%s\n",f1.c_str(),f1.c_str(),f2.c_str(),f3.c_str(),f4.c_str());
	      }	
	    } else {
	      fprintf(stderr,"Check that the bed file is tab delimited all the way through.\n");
	      assert(0);
	      //	      str.setstate(std::ios::failbit);
	    }
	    //std::cout << line << '\n';
	  }
	myfile.close();
      }

    else std::cout << "Unable to open file";

    ComputeCoverage();
  }

  void dumpCoverage() const {
    fprintf(stdout,"dumpCoverage.  (%d segmetns)\n",int(segments_.size()));
    for (int i=0;i<int(segments_.size());i++) {
      fprintf(stdout,"segment: %d\t%d\t%d\t%d\n",i,segments_[i].target_id,segments_[i].start,segments_[i].end);
    }
    if (!coverage_valid_) {
      assert(1==0);
      //ComputeCoverage();

    };
    for (int i=0;i<int(coverage_steps_.size());i++) {
      fprintf(stdout,"coverage curve: %d\t%d\t%d\t%g\n",i,coverage_steps_[i].target_id,coverage_steps_[i].x,coverage_steps_[i].y );
    }
  }

  bool isCovered(Read r) const {
    return ( isCovered( Base(r.target_1,r.pos_1) ) || isCovered( Base(r.target_2,r.pos_2) ) ); 
  }

  bool isCovered(std::vector<Read>::iterator r) const {
    return ( isCovered( Base(r->target_1,r->pos_1) ) || isCovered( Base(r->target_2,r->pos_2) ) ); 
  }

  bool isCovered(Base x) const {

    if (coverage_steps_.size() == 0) {return(false);}
    
    if (!coverage_valid_) {
      assert(0);
      //      ComputeCoverage();
    };
    //TODO:  accelerate with binary search of coverage_steps_;
    int i=0;    

#if BINARY_SEARCH_CHECK    
    while ((i<=coverage_steps_.size()) && (coverage_steps_[i]<=x)) {i++;}
    i-=1;
#endif

    int lo=0;
    int mid=0;
    int hi=coverage_steps_.size()-1;
    if (x<coverage_steps_[0])       {return(false);}
    if (coverage_steps_.back() <= x) {return(false);}

    mid = lo + (hi-lo)/2;
    while (lo<hi) {
      if ( (coverage_steps_[mid]<=x) && !(coverage_steps_[mid+1]<=x) ) {
	//fprintf(stdout,"break!\n");
	break;}
      if ( (coverage_steps_[hi]<=x) && !(coverage_steps_[hi+1]<=x) ) {
	mid=hi; 
	//fprintf(stdout,"break!\n"); 
	break;}
      if ( (coverage_steps_[lo]<=x) && !(coverage_steps_[lo+1]<=x) ) {
	mid=lo; 
	//fprintf(stdout,"break!\n"); 
	break;}
      if (coverage_steps_[mid]<x) {
	lo=mid+1;
      }	else if (x<=coverage_steps_[mid]) {
	hi=mid-1;
      } else {
	assert(0);
      }
      //      fprintf(stdout,"lo,mid,hi (i): %d,%d,%d  (%d)\t: %d %d / %d %d %g -- %d %d\n",lo,mid,hi,i,x.target_id,x.x,coverage_steps_[lo].target_id,coverage_steps_[lo].x,coverage_steps_[lo].y,coverage_steps_[hi].target_id,coverage_steps_[hi].x);
      mid = lo + (hi-lo)/2;
      
    }

#if BINARY_SEARCH_CHECK
    fprintf(stdout,"mid,i: %d %d\n",mid,i);  
    assert(i==mid);
#else
    i=mid;
#endif

    //fprintf(stdout,"test:\t%d\t%d\t%d\t%g :: %d\t%d\n",i,coverage_steps_[i].target_id,coverage_steps_[i].x,coverage_steps_[i].y,x.target_id,x.x);
    if (i<0) {return(false);}
    if (i==int(coverage_steps_.size())) {return(false);}
    //    if (x<coverage_steps_[i]) {return(false);}
    if (coverage_steps_[i].y>0) { return(true); } else { return(false);}
  }

  void ComputeCoverage() {
    //std::cerr << "coverage" << "\n";
    coverage_steps_.clear();
    coverage_steps_.reserve( segments_.size()*2 );
    for(std::vector<MaskedSegment>::iterator it = segments_.begin(); it != segments_.end(); ++it) {
      //std::cerr << "t: " << it->target_id << " " << it->start << " " << it->end << "\n";
      coverage_steps_.push_back( CoverageStep(it->target_id,it->start,1.0) ) ;
      coverage_steps_.push_back( CoverageStep(it->target_id,it->end ,-1.0) ) ;
      
    }
  
    //for (int i=0;i<coverage_steps_.size();i++) {
    //  fprintf(stdout,"x: %d\t%d\t%d\t%g\n",i,coverage_steps_[i].target_id,coverage_steps_[i].x,coverage_steps_[i].y);
    //}


    std::sort(coverage_steps_.begin(),coverage_steps_.end());

    double rs=0.0;
    for(std::vector<CoverageStep>::iterator it = coverage_steps_.begin(); it != coverage_steps_.end(); ++it) {
      rs += it->y;
      it->y = rs;
    }    


    coverage_valid_ = true;
    std::cerr << "coverage done \n" ;
  }

  /*
  bool operator covers (const Base b) const {
    //TODO  test whether base b is covered by the spans.    
    //by using a binary search... 
  }
  */

  std::vector<CoverageStep> coverage_steps_;

 private:
  segment_vec segments_;
  bool coverage_valid_;
};

#endif
