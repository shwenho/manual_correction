#ifndef CHICAGO_SV_BINNER_H
#define CHICAGO_SV_BINNER_H

#include <stdexcept>
#include <fstream>
#include <sstream>
#include <map>
#include "chicago_read_pairs.h"

#define INTER_CHROM_PADDING 10000

struct ChromIndex {
    std::string chrom_name;
    int64_t start;
    int64_t end;
    int64_t length;

    ChromIndex(std::stringstream& buf) {
        buf >> chrom_name >> start >> end >> length;
    }
    ChromIndex(std::string _chrom_name,
               int64_t _start,
               int64_t _end,
               int64_t _length) :
        chrom_name(_chrom_name), start(_start), end(_end), length(_length) {}
};

typedef std::vector<ChromIndex> GenomeMap;

class SVReadPairBinner {
private:
    int64_t m_binsize;
    GenomeMap m_genome_map;

    const int64_t _get_scaffold_id(std::string scaffold) const {
        for (int64_t i = 0; i < m_genome_map.size(); ++i)
            if (m_genome_map[i].chrom_name == scaffold)
                return i;
        throw std::runtime_error("Invalid scaffold ID: " + scaffold);
    }

    const int64_t _pos_to_bin(int64_t scaffold, int64_t pos) const {
        const ChromIndex& idx = m_genome_map[scaffold];
        return (pos+idx.start) / m_binsize;
    }

    const int64_t _pos_to_bin(std::string scaffold, int64_t pos) const {
        return _pos_to_bin(_get_scaffold_id(scaffold), pos);
    }

public:
    SVReadPairBinner(int64_t _binsize) : m_binsize(_binsize) {}

    // Getters
    const int64_t get_binsize() const { return m_binsize; }

    void load_genome_map(std::string fp) {
        // Load genome map from file
        std::ifstream stream(fp.c_str());
        return load_genome_map(stream);
    }

    void load_genome_map(std::ifstream& stream) {
        if (stream.fail())
            throw std::runtime_error("Failed to read genome map.");
        std::string line, chrom_name;
        while (std::getline(stream, line)) {
            std::stringstream buf(line);
            m_genome_map.push_back(ChromIndex(buf));
        }
        stream.close();
    }

    void load_genome_map(const References& refs) {
        int64_t length = 0;
        References::const_iterator it;
        for (it = refs.begin(); it != refs.end(); ++it) {
            ChromIndex idx(it->first, length, length+it->second, it->second);
            m_genome_map.push_back(idx);
            length += it->second + INTER_CHROM_PADDING;
        }
    }

    void bin(const Read& read, int64_t& xbin, int64_t& ybin) const {
        assert(read.target_1 == read.target_2);
        int bin1 = _pos_to_bin(read.target_1, read.pos_1);
        int bin2 = _pos_to_bin(read.target_2, read.pos_2);
        xbin = (bin1+bin2) / 2;
        ybin = abs(bin2-bin1);
    }

    void bin(std::string scaffold,
             int64_t start,
             int64_t end,
             std::map<int64_t, int64_t>& bins) const {
        for (int64_t i = start; i < end; ++i)
            bins[_pos_to_bin(scaffold, i)] += 1;
    }

    void bin(std::string scaffold,
             int64_t start,
             int64_t end,
             std::map<int64_t, int64_t>& bins,
	     int64_t& scaffold_id
	     ) const {
      scaffold_id = _get_scaffold_id(scaffold);
      for (int64_t i = start; i < end; ++i)
	bins[_pos_to_bin(scaffold_id, i)] += 1;
    }

    bool debin(int64_t xbin, std::string& chrom, int64_t& pos) const {
        GenomeMap::const_iterator it;
        int64_t genome_pos = xbin * m_binsize;
        for (it = m_genome_map.begin(); it != m_genome_map.end(); ++it) {
            if (it->start <= genome_pos && genome_pos < it->end) {
                chrom = it->chrom_name;
                pos = genome_pos - it->start;
                return true;
            }
        }
        return false;
    }
};


#endif
