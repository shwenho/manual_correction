#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <time.h>
#include <float.h>
#include "picojson.h"
#include "chicago_read.h"
#include "link_range_iterator.h"
#include "chicago_links.h"
#include "paired_read_likelihood_model.h"
#include "model_parser.h"
#include "chicago_scaffolds.h"
#include "hirise_threads.h"
//#include "oo_requests.h"
#include <fenv.h>
#include <tclap/CmdLine.h>
#include "contig_index.h"

void log_progress_info( std::ostream& out,const std::string& message,const std::chrono::time_point<std::chrono::high_resolution_clock>& chrono_start) {
  out << message ;
  out << "[ " << std::chrono::duration_cast<std::chrono::minutes>(  std::chrono::high_resolution_clock::now() - chrono_start   ).count() << " minutes in ]";
  out << "\n" ;
  out.flush();
}


inline bool looks_like_bamfile(std::string const &filename) {
  if ( filename.length() >= 5 ) {
    return (0== filename.compare( filename.length() - 4, 4, ".bam" ) );
  } 
  return false;
}

inline bool file_exists (const std::string& name) {
  ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }   
}

struct GenericOpts {
  int nthreads;
  std::string outfile;
  double minscore;
  GenericOpts() : nthreads(1), minscore(20.0) {};
};

std::vector<PairedReadLikelihoodModel *> set_models(const std::vector<std::string>& model_files) {
  std::string model_json;
  std::vector<PairedReadLikelihoodModel *> models;

  for (auto& model_file : model_files) {
    // reset model_stream for every file
    std::ifstream model_stream ;

    model_stream.open(model_file);
    std::getline(model_stream, model_json);
    LikelihoodModelParser model_parser;
    PairedReadLikelihoodModel *model = model_parser.ParseModelJson(model_json.c_str());
    models.push_back(model);
  }
  return models;
}

void check_file(const std::string filename) {
  bool exists = file_exists(filename);
  if(!exists) {
    std::cerr << "Could not open file: " << filename << "\n";
    throw std::runtime_error("Unable to open file");
  }
}

void parse_command_line(int argc, char** argv, int& mapq, std::string& hra, int& nthreads, std::string& read_dump,
			std::vector<std::string>& models, std::vector<std::string>& bams,
			std::string& mask, std::string& output) {
  try {  
    TCLAP::CmdLine cmd("Command description message", ' ', "0.9");

    TCLAP::ValueArg<int> mapArg("q","mapquality","Map quality score",false,20, "int");
    cmd.add(mapArg);

    TCLAP::ValueArg<std::string> hraArg("i","hra","hra file",true,"","filename");
    cmd.add(hraArg);

    TCLAP::ValueArg<int> threadArg("j","threads","Number of threads to use",false, 16,"int");
    cmd.add(threadArg);
    
    TCLAP::ValueArg<std::string> dumpArg("X","reads","Binary file of reads (from bam + models)",false, "", "filename");
    cmd.add(dumpArg);

    TCLAP::MultiArg<std::string> modelArg("M","model","File of library model",true, "filename");
    cmd.add(modelArg);

    TCLAP::MultiArg<std::string> bamArg("b","bam","Bam file (must be matched with a model file",true, "filename");
    cmd.add(bamArg);

    TCLAP::ValueArg<std::string> maskArg("m","mask","BED file of masked out regions",true, "", "filename");
    cmd.add(maskArg);

    TCLAP::ValueArg<std::string> outputArg("o","output","File to write output",true, "", "filename");
    cmd.add(outputArg);


    // Parse command line arguments
    cmd.parse( argc, argv );

    // Get the value parsed by each arg. 
    mapq = mapArg.getValue();
    hra = hraArg.getValue();
    nthreads = threadArg.getValue();
    read_dump = dumpArg.getValue();
    models = modelArg.getValue();
    bams = bamArg.getValue();
    mask = maskArg.getValue();
    output = outputArg.getValue();
    
    check_file(hra);
    check_file(mask);
    for(auto& bam : bams) {
      check_file(bam);
    }
    for(auto& model : models) {
      check_file(model);
    }
    
    if(read_dump != "") {
      check_file(read_dump);
    }
    // expand models vector if only 1 given, but multiple bam files
    if(models.size() == 1 && bams.size() > 1) {
      for(uint i = 1; i < bams.size(); i++) {
	models.push_back(models[0]);
      }
    }

    // if not that particular case, then they need to be the same number
    if (bams.size() != models.size()) {
      std::cerr << "Multiple bamfiles and model files cannot disagree in counts: " << bams.size() << " " << models.size() << "\n";
      exit(1);
    }

  } catch (TCLAP::ArgException &e)  // catch any exceptions
    { std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; }
}

/*
GenericOpts setup(int argc, char * argv[],   ChicagoLinks& links, PathSet& scaffolds ) {
  GenericOpts opts;
  bool restore_dump=false;
  bool read_bed=false;
  int mapq = 0;
  int n_threads=16;
  std::string hrafile;
  std::vector<std::string> model_files;
  std::string output_file;
  std::string reads_dump_in ;
  std::string bedfile ;
  std::vector<std::string> bamfiles;
  std::string shotfile = "";

  // start the clock!
  auto chrono_start = std::chrono::high_resolution_clock::now();

  parse_command_line(argc, argv, mapq, hrafile, n_threads, reads_dump_in, model_files, bamfiles, bedfile);
  bool use_broken_contig = true;

  auto models = set_models(model_files);

  links.min_mapq() = mapq;
  std::cout << restore_dump << std::endl;
  if (restore_dump) { 
    std::cout << "restoring reads " << std::endl;
    links.PopulateFromBam( bamfiles[0].c_str(), false, models[0], 0 ,reads_dump_in);
    links.SetModelsVector(models);
  }
  else {
    // If only one model, use for all chicago bamfiles (model_increment == 0)
    // otherwise chicago & models match up one by one (model_increment == 1)

    for (int i = 0, model_i = 0; i < bamfiles.size() && model_i < models.size(); i++, model_i += model_increment) {
      std::cout << "Load data from:" <<  bamfiles[i] << "\twith model:\t" << model_files[model_i] << "as bamfile? "<< looks_like_bamfile(bamfiles[i]) << "\n";
      if (looks_like_bamfile(bamfiles[i])) {
	fprintf(stdout,"looks like bamfile: %s\n",bamfiles[i].c_str());
	links.PopulateFromBam(   bamfiles[i] , false, models[model_i], model_i );
      } else {
	fprintf(stdout,"looks like tablefile: %s\n",bamfiles[i].c_str());
	links.PopulateFromTable( bamfiles[i] , false, models[model_i], model_i );	
      }	
    }
    //    links.SortByContigs();
    std::cout << "\n";
  }

  log_progress_info(std::cout, "Done loading read data", chrono_start);

  scaffolds.SetHeader(links.GetHeader());
  //  PathSet scaffolds(links.GetHeader());
  //  OO_Requests oo_reqs;

  scaffolds.ParseFromHRA(links.GetHeader(), hrafile );

  log_progress_info(std::cout, "Done loading scaffolding", chrono_start);

  //std::cout << "##\n";
  //std::cout << scaffolds;


  if (read_bed) {
    scaffolds.ReadMaskedSegments(bedfile,links.GetHeader());
    log_progress_info(std::cout, "Done loading masked segments", chrono_start);
  }

  const SegmentSet masking = scaffolds.MaskedSegments() ;
  links.SetMaskedFlags( masking );
  log_progress_info(std::cout, "Done setting masking flags", chrono_start);
  
  // "fake" ugliness affects this file only. Can call utility method in a cleaner way later, if needed.
  if (!shotfile.empty()) {
    if (shotfile == "fake") {
      scaffolds.FakeContigSegShotgun();
    }
    else {
      scaffolds.CountContigSegShotgun(shotfile);
    }
    fprintf(stdout, "Switching to MetaHirise\n");
    for (int i = 0; i < models.size(); i++) {
      //ChicagoExpModel* cmodel = dynamic_cast<ChicagoExpModel*>(models[i]);
      models[i]->SetMetagenomic(scaffolds.ShotgunCounted(), false);
    }
  }

  //  if (!oo_req_file.empty()) {
  //  Load_OO_Reqs(oo_reqs, oo_req_file, true);
  //}
    



  //  if (use_broken_contig) {
    fprintf(stderr,"Switching to broken contig coordinates...\n");
    links.SwitchToBrokenContigCoordinates( scaffolds );
  log_progress_info(std::cout, "Done switching to broken scaffold coordinates", chrono_start);
    //} else {
    // fprintf(stderr,"Switching to scaffold coordinates...\n");
    //links.SwitchToScaffoldCoordinates( scaffolds );
    //}
  fprintf(stderr,"Indexing masked segments...\n");
  fprintf(stdout,"Indexing masked segments...\n");
  scaffolds.IndexMaskedScaffoldSegments();
  log_progress_info(std::cout, "Done indexing the masked segemnts in broken contig coordinates", chrono_start);

  return opts;

}
*/

void read_in_reads(std::string read_dump, std::vector<std::string> bam_files,
		   std::vector<PairedReadLikelihoodModel *> models,
		   std::vector<std::string> model_files, 
		   ChicagoLinks& links) {

    // restore reads from binary dump
    if (read_dump != "") { 
      std::cout << "restoring reads " << read_dump << "\n";
      links.PopulateFromBam( bam_files[0].c_str(), false, models[0], 0 ,read_dump);
      links.SetModelsVector(models);
      std::cout << "got " << links.size() << " links\n";
    }

    // load reads from bam files
    else {
      // expects model files to match bam files
      if(bam_files.size() != model_files.size()) {
	  throw std::runtime_error("Must have same number of bams as models.\n");
	}
      for (int i = 0; i < bam_files.size(); i++) {
	std::cout << "Load data from: " <<  bam_files[i] << "\twith model:\t" << model_files[i] << "as bamfile? "<< looks_like_bamfile(bam_files[i]) << "\n";
	if (looks_like_bamfile(bam_files[i])) {
	  std::cout << "looks like bamfile: " << bam_files[i] << "\n";
	  links.PopulateFromBam(   bam_files[i] , false, models[i], i );
	} else {
	  std::cout << "looks like tablefile: " << bam_files[i] << "\n";
	  links.PopulateFromTable( bam_files[i] , false, models[i], i );	
	}	
      }
    std::cout << "\n";
  }

}
void set_masking(ChicagoLinks& links, std::string mask, 
		 PathSet& scaffolds) {
    if (mask != "") {
      scaffolds.ReadMaskedSegments(mask,links.GetHeader());
    }
    const SegmentSet masking = scaffolds.MaskedSegments() ;
    links.SetMaskedFlags( masking );
}

void set_links_and_scaffolds(ChicagoLinks& links, PathSet& scaffolds, int mapq, const std::string hra, const std::string read_dump,
			     const std::vector<std::string> model_files, const std::vector<std::string> bam_files, const std::string mask, 
			     const std::chrono::time_point<std::chrono::high_resolution_clock>& chrono_start) {

  auto models = set_models(model_files);
  
  links.min_mapq() = mapq;
  read_in_reads(read_dump, bam_files, models, model_files, links);
  log_progress_info(std::cout, "Done loading read data", chrono_start);

  scaffolds.SetHeader(links.GetHeader());
  scaffolds.ParseFromHRA(links.GetHeader(), hra );
  log_progress_info(std::cout, "Done loading scaffolds", chrono_start);
    
  set_masking(links, mask, scaffolds);
  log_progress_info(std::cout, "Done setting masking flags", chrono_start);

  log_progress_info(std::cout, "Switching to broken contig coordinates...\n", chrono_start);
  links.SwitchToBrokenContigCoordinates(scaffolds);
  log_progress_info(std::cout, "Done switching to broken scaffold coordinates", chrono_start);

  auto links_index_contigs   =  IndexContigs( links ) ;
  log_progress_info(std::cout, "Time: Done building index of links table by contig pair", chrono_start);
  auto links_index_scaffolds =  IndexScaffolds( links , scaffolds) ;
  log_progress_info(std::cout, "Time: Done building index of links table by scaffold pair", chrono_start);

  links.InitContigIndex( links_index_contigs, links_index_scaffolds  );
  log_progress_info(std::cout, "Time: Done indexing the links table by contigs and scaffolds", chrono_start);

  log_progress_info(std::cout, "Indexing masked segments...\n", chrono_start);
  scaffolds.IndexMaskedScaffoldSegments(true);
  log_progress_info(std::cout, "Done indexing the masked segments in broken contig coordinates", chrono_start);


  /*
  links.SortByScaffolds(scaffolds);
  log_progress_info(std::cout, "Time: Done sorting by scaffolds", chrono_start);
  update_contig_index( links, links_index_contigs ) ;
  log_progress_info(std::cout, "Time: Done re-building index of links table by contig pair", chrono_start);
  links_index_scaffolds =  IndexScaffolds( links , scaffolds) ;
  log_progress_info(std::cout, "Time: Done re-building index of links table by scaffold pair", chrono_start);
  links.InitContigIndex( links_index_contigs, links_index_scaffolds );
  */
}
