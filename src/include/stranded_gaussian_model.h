#ifndef STRANDED_GAUSSIAN_MODEL_H
#define STRANDED_GAUSSIAN_MODEL_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
#include <Eigen/Dense>
#include <picojson.h>
#include "error_msg.h"
#include "chicago_read.h"
#include "segment_set.h"
#include "paired_read_likelihood_model.h"
#include "gaussian_lm.h"


class StrandedGaussianLM : public PairedReadLikelihoodModel {

 public:

  StrandedGaussianLM() : expected_orientation_(0) { Init(); }

  StrandedGaussianLM(const char *model_json) {
    expected_orientation_=0;
    Init();
    gmodel_->SetModel(model_json);
  }

  void SetModel(const char *model_json) {    
    gmodel_->SetModel(model_json);  

    try {
      picojson::value v;
      std::string err = picojson::parse(v, model_json);
      if(err != "") {
	std::cerr << "model file " << model_json << " has pico json err: " << err;
	throw std::runtime_error(err);
      }

      auto orientation = v.get("orientation").get<std::string>();

      if (orientation=="out" || orientation=="outie" ) {
	std::cout << "make an outie model";
	expected_orientation_=1;
      } 
    } catch (...) {
      
    }
  } ;

  void SetModel(const char *model_json, const bool fillCache ) {    
    gmodel_->SetModel(model_json, fillCache);  
    try {
      picojson::value v;
      std::string err = picojson::parse(v, model_json);
      auto orientation = v.get("orientation").get<std::string>();
      //      auto orientation = v.get("orientation");
      if (orientation=="out" || orientation=="outie" ) {
	std::cout << "make an outie model";
	expected_orientation_=1;
      } 
    } catch (...) {
      
    }

  } ;

  void LLRAreaContribution( ContigPair *cp , int n, double *result ) {
    gmodel_->LLRAreaContribution(cp,n,result);
  }

  void LLRReadPairContribution( ContigPair *cp, Read r, double *result ) {
    if ( has_expected_orientation(cp,r) ) {
      gmodel_->LLRReadPairContribution(cp,r,result);
    } 
  };

  void fit(const HistogramProvider& data) {assert(1==0);};

  void InsertSizeDist(     int l, double *result ) { gmodel_->InsertSizeDist(l,result)    ; } ;
  float InsertSizeDistF(   int l                 ) { return(gmodel_->InsertSizeDistF(l))    ; } ;
  void NonNoiseInsertDist( int l, double *result ) { gmodel_->NonNoiseInsertDist(l,result); } ;

  void H0( int l, double *result ) { gmodel_->H0(l,result); } ;
  double genome_size() { return( gmodel_->genome_size()); };
  double noise_probability() { return(gmodel_->noise_probability()); } ;
  const int32_t N() const { return(gmodel_->N()); } ;

  void RescaleGenomesize(double GG, bool fill_cache=false) { gmodel_->RescaleGenomesize(GG,fill_cache); }  ;

  void n_bar(int l1,int l2,int g,double *l) {gmodel_->n_bar(l1,l2,g,l);};
  void n_bar(int l1,double *l) {             gmodel_->n_bar(l1,l);  }
  void set_N(double N) {    gmodel_->set_N(N);  };


  int sample_distribution() { return(gmodel_->sample_distribution()); };

  void SetMetagenomic(int64_t n_shotgun, bool useDepthPenalty=false) { gmodel_->SetMetagenomic(n_shotgun,useDepthPenalty); } ;
  void UnSetMetagenomic() {                                            gmodel_->UnSetMetagenomic(); };
  bool NeedDepthDiscrep(ContigPair *cp) { return(gmodel_->NeedDepthDiscrep(cp)); };
  void LLRDepthDiscrep(ContigPair *cp, double *result) {gmodel_->LLRDepthDiscrep(cp,result); };

  void DescriptionString(char* sbuff) {
    sprintf(sbuff,"Stranded Gaussian Model: ");
    gmodel_->DescriptionString(sbuff + 25);
  };

 private:

  int expected_orientation_;
  
  bool has_expected_orientation(ContigPair *cp, Read r) {

    //   0 :    -->      <--    Innie
    //   1 :    <--      -->    Outie
    //   2 :    -->      -->    Fwd
    //   3 :    <--      <--    Rev

    //    std::cout << "debug stranded model: pair type: " << int(cp->pair_strand_type(r)) << "\tcp:\t" << int(cp->strand_1) << "\t" << int(cp->strand_2) << "\tread:\t" << int(r.read1_flipped()) << "\t" << int(r.read2_flipped()) << "\n"; 

    if (cp->pair_strand_type(r) == expected_orientation_ ) { return(true); }  // this models a library where the pairs are supposed to be innies.
 
    return(false);
  }

  void Init() {
    gmodel_ = new GaussianLM();
  };
  GaussianLM *gmodel_ ;

};

#endif
