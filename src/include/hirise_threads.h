#ifndef HIRISE_THREAD_H
#define HIRISE_THREAD_H

#include <pthread.h>
#include <map>
#include <vector>
#include <list>
#include <deque>
#include "common.h"
#include "error_msg.h"
#include <assert.h>
#include <stdint.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "scaffold_breaker.h"
#include "scaffold_join_score.h"
#include "oo_requests.h"

static void* threadLauncher(void* arg);

class HRThread {
 public:
  
 HRThread(): thread_id_(0), running_(0), detached_(0) {  };
  virtual ~HRThread() {
    {
      if (running_ == 1 && detached_ == 0) {
	pthread_detach(thread_id_);
      }
      if (running_ == 1) {
	pthread_cancel(thread_id_);
      }
    }
  }

  int start() {
    int result = pthread_create(&thread_id_, NULL, threadLauncher, this);
    if (result == 0) {
      running_ = 1;
    }
    return result;
  };

  int join() {
    int result = -1;
    if (running_ == 1) {
      result = pthread_join(thread_id_, NULL);
      if (result == 0) {
        running_ = 0;
	detached_ = 1;
      }
    }
    return result;
  };

  int detach() {
    int result = -1;
    if (running_ == 1 && detached_ == 0) {
      result = pthread_detach(thread_id_);
      if (result == 0) {
	detached_ = 1;
      }
    }
    return result;
  };

  pthread_t id() {
    //return id_;
    return thread_id_;
  }
  
  virtual void* run() = 0;

private:
  pthread_t thread_id_;
  int running_ ;
  int detached_;
};

static void* threadLauncher(void* arg)
{
  return ((HRThread*)arg)->run();
}

template<typename T> class HRTaskQueue {
  
public:
  HRTaskQueue() {
    pthread_mutex_init( &mutex_, NULL);
    pthread_cond_init(  &condv_, NULL);
    closed_=0;
  }

  ~HRTaskQueue() {
    pthread_mutex_destroy( &mutex_ );
    pthread_cond_destroy(  &condv_ );
  }

  void enqueue(T task) {
    pthread_mutex_lock(   &mutex_ );
    tasks_.push_back(      task   );
    pthread_cond_signal(  &condv_ );
    pthread_mutex_unlock( &mutex_ );
  }

  /* this seems like a nice idea, but it leads to a deadlock... TODO ? */
  void enqueue(T task, int max_qsize) {
    pthread_mutex_lock(   &mutex_ );
    while ( tasks_.size() > max_qsize ) {
      pthread_cond_wait(&condv_, &mutex_);
    }
    
    tasks_.push_back(      task   );
    pthread_cond_signal(  &condv_ );
    pthread_mutex_unlock( &mutex_ );
  }

  T dequeue() {
    pthread_mutex_lock(   &mutex_ );
    while (closed_!=1 && tasks_.size() == 0) {
      pthread_cond_wait(&condv_, &mutex_);
    }

    if (closed_==1 && tasks_.size() == 0) {
      pthread_mutex_unlock( &mutex_ );    
      return T();
    }

    T result = tasks_.front();
    tasks_.pop_front();
      
    pthread_mutex_unlock( &mutex_ );    
    return result;
  }

  void dequeue(int n, std::vector<T> & thread_queue) {
    int i=0;
    pthread_mutex_lock(   &mutex_ );
    while (closed_!=1 && tasks_.size() == 0) {
      pthread_cond_wait(&condv_, &mutex_);
    }

    if (closed_==1 && tasks_.size() == 0) {
      pthread_mutex_unlock( &mutex_ );    
      thread_queue.push_back(T());
      return;
    }

    while ( tasks_.size() >0 && i<n ) {
      T result = tasks_.front();
      tasks_.pop_front();
      thread_queue.push_back(result);
      i++;
    }  

    pthread_mutex_unlock( &mutex_ );    
    //    return result;
    return;
  }

  bool Done() {
    bool result ; 
    pthread_mutex_lock(   &mutex_ );
    result = (closed_==1)&& (tasks_.size()==0);
    pthread_mutex_unlock( &mutex_ );
    return result ;    
  }

  bool isClosed() {
    bool result ; 
    pthread_mutex_lock(   &mutex_ );
    result = (closed_==1);
    pthread_mutex_unlock( &mutex_ );
    return result ;
  }

  void close() {
    pthread_mutex_lock(   &mutex_ );
    closed_ = 1;
    pthread_cond_broadcast(  &condv_ );
    pthread_mutex_unlock( &mutex_ );
  }

private:

  int size() {
    int result ; 
    //    pthread_mutex_lock(   &mutex_ );    These shouldn't be needed because threads calling this alreday have the lock.  
    result = tasks_.size();
    //    pthread_mutex_unlock( &mutex_ );
    return result ;
  }


  std::deque<T>    tasks_;
  pthread_mutex_t mutex_;
  pthread_cond_t  condv_;
  int closed_;

};

//  std::vector<JoinerWorkerThread *> threads  ;
//  HRTaskQueue<HRScaffoldJoinScoreTask> queue;

struct HRScaffoldJoinScoreTask {
  int scaffold_1;
  int scaffold_2;
  unsigned long long mini, maxi;
  //  ChicagoLinks links;
  const ChicagoLinks& links ;  
  const PathSet& scaffolds ;  
  OO_Vec *oo_reqs;
  bool dummy_;  // this isn't a real task -- it's a dummy that means "there are no more tasks coming"

HRScaffoldJoinScoreTask(const ChicagoLinks& l, const PathSet& p, int s1, int s2, unsigned long long i, unsigned long long j, OO_Vec *oor = NULL): links(l), scaffolds(p), scaffold_1(s1), scaffold_2(s2), mini(i), maxi(j), oo_reqs(oor), dummy_(false) {}
HRScaffoldJoinScoreTask(): dummy_(true), links( ChicagoLinks() ), scaffolds( PathSet() ) {};
  bool operator==(int x) const {
    if (x==0 && dummy_){return(true);}
    else {return(false);}
  }
};


struct HRScaffoldIntercScoreTask {
  int scaffold_1;
  int scaffold_2;
  const ChicagoLinks& links ;  
  const PathSet& scaffolds ;  
  int gapsize = 1000;
  bool dummy_;  // this isn't a real task -- it's a dummy that means "there are no more tasks coming"
  bool end_to_end_only;

HRScaffoldIntercScoreTask(const ChicagoLinks& l, const PathSet& p, int s1, int s2, int gapsize): links(l), scaffolds(p), scaffold_1(s1), scaffold_2(s2), dummy_(false), end_to_end_only(false) {}
HRScaffoldIntercScoreTask(const ChicagoLinks& l, const PathSet& p, int s1, int s2, int gapsize, bool flag): links(l), scaffolds(p), scaffold_1(s1), scaffold_2(s2), dummy_(false), end_to_end_only(flag) {}
HRScaffoldIntercScoreTask(): links(ChicagoLinks() ), scaffolds(PathSet()), dummy_(true), end_to_end_only(false) {}

  //HRScaffoldJoinScoreTask(): dummy_(true), links( ChicagoLinks() ), scaffolds( PathSet() ) {};
  bool operator==(int x) const {
    if (x==0 && dummy_){return(true);}
    else {return(false);}
  }
};


class IntercScoreWorkerThread :  public HRThread {
  HRTaskQueue<HRScaffoldIntercScoreTask>& queue_;
  int id_;
  std::string outfile_name;
  ofstream thread_out;
  Joiner joiner;
  std::vector<HRScaffoldIntercScoreTask> local_queue_;

public:
  std::vector<ScaffoldMergeMove> moves_ ;
 
 IntercScoreWorkerThread(HRTaskQueue<HRScaffoldIntercScoreTask>& queue, int i, std::string ofname) : queue_(queue), id_(i), outfile_name(ofname), joiner(thread_out) {
    thread_out.open( outfile_name );
    local_queue_.resize(0);
    //    breaker = Breaker(thread_out);
  }

  void finalize() {
    std::sort(moves_.begin(),moves_.end());
  }

  void* run() {
    ScaffoldMergeMove move;
    uint j,k,i;
    bool done=false;
    for (int i=0;;i++) {
      //      HRScaffoldJoinScoreTask task= (HRScaffoldJoinScoreTask) queue_.dequeue();
      queue_.dequeue(20,local_queue_);
      j=0;
      thread_out << "#worker thread " << id_ << " (" << id() << ") got " << local_queue_.size() << " tasks for local queue\n"; //,id_,id(),local_queue_.size());
      for (j=0;j<local_queue_.size();j++) {
	HRScaffoldIntercScoreTask task= local_queue_[j];
	if (task==0) { 
	  done=true;
	  break;
	}

	double x=0.0;
	joiner.IntercScore(task.scaffold_1,task.scaffold_2,task.links,task.scaffolds,thread_out,moves_);
	//	if(move.score >0.0) {moves_.push_back(move);}
	if (queue_.Done() and local_queue_.size()==0) {done=true; break; }

      }
      local_queue_.resize(0);
      if (done) break;
    }
    thread_out.close();
    finalize();
    return NULL;
  }
};



class AltJoinsWorkerThread :  public HRThread {
  HRTaskQueue<HRScaffoldIntercScoreTask>& queue_;
  int id_;
  std::string outfile_name;
  ofstream thread_out;
  Joiner joiner;
  std::vector<HRScaffoldIntercScoreTask> local_queue_;

public:
  std::vector<ScaffoldMergeMove> moves_ ;
 
  AltJoinsWorkerThread(HRTaskQueue<HRScaffoldIntercScoreTask>& queue, int i, std::string ofname) : queue_(queue), id_(i), outfile_name(ofname), joiner(thread_out) {
    //    thread_out.open( outfile_name );
    local_queue_.resize(0);
  }

  void finalize() {
    std::sort(moves_.begin(),moves_.end());
  }

  void* run() {
    ScaffoldMergeMove move;
    uint j,k,i;
    bool done=false;
    for (int i=0;;i++) {
      //      HRScaffoldJoinScoreTask task= (HRScaffoldJoinScoreTask) queue_.dequeue();
      queue_.dequeue(20,local_queue_);
      j=0;
      //      thread_out << "#worker thread " << id_ << " (" << id() << ") got " << local_queue_.size() << " tasks for local queue\n"; //,id_,id(),local_queue_.size());
      for (j=0;j<local_queue_.size();j++) {
	HRScaffoldIntercScoreTask task= local_queue_[j];

	if (task==0) {  done=true;break;} // a dummy task signaling no more are coming.

	double x=0.0;
	joiner.AltJoins(task.scaffold_1,task.scaffold_2,task.links,task.scaffolds,thread_out,moves_);
	if (queue_.Done() and local_queue_.size()==0) {done=true; break; }

      }
      local_queue_.resize(0);
      if (done) break;
    }
    //    thread_out.close();
    finalize();
    return NULL;
  }
};



class JoinScoreWorkerThread :  public HRThread {
  HRTaskQueue<HRScaffoldJoinScoreTask>& queue_;
  int id_;
  std::string outfile_name;
  ofstream thread_out;
  Joiner joiner;
  std::vector<HRScaffoldJoinScoreTask> local_queue_;
public:
 
 JoinScoreWorkerThread(HRTaskQueue<HRScaffoldJoinScoreTask>& queue, int i, std::string ofname) : queue_(queue), id_(i), outfile_name(ofname), joiner(thread_out) {
    thread_out.open( outfile_name );
    local_queue_.resize(0);
    //    breaker = Breaker(thread_out);
  }

  void* run() {
    int j,k,i;
    bool done=false;
    for (int i=0;;i++) {
      //      HRScaffoldJoinScoreTask task= (HRScaffoldJoinScoreTask) queue_.dequeue();
      queue_.dequeue(1000,local_queue_);
      j=0;
      thread_out << "#worker thread " << id_ << " (" << id() << ") got " << local_queue_.size() << " tasks for local queue\n"; //,id_,id(),local_queue_.size());
      for (j=0;j<local_queue_.size();j++) {
	HRScaffoldJoinScoreTask task= local_queue_[j];
	if (task==0) { 
	  done=true;
	  break;
	}
	if (task.oo_reqs == NULL) {
	  double x=0.0;
	  //fprintf(stdout,"worker thread %d %lx got job: %d,%d %d-%d\n",id_,id(),task.scaffold_1, task.scaffold_2, task.mini, task.maxi);
	  //fflush(stdout);

	  joiner.JoinScore(task.mini, task.maxi,task.links,task.scaffolds,thread_out);

	  //fprintf(stdout,"worker thread %d %lx done (%g) local queue size: %d\n",id_,id(),x,int(local_queue_.size()));
	  //fflush(stdout);
	  if (queue_.Done() and local_queue_.size()==0) {done=true; break; }
	}

	else {
	  for (auto it = task.oo_reqs->begin(); it != task.oo_reqs->end(); ++it) {
	    double x=0.0;
	    //fprintf(stdout,"worker thread %d %lx got job: %d,%d, %d-%d\n",id_,id(),task.scaffold_1, task.scaffold_2, task.mini, task.maxi);
	    //fflush(stdout);
      
	    joiner.JoinScore(task.mini, task.maxi,task.links,task.scaffolds,thread_out, it->gap, task.scaffold_1, task.scaffold_2);

	    //fprintf(stdout,"worker thread %d %lx done (%g) local queue size: %d\n",id_,id(),x,int(local_queue_.size()));
	    //fflush(stdout);
	    if (queue_.Done() and local_queue_.size()==0) {done=true; break; }
	  }

	}
      }
      local_queue_.resize(0);
      if (done) break;
    }
    thread_out.close();
    return NULL;
  }
};

struct HRScaffoldSupportTask {
  int scaffold_id;
  unsigned long long mini, maxi;
  //  ChicagoLinks links;
  const ChicagoLinks& links ;  
  const PathSet& scaffolds ;  
  bool dummy_;
HRScaffoldSupportTask(const ChicagoLinks& l, const PathSet& p, int s, unsigned long long i, unsigned long long j): links(l), scaffolds(p), scaffold_id(s), mini(i), maxi(j), dummy_(false) {}
HRScaffoldSupportTask(): dummy_(true), links( ChicagoLinks() ), scaffolds( PathSet() ) {};
  bool operator==(int x) const {
    if (x==0 && dummy_){return(true);}
    else {return(false);}
  }
};

class SupportWorkerThread :  public HRThread {
  HRTaskQueue<HRScaffoldSupportTask>& queue_;
  int id_;
  std::string outfile_name;
  ofstream thread_out;
  Breaker breaker;
public:
  //  SupportWorkerThread(HRTaskQueue<HRScaffoldSupportTask>& queue, int i) : queue_(queue), id_(i) {}
 SupportWorkerThread(HRTaskQueue<HRScaffoldSupportTask>& queue, int i, std::string ofname) : queue_(queue), id_(i), outfile_name(ofname), breaker(thread_out) {
    thread_out.open( outfile_name );
    //    breaker = Breaker(thread_out);
  }

  void* run() {
    int j,k;
    for (int i=0;;i++) {
      HRScaffoldSupportTask task= (HRScaffoldSupportTask) queue_.dequeue();
      if (task==0) break;
      double x=0.0;
      fprintf(stdout,"worker thread %d %lx got job: %d %llu-%llu\n",id_,id(),task.scaffold_id, task.mini, task.maxi);
      fflush(stdout);
      
      breaker.ScaffoldSupport(task.mini, task.maxi,task.links,task.scaffolds,thread_out);

      fprintf(stdout,"worker thread %d %lx done (%g)\n",id_,id(),x);
      fflush(stdout);
      if (queue_.Done()) { break; }
    }
    thread_out.close();
    return NULL;
  }
};

template<typename ResultType> 
struct PoolResultsMergeIterator {
  std::vector< std::reference_wrapper<std::vector<ResultType> >  > vectors ;  
  std::vector<int> ptri ; 
  ResultType current_;
  bool done_;

  PoolResultsMergeIterator() : done_(false) {};
  bool done() {    return done_;  }

  PoolResultsMergeIterator& operator++() {

    int i;
    bool done=true;
    bool found_one=false;
    double best_score=0.0;
    int besti;
    for(i=0;i<vectors.size();i++) {
      const std::vector<ResultType>& v = vectors[i]; 
      if (ptri[i]>=0) {
	done=false;
	if ((!found_one) || (best_score < v[ptri[i]].score) ) {
	  best_score = v[ptri[i]].score;
	  besti = i;
	  found_one=true;
	}
      }
    }

    if  (!found_one) {
      done_=true;
      current_ = ResultType();
    } else {
      const std::vector<ResultType>& v = vectors[besti]; 
      current_ = v[ptri[besti]];
      ptri[besti]--;
    }

  };

};


template<typename ThreadType, typename TaskType, typename ResultType> 
class HRThreadPool {
  
 public:

 HRThreadPool(int n, std::string name) : n_threads_(n), output_file_(name) {};

  PoolResultsMergeIterator<ResultType> result_iterator() {
    PoolResultsMergeIterator<ResultType> it = PoolResultsMergeIterator<ResultType>() ;
    int i;

    //    it.vectors.resize(n_threads_);
    for(i=0;i<n_threads_;i++) {
      it.vectors.push_back ( threads[i]->moves_ );
      //      it.vectors[i] =  threads[i]->moves_ ;
      it.ptri.push_back( threads[i]->moves_.size()-1 );
    }

    ++it;

    return it;
  }

  void init() {
    
    int i;
    int bsize = 512;
        
    char* buffer=0;
    buffer = new char[bsize];
    for(i=0;i<n_threads_;i++) {
      sprintf(buffer,(output_file_ + "_%d.txt").c_str(),i);
      threads.push_back( new ThreadType(queue,i, std::string(buffer)) );
      threads[i]->start();
      //      fprintf(stdout,"thread started: %d\t%lx\n",i,threads[i]->id()); fflush(stdout);
    }
   
  }

  void enqueue(TaskType task) {
    queue.enqueue(task);
  }

  void close_queue() {
    queue.close();
  }

  
  void join_all() {
    int i;
    for(i=0;i<n_threads_;i++) {
      //fprintf(stdout,"thread to join: %d\n",i); fflush(stdout);
      threads[i]->join();
      //std::cout << "thread had " << threads[i]->moves_.size() << " moves.\n"; 
    }
  }

  

 private:
  int n_threads_;
  std::string output_file_;
  std::vector<ThreadType *> threads;
  HRTaskQueue<TaskType> queue;

};




#endif
