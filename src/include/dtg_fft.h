#ifndef MODEL_PARSER_H
#define MODEL_PARSER_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
//#include <Eigen/Dense>
#include <picojson.h>
#include "error_msg.h"
#include <complex>
#include <cmath>
#include <assert.h>

const double PI  =3.141592653589793238463;

//#define debug

void dtg_fft(const std::vector<std::complex<double>> & W,
	     const std::vector<double> & x, std::vector<std::complex<double>> &X, 
	     std::vector<std::complex<double>> &Z, int N, int i, int j, int s ) {
  int k;
  int ii,jj;
#ifdef debug
  fprintf(stdout,"#fft\tN=%d\ti=%d\tj=%d\ts=%d\n",N,i,j,s );
#endif

  std::complex<double> t;
  std::complex<double> p;

  assert(i<X.size());
  assert(i<x.size());

  if (N==1) {
    X[i] = std::complex<double>(x[i],0.0);
#ifdef debug
    fprintf(stdout,"#%d,(%g,%g)\n",i,std::real(X[i]),std::imag(X[i]) );
#endif
  }
  else {
    dtg_fft(W,x,X,Z,N/2,i+0 ,j       ,2*s);
    dtg_fft(W,x,X,Z,N/2,i+s ,j+N/2   ,2*s);
    for (k=0;k<N/2;k++) {
      
#ifdef debug
      fprintf(stdout,"#\t\t(fft\tN=%d\ti=%d\tj=%d\ts=%d)\n",N,i,j,s );
      fprintf(stdout,"#j+s*k+N/2<x.size() ?  %d < %lu ?  %d %d %d %d\n", j+s*k+N/2 , x.size(),j,s,k,N/2 );
#endif
      ii = i+s*k;
      jj = i+s*(k+N/2);
      assert(jj<X.size());

      t = X[i+2*s*k];  // E[j+k]
      //      p = exp( std::complex<double>(0.0,-2.0*PI* double(k)/N));
      p=W[k*s];
#ifdef debug
      fprintf(stdout,"#t                = (%g,%g)\n",std::real(t),std::imag(t) );
      fprintf(stdout,"#exp(-2 Pi i k/N) = (%g,%g)  |p|=%g\n",std::real(p),std::imag(p), std::norm(p) );
      fprintf(stdout,"#X[k+N/2]         = (%g,%g)\n", std::real(X[j+k+N/2]),std::imag(X[j+k+N/2]) );
#endif
      Z[ii ] = t + p*X[i+(2*k+1)*s];
      Z[jj ] = t - p*X[i+(2*k+1)*s];
    }
    for (k=0;k<N;k++) {
      X[i+k*s] = Z[i+k*s];
    }
  }
}

#endif
