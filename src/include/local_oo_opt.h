#include <algorithm>
#include <stdio.h>
#include <iostream>
#include <unordered_map>


class Cell {
  int position;
  int permutation;
  int flips;
  int window_size;
  std::vector<int> contig_ids;

 public: 
  Cell(const std::vector<int>& contig_ids_,  int position_, int permutation_, int flips_, int window_size_) {
    position    = position_;
    permutation = permutation_;
    flips       = flips_;
    window_size = window_size_;
    contig_ids  = contig_ids_;
  }

  // position in original contig layout
  int start_contig_position() {
    return position;
  }

  
  int end_contig_position() {
    return position + window_size;
  }

  int get_contig_id(int num) {
    return contig_ids[num];
  }
};


class LOOMatrix {

  int end_n;
  int start_n;
  int window_size;
  std::unordered_map<int, std::vector<int>> permutation_lookup;

  
 public:
  LOOMatrix(int start, int end, int window_size_) {
    start_n            = start;
    end_n              = end;
    window_size        = window_size_;
    std::unordered_map<int, std::vector<int>> permutation_lookup_;
    fill_permutation_lookup(window_size_, permutation_lookup_);
    permutation_lookup = permutation_lookup_;
  };

  void fill_permutation_lookup(int window_size, std::unordered_map<int, std::vector<int>>& permutation_lookup) {
    // fill first vector with ascending numbers
    std::vector<int> permutation(window_size);
    std::iota (std::begin(permutation), std::end(permutation), 0);
    int i = 0;
    permutation_lookup[i] = permutation;

    // fill the lookup with every permutation
    while (next_permutation(permutation.begin(), permutation.end())) {
      i += 1;
      permutation_lookup[i] = permutation;
    }
  }

  void fill_matrix() {

    for(auto kv : permutation_lookup) {
      std::cout << "key: " << kv.first << " ";
      for(auto k2 : kv.second) {
	std::cout << "value: " << k2 << " ";
      }
      std::cout << "\n";
    }

    std::cout << "filling matrix with start " << start_n << " and end " << end_n << "\n" ;
    
    int permutation = 3;
    int n = 4;
    for(auto local_pos : permutation_lookup[permutation]) {
      std::cout << "local pos: " << local_pos << " actual pos: " << local_pos + n << "\n";
    }
    
    std::vector<int> contig_ids {0, 7, 8, 9, 11};
    Cell mycell = Cell(contig_ids, 0, 0, 0, 3);
    std::cout << "starting " << mycell.start_contig_position() << " end pos: " << mycell.end_contig_position() << "\n";
    for(int i=mycell.start_contig_position(); i < mycell.end_contig_position(); i++) {
      
      std::cout << "cell i contig pos: " << i << " " ;
    }
    std::cout << "\n";
  }
};



