#ifndef SPARSE_KERNEL_SCORES_DEFS_H
#define SPARSE_KERNEL_SCORES_DEFS_H

#define PROB_EPSILON 1.0e-16

#define WW 1000
#define W2 WW*2
#define HH 1000
#define NKERNELS 1000

#define HH 1000

// #define BINSIZE 300

#define KERNEL_INDEX(X,Y,K) K*HH*W2 + Y*W2 + X+WW
#define AREAVS_INDEX(X,K  ) K*W2           + X+WW
// #define KERNEL_INDEX(X,Y,K) K*HH*W2 + (X+WW)*HH + Y
// #define KERNEL_INDEX(X,Y,K) (((X)+WW)*(NKERNELS*HH) + (Y)*NKERNELS + (K))
// #define KERNEL_INDEX(X,Y,K) (((X))*(NKERNELS*HH) + (Y)*NKERNELS + (K))
// #define SCORE_INDEX(X,K) K*MAXBIN + X
// #define SCORE_INDEX(I,J) ((I)*NKERNELS+(J))

//#define MAXBIN 10000000
//#define NREADS 20000000

// 1% full-size
#define MAXBIN 100000
#define NREADS 200000

//0.1X full-size
//#define MAXBIN 1000000
//#define NREADS 2000000

//full-size
//#define MAXBIN 10000000 // 10 million bins along the genome (i.e. human at 300 bp interval)
//#define NREADS 20000000 // 20 million read pairs

#endif
