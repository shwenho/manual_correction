#ifndef CHICAGO_SI_LM_H
#define CHICAGO_SI_LM_H
#define PICOJSON_USE_INT64

#include <stdint.h>
#include <assert.h>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/NonLinearOptimization>
#include <picojson.h>
#include <pthread.h>
#include "error_msg.h"
#include "chicago_read.h"
#include "pair_distance_likelihood_model.h"
#include "histogram_provider.h"
#include "serialization_utils.h"

#define root_two_pi          2.506628274631000502415765284811045253006986740609938316629
#define pi                   3.14159265358979323846
#define one_over_sqrt_two_pi 0.398942280401432677939946059934381868475858631164934657665

/*
f(x) = (N p_n)/G + (1-p_n)\left( \gamma \sum_i \alpha_i \beta_i e^{-\beta_i x} + (1-\gamma) frac{1}{\sigma \sqrt{2 \pi}} e^{-(x-\mu)^2/2\sigma^2}  \right)
 */

#define DEBUG_LVL 0

class ChicagoSILM : public PairDistanceLM  {
  friend class SVKernelScorer;
private:
  // Matrices and vectors of data
  Eigen::ArrayXd  alpha_;       ///< alpha coefficients for exponential model;
  Eigen::ArrayXd  beta_;        ///< beta coefficients for exponential model;
  Eigen::ArrayXd  neg_beta_;    ///< cached negative coefficients for exponential model;
  Eigen::ArrayXd  neg_alpha_;    ///< cached negative coefficients for exponential model;
  Eigen::ArrayXd  neg_alpha_div_beta_;    ///< cached negative coefficients for exponential model;
  Eigen::ArrayXd  alpha_beta_;  ///< cached alpha*beta coefficients for exponential model;
  Eigen::ArrayXd  a_scratch_;   ///< vector for intermediate results
  Eigen::MatrixXd m_scratch_;   ///< Matrix for storing intermediate results;
  double gamma_;
  double one_minus_gamma_;
  double mu_;
  double sigma_;
  double one_over_sigma_root_2pi_;
  double one_over_2sigma2_;

  double  norm_factor_ ;
  double  sigma_over_root_2pi_;
  double  two_sigma_squared_;
  double  sigma_root2_;


public:

  ChicagoSILM() { Init(); }
  ChicagoSILM(const char *model_json, int64_t n_shotgun = 0) {
    Init();
    SetModel(model_json);
    if (n_shotgun) {
      SetMetagenomic(n_shotgun);
    }
  }

  //
  // Model specific stuff  (override in implementations)
  //


  void fit(const HistogramProvider& data);

  void SetModel(const char *model_json, const bool fill_cache = true) {
      picojson::value v;
      std::string err = picojson::parse(v, model_json);
      std::vector<double> beta_vec =  v.get_vector<double>("beta");
      std::vector<double> alpha_vec = v.get_vector<double>("alpha");
      num_links_   = v.get("N").get<double>();;
      genome_size_ = v.get("G").get<double>();
      prob_noise_  = v.get("pn").get<double>();
      gamma_       = v.get("gamma").get<double>();
      mu_          = v.get("mu").get<double>();
      sigma_       = v.get("sigma").get<double>();
      one_over_sigma_root_2pi_ = 1.0/(root_two_pi*sigma_);
      one_over_2sigma2_ = 1.0/(2.0*sigma_*sigma_);
      one_minus_gamma_ = 1.0-gamma_;
      DT_ASSERT(beta_vec.size() == alpha_vec.size(), "alpha and beta must match in size.");
      int size = beta_vec.size();
      alpha_.resize(size);
      beta_.resize(size);
      a_scratch_.resize(size);
      std::copy(beta_vec.begin(), beta_vec.end(), &(beta_(0)));
      std::copy(alpha_vec.begin(), alpha_vec.end(), &(alpha_(0)));
      neg_beta_ = -1.0f * beta_;
      neg_alpha_ = -1.0f * alpha_;
      alpha_beta_ = alpha_ * beta_; // element wise, not dot product
      neg_alpha_div_beta_ = -1.0 * alpha_ / beta_;
      prob_noise_div_genome_size_ = prob_noise_ / genome_size_;
      one_minus_prob_noise_ = 1.0d - prob_noise_;
      mean_num_links_null_coeff_ = num_links_ * prob_noise_ / (genome_size_ * genome_size_);
      one_minus_prob_noise_N_over_G_ = one_minus_prob_noise_ * num_links_ / genome_size_ ;
      //one_minus_prob_noise_N_over_G_ = one_minus_prob_noise_ * num_links_ / genome_size_ ;
      is_metagenomic_=false;
      logG_ = log(genome_size_);
      logN_ = log(num_links_);
      lnFinf_ = log(prob_noise_) - logG_;
    //        self.lnFinf = math.log(self.pn)-math.log(self.G)
      update_precalculated_values();
      if (fill_cache) {
	fill_caches();
      }
  }

  int sample_distribution() {

    double x;
    double short_innie_prob = 0.2;

    if (false) {
    x = drand48();
    if (x<prob_noise_) {
      return(-1);
    }

    x = drand48();
    if (x<short_innie_prob) {
      return(-2);
    }
    }

    int i=0;
    double rs=alpha_[i];
    x = drand48();
    while (x>rs) {
      i++;
      rs+=alpha_[i];
    }

    x = - log(1.0-drand48())  / beta_[i];

    return(int(x));
  }

  //  inline int32_t NumExponential() { return beta_.rows(); }

  /* void ModelFunctionInserts(const Eigen::VectorXd &inserts, double *likelihood) { */
  /*   // outer product to create matrix with row for each insert */
  /*   // and column for each coefficient */
  /*   m_scratch_ = inserts.transpose() * neg_beta_; */
  /*   m_scratch_.exp(); */
  /*   m_scratch_ = m_scratch_ * alpha_beta_; */
  /*   double result = m_scratch_.sum(); */
  /*   *likelihood = result; */
  /* } */

  // Currently slower with small values than just regular math version below
  void NonNoiseInsertDist(int insert_size, double *likelihood) {
    Eigen::ArrayXd a_scratch ;
    //a_scratch.resize();
    //    pthread_mutex_lock(   &mutex_ );
    a_scratch = insert_size * neg_beta_;
    a_scratch = a_scratch .exp();
    a_scratch *= alpha_beta_;
    *likelihood = a_scratch.sum();

    *likelihood *= gamma_;
    *likelihood += one_minus_gamma_*one_over_sigma_root_2pi_*exp(-one_over_2sigma2_*(insert_size - mu_)*(insert_size-mu_));

    //pthread_mutex_unlock(   &mutex_ );
  }

  /* // Current fastest way of calculating mixture of exponential probability currently */
  /* // P_{raw}(x)= \sum \alpha_i \beta_i e^{-x \beta_i}  */
  /* // f0 in python */
  /* void ExpLikelihood(int insert_size, double *likelihood) { */
  /*   double *alpha_beta_start = alpha_beta_start_; */
  /*   double *neg_beta_start = neg_beta_start_; */
  /*   double *neg_beta_end = neg_beta_end_; */
  /*   *likelihood = 0.0d; */
  /*   while(neg_beta_start != neg_beta_end) { */
  /*     *likelihood += *alpha_beta_start++ * exp(*neg_beta_start++ * insert_size); */
  /*   } */
  /* } */


  inline void F_good(int insert_size, double *result) {

    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;

    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= neg_alpha_;
    *result = - a_scratch.sum();
    *result *= gamma_;

    *result += (1.0-gamma_)*(1.0 - 0.5*std::erf((insert_size - mu_)/sigma_root2_ ));
    //             *result = 1.0 - 0.5*std::erf((insert_size - insert_mean_)/sigma_root2_);
  
    //fprintf(stdout,"F_good\t%g\n",a_scratch.sum());
    //    *result *= one_minus_prob_noise_;
    //*result += prob_noise_div_genome_size_ * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  inline void H_good(int insert_size, double *result) {

    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;
    // 0.5*d*d*self.pn/self.G +
    // (1.0-self.pn) * sum([ -(old_div(alpha[i],beta[i]))*( math.exp(-beta[i]*d) * (beta[i] * d + 1.0)) for i in range(self.nexps) ])
    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= (beta_ * insert_size) + 1;
    a_scratch *= neg_alpha_div_beta_;
    *result = - a_scratch.sum();
    *result *= gamma_;

    double tmp = 0.5*mu_;
    tmp -=  0.5*mu_*std::erf((insert_size - mu_)/sigma_root2_);
    tmp -= -sigma_over_root_2pi_ *exp(-one_over_2sigma2_*(insert_size - mu_)*(insert_size-mu_));// exp( - pow( insert_size - insert_mean_ , 2.0) / two_sigma_squared_ );
    *result += (1.0-gamma_)*tmp;

  }

  inline void F0(int insert_size, double *result) {
    throw std::runtime_error("F0 not implemented yet for chicago-with_short-innies-LM.");
    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;

    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= neg_alpha_;
    *result = a_scratch.sum();
    *result *= one_minus_prob_noise_;
    *result += prob_noise_div_genome_size_ * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  inline void H0(int insert_size, double *result) {
    throw std::runtime_error("H0 not implemented yet for chicago-with_short-innies-LM.");
    //pthread_mutex_lock(   &mutex_ );
    Eigen::ArrayXd a_scratch ;
    // 0.5*d*d*self.pn/self.G +
    // (1.0-self.pn) * sum([ -(old_div(alpha[i],beta[i]))*( math.exp(-beta[i]*d) * (beta[i] * d + 1.0)) for i in range(self.nexps) ])
    a_scratch = neg_beta_ * insert_size;
    a_scratch = a_scratch.exp();
    a_scratch *= (beta_ * insert_size) + 1;
    a_scratch *= neg_alpha_div_beta_;
    *result = a_scratch.sum();
    *result *= one_minus_prob_noise_;
    *result += prob_noise_div_genome_size_ * 0.5 * insert_size * insert_size;
    //pthread_mutex_unlock(   &mutex_ );
  }

  /// Initialize with default values
  void Init() {

    gamma_       = 0.0;
    mu_          = 0.0;
    sigma_       = 0.0;
    one_over_sigma_root_2pi_ = 0.0; //1.0/(root_two_pi*sigma_);
    one_over_2sigma2_ = 0.0;        //1.0/(2.0*sigma_*sigma_);
    one_minus_gamma_ = 0.0;

    num_links_ = 0;
    genome_size_ = 0.0d;
    prob_noise_ = 0.0d;
    prob_noise_div_genome_size_ = 0.0d;
    one_minus_prob_noise_ = 0.0d;
    mean_num_links_null_coeff_ = 0.0d;
    beta_.resize(0);
    alpha_.resize(0);
    alpha_beta_.resize(0);
    neg_beta_.resize(0);
    cache_max_ = 400000;
    cache_min_ =-400000;

    //    std::cerr << "set cache min and max\n";

    Tcache_.resize(cache_max_-cache_min_);
    Tcache_fill_.resize(cache_max_-cache_min_);
    std::fill(Tcache_fill_.begin(),Tcache_fill_.end(),0);

    TPluscache_.resize(cache_max_-cache_min_);
    TPluscache_fill_.resize(cache_max_-cache_min_);
    std::fill(TPluscache_fill_.begin(),TPluscache_fill_.end(),0);

    Fcache_.resize(cache_max_-cache_min_);
    Fcache_fill_.resize(cache_max_-cache_min_);
    std::fill(Fcache_fill_.begin(),Fcache_fill_.end(),0);

    pthread_mutex_init( &mutex_, NULL);
    pthread_mutex_init( &mutex_T_, NULL);
    pthread_mutex_init( &mutex_Tplus_, NULL);
    pthread_mutex_init( &mutex_F_, NULL);
    pthread_mutex_init( &mutex_A_, NULL);
    pthread_mutex_init( &mutex_P_, NULL);

    update_precalculated_values();

  }

  void DescriptionString(char* sbuff) {
    sprintf(sbuff,"Chicago Model, N=%d; pn=%g",num_links_,prob_noise_);
  };

  void write_gnuplot_string(std::ostream& fh) const {
    int n = beta_.size();
    fh << num_links_ << "*( (" << prob_noise_ << "/" << genome_size_ << ") + (1-"<< prob_noise_ <<")*(" << gamma_ << "*("  ;
    int i=0;

    fh << " (" <<     beta_(i)*alpha_(i) << "*exp(-x*"<<beta_(i)<<") "   << ") ";
    for(i=1;i<n-3;i++) {
      fh << "+(" <<         beta_(i)*alpha_(i) << "*exp(-x*"<<beta_(i)<<") "     << ") ";
    }
    fh << ") + ("<<(1.0-gamma_)<<"*"<<one_over_sigma_root_2pi_<<"*exp(-(x-"<<mu_<<")*(x-"<<mu_<<")*"<< one_over_2sigma2_ <<"))))";
  }

  void write_gnuplot_string(std::ostream& fh,double A, double B, double C) const {
    int n = beta_.size();
    fh << "( " << A << " + " << B << "*("  ;
    int i=0;

    fh << " (" <<     beta_(i)*alpha_(i) << "*exp(-x*"<<beta_(i)<<") "   << ") ";
    for(i=1;i<n-3;i++) {
      fh << "+(" <<         beta_(i)*alpha_(i) << "*exp(-x*"<<beta_(i)<<") "     << ") ";
    }
    fh << ") + "<<C<<"*(exp(-(x-"<<mu_<<")*(x-"<<mu_<<")*"<< one_over_2sigma2_ <<")))";
  }

 void print_chicago_json(std::ostream& fh) const {

   double pn = prob_noise_;
   double N  = num_links_;
   double Nn = N*pn;
   double G = genome_size_;

   fh << "{";
   fh << "\"N\": " << N;
   fh << ", " << "\"pn\": " << pn;
   fh << ", " << "\"Nn\": " << Nn;
   fh << ", " << "\"G\": " << G;
   fh << ", " << "\"gamma\": " << gamma_;
   fh << ", " << "\"mu\": " << mu_;
   fh << ", " << "\"sigma\": " << sigma_;
   fh << ", " << "\"model_type\": \"chicagoSI\" ";
   fh << ", \"beta\": " ; vec2json(fh,beta_);

   fh << ", " << "\"alpha\": "; vec2json(fh,alpha_);

   fh << ", " << "\"fn\": \"" ; write_gnuplot_string(fh) ; fh << '\"';
   fh << "}\n";
 }

 void set_genome_size(double G) {genome_size_=G;};
 // void set_beta(Eigen::VectorXd beta) {   beta_ = beta; }
 // void set_alpha(Eigen::VectorXd beta) {  alpha_ = alpha; }

private:

 void update_precalculated_values() {

   int size = beta_.size();
   //      alpha_.resize(size);
   //   beta_.resize(size);
   a_scratch_.resize(size);
   //   m_scratch_.resize(size);
   neg_beta_ = -1.0f * beta_;
   neg_alpha_ = -1.0f * alpha_;
   alpha_beta_ = alpha_ * beta_; // element wise, not dot product
   neg_alpha_div_beta_ = -1.0 * alpha_ / beta_;
   prob_noise_div_genome_size_ = prob_noise_ / genome_size_;
   one_minus_prob_noise_ = 1.0d - prob_noise_;
   mean_num_links_null_coeff_ = num_links_ * prob_noise_ / (genome_size_ * genome_size_);
   one_minus_prob_noise_N_over_G_ = one_minus_prob_noise_ * num_links_ / genome_size_ ;

   //one_minus_prob_noise_N_over_G_ = one_minus_prob_noise_ * num_links_ / genome_size_ ;
   is_metagenomic_=false;
   logG_ = log(genome_size_);
   logN_ = log(num_links_);
   lnFinf_ = log(prob_noise_) - logG_;

    one_over_sigma_root_2pi_ = 1.0/(root_two_pi*sigma_);
    one_over_2sigma2_       =  1.0/(2.0*sigma_*sigma_);
    one_minus_gamma_ = 1.0-gamma_;

    norm_factor_ = one_over_sqrt_two_pi / sigma_ ;
    sigma_over_root_2pi_ = sigma_ / root_two_pi ;
    two_sigma_squared_ = 2.0 * sigma_ * sigma_ ;
    sigma_root2_ = sqrt(2.0)*sigma_ ;

 }

};

std::ostream& operator<<(std::ostream& out,const ChicagoSILM& model) {

  model.print_chicago_json(out);
  //  print_chicago_json(out,model);
  return out;

}

#define NEXTRA_PARAMS 4

struct ChicagoSIFunctor {
  typedef double Scalar;

  enum {
    InputsAtCompileTime = Eigen::Dynamic,
    ValuesAtCompileTime = Eigen::Dynamic
  };

  typedef Eigen::Matrix<Scalar,InputsAtCompileTime,1> InputType;
  typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,1> ValueType;
  typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,InputsAtCompileTime> JacobianType;

  int m_inputs, m_values;

  ChicagoSIFunctor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
  ChicagoSIFunctor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

  int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const {

    int n = x.size();
    double chisq=0.0;
    Eigen::VectorXd alpha(n-NEXTRA_PARAMS);

    for(int j =0;j<n-NEXTRA_PARAMS;j++) {
      alpha(j) = (tanh(x(j))+1.0)/2.0;
    }

    //    std::cout << "size: " << n << "\n";
    //    double pn = (tanh(x(n-2))+1.0)/2.0;
    //double pn = x(n-2);
    //double N  = x(n-1);
    //    double lambda  = x(n-1);
    double B     = x(n-1);
    double sigma = x(n-2);
    double C     = x(n-3);
    double mu    = x(n-4);
    double one_over_two_sigma_sq = 1.0/(2.0*sigma*sigma);
    for(int i = 1; i < this->Points.size(); ++i) {
      double d = this->Points[i].x;
      //      if (d<1000) {fvec(i)=0.0; continue;}
      double f = 0.0;

      for(int j =0;j<n-NEXTRA_PARAMS;j++) {
	f += alpha(j)*this->beta(j) * exp(-this->beta(j)*d)  ;
      }
      //      f *= N*(1.0-pn)   ;
      f *= B ;

      f += C*exp(-(d-mu)*(d-mu)*one_over_two_sigma_sq);

      f+= this->pn_N_over_G ;
      //      f*=N;
      fvec(i) = (this->Points[i].y - f ) / this->Points[i].dy;
      chisq+= fvec(i)*fvec(i);
    }

    double sum_alpha=0.0;
    for(int j =0;j<n-NEXTRA_PARAMS;j++) {
      sum_alpha += alpha(j);
    }
    sum_alpha -= 1.0;

    sum_alpha *= 100.0;
    //    std::cout << "sum alpha: " << sum_alpha << "\n";
    fvec(0) = (sum_alpha);
    //    fvec(1) = 100.0 *( (pn_N_over_G * G / N) - pn )  ;

    //chisq += sum_alpha * sum_alpha;
    //chisq += fvec(1)*fvec(1);
    //    std::cout << "dp\t" << B << "\t" << chisq << "\n";// << j << "\t" << alpha(i) << "\t" << this->beta(j) << "\n";
#if DEBUG_LVL > 0
    std::cout << "dp:\t" << chisq<< "\t"<< B << "\t" << C << "\t" << sigma <<"\t" << mu << "\n";// << j << "\t" << alpha(i) << "\t" << this->beta(j) << "\n";
#endif
    return 0;
  }

  PointVector Points;
  Eigen::VectorXd beta;
  //  Eigen::VectorXd alpha;
  double G;
  double pn_N_over_G;
  //  double N_tmp;

  int inputs() const { return 2; }
  int values() const { return this->Points.size(); } // The number of observations

};


struct FunctorNumericalDiffSI : Eigen::NumericalDiff<ChicagoSIFunctor> {};


void ChicagoSILM::fit( const HistogramProvider& data ) {
  genome_size_ = data.genome_size();
  double G = genome_size_;
  double pn=0.5;
  //std::cout << "hi\n";
  double N=0.0;
  for( PointVector_const_it it=data.histogram.begin();it!=data.histogram.end();it++) {
#if DEBUG_LVL > 0
    std::cout << "data:\t" << it->x << "\t" << it->y <<"\t" << it->dy  <<"\n";
#endif
    //if (it->x>1000) {N += it->y;}
    N += it->y;
  }

  int n=10;
  Eigen::VectorXd beta(n) ;
  Eigen::VectorXd alpha(n) ;
  Eigen::VectorXd x(n+NEXTRA_PARAMS);
  //  beta << 0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000e-05 ;

  const std::vector<double> betaV = {0.002, 0.0010026386447354524, 0.0005026421259584724, 0.0002519842099789746, 0.00012632455339402638, 6.332893950589896e-05, 3.174802103936399e-05, 1.591589639397027e-05, 7.978946395100112e-06, 4e-06};// 0.002, 0.001110094615569623, 0.0006161550277583347, 0.00034199518933533945, 0.00018982350911593703, 0.00010536102768906649, 5.8480354764257334e-05, 3.245936347020171e-05, 1.8016482306544117e-05, 1.0000000000000003e-05};
  //  const std::vector<double> alphaV = {0.2596398458617465, 0.2184877929541886, 0.19276735652678798, 0.06612921087467549, 0.13835425638673426, 0.09578541569209319, 0.028836121703774004, 0.0, 0.0, 0.0};

  for(int i=0;i<n;i++) {
    //    alpha(i) = alphaV[i];
      alpha(i) = 1.0;
    beta(i) = betaV[i];
  }
  alpha = (1.0/alpha.sum())*alpha ;

  FunctorNumericalDiffSI functor;
  functor.Points = data.histogram;
  functor.beta = beta;
  //  functor.beta = alpha;
  functor.G = G;
  functor.pn_N_over_G = data.histogram.back().y;
  //  std::cout << "starting values: " << x << std::endl;

  while ( functor.pn_N_over_G * G / N > 1.0 ) { N *= 1.5; }
  //  std::cout << "Noise level: " << functor.pn_N_over_G  << "\t" << data.histogram.back().x <<"\t" <<data.histogram.back().y << std::endl;

  //  functor.N = N ;

  for(int i=0;i<n;i++) {
    x(i) = atanh( (alpha(i)+1.0e-9)*2.0 - 1.0) ;
  }

  /*
  double mu    = x(n-4);
  double C     = x(n-3);
  double sigma = x(n-2);
  double B     = x(n-1);
  */
 
  //Initial guesses for short innie params
  double gamma = 0.5  ;  // fraction of reads that are short innies
  double mu    = 250.0;  // mean length of short innies
  double sigma = 50.0;  // standard deviation of short innie length

  x(n  ) = mu;                    // mu
  x(n+1) = N*(1.0-pn)*(1.0-gamma) / (sqrt(2.0*pi)*sigma);  // C
  x(n+2) = sigma                  ;  // sigma
  x(n+3) = N*(1.0-pn)*gamma; //atanh( (pn )*2.0 - 1.0);  // B

  Eigen::LevenbergMarquardt<FunctorNumericalDiffSI> lm(functor);
  Eigen::LevenbergMarquardtSpace::Status status = lm.minimize(x);

  for(int i=0;i<n;i++) {    alpha(i) = (tanh(x(i))+1.0)/2.0;  }

  mu = x(n  ) ;
  double C = x(n+1)  ; //= (1.0-gamma) / (sqrt(2.0*pi)*sigma);  // C
  sigma = x(n+2) ;//= sigma                  ;  // sigma
  double B = x(n+3); // = atanh( (pn )*2.0 - 1.0);  // B
  double A = functor.pn_N_over_G;

#if DEBUG_LVL > 0
  std::cout << "fit results:\n" ;
  std::cout << "   mu=\t" << mu    << "\n" ;
  std::cout << "sigma=\t" << sigma << "\n" ;
  std::cout << "    A=\t" << A << "\n" ;
  std::cout << "    B=\t" << B << "\n" ;
  std::cout << "    C=\t" << C << "\n" ;  
#endif

  beta_ = beta;
  alpha_= alpha;

  double Cp = C*sigma*sqrt(2.0*pi);
  double Ap = A*G;
  double zz = B/Cp;

  gamma_ = zz/(1.0+zz);
  N = B/gamma_ +Ap;

  //  double ww = A*G*gamma/B;
  pn = Ap/N; //;ww/(1.0+ww);
  prob_noise_ = pn;

  num_links_ = N;
  genome_size_ = G;

  sigma_=sigma;
  mu_=mu;
  update_precalculated_values();

#if DEBUG_LVL > 0
  write_gnuplot_string(std::cout,A,B,C);
  std::cout << "\n";
  print_chicago_json(std::cout);
  /*
  pn = G*z / (1.0 + G*z);
  N =  B / (1.0-pn) ;


  */
  std::cout.flush();
#endif  
}

#endif // CHICAGO_LM_H
