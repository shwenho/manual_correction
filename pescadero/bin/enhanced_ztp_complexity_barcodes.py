#!/usr/bin/env python3


import argh
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

def main(histogram, output="/dev/stdout"):
    counts = parse_hist(histogram)
    complexity = calc_ztp(counts)
    with open(output, 'w') as out:
        for c in complexity:
            print(int(c[0]), file=out)

def parse_hist(histogram):
    unique_molecules = 0
    total_reads = 0
    with open(histogram) as f:
        for line in f:
            s = line.split()
            unique_molecules = int(s[1])
            total_reads = int(s[2])
            yield unique_molecules, total_reads



def calc_ztp(counts):
    for C, N in counts:
        first_guess = 10000
        # Finding the roots of the function
        func = lambda x: 1- math.exp(-N/x) - C/x
        library_complexity = fsolve(func, first_guess)
        yield library_complexity

            
if __name__ == "__main__":
    argh.dispatch_command(main)
