#!/usr/bin/env python3

"""
Author: Jonathan Stites
Company: Dovetail Genomics
Contact: jon@dovetail-genomics.com

Takes as input forward and reverse fastq files, an IDT-formatted file 
of barcodes, and the "order" of the round of barcode addition. Outputs 
reads with the "compound barcodes" removed and a file listing which 
compound barcode was found. 

Usage:


Note: Only uses [ATGCatgc] for the barcodes. Ignores e.g. "/5Phos"
Requires dovetools installation. Requires specific format for barcodes
of row_num  [0-9][fr][0-9] barcode
"""

import argh
import json
from dovetools.utils import FastqHelper
import sys

def main(r1, r2, barcodes=None, order=None, edit=None, barcode_positions=None, out_r1="/dev/stdout", tags="tags.txt", out_r2="/dev/stdout", basic_counts_out="read_counts.txt",barcode_only=True, debug=False):
    
    parsed_barcodes = barcodes_from_file(barcodes, order, barcode_positions)

    for key, value in parsed_barcodes.items():
        print("barcode:", key, value, file=sys.stderr)
    
    #basic_counts: [total_reads,failed_reads,outputted_reads]
    #outputted reads is total_reads-failed_reads-reads_with_length0
    basic_counts = [0,0,0]
    with open(out_r1, 'w') as out_handle, open(tags, 'w') as tag_handle, open(out_r2, 'w') as out_handle2:
        counts = read_fastq(r1, r2, parsed_barcodes, order, out_handle, out_handle2, tag_handle, barcode_only,basic_counts)
    with open(basic_counts_out,'w') as output: print("\t".join(map(str,basic_counts)),file=output)
    json.dump(counts, sys.stdout, indent=4)
    if debug:
        for k, v in counts.items():
            for k2, v2 in v.items():
                for k3, v3 in v2.items():
                    print(k, k2, k3, v3, file=sys.stderr)

def read_fastq(r1, r2, parsed_barcodes, order, out_r1, out_r2, tags, barcode_only,basic_counts):
    counts = {}
    failed_r1 = open("failed.r1.fq", 'w')
    failed_r2 = open("failed.r2.fq", 'w')

    for read1, read2 in get_reads(r1, r2):
        basic_counts[0] += 1
        bars, labels, read1.sequence = update_barcode_counts(read1.sequence, parsed_barcodes, order, counts)
        read1.quality = read1.quality[-len(read1.sequence):]
        read1.as_string = read1.get_string()

        bars2, labels2, read2.sequence = update_barcode_counts(read2.sequence, parsed_barcodes, order, counts)
        read2.quality = read2.quality[-len(read2.sequence):]
        read2.as_string = read2.get_string()
        
        if barcode_only and "None" in bars and "None" in bars2:
            basic_counts[1] += 1
            print(read1.as_string, file=failed_r1)
            print(read2.as_string, file=failed_r2)
            continue

        if "None" in bars and "None" not in bars2:
            labels = labels2

        if len(read1.sequence) > 0 and len(read2.sequence) > 0:
            basic_counts[2] += 1
            print(read1.as_string, file=out_r1)
            print(read1.name.strip("@"), ','.join(labels), file=tags, sep="\t")
            print(read2.as_string, file=out_r2)
    return counts

def update_barcode_counts(read, parsed_barcodes, order, counts):
    bars = ["None" for i in parsed_barcodes.keys()]
    labels = ["None" for i in parsed_barcodes.keys()]

    for i, barcode_round in enumerate(order.split(",")[::-1]):
        for label, barcode in parsed_barcodes[barcode_round].items():
            if barcode in read or rc(barcode) in read:
                bars[i] = barcode
                labels[i] = label
                point = read.find(barcode)
                read = read[point + len(barcode):]


    counts.setdefault(bars[0], {})
    counts[bars[0]].setdefault(bars[1], {})
    counts[bars[0]][bars[1]].setdefault(bars[2], 0)
    counts[bars[0]][bars[1]][bars[2]] += 1
    return bars, labels, read

def get_reads(r1, r2):
    for read1, read2 in FastqHelper.read_paired_lines(r1, r2):
        yield read1, read2

def barcodes_from_file(barcodes, order, barcode_positions):
    parsed_barcodes = {}
    order_to_pos = order_to_positions(order, barcode_positions)
    with open(barcodes) as f:
        for line in f:
            s = line.split("\t")
            if len(s) < 3:
                continue

            label = s[1]
            key = label[:2]
            barcode = s[2]
            stripped_barcode = strip_barcode(barcode)
            print(stripped_barcode)
            try:
                start, end = order_to_pos[key]
            except KeyError:
                continue
            parsed_barcodes.setdefault(key, {})
            parsed_barcodes[key][label] = stripped_barcode[start:end]

    return parsed_barcodes

def order_to_positions(order, positions):
    order_to_pos = {}
    for key, pos in zip(order.split(","), positions.split(",")):
        start,end = map(int, pos.split("-"))
        order_to_pos[key] = [start, end]
    return order_to_pos

def strip_barcode(barcode):
    bases = set("ATGCatgc")
    return ''.join([i for i in barcode if i in bases])

def rc(seq):
    bases = dict(zip("ATGCNatgcn", "TACGNtacgn"))
    return ''.join([bases[i] for i in seq])

def get_compound_barcode(read, barcodes, order):
    pass

def write_outputs():
    pass

if __name__ == "__main__":
    argh.dispatch_command(main)
