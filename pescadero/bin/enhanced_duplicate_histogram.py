#!/usr/bin/env python3


import argh
import pysam
from collections import Counter
import sys

def main(bam, output="/dev/stdout", mapq=20, single=False, debug=False, barcodes=False, summary=False,
         one_number=False, hist=None):
    histograms, all_barcodes = get_duplicate_counts(bam, mapq, single, debug, barcodes)

    all_total = 0
    all_unique = 0
    total_histogram = Counter()
    if not barcodes:
        oh = open(output,'w')
        histogram = histograms[0]
        maximum = max(histogram.keys())
        maximum2 = max(maximum, 4)
        unique = 0
        total = 0
        for i in range(1, maximum2+1):
            total += i * histogram[i]
            unique += histogram[i]
            if i <= 4 and histogram[i] <1:
                histogram[i] = 1
        print("all", unique, total,file=oh,sep='\t')
        oh.close()
        if hist:
            out = open(hist,'w')
            for i in range(1,maximum2+1):
                print(i,histogram[i],file=out,sep='\t')
            out.close()
        sys.exit(0)

    with open(output, 'w') as output_handle:
        for histogram, barcode in zip(histograms, all_barcodes):
            maximum = max(histogram.keys())
            maximum2 = max(maximum, 4)
            unique = 0
            total = 0
            for i in range(1, maximum2+1):
                total_histogram[i] += histogram[i]
                if summary:
                    total += i * histogram[i]
                    unique += histogram[i]
                    
                if i <=4 and histogram[i] < 1:
                    histogram[i] = 1
                    
                if not summary:
                    print(barcode, i, histogram[i], file=output_handle, sep="\t")
            if summary and not one_number:
                print(barcode, unique, total, file=output_handle, sep="\t")
            all_total += total
            all_unique += unique

        if summary and one_number:
            print("all", all_unique, all_total, file=output_handle, sep="\t")

        if hist and one_number:
            maximum = max(total_histogram.keys())
            maximum2 = max(maximum, 4)
            out = open(hist, 'w')
            for i in range(1, maximum2+1):
                if i <=4 and total_histogram[i] < 1:
                    total_histogram[i] = 1
                print(i, total_histogram[i], file=out, sep="\t")
                    

def get_duplicate_counts(bam, mapq, single, debug, barcodes):
    counts = Counter()
    for identifier in get_identifiers(bam, mapq, single, barcodes):
        counts[identifier] += 1

    if debug:
        for key, value in counts.items():
            print(key, value, file=sys.stderr)
    histograms = []
    if barcodes:
        all_barcodes = set(x[-1] for x in counts.keys())

        for x in all_barcodes:
            this_counts = Counter([value for key, value in counts.items() if key[-1] == x])
            histograms.append(this_counts)
    else:
        all_barcodes = []
        histograms.append(Counter([value for key,value in counts.items()]))
    return histograms, all_barcodes


def get_identifiers(bam, mapq, single, barcodes):

    with pysam.AlignmentFile(bam, 'rb') as bam_handle:
        prev_id = None
        reads_with_same_id = []
        for aln in bam_handle:
            if aln.is_read2:
                continue
            if aln.is_unmapped or aln.mate_is_unmapped:
                continue
            if aln.mapq < mapq or aln.opt("MQ") < mapq:
                continue
            if single:
                ident = get_identifier_se(aln, barcodes)
            
            else:
                ident = get_identifier_pe(aln, barcodes)
            yield ident


def get_identifier_se(aln, barcodes):
    pos1 = aln.reference_start
    ref1 = aln.reference_id
    if barcodes:
        identifier = (ref1, pos1, aln.opt("xc"))
    else:
        identifier = (ref1, pos1)
    return identifier


def get_identifier_pe(aln, barcodes):
    pos1, pos2 = aln.reference_start, aln.next_reference_start
    ref1, ref2 = aln.reference_id, aln.next_reference_id
    if barcodes: 
        identifier = (ref1, pos1, ref2, pos2, aln.opt("xc"))
    else:
        identifier = (ref1, pos1, ref2, pos2)
    return identifier

if __name__ == "__main__":
    argh.dispatch_command(main)
