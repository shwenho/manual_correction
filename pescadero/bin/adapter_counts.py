#!/usr/bin/env python3


import sys
import argh
import re
from dovetools.utils import FastqHelper
import json


def main(r1, r2=None, barcodes=None, order=None, barcode_positions=None, restriction_site=None, independent=False, output="/dev/stdout"):
    parsed_barcodes = barcodes_from_file(barcodes, order, barcode_positions)
        
    counts = [0] * ((len(order.split(","))+3))
    
    if r2:
        for read1, read2 in FastqHelper.read_paired_lines(r1, r2):
            update_counts(read1, read2, parsed_barcodes, counts, independent, order, restriction_site)
    else:
        for read1 in FastqHelper.read_lines(r1):
            update_counts(read1, None, parsed_barcodes, counts, independent, order, restriction_site)
    prev = counts[0]

    json_output = {}
    names = ["total", "restriction_site", "estimated_free_ends"] + ["adapter_" + str(i + 1) for i in range(len(order.split(",")))]
    for name, count in zip(names, counts):
        if name != "estimated_free_ends":
            json_output[name+"_percent"] = round((100*count)/prev, 3)
        else:
            json_output[name+"_percent"] = round((100*count)/counts[0], 3)
        json_output[name] = count
        prev = count

    json_output["Final"] = round((100*counts[-1])/counts[0], 3)
    json_output["Final_restriction_site"] = round((100*counts[-1])/counts[1], 3)
    json_output["Final_free_end"] = round((100*counts[-1])/counts[2], 3)

    with open(output, 'w') as out_handle:
        json.dump(json_output, out_handle, indent=4)
        
def update_counts(read1, read2, parsed_barcodes, counts, independent, order, restriction_site):
    read1_adapters = adapters_in_reads(read1, parsed_barcodes, order, restriction_site)

    if not read2:
        for count in read1_adapters:
            for i, pos in enumerate(read1_adapters):
                counts[i] += pos
        return

    read2_adapters = adapters_in_reads(read2, parsed_barcodes, order, restriction_site)

    if not independent:
        count = [i | j for i, j in zip(read1_adapters, read2_adapters)]
        for i, pos in enumerate(count):
            counts[i] += pos

    else:
        # why do this?
        for count in (read1_adapters, read2_adapters):
            for i, pos in enumerate(count):
                counts[i] += pos

#count: [total,restriction_site,free_end,bc1,bc2,bc3]    
def adapters_in_reads(read, parsed_barcodes, order, restriction_site):
    count = [0] * (len(parsed_barcodes)+3 )
    # Increment total
    count[0] += 1
    
    if re.search(restriction_site, read.sequence):
        count[1] += 1
    else:
        return count


    free_end = False
    for i, barcode_round in enumerate(order.split(",")):
        free_end = False
        for label, barcode in parsed_barcodes[barcode_round].items():
            if barcode in read.sequence:
                free_end = True
    for enyzme in restriction_site.split("|"):
        if re.search("^" + restriction_site, read.sequence):
            free_end = True
    if free_end:
        count[2] += 1

    for i, barcode_round in enumerate(order.split(",")):
        present = False
        for label, barcode in parsed_barcodes[barcode_round].items():
            if barcode in read.sequence or rc(barcode) in read.sequence:
                count[i+3] += 1
                present = True
                break
        if not present:
            break
                
    return count

def barcodes_from_file(barcodes, order, barcode_positions):
    parsed_barcodes = {}
    order_to_pos = order_to_positions(order, barcode_positions)
    with open(barcodes) as f:
        for line in f:
            s = line.split("\t")
            if len(s) < 3:
                continue

            label = s[1]
            key = label[:2]
            barcode = s[2]
            stripped_barcode = strip_barcode(barcode)
            try:
                start, end = order_to_pos[key]
            except KeyError:
                continue
            parsed_barcodes.setdefault(key, {})
            parsed_barcodes[key][label] = stripped_barcode[start:end]
    return parsed_barcodes

def order_to_positions(order, positions):
    order_to_pos = {}
    for key, pos in zip(order.split(","), positions.split(",")):
        start,end = map(int, pos.split("-"))
        order_to_pos[key] = [start, end]
    return order_to_pos

def strip_barcode(barcode):
    bases = set("ATGCatgc")
    return ''.join([i for i in barcode if i in bases])

def rc(seq):
    bases = dict(zip("ATGCNatgcn", "TACGNtacgn"))
    return ''.join([bases[i] for i in seq])


if __name__ == "__main__":
    argh.dispatch_command(main)
