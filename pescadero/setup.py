#!/usr/bin/env python3

from setuptools import setup

setup(name='pescadero',
      version='0.1.0',
      description="Pescadero utility tools.",
      author='Jonathan Stites',
      author_email='jon@dovetail-genomics.com',
      license='None',
      packages=['pescadero'],
      install_requires=[
          'pysam>=0.8.1',
          'argh',
          'numpy',
          'matplotlib',
          'snakemake>=3.2'
      ],
      zip_safe=False,
      scripts=["bin/enhanced_duplicate_histogram.py",
               "bin/enhanced_ztp_complexity_barcodes.py",
               "bin/pescadero_remove_barcodes.py",
               "bin/adapter_counts.py"])
