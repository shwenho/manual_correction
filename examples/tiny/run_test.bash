#!/usr/bin/env bash
snap index contigs.fa contigs-index
snap paired contigs-index -pairedInterleavedFastq  reads.fastq  -o tiny.bam
hirise_assembly.py --trivial -b tiny.bam -o test.hra -M chicago_model.json
../../src/test/threaded_joiner -b tiny.bam -M chicago_model.json -i test.hra -m dummy.bed
