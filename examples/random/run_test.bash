#!/usr/bin/env bash
fake_scaffold_gen.py -n 10 -L 50000 -C contigs.fa -R reads.fastq
snap index contigs.fa contigs-index
snap paired contigs-index -pairedInterleavedFastq  reads.fastq  -o test.bam
hirise_assembly.py --trivial -b test.bam -o test.hra -M ../tiny/chicago_model.json
../../src/test/threaded_joiner -b test.bam -M ../tiny/shotgun_model.json -i test.hra -m dummy.bed -o links -j 1
cat links_*.txt | grep -v "^#"


