Employees of Dovetail Genomics are entitled to use of this package.
No one else is permitted without explicit approval of the author.

Contact jon@dovetail-genomics.com
