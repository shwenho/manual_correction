#!/usr/bin/env python3

from setuptools import setup

setup(name='cloud',
      version='0.2.0',
      description="Dovetail cloud utility tools.",
      author='Jonathan Stites',
      author_email='jon@dovetail-genomics.com',
      license='None',
      packages=['cloud'],
      install_requires=[
          'argh',
          'paramiko',
          'boto3',
          "invoke",
          "pyyaml"
      ],
      zip_safe=False,
      scripts=["cloud/cloud_rise.py"])

