#!/usr/bin/env python3

import boto3
import random
import argh
import string
import paramiko
import time
import os
from cloud import interactive
import sys
import yaml
import uuid
from cloud.exceptions import InstanceException


@argh.arg("-a", "--assembly", required=False)
def main(project, config="~/.cloud_rise/config.yaml", instance_id=None,
         assembly="derp", overwrite=False, dove_cmd=None, hirise=False,
         compose_only=False,image=None,project_type="hirise_vanilla"):
    my_config = os.path.expanduser(config)
    params = get_params(my_config)
    if not image:
        image = params["image"]

    if compose_only:
        docker_compose = write_docker_compose(project, params, params["branch"], assembly, overwrite, dove_cmd,image) 
        print(docker_compose)
        sys.exit(1)

    if instance_id:
        print("Using given instance ", instance_id, file=sys.stderr)
        instance = get_instance(instance_id)
    else:
        print("Launching new instance ", file=sys.stderr)
        instance = launch_instance(params)
    
    print("Waiting for instance to start up", file=sys.stderr)
    wait_for_instance(instance)

    name = "cloud_rise_" + project
    print("Instance name is: ", name, file=sys.stderr)
    print("Instance id is: ", instance.instance_id, file=sys.stderr)
    tag_instance_and_volume(instance,name,project_type,project)
    
    print("ssh into instance. Instance may not be ready yet.", file=sys.stderr)
    client = ssh_instance(instance, params)

    print("Copying config parameters", file=sys.stderr)
    invoke_config = write_invoke_config(project, params, params["branch"], assembly, overwrite, dove_cmd, image)
    docker_compose = write_docker_compose(project, params, params["branch"], assembly, overwrite, dove_cmd, image)
    
    copy_to_remote(instance, params, params["tasks"])
    copy_to_remote(instance, params, invoke_config, remote_file="/home/ubuntu/invoke.yaml")
    copy_to_remote(instance, params, docker_compose, remote_file="/home/ubuntu/docker-compose.yml")
    copy_to_remote(instance, params, params["run_hirise"])
    if params["sftp_key"]:
        copy_to_remote(instance,params,params["sftp_key"],remote="/home/ubuntu/.ssh/")
    
    os.remove(invoke_config)
    if not instance_id:
        remote_execute(client, "invoke mount_vol; invoke docker_init;")
    if hirise:
        remote_execute(client, "source /home/ubuntu/environments/dovetail/bin/activate; invoke hirise")
    print("You can ssh into ", instance.public_dns_name, file=sys.stderr)


def tag_instance_and_volume(instance,name,project_type,project):
    tags = [{"Key":"Name","Value":name},{"Key":"Organism","Value":project},{"Key":"Project_type","Value":project_type}]
    instance.create_tags(Tags=tags)
    volume = list(instance.volumes.all())[0]
    volume.create_tags(Tags=tags)
    


def get_params(config):
    with open(config) as f:
        params = yaml.load(f)
    return params

def launch_instance(params):
    ec2 = boto3.resource("ec2")
    client_token = generate_client_token()

    ami = params["ami"]
    key_name = params["key_name"]
    security_group = params["security_group"].split(",")
    instance_type = params["instance_type"]
    zone = params["zone"]
    volume_size = int(params["volume_size"])
    volume_type = params["volume_type"]
    delete_on_termination = (params["delete_on_termination"])

    instances = ec2.create_instances(
        MinCount=1,
        MaxCount=1,
        ImageId=ami,
        DryRun=False, 
        KeyName=key_name, 
        SecurityGroupIds=security_group,
        InstanceType=instance_type,
        ClientToken=client_token,
        Placement={"AvailabilityZone": zone},
        BlockDeviceMappings=[
            {
                "VirtualName": "ephemeral1",
                "DeviceName": "/dev/xvdj",
                "Ebs": {
                    "VolumeSize": volume_size,
                    "DeleteOnTermination": delete_on_termination,
                    "VolumeType": volume_type
                    }
                }
        ]
    )
    
    if len(instances) != 1:
        raise InstanceException("Should be exactly one instance: ", instances)
    instance = instances[0]
    print(instance.instance_id, instance.public_dns_name)
    return instance

def get_instance(instance_id):
    ec2 = boto3.resource("ec2")
    instances = list(ec2.instances.filter(InstanceIds=[instance_id]))
    if len(instances) != 1:
        raise InstanceException("Should be exactly one instance: ", instances)
    return instances[0]

def wait_for_instance(instance):
    status = instance.state["Name"]
    while status == "pending":
        time.sleep(10)
        instance.reload()
        status = instance.state["Name"]
        print("Instance status: ", status)
    while status != "running":
        print("Instance status (may be a problem): ", status)
        instance.reload()
        status = instance.state["Name"]
        time.sleep(10)
    return

def add_tag(instance, key, value):
    instance.create_tags(Tags=[{"Key": key, "Value": value}])

def ssh_instance(instance, params):
    key = params["key"]
    ec2_user = params["default_ec2_user"]

    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.WarningPolicy())
    pkey = paramiko.RSAKey.from_private_key(open(key, 'r'))

    retry = True
    print("*** Connecting...")
    while retry:
        try:
            client.connect(instance.public_dns_name, 22, ec2_user, pkey=pkey)
            retry = False
        except ConnectionRefusedError:
            time.sleep(10)
        except paramiko.ssh_exception.NoValidConnectionsError:
            time.sleep(10)

    return client


def remote_execute(client, command):
    print("executing command", command)
    stdin, stdout, stderr = client.exec_command(command)
    for line in stdout:
        print('... ' + line.strip('\n'))
        sys.stdout.flush()
    for line in stderr:
        print('. ' + line.strip('\n'))
        sys.stdout.flush()

def generate_client_token():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(64))

def copy_to_remote(instance, params, local, remote="/home/ubuntu", remote_file=None):
    key = params["key"]
    ec2_user = params["default_ec2_user"]
    pkey = paramiko.RSAKey.from_private_key(open(key, 'r'))

    t = paramiko.Transport((instance.public_dns_name, 22))
    t.start_client()
    t.auth_publickey(ec2_user, pkey)

    sftp = paramiko.SFTPClient.from_transport(t)
    if not remote_file:
        remote_file = os.path.join(remote, os.path.basename(local))
    sftp.put(local, remote_file)

def interact(client):
    height, width = os.popen("stty size", "r").read().split()
    chan = client.invoke_shell(width=int(width), height=int(height))
    interactive.interactive_shell(chan)
    chan.close()
    client.close()
    return client


def write_docker_compose(project, params, branch, assembly, overwrite, dove_cmd,image):
    
    outfile = os.path.join("/tmp", str(uuid.uuid4())[:5] + "docker-compose.yml.tmp" )
    data = {}
    data[image] = {}
    data[image]["image"] = "431382570195.dkr.ecr.us-east-1.amazonaws.com/%s" % (image)
    data[image]["volumes"] = []
    data[image]["volumes"].append("/mnt:/mnt")
    if image == "production":
        data[image]["command"] = "/opt/productionInit.sh"
    else:
        data[image]["command"] = "/bin/bash"
    if image == "hirise-lite":
        data[image]["environment"] = {}
        data[image]["environment"]["SHELL"] = "/bin/bash"
        data[image]["environment"]["LD_LIBRARY_PATH"] = "/usr/local/lib"
        data[image]["environment"]["HIRISE_COMMIT"] = "v2.1.2"
        data[image]["environment"]["HIRISE_HELPER_COMMIT"] = "v2.1.3"
        data[image]["environment"]["HIRISE_UTILS_COMMIT"] = "v2.1.8"

    
    with open(outfile, 'w') as f:
        f.write(yaml.dump(data, default_flow_style=False))
    return outfile

def write_invoke_config(project, params, branch, assembly, overwrite, dove_cmd,image):
    
    outfile = os.path.join("/tmp", str(uuid.uuid4())[:5] + "invoke.yaml.tmp" )

    has_config_defaults = [
        "git_user",
        "json_replace",
        "install_packages",
        "git_password",
        "jira_user",
        "jira_password",
        "image",
        "branch"
        ]

    data = {
        "project": project,
        "branch": branch,
        "assembly": assembly,
        "overwrite": overwrite,
        "image":image,
        "dove_cmd": dove_cmd
        }

    
    for key in has_config_defaults:
        try:
            if not data[key]:
                data[key] = params[key]
        except KeyError:
            data[key] = params[key]

    with open(outfile, 'w') as f:
        f.write(yaml.dump(data, default_flow_style=False))
    return outfile

if __name__ == "__main__":
    argh.dispatch_command(main)
