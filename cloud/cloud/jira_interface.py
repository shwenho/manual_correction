import os, subprocess, csv, io, dateutil, datetime, uuid, sys, string, hashlib, tempfile, gzip
import logging
import subprocess
import io


class JiraInterface:

    def __init__(self):
        pass

    def get_customer_ticket(self, project):
        jira_issues = self.get_CP_by_name(project)
        num_issues = len(jira_issues)
        
        if num_issues == 1:
            return jira_issues[0]["Key"]
        elif num_issues == 0:
            msg = "No issues match project {0}".format(project)
        else:
            issues = '\n'.join([issue["Summary"] for issue in jira_issues])
            msg = "{1} issues match project {0}:\n{2}".format(project, num_issues, issues)
        
    def get_CP_by_name(self, project):
        jira_search = subprocess.check_output([
            "jira", "--action", "getIssueList", "--jql",
            """project="Customer Projects" and type="Customer Project" 
            and summary ~ '{0}' """.format(project)], universal_newlines=True)
        jira_reader = self.convert_to_issues(jira_search)
        return jira_reader

    def get_CP_by_ticket(self, ticket):
        jira_search = subprocess.check_output(
            ["jira", "--action", "getIssueList", "--jql", 
             "issue='{0}'".format(ticket)], 
            universal_newlines=True)
        
        jira_issues = self.convert_to_issues(jira_search)
        msg = "More than one issue with {0}: {1}".format(ticket, jira_issues)
        if len(jira_issues) != 1:
            raise MetadataError(msg)
        return jira_issues[0]

    def get_description(self, ticket):
        jira_issues = self.get_CP_by_ticket(ticket)
        description = jira_issues["Summary"]
        return description

    def get_date(self, ticket):
        jira_issues = self.get_CP_by_ticket(ticket)
        date = jira_issues["Created"]
        return date

    def convert_to_issues(self, jira_output):
        jira_filelike = io.StringIO(jira_output)
        jira_filelike.readline()
        jira_reader = csv.DictReader(jira_filelike)
        jira_issues = list(jira_reader)
        return jira_issues
