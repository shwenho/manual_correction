#!/usr/bin/env bash

set -e

assembly=$1
repo=$2
chicago_snake=${repo}/snake/snakefile.chicago.local
shotgun_snake=${repo}/snake/snakefile.shotgun.justsnap
hirise_snake=${repo}/snake/snakefile.hirise

#export PATH=$PATH:/opt/dovetail/bin:/home/ubuntu/jira-cli-4.5.0-SNAPSHOT
#source /home/ubuntu/environments/dovetail/bin/activate

echo $PATH

if [[ -z $assembly  ]]
then
    echo "No assembly specified."
    exit 1;
fi

if [[ -z $hirise ]]
then
    hirise=hirise_$date    
fi


chicago_align() {
    local assembly=$1
    local chi=$2
    echo "assembly: $assembly"
    echo "chicago: $chi"
    dove-pipe snake -w $chi  -g "chicago|$chi" -g "general"  -t $chicago_snake -c 'snakemake -j 24 -T -p  -r results.json' -m results.json -s "chicago|$chi|output"
    echo "alignment done"
}

export -f chicago_align

dove-pipe set -s "general|assembly_name" -v $assembly
contigs=$(jq   ".data.assemblies[\"$assembly\"].contigs" dove.json | tr -d '"')
dove-pipe set -s "general|contigs" -v $contigs

snap_index=$(jq   ".data.assemblies[\"$assembly\"].snap_index" dove.json | tr -d '"')
dove-pipe set -s "general|snap_index_chicago" -v $snap_index
dove-pipe set -s "general|snap_index_shotgun" -v $snap_index

for i in $(jq   ".data.chicago | keys | .[]" dove.json | tr -d '"')
do
echo "aligning $i"
sleep 2
chicago_align $assembly $i
bam=$(jq ".data.chicago.$i.output.md_bam" dove.json | tr -d '"')
dove-pipe set -s "hirise|bam_files|$i" -v $bam
done


date=$(date +"%Y-%m-%d" )
mkdir -p reports
chicago_json_to_report.py -j dove.json --all-chicago --html reports/QC_reports_all_${date}.html


shotgun=$(jq   ".data.shotgun | keys | .[]" dove.json | tr -d '"')
if [[ -z $shotgun ]]
then
    echo "proceeding without shotgun"
    dove-pipe set -s "hirise|nodepthscreen" -v "True"
else
    dove-pipe snake -w shotgun -g 'general' -g 'shotgun' -t $shotgun_snake -c "snakemake -j 16 -T -p " -m results.json -s "shotgun|output"
    shotgun_bam=$(jq ".data.shotgun.output.shotgun_bam" dove.json | tr -d '"')
    dove-pipe set -s "hirise|shotgun_bam" -v $shotgun_bam
fi

dove-pipe snake -w hirise_$date -g 'hirise' -g 'general' -t $hirise_snake -c 'snakemake  --cores 16 -T -p  packaged.txt' 
