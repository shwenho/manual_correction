#!/usr/bin/env python3

import argh
import gzip

def get_NX(assembly):
    lengths = get_lengths(assembly)
    total_bases = sum(lengths)
    running_total = 0
    nx = 1
    for i, scaffold_length in enumerate(lengths):
        running_total += scaffold_length
        running_percent = 100*running_total / total_bases
        while running_percent >= nx:
            yield nx, scaffold_length, i
            nx += 1

def get_lengths(assembly):
    lengths = []
    fasta = FastAreader(assembly)
    for header, seq in fasta.readFasta():
        lengths.append(len(seq))
    return sorted(lengths, reverse=True)

class FastAreader:
    '''
    Class to provide reading of a file containing one or more FASTA
    formatted sequences:
    object instantiation:
    FastAreader(<file name>):

    object attributes:
    fname: the initial file name

    methods:
    readFasta() : returns header and sequence as strings.
    Author: David Bernick
    Date: April 19, 2013
    '''
    def __init__(self, fname):
        '''contructor: saves attribute fname '''
        self.fname = fname

    def readFasta(self):
        '''
        using filename given in init, returns each included FastA record
        as 2 strings - header and sequence.
        whitespace is removed, no adjustment is made to sequence contents.
        The initial '>' is removed from the header.
        '''
        header = ''
        sequence = ''

        if self.fname.endswith(".gz"):
            open_type = gzip.open
            open_mode = "rb"
        else:
            open_type = open
            open_mode = "r"
        with open_type(self.fname, open_mode) as fileH:
            # initialize return containers
            header = ''
            sequence = ''

            # skip to first fasta header
            line = fileH.readline()
            while not line.startswith('>'):
                line = fileH.readline()
            header = line[1:].rstrip()

            # header is saved, get the rest of the sequence
            # up until the next header is found
            # then yield the results and wait for the next call.
            # next call will resume at the yield point
            # which is where we have the next header
            for line in fileH:
                if line.startswith('>'):
                    yield header, sequence
                    header = line[1:].rstrip()
                    sequence = ''
                else:
                    sequence += ''.join(line.rstrip().split()).upper()
        # final header and sequence will be seen with an end of file
        # with clause will terminate, so we do the final yield of the data
        yield header, sequence

if __name__ == "__main__":
    argh.dispatch_command(get_NX)
