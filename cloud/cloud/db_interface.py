#!/usr/bin/env python3

import logging
import pymongo
import os
from bson.objectid import ObjectId
from pprint import pprint
from jira_interface import JiraInterface


class Misc:
    def upload_to_s3():
        pass

    def create_id():
        return ObjectId()

    def bson_verify(d, logger=None):
        logger = logger or logging.getLogger(__name__)
        for key, value in d.items():
            if isinstance(value, dict):
                value = bson_verify(value)
            if "." in key or key.startswith("$"):
                logger.error("{0} has improper bson characters".format(key))
                raise AttributeError


class Project():
    
    def __init__(self, project_name, logger=None):

        # Will remove this 
        logging.basicConfig(level=logging.DEBUG)

        self.logger = logger or logging.getLogger(__name__)
        self.project_name = project_name
        self.attributes = self.use_existing()

    def use_existing(self):
        project_number = db.projects.count({"project": self.project_name})

        if project_number > 1:
            self.logger.error("{0} matches {1} projects".format(
                self.project_name, project_number))
            raise RunTimeError(
                "Should not have {0} projects matching {1}".format(
                project_number, self.project_name))

        elif project_number == 1:
            project = db.projects.find_one({"project": self.project_name})
            self.logger.debug(
                "{0} matches {1}".format(self.project_name, project))
            return project

        self.logger.info("Creating new project {0}".format(self.project_name))
        return {}

    def override_from_config(self):
        pass

    def add_required(self):
        attributes = self.attributes

        self.logger.info("creating project {0}".format(self.project_name))
        
        attributes.setdefault("_id", Misc.create_id())
        self.logger.debug("set _id {0}".format(attributes["_id"]))

        attributes.setdefault("name", self.project_name)
        self.logger.debug("set project name {0}".format(attributes["name"]))

        attributes.setdefault("dt_type", "project")
        self.logger.debug("set dt_type {0}".format(attributes["dt_type"]))

        self.attributes = attributes

    def add_optional(self):
        attributes = self.attributes
        
        self.logger.debug("searching for jira ticket")
        ticket = self.get_ticket()
        if ticket:
            attributes.setdefault("jira_ticket", ticket)
            self.logger.info("using jira ticket {0}".format(ticket))
        else:
            self.logger.warn("no jira ticket found")

        attributes.setdefault("s3_key", self.get_s3_key())
        self.logger.debug("using s3 key {0}".format(attributes["s3_key"]))
        
    def verify(self):
        required = ["_id", "name", "dt_type"]
        for key in required:
            if key not in self.attributes:
                self.logger.error("{0} not set".format(key))
                raise KeyError(key)
        Misc.bson_verify(self.attributes)

    def get_ticket(self):
        ticket = jira_interface.get_customer_ticket(self.attributes["name"])
        return ticket

    def get_s3_key(self):
        s3_key = os.path.join("projects", self.attributes["name"])
        return s3_key

class Read:
    def add_required():
        pass

    def verify():
        pass

    def create_id():
        pass

    def add_type():
        pass

    def add_project_id():
        pass
        
    # Move to library?
    def add_subcategory():
        pass

    def add_dataset_id():
        pass

    def add_md5sum():
        pass

    # Warn if can't?
    def add_pair_type():
        pass
        
    # Warn if can't?
    def add_pair_fastq():
        pass


class Dataset:
    
    def add_required():
        pass

    def verify():
        pass

    def create_id():
        pass

    def add_type():
        pass
    
    def add_project_id():
        pass
    
    def add_library_id():
        pass

    def add_name():
        pass

    def add_project_id():
        pass
        
class Library:
    def add_required():
        pass

    def verify():
        pass

    def create_id():
        pass

    def add_type():
        pass
    
    def add_project_id():
        pass

    # Warn if can't?
    def add_junction():
        pass

class Bam:
    def add_required():
        pass

    def verify():
        pass

    def create_id():
        pass

    def add_type():
        pass
    
    def add_project_id():
        pass

    def add_md5sum():
        pass

    # Warn ?
    def add_assembly_id():
        pass

    # Warn ?
    def add_read_ids():
        pass

class Assembly:
    def add_required():
        pass

    def verify():
        pass

    def create_id():
        pass

    def add_type():
        pass
    
    def add_project_id():
        pass

    def add_md5sum():
        pass
    
    def add_subcategory():
        pass

    # Hirise specific:
    def add_bam_ids():
        pass
        
    # Make hirise iter a data type? 
    def add_hirise_iter():
        pass

    def add_input_assembly():
        pass

    # Move to hirise run?
    def add_version_info():
        pass



if __name__ == "__main__":
    client = pymongo.MongoClient("192.168.89.249", 27017)
    db = client.production
    jira_interface = JiraInterface()

    p = Project("testingXXXXYYYZZZ")
    p.add_required()
    p.verify()
    p.add_optional()
    p.verify()
