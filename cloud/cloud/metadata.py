#!/usr/bin/env python3

import os, subprocess, csv, io, dateutil, datetime, uuid, sys, string, hashlib, tempfile, gzip
import yaml
import json
import boto3
from pprint import pprint
import NX
from utils import FastqHelper
from collections import Counter
import itertools
import glob


def get_md5sum(afile):
    open_type = open
    if afile.endswith(".gz"):
        open_type = gzip.open

    with open_type(afile, 'rb') as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
    return m.hexdigest()

def s3_upload(local, s3_key, md5sum):
    client = boto3.client("s3")
    transfer = boto3.s3.transfer.S3Transfer(client)
    response = client.list_objects(
        Bucket="dovetailgenomics", 
        Prefix=s3_key)
    try:
        response["Contents"]
        s3_object = client.get_object(Bucket="dovetailgenomics", Key=s3_key)
        s3_object_md5sum = s3_object["Metadata"].get("md5sum")
        print(md5sum, "compared to", s3_object_md5sum)

    except KeyError:
        s3_object_md5sum = None        
    
    if md5sum != s3_object_md5sum:
        print("uploading", local)
        transfer.upload_file(
            local, "dovetailgenomics", s3_key,
            extra_args={"Metadata":{"md5sum":md5sum}})

class Project:

    def __init__(self):
        self.sdb_interface = SimpleDBInterface()
        self.jira_interface = JiraInterface()

    def use_existing_project(self, project_uid=None, select=None, project_name=None):
        item = self.get_project_sdb(select=select, project_name=project_name, project_uid=project_uid)
        if not item:
            raise MetadataError("Project {3} doesn't match {0} {1} {2}".format(project_uid, project_name, select, item))
        self.attributes = self.sdb_interface.get_item_attributes(item)
        return item

    def set_params(
            self, project_name=None, select=None, s3_key=None, jira_ticket=None, 
            description=None, date=None, no_ticket=False, project_uid=None):
        
        if not (select or project_name or project_uid):
            raise MetadataError("Must provide query parameters or project name")

        if project_name and " " in project_name:
            msg = "Project name {0} should not have spaces on Linux systems!".format(project_name)
            raise MetadataError(msg)

        new_attributes = {
            "category": "project",
            "name": project_name,
            "uid": None,
            "s3_key": s3_key,
            "jira_ticket": jira_ticket,
            "description": description,
            "date": date,
            "project": project_name
        }

        self.add_default_values(new_attributes, no_ticket)
        new_attributes["date"] = self.sdb_interface.format_date(new_attributes["date"])
        self.attributes = {k:v for k, v in new_attributes.items() if v is not None}
        self.sdb_interface.is_only(
            {
                "category": "project", 
                "name": self.attributes["name"]
                })

    def put_attributes(self, dryrun=False, force=False):
        self.sdb_interface.put_attributes(self.attributes, dryrun, force)

    def show_attributes(self, attributes=None):
        if attributes is None and self.attributes:
            pprint(self.attributes)
        else:
            pprint(attributes)

    def add_default_values(self, attributes, no_ticket):
        if not attributes["s3_key"]:
            self.add_default_s3_key(attributes)

        if not attributes["jira_ticket"] and not no_ticket:
            self.add_default_ticket(attributes)

        if not attributes["description"] and not no_ticket:
            self.add_default_description(attributes)

        if not attributes["date"]:
            self.add_default_date(attributes)

        if not attributes["uid"]:
            self.add_default_uid(attributes)

    def add_default_date(self, attributes):
        ticket = attributes["jira_ticket"]
        date = self.jira_interface.get_date(ticket)
        attributes["date"] = date

    def add_default_uid(self, attributes):
        uid = self.sdb_interface.create_uid()
        attributes["uid"] = uid

    def add_default_description(self, attributes):
        ticket = attributes["jira_ticket"]
        description = self.jira_interface.get_description(ticket)
        attributes["description"] = description

    def add_default_s3_key(self, attributes):
        project = attributes["name"]
        s3_key = os.path.join("projects", project)
        attributes["s3_key"] = s3_key

    def add_default_ticket(self, attributes):
        project = attributes["name"]
        summary = self.project_to_summary(project)
        ticket = self.jira_interface.get_customer_ticket(summary)
        attributes["jira_ticket"] = ticket

    def get_project_sdb(self, select=None, project_name=None, project_uid=None):
        if project_uid:
            select = "select * from {1} where category = 'project' and itemName() = '{0}'".format(project_uid, self.sdb_interface.domain)
        elif not select:
            select = "select * from {1} where category = 'project' and name = '{0}'".format(project_name, self.sdb_interface.domain)

        items = self.sdb_interface.select(select_command=select)
        
        if len(items) > 1:
            msg = """There are multiple projects matching uid {0} or project name {1}.
            This should not happen.""".format(uid, project_name)
            raise MetadataError(error_msg)

        elif len(items) == 1:
            item = items[0]
            return item

        else:
            return {}

    def get_project_attributes_sdb(self, select=None, project_name=None, project_uid=None):
        item = self.get_project_sdb(select, project_name, project_uid)
        return self.sdb_interface.get_item_attributes(item)

    def get_project_uid(self, select=None, project_name=None):
        item = self.get_project_sdb(select=select, project_name=project_name)
        return self.sdb_interface.get_item_uid(item)
            
    def summary_to_project(self, summary):
        return summary.replace(" ", "_")

    def project_to_summary(self, project):
        return project.replace("_", " ")


class Assembly:

    def __init__(self):
        self.sdb_interface = SimpleDBInterface()

    def set_params(
            self, assembly, subcategory, project_name=None, project_uid=None, s3_key=None, 
            jira_ticket=None, description=None, date=None, select=None, project_select=None,
            assembly_uid=None, assembly_name=None, md5sum=None):

        if not (project_select or project_name or project_uid):
            raise MetadataError("Must provide query parameters or project name or project uid")

        if project_name and " " in project_name:
            msg = "Project name {0} should not have spaces on Linux systems!".format(project_name)
            raise MetadataError(msg)

        self.check_gzip(assembly)
            
        project = Project()
        project.use_existing_project(project_name=project_name, project_uid=project_uid, select=project_select)
        known_project_uid = project.attributes["uid"]

        self.sdb_interface
        
        new_attributes = {
            "category": "assembly",
            "subcategory": subcategory,
            "name": assembly_name,
            "uid": assembly_uid,
            "s3_key": s3_key,
            "jira_ticket": jira_ticket,
            "description": description,
            "date": date,
            "of": known_project_uid,
            "md5sum": md5sum,
            "nx_stats": None,
            "project": project.attributes["project"]
        }

        self.project = project
        self.add_default_values(new_attributes, assembly)
        new_attributes["date"] = self.sdb_interface.format_date(new_attributes["date"])
        self.attributes = {k:v for k, v in new_attributes.items() if v is not None}
        self.assembly = os.path.expanduser(assembly)

        self.sdb_interface.is_only({"md5sum": self.attributes["md5sum"]})
        self.sdb_interface.is_only({"s3_key": self.attributes["s3_key"]})
        self.sdb_interface.is_only({"name": self.attributes["name"]})

    def upload_fasta(self):
        s3_upload(self.assembly, self.attributes["s3_key"], self.attributes["md5sum"])
        

    def upload_nx(self):
        temp_dir = tempfile.mkdtemp()
        temp_file = os.path.join(temp_dir, "nx.tsv")
        with open(temp_file, 'w') as temp_handle:
            for n, nx, lx in NX.get_NX(self.assembly):
                print(n, nx, lx, sep="\t", file=temp_handle)
        md5sum = get_md5sum(temp_file)
        s3_upload(temp_file, self.attributes["nx_stats"], md5sum)
        os.remove(temp_file)
        os.rmdir(temp_dir)


    def put_attributes(self, dryrun=False, force=False):
        self.sdb_interface.put_attributes(self.attributes, dryrun, force)

    def show_attributes(self, attributes=None):
        if attributes is None and self.attributes:
            pprint(self.attributes)
        else:
            pprint(attributes)

    def add_default_values(self, attributes, assembly):
        if not attributes["name"]:
            self.add_default_assembly_name(attributes, assembly)
        
        if not attributes["md5sum"]:
            self.add_md5sum(attributes, assembly)

        if not attributes["s3_key"]:
            self.add_default_s3_key(attributes, assembly)

        if not attributes["date"]:
            self.add_default_date(attributes)

        if not attributes["uid"]:
            self.add_default_uid(attributes)

        if not attributes["description"]:
            self.add_default_description(attributes)

        if not attributes["nx_stats"]:
            self.add_default_nx_stats(attributes)

    def add_md5sum(self, attributes, assembly):
        md5sum = get_md5sum(assembly)
        attributes["md5sum"] = md5sum

    def add_default_nx_stats(self, attributes):
        project_s3_key = self.project.attributes["s3_key"]
        assembly_name = attributes["name"]
        nx_stats = os.path.join(project_s3_key, "analysis", assembly_name, "nx_stats.txt")
        attributes["nx_stats"] = nx_stats

    def add_default_assembly_name(self, attributes, assembly):
        attributes["name"] = self.get_assembly_name(assembly)

    def get_assembly_name(self, assembly):
        base = os.path.basename(assembly)
        no_ext = os.path.splitext(base)[0]
        return no_ext

    def check_gzip(self, assembly):
        if assembly.endswith(".gz"):
            print(failure(
                "Assembly {0} is gzipped. This will cause"
                "problems with snap index building. Use --force"
                "to continue or decompress the assembly".format(assembly)))
            sys.exit(2)

    def add_default_date(self, attributes):
        ticket = attributes["jira_ticket"]
        if ticket:
            date = self.jira_interface.get_date(ticket)
        else:
            date = datetime.datetime.now().isoformat()
            attributes["date"] = date

    def add_default_uid(self, attributes):
        uid = self.sdb_interface.create_uid()
        attributes["uid"] = uid

    def add_default_description(self, attributes):
        ticket = attributes["jira_ticket"]
        if ticket:

            description = self.jira_interface.get_description(ticket)
            attributes["description"] = description

    def add_default_s3_key(self, attributes, assembly):
        project_s3_key = self.project.attributes["s3_key"]
        assembly_name = attributes["name"]
        file_name = os.path.basename(assembly)
        s3_key = os.path.join(project_s3_key, "results", assembly_name, file_name)
        attributes["s3_key"] = s3_key


class Read:

    def __init__(self):
        self.sdb_interface = SimpleDBInterface()
        self.jira_interface = JiraInterface()
        self.junctions = set(["GATCGATC", "AATTAATT"])

    def set_params(self, fastq, subcategory=None, jira_ticket=None, library=None):

        if not jira_ticket:
            jira_ticket = self.guess_ticket(fastq)

        if not (jira_ticket or library):
            raise MetadataError("Needs a JIRA ticket or library name: {0}".format(fastq))
        
        if not (jira_ticket or subcategory):
            raise MetadataError("Needs a JIRA ticket or library subcategory (e.g. chicago): {0}".format(fastq))

        self.fastq = fastq

        new_attributes = {
            "jira_ticket": jira_ticket,
            "subcategory": subcategory,
            "library": (library or jira_ticket),
            "category": "read",
            "name": None,
            "run_id": None,
            "machine_id": None,
            "md5sum": None, 
            "of": None,
            "project": None,
            "s3_key": None,
            "read_number": None,
            "read_length": None,
            "read_pair": None,
            "junction": None,
            "date": None,
            "uid": None,
            "description": None
            }

        self.set_defaults(fastq, new_attributes)
        self.attributes = new_attributes
        self.sdb_interface.is_only({"md5sum": self.attributes["md5sum"]})
        self.sdb_interface.is_only({"s3_key": self.attributes["s3_key"]})

    def set_defaults(self, fastq, attributes):
        if not attributes["uid"]:
            self.set_default_uid(attributes)

        if not attributes["jira_ticket"]:
            self.set_default_ticket(fastq, attributes)
        
        self.parent = self.jira_interface.get_CP_by_ticket(attributes["jira_ticket"])

        if not attributes["description"] and attributes["jira_ticket"]:
            self.set_default_description(attributes)
        
        if not attributes["subcategory"] and attributes["jira_ticket"]:
            self.set_default_subcategory(attributes)

        if not attributes["library"] and  attributes["jira_ticket"]:
            self.set_default_library(attributes)

        if not attributes["run_id"]:
            self.set_default_run_id(fastq, attributes)

        if not attributes["machine_id"]:
            self.set_default_machine_id(fastq, attributes)

        if not attributes["name"] and attributes["jira_ticket"]:
            self.set_default_name(attributes)
        
        if not attributes["md5sum"]:
            self.set_default_md5sum(attributes, fastq)

        if not attributes["project"] and attributes["jira_ticket"]:
            self.set_default_project(attributes)

        if not attributes["of"]:
            self.set_default_of(attributes)

        if not attributes["s3_key"]:
            self.set_default_s3_key(attributes, fastq)

        if not (attributes["read_number"] and
                attributes["read_length"] and 
                attributes["junction"]):
            read_number, read_length, junction = self.read_fastq_file(fastq)
        if not attributes["read_number"]:
            attributes["read_number"] = read_number

        if not attributes["read_length"]:
            attributes["read_length"] = read_length

        if not attributes["read_pair"]:
            self.set_default_read_pair(attributes, fastq)

        if not attributes["junction"]:
            attributes["junction"] = junction

        if not attributes["date"]:
            self.set_default_date(attributes)

    def set_default_description(self, attributes):
        ticket = attributes["jira_ticket"]
        description = self.jira_interface.get_description(ticket)
        attributes["description"] = description

    def set_default_uid(self, attributes):
        uid = self.sdb_interface.create_uid()
        attributes["uid"] = uid

    def upload_fastq(self):
        s3_upload(self.fastq, self.attributes["s3_key"], self.attributes["md5sum"])

    def put_attributes(self, dryrun=False, force=False):
        self.sdb_interface.put_attributes(self.attributes, dryrun, force)

    def show_attributes(self, attributes=None):
        if attributes is None and self.attributes:
            pprint(self.attributes)
        else:
            pprint(attributes)

    def set_default_date(self, attributes):
        attributes["date"] = self.sdb_interface.datenow()

    def set_default_read_pair(self, attributes, fastq):
        for read in FastqHelper.read_lines(fastq):
            comment = read.comment
            break
        read_type = comment.split(":")[0]
        if read_type == "1" or read_type == "2":
            attributes["read_pair"] = read_type
        else:
            raise MetadataError("Could not identify read pair of {0}".format(fastq))

    def read_fastq_file(self, fastq):
        junction_counts = Counter(self.junctions)
        read_lengths = 0
        for read_number, read in enumerate(FastqHelper.read_lines(fastq)):
            if len(read.sequence) > read_lengths:
                read_lengths = len(read.sequence)
            for junction in self.junctions:
                if junction in read.sequence:
                    junction_counts[junction] += 1
        return read_number, read_lengths, max(junction_counts)

    def set_default_s3_key(self, attributes, fastq):
        project_ticket = self.parent["Parent"]
        items = self.sdb_interface.select(
            "select * from {0} where jira_ticket = '{1}'".format(self.sdb_interface.domain, project_ticket))
        if len(items) != 1:
            raise MetadataError("Should be exactly one {0}: {1}".format(project_ticket, items))
        item = self.sdb_interface.get_item_attributes(items[0])
        parent_s3_key = item["s3_key"]
        fastq_name = attributes["name"]
        file_name = os.path.basename(fastq)
        s3_key = os.path.join(parent_s3_key, "data", fastq_name, file_name)
        attributes["s3_key"] = s3_key

    def set_default_of(self, attributes):
        project_ticket = self.parent["Parent"]
        items = self.sdb_interface.select(
            "select * from {0} where jira_ticket = '{1}'".format(self.sdb_interface.domain, project_ticket))
        if len(items) != 1:
            raise MetadataError("Should be exactly one {0}: {1}".format(project_ticket, items))
        item = self.sdb_interface.get_item_attributes(items[0])
        attributes["of"] = item["uid"]
        
        
    def set_default_project(self, attributes):
        project_ticket = self.parent["Parent"]
        items = self.sdb_interface.select(
            "select * from {0} where jira_ticket = '{1}'".format(self.sdb_interface.domain, project_ticket))
        if len(items) != 1:
            raise MetadataError("Should be exactly one {0}: {1}".format(project_ticket, items))
        item = self.sdb_interface.get_item_attributes(items[0])
        attributes["project"] = item["project"]

    def set_default_md5sum(self, attributes, fastq):
        md5sum = get_md5sum(fastq)
        attributes["md5sum"] = md5sum

    def set_default_machine_id(self, fastq, attributes):
        machine_id = self.get_machine_id(fastq)
        attributes["machine_id"] = machine_id

    def set_default_run_id(self, fastq, attributes):
        run_id = self.get_run_id(fastq)
        attributes["run_id"] = run_id

    def set_default_name(self, attributes):
        library    = attributes["library"]
        machine_id = attributes["machine_id"]
        run_id     = attributes["run_id"]
        name = "_".join([library, machine_id, run_id])
        attributes["name"] = name

    def get_run_id(self, fastq):
        for read in FastqHelper.read_lines(fastq):
            read_name = read.name
            run_id = read_name.split(":")[1]
            break
        return run_id

    def get_machine_id(self, fastq):
        for read in FastqHelper.read_lines(fastq):
            read_name = read.name
            run_id = read_name.split(":")[0].strip("@")
            break
        return run_id

    def set_default_library(self, attributes):
        attributes["library"] = attributes["jira_ticket"]

    def set_default_ticket(self, fastq, attributes):
        jira_ticket = self.guess_ticket(fastq)            
        attributes["jira_ticket"] = jira_ticket

    def set_default_subcategory(self, attributes):
        attributes["subcategory"] = self.parent["Type"]

    def guess_ticket(self, fastq):
        basename = os.path.basename(fastq)
        guess = basename.split("_")[0]
        try:
            issue = self.jira_interface.get_CP_by_ticket(guess)
            return issue["Key"]
        except subprocess.CalledProcessError:
            return

class Bam:

    def __init__(self):
        self.sdb_interface = SimpleDBInterface()
        self.jira_interface = JiraInterface()

    def set_params(self, bam):
        
        subcategory = "chicago"
        new_attributes = {
            "uid": None,
            "subcategory": subcategory,
            "name": None,
            "project": None,
            "of": None,
            "md5sum": None,
            "s3_key": None,
            "description": None,
            "num_reads": None,
            "date": None,
            "version_info": None
            }

        self.set_defaults(bam, new_attributes)
        self.attributes = new_attributes
        self.sdb_interface.is_only({"md5sum": self.attributes["md5sum"]})
        #self.sdb_interface.is_only({"s3_key": self.attributes["s3_key"]})

    def set_defaults(self, bam, attributes):
        if not attributes["uid"]:
            self.set_default_uid(attributes)
        if not attributes["md5sum"]:
            self.set_default_md5sum(attributes, bam)
        if not attributes["date"]:
            self.set_default_date(attributes)
        #if not attributes["s3_key"]:
            #self.set_default_s3_key(attributes, bam)
        if not attributes["of"]:
            self.set_default_of(attributes, bam)
        
    def set_default_uid(self, attributes):
        uid = self.sdb_interface.create_uid()
        attributes["uid"] = uid
    
    def set_default_md5sum(self, attributes, bam):
        md5sum = get_md5sum(bam)
        attributes["md5sum"] = md5sum

    def show_attributes(self, attributes=None):
        if attributes is None and self.attributes:
            pprint(self.attributes)
        else:
            pprint(attributes)

    def set_default_date(self, attributes):
        attributes["date"] = self.sdb_interface.datenow()

    def set_default_of(self, attributes, bam):
        config = self.get_config(bam)
        assembly_uid = self.get_assembly_uid(config)
        reads_uid = self.get_reads_uid(config)

        for read in reads_uid:
            attributes["of"] = read

        attributes["of"] = assembly_uid
        print("should be", assembly_uid, reads_uid)

    def get_config(self, bam):
        dir_path = os.path.dirname(bam)
        if os.path.basename(dir_path) == "aln":
            expected_config = os.path.join(dir_path, "../config.json")
        else:
            expected_config = os.path.join(dir_path, "config.json")

        if not os.path.exists(expected_config):
            raise MetadataError("No config file found for {0} at {1}".format(bam, expected_config))

        return expected_config

    def get_assembly_uid(self, config):
        with open(config) as config_handle:
            data = json.load(config_handle)

        assembly = self.get_assembly_config(data)
        assembly_md5sum = get_md5sum(assembly)
        assembly_uid = self.sdb_interface.uid_from_md5sum(assembly_md5sum)
        return assembly_uid
        
    def get_assembly_config(self, data):
        try:
            return data["contigs"]
        except KeyError:
            raise MetadataError("No contigs found in {0}".format(data))

    def get_reads_uid(self, config):
        with open(config) as config_handle:
            data = json.load(config_handle)
        reads = self.get_reads_config(data)
        reads_md5sum = [get_md5sum(i) for i in reads]
        reads_uids = [self.sdb_interface.uid_from_md5sum(i) for i in reads_md5sum]
        return reads_uids

    def get_reads_config(self, data):
        try:
            reads_1 = data["raw_regex_r1"]
            reads_2 = data["raw_regex_r2"]
        except KeyError:
            try:
                reads_1 = data["f1_glob"]
                reads_2 = data["f2_glob"]
            except KeyError:
                raise MetadataError("No reads found in config {0}".format(data))

        raw_r1 = sorted(itertools.chain.from_iterable(glob.glob(i) for i in reads_1.split('|')))
        raw_r2 = sorted(itertools.chain.from_iterable(glob.glob(i) for i in reads_2.split('|')))

        if len(raw_r1) != len(raw_r2):
            raise MetadataError("number of R1 != R2 : {0},  {1}".format(raw_regex_r1, raw_regex_r2))
        if len(raw_r1) == 0:
            raise MetadataError("No reads present: {0}".format(raw_regex_r1))

        for i in raw_r1:
            if i in raw_r2:
                raise MetadataError("Same read file present in r1 is in r2: {0}".format(i))
        for i in raw_r2:
            if i in raw_r1:
                raise MetadataError("Same read file present in r2 is in r1: {0}".format(i))

        return raw_r1 + raw_r2

class JiraInterface:

    def __init__(self):
        pass

    def get_customer_ticket(self, project):
        jira_issues = self.get_CP_by_name(project)
        num_issues = len(jira_issues)
        
        if num_issues == 1:
            return jira_issues[0]["Key"]
        elif num_issues == 0:
            msg = "No issues match project {0}".format(project)
        else:
            issues = '\n'.join([issue["Summary"] for issue in jira_issues])
            msg = "{1} issues match project {0}:\n{2}".format(project, num_issues, issues)
        raise MetadataError(msg)
        
    def get_CP_by_name(self, project):
        jira_search = subprocess.check_output([
            "jira", "--action", "getIssueList", "--jql",
            """project="Customer Projects" and type="Customer Project" 
            and summary ~ '{0}' """.format(project)], universal_newlines=True)
        jira_reader = self.convert_to_issues(jira_search)
        return jira_reader

    def get_CP_by_ticket(self, ticket):
        jira_search = subprocess.check_output(
            ["jira", "--action", "getIssueList", "--jql", 
             "issue='{0}'".format(ticket)], 
            universal_newlines=True)
        
        jira_issues = self.convert_to_issues(jira_search)
        msg = "More than one issue with {0}: {1}".format(ticket, jira_issues)
        if len(jira_issues) != 1:
            raise MetadataError(msg)
        return jira_issues[0]

    def get_description(self, ticket):
        jira_issues = self.get_CP_by_ticket(ticket)
        description = jira_issues["Summary"]
        return description

    def get_date(self, ticket):
        jira_issues = self.get_CP_by_ticket(ticket)
        date = jira_issues["Created"]
        return date

    def convert_to_issues(self, jira_output):
        jira_filelike = io.StringIO(jira_output)
        jira_filelike.readline()
        jira_reader = csv.DictReader(jira_filelike)
        jira_issues = list(jira_reader)
        return jira_issues

class SimpleDBInterface:

    def __init__(self, config="~/.cloud_rise/config.yaml"):
        self.client        = boto3.client("sdb")
        self.configuration = self.parse_config(config)
        self.domain        = self.configuration["sdb_domain"]
        self.invalid_characters = frozenset("'’")

    def is_only(self, checks):
        start = "select * from {0} where ".format(self.domain)
        end = "and".join([" {0} = '{1}' ".format(k, v) for k, v in checks.items()])
        select_command = start + end
        results = self.select(select_command)
        if results:
            raise MetadataError("{0} matching {1} exists.".format(checks, results))
        return True

    def query_from_uid(self, uid):
        select = "select uid from {1} where uid = '{0}'".format(uid, self.domain)
        return select

    def uid_from_md5sum(self, md5sum):
        select = "select uid from {1} where md5sum = '{0}'".format(md5sum, self.domain)
        results = self.select(select)
        if len(results) > 1:
            raise MetadataError("Multiple hits for md5sum {0}: {1}".format(md5sum, results))
        try:
            return results[0]["Name"]
        except IndexError:
            raise MetadataError("No hits for md5sum {0}.".format(md5sum))
        
    def check_response(self, response):
        return_code = response["ResponseMetadata"]["HTTPStatusCode"]
        if return_code != 200:
            msg = "SimpleDB return code {0} not equal to 200.".format(return_code)
            raise MetadataError(msg)
            
    def parse_config(self, config):
        full_path = os.path.expanduser(config)
        with open(full_path) as f:
            params = yaml.load(f)
        return params

    def get_item_uid(self, item):
        return item["Name"]

    def get_item_attributes(self, item):
        attributes = {}
        attributes["uid"] = item["Name"]
        for attribute in item["Attributes"]:
            name = attribute["Name"]
            value = attribute["Value"]
            attributes[name] = value
        return attributes

    def create_uid(self):
        u = uuid.uuid4()
        return u.hex
        
    def format_date(self, text):
        if text:
            date = dateutil.parser.parse(text)
            return date.isoformat()

    def datenow(self):
        return datetime.datetime.now().isoformat()

    def format_attributes(self, attributes, force):
        # Format attributes in the format SimpleDB wants
        attribute_list = []
        uid = None
        for key, value in attributes.items():
            new_value = self.filter_character(value)
            if key == "uid":
                uid = new_value
            else:
                this_attribute = {"Name": key, "Value": new_value}
                if force:
                    this_attribute["Replace"] = True
                attribute_list.append(this_attribute)

        return uid, attribute_list

    def filter_character(self, s):
        string_version = str(s)
        stripped = ''.join([i for i in string_version if i not in self.invalid_characters])
        return stripped

    def put_attributes(self, attributes, dryrun, force):
        uid, formatted_attributes = self.format_attributes(attributes, force)

        if not uid:
            raise MetadataError("Item does not have uid: {0}".format(attributes))

        if dryrun:
            pprint({"DomainName": self.domain,
                   "ItemName": uid,
                   "Attributes": formatted_attributes})
            sys.exit(1)
            
        
        response = self.client.put_attributes(
            DomainName=self.domain,
            ItemName=uid,
            Attributes=formatted_attributes
        )

        self.check_response(response)

    def keys_from_items(self, items, keys, all_keys=False):
        if all_keys:
            return items

        prepared_keys = []

        for item in items:
            new_item = {}
            new_item["Name"] = item["Name"]
            new_item["Attributes"] = []

            for key in keys:
                value = self.get_item_value(item, key)
                if value:
                    d = {"Name": key, "Value": value}
                    new_item["Attributes"].append(d)

            if new_item["Attributes"]:
                prepared_keys.append(new_item)

        return prepared_keys

    def get_item_value(self, item, target):
        try:
            for attribute in item["Attributes"]:
                key = attribute["Name"]
                value = attribute["Value"]
                if key == target:
                    return value
        except KeyError:
            return
            
    def delete_keys(self, items, keys, dryrun=False, all_keys=False):

        keys_for_deletion = self.keys_from_items(items, keys, all_keys=all_keys)

        if not keys_for_deletion:
            msg = "No attributes specified for deletion."
            raise MetadataError(msg)
        if dryrun:
            print("Targets for deletion:")
            pprint({"DomainName": self.domain,
                    "Items": keys_for_deletion})
            sys.exit(1)
    
        response = self.client.batch_delete_attributes(
            DomainName=self.domain,
            Items=keys_for_deletion
        )
        
        self.check_response(response)

    def select(self, select_command):
        print("executing {0}".format(select_command))
        response = self.client.select(SelectExpression=select_command)
        self.check_response(response)
        try:
            return response["Items"]
        except KeyError:
            return []
        

class MetadataError(Exception):
    pass



