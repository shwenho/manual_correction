#!/usr/bin/env python3

import os
import re
from update_sdb import ProjectArguments
from pprint import pprint
import pymongo
import argh
import subprocess
import uuid
import glob
import json
import update_sdb


def get_library_info(db, libraries, project):
    if not libraries:
        libraries_info = db.libraries.find({"project": project})
    else:
        libraries_info = []
        for library in libraries:
            library_info = db.libraries.find_one({"project": project, "name": library})
            if library_info:
                libraries_info.append(library_info)
            else:
                print("NO lIBRARY", library)
    return libraries_info

def get_bams(db, library, project, assembly_id):
    print("library:", library)
    read_ids = []
    for dataset in db.datasets.find({"library_id": library["_id"]}):
        for read in db.reads.find({"dataset_id": dataset["_id"]}):
            read_ids.append(read["_id"])
    matching_bams = []
    for bam in db.bams.find({"project": project, "assembly_id": assembly_id}):
        bam_reads = bam["read_id"]
        missing = False
        for read_id in read_ids:
            if read_id not in bam_reads:
                missing = True
        if not missing:
            matching_bams.append(bam)
        print()
        pprint(matching_bams)
    return matching_bams

def make_bam(db, library, read_ids, project, assembly_id):
    if library["subcategory"] == "Chicago Type":
        config["project"] = project
        config["cores"] = 16
        config["lib"] = library["name"]
        config["assembly_name"] = assembly
        config["contigs"] = find_file(project_dir, assembly_info["md5sum"])
        reads1, reads2 = get_reads(project_dir, db, read_ids)
        config["raw_regex_r1"] = "|".join(reads1)
        config["raw_regex_r2"] = "|".join(reads2)
        a_read = db.reads.find_one({"_id": read_ids[0]})
        print(a_read["junction"])
        pprint(a_read)
        config["junction"] = a_read["junction"]
        config["seed_size"] = 16
        pprint(config)
        random_id = get_random_id()
        working_dir = "_".join([library["name"], assembly, random_id])
        full_working_dir = os.path.join(project_dir, "analysis", "local", working_dir)
        print(full_working_dir)
        os.mkdir(full_working_dir)
        config_file = os.path.join(full_working_dir, "config.json")
        with open(config_file, 'w') as config_handle:
            json.dump(config, config_handle, indent=4, sort_keys=True)
        orig_wd = os.getcwd()
        os.chdir(full_working_dir)

        subprocess.call(["snakemake", "-j", "24", "-T", "-p",  "-r", "results.json", "-s","/home/jon/dovetail/snake/snakefile.chicago.local"])
        bam_glob = os.path.join(full_working_dir, "aln", "*bam")

    elif library["subcategory"] == "Shotgun Type":
        config["project"] = project
        config["cores"] = 16
        config["lib"] = library["name"]
        config["tmpdir"] = "."
        config["assembly_name"] = assembly
        config["contigs"] = find_file(project_dir, assembly_info["md5sum"])
        reads1, reads2 = get_reads(project_dir, db, read_ids)
        config["f1_glob"] = "|".join(reads1)
        config["f2_glob"] = "|".join(reads2)
        config["seed_size"] = 16
        pprint(config)
        random_id = get_random_id()
        working_dir = "_".join([library["name"], assembly, random_id])
        full_working_dir = os.path.join(project_dir, "analysis", "local", working_dir)
        print(full_working_dir)
        os.mkdir(full_working_dir)
        config_file = os.path.join(full_working_dir, "config.json")
        with open(config_file, 'w') as config_handle:
            json.dump(config, config_handle, indent=4, sort_keys=True)
        orig_wd = os.getcwd()
        os.chdir(full_working_dir)

        subprocess.call(["snakemake", "-j", "24", "-T", "-p", "-s","/home/jon/dovetail/snake/snakefile.shotgun.justsnap"])
        bam_glob = os.path.join(full_working_dir, "*bam")
    os.chdir(orig_wd)
    return bam_glob
@argh.arg("-l", "--libraries", nargs="+")
def hirise_db(project_dir, assembly, libraries=None):
    
    # project, cores, lib, assembly_name, contigs, read1, read2, junction, snap index
    config = os.path.join(project_dir, "project.yaml")
    print(config, project_dir)
    pa = ProjectArguments(config, project_dir)
    project = pa.data["project"]["project"]
    client = pymongo.MongoClient("192.168.89.249", 27017)
    db = client.production


    subprocess.call(["/home/jon/dovetail/cloud/cloud/update_sdb.py", "whole-project",
                     "--project-dir", project_dir, "--skip-existing"])

    print("project: ", project)
    assembly_info = db.assemblies.find_one({"project": project, "name": assembly})
    assembly_id = assembly_info["_id"]
    print("assembly: ",assembly_info)
    libraries_info = get_library_info(db, libraries, project)
    chicago_bams = {}
    chicago_ids = []
    shotgun_bam = "/dev/null"
    shotgun_id = None
    for i, library in enumerate(libraries_info):
        matching_bams = get_bams(db, library, project, assembly_info["_id"])
        if not matching_bams:
            print("Missing bam")
            config = {}
            # project, cores, lib, assembly_name, contigs, read1, read2, junction, snap index
            
            bam_glob = make_bam(db, library, read_ids, project, assembly_info["_id"])
            print(bam_glob)
            bam = list(glob.glob(bam_glob))[0]
            print(bam)
            update_sdb.new_bam(bam, skip_existing=True)
        matching_bams = get_bams(db, library, project, assembly_info["_id"])

        this_bam = matching_bams[0]

        if library["subcategory"] == "Chicago Type":
            chicago_bams[str(i)] = find_file(project_dir, this_bam["md5sum"])
            print(find_file(project_dir, this_bam["md5sum"]))
            chicago_ids.append(this_bam["_id"])
        elif library["subcategory"] == "Shotgun Type":
            shotgun_bam = find_file(project_dir, this_bam["md5sum"])
            shotgun_id = this_bam["_id"]
    print(pa.data["hirise"])
    hirise_assemblies = get_hirise_assemblies(project, assembly_info["_id"], chicago_bams, shotgun_bam, db)
    if not hirise_assemblies:
        run_hirise(pa.data["hirise"], assembly_info, project, chicago_bams, shotgun_bam, db, project_dir)

def get_hirise_assemblies(project, assembly_id, chicago_ids, shotgun_id, db):
    possible_hirise_assemblies = db.assemblies.find({"project": project, "assembly_id": assembly_id, "subcategory": "hirise"})    
    hirise_assemblies = []
    for assembly in possible_hirise_assemblies:
        missing = False
        for chicago_id in chicago_ids:
            if chicago_id not in assembly["chicago_ids"]:
                missing = True
        if shotgun_id and shotgun_id != assembly["shotgun_id"]:
            missing = True
        if missing:
            continue
        hirise_assemblies.append(assembly)
    return hirise_assemblies

def get_random_id():
    return str(uuid.uuid4())[:5]

def run_hirise(hirise_data, assembly_info, project, chicago_bams, shotgun_bam, db, project_dir):
    hirise_data["contigs"] = find_file(project_dir, assembly_info["md5sum"])
    hirise_data["bam_files"] = chicago_bams
    hirise_data["project"] = project
    hirise_data["shotgun_bam"] = shotgun_bam
    pprint(hirise_data)
    random_id = get_random_id()
    working_dir = "_".join(["hirise", random_id])
    full_working_dir = os.path.join(project_dir,"analysis", "local", working_dir)
    print(full_working_dir)
    os.mkdir(full_working_dir)
    config_file = os.path.join(full_working_dir, "config.json")
    with open(config_file, 'w') as config_handle:
        json.dump(hirise_data, config_handle, indent=4, sort_keys=True)
    orig_wd = os.getcwd()
    os.chdir(full_working_dir)

    subprocess.call(["snakemake", "-j", "24", "-T", "-p",  "-r", "hirise_iter_broken_3.fasta", "-s","/home/jon/dovetail/snake/snakefile.hirise"])
    subprocess.call(["hirise_stats.py", "--json", "--all", "-o", "stats.json"])
    assembly_glob = os.path.join(full_working_dir, "hirise_iter_*.fasta")
    os.chdir(orig_wd)
    for hirise_assembly in glob.glob(assembly_glob):
        update_sdb.new_assembly(hirise_assembly, "hirise",
            project_name=project, 
            no_upload=True,
            skip_existing=True)


def find_file(project_dir, md5sum):
    cwd = os.getcwd()
    for root, dirs, files in os.walk(project_dir):
        for f in files:
            if f == "md5sum.txt":
                full_path = os.path.join(root, f)
                with open(full_path) as handle:
                    for line in handle:
                        s = line.split()
                        if s[0] == md5sum:
                            return os.path.abspath(os.path.join(root, s[1]))
                            

def get_reads(project_dir, db, read_ids):
    reads_1 = []
    reads_2 = []
    for read_id in read_ids:
        read = db.reads.find_one({"_id": read_id})
        if read["pair_type"] == 1:
            r1_file = find_file(project_dir, read["md5sum"])
            if not os.path.exists(r1_file):
                r1_file = r1_file + ".gz"
            reads_1.append(r1_file)
            read2 = db.reads.find_one({"_id": read["pair"]})
            r2_file = find_file(project_dir, read2["md5sum"])
            if not os.path.exists(r2_file):
                r2_file = r2_file + ".gz"
            reads_2.append(r2_file)
    return reads_1, reads_2
if __name__ == "__main__":
    argh.dispatch_command(hirise_db)
