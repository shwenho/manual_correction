#!/Usr/bin/env python3

import os, subprocess, csv, io, dateutil, datetime, uuid, sys, string, hashlib, tempfile, gzip
import yaml
import json
import boto3
from pprint import pprint
import NX
from utils import FastqHelper
from collections import Counter
import itertools
import glob
import pymongo
import re
import threading

def replace_nas(data):
    replace_targets = [
        "/mnt/nas/projects/",
        "/mnt/nas2/projects/",
        "/mnt/scratch1/projects/",
        "/mnt/scratch2/projects/",
        "/mnt/scratch3/projects/",
        "/mnt/nas2/bigdove1/scratch1/projects/",
        "/mnt/nas2/bigdove1/scratch2/projects/"
        ]
    replacement = "/mnt/nas/projects/"
    new = {}
    for k, v in data.items():
        new_value = v
        if isinstance(v, dict):
            new_value = replace_nas(v)
        elif isinstance(v, str):
            for target in replace_targets:
                new_value = new_value.replace(target, replacement)
        new[k] = new_value
    return new
    


class ProgressPercentage(object):
    def __init__(self, filename):
        self._filename = filename
        self._size = float(os.path.getsize(filename))
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        # To simplify we'll assume this is hooked up
        # to a single filename.
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\r%s  %s / %s  (%.2f%%)" % (self._filename, self._seen_so_far,
                                             self._size, percentage))
            sys.stdout.flush()

def bson_replace(d):
    new = {}
    for k, v in d.items():
        if isinstance(v, dict):
            v = bson_replace(v)
        new[k.replace('.', '-')] = v
    return new

def format_date(text):
    if text:
        date = dateutil.parser.parse(text)
        return date.isoformat()

def get_md5sum(afile, block_size=256*128):
    open_type = open
    if afile.endswith(".gz"):
        open_type = gzip.open

    hasher = hashlib.md5()
    with open_type(afile, 'rb') as fh:
        buf = fh.read(block_size)
        while len(buf) > 0:
            hasher.update(buf)
            buf = fh.read(block_size)
    return hasher.hexdigest()

def s3_upload(local, s3_key, md5sum):
    client = boto3.client("s3")
    config = boto3.s3.transfer.TransferConfig(
        max_concurrency=10,
        num_download_attempts=10,
    )

    transfer = boto3.s3.transfer.S3Transfer(client, config)
    response = client.list_objects(
        Bucket="dovetailgenomics", 
        Prefix=s3_key)
    try:
        response["Contents"]
        s3_object = client.get_object(Bucket="dovetailgenomics", Key=s3_key)
        s3_object_md5sum = s3_object["Metadata"].get("md5sum")
        print(md5sum, "compared to", s3_object_md5sum)

    except KeyError:
        s3_object_md5sum = None  

    
    
    if md5sum != s3_object_md5sum:
        print("uploading", local)
        transfer.upload_file(
            local, "dovetailgenomics", s3_key,
            callback=ProgressPercentage(local),
            extra_args={"Metadata":{"md5sum":md5sum}})

class Dataset:

    def __init__(self):
        self.jira_interface = JiraInterface()
        self.client = pymongo.MongoClient("192.168.89.249", 27017)
        self.db = self.client.production
    
    def set_params(self, project=None, library=None, name=None):
        
        attributes = {
            "project": project,
            "name": name,
            "dt": "dataset",
            "library_id": library
            }
    
        self.set_defaults(attributes)
        self.attributes = attributes

    def set_defaults(self, attributes):

        if not attributes.get("project_id", None):
            self.set_default_project_id(attributes)

        #if not attributes.get("library", None):
         #   self.set_default_library(attributes, jira_ticket)

    def set_default_library(self, attributes, jira_ticket):
        if library:
            target = {"name": library, "project": attributes["project"]}
        elif fastq.attributes["jira_ticket"]: 
            target = {"jira_ticket": jira_ticket, "project": attributes["project"]}
        else:
            raise MetadataError("Needs a JIRA ticket or library name: {0}".format(attributes))
        count = self.db.libraries.count(target)
        if count > 1:
            raise MetadataError("Should only be one library with {0} , not {1}".format(target, count))
        elif count == 1:
            lib =  self.db.libraries.find_one(target)
            attributes["library_id"] = lib["_id"]

    def set_default_project_id(self, attributes):
        project_name = attributes["project"]
        num = self.db.projects.count({"project": project_name})
        items = list(self.db.projects.find({"project": project_name}))
        if num != 1:
            raise MetadataError("Should be exactly one {0}, not {2}: {1}".format(project_name, items, num))
        item = list(items)[0]
        attributes["project_id"] = item["_id"]
    def put_attributes(self, dryrun=False):
        attributes = self.attributes

        if dryrun:
            pprint(attributes)
            return
        self.db.datasets.insert_one(attributes)

class Library:
    
    def __init__(self):
        self.jira_interface = JiraInterface()
        self.client = pymongo.MongoClient("192.168.89.249", 27017)
        self.db = self.client.production

    def put_attributes(self, dryrun=False):
        attributes = self.attributes

        if dryrun:
            pprint(attributes)
            return
        self.db.libraries.insert_one(attributes)

    def set_params(self, project=None, jira_ticket=None, library=None, subcategory=None):
        
        attributes = {
            "project": project,
            "jira_ticket": jira_ticket,
            "name": (library or jira_ticket),
            "dt": "library",
            "subcategory": subcategory
            }

        self.set_defaults(attributes)
        self.attributes = attributes


    def set_defaults(self, attributes):

        if attributes["jira_ticket"]:
            self.parent = self.jira_interface.get_CP_by_ticket(attributes["jira_ticket"])
            
        if not attributes.get("description", None) and attributes["jira_ticket"]:
            self.set_default_description(attributes)
        
        if not attributes.get("subcategory", None) and attributes["jira_ticket"]:
            self.set_default_subcategory(attributes)
            
        if not attributes.get("project", None) and attributes["jira_ticket"]:
            self.set_default_project(attributes)

        if not attributes.get("project_id", None):
            self.set_default_project_id(attributes)

    def set_default_project_id(self, attributes):
        project_name = attributes["project"]
        num = self.db.projects.count({"project": project_name})
        items = list(self.db.projects.find({"project": project_name}))
        if num != 1:
            raise MetadataError("Should be exactly one {0}, not {2}: {1}".format(project_name, items, num))
        item = list(items)[0]
        attributes["project_id"] = item["_id"]

    def set_default_description(self, attributes):
        ticket = attributes["jira_ticket"]
        description = self.jira_interface.get_description(ticket)
        attributes["description"] = description

    def set_default_project(self, attributes):
        project_ticket = self.parent["Parent"]
        num = self.db.projects.count({"jira_ticket": project_ticket})
        items = list(self.db.projects.find({"jira_ticket": project_ticket}))
        if num != 1:
            raise MetadataError("Should be exactly one {0}, not {2}: {1}".format(project_ticket, items, num))
        item = list(items)[0]
        attributes["project"] = item["project"]

    def set_default_subcategory(self, attributes):
        attributes["subcategory"] = self.parent["Type"]
    
class Project:

    def __init__(self):
        self.jira_interface = JiraInterface()
        self.client = pymongo.MongoClient("192.168.89.249", 27017)
        self.db = self.client.production

    def set_params(
            self, project_name, jira_ticket=None, 
            description=None, no_ticket=False):
        
        if project_name and " " in project_name:
            msg = "Project name {0} should not have spaces on Linux systems!".format(project_name)
            raise MetadataError(msg)

        new_attributes = {
            "s3_key": None,
            "jira_ticket": jira_ticket,
            "description": description,
            "date": None,
            "project": project_name,
            "dt": "project"
            
        }

        self.add_default_values(new_attributes, no_ticket)
        new_attributes["date"] = format_date(new_attributes["date"])
        self.attributes = {k:v for k, v in new_attributes.items() if v is not None}
        
        
    def is_only_project(self, name):
        if self.db.projects.count({"project": name}) > 0:
            raise MetadataError("Multiple projects exist: {0}".format(name))

    def get_existing_project(self, name):
        return self.db.projects.find_one({"project": name})

    def put_attributes(self, dryrun=False):
        attributes = self.attributes

        if dryrun:
            pprint(attributes)
            return
        self.db.projects.insert_one(attributes)

    def show_attributes(self, attributes=None):
        if attributes is None and self.attributes:
            pprint(self.attributes)
        else:
            pprint(attributes)

    def add_default_values(self, attributes, no_ticket):
        if not attributes["s3_key"]:
            self.add_default_s3_key(attributes)

        if not attributes["jira_ticket"] and not no_ticket:
            self.add_default_ticket(attributes)

        if not attributes["description"] and not no_ticket:
            self.add_default_description(attributes)

        if not attributes["date"] and not no_ticket:
            self.add_default_date(attributes)

    def add_default_date(self, attributes):
        ticket = attributes["jira_ticket"]
        date = self.jira_interface.get_date(ticket)
        attributes["date"] = date

    def add_default_description(self, attributes):
        ticket = attributes["jira_ticket"]
        description = self.jira_interface.get_description(ticket)
        attributes["description"] = description

    def add_default_s3_key(self, attributes):
        project = attributes["project"]
        s3_key = os.path.join("projects", project)
        attributes["s3_key"] = s3_key

    def add_default_ticket(self, attributes):
        project = attributes["project"]
        summary = self.project_to_summary(project)
        ticket = self.jira_interface.get_customer_ticket(summary)
        attributes["jira_ticket"] = ticket

    def summary_to_project(self, summary):
        return summary.replace(" ", "_")

    def project_to_summary(self, project):
        return project.replace("_", " ")


class Assembly:

    def __init__(self):
        self.jira_interface = JiraInterface()
        self.client = pymongo.MongoClient("192.168.89.249", 27017)
        self.db = self.client.production

    def set_params(
            self, assembly, subcategory, project_name=None, s3_key=None, 
            jira_ticket=None, description=None, assembly_name=None, 
            md5sum=None):

        assembly = os.path.expanduser(assembly)
        self.assembly = os.path.abspath(assembly)
        if project_name and " " in project_name:
            msg = "Project name {0} should not have spaces on Linux systems!".format(project_name)
            raise MetadataError(msg)


        self.check_gzip(assembly)

        num = self.client.production.projects.count({"project":project_name})
        if num != 1:
            raise MetadataError("Should be exactly one project with name {0}, not {1}".format(project_name, num))

        project = list(self.client.production.projects.find({"project":project_name}))[0]  
        self.project = project
        new_attributes = {
            "subcategory": subcategory,
            "name": assembly_name,
            "s3_key": s3_key,
            "jira_ticket": jira_ticket,
            "description": description,
            "date": None,
            "project_id": project["_id"],
            "md5sum": md5sum,
            "project": project["project"],
            "nx_stats": None,
            "hirise_iter": None,
            "hirise_run": None,
            "stats.json": None,
            "chicago_bam_ids": None,
            "shotgun_bam_ids": None,
            "input_assembly_id": None,
            "config": None,
            "version_info": None,
            "stats": None,
            "dt": "assembly"
            
        }
        
        self.add_default_values(new_attributes, assembly, project_name)
        new_attributes["date"] = format_date(new_attributes["date"])
        self.attributes = {k:v for k, v in new_attributes.items() if v is not None}

    def get_config(self, assembly):
        dir_path = os.path.dirname(assembly)
        expected_config = os.path.join(dir_path, "config.json")

        if not os.path.exists(expected_config):
            raise MetadataError("No config file found for {0} at {1}".format(assembly, expected_config))

        return expected_config
    
    def upload_fasta(self):
        s3_upload(self.assembly, self.attributes["s3_key"], self.attributes["md5sum"])

    def is_only_md5sum(self, md5sum=None):
        if not md5sum:
            md5sum = self.attributes["md5sum"]
        if self.db.assemblies.find({"md5sum": md5sum}).count() == 0:
            return True

    def is_only_s3_key(self):
        s3_key = self.attributes["s3_key"]
        if self.db.projects.find({"s3_key": s3_key}).count() > 0:
            raise MetadataError("File already exists: {0}".format(name))
        
    def put_attributes(self, dryrun=False):
        attributes = self.attributes
        attributes = bson_replace(attributes)
        print("INSERTING")
        pprint(attributes)
        if dryrun:
            pprint(attributes)
            return
        self.db.assemblies.insert_one(attributes)

    def show_attributes(self, attributes=None):
        if attributes is None and self.attributes:
            pprint(self.attributes)
        else:
            pprint(attributes)

    def add_default_values(self, attributes, assembly, project_name):
        if not attributes["name"]:
            self.add_default_assembly_name(attributes, assembly)
        
        if not attributes["md5sum"]:
            self.add_md5sum(attributes, assembly)
            
        if not attributes["s3_key"]:
            self.add_default_s3_key(attributes, assembly)

        if not attributes["date"]:
            self.add_default_date(attributes)

        if not attributes["description"]:
            self.add_default_description(attributes)

        if not attributes["nx_stats"]:
            self.add_default_nx(attributes, assembly)

        if attributes["subcategory"] == "hirise":
            if not attributes["hirise_iter"]:
                self.add_default_hirise_iter(attributes, assembly)
            if not attributes["hirise_run"]:
                self.add_default_hirise_run(attributes, assembly)
            if not attributes["shotgun_bam_ids"]:
                self.add_default_shotgun_ids(attributes, assembly)
            if not attributes["chicago_bam_ids"]:
                self.add_default_chicago_ids(attributes, assembly)
            if not attributes["input_assembly_id"]:
                self.add_default_input_assembly_id(attributes, assembly)
            if not attributes["config"]:
                self.add_default_config(attributes, assembly)
            if not attributes["version_info"]:
                self.add_default_version_info(attributes, assembly)
            if not attributes["stats"]:
                self.add_default_stats(attributes, assembly)

    def add_default_hirise_run(self, attributes, assembly):
        dirname = os.path.dirname(assembly)
        hirise_run_id = datetime.datetime.fromtimestamp(os.path.getctime(dirname))        
        attributes["hirise_run"] = hirise_run_id
                
    def add_default_stats(self, attributes, assembly):
        stats = self.get_stats(assembly)
        attributes["stats"] = stats

    def add_default_version_info(self, attributes, assembly):
        version_info = self.get_version_info(assembly)
        attributes["version_info"] = version_info

    def get_stats(self, assembly):
        dir_name = os.path.dirname(assembly)
        stats = os.path.join(dir_name, "stats.json")
        if not os.path.exists(stats):
            raise MetadataError("No stats file found for {0} at {1}".format(assembly, stats))
        with open(stats) as handle:
            data = replace_nas(json.load(handle))
        return data

    def get_version_info(self, assembly):
        dir_name = os.path.dirname(assembly)
        version_info = os.path.join(dir_name, "version_info.txt")
        if not os.path.exists(version_info):
            raise MetadataError("No version file found for {0} at {1}".format(assembly, version_info))
        with open(version_info) as handle:
            data = handle.readlines()
        version = data[1].strip().rstrip()
        return version
        
    def add_default_config(self, attributes, assembly):
        config = self.get_config(assembly)
        with open(config) as handle:
            data = replace_nas(json.load(handle))
        attributes["config"] = replace_nas(data)

    def add_default_hirise_iter(self, attributes, assembly):
        basename = os.path.basename(assembly)
        if basename.startswith("hirise"):
            if basename.endswith(".gz"):
                base = os.path.splitext(base)
            no_ext = os.path.splitext(basename)[:-1]
            attributes["hirise_iter"] = ''.join(no_ext)
        else:
            raise MetadataError("Can't determine hirise iter of: {0}".format(assembly))
        
    def add_default_shotgun_ids(self, attributes, assembly):
        config = self.get_config(assembly)
        with open(config) as config_handle:
            data = replace_nas(json.load(config_handle))
        try:
            shotgun_bam = data["shotgun_bam"]
        except KeyError:
            if not "nodepthscreen" in data:
                raise MetadataError("Could not figure out shotgun bam for {0}".format(assembly))
            else:
                return

        md5sum = get_md5sum(shotgun_bam)
        num = self.db.bams.count({"md5sum": md5sum})
        if num != 1:
            raise MetadataError("Should be exactly one bam matching {0}, not {1}".format(md5sum, num))
        shotgun_id = list(self.db.bams.find({"md5sum": md5sum}))[0]["_id"]
        attributes["shotgun_bam_ids"] = shotgun_id

    def add_default_chicago_ids(self, attributes, assembly):
        config = self.get_config(assembly)
        with open(config) as config_handle:
            data = replace_nas(json.load(config_handle))
        try:
            chicago_bams = data["bam_files"]
        except KeyError:
            raise MetadataError("Could not figure out chicago bams for {0}".format(assembly))

        chicago_ids = []
        for library, chicago_bam in chicago_bams.items():
            md5sum = get_md5sum(chicago_bam)
            num = self.db.bams.count({"md5sum": md5sum})
            if num != 1:
                raise MetadataError("Should be exactly one bam matching {0}, not {1}".format(md5sum, num))
            chicago_id = list(self.db.bams.find({"md5sum": md5sum}))[0]["_id"]
            chicago_ids.append(chicago_id)
        attributes["chicago_bam_ids"] = chicago_ids

    def add_default_input_assembly_id(self, attributes, assembly):
        config = self.get_config(assembly)
        with open(config) as config_handle:
            data = replace_nas(json.load(config_handle))
        try:
            input_assembly = data["contigs"]
        except KeyError:
            raise MetadataError("Could not figure out contigs for {0}".format(assembly))

        md5sum = get_md5sum(input_assembly)
        num = self.db.assemblies.count({"md5sum": md5sum})
        if num != 1:
            raise MetadataError("Should be exactly one bam matching {0}, not {1}".format(md5sum, num))
        assembly_id = self.db.assemblies.find_one({"md5sum": md5sum})["_id"]
        attributes["input_assembly_id"] = assembly_id

    def add_default_nx(self, attributes, assembly):
        attributes["nx_stats"] = list(NX.get_NX(assembly))

    def add_md5sum(self, attributes, assembly):
        md5sum = get_md5sum(assembly)
        attributes["md5sum"] = md5sum

    def add_default_assembly_name(self, attributes, assembly):
        attributes["name"] = self.get_assembly_name(attributes, assembly)

    def get_assembly_name(self, attributes, assembly):
        if attributes["subcategory"] == "hirise":
            customer_id = self.get_id(assembly)
            customer_id = str(customer_id)
            project_name = attributes["project"]
            name =   project_name+"_"+datetime.datetime.fromtimestamp(os.path.getctime(assembly)).strftime("%d%b%Y")+"_"+customer_id
        else:
            base = os.path.basename(assembly)
            if base.endswith(".gz"):
                base = os.path.splitext(base)
            no_ext = os.path.splitext(base)
            name = ''.join(no_ext[:-1])
        return name

    def get_id(self, fastafile):
        if fastafile.endswith(".gz"):
            open_with = gzip.open
            mode = "rt"
        else:
            open_with = open
            mode = "r"
        for line in open_with(fastafile, mode):
            if line.startswith(">"):
                m=re.match(">Sc(.*)_",line)
                if m:
                    return m.group(1)
                return

    def check_gzip(self, assembly):
        if assembly.endswith(".gz"):
            print(failure(
                "Assembly {0} is gzipped. This will cause"
                "problems with snap index building. Use --force"
                "to continue or decompress the assembly".format(assembly)))
            sys.exit(2)

    def add_default_date(self, attributes):
        ticket = attributes["jira_ticket"]
        if ticket:
            date = self.jira_interface.get_date(ticket)
        else:
            date = datetime.datetime.now().isoformat()
            attributes["date"] = date

    def add_default_uid(self, attributes):
        uid = self.sdb_interface.create_uid()
        attributes["uid"] = uid

    def add_default_description(self, attributes):
        ticket = attributes["jira_ticket"]
        if ticket:
            description = self.jira_interface.get_description(ticket)
            attributes["description"] = description

    def add_default_s3_key(self, attributes, assembly):
        project_s3_key = self.project["s3_key"]
        assembly_name = attributes["name"]
        if assembly.endswith(".gz"):
            file_name = assembly_name + ".fasta.gz"
        else:
            file_name = assembly_name + ".fasta"
        s3_key = os.path.join(project_s3_key, "results", assembly_name, file_name)
        attributes["s3_key"] = s3_key


class Read:

    def __init__(self):
        self.jira_interface = JiraInterface()
        self.junctions = set(["GATCGATC", "AATTAATT"])
        self.client = pymongo.MongoClient("192.168.89.249", 27017)
        self.db = self.client.production
        
    def check_are_paired(self, fastq1, fastq2):
        
        read_num1 = fastq1.attributes["read_number"]
        read_num2 = fastq2.attributes["read_number"]
        if read_num1 != read_num2:
            raise MetadataError("Number of reads do not match {0} and {1}".format(read_num1, read_num2))

        for read1, read2 in FastqHelper.read_paired_lines(fastq1.fastq, fastq2.fastq):
            if read1.name != read2.name:
                if read1.name[:-1] != read2.name[:-1]:
                    raise MetadataError("Read {0} does not match pair {1}".format(read1.name, read2.name))
        
    def add_mate(self, fastq2):
        self.attributes["pair"] = fastq2.attributes["_id"]

    def set_params(self, fastq, subcategory=None, jira_ticket=None, library=None, no_ticket=None, project_name=None, read_type=None, skip_run_id=None, md5sum=None):
        if not jira_ticket and not no_ticket:
            jira_ticket = self.guess_ticket(fastq)

        if not (jira_ticket or library):
            raise MetadataError("Needs a JIRA ticket or library name: {0}".format(fastq))

        if not (jira_ticket or project_name):
            raise MetadataError("Needs a JIRA ticket or project name: {0}".format(fastq))

        if not (jira_ticket or subcategory):
            raise MetadataError("Needs a JIRA ticket or library subcategory (e.g. Chicago Type): {0}".format(fastq))

        self.fastq = fastq

        new_attributes = {
            "jira_ticket": jira_ticket,
            "subcategory": subcategory,
            "library": (library or jira_ticket),
            "name": None,
            "run_id": None,
            "machine_id": None,
            "md5sum": md5sum, 
            "project_id": None,
            "project": project_name,
            "s3_key": None,
            "read_number": None,
            "read_length": None,
            "pair_type": read_type,
            "junction": None,
            "date": None,
            "description": None,
            "dt": "read"
            }

        self.set_defaults(fastq, new_attributes, no_ticket, skip_run_id)
        self.attributes = new_attributes


    def set_defaults(self, fastq, attributes, no_ticket, skip_run_id):

        if not attributes["jira_ticket"] and not no_ticket:
            self.set_default_ticket(fastq, attributes)

        #if attributes["jira_ticket"]:
        #    self.parent = self.jira_interface.get_CP_by_ticket(attributes["jira_ticket"])
            
        self.set_fastq_format(attributes, fastq)

        #if not attributes["description"] and attributes["jira_ticket"]:
        #    self.set_default_description(attributes)
        
        #if not attributes["subcategory"] and attributes["jira_ticket"]:
        #    self.set_default_subcategory(attributes)
            
        #if not attributes["library"] and attributes["jira_ticket"]:
         #   self.set_default_library(attributes)

        if not attributes["project"] and attributes["jira_ticket"]:
            self.set_default_project(attributes)

        if not attributes["project_id"]:
            self.set_default_project_id(attributes)

        #if not attributes["name"]:
        #    self.set_default_name(attributes)

        if not attributes["md5sum"]:
            self.set_default_md5sum(attributes, fastq)

        #if not attributes["s3_key"]:
        #    self.set_default_s3_key(attributes, fastq)

        if not (attributes["read_number"] and
                attributes["read_length"] and 
                attributes["junction"]):
            read_number, read_length, junction = self.read_fastq_file(fastq)

        if not attributes["read_number"]:
            attributes["read_number"] = read_number

        if not attributes["read_length"]:
            attributes["read_length"] = read_length

        if not attributes["junction"] and attributes["subcategory"] == "Chicago Type":
            attributes["junction"] = junction
    
        if not attributes["date"]:
            self.set_default_date(attributes)
    
    def set_default_description(self, attributes):
        ticket = attributes["jira_ticket"]
        description = self.jira_interface.get_description(ticket)
        attributes["description"] = description

    def upload_fastq(self):
        s3_upload(self.fastq, self.attributes["s3_key"], self.attributes["md5sum"])

    def is_only_md5sum(self, md5sum=None):
        if not md5sum:
            md5sum = self.attributes["md5sum"]
        if self.client.production.reads.count({"md5sum": md5sum}) == 0:
            return True

    def is_only_s3_key(self):
        if self.client.production.reads.count({"s3_key": self.attributes["s3_key"]}) == 0:
            return True

    def put_attributes(self, dryrun=False):
        if not self.is_only_md5sum():
            raise ObjectExistsError("Read already exists with md5sum {0}".format(self.attributes["md5sum"]))
        if not self.is_only_s3_key():
            raise ObjectExistsError("Read already exists with s3_key {0}".format(self.attributes["s3_key"]))

        attributes = self.attributes

        if dryrun:
            pprint(attributes)
            return
        
        self.db.reads.insert_one(attributes)

    def show_attributes(self, attributes=None):
        if attributes is None and self.attributes:
            pprint(self.attributes)
        else:
            pprint(attributes)

    def set_default_date(self, attributes):
        creation_time = os.path.getctime(self.fastq)
        date = datetime.datetime.fromtimestamp(creation_time)
        attributes["date"] = date

    def read_fastq_file(self, fastq):
        junction_counts = Counter(self.junctions)
        read_lengths = 0
        for read_number, read in enumerate(FastqHelper.read_lines(fastq)):
            if len(read.sequence) > read_lengths:
                read_lengths = len(read.sequence)
            for junction in self.junctions:
                if junction in read.sequence:
                    junction_counts[junction] += 1
        return read_number + 1, read_lengths, max(junction_counts)

    def set_default_s3_key(self, attributes, fastq):
        project_name = attributes["project"]
        num = self.db.projects.count({"project": project_name})
        items = list(self.db.projects.find({"project": project_name}))
        if num != 1:
            raise MetadataError("Should be exactly one {0}, not {2}: {1}".format(project_ticket, items, num))
        item = list(items)[0]
        parent_s3_key = item["s3_key"]
        dataset_attr = self.db.datasets.find_one({"_id": attributes["dataset_id"]})
        dataset_name = dataset_attr["name"]
        library_name = self.db.libraries.find_one({"_id": dataset_attr["library_id"]})["name"]
        fastq_name = "_".join([library_name, dataset_name])
        file_name = os.path.basename(fastq)
        print(parent_s3_key, fastq_name, file_name)
        s3_key = os.path.join(parent_s3_key, "data", fastq_name, file_name)
        self.attributes["s3_key"] = s3_key
        print(s3_key)

    def set_default_project_id(self, attributes):
        project_name = attributes["project"]
        num = self.db.projects.count({"project": project_name})
        items = list(self.db.projects.find({"project": project_name}))
        if num != 1:
            raise MetadataError("Should be exactly one {0}, not {2}: {1}".format(project_name, items, num))
        item = list(items)[0]
        attributes["project_id"] = item["_id"]
        
    def set_default_project(self, attributes):
        project_ticket = self.parent["Parent"]
        num = self.db.projects.count({"jira_ticket": project_ticket})
        items = list(self.db.projects.find({"jira_ticket": project_ticket}))
        if num != 1:
            raise MetadataError("Should be exactly one {0}, not {2}: {1}".format(project_ticket, items, num))
        item = list(items)[0]
        attributes["project"] = item["project"]

    def set_default_md5sum(self, attributes, fastq):
        md5sum = get_md5sum(fastq)
        attributes["md5sum"] = md5sum

    def set_fastq_format(self, attributes, fastq):
        illumina_1_name = r"[@]?[-_A-z0-9]+:[0-9]+:[0-9]+:[0-9]+:+[0-9]+" 
        illumina_1_comment = r"\s#[0-9]+/[12]"
        illumina_2_name = r"[@]?[-_A-z0-9]+:[0-9]+:[-_A-z0-9]+:[0-9]+:[0-9]+:[0-9]+:[0-9]+" 
        illumina_2_comment = r"\s[0-9]+:[YN]:[0-9]+:"
        illumina_3_name = r"[@]?[-_A-z0-9]+:[0-9]+:[0-9]+:[0-9]+:+[0-9]+/[12]" 
        sra_1 = r"[@]?SR[AR][0-9]+\.[0-9]\s" + illumina_1_name
        sra_2 = r"[@]?SR[AR][0-9]+\.[0-9]\s" + illumina_2_name
        
        for key in ["machine_id", "flowcell_id", "pair_type", "run_id", "flowcell_lane"]:
            if key in attributes:
                if attributes[key] == None:
                    attributes.pop(key)
        
        for read in FastqHelper.read_lines(fastq):
            line = read.first_line
            if re.match(illumina_1_name + illumina_1_comment, line):
                attributes.setdefault("machine_id", line.split(":")[0].strip("@"))
                attributes.setdefault("flowcell_id", line.split(":")[1])
                attributes.setdefault("pair_type", int(line.split("/")[1][0]))
            elif re.match(illumina_2_name + illumina_2_comment, line):
                attributes.setdefault("machine_id", line.split(":")[0].strip("@"))
                attributes.setdefault("run_id", int(line.split(":")[1]))
                attributes.setdefault("flowcell_id", line.split(":")[2])
                attributes.setdefault("flowcell_lane", int(line.split(":")[3]))
                attributes.setdefault("pair_type", int(line.split()[1][0]))
            if re.match(illumina_2_name, line):
                attributes.setdefault("machine_id", line.split(":")[0].strip("@"))
                attributes.setdefault("run_id", int(line.split(":")[1]))
                attributes.setdefault("flowcell_id", line.split(":")[2])
                attributes.setdefault("flowcell_lane", int(line.split(":")[3]))
            elif re.match(sra_1, line):
                attributes.setdefault("machine_id", line.split()[1].split(":")[0].strip("@"))
                attributes.setdefault("flowcell_id", line.split()[1].split(":")[1])
            elif re.match(sra_2, line):
                attributes.setdefault("machine_id", line.split()[1].split(":")[0].strip("@"))
                attributes.setdefault("run_id", int(line.split()[1].split(":")[1]))
                attributes.setdefault("flowcell_id", line.split()[1].split(":")[2])
                attributes.setdefault("flowcell_lane", int(line.split()[1].split(":")[3]))
            elif re.match(illumina_3_name, line):
                attributes.setdefault("machine_id", line.split(":")[0].strip("@"))
                attributes.setdefault("flowcell_id", line.split(":")[1])
                attributes.setdefault("pair_type", int(line.split("/")[1][0]))
            else:
                raise MetadataError("Cannot identify format of {0}".format(line))
            break

    def get_dataset(self, fastq, project, library):
        dataset = self.get_dataset_name(fastq, project)
        library_name = (self.attributes["jira_ticket"] or self.attributes["library"])
        if not library_name:
            raise MetadataError("Needs a JIRA ticket or library name: {0}".format(self.fastq))
        library = self.get_library(library)
        if library:
            count = self.db.datasets.count({"project": project, "name": dataset, "library_id": library["_id"]})
        else:
            return
        if count > 1:
            raise MetadataError("Should only be one dataset with {0} and {1}, not {2}".format(project, dataset, count))
        elif count == 1:
            return self.db.datasets.find_one({"project": project, "name": dataset, "library_id": library["_id"]})

    def get_dataset_name(self, fastq, project):
        d = {}
        self.set_fastq_format(d, fastq)
        self.set_default_name(d)
        dataset = d["name"]
        return dataset

    def get_library(self, library):
        if library:
            target = {"name": library, "project": self.attributes["project"]}
        elif self.attributes["jira_ticket"]: 
            target = {"jira_ticket": self.attributes["jira_ticket"], "project": self.attributes["project"]}
        else:
            raise MetadataError("Needs a JIRA ticket or library name: {0}".format(self.fastq))

        count = self.db.libraries.count(target)
        if count > 1:
            raise MetadataError("Should only be one library with {0} , not {1}".format(target, count))
        elif count == 1:
            return self.db.libraries.find_one(target)
            
    def set_default_name(self, attributes):
        machine_id = attributes["machine_id"]
        run_id     = attributes.get("run_id", "no_run")
        if machine_id and run_id: 
            name = "_".join([str(machine_id), str(run_id)])
        else:
            name = os.path.basename(self.fastq)
        attributes["name"] = name
            
    def set_default_library(self, attributes):
        attributes["library"] = attributes["jira_ticket"]

    def set_default_ticket(self, fastq, attributes):
        jira_ticket = self.guess_ticket(fastq)            
        attributes["jira_ticket"] = jira_ticket

    def set_default_subcategory(self, attributes):
        attributes["subcategory"] = self.parent["Type"]

    def guess_ticket(self, fastq):
        basename = os.path.basename(fastq)
        guess = basename.split("_")[0]
        ticket = self.try_ticket(guess)
        if not ticket:
            dirname = os.path.dirname(fastq)
            dir_base = os.path.basename(dirname)
            guess = dir_base.split("_")[0]
            if guess.startswith("CP") and "-" not in guess:
                guess = guess[:2] + "-" + guess[2:]
            ticket = self.try_ticket(guess)
        return ticket

    def try_ticket(self, guess):

        try:
            issue = self.jira_interface.get_CP_by_ticket(guess)
            return issue["Key"]
        except subprocess.CalledProcessError:
            return

class Bam:

    def __init__(self):
        self.client = pymongo.MongoClient("192.168.89.249", 27017)
        self.db = self.client.production
        self.jira_interface = JiraInterface()

    def set_params(self, bam, md5sum=None):
        bam = os.path.expanduser(bam)
        self.bam = os.path.abspath(bam)
        self.assembly = self.get_assembly(bam)
        self.reads = self.get_reads(bam)
        self.project = self.get_project(bam)

        new_attributes = {
            "subcategory": None,
            "config": None,
            "name": None,
            "project": None,
            "assembly_id": None,
            "read_id": None,
            "md5sum": md5sum,
            "s3_key": None,
            "project_id": None,
            "description": None,
            "date": None,
            "version_info": None,
            "results": None,
            "dt": "bam"
            }

        self.set_defaults(bam, new_attributes)
        self.attributes = new_attributes

    def is_only_md5sum(self, md5sum=None):
        if not md5sum:
            md5sum = self.attributes["md5sum"]
        if self.db.bams.find({"md5sum": md5sum}).count() == 0:
            return True
            
    def set_defaults(self, bam, attributes):
        if not attributes["md5sum"]:
            self.set_default_md5sum(attributes, bam)
        if not attributes["date"]:
            self.set_default_date(attributes)
        if not attributes["assembly_id"]:
            self.set_default_assembly(attributes, bam)
        if not attributes["read_id"]:
            self.set_default_reads(attributes, bam)
        if not attributes["project"]:
            self.set_default_project(attributes)
        if not attributes["project_id"]:
            self.set_default_project_id(attributes)
        if not attributes["name"]:
            self.set_default_name(attributes)
        if not attributes["subcategory"]:
            self.set_default_subcategory(attributes)
        if not attributes["s3_key"]:
            self.set_default_s3_key(attributes, bam)
        if not attributes["results"]:
            self.set_default_results(attributes, bam)

    def set_default_results(self, attributes, bam):
        results = self.get_results(bam)
        attributes["results"] = results

    def get_results(self, bam):
        dir_name = os.path.dirname(bam)
        if os.path.basename(dir_name) == "aln":
            results = os.path.join(dir_name, "../results.json")
        else:
            results = os.path.join(dir_name, "results.json")

        if not os.path.exists(results):
            raise MetadataError("No results file found for {0} at {1}".format(bam, results))
        with open(results) as handle:
            data = replace_nas(json.load(handle))
        return data

    def set_default_project_id(self, attributes):
        project_name = attributes["project"]
        num = self.db.projects.count({"project": project_name})
        item = self.db.projects.find_one({"project": project_name})
        if num != 1:
            raise MetadataError("Should be exactly one {0}, not {2}: {1}".format(project_name, items, num))
        attributes["project_id"] = item["_id"]

    def put_attributes(self, dryrun=False):
        attributes = self.attributes
        attributes = bson_replace(attributes)
        if dryrun:
            pprint(attributes)
            return
            
        self.db.bams.insert_one(attributes)

    def upload_bam(self):
        s3_upload(self.bam, self.attributes["s3_key"], self.attributes["md5sum"])

    def get_project(self, attributes):
        project = self.assembly["project"]
        num = self.db.projects.count({"project": project})
        if num != 1:
            raise MetadataError("Should be 1 project {0}, not {1}".format(project, num))
        parent = list(self.db.projects.find({"project": project}))[0]
        return parent
        

    def set_default_project(self, attributes):
        attributes["project"] = self.assembly["project"]
        
    def set_default_name(self, attributes):
        names = set(r["name"] for r in self.reads)
        if len(names) == 1:
            name = list(names)[0]
        else:
            libraries = set(r["libraries"] for r in self.reads)
            name = "_".join(sorted(libraries))
        dirname = os.path.dirname(self.bam)
        dir_dirname = os.path.dirname(dirname)
        if os.path.exists(os.path.join(dir_dirname, "config.json")):
            name = os.path.basename(dir_dirname)

        elif os.path.exists(os.path.join(dirname, 'config.json')):
            name = os.path.basename(dirname)

        else:
            raise MetadataError("Cannot decide directory name for {0}".format(self.bam))
        attributes["name"] = name

    def set_default_subcategory(self, attributes):
        subcategories = set(r["subcategory"] for r in self.reads)
        if len(subcategories) == 1:
            subcat = list(subcategories)[0]
        else:
            raise MetadataError("Should only be one type of read per bam file: {0}".format(subcategories))
        attributes["subcategory"] = subcat

    def set_default_s3_key(self, attributes, bam):
        name = attributes["name"]
        parent_s3_key = self.project["s3_key"]
        file_name = os.path.basename(bam)
        s3_key = os.path.join(parent_s3_key, "analysis", "local", name, file_name)
        attributes["s3_key"] = s3_key

        
    def set_default_md5sum(self, attributes, bam):
        md5sum = get_md5sum(bam)
        attributes["md5sum"] = md5sum

    def show_attributes(self, attributes=None):
        if attributes is None and self.attributes:
            pprint(self.attributes)
        else:
            pprint(attributes)

    def set_default_date(self, attributes):
        creation_time = os.path.getctime(self.bam)
        date = datetime.datetime.fromtimestamp(creation_time)
        attributes["date"] = date

    def set_default_reads(self, attributes, bam):
        config = self.get_config(bam)
        reads_uid = self.get_reads_id(config)
        attributes["read_id"] = reads_uid

    def set_default_assembly(self, attributes, bam):
        config = self.get_config(bam)
        assembly_id = self.get_assembly_id(config)
        attributes["assembly_id"] = assembly_id

    def set_default_config(self, bam):
        config = self.get_config(bam)
        with open(config) as handle:
            data = replace_nas(json.load(handle))
        attributes["config"] = data 
    
    def get_config(self, bam):
        dir_path = os.path.dirname(bam)
        if os.path.basename(dir_path) == "aln":
            expected_config = os.path.join(dir_path, "../config.json")
        else:
            expected_config = os.path.join(dir_path, "config.json")

        if not os.path.exists(expected_config):
            raise MetadataError("No config file found for {0} at {1}".format(bam, expected_config))

        return expected_config

    def get_assembly(self, bam):
        config = self.get_config(bam)
        assembly_id = self.get_assembly_id(config)
        with open(config) as config_handle:
            data = replace_nas(json.load(config_handle))
        assembly = self.get_assembly_config(data)
        assembly_md5sum = get_md5sum(assembly)
        num = self.db.assemblies.count({"md5sum": assembly_md5sum})
        results = self.db.assemblies.find({"md5sum": assembly_md5sum})
        if num != 1:
            raise MetadataError("Should be exactly one assembly matching {0}, not {1}".format(assembly_md5sum, num))
        return results[0]


    def get_assembly_id(self, config):
        with open(config) as config_handle:
            data = replace_nas(json.load(config_handle))
        assembly = self.get_assembly_config(data)
        assembly_md5sum = get_md5sum(assembly)
        num = self.db.assemblies.count({"md5sum": assembly_md5sum})
        results = self.db.assemblies.find({"md5sum": assembly_md5sum})
        if num != 1:
            raise MetadataError("Should be exactly one assembly matching {0}, not {1}".format(assembly_md5sum, num))
        assembly_id = results[0]["_id"]
        return assembly_id
        
    def get_assembly_config(self, data):
        try:
            return data["contigs"]
        except KeyError:
            raise MetadataError("No contigs found in {0}".format(data))

    def get_reads(self, bam):
        config = self.get_config(bam)
        with open(config) as config_handle:
            data = replace_nas(json.load(config_handle))
        reads = self.get_reads_config(data)
        reads_md5sum = [get_md5sum(i) for i in reads]

        for md5sum in reads_md5sum:
            num = self.db.reads.count({"md5sum": md5sum})
            if num != 1:
                raise MetadataError("Should be exactly 1 read with md5sum {0}, not {1}".format(md5sum, num))
        return [list(self.db.reads.find({"md5sum": i}))[0] for i in reads_md5sum]
        
    def get_reads_id(self, config):
        with open(config) as config_handle:
            data = replace_nas(json.load(config_handle))
        reads = self.get_reads_config(data)
        reads_md5sum = [get_md5sum(i) for i in reads]
        reads_uids = [self.db.reads.find({"md5sum": i})[0]["_id"] for i in reads_md5sum]
        return reads_uids

    def get_reads_config(self, data):
        try:
            reads_1 = data["raw_regex_r1"]
            reads_2 = data["raw_regex_r2"]
        except KeyError:
            try:
                reads_1 = data["f1_glob"]
                reads_2 = data["f2_glob"]
            except KeyError:
                raise MetadataError("No reads found in config {0}".format(data))

        raw_r1 = sorted(itertools.chain.from_iterable(glob.glob(i) for i in reads_1.split('|')))
        raw_r2 = sorted(itertools.chain.from_iterable(glob.glob(i) for i in reads_2.split('|')))

        if len(raw_r1) != len(raw_r2):
            raise MetadataError("number of R1 != R2 : {0},  {1}".format(raw_regex_r1, raw_regex_r2))
        if len(raw_r1) == 0:
            raise MetadataError("No reads present: {0}".format(reads_1))

        for i in raw_r1:
            if i in raw_r2:
                raise MetadataError("Same read file present in r1 is in r2: {0}".format(i))
        for i in raw_r2:
            if i in raw_r1:
                raise MetadataError("Same read file present in r2 is in r1: {0}".format(i))

        return raw_r1 + raw_r2

class JiraInterface:

    def __init__(self):
        pass

    def get_customer_ticket(self, project):
        jira_issues = self.get_CP_by_name(project)
        num_issues = len(jira_issues)
        
        if num_issues == 1:
            return jira_issues[0]["Key"]
        elif num_issues == 0:
            msg = "No issues match project {0}".format(project)
        else:
            issues = '\n'.join([issue["Summary"] for issue in jira_issues])
            msg = "{1} issues match project {0}:\n{2}".format(project, num_issues, issues)
        raise MetadataError(msg)
        
    def get_CP_by_name(self, project):
        jira_search = subprocess.check_output([
            "jira", "--action", "getIssueList", "--jql",
            """project="Customer Projects" and type="Customer Project" 
            and summary ~ '{0}' """.format(project)], universal_newlines=True)
        jira_reader = self.convert_to_issues(jira_search)
        return jira_reader

    def get_CP_by_ticket(self, ticket):
        jira_search = subprocess.check_output(
            ["jira", "--action", "getIssueList", "--jql", 
             "issue='{0}'".format(ticket)], 
            universal_newlines=True)
        
        jira_issues = self.convert_to_issues(jira_search)
        msg = "More than one issue with {0}: {1}".format(ticket, jira_issues)
        if len(jira_issues) != 1:
            raise MetadataError(msg)
        return jira_issues[0]

    def get_description(self, ticket):
        jira_issues = self.get_CP_by_ticket(ticket)
        description = jira_issues["Summary"]
        return description

    def get_date(self, ticket):
        jira_issues = self.get_CP_by_ticket(ticket)
        date = jira_issues["Created"]
        return date

    def convert_to_issues(self, jira_output):
        jira_filelike = io.StringIO(jira_output)
        jira_filelike.readline()
        jira_reader = csv.DictReader(jira_filelike)
        jira_issues = list(jira_reader)
        return jira_issues


class MetadataError(Exception):
    pass

class ObjectExistsError(Exception):
    pass



