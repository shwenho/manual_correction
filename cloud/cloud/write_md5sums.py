#!/usr/bin/env python3


import argh
import hashlib
import os
import gzip

@argh.arg("-f", "--files", nargs="+")
def main(input_dir=".", output=None, files=None):
    input_dir = os.path.abspath(input_dir)
    if not output:
        output = os.path.join(input_dir, "md5sum.txt")
    if not os.path.isdir(input_dir):
        return
    with open(output, 'w') as handle:
        if not files:
            files = [os.path.join(input_dir, i) for i in os.listdir(input_dir)]
        for entry in files:

            entry = os.path.join(input_dir, entry)
            if os.path.isfile(entry):
                if entry.endswith(".txt"):
                    continue
                md5sum = get_md5sum(entry)
                basename = os.path.basename(entry)
                if basename.endswith(".gz"):
                    basename = os.path.splitext(basename)[0]
                print(md5sum, basename, file=handle)

def get_md5sum(afile, block_size=256*128):
    open_type = open
    if afile.endswith(".gz"):
        open_type = gzip.open

    hasher = hashlib.md5()
    with open_type(afile, 'rb') as fh:
        buf = fh.read(block_size)
        while len(buf) > 0:
            hasher.update(buf)
            buf = fh.read(block_size)
    return hasher.hexdigest()


if __name__ == "__main__":
    argh.dispatch_command(main)
