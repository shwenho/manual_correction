#!/usr/bin/env python3

import matplotlib.pyplot as plt
import argh
import networkx as nx
from pymongo import MongoClient
from pprint import pprint


class DBObject:
    def __init__(self, db_obj, db):
        self.db = db
        try:
            self.db_type  = db_obj["dt_type"]
        except KeyError:
            pprint(db_obj)
            sys.exit(2)
        self.db_obj   = db_obj
        self.name     = self.get_name(db_obj, db_type)

    def get_name(self, db_obj):
        db_type = self.db_type
        if db_type == "bam":
            try:
                coverage = "{0:.2f}".format(db_obj["results"]["bam"]["stats"]["raw_coverage"]) + "X"
            except KeyError:
                coverage = ""
            name = db_obj["name"]  + "\n" + str(db_obj["_id"]) + "\n" + coverage
        elif db_type == "assembly":
            name = db_obj["subcategory"]+ " " + db_obj.get("hirise_iter", "") + "\n" + "N50:" + "{:,}".format(db_obj["nx_stats"][49][1]) + "\n"+ db_obj["name"]  + "\n" + str(db_obj["_id"])
        elif db_type == "library":
            name = db_obj["name"] + "\n" +str(db_obj["_id"])
        elif db_type == "dataset":
            dt_name = db_obj["name"]
            dt_id = str(db_obj["_id"])
            dt_read_num = "read number: " + "{:,}".format(count_read_pairs_dataset(db_obj, self.db))
            name = "\n".join([dt_name, dt_id, dt_read_num])
        elif db_type == "project":
            name = db_obj["project"] + "\n" + str(db_obj["_id"])
        return db_type + ":\n" + name
        
    def slurp_up(self, G, db):
        G.add_node(self.name)
        if self.db_type   == "project":
            return

        elif self.db_type == "assembly":

            if self.db_obj["subcategory"] != "hirise":
                project = get_project(db, self.db_obj["project"])
                project_node = DBObject(project, db)
                project_node.slurp_up(G, db)
                G.add_edge(project_node.name, self.name)

            else:
                bams = self.db_obj["chicago_bam_ids"] 
                shotgun = self.db_obj.get("shotgun_bam_ids", None)
                if shotgun:
                    bams = bams + [shotgun]
                hirise_run = "hirise_run\n" + str(self.db_obj["hirise_run"])
                G.add_node(hirise_run)
                G.add_edge(hirise_run, self.name)
                for bam_id in bams:
                    bam = db.bams.find_one({"_id": bam_id})
                    bam_node = DBObject(bam, db)
                    bam_node.slurp_up(G, db)
                    G.add_edge(bam_node.name, hirise_run)

                input_assembly_id = self.db_obj["input_assembly_id"]
                input_assembly = db.assemblies.find_one({"_id": input_assembly_id})
                input_assembly_node = DBObject(input_assembly, db)
                input_assembly_node.slurp_up(G, db)
                G.add_edge(input_assembly_node.name, hirise_run)
            

        elif self.db_type == "bam":
            for read_id in self.db_obj["read_id"]:
                read = db.reads.find_one({"_id": read_id})
                dataset = db.datasets.find_one({"_id": read["dataset_id"]})
                dataset_node = DBObject(dataset, db)
                dataset_node.slurp_up(G, db)
                G.add_edge(dataset_node.name, self.name)

            assembly_id = self.db_obj["assembly_id"]
            assembly = db.assemblies.find_one({"_id": assembly_id})
            assembly_node = DBObject(assembly, db)
            G.add_edge(assembly_node.name, self.name)
            assembly_node.slurp_up(G, db)

        elif self.db_type == "dataset":
            library_id = self.db_obj["library_id"]
            library = db.libraries.find_one({"_id": library_id})
            library_node = DBObject(library, db)
            library_node.slurp_up(G, db)
            G.add_edge(library_node.name, self.name)

        elif self.db_type == "library":
            project = get_project(db, self.db_obj["project"])
            project_node = DBObject(project, db)
            project_node.slurp_up(G, db)
            G.add_edge(project_node.name, self.name)

            

def node_name(name, node_type):
    return node_type + "\n" + name

def main(project_name, output="/dev/stdout"):
    client = MongoClient("192.168.89.249", 27017)
    db = client.production
    G = nx.DiGraph()
    
    project = get_project(db, project_name)
    project_id = project["_id"]
    project_node = DBObject(project, db)
    G.add_node(project_node.name)

    for assembly in get_assemblies(db, project_id):
        assembly_name = assembly["name"]
        assembly_node = DBObject(assembly, db)
        assembly_node.slurp_up(G, db)

    for dataset in get_datasets(db, project_id):

        dataset = DBObject(dataset, db)
        dataset.slurp_up(G, db)

    for bam in get_bams(db, project_name):
        bam_node = DBObject(bam, db)
        bam_node.slurp_up(G, db)

    nx.write_dot(G, output)

def count_read_pairs_dataset(dataset, db):
    num_reads = 0
    reads = db.reads.find({"dataset_id": dataset["_id"], "pair_type": 1})
    if reads:
        for read in reads:
            num_reads += read["read_number"]
    return num_reads

def get_bams(db, project):
    bams = list(db.bams.find({"project": project}))        
    return bams

def get_datasets(db, project_id):
    datasets = db.datasets.find({"project_id": project_id})
    return datasets

def get_libraries(db, project_id):
    reads = get_reads(db, project_id)
    libraries = set()
    for read in reads:
        libraries.add(read["library"])
    return list(libraries)
        
def get_reads(db, project_id):
    reads = list(db.reads.find({"project_id": project_id}))
    return reads

def get_project(db, project_name):
    project = db.projects.find_one({"project": project_name})
    return project

def get_assemblies(db, project_id):
    assemblies = db.assemblies.find({"project_id": project_id})
    return assemblies
                      

if __name__ == "__main__":
    argh.dispatch_command(main)
