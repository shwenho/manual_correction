#!/usr/bin/env python3

from invoke import task, run, Collection, ctask
import os
import re


@ctask
def docker_init(ctx,image=None):
    image = image or ctx.image
    run("eval `aws ecr get-login --region=us-east-1`")
    if not image: image = "production"
    run("docker pull 431382570195.dkr.ecr.us-east-1.amazonaws.com/%s" % (image))
    if image == "production":
        run("docker-compose run -d production init")
    
@ctask
def s3_sync(ctx, project=None):
    project = project or ctx.project
    mount_point = os.path.join("/mnt", "projects")
    project_point = os.path.join(mount_point, project)
    s3_dir = os.path.join("s3://dovetailgenomics", "projects", project)
    run("aws s3 sync " + s3_dir + " " + project_point)

@ctask
def update_repo(ctx, branch=None, git_user=None, git_password=None, install_packages=None):    
    git_user = git_user or ctx.git_user
    git_password = git_password or ctx.git_password
    git_branch = branch or ctx.branch
    packages = install_packages or ctx.install_packages

    packages_str = " ".join(packages)
    run("""git config user.name {1};
    rm dovetail -rf;
    git clone https://{1}:{3}@bitbucket.org/dovetail-drylab/dovetail.git;
    cd dovetail;
    git checkout {0};
    git reset --hard;
    git describe;
    pip3 install -e {2}""".format(git_branch, git_user, packages_str, git_password))

@ctask
def mount_vol(ctx, project=None):
    project = project or ctx.project
    mount_point = os.path.join("/mnt", "projects")
    project_point = os.path.join(mount_point, project)
    s3_dir = os.path.join("s3://dovetailgenomics", "projects", project)
    run("sudo mkfs.ext4 /dev/xvdj")
    run("sudo mkdir -p " + mount_point)
    run("sudo mount /dev/xvdj " + mount_point)
    run("sudo mkdir -p " + project_point)
    run("sudo chmod 777 " + project_point)

@ctask
def jira_update(ctx, jira_user=None, jira_password=None):
    jira_file = "/home/ubuntu/atlassian-cli-4.5.0/jira"
    tmp_jira_file = jira_file + ".tmp"
    jira_user = jira_user or ctx.jira_user
    jira_password = jira_password or ctx.jira_password

    with open(jira_file) as f:
        data = f.readlines()
    
    with open(tmp_jira_file, 'w') as f:
        for line in data:
            line = re.sub("jira_password", jira_password, line)
            line = re.sub("jira_user", jira_user, line)
            f.write(line)
    os.rename(tmp_jira_file, jira_file)
    run("sudo chmod 777 " + jira_file)

@ctask(jira_update)
def dove_init(ctx, project=None, overwrite=None, dove_cmd=None):
    project = project or ctx.project
    mount_point = os.path.join("/mnt", "projects")
    project_point = os.path.join(mount_point, project)
    local = os.path.join(project_point, "analysis", "local")
    overwrite = overwrite or ctx.overwrite
    dove_cmd = dove_cmd or ctx.dove_cmd
    
    run("mkdir -p " +  local)    
    if dove_cmd:
        run("cd " + local + ";" + dove_cmd)
    elif overwrite:
        run("cd " + local + ";echo starting;dove-pipe init --overwrite ")

@ctask
def json_edit(ctx, project=None, replace_json=None):

    project = project or ctx.project
    mount_point = os.path.join("/mnt", "projects")
    project_point = os.path.join(mount_point, project)
    local = os.path.join(project_point, "analysis", "local")

    paths = ctx.json_replace
    replace = "/mnt/projects/"
    json = os.path.join(local, "dove.json")
    tmp_json = json + ".tmp"

    with open(json) as f:
        data = f.readlines()

    with open(tmp_json, 'w') as f:
        for line in data:
            for path in paths:
                if not path.endswith("/"):
                    path = path + "/"
                line = re.sub(path, replace, line)
            f.write(line)
    os.rename(tmp_json, json)

@ctask(update_repo, s3_sync, dove_init, json_edit)
def hirise(ctx, project=None, assembly=None):
    project = project or ctx.project
    assembly = assembly or ctx.assembly
    mount_point = os.path.join("/mnt", "projects")
    project_point = os.path.join(mount_point, project)
    local = os.path.join(project_point, "analysis", "local")
    run("cd " + local + ";chmod +x ~/run_pipeline.sh; ~/run_pipeline.sh " + assembly)

#@task
#def update_db(project_dir):
#    print(project_dir)
#    run("~/dovetail/cloud/cloud/update_sdb.py whole-project --project-dir {0} --skip-existing".format(project_dir), pty=True)

#@task(update_db)
@task
def hirise_db(project_dir, assembly, libraries=None):
    
    # project, cores, lib, assembly_name, contigs, read1, read2, junction, snap index
    libraries = libraries.split(",")
    config = os.path.join(project_dir, "project.yaml")
    print(config, project_dir)
    pa = ProjectArguments(config, project_dir)
    project = pa.data["project"]["project"]
    client = pymongo.MongoClient("192.168.89.249", 27017)
    db = client.production

    print("project: ", project)
    assembly_info = db.assemblies.find_one({"project": project, "name": assembly})
    assembly_id = assembly_info["_id"]
    print("assembly: ",assembly_info)
    if not libraries:
        libraries_info = db.libraries.find({"project": project})
    else:
        libraries_info = []
        for library in libraries:
            library_info = db.libraries.find_one({"project": project, "name": library})
            if library_info:
                libraries_info.append(library_info)
            else:
                print("NO lIBRARY", library)
    for library in libraries_info:
        print("library:", library)
        read_ids = []
        for dataset in db.datasets.find({"library_id": library["_id"]}):
            for read in db.reads.find({"dataset_id": dataset["_id"]}):
                read_ids.append(read["_id"])
        matching_bams = []
        for bam in db.bams.find({"project": project, "assembly_id": assembly_id}):
            bam_reads = bam["read_id"]
            missing = False
            for read_id in read_ids:
                if read_id not in bam_reads:
                    missing = True
            if not missing:
                matching_bams.append(bam)
        print()
        print("has", read_ids)
        print("matching bams", matching_bams)
            
    

ns = Collection(hirise, dove_init, mount_vol, update_repo, s3_sync, json_edit, jira_update,docker_init)
