#!/usr/bin/env bash
set -e
echo "project"
python3 ./update_sdb.py new-project --project-name NA12878_chr22 --no-ticket
echo "customer assembly"
python3 ./update_sdb.py new-assembly --project-name NA12878_chr22 /mnt/nas2/bigdove1/scratch1/projects/chr22/results/meraculous_lighter_k49mer/meraculous_lighter_k49mer.fa  "customer"
echo "chicago 17"
python3 ./update_sdb.py new-read-pair /mnt/nas2/bigdove1/scratch1/projects/chr22/data/chicago_017/chr22_chicago_017_TATGAGT_untrimmed_r?.fq.gz --no-ticket --library "DT019" --project "NA12878_chr22" -s "Chicago Type" 
echo "shotgun"
python3 ./update_sdb.py new-read-pair /mnt/nas2/bigdove1/scratch1/projects/chr22/data/shotgun/r?.fq.gz -s "Shotgun Type" --library "illumina_pcr_free"  --no-ticket --project NA12878_chr22  --no-validate
echo "chicago 16"
python3 ./update_sdb.py new-read-pair /mnt/nas2/bigdove1/scratch1/projects/chr22/data/chicago_016/chr22_chicago_016_GAGGTTG_untrimmed_r?.fq.gz --no-ticket --library "DT018" --project "NA12878_chr22" -s "Chicago Type"
echo "chicago 16 bam"
python3 ./update_sdb.py new-bam /mnt/nas2/bigdove1/scratch1/projects/chr22/analysis/local2/chicago_016/aln/chr22.chicago_016.snap.md.sorted.bam
echo "chicago 17 bam"
python3 ./update_sdb.py new-bam /mnt/nas2/bigdove1/scratch1/projects/chr22/analysis/local2/chicago_017/aln/chr22.chicago_017.snap.md.sorted.bam
echo "shotgun bam"
python3 ./update_sdb.py new-bam /mnt/nas2/bigdove1/scratch1/projects/chr22/analysis/local/shotgun/chr22.snap.md.sorted.bam

echo "hirise"
python3 ./update_sdb.py new-assembly /mnt/nas2/bigdove1/scratch1/projects/chr22/analysis/local2/hirise_2015-09-16/hirise_iter_broken_3.fasta  hirise  --project-name NA12878_chr22  
