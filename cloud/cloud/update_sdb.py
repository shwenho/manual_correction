#!/usr/bin/env python3

import argh
import os
import sys
from pprint import pprint
import NX
import gzip
from bson.objectid import ObjectId
from mongo_db import Project
from mongo_db import Assembly
from mongo_db import MetadataError
from mongo_db import Read
from mongo_db import Bam
from mongo_db import get_md5sum
from mongo_db import Library
from mongo_db import Dataset
import yaml
import itertools
import re
import graph_db
import subprocess
from utils import FastqHelper

def get_id(fastafile):
    if fastafile.endswith(".gz"):
        open_with = gzip.open
        mode = "rt"
    else:
        open_with = open
        mode = "r"
    for line in open_with(fastafile, mode):
        if line.startswith(">"):
            m=re.match(">Sc(.*)_",line)
            if m:
                return m.group(1)
            return

class ProjectArguments:
    
    def __init__(self, config, project_dir):
        self.data = self.read_config(config)
        self.project_dir = os.path.abspath(project_dir)
        self.update_args()
        pprint(self.data)

    def update_args(self):
        self.update_project_name()
        self.collect_fastq()
        self.find_assemblies()
        self.find_hirise_assemblies()
        self.find_bams()

    def update_project_name(self):
        base_dir = os.path.basename(self.project_dir)
        self.data.setdefault("project", {})
        self.data["project"].setdefault("project", base_dir)

    def collect_fastq(self):
        data_dir = os.path.join(self.project_dir, "data")

        if not os.path.exists(data_dir):
            return

        self.data.setdefault("fastq", {})

        for fastq_dir in self.data["fastq"].keys():
            full_fastq_dir = os.path.join(data_dir, fastq_dir)
            if not os.path.isdir(full_fastq_dir):
                continue
            self.data["fastq"].setdefault(fastq_dir, {})
            r1, r2 = self.find_read_data(full_fastq_dir)
            print(self.data["fastq"])
            self.data["fastq"][fastq_dir].setdefault("r1", r1)
            self.data["fastq"][fastq_dir].setdefault("r2", r2)
            
        for fastq_dir in os.listdir(data_dir):
            full_fastq_dir = os.path.join(data_dir, fastq_dir)
            if not os.path.isdir(full_fastq_dir):
                continue
            self.data["fastq"].setdefault(fastq_dir, {})
            r1, r2 = self.find_read_data(full_fastq_dir)
            self.data["fastq"][fastq_dir].setdefault("r1", r1)
            self.data["fastq"][fastq_dir].setdefault("r2", r2)

    def find_bams(self):
        bam_dir = os.path.join(self.project_dir, "analysis", "local")
        self.data.setdefault("bam", {})
        for root,dirs,files in os.walk(bam_dir):
            for f in files:
                if re.search('.*bam$', f):
                    dirname = os.path.basename(root)
                    # Go up one more if directory is "aln"
                    if dirname == "aln":
                        abs_dir = os.path.dirname(root)
                        dirname = os.path.basename(abs_dir)
                    self.data["bam"].setdefault(dirname, {})
                    bam_file = os.path.join(root, f)
                    self.data["bam"][dirname]["bam_file"] = bam_file


    def find_assemblies(self):
        assembly_dir = os.path.join(self.project_dir, "results")
        self.data.setdefault("assembly", {})
        for root,dirs,files in os.walk(assembly_dir):
            for f in files:
                if re.search('.*fasta$|.*fa$|.*fna$', f):
                    name = os.path.basename(root)
                    contigs = os.path.join(root, f)
                    customer_id = get_id(contigs)
                    if customer_id:
                        if customer_id in name:
                            continue
                        subcategory = "hirise"
                    else:
                        subcategory = "denovo"
                    print(name)
                    self.data["assembly"].setdefault(name, {})
                    self.data["assembly"][name]["contigs"] = contigs
                    self.data["assembly"][name]["subcategory"] = subcategory
                    self.data["assembly"][name]["name"] = name

    def find_hirise_assemblies(self):
        assembly_dir = os.path.join(self.project_dir, "analysis", "local")
        self.data.setdefault("assembly_stats_only", {})
        for root,dirs,files in os.walk(assembly_dir):
            for f in files:
                if re.search('hirise.*fasta$|hirise.*fa$', f):
                    name = os.path.join(root, f)
                    print(name)
                    self.data["assembly_stats_only"].setdefault(name, {})
                    contigs = os.path.join(root, f)
                    self.data["assembly_stats_only"][name]["contigs"] = contigs
                    self.data["assembly_stats_only"][name]["subcategory"] = "hirise"
                
    def read_config(self, config):
        if os.path.exists(config):
            with open(config) as handle:
                data = yaml.load(handle) or {}
        else:
            data = {}
        return data

    def find_read_data(self, data_dir):
        r1_filters = ["r1","R1", "_1"]
        r2_filters = ["r2","R2", "_2"]
        fastq_ext = [".fq", ".fastq", ".fq.gz", ".fastq.gz"]
        r1 = []
        r2 = []
        for entry in os.listdir(data_dir):
            if os.path.isdir(os.path.join(data_dir,entry)): continue
            is_fastq = False
            for ext in fastq_ext:
                if entry.endswith(ext):
                   is_fastq = True
            if not is_fastq:
                continue
            abs_path = os.path.abspath(os.path.join(data_dir,entry))
            this_read_pair = self.parse_read_pair(abs_path)

            if this_read_pair:
                if this_read_pair == 1:
                    r1.append(abs_path)
                elif this_read_pair == 2:
                    r2.append(abs_path)
                continue
            
            for r1_filter,ext in itertools.product(r1_filters,fastq_ext):
                if r1_filter in entry and entry.endswith(ext):
                    r1.append(abs_path)
            for r2_filter,ext in itertools.product(r2_filters,fastq_ext):
                if r2_filter in entry and entry.endswith(ext):
                    r2.append(abs_path)
        if not (r1 and r2) or len(r1) != len(r2):
            print("Warning: Did not find paired read files in directory: {0}\nHad {1} r1 and {2} r2.".format(data_dir, len(r1), len(r2)),file=sys.stderr)
            print("They were: {0} r1 and {1} r2.".format(r1, r2),file=sys.stderr)
            sys.exit(2)
        return sorted(r1), sorted(r2)

    def parse_read_pair(self, entry):
        read_type = self.get_fastq_pair(entry)
                
        if read_type == "1" or read_type == "2":
            return int(read_type)
        else:
            return


    def get_fastq_pair(self, fastq):
        illumina_1_name = r"[@]?[-_A-z0-9]+:[0-9]+:[0-9]+:[0-9]+:+[0-9]+" 
        illumina_1_comment = r"#[0-9]+/[1,2]"
        illumina_2_name = r"[@]?[-_A-z0-9]+:[0-9]+:[-_A-z0-9]+:[0-9]+:[0-9]+:[0-9]+:[0-9]+\s" 
        illumina_2_comment = r"[0-9]+:[YN]:[0-9]+:"

        sra_1 = r"[@]?SR[AR][0-9]+\.[0-9]\s" + illumina_1_name
        sra_2 = r"[@]?SR[AR][0-9]+\.[0-9]\s" + illumina_2_name
        
        for read in FastqHelper.read_lines(fastq):
            line = read.first_line
            if re.match(illumina_1_name + illumina_1_comment, line):
                return line.split("/")[1][0]
            elif re.match(illumina_2_name + illumina_2_comment, line):
                return line.split()[1][0]
            elif re.match(sra_1 + illumina_1_name, line):
                return line.split()[1].split("/")[1][0]
            elif re.match(sra_2 + illumina_2_name, line):
                return
            return

def whole_project(project_dir=".", config=None, skip_existing=False):
    if not config:
        config = os.path.join(project_dir, "project.yaml")
    pa = ProjectArguments(config, project_dir)
    project_info = pa.data["project"]
    read_info = pa.data["fastq"]
    
    new_project(
        project_name=project_info["project"],
        ticket=project_info.get("jira_ticket", None),
        skip_existing=skip_existing,
        description=project_info.get("description" or None),
        no_ticket=project_info.get("no_ticket" or None),
        )
    
    read_info = pa.data["fastq"]
    print("read info")
    pprint(read_info)
    for dataset in read_info.keys():
        print("dealing with {0}".format(dataset))
        dataset_info = read_info[dataset]
        r1, r2 = dataset_info["r1"], dataset_info["r2"]
        for read1, read2 in zip(r1, r2):
            print("placing {0} and {1}".format(read1, read2))
            new_read_pair(
                read1, read2, subcategory=dataset_info.get("subcategory", None), 
                jira_ticket=dataset_info.get("jira_ticket", None), 
                library=dataset_info.get("library", None), 
                no_ticket=dataset_info.get("no_ticket", None),
                project=project_info["project"], 
                no_validate=dataset_info.get("no_validate", None), 
                skip_run_id=dataset_info.get("skip_run_id", None),
                skip_existing=skip_existing)
            
    all_assembly_info = pa.data["assembly"]
    print("assembly info")
    pprint( all_assembly_info)
    for assembly in all_assembly_info.keys():
        print("dealing with {0}".format(assembly))
        assembly_info = all_assembly_info[assembly]
        if assembly_info["subcategory"] == "hirise":
            continue
        new_assembly(
            assembly_info["contigs"], assembly_info["subcategory"],
            project_name=project_info["project"], ticket=assembly_info.get("jira_ticket", None), 
            description=assembly_info.get("description", None), assembly_name=assembly,
            skip_existing=skip_existing)
    all_bam_info = pa.data["bam"]
    print("bam info")
    pprint(all_bam_info)
    for bam in all_bam_info.keys():
        print("dealing with {0}".format(bam))
        bam_info = all_bam_info[bam]
        new_bam(bam_info["bam_file"], skip_existing=skip_existing)

    for assembly in all_assembly_info.keys():
        print("dealing with {0}".format(assembly))
        assembly_info = all_assembly_info[assembly]
        if assembly_info["subcategory"] != "hirise":
            continue
        new_assembly(
            assembly_info["contigs"], assembly_info["subcategory"],
            project_name=project_info["project"], ticket=assembly_info.get("jira_ticket", None), 
            description=assembly_info.get("description", None),
            skip_existing=skip_existing)

    all_stats_only_info = pa.data["assembly_stats_only"]
    print("assembly info_stats_only")
    pprint( all_stats_only_info)

    for assembly in all_stats_only_info.keys():
        print("dealing with {0}".format(assembly))
        assembly_info = all_stats_only_info[assembly]
        new_assembly(
            assembly_info["contigs"], assembly_info["subcategory"],
            project_name=project_info["project"], ticket=assembly_info.get("jira_ticket", None), 
            description=assembly_info.get("description", None), no_upload=True,
            skip_existing=True)
    
    graph_rep = os.path.join(project_dir, "database_entries.txt")
    graph_rep_pdf = os.path.join(project_dir, "database_entries.pdf")

    graph_db.main(project_info["project"], output=graph_rep)
    subprocess.call(["dot", graph_rep, "-T", "pdf", "-o", graph_rep_pdf]) 

def new_project(
        project_name=None, ticket=None, skip_existing=False,
        dryrun=False, description=None,
        no_ticket=False):

    if not (project_name):
        raise MetadataError("Must provide project name or select query")

    project = Project()
    print("existing", project.get_existing_project(project_name), 
          project.get_existing_project(project_name) == {} )
    print("skip existing", skip_existing)
    if skip_existing and project.get_existing_project(project_name) != None:
        print("SKIPPING EXISTING PROJECT")
        return
    else:
        project.is_only_project(project_name)

            
    project.set_params(
        project_name=project_name, jira_ticket=ticket, 
        description=description, 
        no_ticket=no_ticket)

    project.show_attributes()
    project.put_attributes(dryrun)


@argh.arg("subcategory", choices=["hirise", "denovo", "customer"]) 
def new_assembly(assembly_file, subcategory, no_upload=False,
                 project_name=None, s3_key=None, ticket=None, assembly_name=None,
                 dryrun=False, description=None, skip_existing=False):

    if not (project_name or select or project_uid):
        raise MetadataError("Must provide project name or select query or project uid")

    assembly = Assembly()
    md5sum = None

    print("getting md5sum {0}".format(assembly_file))
    md5sum = get_md5sum_file(assembly_file)
    if not md5sum:
        md5sum = get_md5sum(assembly_file)
        add_md5sum_file(assembly_file, md5sum)
    print(assembly.is_only_md5sum(md5sum=md5sum))

    if not assembly.is_only_md5sum(md5sum=md5sum):
        if skip_existing:
            print("Not updating {0}".format(assembly_file))
            return
        raise MetadataError("Assembly with md5sum {0} already exists".format(md5sum))

    assembly.set_params(
        assembly_file, subcategory, project_name=project_name, s3_key=s3_key, jira_ticket=ticket, 
        description=description, md5sum=md5sum, assembly_name=assembly_name)

    if no_upload:
        assembly.attributes.pop("s3_key", None)

    assembly.show_attributes()
    if not no_upload and not dryrun:
        assembly.upload_fasta()
    if not dryrun:
        print("Inserting : {0}".format(assembly_file))
        assembly.put_attributes(dryrun)


@argh.arg("-s", "--subcategory", choices=["Chicago Type", "Shotgun Type"])
def new_read(fastq, subcategory=None, jira_ticket=None, library=None, dryrun=False, force=False, no_ticket=False,
             project=None):
    read = Read()
    read.set_params(fastq, subcategory=subcategory, no_ticket=no_ticket,
                        jira_ticket=jira_ticket, library=library, project_name=project)
    read.show_attributes()
    if not dryrun:
        read.upload_fastq()
        read.put_attributes(dryrun)

@argh.arg("-s", "--subcategory", choices=["Chicago Type", "Shotgun Type"])
def new_read_pair(fastq, fastq2, subcategory=None, jira_ticket=None, library=None, dryrun=False, no_ticket=False,
                  project=None, no_validate=False, skip_run_id=False, skip_existing=False):
    read_type = None
    if subcategory not in ["Chicago Type", "Shotgun Type", None]:
        raise MetadataError("{0} not valid read type".format(subcategory))
    if no_validate:
        read_type = 1
    read1 = Read()
    md5sum = None
    md5sum2 = None

    print("getting md5sum {0}".format(fastq))
    md5sum = get_md5sum_file(fastq)
    if not md5sum:
        md5sum = get_md5sum(fastq)
        add_md5sum_file(fastq, md5sum)
    print("getting md5sum {0}".format(fastq2))
    md5sum2 = get_md5sum_file(fastq2)
    if not md5sum2:
        md5sum2 = get_md5sum(fastq2)
        add_md5sum_file(fastq2, md5sum2)
    r1_missing = read1.is_only_md5sum(md5sum=md5sum)
    r2_missing = read1.is_only_md5sum(md5sum=md5sum2)
    if r1_missing != r2_missing:
        raise MetadataError("{0} is missing: {1} , but {2} is missing: {3}".format(fastq, r1_missing, fastq2, r2_missing))

    if not r1_missing and not r2_missing:
        if skip_existing:
            print("returning")
            return
        raise MetadataError("Reads exist: {0} {1}".format(fastq, fastq2))
        
    print("setting params {0}".format(fastq))
    
    read1.set_params(
        fastq, subcategory=subcategory, no_ticket=no_ticket,
        jira_ticket=jira_ticket, library=library, project_name=project, read_type=read_type, skip_run_id=skip_run_id, md5sum=md5sum)

    dataset_attr = read1.get_dataset(fastq, project, library)
    library_attr = read1.get_library(library)
    new_dataset = False
    new_library = False

    if not dataset_attr:
        print("dataset")
        library_attr = read1.get_library(library)
        new_dataset = True
        if not library_attr:
            print("library")
            new_library = True
            library_obj = Library()
            print(project)
            library_obj.set_params(project=project, jira_ticket=read1.attributes["jira_ticket"], library=library, subcategory=subcategory)
            library_obj.attributes["_id"] = ObjectId()
            pprint(library_obj.attributes)
            library_attr = library_obj.attributes
            library_obj.put_attributes(dryrun)
        dataset_obj = Dataset()
        dataset_obj.set_params(project=project, library=library_attr["_id"], name=read1.get_dataset_name(fastq, project))
        dataset_obj.attributes["_id"] = ObjectId()
        pprint(dataset_obj.attributes)
        dataset_attr = dataset_obj.attributes
        dataset_obj.put_attributes(dryrun)

    read1.attributes["dataset_id"] = dataset_attr["_id"]
    read1.set_default_s3_key(read1.attributes, fastq)
    print(read1.attributes["s3_key"])
    if no_validate:
        read_type = 2

    read2 = Read()
    print("setting params {0}".format(fastq2))
    read2.set_params(fastq2, subcategory=subcategory, no_ticket=no_ticket,
                        jira_ticket=jira_ticket, library=library, project_name=project, read_type=read_type, skip_run_id=skip_run_id, md5sum=md5sum2)

    read2.attributes["dataset_id"] = dataset_attr["_id"]
    read2.set_default_s3_key(read2.attributes, fastq2)
    print(read2.attributes["s3_key"])
    if not no_validate:
        read1.check_are_paired(read1, read2)
    read1.attributes["_id"] = ObjectId()
    read2.attributes["_id"] = ObjectId()
    read1.add_mate(read2)
    read2.add_mate(read1)
    read1.show_attributes()
    read2.show_attributes()
    if not dryrun:
        read1.upload_fastq()
        read2.upload_fastq()

    read1.put_attributes(dryrun)
    read2.put_attributes(dryrun)

def new_bam(bam_file, dryrun=False, skip_existing=False):
    bam = Bam()
    md5sum = None
    if skip_existing:
        print("getting md5sum {0}".format(bam_file))
        md5sum = get_md5sum_file(bam_file)

        if not md5sum:
            print("No md5sum in file")
            md5sum = get_md5sum(bam_file)
            add_md5sum_file(bam_file, md5sum)
        if not bam.is_only_md5sum(md5sum=md5sum):
            print("Not updating {0}".format(bam_file))
            return


    bam.set_params(bam_file, md5sum=md5sum)
    bam.show_attributes()
    if not dryrun:
        bam.upload_bam()
        bam.put_attributes(dryrun)

def get_md5sum_file(afile):
    basename = os.path.basename(afile)
    if basename.endswith(".gz"):
        basename = os.path.splitext(basename)[0]
    parent_dir = os.path.dirname(afile)
    md5sum_file = os.path.join(parent_dir, "md5sum.txt")
    print("Looking for md5sum of {0} in {1}".format(afile, md5sum_file))
    if os.path.exists(md5sum_file):
        with open(md5sum_file) as handle:
            for line in handle:
                s = line.split()
                print(s[1], basename, s[1] == basename)
                if s[1] == basename:
                    md5sum = s[0]
                    print("using md5sum {0}".format(md5sum))
                    return md5sum

def add_md5sum_file(afile, md5sum):
    print(afile)
    basename = os.path.basename(afile)
    print(basename)
    if basename.endswith(".gz"):
        basename = os.path.splitext(basename)[0]
    parent_dir = os.path.dirname(afile)
    md5sum_file = os.path.join(parent_dir, "md5sum.txt")
    with open(md5sum_file, "a") as handle:
        print(md5sum, basename, file=handle)
    
parser = argh.ArghParser()
parser.add_commands([new_project, new_assembly, new_read, new_read_pair, new_bam, whole_project])

if __name__ == "__main__":
    parser.dispatch()
