from snakemake.utils import min_version

# snakemake 3.2 supports benchmarking
min_version("3.2")

configfile: "config.json"
import os
import glob
import itertools

cwd = os.getcwd();

#             + human/
#               + aln // all alignments
#               + analysis // insert distribution, reports generate
#               + benchmarks // benchmarks created by snakemake

# Project, eg human

try:
    cores = config["cores"]
except KeyError:
    cores = 16

# Library name, eg chicago_007
lib = config["lib"]

raw_regex_r1 = config["raw_regex_r1"]
raw_regex_r2 = config["raw_regex_r2"]

# SNAP index for the assembly
try:
    snap_index = config["snap_index_chicago"]
except KeyError:
    snap_index = "/mnt/nas/projects/human/results/hg19/snap_defaults"


###############################################################################

# Sort the read files so that read1 and read2 match up correctly.
raw_r1 = sorted(itertools.chain.from_iterable(glob.glob(i) for i in raw_regex_r1.split('|')))
raw_r2 = sorted(itertools.chain.from_iterable(glob.glob(i) for i in raw_regex_r2.split('|')))
r1_base = os.path.splitext(os.path.splitext(os.path.basename(raw_r1[0]))[0])[0]
r2_base = os.path.splitext(os.path.splitext(os.path.basename(raw_r2[0]))[0])[0]

# Cat fastq files together because SNAP memory maps fastq files and can 
# blow out memory
merged_r1 = "merged_r1.fq.gz"
merged_r2 = "merged_r2.fq.gz"

# Bam file.
md_bam = "aln/" + lib + '.snap.md.sorted.bam'
report_tex= lib + "_clevelandReport.tex"
report= lib + "_clevelandReport.pdf"
package= lib + "_clevelandAnalysis.tar"

###############################################################################

rule all:
    input: "results.json",report,package
 
rule package:
    input: "results.json","analysis/clevelandMapping.png", "analysis/mapped.barcode_counts.R1.txt","analysis/mapped.barcode_counts.R2.txt","analysis/unmapped.barcode_counts.R1.txt","analysis/unmapped.barcode_counts.R2.txt","analysis/mapped.barcode_counts.R1.png","analysis/mapped.barcode_counts.R2.png","analysis/unmapped.barcode_counts.R1.png","analysis/unmapped.barcode_counts.R2.png",report,"fastqc_r1/" + lib + ".r1.html", "fastqc_r2/" + lib + ".r2.html"
    output: package
    run:
        files = " ".join(input)
        shell("tar -cvf {output} %s" % files)
    

rule make_report:
    input: "results.json","analysis/clevelandMapping.png", "analysis/mapped.barcode_counts.R1.txt","analysis/mapped.barcode_counts.R2.txt","analysis/unmapped.barcode_counts.R1.txt","analysis/unmapped.barcode_counts.R2.txt","analysis/mapped.barcode_counts.R1.png","analysis/mapped.barcode_counts.R2.png","analysis/unmapped.barcode_counts.R1.png","analysis/unmapped.barcode_counts.R2.png"
    output: report
    shell:"""
        cleveland_report.py -s {input[0]} -m {input[1]} -c {input[2]} {input[3]} {input[4]} {input[5]} -p {input[6]} {input[7]} {input[8]} {input[9]} -o {report_tex}
        pdflatex {report_tex}
    """
        

#Valid pairs defined as read pairs where at least one read contains all 3 barcode
#sequences, and each read pair is at least length 1 after trimming the barcode
rule get_stats:
    input: "analysis/lc_extrap.paired.txt", "analysis/lc_extrap.single.txt", "analysis/bam_stats.json"
    output: "results.json"
    shell: """
    echo '{{ }} ' > results.json
    dove-pipe set -s 'library_complexity_preseq_SE' -v $(awk '$1==300000000 {{printf "%.0f", $2}}' {input[1]}) -j results.json -o results.json --prefix ""
    dove-pipe set -s 'library_complexity_preseq_PE' -v $(awk '$1==300000000 {{printf "%.0f", $2}}' {input[0]}) -j results.json -o results.json --prefix ""
    dove-pipe merge -m  {input[2]} -s 'mapping' -j results.json -o results.json --prefix ""
    """

rule cat_fastq:
    """Cat fastq files together because SNAP memory maps fastq files and can 
    blow out memory"""
    input: raw_r1, raw_r2
    output: temp(merged_r1), temp(merged_r2)
    benchmark: "benchmarks/cat_fastq.json"
    shell: """
    if [[ {raw_r1[0]} =~ \.gz$ ]];	
    then
    zcat {raw_r1} | pigz -p {cores} -c > {output[0]}
    zcat {raw_r2} | pigz -p {cores} -c > {output[1]}
    else
    cat {raw_r1} | pigz -p {cores} -c > {output[0]}
    cat {raw_r2} | pigz -p {cores} -c > {output[1]}
    
    fi
    """


rule fastqc:
    input: raw_r1, raw_r2
    output: "fastqc_r1/" + lib + ".r1.html", "fastqc_r2/" + lib + ".r2.html"
    shell: """

    fastqc -o fastqc_r1 {input[0]}
    mv fastqc_r1/{r1_base}_fastqc.html {output[0]}

    fastqc -o fastqc_r2 {input[1]}
    mv fastqc_r2/{r2_base}_fastqc.html {output[1]}
    """

rule align:
    input: merged_r1,merged_r2 
    output: (lib + ".snap.bam")
    shell: """
    snap paired {snap_index} {input[0]} {input[1]}  -t 16 -o \
    -bam {output} -ku -as -C-+ -= -mrl 20 -pf \
    alignment_stats.txt 2>&1 | tee snap.err
    """

rule sort:
    input: lib + ".snap.bam" 
    output: md_bam
    shell: """
    samtools view -u {input} | novosort -i -t . --compression 3 --md -t ./ -  --output {output} 2> sort.err
    """

rule bam_stats:
    input: md_bam
    output: "analysis/bam_stats.json"
    shell: "bam_stats_only.py -b {input} -o {output}" 

rule separate_mapped:
    input: md_bam
    output: "aln/mapped.snap.md.sorted.bam","aln/unmapped.snap.md.sorted.bam"
    shell: """
        samtools view -b -F 1028 {input} > {output[0]}
        samtools view -b -F 1024 -f 4 {input} > {output[1]}
    """

rule count_barcodes:
    input: "aln/{group}.snap.md.sorted.bam"
    output: "analysis/{group}.barcode_counts.R1.txt","analysis/{group}.barcode_counts.R2.txt"
    shell: """
        samtools view -f 64 {input} | cut -f 10 | cut -c -10 | sort | uniq -c | sort -k1,1nr > {output[0]}
        samtools view -f 128 {input} | cut -f 10 | cut -c -10 | sort | uniq -c | sort -k1,1nr > {output[1]}
    """

rule barcode_plots:
    input: "analysis/{prefix}.txt"
    output: "analysis/{prefix}.png"
    shell: "counts2plot.sh {input}"

rule mapping_plots:
    input: md_bam
    output: "analysis/clevelandMapping.png"
    shell: "mappingHisto.py -i {input} -o {output}"

rule single_duplicates:
    input: md_bam
    output: "analysis/dups_single.txt"
    shell: "duplicate_histogram.py {input}  --single --output {output}"

rule single_preseq:
    input: "analysis/dups_single.txt"
    output: "analysis/lc_extrap.single.txt"
    shell: """preseq lc_extrap -H {input}  > {output}
    if [[ $? != 0 ]];     
    then
    echo -e "300000000\t0" > {output}
    echo "WARNING: preseq paired did not run successfully" 
    fi
"""

rule duplicate_histogram:
    """Generate histogram of duplicates using PE"""
    input: md_bam
    output: "analysis/dup_histogram_paired.txt"
    shell: "duplicate_histogram.py {input} -o {output}"

rule preseq_lc_extrap_paired:
    """Extrapolate unique reads from existing coverage up to many millions,
    using paired duplicates"""
    input: md_bam, "analysis/dup_histogram_paired.txt"
    output: "analysis/lc_extrap.paired.txt"
    benchmark: "benchmarks/lc_extrap_paired.out"
    shell: """
    preseq lc_extrap -H {input[1]} -o {output} -s 5000000
    if [[ $? != 0 ]];
    then
    echo -e "300000000\t0" > {output}
    echo "WARNING: preseq paired did not run successfully"
    fi
    """
